<?php
/**
 * Главный файл конфигурации приложения.
 * Описание структуры возвращаемого массива:
 * 
 * 	array(
 * 		'name'=>value,
 * 		...
 * 	)
 */
Yii::setPathOfAlias('widgets', dirname(dirname(__FILE__)).'/'.'components'.'/'.'widgets');

return array(
	'basePath'=>dirname(dirname(__FILE__)),
	'name'=>'Фонд капитального ремонта ПК',
	'defaultController'=>'main/index',
	'sourceLanguage'=>'ru',
	'language'=>'ru',
	'charset'=>'utf-8',
	'preload'=>array('param'),
	
	'import'=>array(
		'application.components.*',
		'application.components.connection.*',
		'application.components.directory.*',
		'application.components.system.*',
		'application.components.validators.*',
		'application.components.widgets.ActiveForm.ActiveForm',
		
		'application.controllers.*',
		'application.controllers.actions.*',
		'application.helpers.*',
		
		'application.models.account.*',
		'application.models.appeal.*',
		'application.models.auth.*',
		'application.models.counter.*',
		'application.models.compensation.*',
		'application.models.deploy.*',
		'application.models.directory.*',
		'application.models.export.*',
		'application.models.file.*',
		'application.models.house.*',
		'application.models.import.*',
		'application.models.log.*',
		'application.models.manage.*',
		'application.models.message.*',
		'application.models.reserve.*',
		'application.models.scheme.*',
		'application.models.service.*',
		'application.models.setting.*',
		'application.models.user.*'
	),
	
	'components'=>array(
		// Системные компоненты
		'cache'=>array(
			'class'=>'DummyCache'
		),
		'db'=>array(
			'class'=>'DbConnection',
			'charset'=>'utf8',
			'tablePrefix'=>'rgis_',
		),
		'dbOld'=>array(
			'class'=>'CDbConnection',
			'connectionString'=>'pgsql:host=192.168.60.28;port=5432;dbname=lkc',
			'username'=>'su',
			'password'=>'SkiMra$d',
// 			'connectionString'=>'pgsql:host=localhost;port=5432;dbname=test',
// 			'username'=>'postgres',
// 			'password'=>'',
			'charset'=>'utf8',
			'tablePrefix'=>'lkz_',
		),
		'errorHandler'=>array(
			'errorAction'=>'main/error'
		),
		'session'=>array(
			'class'=>'CHttpSession',
			'autoStart'=>false
		),
		'urlManager'=>array(
			'class'=>'UrlManager',
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
				'private'=>'main/private',
				'profile/<action:\w+>'=>'user/p<action>',
				'<action:(registration|restore)>/<code:[A-Za-z\d]+>'=>'user/<action>',
				'<action:(logout|registration)>/*'=>'user/<action>',
				'<controller:\w+>/<action:\w+>/<id:\d+>/*'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>/*'=>'<controller>/<action>',
// 				'<controller:\w+>/<id:\d+>/*'=>'<controller>/index',
				'<controller:\w+>/*'=>'<controller>/index',
			)
		),
		'user'=>array(
			'class'=>'WebUser'
		),
		
		// Компоненты приложения
		'accessAttribute'=>array(
			'class'=>'AccessAttribute'
		),
		'actionConfirm'=>array(
			'class'=>'ActionConfirm'
		),
		'appeal'=>array(
			'class'=>'Appeal'
		),
		'asset'=>array(
			'class'=>'Asset',
			'forceCopy'=>YII_DEBUG
		),
		'attribute'=>array(
			'class'=>'Attribute'
		),
		'attributePattern'=>array(
			'class'=>'AttributePattern'
		),
		'attributePatternGroupSet'=>array(
			'class'=>'AttributePatternGroupSet'
		),
		'authDb'=>array(
			'class'=>'DbAuthManager',
			'defaultRoles'=>array('guest')
		),
		'authPhp'=>array(
			'class'=>'PhpAuthManager',
			'defaultRoles'=>array('guest', 'developer')
		),
		'bss1'=>array(
			'class'=>'Bss1'
		),
		'bss2'=>array(
			'class'=>'Bss2'
		),
		'bssOld'=>array(
			'class'=>'BssOld'
		),
		'code'=>array(
			'class'=>'UserCode'
		),
		'conformity'=>array(
			'class'=>'Conformity'
		),
		'directory'=>array(
			'class'=>'ComponentDirectory'
		),
		'email'=>array(
			'class'=>'Email'
		),
		'errorAttribute'=>array(
			'class'=>'ErrorAttribute'
		),
		'errorEvent'=>array(
			'class'=>'ErrorEvent'
		),
		'esia'=>array(
			'class'=>'Esia'
		),
		'event'=>array(
			'class'=>'Event'
		),
		'fias'=>array(
			'class'=>'Fias'
		),
		'file'=>array(
			'class'=>'File'
		),
		'log'=>array(
			'class'=>'Log'
		),
		'label'=>array(
			'class'=>'Label'
		),
		'notification'=>array(
			'class'=>'Notification'
		),
		'notificationMessage'=>array(
			'class'=>'NotificationMessage'
		),
		'param'=>array(
			'class'=>'Param',
		),
		'phone'=>array(
			'class'=>'Phone',
		),
		'portal'=>array(
			'class'=>'Portal'
		),
		'scheme'=>array(
			'class'=>'Scheme'
		),
		'setting'=>array(
			'class'=>'Setting'
		),
		'table'=>array(
			'class'=>'Table'
		),
		'test'=>array(
			'class'=>'Test'
		),
		'value'=>array(
			'class'=>'Value'
		)
	)
	
);
