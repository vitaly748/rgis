<?php
return array(
	'urn:esia:orgShortName'=>array('ИП ЗУБОВ В. В.'),
	'urn:mace:dir:attribute:userId'=>array('1022329580'),
	'urn:esia:orgType'=>array('B'),
	'urn:esia:globalRole'=>array('E'),
	'urn:esia:principalAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalAddresses>
<address>
<addressType>PERSON_REGISTRATION</addressType>
<contryChar3Code>RUS</contryChar3Code>
<index>617050</index>
<region>Пермский Край</region>
<district>Краснокамский Район</district>
<settlement>Оверята Поселок</settlement>
<street>Строителей Улица</street>
<house>8</house>
<flat>47</flat>
</address>
<address>
<addressType>PERSON_LIVE</addressType>
<contryChar3Code>RUS</contryChar3Code>
<index>617050</index>
<region>Пермский Край</region>
<district>Краснокамский Район</district>
<settlement>Оверята Поселок</settlement>
<street>Строителей Улица</street>
<house>8</house>
<flat>47</flat>
</address>
</principalAddresses>'),
	'urn:esia:gender'=>array('MALE'),
	'urn:esia:orgContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgContacts>
<contact>
<contactType>PHN</contactType>
<value>+7(342)2000438*5607</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>EML</contactType>
<value>v.zubov@softm.tv</value>
<verificationStatus>N</verificationStatus>
</contact>
</orgContacts>'),
	'urn:mace:dir:attribute:middleName'=>array('Васильевич'),
	'urn:esia:orgOGRN'=>array('317595800020385'),
	'urn:mace:dir:attribute:firstName'=>array('Виталий'),
	'urn:esia:personOGRN'=>array('317595800020385'),
	'urn:esia:personCitizenship'=>array('RUS'),
	'urn:esia:orgPosition'=>array('Руководитель'),
	'urn:mace:dir:attribute:lastName'=>array('Зубов'),
	'urn:esia:principalDocuments'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalDocuments>
<document>
<documentType>01</documentType>
<verificationStatus>S</verificationStatus>
<number>704162</number>
<series>5705</series>
<issueDate>2005-11-03T00:00:00</issueDate>
<issuedBy>УВД Ленинского района г.Перми</issuedBy>
</document>
<document>
<documentType>05</documentType>
<verificationStatus>N</verificationStatus>
<number>847104</number>
<series>5905</series>
</document>
</principalDocuments>'),
	'urn:esia:personSNILS'=>array('103-060-412 76'),
	'urn:esia:authnMethod'=>array('PWD'),
	'urn:esia:birthDate'=>array('02-10-1985 00:00:00'),
	'urn:esia:orgOid'=>array('1039736873'),
	'urn:esia:personEMail'=>array('vitaly.fsb@mail.ru'),
	'urn:esia:orgINN'=>array('810700351764'),
	'urn:esia:personTrusted'=>array('Y'),
	'urn:mace:dir:attribute:authToken'=>array('PO-C7BI9-SApWOeSbJBKPsfIlOVx2mWElzdH'),
	'urn:esia:orgName'=>array('Индивидуальный предприниматель ЗУБОВ ВИТАЛИЙ ВАСИЛЬЕВИЧ'),
	'urn:esia:personINN'=>array('810700351764'),
	'urn:esia:personMobilePhone'=>array('+7(909)1145970'),
	'urn:esia:systemAuthority'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<esia-encoder:Authorities xmlns:esia-encoder="urn:esia:shibboleth:2.0:attribute:encoder">
<esia-encoder:Authority system="SIA">ORG_CHIEF</esia-encoder:Authority>
</esia-encoder:Authorities>'),
	'urn:esia:assuranceLevel'=>array('AL20'),
	'urn:esia:principalContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalContacts>
<contact>
<contactType>EML</contactType>
<value>vitaly.fsb@mail.ru</value>
<verificationStatus>S</verificationStatus>
</contact>
<contact>
<contactType>MBT</contactType>
<value>+7(909)1145970</value>
<verificationStatus>S</verificationStatus>
</contact>
<contact>
<contactType>CEM</contactType>
<value>v.zubov@softm.tv</value>
<verificationStatus>N</verificationStatus>
</contact>
</principalContacts>')
);
