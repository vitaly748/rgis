<?php
return array(
	'urn:mace:dir:attribute:userId'=>array('1000299654'),
	'urn:esia:globalRole'=>array('P'),
	'urn:esia:gender'=>array('MALE'),
	'urn:mace:dir:attribute:middleName'=>array('Отчество002'),
	'urn:mace:dir:attribute:firstName'=>array('Имя002'),
	'urn:mace:dir:attribute:lastName'=>array('Фамилия002'),
	'urn:esia:personSNILS'=>array('000-000-600 02'),
	'urn:esia:authnMethod'=>array('PWD'),
	'urn:esia:birthDate'=>array('02-01-1998 00:00:00'),
	'urn:esia:personEMail'=>array('EsiaTest002@yandex.ru'),
	'urn:esia:personTrusted'=>array('Y'),
	'urn:mace:dir:attribute:authToken'=>array('O59chgEAAAAAWMFkMbfWpjjEUzEPSikKqiBP'),
	'urn:esia:personINN'=>array('000123123002'),
	'urn:esia:personMobilePhone'=>array('+7(000)0000002'),
	'urn:esia:assuranceLevel'=>array('AL20'),
	'urn:esia:principalContacts'=>array('


EML
EsiaTest002@yandex.ru
S


MBT
+7(000)0000002
S

')
);
