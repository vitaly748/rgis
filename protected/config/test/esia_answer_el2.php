<?php
return array(
	'urn:esia:orgShortName'=>array('ООО "СОФТ М"'),
	'urn:mace:dir:attribute:userId'=>array('169199'),
	'urn:esia:orgType'=>array('L'),
	'urn:esia:globalRole'=>array('E'),
	'urn:esia:principalAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalAddresses>
<address>
<addressType>PERSON_REGISTRATION</addressType>
<contryChar3Code>RUS</contryChar3Code>
<kladrCode>59000000000</kladrCode>
<russianRegionCode>59</russianRegionCode>
<index>614105</index>
<region>Пермский край</region>
<city>г Пермь</city>
<settlement>п Новые Ляды</settlement>
<street>ул 40 лет Победы</street>
<house>60</house>
</address>
<address>
<addressType>PERSON_LIVE</addressType>
<contryChar3Code>RUS</contryChar3Code>
<kladrCode>59000000000</kladrCode>
<russianRegionCode>59</russianRegionCode>
<index>614105</index>
<region>Пермский край</region>
<city>г Пермь</city>
<settlement>п Новые Ляды</settlement>
<street>ул 40 лет Победы</street>
<house>60</house>
</address>
</principalAddresses>'),
	'urn:esia:gender'=>array('MALE'),
	'urn:esia:orgContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgContacts>
<contact>
<contactType>EML</contactType>
<value>info@softm.tv</value>
<verificationStatus>N</verificationStatus>
</contact>
</orgContacts>'),
	'urn:mace:dir:attribute:middleName'=>array('Расихович'),
	'urn:esia:orgOGRN'=>array('1125903003093'),
	'urn:mace:dir:attribute:firstName'=>array('Руслан'),
	'urn:esia:personCitizenship'=>array('RUS'),
	'urn:esia:orgPosition'=>array('ГЕНЕРАЛЬНЫЙ ДИРЕКТОР'),
	'urn:mace:dir:attribute:lastName'=>array('Каримов'),
	'urn:esia:principalDocuments'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalDocuments>
<document>
<documentType>01</documentType>
<verificationStatus>S</verificationStatus>
<number>833876</number>
<series>5703</series>
<issueDate>2003-03-28T00:00:00</issueDate>
<issuedBy>УВД Индустриального района гор.Перми</issuedBy>
</document>
<document>
<documentType>05</documentType>
<verificationStatus>N</verificationStatus>
<number>465320</number>
<series>5919</series>
<issueDate>2004-10-26T00:00:00</issueDate>
<expiryDate>2024-10-25T00:00:00</expiryDate>
</document>
<document>
<documentType>06</documentType>
<verificationStatus>N</verificationStatus>
<number>АБ926919</number>
</document>
</principalDocuments>'),
	'urn:esia:personSNILS'=>array('028-987-429 17'),
	'urn:esia:authnMethod'=>array('PWD'),
	'urn:esia:birthDate'=>array('07-09-1970 00:00:00'),
	'urn:esia:orgOid'=>array('1023077001'),
	'urn:esia:personEMail'=>array('badrus@mail.ru'),
	'urn:esia:orgINN'=>array('5903100206'),
	'urn:esia:personTrusted'=>array('Y'),
	'urn:mace:dir:attribute:authToken'=>array('aAKU7wo8-uqJWPcZUP032v7_XVTzh8g9sIOg'),
	'urn:esia:orgAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgAddresses>
<address>
<addressType>ORG_LEGAL</addressType>
<contryChar3Code>RUS</contryChar3Code>
<kladrCode>590000010000715</kladrCode>
<russianRegionCode>59</russianRegionCode>
<index>614087</index>
<region>Пермский край</region>
<city>г Пермь</city>
<street>ул Малкова</street>
<house>12</house>
<flat>124</flat>
</address>
</orgAddresses>'),
	'urn:esia:orgName'=>array('ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "СОФТ МЕНЕДЖМЕНТ"'),
	'urn:esia:personINN'=>array('590579403423'),
	'urn:esia:personMobilePhone'=>array('+7(922)3070516'),
	'urn:esia:orgKPP'=>array('590301001'),
	'urn:esia:systemAuthority'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<esia-encoder:Authorities xmlns:esia-encoder="urn:esia:shibboleth:2.0:attribute:encoder">
<esia-encoder:Authority system="SIA">ORG_CHIEF</esia-encoder:Authority>
</esia-encoder:Authorities>'),
	'urn:esia:assuranceLevel'=>array('AL20'),
	'urn:esia:orgLegalForm'=>array('Общества с ограниченной ответственностью'),
	'urn:esia:principalContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalContacts>
<contact>
<contactType>CEM</contactType>
<value>r.karimov@softm.tv</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>MBT</contactType>
<value>+7(922)3070516</value>
<verificationStatus>S</verificationStatus>
</contact>
<contact>
<contactType>EML</contactType>
<value>badrus@mail.ru</value>
<verificationStatus>S</verificationStatus>
</contact>
<contact>
<contactType>CPH</contactType>
<value>+7(342)2000260</value>
<verificationStatus>N</verificationStatus>
</contact>
</principalContacts>')
);
