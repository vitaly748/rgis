<?php
return array(
	'urn:esia:orgShortName'=>array('Тестовая организация'),
	'urn:mace:dir:attribute:userId'=>array('1000299654'),
	'urn:esia:orgType'=>array('L'),
	'urn:esia:globalRole'=>array('E'),
	'urn:esia:gender'=>array('MALE'),
	'urn:esia:orgContacts'=>array('


PHN
+7(495)5666667*443466
N


FAX
+7(495)8765985
N


EML
EsiaTest004OL@mfsa.ru
N

			'),
	'urn:mace:dir:attribute:middleName'=>array('Отчество002'),
	'urn:esia:orgOGRN'=>array('1234567890123'),
	'urn:mace:dir:attribute:firstName'=>array('Имя002'),
	'urn:esia:orgPosition'=>array('должность004'),
	'urn:mace:dir:attribute:lastName'=>array('Фамилия002'),
	'urn:esia:personSNILS'=>array('000-000-600 02'),
	'urn:esia:authnMethod'=>array('PWD'),
	'urn:esia:birthDate'=>array('02-01-1998 00:00:00'),
	'urn:esia:orgOid'=>array('1000299543'),
	'urn:esia:personEMail'=>array('EsiaTest002@yandex.ru'),
	'urn:esia:orgINN'=>array('7700123123'),
	'urn:esia:personTrusted'=>array('Y'),
	'urn:mace:dir:attribute:authToken'=>array('O59chgEAAAAAWMFkMbfWpjjEUzEPSikKqiBP'),
	'urn:esia:orgAddresses'=>array('


ORG_POSTAL
RUS
117418
Москва Город
Цюрупы Улица
11
5


ORG_LEGAL
RUS
127434
Город Москва
Улица Дубки

			'),
	'urn:esia:orgName'=>array('ОРГАНИЗАЦИЯ -869565001'),
	'urn:esia:personINN'=>array('000123123002'),
	'urn:esia:personMobilePhone'=>array('+7(000)0000002'),
	'urn:esia:systemAuthority'=>array('

ORG_CHIEF
			'),
	'urn:esia:assuranceLevel'=>array('AL20'),
	'urn:esia:orgLegalForm'=>array('Общества с ограниченной ответственностью'),
	'urn:esia:principalContacts'=>array('


MBT
+7(000)0000004
S


CEM
esiatest-OL004@gmail.ru
N


EML
EsiaTest004@yandex.ru
S


CPH
+7(900)0000000*000004
N

			')
);
