<?php
return array(
	'urn:mace:dir:attribute:userId'=>array('1022329580'),
	'urn:esia:globalRole'=>array('P'),
	'urn:esia:principalAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalAddresses>
<address>
<addressType>PERSON_REGISTRATION</addressType>
<contryChar3Code>RUS</contryChar3Code>
<index>617050</index>
<region>Пермский Край</region>
<district>Краснокамский Район</district>
<settlement>Оверята Поселок</settlement>
<street>Строителей Улица</street>
<house>8</house>
<flat>47</flat>
</address>
<address>
<addressType>PERSON_LIVE</addressType>
<contryChar3Code>RUS</contryChar3Code>
<index>617050</index>
<region>Пермский Край</region>
<district>Краснокамский Район</district>
<settlement>Оверята Поселок</settlement>
<street>Строителей Улица</street>
<house>8</house>
<flat>47</flat>
</address>
</principalAddresses>'),
	'urn:esia:gender'=>array('MALE'),
	'urn:mace:dir:attribute:middleName'=>array('Васильевич'),
	'urn:mace:dir:attribute:firstName'=>array('Виталий'),
	'urn:esia:personCitizenship'=>array('RUS'),
	'urn:mace:dir:attribute:lastName'=>array('Зубов'),
	'urn:esia:principalDocuments'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalDocuments>
<document>
<documentType>01</documentType>
<verificationStatus>S</verificationStatus>
<number>704162</number>
<series>5705</series>
<issueDate>2005-11-03T00:00:00</issueDate>
<issuedBy>УВД Ленинского района г.Перми</issuedBy>
</document>
<document>
<documentType>05</documentType>
<verificationStatus>N</verificationStatus>
<number>847104</number>
<series>5905</series>
</document>
</principalDocuments>'),
	'urn:esia:personSNILS'=>array('103-060-412 76'),
	'urn:esia:authnMethod'=>array('PWD'),
	'urn:esia:birthDate'=>array('02-10-1985 00:00:00'),
	'urn:esia:personEMail'=>array('vitaly.fsb@mail.ru'),
	'urn:esia:personTrusted'=>array('Y'),
	'urn:mace:dir:attribute:authToken'=>array('PO-C7AEAAAAAWOeSV0jI3WHIf5V48nvYFy8U'),
	'urn:esia:personINN'=>array('810700351764'),
	'urn:esia:personMobilePhone'=>array('+7(909)1145970'),
	'urn:esia:assuranceLevel'=>array('AL20'),
	'urn:esia:principalContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalContacts>
<contact>
<contactType>EML</contactType>
<value>vitaly.fsb@mail.ru</value>
<verificationStatus>S</verificationStatus>
</contact>
<contact>
<contactType>MBT</contactType>
<value>+7(909)1145970</value>
<verificationStatus>S</verificationStatus>
</contact>
</principalContacts>')
);
