<?php
return array(
	'urn:esia:orgShortName'=>array('ООО "СОФТ М"'),
	'urn:mace:dir:attribute:userId'=>array('1022329580'),
	'urn:esia:orgType'=>array('L'),
	'urn:esia:globalRole'=>array('E'),
	'urn:esia:principalAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalAddresses>
<address>
<addressType>PERSON_REGISTRATION</addressType>
<contryChar3Code>RUS</contryChar3Code>
<index>617050</index>
<region>Пермский Край</region>
<district>Краснокамский Район</district>
<settlement>Оверята Поселок</settlement>
<street>Строителей Улица</street>
<house>8</house>
<flat>47</flat>
</address>
<address>
<addressType>PERSON_LIVE</addressType>
<contryChar3Code>RUS</contryChar3Code>
<index>617050</index>
<region>Пермский Край</region>
<district>Краснокамский Район</district>
<settlement>Оверята Поселок</settlement>
<street>Строителей Улица</street>
<house>8</house>
<flat>47</flat>
</address>
</principalAddresses>'),
	'urn:esia:gender'=>array('MALE'),
	'urn:esia:orgContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgContacts>
<contact>
<contactType>EML</contactType>
<value>info@softm.tv</value>
<verificationStatus>N</verificationStatus>
</contact>
</orgContacts>'),
	'urn:mace:dir:attribute:middleName'=>array('Васильевич'),
	'urn:esia:orgOGRN'=>array('1125903003093'),
	'urn:mace:dir:attribute:firstName'=>array('Виталий'),
	'urn:esia:personCitizenship'=>array('RUS'),
	'urn:mace:dir:attribute:lastName'=>array('Зубов'),
	'urn:esia:principalDocuments'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalDocuments>
<document>
<documentType>01</documentType>
<verificationStatus>S</verificationStatus>
<number>704162</number>
<series>5705</series>
<issueDate>2005-11-03T00:00:00</issueDate>
<issuedBy>УВД Ленинского района г.Перми</issuedBy>
</document>
<document>
<documentType>05</documentType>
<verificationStatus>N</verificationStatus>
<number>847104</number>
<series>5905</series>
</document>
</principalDocuments>'),
	'urn:esia:personSNILS'=>array('103-060-412 76'),
	'urn:esia:authnMethod'=>array('PWD'),
	'urn:esia:birthDate'=>array('02-10-1985 00:00:00'),
	'urn:esia:orgOid'=>array('1023077001'),
	'urn:esia:personEMail'=>array('vitaly.fsb@mail.ru'),
	'urn:esia:orgINN'=>array('5903100206'),
	'urn:esia:personTrusted'=>array('Y'),
	'urn:mace:dir:attribute:authToken'=>array('PO-C7Ao8-uqJWPcI7VUrXfXRI_XAB9yIYkRC'),
	'urn:esia:orgAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgAddresses>
<address>
<addressType>ORG_LEGAL</addressType>
<contryChar3Code>RUS</contryChar3Code>
<kladrCode>590000010000715</kladrCode>
<russianRegionCode>59</russianRegionCode>
<index>614087</index>
<region>Пермский край</region>
<city>г Пермь</city>
<street>ул Малкова</street>
<house>12</house>
<flat>124</flat>
</address>
</orgAddresses>'),
	'urn:esia:orgName'=>array('ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "СОФТ МЕНЕДЖМЕНТ"'),
	'urn:esia:personINN'=>array('810700351764'),
	'urn:esia:personMobilePhone'=>array('+7(909)1145970'),
	'urn:esia:orgKPP'=>array('590301001'),
	'urn:esia:assuranceLevel'=>array('AL20'),
	'urn:esia:orgLegalForm'=>array('Общества с ограниченной ответственностью'),
	'urn:esia:principalContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalContacts>
<contact>
<contactType>CEM</contactType>
<value>v.zubov@softm.tv</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>MBT</contactType>
<value>+7(909)1145970</value>
<verificationStatus>S</verificationStatus>
</contact>
<contact>
<contactType>EML</contactType>
<value>vitaly.fsb@mail.ru</value>
<verificationStatus>S</verificationStatus>
</contact>
</principalContacts>')
);
