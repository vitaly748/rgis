<?php
return array(
	'urn:esia:orgShortName'=>array('ТСЖ  "ЧЕРНЫШЕВСКОГО - 5"'),
	'urn:mace:dir:attribute:userId'=>array('1012458168'),
	'urn:esia:orgType'=>array('L'),
	'urn:esia:globalRole'=>array('E'),
	'urn:esia:principalAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalAddresses>
<address>
<addressType>PERSON_REGISTRATION</addressType>
<contryChar3Code>RUS</contryChar3Code>
<index>614090</index>
<region>Пермский Край</region>
<city>Пермь Город</city>
<street>Никулина Улица</street>
<house>35</house>
<flat>22</flat>
</address>
<address>
<addressType>PERSON_LIVE</addressType>
<contryChar3Code>RUS</contryChar3Code>
<index>614002</index>
<region>Пермский Край</region>
<city>Пермь Город</city>
<street>Чернышевского Улица</street>
<house>8</house>
<flat>5</flat>
</address>
</principalAddresses>'),
	'urn:esia:gender'=>array('FEMALE'),
	'urn:esia:orgContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgContacts>
<contact>
<contactType>PHN</contactType>
<value>+7(919)4435542</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>EML</contactType>
<value>tszh.chernyshevskogo5@gmail.com</value>
<verificationStatus>N</verificationStatus>
</contact>
</orgContacts>'),
	'urn:mace:dir:attribute:middleName'=>array('Борисовна'),
	'urn:esia:orgOGRN'=>array('1055901709269'),
	'urn:mace:dir:attribute:firstName'=>array('Валерия'),
	'urn:esia:personCitizenship'=>array('RUS'),
	'urn:esia:orgPosition'=>array('Администратор'),
	'urn:mace:dir:attribute:lastName'=>array('Ситникова'),
	'urn:esia:principalDocuments'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalDocuments>
<document>
<documentType>01</documentType>
<verificationStatus>S</verificationStatus>
<number>215848</number>
<series>5714</series>
<issueDate>2014-08-29T00:00:00</issueDate>
<issuedBy>Отделом УФМС России по Пермскому краю в Свердловском районе гор.Перми</issuedBy>
</document>
<document>
<documentType>05</documentType>
<verificationStatus>N</verificationStatus>
<number>041534</number>
<series>5917</series>
<issueDate>2014-04-29T00:00:00</issueDate>
<expiryDate>2024-04-29T00:00:00</expiryDate>
</document>
</principalDocuments>'),
	'urn:esia:personSNILS'=>array('158-360-565 84'),
	'urn:esia:authnMethod'=>array('PWD'),
	'urn:esia:birthDate'=>array('26-01-1992 00:00:00'),
	'urn:esia:orgOid'=>array('1039065496'),
	'urn:esia:personEMail'=>array('Lerap26@mail.ru'),
	'urn:esia:orgINN'=>array('5904129254'),
	'urn:esia:personTrusted'=>array('Y'),
	'urn:mace:dir:attribute:authToken'=>array('PFjiuAo97uGYWPDCsFiA9Bd-sFk6l--JxbOd'),
	'urn:esia:orgAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgAddresses>
<address>
<addressType>ORG_LEGAL</addressType>
<contryChar3Code>RUS</contryChar3Code>
<kladrCode>590000010001336</kladrCode>
<russianRegionCode>59</russianRegionCode>
<index>614002</index>
<region>Пермский край</region>
<city>г Пермь</city>
<street>ул Чернышевского</street>
<house>5</house>
</address>
<address>
<addressType>ORG_POSTAL</addressType>
<contryChar3Code>RUS</contryChar3Code>
<index>614002</index>
<region>Пермский Край</region>
<city>Пермь Город</city>
<street>Чернышевского Улица</street>
<house>5</house>
</address>
</orgAddresses>'),
	'urn:esia:orgName'=>array('ТОВАРИЩЕСТВО СОБСТВЕННИКОВ ЖИЛЬЯ "ЧЕРНЫШЕВСКОГО - 5"'),
	'urn:esia:personINN'=>array('590422651668'),
	'urn:esia:personMobilePhone'=>array('+7(952)3183707'),
	'urn:esia:orgKPP'=>array('590401001'),
	'urn:esia:assuranceLevel'=>array('AL20'),
	'urn:esia:orgLegalForm'=>array('Товарищества собственников жилья'),
	'urn:esia:principalContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalContacts>
<contact>
<contactType>MBT</contactType>
<value>+7(952)3183707</value>
<verificationStatus>S</verificationStatus>
</contact>
<contact>
<contactType>CEM</contactType>
<value>Lerap26@mail.ru</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>EML</contactType>
<value>Lerap26@mail.ru</value>
<verificationStatus>S</verificationStatus>
</contact>
<contact>
<contactType>CPH</contactType>
<value>+7(952)3183707</value>
<verificationStatus>N</verificationStatus>
</contact>
</principalContacts>')
);
