<?php
return array(
	'urn:esia:orgShortName'=>array('ТСЖ "СОВЕТСКАЯ 17"'),
	'urn:mace:dir:attribute:userId'=>array('1022329741'),
	'urn:esia:orgType'=>array('L'),
	'urn:esia:globalRole'=>array('E'),
	'urn:esia:principalAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalAddresses>
<address>
<addressType>PERSON_REGISTRATION</addressType>
<contryChar3Code>RUS</contryChar3Code>
<index>618590</index>
<region>Пермский Край</region>
<city>Красновишерск Город</city>
<district>Красновишерский Район</district>
<street>Советская Улица</street>
<house>17</house>
<flat>44</flat>
</address>
<address>
<addressType>PERSON_LIVE</addressType>
<contryChar3Code>RUS</contryChar3Code>
<index>618590</index>
<region>Пермский Край</region>
<city>Красновишерск Город</city>
<district>Красновишерский Район</district>
<street>Советская Улица</street>
<house>17</house>
<flat>44</flat>
</address>
</principalAddresses>'),
	'urn:esia:gender'=>array('FEMALE'),
	'urn:esia:orgContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgContacts>
<contact>
<contactType>EML</contactType>
<value>tsgsovet17@mail.ru</value>
<verificationStatus>N</verificationStatus>
</contact>
</orgContacts>'),
	'urn:mace:dir:attribute:middleName'=>array('Анатольевна'),
	'urn:esia:orgOGRN'=>array('1075900003563'),
	'urn:mace:dir:attribute:firstName'=>array('Александра'),
	'urn:esia:personCitizenship'=>array('RUS'),
	'urn:esia:orgPosition'=>array('ПРЕДСЕДАТЕЛЬ ПРАВЛЕНИЯ'),
	'urn:mace:dir:attribute:lastName'=>array('Кичигина'),
	'urn:esia:principalDocuments'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalDocuments>
<document>
<documentType>01</documentType>
<verificationStatus>S</verificationStatus>
<number>163179</number>
<series>5704</series>
<issueDate>2003-12-08T00:00:00</issueDate>
<issuedBy>ОВД Красновишерского района Пермской области</issuedBy>
</document>
</principalDocuments>'),
	'urn:esia:personSNILS'=>array('034-897-461 97'),
	'urn:esia:authnMethod'=>array('PWD'),
	'urn:esia:birthDate'=>array('05-11-1958 00:00:00'),
	'urn:esia:orgOid'=>array('1031894649'),
	'urn:esia:orgINN'=>array('5919006390'),
	'urn:esia:personTrusted'=>array('Y'),
	'urn:mace:dir:attribute:authToken'=>array('PO-DjQo9gXZ5WPTvs1iggc1EzlqhgIny-nvi'),
	'urn:esia:orgAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgAddresses>
<address>
<addressType>ORG_LEGAL</addressType>
<contryChar3Code>RUS</contryChar3Code>
<kladrCode>590130010000037</kladrCode>
<russianRegionCode>59</russianRegionCode>
<index>618590</index>
<region>Пермский край</region>
<city>г Красновишерск</city>
<district>Красновишерский р-н</district>
<street>ул Советская</street>
<house>17</house>
</address>
</orgAddresses>'),
	'urn:esia:orgName'=>array('ТОВАРИЩЕСТВО СОБСТВЕННИКОВ ЖИЛЬЯ "СОВЕТСКАЯ 17"'),
	'urn:esia:personINN'=>array('594100801355'),
	'urn:esia:personMobilePhone'=>array('+7(951)9470107'),
	'urn:esia:orgKPP'=>array('591901001'),
	'urn:esia:systemAuthority'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<esia-encoder:Authorities xmlns:esia-encoder="urn:esia:shibboleth:2.0:attribute:encoder">
<esia-encoder:Authority system="SIA">ORG_CHIEF</esia-encoder:Authority>
</esia-encoder:Authorities>'),
	'urn:esia:assuranceLevel'=>array('AL20'),
	'urn:esia:orgLegalForm'=>array('Товарищества собственников жилья'),
	'urn:esia:principalContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalContacts>
<contact>
<contactType>CPH</contactType>
<value>+7(951)9470107</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>CEM</contactType>
<value>tsgsovet17@mail.ru</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>MBT</contactType>
<value>+7(951)9470107</value>
<verificationStatus>S</verificationStatus>
</contact>
</principalContacts>')
);
