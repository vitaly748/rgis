<?php
return array(
	'urn:esia:orgShortName'=>array('Администрация Ольховского сельского поселения'),
	'urn:mace:dir:attribute:userId'=>array('1008715868'),
	'urn:esia:orgType'=>array('A'),
	'urn:esia:globalRole'=>array('E'),
	'urn:esia:gender'=>array('MALE'),
	'urn:esia:orgContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgContacts>
<contact>
<contactType>FAX</contactType>
<value>+7(34241)44694</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>EML</contactType>
<value>osp-chaik@list.ru</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>PHN</contactType>
<value>+7(34241)44694</value>
<verificationStatus>N</verificationStatus>
</contact>
</orgContacts>'),
	'urn:mace:dir:attribute:middleName'=>array('Леонидович'),
	'urn:esia:orgOGRN'=>array('1055906308150'),
	'urn:mace:dir:attribute:firstName'=>array('Михаил'),
	'urn:esia:personCitizenship'=>array('RUS'),
	'urn:esia:orgPosition'=>array('ГЛАВА СЕЛЬСКОГО ПОСЕЛЕНИЯ-ПРЕДСЕДАТЕЛЬ СОВЕТА ДЕПУТАТОВ ОЛЬХОВСКОГО СЕЛЬСКОГО ПОСЕЛЕНИЯ'),
	'urn:mace:dir:attribute:lastName'=>array('Клабуков'),
	'urn:esia:principalDocuments'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalDocuments>
<document>
<documentType>01</documentType>
<verificationStatus>S</verificationStatus>
<number>591514</number>
<series>5701</series>
<issueDate>2001-07-16T00:00:00</issueDate>
<issuedBy>Чайковским ГОВД Пермской обл.</issuedBy>
</document>
</principalDocuments>'),
	'urn:esia:personSNILS'=>array('032-760-200 15'),
	'urn:esia:authnMethod'=>array('PWD'),
	'urn:esia:birthDate'=>array('20-01-1956 00:00:00'),
	'urn:esia:orgOid'=>array('1000322986'),
	'urn:esia:personEMail'=>array('OSP-chaik@list.ru'),
	'urn:esia:orgINN'=>array('5920023304'),
	'urn:esia:personTrusted'=>array('Y'),
	'urn:mace:dir:attribute:authToken'=>array('PB_IXBo7n7eqWOyJ4GvLq2cMlrPxBVRsqluh'),
	'urn:esia:orgAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgAddresses>
<address>
<addressType>ORG_POSTAL</addressType>
<contryChar3Code>RUS</contryChar3Code>
<index>617742</index>
<region>Пермский край</region>
<city>г Чайковский</city>
<settlement>п Прикамский</settlement>
<street>ул Солнечная</street>
<house>1</house>
</address>
<address>
<addressType>ORG_LEGAL</addressType>
<contryChar3Code>RUS</contryChar3Code>
<kladrCode>590000000000000</kladrCode>
<russianRegionCode>59</russianRegionCode>
<index>617766</index>
<region>Пермский край</region>
<city>г Чайковский</city>
<street>ул Солнечная</street>
<house>1</house>
</address>
</orgAddresses>'),
	'urn:esia:orgName'=>array('Администрация Ольховского сельского поселения'),
	'urn:esia:personINN'=>array('592000785000'),
	'urn:esia:orgKPP'=>array('592001001'),
	'urn:esia:systemAuthority'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<esia-encoder:Authorities xmlns:esia-encoder="urn:esia:shibboleth:2.0:attribute:encoder">
<esia-encoder:Authority system="SIA">ORG_CHIEF</esia-encoder:Authority>
</esia-encoder:Authorities>'),
	'urn:esia:assuranceLevel'=>array('AL20'),
	'urn:esia:orgLegalForm'=>array('Муниципальные казенные учреждения'),
	'urn:esia:principalContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalContacts>
<contact>
<contactType>EML</contactType>
<value>OSP-chaik@list.ru</value>
<verificationStatus>S</verificationStatus>
</contact>
</principalContacts>')
);
