<?php
return array(
	'urn:esia:orgShortName'=>array('РАЗГУЛЯЙ  ТСЖ'),
	'urn:mace:dir:attribute:userId'=>array('1014903999'),
	'urn:esia:orgType'=>array('L'),
	'urn:esia:globalRole'=>array('E'),
	'urn:esia:principalAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalAddresses>
<address>
<addressType>PERSON_REGISTRATION</addressType>
<contryChar3Code>RUS</contryChar3Code>
<index>614000</index>
<region>Пермский Край</region>
<city>Пермь Город</city>
<street>Пермская Улица</street>
<house>8</house>
<flat>9</flat>
</address>
<address>
<addressType>PERSON_LIVE</addressType>
<contryChar3Code>RUS</contryChar3Code>
<index>614000</index>
<region>Пермский Край</region>
<city>Пермь Город</city>
<street>Пермская Улица</street>
<house>8</house>
<flat>9</flat>
</address>
</principalAddresses>'),
	'urn:esia:gender'=>array('MALE'),
	'urn:esia:orgContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgContacts>
<contact>
<contactType>PHN</contactType>
<value>+7(342)2181908</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>EML</contactType>
<value>permskaya8@mail.ru</value>
<verificationStatus>N</verificationStatus>
</contact>
</orgContacts>'),
	'urn:mace:dir:attribute:middleName'=>array('Владимирович'),
	'urn:esia:orgOGRN'=>array('1025900536441'),
	'urn:mace:dir:attribute:firstName'=>array('Роман'),
	'urn:esia:personCitizenship'=>array('RUS'),
	'urn:esia:orgPosition'=>array('ПРЕДСЕДАТЕЛЬ ПРАВЛЕНИЯ'),
	'urn:mace:dir:attribute:lastName'=>array('Жуйков'),
	'urn:esia:principalDocuments'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalDocuments>
<document>
<documentType>01</documentType>
<verificationStatus>S</verificationStatus>
<number>614945</number>
<series>5701</series>
<issueDate>2001-08-21T00:00:00</issueDate>
<issuedBy>УВД Мотовилихинского района г. Перми</issuedBy>
</document>
<document>
<documentType>05</documentType>
<verificationStatus>N</verificationStatus>
<number>211206</number>
<series>59РМ</series>
<issueDate>1997-10-13T00:00:00</issueDate>
<expiryDate>2017-10-13T00:00:00</expiryDate>
</document>
<document>
<documentType>06</documentType>
<verificationStatus>N</verificationStatus>
<number>022814329</number>
<expiryDate>2018-01-10T00:00:00</expiryDate>
</document>
</principalDocuments>'),
	'urn:esia:personSNILS'=>array('139-091-506 65'),
	'urn:esia:authnMethod'=>array('PWD'),
	'urn:esia:birthDate'=>array('27-01-1975 00:00:00'),
	'urn:esia:orgOid'=>array('1016767350'),
	'urn:esia:personEMail'=>array('permskaya8@mail.ru'),
	'urn:esia:orgINN'=>array('5902603292'),
	'urn:esia:personTrusted'=>array('Y'),
	'urn:mace:dir:attribute:authToken'=>array('PH40vwo8mqN2WO-QuaWQ6_cboRfoaD6N0MzP'),
	'urn:esia:orgAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgAddresses>
<address>
<addressType>ORG_LEGAL</addressType>
<contryChar3Code>RUS</contryChar3Code>
<kladrCode>590000010000493</kladrCode>
<russianRegionCode>59</russianRegionCode>
<index>614000</index>
<region>Пермский край</region>
<city>г Пермь</city>
<street>ул Пермская</street>
<house>8</house>
</address>
</orgAddresses>'),
	'urn:esia:orgName'=>array('ТОВАРИЩЕСТВО СОБСТВЕННИКОВ ЖИЛЬЯ " РАЗГУЛЯЙ "'),
	'urn:esia:personINN'=>array('590612340685'),
	'urn:esia:personMobilePhone'=>array('+7(982)4819500'),
	'urn:esia:orgKPP'=>array('590201001'),
	'urn:esia:systemAuthority'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<esia-encoder:Authorities xmlns:esia-encoder="urn:esia:shibboleth:2.0:attribute:encoder">
<esia-encoder:Authority system="SIA">ORG_CHIEF</esia-encoder:Authority>
</esia-encoder:Authorities>'),
	'urn:esia:assuranceLevel'=>array('AL20'),
	'urn:esia:orgLegalForm'=>array('Товарищества собственников жилья'),
	'urn:esia:principalContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalContacts>
<contact>
<contactType>CEM</contactType>
<value>permskaya8@mail.ru</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>CPH</contactType>
<value>+7(982)4819500</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>MBT</contactType>
<value>+7(982)4819500</value>
<verificationStatus>S</verificationStatus>
</contact>
<contact>
<contactType>EML</contactType>
<value>permskaya8@mail.ru</value>
<verificationStatus>S</verificationStatus>
</contact>
<contact>
<contactType>PHN</contactType>
<value>+7(342)2101686</value>
<verificationStatus>N</verificationStatus>
</contact>
</principalContacts>')
);
