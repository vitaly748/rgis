<?php
return array(
	'urn:esia:orgShortName'=>array('ФОНД КАПИТАЛЬНОГО РЕМОНТА ПК'),
	'urn:mace:dir:attribute:userId'=>array('242684866'),
	'urn:esia:orgType'=>array('L'),
	'urn:esia:globalRole'=>array('E'),
	'urn:esia:principalAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalAddresses>
<address>
<addressType>PERSON_REGISTRATION</addressType>
<contryChar3Code>RUS</contryChar3Code>
<kladrCode>59000000000</kladrCode>
<russianRegionCode>59</russianRegionCode>
<index>614051</index>
<region>Пермский Край</region>
<city>Пермь Город</city>
<street>Юрша Улица</street>
<house>1</house>
<flat>88</flat>
</address>
<address>
<addressType>PERSON_LIVE</addressType>
<contryChar3Code>RUS</contryChar3Code>
<kladrCode>59000000000</kladrCode>
<russianRegionCode>59</russianRegionCode>
<index>614051</index>
<region>Пермский край</region>
<city>г Пермь</city>
<street>ул Пономарева</street>
<house>75</house>
<flat>1</flat>
</address>
</principalAddresses>'),
	'urn:esia:gender'=>array('MALE'),
	'urn:esia:orgContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgContacts>
<contact>
<contactType>EML</contactType>
<value>info@fond59.ru</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>FAX</contactType>
<value>+7(342)2110020</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>PHN</contactType>
<value>+7(342)2110030*312</value>
<verificationStatus>N</verificationStatus>
</contact>
</orgContacts>'),
	'urn:mace:dir:attribute:middleName'=>array('Юрьевич'),
	'urn:esia:orgOGRN'=>array('1145958003366'),
	'urn:mace:dir:attribute:firstName'=>array('Дмитрий'),
	'urn:esia:personCitizenship'=>array('RUS'),
	'urn:esia:orgPosition'=>array('Заместитель генерального директора,  начальник управления расчетов  и учета взносов'),
	'urn:mace:dir:attribute:lastName'=>array('Стерлядев'),
	'urn:esia:principalDocuments'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalDocuments>
<document>
<documentType>01</documentType>
<verificationStatus>S</verificationStatus>
<number>908043</number>
<series>5705</series>
<issueDate>2006-08-10T00:00:00</issueDate>
<issuedBy>Отделом Внутренних Дел Орджоникидзевского района города Перми</issuedBy>
</document>
<document>
<documentType>11</documentType>
<verificationStatus>N</verificationStatus>
<number>2271817</number>
<series>АЕ</series>
<issueDate>2005-12-01T00:00:00</issueDate>
<issuedBy>Орджоникидзевского р-на г.Перми</issuedBy>
</document>
<document>
<documentType>05</documentType>
<verificationStatus>N</verificationStatus>
<number>464166</number>
<series>5919</series>
<issueDate>2014-10-14T00:00:00</issueDate>
<expiryDate>2024-10-14T00:00:00</expiryDate>
</document>
</principalDocuments>'),
	'urn:esia:personSNILS'=>array('122-016-003 71'),
	'urn:esia:authnMethod'=>array('PWD'),
	'urn:esia:birthDate'=>array('31-07-1986 00:00:00'),
	'urn:esia:orgOid'=>array('1006442913'),
	'urn:esia:personEMail'=>array('dima-ster@ya.ru'),
	'urn:esia:orgINN'=>array('5902990563'),
	'urn:esia:personTrusted'=>array('Y'),
	'urn:mace:dir:attribute:authToken'=>array('DncTwgo7_RmhWOu0h84_8Y6KCXKux8hQT8GV'),
	'urn:esia:orgAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgAddresses>
<address>
<addressType>ORG_POSTAL</addressType>
<contryChar3Code>RUS</contryChar3Code>
<index>614000</index>
<house>66</house>
<corpus>1</corpus>
</address>
<address>
<addressType>ORG_LEGAL</addressType>
<contryChar3Code>RUS</contryChar3Code>
<kladrCode>590000010001487</kladrCode>
<russianRegionCode>59</russianRegionCode>
<index>614990</index>
<region>КРАЙ ПЕРМСКИЙ</region>
<city>Г ПЕРМЬ</city>
<street>УЛ ПОПОВА</street>
<house>Д 11</house>
<flat>ОФИС 605</flat>
</address>
</orgAddresses>'),
	'urn:esia:orgName'=>array('НЕКОММЕРЧЕСКАЯ ОРГАНИЗАЦИЯ "ФОНД КАПИТАЛЬНОГО РЕМОНТА ОБЩЕГО ИМУЩЕСТВА В МНОГОКВАРТИРНЫХ ДОМАХ В ПЕРМСКОМ КРАЕ"'),
	'urn:esia:personINN'=>array('590707065420'),
	'urn:esia:personMobilePhone'=>array('+7(902)4718518'),
	'urn:esia:orgKPP'=>array('590201001'),
	'urn:esia:assuranceLevel'=>array('AL20'),
	'urn:esia:orgLegalForm'=>array('Учреждения'),
	'urn:esia:principalContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalContacts>
<contact>
<contactType>MBT</contactType>
<value>+7(902)4718518</value>
<verificationStatus>S</verificationStatus>
</contact>
<contact>
<contactType>EML</contactType>
<value>dima-ster@ya.ru</value>
<verificationStatus>S</verificationStatus>
</contact>
<contact>
<contactType>CEM</contactType>
<value>dysterlyadev@fond59.ru</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>CPH</contactType>
<value>+7(342)2110031</value>
<verificationStatus>N</verificationStatus>
</contact>
</principalContacts>')
);
