<?php
return array(
	'urn:esia:orgShortName'=>array('АДМИНИСТРАЦИЯ ПАЛЬСКОГО СЕЛЬСКОГО ПОСЕЛЕНИЯ'),
	'urn:mace:dir:attribute:userId'=>array('1008704720'),
	'urn:esia:orgType'=>array('A'),
	'urn:esia:globalRole'=>array('E'),
	'urn:esia:gender'=>array('FEMALE'),
	'urn:esia:orgContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgContacts>
<contact>
<contactType>EML</contactType>
<value>admpal@mail.ru</value>
<verificationStatus>N</verificationStatus>
</contact>
</orgContacts>'),
	'urn:mace:dir:attribute:middleName'=>array('Викторовна'),
	'urn:esia:orgOGRN'=>array('1055907101140'),
	'urn:mace:dir:attribute:firstName'=>array('Надежда'),
	'urn:esia:personCitizenship'=>array('RUS'),
	'urn:esia:orgPosition'=>array('ГЛАВА ПАЛЬСКОГО СЕЛЬСКОГО ПОСЕЛЕНИЯ - ГЛАВА АДМИНИСТРАЦИИ ПАЛЬСКОГО СЕЛЬСКОГО ПОСЕЛЕНИЯ'),
	'urn:mace:dir:attribute:lastName'=>array('Хромина'),
	'urn:esia:principalDocuments'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalDocuments>
<document>
<documentType>01</documentType>
<verificationStatus>S</verificationStatus>
<number>408904</number>
<series>5715</series>
<issueDate>2016-05-17T00:00:00</issueDate>
<issuedBy>Отделением УФМС России по Пермскому краю в Осинском районе</issuedBy>
</document>
</principalDocuments>'),
	'urn:esia:personSNILS'=>array('047-833-897 03'),
	'urn:esia:authnMethod'=>array('PWD'),
	'urn:esia:birthDate'=>array('02-05-1971 00:00:00'),
	'urn:esia:orgOid'=>array('1015673820'),
	'urn:esia:personEMail'=>array('admpal@bk.ru'),
	'urn:esia:orgINN'=>array('5944202630'),
	'urn:esia:personTrusted'=>array('Y'),
	'urn:mace:dir:attribute:authToken'=>array('PB-c0Bo8ifPcWPTGZSZXI_7Y7sIzhW5EmX2d'),
	'urn:esia:orgAddresses'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<orgAddresses>
<address>
<addressType>ORG_LEGAL</addressType>
<contryChar3Code>RUS</contryChar3Code>
<kladrCode>590160000120005</kladrCode>
<russianRegionCode>59</russianRegionCode>
<index>618134</index>
<region>Пермский край</region>
<district>Осинский р-н</district>
<settlement>с Паль</settlement>
<street>ул Центральная</street>
<house>3</house>
</address>
</orgAddresses>'),
	'urn:esia:orgName'=>array('АДМИНИСТРАЦИЯ ПАЛЬСКОГО СЕЛЬСКОГО ПОСЕЛЕНИЯ'),
	'urn:esia:personINN'=>array('594400773099'),
	'urn:esia:orgKPP'=>array('594401001'),
	'urn:esia:systemAuthority'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<esia-encoder:Authorities xmlns:esia-encoder="urn:esia:shibboleth:2.0:attribute:encoder">
<esia-encoder:Authority system="SIA">ORG_CHIEF</esia-encoder:Authority>
</esia-encoder:Authorities>'),
	'urn:esia:assuranceLevel'=>array('AL20'),
	'urn:esia:principalContacts'=>array('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<principalContacts>
<contact>
<contactType>CEM</contactType>
<value>admpal@mail.ru</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>CPH</contactType>
<value>+7(34291)65109</value>
<verificationStatus>N</verificationStatus>
</contact>
<contact>
<contactType>EML</contactType>
<value>admpal@bk.ru</value>
<verificationStatus>S</verificationStatus>
</contact>
</principalContacts>')
);
