<?php
/**
 * Параметры ответов Биллинговых центров
 * Описание структуры возвращаемого массива:
 * 	array(
 * 		'test'=>array(// Название bss-функции
 * 			// Информация о возвращаемом параметре. Сокращённый формат: {Название атрибута}=>{Значение при эмуляции работы Биллинга}.
 * 			// Если первый символ в значении - "!", значит указан символьный идентификатор генератора значения
 *			'date'=>'!date',
 *			'placement'=>array(// Пример с вложенными параметрами
 *				'area'=>'100 кв.метров',
 *				'date_registration'=>'!date',
 *				...
 *			),
 *			'current_date'=>array(// Расширенный формат информации о возвращаемом параметре с указанием параметров генератора
 *				'generator'=>'date',// Cимвольный идентификатор генератора значения. Обязательный параметр
 *				// Значение параметра в формате {Название параметра}=>{Значение параметра}. Необязательный параметр.
 *				// Для каждого параметра любого генератора предусмотрено значение по умолчанию
 *				'current'=>true,
 *				...
 *			),
 *			'operations'=>array(// Пример с генератором вложенных параметров
 *				'generator'=>'list',
 *				'min'=>0,// Минимальное количество элементов
 *				'max'=>100,// Максимальное количество элементов
 *				'data'=>array(// Параметры элементов списка
 *					'type'=>2,
 *					'date'=>'!date',
 *					'sum'=>'!sum',
 *					...
 *				)
 *			),
 *			...
 *		),
 *		...
 * 	)
 */
return array(
	'accrual_house'=>array(
		'credited'=>array(
			Bss1::KEY_GENERATOR=>'number',
			'min'=>5000,
			'max'=>50000,
			'float'=>true
		),
		'paid'=>array(
			Bss1::KEY_GENERATOR=>'number',
			'min'=>5000,
			'max'=>50000,
			'float'=>true
		)
	),
	'accrual_premise'=>array(
		'premises'=>array(
			Bss1::KEY_GENERATOR=>'list',
			'min'=>20,
			'max'=>100,
			'data'=>array(
				'premise'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'min'=>1,
					'max'=>200
				),
				'credited'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'min'=>100,
					'max'=>5000,
					'float'=>true
				),
				'paid'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'min'=>100,
					'max'=>5000,
					'float'=>true
				)
			),
			'unique'=>array('premise')
		)
	),
	'account_info'=>array(
		'type'=>array(
			Bss1::KEY_GENERATOR=>'variant',
			'expression'=>'return get_class($this) == "Bss1" ? 1 : 2;',
			'variants'=>array(
				1=>Bss1::ACCOUNT_TYPE_BSS1,
				2=>Bss1::ACCOUNT_TYPE_BSS2
			)
		),
		'kind'=>array(
			Bss1::KEY_GENERATOR=>'variant',
			'variable'=>'type',
			'variants'=>array(
				Bss1::ACCOUNT_TYPE_BSS1=>0,
				Bss1::ACCOUNT_TYPE_BSS2=>array(
					Bss1::KEY_GENERATOR=>'number',
					'min'=>Bss1::ACCOUNT_KIND_BSS2_BOILER,
					'max'=>Bss1::ACCOUNT_KIND_BSS2_LOCKED
				)
			)
		),
		'balance'=>array(
			Bss1::KEY_GENERATOR=>'number',
			'min'=>-5000,
			'max'=>5000,
			'float'=>true
		),
		'id_house'=>Bss1::PREFIX_GENERATOR.'number',
		'address'=>array(
			Bss1::KEY_GENERATOR=>'phrase',
			'sequence'=>array(
					Bss1::PREFIX_GENERATOR.'postcode',
					', Пермский край, ',
					Bss1::PREFIX_GENERATOR.'locality',
					', ',
					Bss1::PREFIX_GENERATOR.'street',
					', д. ',
					array(
						Bss1::KEY_GENERATOR=>'number',
						'min'=>1,
						'max'=>150
					)
			)
		),
		'municipality'=>'ГО город Пермь',
		'locality'=>Bss1::PREFIX_GENERATOR.'locality',
		'street'=>Bss1::PREFIX_GENERATOR.'street',
		'house'=>array(
			Bss1::KEY_GENERATOR=>'number',
			'min'=>1,
			'max'=>100
		),
		'premise'=>array(
			Bss1::KEY_GENERATOR=>'number',
			'min'=>1,
			'max'=>100
		),
		'area'=>array(
			Bss1::KEY_GENERATOR=>'number',
			'min'=>20,
			'max'=>50
		),
		'share'=>array(
			Bss1::KEY_GENERATOR=>'number',
			'min'=>0.3,
			'max'=>1,
			'float'=>true
		),
		'services'=>array(
			Bss1::KEY_GENERATOR=>'list',
			'min'=>1,
			'max'=>2,
			'data'=>array(
				'name'=>array(
					Bss1::KEY_GENERATOR=>'variant',
					'expression'=>'return get_class($this) == "Bss1" ? 1 : 2;',
					'variants'=>array(
						1=>Bss1::PREFIX_GENERATOR.'service_bss1',
						2=>Bss1::PREFIX_GENERATOR.'service_bss2'
					)
				),
				'costs'=>array(
					Bss1::KEY_GENERATOR=>'list',
					'min'=>1,
					'max'=>4,
					'data'=>array(
						'date'=>Bss1::PREFIX_GENERATOR.'date',
						'cost'=>array(
								Bss1::KEY_GENERATOR=>'number',
								'max'=>200,
								'float'=>true
						)
					)
				)
			)
		),
		'recipient'=>array(
			Bss1::KEY_GENERATOR=>'variant',
			'variable'=>'type',
			'variants'=>array(
				Bss1::ACCOUNT_TYPE_BSS1=>'Министерство строительста и ремонта Пермского края',
				Bss1::ACCOUNT_TYPE_BSS2=>'Фонд капитального ремонта Пермского края'
			)
		),
		'bank'=>Bss1::PREFIX_GENERATOR.'street',
		'inn'=>array(
			Bss1::KEY_GENERATOR=>'number',
			'min'=>12,
			'max'=>12,
			'long'=>true
		),
		'kpp'=>array(
			Bss1::KEY_GENERATOR=>'number',
			'min'=>9,
			'max'=>9,
			'long'=>true
		),
		'bik'=>array(
			Bss1::KEY_GENERATOR=>'number',
			'min'=>15,
			'max'=>15,
			'long'=>true
		),
		'bank_account'=>array(
			Bss1::KEY_GENERATOR=>'number',
			'min'=>20,
			'max'=>20,
			'long'=>true
		),
		'corr_account'=>array(
			Bss1::KEY_GENERATOR=>'number',
			'min'=>20,
			'max'=>20,
			'long'=>true
		)
	),
	'compensation_list'=>array(
		'compensations'=>array(
			Bss1::KEY_GENERATOR=>'list',
			'max'=>3,
			'data'=>array(
				'id'=>'ФИО СНИЛС ЛС',
				'period_date_start'=>Bss1::PREFIX_GENERATOR.'date',
				'period_date_end'=>Bss1::PREFIX_GENERATOR.'date',
				'date_calculation'=>Bss1::PREFIX_GENERATOR.'date',
				'name'=>Bss1::PREFIX_GENERATOR.'service_bss1',
				'family_members'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'min'=>1,
					'max'=>5
				),
				'sum_expenses'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'max'=>5000
				),
				'sum_compensation'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'max'=>5000
				),
				'sum_recalculation'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'max'=>5000
				),
				'sum_compensation_provided'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'max'=>5000
				),
				'state'=>'Принято'
			)
		)
	),
	'compensation_payments'=>array(
		'payments'=>array(
			Bss1::KEY_GENERATOR=>'list',
			'max'=>30,
			'data'=>array(
				'id'=>'ФИО СНИЛС ЛС',
				'type'=>Bss1::PREFIX_GENERATOR.'subsidy_type',
				'date'=>Bss1::PREFIX_GENERATOR.'date',
				'sum'=>array(
						Bss1::KEY_GENERATOR=>'number',
						'max'=>5000
				),
				'state'=>'Принято'
			),
			'sort'=>'date'
		)
	),
	'counter_list'=>array(
		'counters'=>array(
			Bss1::KEY_GENERATOR=>'list',
			'max'=>4,
			'data'=>array(
				'id'=>Bss1::PREFIX_GENERATOR.'number',
				'counter'=>Bss1::PREFIX_GENERATOR.'counter',
				'reading_previous'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'min'=>50,
					'max'=>300,
					'float'=>true
				),
				'reading_current'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'min'=>301,
					'max'=>600,
					'float'=>true
				)
			),
			'unique'=>array('id', 'counter')
		)
	),
	'operation_last'=>array(
		'date'=>array(
			Bss1::KEY_GENERATOR=>'date',
			'min'=>'2016-09-01'
		),
		'sums'=>array(
			Bss1::KEY_GENERATOR=>'list',
			'max'=>3,
			'data'=>array(
				array(
					Bss1::KEY_GENERATOR=>'number',
					'float'=>true
				)
			)
		),
		Bss1::KEY_FIXED=>array(
			'1001'=>array(
				'date'=>'2016-11-01 00:00:00',
				'sums'=>array(
					123.45
				),
				'error'=>array(
					'code'=>0,
					'description'=>'Ok'
				)
			),
			'1002'=>array(
				'date'=>'2016-11-01 00:00:00',
				'sums'=>array(
					123.45
				),
				'error'=>array(
					'code'=>0,
					'description'=>'Ok'
				)
			),
			'1003'=>array(
				'date'=>'2016-11-01 00:00:00',
				'sums'=>array(
					123.45
				),
				'error'=>array(
					'code'=>0,
					'description'=>'Ok'
				)
			)
		)
	),
	'operation_list'=>array(
		'balance_start'=>array(
			Bss1::KEY_GENERATOR=>'number',
			'min'=>-1000,
			'max'=>1000,
			'float'=>true
		),
		'balance_end'=>array(
			Bss1::KEY_GENERATOR=>'number',
			'min'=>-1000,
			'max'=>1000,
			'float'=>true
		),
		'operations'=>array(
			Bss1::KEY_GENERATOR=>'list',
			'min'=>1,
			'max'=>100,
			'data'=>array(
				'date'=>Bss1::PREFIX_GENERATOR.'date',
				'service'=>array(
					Bss1::KEY_GENERATOR=>'variant',
					'expression'=>'return get_class($this) == "Bss1" ? 1 : 2;',
					'variants'=>array(
						1=>Bss1::PREFIX_GENERATOR.'service_bss1',
						2=>Bss1::PREFIX_GENERATOR.'service_bss2'
					)
				),
				'operation'=>array(
					Bss1::KEY_GENERATOR=>'variant',
					'variable'=>'service',
					'variants'=>array(
						'ГВС'=>Bss1::PREFIX_GENERATOR.'operation',
						'ХВС'=>Bss1::PREFIX_GENERATOR.'operation',
						'Электроэнергия'=>Bss1::PREFIX_GENERATOR.'operation',
						'Газ'=>Bss1::PREFIX_GENERATOR.'operation',
						'Охрана'=>Bss1::PREFIX_GENERATOR.'operation',
						'Кап.ремонт'=>Bss1::PREFIX_GENERATOR.'operation',
						'Социальные выплаты'=>'Возврат'
					)
				),
				'parish'=>array(
					Bss1::KEY_GENERATOR=>'variant',
					'variable'=>'operation',
					'variants'=>array(
						'Начисление'=>array(
							Bss1::KEY_GENERATOR=>'number',
							'max'=>200
						),
						'Оплата'=>0,
						'Перерасчёт'=>0,
						'Возврат'=>0
					)
				),
				'spend'=>array(
					Bss1::KEY_GENERATOR=>'variant',
					'variable'=>'operation',
					'variants'=>array(
						'Начисление'=>0,
						'Оплата'=>array(
							Bss1::KEY_GENERATOR=>'number',
							'max'=>200
						),
						'Перерасчёт'=>array(
							Bss1::KEY_GENERATOR=>'number',
							'max'=>200
						),
						'Возврат'=>array(
							Bss1::KEY_GENERATOR=>'number',
							'max'=>200
						)
					)
				)
			),
			'sort'=>'date'
		),
		'services_balance'=>array(
			Bss1::KEY_GENERATOR=>'list',
			'min'=>1,
			'max'=>5,
			'data'=>array(
				'service'=>array(
					Bss1::KEY_GENERATOR=>'variant',
					'expression'=>'return get_class($this) == "Bss1" ? 1 : 2;',
					'variants'=>array(
						1=>Bss1::PREFIX_GENERATOR.'service_bss1',
						2=>Bss1::PREFIX_GENERATOR.'service_bss2'
					)
				),
				'balance_start'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'min'=>-1000,
					'max'=>1000,
					'float'=>true
				),
				'balance_end'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'min'=>-1000,
					'max'=>1000,
					'float'=>true
				)
			)
		)
	),
	'reading_add'=>array(),
	'reading_list'=>array(
		'readings'=>array(
			Bss1::KEY_GENERATOR=>'list',
			'max'=>50,
			'data'=>array(
				'date'=>Bss1::PREFIX_GENERATOR.'date',
				'value'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'min'=>50,
					'max'=>600,
					'float'=>true
				)
			),
			'sort'=>'date'
		)
	),
	'subsidy_list'=>array(
		'subsidies'=>array(
			Bss1::KEY_GENERATOR=>'list',
			'max'=>3,
			'data'=>array(
				'id'=>'ФИО СНИЛС ЛС',
				'period'=>Bss1::PREFIX_GENERATOR.'date',
				'date_calculation'=>Bss1::PREFIX_GENERATOR.'date',
				'family_members'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'min'=>1,
					'max'=>5
				),
				'sum_expenses'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'max'=>5000
				),
				'sum_subsidy'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'max'=>5000
				),
				'sum_recalculation'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'max'=>5000
				),
				'sum_subsidy_provided'=>array(
					Bss1::KEY_GENERATOR=>'number',
					'max'=>5000
				),
				'state'=>'Принято'
			)
		)
	),
	'subsidy_payments'=>array(
		'payments'=>array(
			Bss1::KEY_GENERATOR=>'list',
			'max'=>1,
			'data'=>array(
				'id'=>'ФИО СНИЛС ЛС',
				'type'=>Bss1::PREFIX_GENERATOR.'subsidy_type',
				'date'=>Bss1::PREFIX_GENERATOR.'date',
				'sum'=>array(
						Bss1::KEY_GENERATOR=>'number',
						'max'=>5000
				),
				'state'=>'Принято'
			),
			'sort'=>'date'
		)
	),
	'test'=>array(
		'current_date'=>array(
			Bss1::KEY_GENERATOR=>'date',
			'current'=>true
		)
	),
	'user_search'=>array(
		'id'=>Bss1::PREFIX_GENERATOR.'number',
		'subject_law'=>array(
			Bss1::KEY_GENERATOR=>'number',
			'min'=>Bss1::LAW_TYPE_NATURAL,
			'max'=>Bss1::LAW_TYPE_LEGAL
		),
		'title'=>array(
			Bss1::KEY_GENERATOR=>'variant',
			'variable'=>'subject_law',
			'variants'=>array(
				Bss1::LAW_TYPE_NATURAL=>array(
					Bss1::KEY_GENERATOR=>'phrase',
					'sequence'=>array(
						Bss1::PREFIX_GENERATOR.'surname',
						' ',
						Bss1::PREFIX_GENERATOR.'name',
						' ',
						Bss1::PREFIX_GENERATOR.'patronymic'
					)
				),
				Bss1::LAW_TYPE_LEGAL=>Bss1::PREFIX_GENERATOR.'organization'
			)
		),
		Bss1::KEY_FIXED=>array(
			'1001'.Bss1::FIXED_DELIMITER.Bss1::CONSUMER_TYPE_PAYER=>array(
				'id'=>1993,
				'subject_law'=>Bss1::LAW_TYPE_NATURAL,
				'title'=>'Туленёв Павел Андреевич',
				'error'=>array(
					'code'=>0,
					'description'=>'Ok'
				)
			),
			'1002'.Bss1::FIXED_DELIMITER.Bss1::CONSUMER_TYPE_PAYER=>array(
				'id'=>1994,
				'subject_law'=>Bss1::LAW_TYPE_LEGAL,
				'title'=>'ООО "Метеор"',
				'error'=>array(
					'code'=>0,
					'description'=>'Ok'
				)
			),
			'1003'.Bss1::FIXED_DELIMITER.Bss1::CONSUMER_TYPE_PAYER=>array(
				'id'=>1995,
				'subject_law'=>Bss1::LAW_TYPE_NATURAL,
				'title'=>'Михалев Андрей Валерьевич',
				'error'=>array(
					'code'=>0,
					'description'=>'Ok'
				)
			)
		)
	)
);
