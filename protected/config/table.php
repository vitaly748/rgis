<?php
/**
 * Параметры табличной схемы приложения. Группы и таблицы должны быть описаны в порядке их создания в БД.
 * Описание структуры возвращаемого массива:
 * 
 * 	array(
 * 		// Название группы таблиц. Если в названии присутствует символ "*", группа считается внешней
 * 		'users_group'=>array(
 * 			'users'=>array(// Название таблицы
 * 				'name'=>'TEXT',// Запись данных колонки в формате {Имя колонки}=>{Тип колонки}
 * 				'name2'=>array(,// Запись данных колонки в виде массива {Имя колонки}=>array({Параметры})
 * 					'type'=>'TEXT',// Тип колонки. Обязательный параметр. Парсится для извлечения максимального размера
 * 					'len'=>16,// Максимальный размер. Необязательный параметр. По умолчанию равно null
 * 					'primary'=>true,//Признак первичного ключа. Необязательный параметр. По умолчанию равно false
 * 					'not_null'=>true,//Признак ненулевого значения. Необязательный параметр. По умолчанию равно false
 * 					// Имя внешней таблицы. Применяется в случае, когда колонка является внешним ключом.
 * 					// Необязательный параметр. По умолчанию равно false
 * 					'foreign_table'=>'users',
 * 					// Имя связанного поля внешней таблицы. Примениется, когда задан параметр foreign_table.
 * 					// Необязательный параметр. По умолчанию равно id
 * 					'foreign_name'=>'name',
 * 					// Тип внешней связи для события delete. Необязательный параметр. По умолчанию равно cascade
 * 					'delete'=>'cascade',
 * 					// Тип внешней связи для события update. Необязательный параметр. По умолчанию равно cascade
 * 					'update'=>'cascade',
 * 				),
 * 				...
 * 			),
 * 			...
 * 		),
 * 		...
 * 	)
 */
return array(
	'setting'=>array(
		'setting'=>array(
			'id'=>'pk',
			'section'=>array('type'=>'CHARACTER VARYING(32)', 'not_null'=>true),
			'name'=>array('type'=>'CHARACTER VARYING(32)', 'not_null'=>true),
			'value'=>'CHARACTER VARYING(256)'
		)
	),
	
	'scheme'=>array(
		'scheme_item_static'=>array(
			'id'=>'pk',
			'html'=>array('type'=>'TEXT', 'not_null'=>true)
		),
		'scheme_item'=>array(
			'id'=>'pk',
			'id_static'=>array('type'=>'INTEGER', 'foreign_table'=>'scheme_item_static'),
			'route'=>array('type'=>'CHARACTER VARYING(256)'),
			'title'=>array('type'=>'CHARACTER VARYING(64)', 'not_null'=>true),
			'title_active'=>'CHARACTER VARYING(128)',
			'state'=>array('type'=>'SMALLINT', 'not_null'=>true),
			'enabled'=>'TEXT',
			'visible'=>'TEXT'
		),
		'scheme_item_child'=>array(
			'id_item'=>array('type'=>'INTEGER', 'primary'=>true, 'not_null'=>true, 'foreign_table'=>'scheme_item'),
			'id_parent'=>array('type'=>'INTEGER', 'foreign_table'=>'scheme_item')
		)
	),
	
	'file'=>array(
		'file'=>array(
			'id'=>'pk',
			'type'=>array('type'=>'SMALLINT', 'not_null'=>true),
			'source'=>'INTEGER',
			'category'=>'SMALLINT',
			'name'=>array('type'=>'CHARACTER VARYING(256)', 'not_null'=>true),
			'date'=>array('type'=>'TIMESTAMP WITHOUT TIME ZONE', 'not_null'=>true)
		)
	),
	
	'user'=>array(
		'user'=>array(
			'id'=>'pk',
			'login'=>'CHARACTER VARYING(32)',
			'password'=>'CHARACTER VARYING(64)',
			'state'=>array('type'=>'SMALLINT', 'not_null'=>true),
			'surname'=>'CHARACTER VARYING(32)',
			'name'=>'CHARACTER VARYING(32)',
			'patronymic'=>'CHARACTER VARYING(32)',
			'title'=>'CHARACTER VARYING(128)',
			'address'=>'CHARACTER VARYING(128)',
			'addressid'=>'CHARACTER VARYING(64)',
			'email'=>'CHARACTER VARYING(64)',
			'phone'=>'CHARACTER VARYING(16)',
			'snils'=>'CHARACTER VARYING(16)',
			'registration'=>array('type'=>'TIMESTAMP WITHOUT TIME ZONE', 'not_null'=>true),
			'locked'=>'TIMESTAMP WITHOUT TIME ZONE',
			'delivery'=>array('type'=>'SMALLINT', 'not_null'=>true),
			'agreement'=>'BOOLEAN',
			'secure'=>'BOOLEAN'
		),
		'user_conjugation'=>array(
			'id'=>'pk',
			'id_owner'=>array('type'=>'INTEGER', 'foreign_table'=>'user', 'not_null'=>true),
			'id_external'=>'BIGINT',
			'id_role'=>'INTEGER'
		),
		'user_code'=>array(
			'id'=>'pk',
			'id_owner'=>array('type'=>'INTEGER', 'foreign_table'=>'user', 'not_null'=>true),
			'code'=>'INTEGER',
			'type'=>'SMALLINT',
			'date'=>array('type'=>'TIMESTAMP WITHOUT TIME ZONE', 'not_null'=>true)
		)
	),
	
	'directory'=>array(
		'directory_label'=>array(
			'id'=>'pk',
			'name'=>array('type'=>'CHARACTER VARYING(32)', 'not_null'=>true),
			'description'=>array('type'=>'TEXT', 'not_null'=>true)
		),
		'directory_conformity'=>array(
			'id'=>'pk',
			'section'=>array('type'=>'CHARACTER VARYING(32)', 'not_null'=>true),
			'code'=>array('type'=>'SMALLINT', 'not_null'=>true),
			'name'=>array('type'=>'CHARACTER VARYING(32)', 'not_null'=>true),
			'description'=>array('type'=>'CHARACTER VARYING(128)', 'not_null'=>true)
		),
		'directory_attribute_pattern_group_set'=>array(
			'id'=>'pk',
			'description'=>array('type'=>'CHARACTER VARYING(32)', 'not_null'=>true),
			'set'=>array('type'=>'CHARACTER VARYING(64)', 'not_null'=>true)
		),
		'directory_event'=>array(
			'id'=>'pk',
			'name'=>array('type'=>'CHARACTER VARYING(32)', 'not_null'=>true),
			'description'=>array('type'=>'CHARACTER VARYING(64)', 'not_null'=>true),
			'fix_log'=>array('type'=>'SMALLINT', 'not_null'=>true),
			'fix_flash'=>array('type'=>'SMALLINT', 'not_null'=>true)
		),
		'directory_error_event'=>array(
			'id'=>'pk',
			'name'=>array('type'=>'CHARACTER VARYING(32)', 'not_null'=>true),
			'description'=>array('type'=>'CHARACTER VARYING(256)', 'not_null'=>true),
			'exception'=>'SMALLINT'
		),
		'directory_notification_message'=>array(
			'id'=>'pk',
			'id_event'=>array('type'=>'INTEGER', 'not_null'=>true, 'foreign_table'=>'directory_event'),
			'id_error'=>array('type'=>'INTEGER', 'not_null'=>true, 'foreign_table'=>'directory_error_event'),
			'message'=>array('type'=>'TEXT', 'not_null'=>true),
			'state'=>array('type'=>'SMALLINT', 'not_null'=>true)
		),
		'directory_attribute'=>array(
			'id'=>'pk',
			'id_set'=>array('type'=>'INTEGER', 'foreign_table'=>'directory_conformity'),
			'model'=>array('type'=>'CHARACTER VARYING(32)', 'not_null'=>true),
			'attribute'=>array('type'=>'CHARACTER VARYING(32)', 'not_null'=>true),
			'label'=>array('type'=>'TEXT', 'not_null'=>true),
			'tooltip'=>'TEXT',
			'state'=>array('type'=>'SMALLINT', 'not_null'=>true),
			'service'=>'BOOLEAN'
		),
		'directory_error_attribute'=>array(
			'id'=>'pk',
			'id_attribute'=>array('type'=>'INTEGER', 'foreign_table'=>'directory_attribute'),
			'model'=>'BOOLEAN',
			'attribute'=>'BOOLEAN',
			'validator'=>array('type'=>'CHARACTER VARYING(32)', 'not_null'=>true),
			'message'=>array('type'=>'CHARACTER VARYING(128)', 'not_null'=>true)
		),
		'directory_action_confirm'=>array(
			'id'=>'pk',
			'id_attribute'=>array('type'=>'INTEGER', 'foreign_table'=>'directory_attribute'),
			'action'=>'SMALLINT',
			'type'=>'SMALLINT'
		),
		'directory_attribute_pattern'=>array(
			'id'=>'pk',
			'id_attribute'=>array('type'=>'INTEGER', 'not_null'=>true, 'foreign_table'=>'directory_attribute'),
			'length_min'=>'SMALLINT',
			'length_max'=>'SMALLINT',
			'groups_min'=>'SMALLINT',
			'groups_separation'=>'CHARACTER VARYING(8)',
			'separation_reverse'=>'BOOLEAN',
			'tooltip_inclusion'=>'BOOLEAN'
		),
		'directory_attribute_pattern_group'=>array(
			'id'=>'pk',
			'id_pattern'=>array('type'=>'INTEGER', 'not_null'=>true, 'foreign_table'=>'directory_attribute_pattern'),
			'id_set'=>array('type'=>'INTEGER', 'not_null'=>true,
				'foreign_table'=>'directory_attribute_pattern_group_set'),
			'state'=>array('type'=>'SMALLINT', 'not_null'=>true),
			'pos'=>'SMALLINT',
			'length_min'=>'SMALLINT',
			'length_max'=>'SMALLINT'
		),
		'directory_value'=>array(
			'id'=>'pk',
			'id_parent'=>array('type'=>'INTEGER', 'foreign_table'=>'directory_value'),
			'name'=>array('type'=>'CHARACTER VARYING(32)', 'not_null'=>true),
			'code'=>'CHARACTER VARYING(64)',
			'description'=>array('type'=>'CHARACTER VARYING(256)', 'not_null'=>true),
			'param1'=>'CHARACTER VARYING(32)',
			'param2'=>'CHARACTER VARYING(32)'
		),
		'subsidy'=>array(
			'pers_snils'=>'CHARACTER VARYING(64)',
			'lgot_id'=>'CHARACTER VARYING(64)',
			'lgot_fias'=>'CHARACTER VARYING(64)',
			'lgot_kv'=>'CHARACTER VARYING(64)',
			'lgot_kom'=>'CHARACTER VARYING(64)',
			'lgot_type_reg'=>'CHARACTER VARYING(64)',
			'lgot_type_lgot'=>'CHARACTER VARYING(64)',
			'lgot_from'=>'CHARACTER VARYING(64)',
			'lgot_to'=>'CHARACTER VARYING(64)',
			'lgot_state'=>'CHARACTER VARYING(64)',
			'calc_id'=>'CHARACTER VARYING(64)',
			'calc_state'=>'CHARACTER VARYING(64)',
			'calc_period'=>'CHARACTER VARYING(64)',
			'calc_d'=>'CHARACTER VARYING(64)',
			'calc_family_size'=>'CHARACTER VARYING(64)',
			'calc_fact_ras'=>'CHARACTER VARYING(64)',
			'calc_sum_subs'=>'CHARACTER VARYING(64)',
			'calc_sum_pere'=>'CHARACTER VARYING(64)',
			'calc_sum_final'=>'CHARACTER VARYING(64)',
			'dec_id'=>'CHARACTER VARYING(64)',
			'dec_n'=>'CHARACTER VARYING(64)',
			'dec_d'=>'CHARACTER VARYING(64)',
			'dec_type'=>'CHARACTER VARYING(64)',
			'dec_osn'=>'CHARACTER VARYING(64)',
			'dec_from'=>'CHARACTER VARYING(64)'
		),
		'payment'=>array(
			'lgot_id'=>'CHARACTER VARYING(64)',
			'pay_id'=>'CHARACTER VARYING(64)',
			'pay_type'=>'CHARACTER VARYING(64)',
			'pay_date'=>'CHARACTER VARYING(64)',
			'pay_sum'=>'CHARACTER VARYING(64)',
			'pay_state'=>'CHARACTER VARYING(64)'
		),
		'fias_address'=>array(
			'id'=>'CHARACTER VARYING(64)',
			'address'=>'CHARACTER VARYING(128)',
			'id_bss'=>'INTEGER',
			'id_bars'=>'INTEGER'
		)
	),
	
	'log'=>array(
		'log'=>array(
			'id'=>'pk',
			'id_user'=>'INTEGER',
			'id_event'=>array('type'=>'INTEGER', 'not_null'=>true, 'foreign_table'=>'directory_event'),
			'id_error'=>array('type'=>'INTEGER', 'not_null'=>true, 'foreign_table'=>'directory_error_event'),
			'date'=>array('type'=>'TIMESTAMP WITHOUT TIME ZONE', 'not_null'=>true),
			'params'=>'TEXT'
		)
	),
	
	'message'=>array(
		'chat'=>array(
			'id'=>'pk',
			'id_owner'=>array('type'=>'INTEGER', 'foreign_table'=>'user'),
			'title'=>array('type'=>'CHARACTER VARYING(64)', 'not_null'=>true),
			'state'=>array('type'=>'SMALLINT', 'not_null'=>true)
		),
		'chat_reader'=>array(
			'id'=>'pk',
			'id_chat'=>array('type'=>'INTEGER', 'not_null'=>true, 'foreign_table'=>'chat'),
			'id_user'=>array('type'=>'INTEGER', 'foreign_table'=>'user'),
			'state'=>array('type'=>'SMALLINT', 'not_null'=>true)
		),
		'chat_message'=>array(
			'id'=>'pk',
			'id_chat'=>array('type'=>'INTEGER', 'not_null'=>true, 'foreign_table'=>'chat'),
			'id_owner'=>array('type'=>'INTEGER', 'foreign_table'=>'user'),
			'id_addressee'=>array('type'=>'INTEGER', 'foreign_table'=>'user'),
			'date'=>array('type'=>'TIMESTAMP WITHOUT TIME ZONE', 'not_null'=>true),
			'message'=>'TEXT',
			'state'=>array('type'=>'SMALLINT', 'not_null'=>true)
		),
		'notification'=>array(
			'id'=>'pk',
			'id_addressee'=>array('type'=>'INTEGER', 'not_null'=>true, 'foreign_table'=>'user'),
			'id_message'=>array('type'=>'INTEGER', 'not_null'=>true, 'foreign_table'=>'directory_notification_message'),
			'id_subject'=>'INTEGER',
			'state'=>array('type'=>'SMALLINT', 'not_null'=>true)
		),
		'news'=>array(
			'id'=>'pk',
			'title'=>array('type'=>'CHARACTER VARYING(128)', 'not_null'=>true),
			'date'=>array('type'=>'TIMESTAMP WITHOUT TIME ZONE', 'not_null'=>true),
			'text'=>array('type'=>'TEXT', 'not_null'=>true),
			'state'=>array('type'=>'SMALLINT', 'not_null'=>true)
		)
	),
	
	'auth'=>array(
		'auth_item'=>array(
			'id'=>array('type'=>'SERIAL', 'not_null'=>true),
			'name'=>array('type'=>'CHARACTER VARYING(64)', 'primary'=>true, 'not_null'=>true),
			'type'=>array('type'=>'INTEGER', 'not_null'=>true),
			'description'=>'TEXT',
			'bizrule'=>'TEXT',
			'data'=>'TEXT'
		),
		'auth_item_child'=>array(
			'parent'=>array('type'=>'CHARACTER VARYING(64)', 'primary'=>true, 'not_null'=>true,
				'foreign_table'=>'auth_item', 'foreign_name'=>'name'),
			'child'=>array('type'=>'CHARACTER VARYING(64)', 'primary'=>true, 'not_null'=>true,
				'foreign_table'=>'auth_item', 'foreign_name'=>'name')
		),
		'auth_assignment'=>array(
			'itemname'=>array('type'=>'CHARACTER VARYING(64)', 'primary'=>true, 'not_null'=>true,
				'foreign_table'=>'auth_item', 'foreign_name'=>'name'),
			'userid'=>array('type'=>'INTEGER', 'primary'=>true, 'not_null'=>true, 'foreign_table'=>'user'),
			'bizrule'=>'TEXT',
			'data'=>'TEXT'
		),
		'auth_access_attribute'=>array(
			'id'=>'pk',
			'id_attribute'=>array('type'=>'INTEGER', 'foreign_table'=>'directory_attribute', 'not_null'=>true),
			'id_attribute_conditional'=>array('type'=>'INTEGER', 'foreign_table'=>'directory_attribute'),
			'model'=>'BOOLEAN',
			'operator'=>'INTEGER',
			'owner'=>'INTEGER',
			'value'=>'SMALLINT',
			'value_conditional'=>'SMALLINT',
			'action'=>array('type'=>'SMALLINT', 'not_null'=>true),
			'drop'=>'BOOLEAN'
		)
	),
	'appeal'=>array(
		'appeal'=>array(
			'id'=>'pk',
			'theme'=>array('type'=>'CHARACTER VARYING(128)', 'not_null'=>true),
			'themeid'=>array('type'=>'CHARACTER VARYING(8)', 'not_null'=>true),
			'address'=>array('type'=>'CHARACTER VARYING(128)', 'not_null'=>true),
			'addressid'=>'CHARACTER VARYING(64)',
			'responder'=>array('type'=>'INTEGER', 'not_null'=>true),
			'category'=>'INTEGER',
			'type'=>'INTEGER',
			'timeout'=>array('type'=>'TIMESTAMP WITHOUT TIME ZONE', 'not_null'=>true)
		),
		'appeal_stage'=>array(
			'id'=>'pk',
			'id_appeal'=>array('type'=>'INTEGER', 'foreign_table'=>'appeal', 'not_null'=>true),
			'id_owner'=>array('type'=>'INTEGER', 'foreign_table'=>'user', 'not_null'=>true),
			'text'=>'TEXT',
			'state'=>'SMALLINT',
			'date'=>array('type'=>'TIMESTAMP WITHOUT TIME ZONE', 'not_null'=>true)
		)
	)
);
