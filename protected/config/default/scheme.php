<?php
/**
 * Параметры схемы сайта по умолчанию.
 * Описание структуры возвращаемого массива:
 * 
 * 	array(
 * 		array(// Ассоциативный массив параметров элемента схемы
 * 			// Маршрут страницы в формате "controller/action". Необязательный параметр. По умолчанию равен null.
 * 			// Можно задавать PHP-выражение, предварив строку восклицательным знаком. Для указания маршрута к действию общего
 * 			// назначения следует вместо контроллера указывать звездочку
 * 			'route'=>'appeals/index',
 *	 		'title'=>'Заголовок',// Заголовок раздела. Обязательный параметр
 *			// Расширенный заголовок раздела. Применяется, когда раздел активен. Необязательный параметр. По умолчанию
 *			// равен null
 * 			'title_active'=>'Расширенный заголовок',
 * 			// Символьный идентификатор статуса раздела: "Рабочий", "Заблокированный", "Невидимый". Необязательный
 * 			// параметр. По умолчанию равен 'working'
 * 			'state'=>'working',
 * 			// PHP-выражение, определяющее доступность страницы. Необязательный параметр. По умолчанию равен null. В PHP-выражениях
 * 			// допускается применять специальные псевдонимы: {tw}', '{d}', '{w}', '{pa}', '{st}', '{g}', '{a}', '{c}', '{s}', '{u}'
 * 			'enabled'=>'Yii::app()->controller->deployed',
 * 			// PHP-выражение, определяющее видимость страницы. Необязательный параметр. По умолчанию равен null. Невидимая страница
 * 			// также является и недоступной. По умолчанию, если в данном параметре отсутствует условие по статусу приложения,
 * 			// будет применено условие "Yii::app()->controller->deployed". Данное правило не применимо к ветке элементов,
 * 			// предназначенных для развёртывания приложения
 * 			'visible'=>'(int)Yii::app()->user->payment_type !== (int)Yii::app()->conformity->get("payment_type", "code", "bank")',
 * 			// HTML-код статичной страницы. Необязательный параметр. По умолчанию равен false
 * 			'static'=>'<strong>Привет.</strong>',
 * 			// Массив с информацией о вложенных разделах. Необязательный параметр. По умолчанию равен false
 * 			'childs'=>array({Рекурсия})
 * 		),
 * 		...
 * 	)
 */
return array(
	array(
		'route'=>'main/index',
		'title'=>'Главная',
		'title_active'=>'Главная страница',
		'visible'=>'{d}'
	),
	array(
		'title'=>'Счета',
		'title_active'=>'Мои счета',
		'childs'=>array(
			array(
				'route'=>'account/index',
				'title'=>'Баланс',
				'visible'=>'{s}->get("bss1", "on") || {s}->get("bss2", "on")'
			),
			array(
				'title'=>'Услуги',
				'childs'=>array(
					array(
						'route'=>'service/index',
						'title'=>'Список услуг',
						'visible'=>'{s}->get("bss1", "on") || {s}->get("bss2", "on")'
					),
					array(
						'route'=>'service/history',
						'title'=>'История',
						'title_active'=>'История изменения тарифа',
						'visible'=>'{s}->get("bss1", "on") || {s}->get("bss2", "on")'
					)
				)
			),
			array(
				'route'=>'account/operation',
				'title'=>'Операции',
				'visible'=>'{s}->get("bss1", "on") || {s}->get("bss2", "on")'
			),
			array(
				'title'=>'Оплата',
				'childs'=>array(
					array(
						'title'=>'Выбор способа оплаты',
						'route'=>'account/payment',
						'visible'=>'{s}->get("bss1", "on") || {s}->get("bss2", "on")'
					),
					array(
						'title'=>'Квитанция',
						'title'=>'Просмотр квитанции на оплату',
						'route'=>'account/receipt',
						'visible'=>'{r} && ({s}->get("bss1", "on") || {s}->get("bss2", "on"))'
					)
				)
			),
			array(
				'route'=>'account/connection',
				'title'=>'Подключение счёта',
				'title_active'=>'Подключение лицевого счёта',
				'visible'=>'{s}->get("bss1", "on") || {s}->get("bss2", "on")'
			),
			array(
				'route'=>'account/manage',
				'title'=>'Управление доступом',
				'title_active'=>'Управление доступом к лицевым счетам',
				'state'=>'hidden',
				'visible'=>'{s}->get("bss1", "on") || {s}->get("bss2", "on")'
			),
			array(
				'route'=>'account/disconnection',
				'title'=>'Отключение лицевого счёта',
				'visible'=>'({s}->get("bss1", "on") || {s}->get("bss2", "on")) && {r} && !fp{id}'
			),
			array(
				'title'=>'Социальные выплаты',
				'childs'=>array(
					array(
						'title'=>'Субсидии',
						'childs'=>array(
							array(
								'route'=>'subsidy/index',
								'title'=>'Список субсидий',
								'visible'=>'{s}->get("bss1", "on") || {s}->get("bss2", "on")'
							),
							array(
								'route'=>'subsidy/payment',
								'title'=>'Выплаты по субсидиям',
								'visible'=>'{s}->get("bss1", "on") || {s}->get("bss2", "on")'
							)
						)
					),
					array(
						'title'=>'Компенсации',
						'childs'=>array(
							array(
								'route'=>'compensation/index',
								'title'=>'Список компенсаций',
								'visible'=>'{s}->get("bss1", "on") || {s}->get("bss2", "on")'
							),
							array(
								'route'=>'compensation/payment',
								'title'=>'Выплаты по компенсациям',
								'visible'=>'{s}->get("bss1", "on") || {s}->get("bss2", "on")'
							)
						)
					)
				)
			)
		)
	),
	array(
		'title'=>'Дома',
		'title_active'=>'Мои дома',
		'childs'=>array(
			array(
				'route'=>'house/general',
				'title'=>'Общая информация',
				'title_active'=>'Общая информация по дому'
			),
			array(
				'route'=>'house/premise',
				'title'=>'По помещениям',
				'title_active'=>'Информация по помещениям'
			)
		)
	),
	array(
		'title'=>'Счётчики',
		'title_active'=>'Мои счётчики',
		'childs'=>array(
			array(
				'route'=>'counter/index',
				'title'=>'Показания счётчиков',
				'visible'=>'{s}->get("bss1", "on")'
			),
			array(
				'route'=>'counter/reading',
				'title'=>'Новое показание',
				'visible'=>'{s}->get("bss1", "on") && $_POST'
			),
			array(
				'route'=>'counter/history',
				'title'=>'История',
				'title_active'=>'История ввода показаний',
				'visible'=>'{s}->get("bss1", "on")'
			)
		)
	),
	array(
		'title'=>'Обращения',
		'childs'=>array(
			array(
				'route'=>'appeal/index',
				'title'=>'Список обращений'
			),
			array(
				'route'=>'appeal/create',
				'title'=>'Новое обращение'
			),
			array(
				'route'=>'appeal/view',
				'title'=>'Просмотр обращения',
				'visible'=>'!fp{id} && ({r} || {c}->action->id == "update")'
			),
			array(
				'route'=>'appeal/update',
				'title'=>'Редактирование обращения',
				'visible'=>'!fp{id} && {r}'
			),
			array(
				'route'=>'appeal/revoke',
				'title'=>'Отзыв обращения',
				'visible'=>'{r}'
			),
			array(
				'route'=>'appeal/file',
				'title'=>'Загрузка файла',
				'visible'=>'{r}'
			)
		)
	),
	array(
		'title'=>'Администрирование',
		'childs'=>array(
			array(
				'title'=>'Управление приложением',
				'childs'=>array(
					array(
						'route'=>'setting/general',
						'title'=>'Общие настройки'
					),
					array(
						'route'=>'manage/organization',
						'title'=>'Уполномоченные организации',
						'title_active'=>'Список уполномоченных организаций'
					),
					array(
						'route'=>'manage/role',
						'title'=>'Редактирование ролей пользователей'
					),
					array(
						'title'=>'Журналирование',
						'childs'=>array(
							array(
								'route'=>'log/index',
								'title'=>'Список событий',
								'visible'=>'{s}->get("log_db")'
							),
							array(
								'route'=>'log/view',
								'title'=>'Просмотр данных о событии',
								'visible'=>'!fp{id} && {s}->get("log_db")'
							)
						)
					),
					array(
						'title'=>'Обмен данными с РГИС',
						'childs'=>array(
							array(
								'title'=>'Выгрузка данных',
								'title_active'=>'Выгрузка данных в РГИС',
								'childs'=>array(
									array(
										'route'=>'export/index',
										'title'=>'Список выгрузок'
									),
									array(
										'route'=>'export/export',
										'title'=>'Новый файл выгрузки',
										'visible'=>'{r} && $_POST'
									),
									array(
										'route'=>'export/file',
										'title'=>'Загрузка файла',
										'visible'=>'{r}'
									),
									array(
										'route'=>'export/delete',
										'title'=>'Удаление выгрузки',
										'visible'=>'{r}'
									)
								)
							),
							array(
								'title'=>'Загрузка данных',
								'title_active'=>'Загрузка данных из РГИС',
								'childs'=>array(
									array(
										'route'=>'import/index',
										'title'=>'Список загрузок'
									),
									array(
										'route'=>'import/import',
										'title'=>'Новый файл выгрузки',
										'visible'=>'{r} && $_POST'
									),
									array(
										'route'=>'import/file',
										'title'=>'Загрузка файла',
										'visible'=>'{r}'
									),
									array(
										'route'=>'import/delete',
										'title'=>'Удаление выгрузки',
										'visible'=>'{r}'
									),
									array(
										'route'=>'import/activate',
										'title'=>'Активация выгрузки',
										'visible'=>'{r}'
									)
								)
							)
						)
					),
					array(
						'route'=>'manage/db',
						'title'=>'Проверка целостности БД'
					),
					array(
						'title'=>'Резервное копирование',
						'title_active'=>'Резервное копирование БД',
						'childs'=>array(
							array(
								'route'=>'reserve/index',
								'title'=>'Список копий',
								'title_active'=>'Список резервных копий'
							),
							array(
								'route'=>'reserve/create',
								'title'=>'Создание копии',
								'title_active'=>'Создание резервной копии',
								'visible'=>'{r}'
							),
							array(
								'route'=>'reserve/restore',
								'title'=>'Восстановление копии',
								'title_active'=>'Восстановление резервной копии',
								'visible'=>'{r}'
							),
							array(
								'route'=>'reserve/delete',
								'title'=>'Удаление копии',
								'title_active'=>'Удаление резервной копии',
								'visible'=>'{r}'
							)
						)
					),
					array(
						'title'=>'Развёртывание приложения',
						'childs'=>array(
							array(
								'route'=>'deploy/db',
								'title'=>'Шаг 1 - БД',
								'title_active'=>'Шаг 1 - Настройка подключения к БД',
								'enabled'=>'{st} === "init_db"'
							),
							array(
								'route'=>'deploy/bss1',
								'title'=>'Шаг 2 - Биллинг ЖКХ',
								'title_active'=>'Шаг 2 - Настройка подключения к Биллинговому центру ЖКХ',
								'enabled'=>'{st} === "init_bss1"'
							),
							array(
								'route'=>'deploy/bss2',
								'title'=>'Шаг 3 - Биллинг КР',
								'title_active'=>'Шаг 3 - Настройка подключения к Биллинговому центру Капитального ремонта',
								'enabled'=>'{st} === "init_bss2"'
							),
							array(
								'route'=>'deploy/portal',
								'title'=>'Шаг 4 - Портал',
								'title_active'=>'Шаг 4 - Настройка подключения к Основному порталу',
								'enabled'=>'{st} === "init_portal"'
							),
							array(
								'route'=>'deploy/email',
								'title'=>'Шаг 5 - Почта',
								'title_active'=>'Шаг 5 - Настройка подключения к Почтовому серверу',
								'enabled'=>'{st} === "init_email"'
							),
							array(
								'route'=>'deploy/phone',
								'title'=>'Шаг 6 - SMS',
								'title_active'=>'Шаг 6 - Настройка подключения к SMS-шлюзу',
								'enabled'=>'{st} === "init_phone"'
							),
							array(
								'route'=>'deploy/esia',
								'title'=>'Шаг 7 - ЕСИА',
								'title_active'=>'Шаг 7 - Настройка подключения к ЕСИА',
								'enabled'=>'{st} === "init_esia"'
							),
							array(
								'route'=>'deploy/fias',
								'title'=>'Шаг 8 - ФИАС',
								'title_active'=>'Шаг 8 - Настройка подключения к ФИАС-агенту',
								'enabled'=>'{st} === "init_fias"'
							),
							array(
								'route'=>'deploy/data',
								'title'=>'Шаг 9 - Данные',
								'title_active'=>'Шаг 9 - Генерация данных',
								'enabled'=>'{st} === "init_data"'
							)
						)
					)
				)
			),
			array(
				'title'=>'Управление пользователями',
				'childs'=>array(
					array(
						'route'=>'user/index',
						'title'=>'Список пользователей'
					),
					array(
						'route'=>'user/create',
						'title'=>'Новый пользователь',
						'title_active'=>'Регистрация нового пользователя'
					),
					array(
						'route'=>'user/view',
						'title'=>'Просмотр профиля',
						'title_active'=>'Просмотр профиля пользователя',
						'visible'=>'!fp{id}'
					),
					array(
						'route'=>'user/update',
						'title'=>'Редактирование профиля',
						'title_active'=>'Редактирование профиля пользователя',
						'visible'=>'!fp{id} && ($_GET["id"] != "1" || {u}->id == 1)'
					),
					array(
						'route'=>'user/delete',
						'title'=>'Удаление профиля',
						'title_active'=>'Удаление профиля пользователя',
						'visible'=>'!fp{id} && {r}'
					)
				)
			)
		)
	),
	array(
		'title'=>'Профиль',
		'title_active'=>'Личный профиль',
		'childs'=>array(
			array(
				'title'=>'Личные данные',
				'childs'=>array(
					array(
						'route'=>'user/pview',
						'title'=>'Просмотр',
						'title_active'=>'Просмотр личных данных'
					),
					array(
						'route'=>'user/pupdate',
						'title'=>'Редактирование',
						'title_active'=>'Редактирование личных данных'
					),
					array(
						'route'=>'user/pdelete',
						'title'=>'Удаление личного профиля',
						'visible'=>'{r}'
					)
				)
			),
			array(
				'route'=>'user/setting',
				'title'=>'Настройки',
				'title_active'=>'Мои настройки'
			),
			array(
				'route'=>'user/restore',
				'title'=>'Восстановление доступа',
				'visible'=>'{r} && {s}->get("email", "on")'
			)
		)
	),
	array(
		'route'=>'user/registration',
		'title'=>'Регистрация',
		'visible'=>'{w} && {g}',
		'state'=>'hidden'
	),
	array(
		'title'=>'Информация',
		'childs'=>array(
			array(
				'title'=>'Новости',
				'childs'=>array(
					array(
						'route'=>'news/index',
						'title'=>'Список новостей'
					),
					array(
						'route'=>'news/create',
						'title'=>'Добавление новости'
					),
					array(
						'route'=>'news/view',
						'title'=>'Просмотр новости',
						'visible'=>'!fp{id}'
					),
					array(
						'route'=>'news/update',
						'title'=>'Редактирование новости',
						'visible'=>'!fp{id} && {r}'
					),
					array(
						'route'=>'news/delete',
						'title'=>'Удаление новости',
						'visible'=>'{r}'
					)
				)
			),
			array(
				'route'=>'doc/index',
				'title'=>'Документы',
				/* 'childs'=>array(
					array(
						'route'=>'news2/index',
						'title'=>'Список новостей'
					),
					array(
						'route'=>'news2/create',
						'title'=>'Добавление новости'
					),
					array(
						'route'=>'news2/view',
						'title'=>'Новость',
						'visible'=>'!fp{id}'
					),
					array(
						'route'=>'news2/update',
						'title'=>'Редактирование новости',
						'visible'=>'{r}'
					)
				) */
			),
			array(
				'title'=>'Статьи',
				'childs'=>array(
					array(
						'route'=>'article/answer',
						'title'=>'Вопросы',
						'title_active'=>'Часто задаваемые вопросы'
					)
				)
			)
		)
	),
	array(
		'route'=>'!Yii::app()->portal->get("link")',
		'title'=>'Сайт',
		'visible'=>'{d} && {s}->get("portal", "on") && {u}->checkAccess("portal")'
	),
	array(
		'route'=>'user/logout',
		'title'=>'Выход',
		'visible'=>'{r}'
	),
	array(
		'route'=>'esia/login',
		'title'=>'Авторизация ЕСИА',
		'visible'=>'{r} && {s}->get("esia", "on")'
	),
	array(
		'route'=>'esia/logout',
		'title'=>'Выход ЕСИА',
		'visible'=>'{r} && {s}->get("esia", "on")'
	),
	array(
		'route'=>'*/login',
		'title'=>'Авторизация',
		'visible'=>'{a} && {g}'
	),
	array(
		'route'=>'*/captcha',
		'title'=>'Капча',
		'visible'=>'{r}'
	),
	array(
		'route'=>'*/code',
		'title'=>'Подтверждение',
		'visible'=>'{r}'
	),
	array(
		'route'=>'main/task',
		'title'=>'Выполнение запланированной задачи',
		'visible'=>'{r} && {g}'
	),
	array(
		'route'=>'main/confirm',
		'title'=>'Соглашение',
		'visible'=>'{r} && {g} && ({a} || $_POST)'
	),
	array(
		'route'=>'main/secure',
		'title'=>'Проверка ГОСТ-сертификата',
		'visible'=>'{r} && {g}'
	)
);
