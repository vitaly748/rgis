<?php
/**
 * Параметры ошибок событий по умолчанию.
 * Описание структуры возвращаемого массива:
 * 
 * 	array(
 * 		'error1'=>'Ошибка 1',// Запись данных ошибки в формате {Имя ошибки}=>{Описание ошибки}
 * 		'error2'=>array(// Запись данных ошибки в виде массива {Имя ошибки}=>array({Параметры})
 * 			'description'=>'Не удалось создать файл',// Описание ошибки. Обязательный параметр
 * 			// Код серверной ошибки. Если не ноль, то генерируется исключение. Необязательный параметр. По умолчанию равно 0
 *      'exception'=>404
 *    ),
 * 		...
 * 	)
 */
return array(
	'400'=>'Некорректный запрос',
	'403'=>'Доступ запрещён',
	'404'=>'Страница не найдена',
	'500'=>'Внутренняя ошибка приложения',
	
	'account_connection'=>'Ошибка при подключении лицевого счёта',
	'account_connection_check_account'=>'Ошибка при проверке лицевого счёта',
	'account_connection_check_payment'=>'Ошибка при проверке последнего платежа',
	'account_disconnection'=>array('description'=>'Ошибка при отключении лицевого счёта', 'exception'=>500),
	'account_list'=>array('description'=>'Ошибка при получении информации по лицевым счетам пользователя', 'exception'=>500),
	'account_operation_list'=>array('description'=>'Ошибка при получении информации об операциях по лицевому счёту пользователя',
		'exception'=>500),
	'account_service_list_show'=>array('description'=>'Ошибка при отображении информации по услугам', 'exception'=>500),
	
	'agent_connect'=>array('description'=>'Сбой подключения к агенту', 'exception'=>500),
	'agent_handling'=>array('description'=>'Использование отключенного агента', 'exception'=>500),
	'agent_request'=>array('description'=>$description = 'Ошибка при выполнении запроса к агенту', 'exception'=>500),
	'agent_request_ne'=>$description,
	
	'appeal_create'=>'Ошибка при создании обращения',
	'appeal_file_upload'=>array('description'=>'Ошибка при выгрузке файла обращения пользователю', 'exception'=>500),
	'appeal_list'=>array('description'=>'Ошибка при построении списка обращений', 'exception'=>500),
	'appeal_processing'=>'Ошибка при обработке обращения',
	'appeal_revoke'=>array('description'=>'Ошибка при отзыве обращения', 'exception'=>500),
	'appeal_update'=>'Ошибка при редактировании обращения',
	
	'application'=>array('description'=>'Внутренняя ошибка приложения', 'exception'=>500),
	'application_state'=>array('description'=>'Ошибка при изменении статуса приложения', 'exception'=>500),
	
	'auth_init'=>array('description'=>'Ошибка при инициализации блока авторизации', 'exception'=>500),
	
	'cache_cleaning'=>array('description'=>'Ошибка при очистке кэша', 'exception'=>500),
	'cache_set'=>array('description'=>'Ошибка при записи данных в кэш', 'exception'=>500),
	
	'cfg_param_set'=>array('description'=>'Ошибка при установке параметров приложения', 'exception'=>500),
	
	'code_check'=>array('description'=>'Ошибка при проверке кода подтверждения', 'exception'=>500),
	'code_drop'=>array('description'=>'Ошибка при удалении кода подтверждения', 'exception'=>500),
	'code_generation'=>array('description'=>'Ошибка генерации кода подтверждения', 'exception'=>500),
	'code_hash_check'=>array('description'=>'Ошибка при проверке хеша кода подтверждения', 'exception'=>500),
	
	'compensation_list'=>array('description'=>'Ошибка при получении информации по компенсациям', 'exception'=>500),
	'compensation_payment_list'=>array('description'=>'Ошибка при получении информации о выплатах по компенсации',
		'exception'=>500),
	
	'counter_history_list'=>array('description'=>'Ошибка при получении истории ввода показаний счётчика',
		'exception'=>500),
	'counter_list'=>array('description'=>'Ошибка при получении информации по счётчикам', 'exception'=>500),
	'counter_list_show'=>array('description'=>'Ошибка при отображении информации по счётчикам', 'exception'=>500),
	'counter_reading_add'=>'Ошибка при внесении показания счётчика',
	
	'db_check_tables'=>array('description'=>'Не удалось проверить целостность табличной схемы', 'exception'=>500),
	'db_sql'=>array('description'=>'Ошибка при обращении к Базе данных', 'exception'=>500),
	
	'deploy_data'=>'Ошибка при генерации данных приложения',
	'deploy_group'=>'Ошибка при развёртывании группы таблиц приложения',
	'deploy_group_before'=>'Ошибка при подготовке к развёртыванию группы таблиц приложения',
	'deploy_init'=>'Ошибка при инициализация развёртывания приложения',
	'deploy_settings'=>'Ошибка при развёртывании настроек приложения',
	
	'directory_init'=>array('description'=>'Ошибка при инициализации справочника', 'exception'=>500),
	
	'email_init'=>array('description'=>'Ошибка при инициализации почтового агента', 'exception'=>500),
	'email_send'=>'Не удалось отправить e-mail сообщение',
	
	'esia_agreement'=>'Ошибка при активации профиля',
	'esia_done'=>array('description'=>'Ошибка при отключении пользователя от ЕСИА', 'exception'=>500),
	'esia_email'=>'Авторизация не возможна, не задана электронная почта пользователя ЕСИА. Для добавления электронной почты '.
		'используйте сайт ЕСИА <a href="https://esia.gosuslugi.ru">https://esia.gosuslugi.ru</a>',
	'esia_locked'=>'Профиль пользователя заблокирован',
	'esia_login'=>'Ошибка при авторизации через сервис ЕСИА',
	'esia_logout'=>'Ошибка при выходе через сервис ЕСИА',
	
	'file_access'=>array('description'=>'Доступ к файлу запрещён', 'exception'=>403),
	'file_create'=>array('description'=>'Не удалось создать файл "{file_name}"', 'exception'=>500),
	'file_delete'=>array('description'=>'Ошибка при удалении пользовательского файла с сервера', 'exception'=>500),
	'file_download'=>'Ошибка при загрузке пользовательского файла',
	'file_not_found'=>array('description'=>'Файл не найден', 'exception'=>404),
	'file_save'=>array('description'=>'Ошибка при сохрании пользовательского файла на сервер', 'exception'=>500),
	'file_upload'=>'Ошибка при выгрузке пользовательского файла',
	
	'house_general_list'=>array('description'=>'Ошибка при получении общей информации по домам', 'exception'=>500),
	'house_premise_list'=>array('description'=>'Ошибка при получении информации по дому в разрезе помещений', 'exception'=>500),
	
	'log_list'=>array('description'=>'Ошибка при построении списка событий', 'exception'=>500),
	
	'model_access'=>array('description'=>$description = 'Нет доступа на совершение запрашиваемого действия', 'exception'=>403),
	'model_access_ne'=>$description,
	'model_init_access'=>array('description'=>'Ошибка при расчёте прав доступа модели данных', 'exception'=>500),
	'model_search'=>array('description'=>'Запись не найдена', 'exception'=>404),
	'model_state'=>array('description'=>'Ошибка статуса', 'exception'=>500),
	
	'news_create'=>'Ошибка при добавлении новости',
	'news_delete'=>'Ошибка при удалении новости',
	'news_list'=>array('description'=>'Ошибка при построении списка новостей', 'exception'=>500),
	'news_update'=>'Ошибка при редактировании новости',
	
	'nm_access'=>array('description'=>'nm_access', 'exception'=>403),
	'nm_application'=>array('description'=>'nm_application', 'exception'=>500),
	'nm_not_found'=>array('description'=>'nm_not_found', 'exception'=>404),
	
	'ok'=>'Успех',
	
	'receipt'=>'Ошибка при получении квитанции',
	'receipt_access'=>'Доступ к данной квитанции запрещён',
	'receipt_not_found'=>'Данная квитанция ещё не сформирована',
	
	'setting_general_update'=>'Ошибка при редактировании общих настроек приложения',
	'setting_section_update'=>'Ошибка при редактировании блока настроек приложения',
	
	'scheme_access'=>array('description'=>'Доступ к странице запрещён', 'exception'=>403),
	'scheme_init'=>array('description'=>'Ошибка при инициализации меню', 'exception'=>500),
	'scheme_navigation'=>array('description'=>'Запрошенная страница не найдена', 'exception'=>404),
	
	'subsidy_list'=>array('description'=>'Ошибка при получении информации по субсидиям', 'exception'=>500),
	'subsidy_payment_list'=>array('description'=>'Ошибка при получении информации о выплатах по субсидии', 'exception'=>500),
	
	'task'=>array('description'=>'Ошибка при выполнении запланированной задачи', 'exception'=>500),
	
	'user_assign'=>'Не удаётся назначить роль пользователю',
	'user_revoke'=>'Не удаётся лишить пользователя роли',
	'user_auth'=>'Ошибка проверки авторизации пользователя',
	'user_confirm'=>'Ошибка при обработке подтверждения пользователя',
	'user_create'=>'Ошибка при создании профиля пользователя',
	'user_delete'=>array('description'=>'Ошибка при удалении профиля пользователя', 'exception'=>500),
	'user_list'=>array('description'=>'Ошибка при построении списка пользователей', 'exception'=>500),
	'user_login'=>'Ошибка авторизации',
	'user_logout'=>'Ошибка при выходе пользователя',
	'user_registration'=>'Ошибка регистрации',
	'user_registration_confirm'=>array('description'=>'Неверный код подтверждения', 'exception'=>500),
	'user_restore'=>'Ошибка восстановления доступа',
	'user_restore_check'=>array('description'=>'Неверный код восстановления доступа', 'exception'=>500),
	'user_restore_confirm'=>'Ошибка подтверждения восстановления доступа',
	'user_update'=>'Ошибка при редактировании профиля пользователя',
	'user_pdelete'=>array('description'=>'Ошибка при удалении личного профиля пользователя', 'exception'=>500),
	'user_pupdate'=>'Ошибка при редактировании личного профиля пользователя',
	'user_setting'=>'Ошибка при редактировании личных настроек пользователя',
	'user_setting_empty'=>'Необходимо заполнить логин и пароль пользователя',
	
	'file_exists'=>'Файл уже существует',
	'export_delete'=>array('description'=>'Ошибка при удалении файла выгрузки', 'exception'=>500),
	'export_file_upload'=>array('description'=>'Ошибка при выгрузке файла выгрузки пользователю', 'exception'=>500),
	'export_list'=>array('description'=>'Ошибка при построении списка выгрузок', 'exception'=>500),
	'export_package'=>'Ошибка при формировании файла выгрузки',
	'role_change'=>'Ошибка при редактировании полномочий роли',
	'powers_change'=>'Ошибка при редактировании списка уполномоченных организаций',
	'import_activate'=>array('description'=>'Ошибка при активации\деактивации файла выгрузки РГИС', 'exception'=>500),
	'import_delete'=>array('description'=>'Ошибка при удалении файла выгрузки РГИС', 'exception'=>500),
	'import_file_upload'=>array('description'=>'Ошибка при выгрузке файла выгрузки РГИС пользователю', 'exception'=>500),
	'import_list'=>array('description'=>'Ошибка при построении списка выгрузок РГИС', 'exception'=>500),
	'import_package'=>'Ошибка при загрузке файла выгрузки РГИС',
	'reserve_create'=>'Ошибка при создании файла резервной копии БД',
	'reserve_delete'=>'Ошибка при удалении файла резервной копии БД',
	'reserve_list'=>array('description'=>'Ошибка при построении списка файлов резервных копий БД', 'exception'=>500),
	'reserve_restore'=>'Ошибка при восстановлении резервной копии БД',
	'backup_log'=>'Ошибка при создании резервной копии логов приложения',
	'backup_appeal'=>'Ошибка при создании резервной копии обращений граждан',
	'backup_user'=>'Ошибка при создании резервной копии профилей пользователей',
	'backup_export'=>'Ошибка при экспорте резервных копий приложения',
	'cleaning'=>'Ошибка при чистке данных приложения',
	'service_history_list_show'=>array('description'=>'Ошибка при отображении истории изменения тарифа по услуге',
		'exception'=>500)
);
