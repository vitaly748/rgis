<?php
/**
 * Параметры соответствий по умолчанию.
 * Описание структуры возвращаемого массива:
 * 
 * 	array(
 * 		'section'=>array(// Название блока соответствий
 * 			0=>array(// Цифровой идентификатор соответствия
 * 				// Символьный идентификатор соответствия. Обязательный параметр
 * 				'name'=>'init_db',
 * 				// Описание соответствия. Обязательный параметр
 * 				'description'=>'Настройка подключения к БД',
 * 			),
 * 			...
 * 		),
 * 		...
 * 	)
 */
return array(
	'application_state'=>array(																																				    //			tw	d		w		pa
		10 =>array('name'=>'init_db',     'description'=>'Настройка подключения к БД'),								    	//			+
		20 =>array('name'=>'init_bss1',   'description'=>'Настройка подключения к Биллинговому центру ЖКХ'),//			+
		30 =>array('name'=>'init_bss2',   'description'=>'Настройка подключения к Биллинговому центру КР'), //			+
		40 =>array('name'=>'init_portal', 'description'=>'Настройка подключения к Основному порталу'),	    //			+
		50 =>array('name'=>'init_email',  'description'=>'Настройка подключения к Почтовому серверу'),	    //			+
		60 =>array('name'=>'init_phone',  'description'=>'Настройка подключения к SMS-шлюзу'),				    	//			+
		70 =>array('name'=>'init_esia',   'description'=>'Настройка подключения к ЕСИА'),			       	    	//			+
		80 =>array('name'=>'init_fias',   'description'=>'Настройка подключения к ФИАС-агенту'),				    //			+
		90 =>array('name'=>'init_data',   'description'=>'Генерация данных'),														    //			+
		100=>array('name'=>'locked',      'description'=>'Заблокировано'),														     	//			+		+				+
		110=>array('name'=>'card',        'description'=>'Визитка'),																		    //					+				+
		120=>array('name'=>'working',     'description'=>'Работа')																			    //					+		+
	),
	
	'account_payment_type'=>array(
		10=>array('name'=>'bank', 'description'=>'В банке по квитанции'),
		20=>array('name'=>'card', 'description'=>'По банковской карте'),
		30=>array('name'=>'yd',   'description'=>'Платёжная система "Яндекс Деньги"'),
		40=>array('name'=>'sms',  'description'=>'SMS-платёж')
	),
	'account_type'=>array(
		10=>array('name'=>'bss1', 'description'=>'ЖКХ'),
		20=>array('name'=>'bss2', 'description'=>'Капремонт')
	),
	
	'appeal_category'=>array(
		1=>array('name'=>'sentence',    'description'=>'Предложение'),
		2=>array('name'=>'statement',   'description'=>'Заявление'),
		3=>array('name'=>'complaint',   'description'=>'Жалоба'),
		4=>array('name'=>'claim',       'description'=>'Претензия'),
		5=>array('name'=>'requirement', 'description'=>'Требование'),
		6=>array('name'=>'request',     'description'=>'Запрос'),
		7=>array('name'=>'other',       'description'=>'Иное обращение')
	),
	'appeal_responder'=>array(
		10=>array('name'=>'repair', 'description'=>'Некоммерческая организация "Фонд капитального ремонта общего имущества в '.
			'многоквартирных домах в Пермском крае"'),
		20=>array('name'=>'tsg_gor77', 'description'=>'ТСЖ "Максима Горького 77"')
	),
	'appeal_stage_state'=>array(
		10 =>array('name'=>'created',    'description'=>'Создано'),
		20 =>array('name'=>'revoked',    'description'=>'Отозвано'),
		30 =>array('name'=>'accepted',   'description'=>'Принято'),
		40 =>array('name'=>'redirected', 'description'=>'Перенаправлено'),
		50 =>array('name'=>'returned',   'description'=>'Возвращено'),
		60 =>array('name'=>'requested',  'description'=>'Запрошено'),
		70 =>array('name'=>'answered',   'description'=>'Дан ответ'),
		80 =>array('name'=>'completed',  'description'=>'Исполнено'),
		90 =>array('name'=>'denied',     'description'=>'Отклонено'),
		100=>array('name'=>'extended',   'description'=>'Продлен срок рассмотрения')
	),
	'appeal_state'=>array(
		3=>array('name'=>'registered', 'description'=>'Зарегистрировано'),
		8=>array('name'=>'revoked',    'description'=>'Отозвано'),
		6=>array('name'=>'confirmed',  'description'=>'Подтверждено'),
		4=>array('name'=>'extended',   'description'=>'Продлен срок рассмотрения'),
		5=>array('name'=>'done',       'description'=>'Исполнено'),
		7=>array('name'=>'denied',     'description'=>'Отклонено')
	),
	'appeal_type'=>array(
		1=>array('name'=>'legislation',  'description'=>'Вопрос по законодательству'),
		2=>array('name'=>'system',       'description'=>'Вопрос по работе системы'),
		3=>array('name'=>'customer',     'description'=>'Задачи от заказчика'),
		4=>array('name'=>'integration',  'description'=>'Интеграция'),
		5=>array('name'=>'suggestions',  'description'=>'Предложения по системе'),
		6=>array('name'=>'registration', 'description'=>'Регистрация'),
		7=>array('name'=>'security',     'description'=>'Техническое обеспечение (ошибки)'),
		8=>array('name'=>'tasks',        'description'=>'Типовые задачи (инструкции)')
	),
	
	'attribute_pattern_group_state'=>array(
		10=>array('name'=>'decorative',    'description'=>'Декоративная'),
		20=>array('name'=>'insignificant', 'description'=>'Незначимая'),
		30=>array('name'=>'significant',   'description'=>'Значимая')
	),
	'attribute_state'=>array(
		10=>array('name'=>'disabled', 'description'=>'Отключен'),
		20=>array('name'=>'view',   	'description'=>'Просмотр'),
		30=>array('name'=>'update',   'description'=>'Редактируемый'),
		40=>array('name'=>'required', 'description'=>'Обязательный'),
		50=>array('name'=>'critical', 'description'=>'Критичный')
	),
	
	'cache_type'=>array(
		10=>array('name'=>'disabled', 'description'=>'Отключено'),
		20=>array('name'=>'file', 		'description'=>'Файловая система'),
		30=>array('name'=>'mem',  		'description'=>'Оперативная память')
	),
	
	'chat_message_state'=>array(
		10=>array('name'=>'public',  'description'=>'Публичное'),
		20=>array('name'=>'private', 'description'=>'Скрытое'),
		30=>array('name'=>'remote',  'description'=>'Удалённое')
	),
	'chat_reader_state'=>array(
		10=>array('name'=>'active',  'description'=>'Активный'),
		20=>array('name'=>'private', 'description'=>'Скрытый'),
		30=>array('name'=>'passive', 'description'=>'Пассивный'),
		40=>array('name'=>'locked',  'description'=>'Заблокированный')
	),
	'chat_state'=>array(
		10=>array('name'=>'active', 'description'=>'Активный'),
		20=>array('name'=>'public', 'description'=>'Публичный'),
		30=>array('name'=>'closed', 'description'=>'Закрытый')
	),
	
	'email_protocol'=>array(
		10=>array('name'=>'disabled', 'description'=>'Без шифрования'),
		20=>array('name'=>'ssl',   		'description'=>'SSL'),
		30=>array('name'=>'tls',  		'description'=>'TLS')
	),
	
	'event_fix'=>array(
		10=>array('name'=>'disabled',   'description'=>'Отключена'),
		20=>array('name'=>'by_error',   'description'=>'По ошибке'),
		30=>array('name'=>'by_success', 'description'=>'По успеху'),
		40=>array('name'=>'required',   'description'=>'Обязательная')
	),
	
	'file_category'=>array(
		10=>array('name'=>'agreement',   'description'=>'Соглашение'),
		20=>array('name'=>'instruction', 'description'=>'Инструкция')
	),
	
	'file_type'=>array(
		10=>array('name'=>'scheme',  'description'=>'Схема'),
		20=>array('name'=>'message', 'description'=>'Чат'),
		30=>array('name'=>'appeal',  'description'=>'Обращение'),
		40=>array('name'=>'doc',     'description'=>'Документ'),
		50=>array('name'=>'export',  'description'=>'Выгрузка'),
		60=>array('name'=>'import',  'description'=>'Выгрузка РГИС'),
		70=>array('name'=>'reserve', 'description'=>'Резервная копия БД')
	),
	
	'import_responder'=>array(
		10=>array('name'=>'minsoc', 'description'=>'Министерство социального развития Пермского Края')
	),
	
	'model_action'=>array(
		10 =>array('name'=>'list',                'description'=>'Список'),
		20 =>array('name'=>'create',              'description'=>'Создание'),
		30 =>array('name'=>'delete',              'description'=>'Удаление'),
		40 =>array('name'=>'view',                'description'=>'Просмотр'),
		50 =>array('name'=>'update',              'description'=>'Редактирование'),
		60 =>array('name'=>'restore',             'description'=>'Восстановление'),
		70 =>array('name'=>'deploy',              'description'=>'Развёртывание'),
		80 =>array('name'=>'revoke',              'description'=>'Отзыв'),
		90 =>array('name'=>'history',             'description'=>'Обработка'),
		100=>array('name'=>'accept',              'description'=>'Принятие'),
		110=>array('name'=>'redirect',            'description'=>'Перенаправление'),
		120=>array('name'=>'return',              'description'=>'Возвращение'),
		130=>array('name'=>'request',             'description'=>'Запрос'),
		140=>array('name'=>'answer',              'description'=>'Ответ'),
		150=>array('name'=>'complete',            'description'=>'Завершение'),
		160=>array('name'=>'deny',                'description'=>'Отказ'),
		170=>array('name'=>'rollback',            'description'=>'Откат'),
		180=>array('name'=>'esiadone',            'description'=>'Оключение ЕСИА'),
		190=>array('name'=>'juid_confirm/cancel', 'description'=>'Отмена соглашения'),
		200=>array('name'=>'extend',              'description'=>'Продление срока рассмотрения обращения')
	),
	'model_action_confirm_type'=>array(
		10=>array('name'=>'none',     'description'=>'Без подтверждения'),
		20=>array('name'=>'captcha',  'description'=>'Капча'),
		30=>array('name'=>'query',    'description'=>'Вопрос'),
		40=>array('name'=>'password', 'description'=>'Пароль'),
		50=>array('name'=>'email',    'description'=>'E-mail сообщение'),
		60=>array('name'=>'phone',    'description'=>'SMS сообщение')
	),
	
	'news_state'=>array(
		10=>array('name'=>'preparation', 'description'=>'Подготовка'),
		20=>array('name'=>'publication', 'description'=>'Публикация'),
		30=>array('name'=>'remote',      'description'=>'Удалена')
	),
	
	'notification_message_state'=>array(
		10=>array('name'=>'disabled', 'description'=>'Отключено'),
		20=>array('name'=>'enabled',  'description'=>'Включено'),
		30=>array('name'=>'critical', 'description'=>'Критичное')
	),
	'notification_state'=>array(
		10=>array('name'=>'disabled', 'description'=>'Отключено'),
		20=>array('name'=>'active',   'description'=>'Активно'),
		30=>array('name'=>'critical', 'description'=>'Критично'),
		40=>array('name'=>'remote',   'description'=>'Удалено')
	),
	
	'role_group'=>array(
		10=>array('name'=>'lk',     'description'=>'Личный кабинет'),
		20=>array('name'=>'appeal', 'description'=>'Обращения граждан')
	),
	
	'scheme_item_state'=>array(
		10=>array('name'=>'working', 'description'=>'Рабочий'),
		20=>array('name'=>'locked',  'description'=>'Заблокированный'),
		30=>array('name'=>'hidden',  'description'=>'Скрытый')
	),
	'scheme_stack_type'=>array(
		10=>array('name'=>'disabled', 'description'=>'Отключен'),
		20=>array('name'=>'upper',    'description'=>'Верхний'),
		30=>array('name'=>'lower',    'description'=>'Нижний')
	),
	
	'table_group_install_type'=>array(
		10=>array('name'=>'standart',   'description'=>'Стандартная установка'),
		20=>array('name'=>'generation', 'description'=>'Генерация новых профилей пользователей'),
		30=>array('name'=>'old',        'description'=>'Оставить старые данные')
	),
	
	'user_code_type'=>array(
		10=>array('name'=>'email',   'description'=>'Подтверждение E-mail'),
		20=>array('name'=>'sms',     'description'=>'Подтверждение номера телефона'),
		30=>array('name'=>'restore', 'description'=>'Восстановление доступа')
	),
	'user_conjugation_type'=>array(
		10=>array('name'=>'bss',  'description'=>'Биллинговый центр'),
		20=>array('name'=>'esia', 'description'=>'ЕСИА')
	),
	'user_delivery_type'=>array(
		10=>array('name'=>'disabled', 'description'=>'Отключена'),
		20=>array('name'=>'email',    'description'=>'E-mail'),
		30=>array('name'=>'sms',      'description'=>'SMS')
	),
	'user_generation_type'=>array(
		10=>array('name'=>'base', 'description'=>'Базовый набор профилей'),
		20=>array('name'=>'rand', 'description'=>'Набор случайных профилей')
	),
	'user_registration_type'=>array(
		10=>array('name'=>'simple',         'description'=>'Без подтверждения'),
		20=>array('name'=>'confirm_data',   'description'=>'Подтверждение данных'),
		30=>array('name'=>'confirm_admin',  'description'=>'Подтверждение администратора'),
		40=>array('name'=>'double_confirm', 'description'=>'Двойное подтверждение')
	),
	'user_state'=>array(
		10=>array('name'=>'confirm_data',  'description'=>'Подтверждение данных'),
		20=>array('name'=>'confirm_admin', 'description'=>'Подтверждение администратора'),
		30=>array('name'=>'working',       'description'=>'Рабочий'),
		70=>array('name'=>'locked',        'description'=>'Заблокированный'),
		80=>array('name'=>'remote',        'description'=>'Удалённый')
	)
);
