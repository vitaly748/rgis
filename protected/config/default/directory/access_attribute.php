<?php
/**
 * Параметры доступа к атрибутам моделей по умолчанию.
 * Описание структуры возвращаемого массива:
 * 
 * 	// Первый уровень фильтрации - список моделей данных. Допускается перепрыгивать с одного уровня на другой без соблюдения
 * 	// последовательности их следования, но только по возрастающей
 * 	array(
 * 		// Символьный идентификатор модели данных. Допускается указывать сразу несколько значений, перечислив их через запятую
 * 		'user'=>array(
 * 			// Список доступных действий для модели на текущем уровне. Необязательный параметр. Может присутствовать на каждом
 * 			// уровне. Каждый следующий уровень переопределяет соответствующие значения данного параметра
 * 			//  предыдущих уровней. Доступ к действию "update" предполагает также доступ к действию "view"
 * 			'actions'=>array('view'=>true, 'update'=>false),
 * 			// Сокращённая форма записи. Для отмены некоторого действия, заданного на предыдущих уровнях, нужно
 * 			// использовать префикс "-"
 * 			'actions'=>array('view', '-update'),
 * 			// Сокращённая форма записи в виде строки с разделением значений запятыми
 * 			'actions'=>'view, -update',
 * 			
 * 			// Второй уровень фильтрации - список операторов. Необязательный параметр
 *		 	'operators'=>array(
 *				// Символьный идентификатор роли оператора или идентификатор оператора модели, осуществляющего
 * 				// действие над моделью. Нулевой индекс используется для указания параметров доступа для всех операторов. Ключевое
 * 				// слово AccessAttribute::KEY_OWNER используется для обозначения оператора-владельца модели, ключевое слово
 * 				//  AccessAttribute::KEY_ADDRESSEE используется для обозначения оператора-адресата модели. Допускается указывать сразу
 * 				// несколько значений, перечислив их через запятую. Можно указывать двойные и тройные значения, объединяя значения
 * 				// с помощью символа "&". Например, "addressee&clerk"
 * 				'administrator'=>array(
 * 					'actions'=>array('view', 'update'),// Список доступных действий для модели. Необязательный параметр
 * 					
 * 					// Третий уровень фильтрации - список владельцев. Необязательный параметр
 * 					'owners'=>array(
 * 						// Символьный идентификатор роли владельца или идентификатор владельца экземпляра модели. Нулевой индекс
 * 						// используется для указания параметров доступа для всех владельцев. Ключевое слово AccessAttribute::KEY_NONE
 * 						// используется для обозначения модели без владельца. Допускается указывать сразу несколько значений, перечислив их
 * 						// через запятую
 * 						'developer'=>array(
 * 							'actions'=>array('view', 'update'),// Список доступных действий для модели. Необязательный параметр
 * 							
 * 							// Четвёртый уровень фильтрации - список атрибутов. Необязательный параметр
 * 							'attributes'=>array(
 * 								// Название атрибута модели данных. Нулевой индекс используется для указания параметров доступа
 * 								// непосредственно к модели данных, а не к её атрибуту. Допускается указывать сразу несколько значений,
 * 								// перечислив их через запятую. Также допускается применять параметры сразу для всех атрибутов модели,
 * 								// описанных в файле конфигурации атрибутов, используя символ "*". Если нужно применить параметры для всех
 * 								// атрибутов, кроме нескольких, то можно указать их после "*" через запятую
 * 								'roles'=>array(
 * 									'actions'=>array('view', 'update'),// Список доступных действий. Необязательный параметр
 * 									
 * 									// Пятый уровень фильтрации - список значений атрибутов. Необязательный параметр
 * 									'values'=>array(
 * 										// Значение атрибута модели данных. Нулевой индекс используется для указания параметров доступа
 * 										// в обход значению атрибута. Применяется только для атрибутов, имеющих фиксированный набор значений.
 * 										// При этом указывается цифровой идентификатор значения из файла конфигурации соответствий. Допускается
 * 										// указывать сразу несколько значений, перечислив их через запятую. Также допускается применять параметры
 * 										// сразу для всех фиксированных значений атрибута, используя символ "*". Если нужно применить параметры для
 * 										// всех фиксированных значений атрибута, кроме нескольких, то можно указать их после "*" через запятую
 * 										'working'=>array(
 * 											'actions'=>array('view', 'update'),// Список доступных действий. Необязательный параметр
 * 											
 * 											// Шестой уровень фильтрации - список условных атрибутов. Необязательный параметр
 * 											'attributes_conditional'=>array(
 * 												// Название условного атрибута модели данных. Нулевой индекс используется для указания
 * 												// параметров доступа в обход условного атрибута
 * 												'state'=>array(
 * 													'actions'=>array('view', 'update'),// Список доступных действий. Необязательный параметр
 * 													
 * 													// Седьмой уровень фильтрации - список значений условных атрибутов. Необязательный параметр.
 * 													'values_conditional'=>array(
 * 														// Значение условного атрибута модели данных. Нулевой индекс используется для указания
 * 														// параметров доступа в обход значению условного атрибута. Для атрибутов, имеющих фиксированный 
 * 														// набор значений указывается символьный идентификатор значения из файла конфигурации соответствий.
 * 														// Для атрибутов, не имеющих фиксированный набор значений указывается цифровой эквивалент значения. 
 * 														// Допускается указывать сразу несколько значений, перечислив их через запятую. Также допускается
 * 														// применять параметры сразу для всех фиксированных значений атрибута, используя символ "*". Если
 * 														// нужно применить параметры для всех фиксированных значений атрибута, кроме нескольких, то можно
 * 														// указать их после "*" через запятую
 * 														'working'=>array(
 * 															'actions'=>array('view', 'update')// Список доступных действий. Необязательный параметр
 * 														),
 * 														...
 * 													)
 * 												),
 * 												...
 * 											)
 * 										),
 * 										...
 * 									)
 * 								),
 * 								...
 * 							)
 * 						),
 * 						...
 *					)
 * 				),
 * 				...
 * 			)
 * 		),
 * 		// Сокращённая форма записи - в значении указывается сразу список действий, к которым также применимы сокращённые
 * 		// формы записи, описанные выше. Можно применять на любом уровне
 * 		'user'=>array('view', 'update'),
 * 		...
 * 	)
 */
return array(
	'account, account_connection, account_operation, account_payment, code, compensation, compensation_payment,
		counter, counter_history, house_general, house_premise, payment, service, service_history, subsidy'=>'update',
	
	'appeal'=>array(
		'operators'=>array(
			'citizen, organization'=>array(
				'owners'=>array(
					AccessAttribute::KEY_NONE=>array(
						'actions'=>'create',
						'attributes'=>array(
							'theme, themeid, address, addressid, responder, text, files'=>'update'
						)
					)
				)
			),
			'citizen&'.AccessAttribute::KEY_WITNESS.', organization&'.AccessAttribute::KEY_WITNESS=>array(
				'actions'=>'view',
				'attributes'=>array(
					array(
						'attributes_conditional'=>array(
							'state'=>array(
								'values_conditional'=>array(
									'registered'=>'update, revoke'
								)
							)
						)
					),
					'*, id, theme, themeid, address, addressid, responder, timeout, text, files, answer, answerfiles, state, '.
						'datecreated, datecompleted'=>'-view',
					'id, timeout, answer, answerfiles, state, datecreated, datecompleted'=>'-update'
				)
			),
			'citizen&'.AccessAttribute::KEY_ADDRESSEE.', organization&'.AccessAttribute::KEY_ADDRESSEE=>array(
				'attributes'=>array(
					array(
						'attributes_conditional'=>array(
							'state'=>array(
								'values_conditional'=>array(
									'extended'=>'answer'
								)
							)
						)
					),
					'comment, commentfiles'=>array(
						'attributes_conditional'=>array(
							'state'=>array(
								'values_conditional'=>array(
									'extended'=>'update'
								)
							)
						)
					)
				)
			),
			'specialist&'.AccessAttribute::KEY_WITNESS=>array(
				'owners'=>array(
					'citizen, organization'=>array(
						'actions'=>'view, history',
						'attributes'=>array(
							'*, id, theme, themeid, address, addressid, responder, text, files, answer, answerfiles, state, '.
								'datecreated, datecompleted, category, type, timeout, authorid'=>'-view'
						)
					)
				)
			),
			'specialist&'.AccessAttribute::KEY_ADDRESSEE=>array(
				'owners'=>array(
					'citizen, organization'=>array(
						'attributes'=>array(
							array(
								'attributes_conditional'=>array(
									'state'=>array(
										'values_conditional'=>array(
											'confirmed'=>'redirect, return'
										)
									),
									'clerk'=>array(
										'values_conditional'=>array(
											0=>'-return'
										)
									)
								)
							),
							'answer, answerfiles, datecompleted'=>'-view',
							'comment, commentfiles'=>array(
								'attributes_conditional'=>array(
									'state'=>array(
										'values_conditional'=>array(
											'confirmed'=>'update'
										)
									)
								)
							)
						)
					)
				)
			),
			'developer, administrator'=>array(
				'owners'=>array(
					'citizen, organization'=>array(
						'actions'=>'view, history',
						'attributes'=>array(
							array(
								'attributes_conditional'=>array(
									'state'=>array(
										'values_conditional'=>array(
											'registered'=>'update, revoke',
											'confirmed'=>'extend, rollback',
											'revoked, extended, done, denied'=>'rollback'
										)
									)
								)
							),
							'*, id, theme, themeid, address, addressid, responder, text, files, answer, answerfiles, state, '.
								'datecreated, datecompleted, category, type, timeout, authorid'=>'-view',
							'id, timeout, answer, answerfiles, state, datecreated, datecompleted'=>'-update',
							'category, type'=>array(
								'attributes_conditional'=>array(
									'state'=>array(
										'values_conditional'=>array(
											'registered'=>'-view',
											'*, registered'=>'-update'
										)
									)
								)
							)
						)
					),
					AccessAttribute::KEY_NONE=>array(
						'actions'=>'create',
						'attributes'=>array(
							'theme, themeid, address, addressid, responder, authorid, text, files'=>'update'
						)
					)
				)
			),
			'clerk'=>array(
				'owners'=>array(
					'citizen, organization'=>array(
						'actions'=>'view, history',
						'attributes'=>array(
							array(
								'attributes_conditional'=>array(
									'state'=>array(
										'values_conditional'=>array(
											'registered'=>'accept',
											'confirmed'=>'extend'
										)
									)
								)
							),
							'*, id, theme, themeid, address, addressid, responder, text, files, answer, answerfiles, state, '.
								'datecreated, datecompleted, category, type, timeout, authorid'=>'-view',
							'category, type'=>array(
								'attributes_conditional'=>array(
									'state'=>array(
										'values_conditional'=>array(
											'registered'=>'update'
										)
									)
								)
							)
						)
					),
					AccessAttribute::KEY_NONE=>array(
						'actions'=>'create',
						'attributes'=>array(
							'theme, themeid, address, addressid, responder, authorid, text, files'=>'update'
						)
					)
				)
			),
			'clerk&'.AccessAttribute::KEY_ADDRESSEE=>array(
				'owners'=>array(
					'citizen, organization'=>array(
						'attributes'=>array(
							array(
								'attributes_conditional'=>array(
									'clerk'=>array(
										'values_conditional'=>array(
											1=>'return'
										)
									),
									'state'=>array(
										'values_conditional'=>array(
											'*, confirmed'=>'-return',
											'confirmed'=>'redirect, request, complete, deny'
										)
									)
								)
							),
							'comment, commentfiles'=>array(
								'attributes_conditional'=>array(
									'state'=>array(
										'values_conditional'=>array(
											'confirmed, extended'=>'update'
										)
									)
								)
							)
						)
					)
				)
			)
		)
	),
	
	'bss, data, db, email, esia, fias, phone, portal, reserve, role'=>array(
		'operators'=>array(
			'developer'=>'update'
		)
	),
	
	'confirm'=>array(
		'operators'=>array(
			'guest'=>'update, juid_confirm/cancel'
		)
	),
	
	'export, import, log'=>array(
		'operators'=>array(
			'developer, administrator'=>'update'
		)
	),
	
	'login'=>array(
		'operators'=>array(
			'guest'=>'update'
		)
	),
	
	'news'=>array(
		'operators'=>array(
			array(
				'attributes'=>array(
					array(
						'attributes_conditional'=>array(
							'state'=>array(
								'values_conditional'=>array(
									'publication'=>'view'
								)
							)
						)
					),
					'state'=>'-view'
				)
			),
			'developer, administrator, redactor'=>array(
				'actions'=>'create, update',
				'attributes'=>array(
					array(
						'attributes_conditional'=>array(
							'state'=>array(
								'values_conditional'=>array(
									'*, remote'=>'-create, delete',
									'remote'=>'-create'
								)
							)
						)
					),
					'state'=>array(
						'values'=>array(
							'remote'=>array(
								'actions'=>'-update',
								'attributes_conditional'=>array(
									'state'=>array(
										'values_conditional'=>array(
											'*'=>'update'
										)
									)
								)
							)
						)
					)
				)
			)
		)
	),
	
	'setting_general'=>array(
		'operators'=>array(
			'developer'=>array(
				'actions'=>'update, deploy',
				'attributes'=>array(
					'state'=>array(
						'values'=>array(
							'*, init_data, locked, card, working'=>'-view'
						)
					),
					'registration'=>array(
						'values'=>array(
							'*, simple, confirm_data'=>'-update'
						)
					),
					'cache'=>array(
						'values'=>array(
							'mem'=>'-update'
						)
					)
				)
			),
			'administrator'=>array(
				'actions'=>'update',
				'attributes'=>array(
					'state'=>array(
						'values'=>array(
							'*, locked, card, working'=>'-view'
						)
					),
					'registration, retries, timeout, logdb, logfile, cache, duration1, duration2, duration3, duration4'=>'-view'
				)
			)
		)
	),
	
	'user'=>array(
		'operators'=>array(
			'developer'=>array(
				'owners'=>array(
					array(
						'actions'=>'update',
						'attributes'=>array(
							array(
								'attributes_conditional'=>array(
									'state'=>array(
										'values_conditional'=>array(
											'*, remote'=>'delete'
										)
									)
								)
							),
							'roles'=>array(
								'values'=>array(
									'developer, bss1_payer, bss2_payer, esia, guest'=>'-view'
								)
							),
							'state'=>array(
								'values'=>array(
									'confirm_data'=>array(
										'attributes_conditional'=>array(
											'state'=>array(
												'values_conditional'=>array(
													'*, confirm_data'=>'-update'
												)
											)
										)
									),
									'confirm_admin'=>array(
										'attributes_conditional'=>array(
											'state'=>array(
												'values_conditional'=>array(
													'*, confirm_admin'=>'-update'
												)
											)
										)
									)
								)
							),
							'bsscode, delivery, agreement, restore'=>'-view',
							'registration, delete'=>'-update'
						)
					),
					'administrator'=>array(
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'redactor'=>'-view'
								)
							)
						)
					),
					'bss1_payer'=>array(
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'bss1_payer'=>'view'
								)
							)
						)
					),
					'bss2_payer'=>array(
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'bss2_payer'=>'view'
								)
							)
						)
					),
					'esia'=>array(
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'esia'=>'view'
								)
							)
						)
					),
					AccessAttribute::KEY_NONE=>array(
						'actions'=>'create',
						'attributes'=>array(
							'state'=>array(
								'values'=>array(
									'confirm_data, confirm_admin'=>'-view'
								)
							),
							'bsscode'=>'update',
							'registration, delete'=>'-view'
						)
					)
				)
			),
			'administrator'=>array(
				'owners'=>array(
					array(
						'actions'=>'update',
						'attributes'=>array(
							array(
								'attributes_conditional'=>array(
									'state'=>array(
										'values_conditional'=>array(
											'*, remote'=>'delete'
										)
									)
								)
							),
							'roles'=>array(
								'values'=>array(
									'developer, administrator, bss1_payer, bss2_payer, esia, guest'=>'-view'
								)
							),
							'state'=>array(
								'values'=>array(
									'confirm_data'=>array(
										'attributes_conditional'=>array(
											'state'=>array(
												'values_conditional'=>array(
													'*, confirm_data'=>'-update'
												)
											)
										)
									),
									'confirm_admin'=>array(
										'attributes_conditional'=>array(
											'state'=>array(
												'values_conditional'=>array(
													'*, confirm_admin'=>'-update'
												)
											)
										)
									)
								)
							),
							'bsscode, delivery, agreement, restore'=>'-view',
							'registration, delete'=>'-update'
						)
					),
					'developer'=>array(
						'actions'=>'-update, -delete',
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'developer'=>'view',
									'redactor'=>'-view'
								)
							)
						)
					),
					'administrator'=>array(
						'actions'=>'-update, -delete',
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'administrator'=>'view',
									'redactor'=>'-view'
								)
							)
						)
					),
					'bss1_payer'=>array(
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'bss1_payer'=>'view'
								)
							)
						)
					),
					'bss2_payer'=>array(
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'bss2_payer'=>'view'
								)
							)
						)
					),
					'esia'=>array(
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'esia'=>'view'
								)
							)
						)
					),
					AccessAttribute::KEY_NONE=>array(
						'actions'=>'create',
						'attributes'=>array(
							'state'=>array(
								'values'=>array(
									'confirm_data, confirm_admin'=>'-view'
								)
							),
							'bsscode'=>'update',
							'registration, delete'=>'-view',
						)
					)
				)
			),
			'clerk, specialist'=>array(
				'actions'=>'view',
				'attributes'=>array(
					'roles'=>array(
						'values'=>array(
							'guest'=>'-view'
						)
					),
					'bsscode, delivery, agreement, restore'=>'-view'
				)
			),
			'guest'=>array(
				'owners'=>array(
					AccessAttribute::KEY_NONE=>array(
						'actions'=>'create, restore',
						'attributes'=>array(
							'*, state, registration, locked, delete, delivery'=>'update',
							'roles'=>array(
								'values'=>array(
									'*, citizen, organization'=>'-view'
								)
							)
						)
					)
				)
			),
			AccessAttribute::KEY_OWNER=>array(
				'actions'=>'update, delete',
				'owners'=>array(
					array(
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'*, citizen, organization'=>'-view'
								)
							),
							'bsscode, state, delivery, agreement, restore'=>'-view',
							'registration, locked, delete'=>'-update'
						)
					),
					'developer'=>array(
						'actions'=>'-delete',
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'developer, bss1_payer, bss2_payer, esia'=>'view',
									'clerk, specialist'=>'update'
								)
							),
							'state'=>'view',
						)
					),
					'administrator'=>array(
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'administrator, clerk, specialist'=>'update',
									'bss1_payer, bss2_payer, esia'=>'view'
								)
							)
						)
					),
					'redactor'=>array(
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'redactor'=>'view'
								)
							)
						)
					),
					'clerk'=>array(
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'bss1_payer, bss2_payer, clerk, esia'=>'view'
								)
							)
						)
					),
					'specialist'=>array(
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'bss1_payer, bss2_payer, specialist, esia'=>'view'
								)
							)
						)
					),
					'esia'=>array(
						'attributes'=>array(
							'roles'=>array(
								'values'=>array(
									'citizen, organization'=>'-update'
								)
							), 
							'login, password, password2'=>'-view',
							'surname, name, patronymic, title'=>'-update'
						)
					)
				)
			)
		)
	),
	
	'user_conjugation'=>array(
		'operators'=>array(
			AccessAttribute::KEY_OWNER=>'update'
		)
	),
	
	'user_setting'=>array(
		'operators'=>array(
			'citizen, organization'=>array(
				'attributes'=>array(
					array(
						'actions'=>'update',
						/* 'attributes_conditional'=>array(
							'esia'=>array(
								'values_conditional'=>array(
									1=>'esiadone'
								)
							)
						) */
					),
					'delivery'=>array(
						'values'=>array(
							'sms'=>'-update'
						)
					),
					'esia'=>'-update'
				)
			)
		)
	)
);
