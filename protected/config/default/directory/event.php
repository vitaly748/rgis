<?php
/**
 * Параметры событий по умолчанию.
 * Описание структуры возвращаемого массива:
 * 
 * 	array(
 * 		'init'=>'Инициализация',// Запись данных события в формате {Имя события}=>{Описание события}
 * 		'init2'=>array(// Запись данных события в виде массива {Имя события}=>array({Параметры})
 * 			'description'=>'Инициализация',// Описание события. Обязательный параметр
 * 			// Тип фиксации события в логах: "Отключена", "По ошибке", "По успеху", "Обязательная". Необязательный параметр.
 * 			// По умолчанию равно "by_error" ("По ошибке")
 *      'fix_log'=>'by_error',
 *      // Аналогично параметру "fix_log", только фиксация происходит в флэш-сообщениях. Необязательный параметр.
 * 			// По умолчанию равно "by_error"
 *      'fix_flash'=>'by_error'
 *    ),
 * 		...
 * 	)
 * 
 * fix_log\result	error	success
 * ----------------------------
 * disabled					-			-
 * by_error					+			-
 * by_success				-			+
 * required					+			+
 */
return array(
	'account_connection'=>array('description'=>'Подключение лицевого счёта', 'fix_log'=>'required', 'fix_flash'=>'by_success'),
	'account_connection_check_account'=>array('description'=>'Проверка лицевого счёта', 'fix_log'=>'required',
		'fix_flash'=>'disabled'),
	'account_connection_check_payment'=>array('description'=>'Проверка последнего платежа', 'fix_log'=>'required',
		'fix_flash'=>'disabled'),
	'account_disconnection'=>array('description'=>'Отключение лицевого счёта', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'account_list'=>array('description'=>'Получение информации по лицевым счетам пользователя', 'fix_flash'=>'disabled'),
	'account_operation_list'=>array('description'=>'Получение информации об операциях по лицевому счёту пользователя',
		'fix_flash'=>'disabled'),
	'account_service_list_show'=>array('description'=>'Отображение информации по услугам', 'fix_flash'=>'disabled'),
	
	'appeal_create'=>array('description'=>'Создание обращения', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'appeal_file_upload'=>array('description'=>'Выгрузка файла обращения пользователю', 'fix_log'=>'required',
		'fix_flash'=>'disabled'),
	'appeal_list'=>array('description'=>'Получение списка обращений', 'fix_flash'=>'disabled'),
	'appeal_processing'=>array('description'=>'Обработка обращения', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'appeal_revoke'=>array('description'=>'Отзыв обращения', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'appeal_update'=>array('description'=>'Редактирование обращения', 'fix_log'=>'required', 'fix_flash'=>'required'),
	
	'application_init'=>'Инициализация приложения',
	'application_state'=>array('description'=>'Изменение статуса приложения', 'fix_log'=>'required'),
	
	'auth_init'=>'Инициализация блока авторизации',
	
	'bss_handling'=>'Обращение к Биллинговому центру',
	'bss_request'=>array('description'=>'Запрос к Биллинговому центру', 'fix_log'=>'required'),
	
	'cache_cleaning'=>array('description'=>'Очистка записей кэша', 'fix_flash'=>'disabled'),
	'cache_set'=>array('description'=>'Запись данных в кэш', 'fix_flash'=>'disabled'),
	
	'cfg_web_create'=>array('description'=>'Создание файла конфигурации приложения', 'fix_log'=>'required'),
	'cfg_param_set'=>array('description'=>'Установка параметров приложения', 'fix_log'=>'required'),
	
	'code_check'=>'Проверка кода подтверждения',
	'code_drop'=>'Удаление кода подтверждения',
	'code_generation'=>'Генерация кода подтверждения',
	'code_hash_check'=>'Проверка хеша кода подтверждения',
	
	'compensation_list'=>array('description'=>'Получение информации по компенсациям', 'fix_flash'=>'disabled'),
	'compensation_payment_list'=>array('description'=>'Получение информации о выплатах по компенсации',
		'fix_flash'=>'disabled'),
	
	'counter_history_list'=>array('description'=>'Получение истории ввода показаний счётчика', 'fix_flash'=>'disabled'),
	'counter_list'=>array('description'=>'Получение информации по счётчикам', 'fix_flash'=>'disabled'),
	'counter_list_show'=>array('description'=>'Отображение информации по счётчикам', 'fix_flash'=>'disabled'),
	'counter_reading_add'=>array('description'=>'Внесение показания счётчика', 'fix_log'=>'required', 'fix_flash'=>'required'),
	
	'db_check_tables'=>'Проверка целостности табличной схемы',
	'db_find_all'=>'Выполнение поискового запроса к БД',
	'db_find_bypk'=>'Выполнение поиска в БД записи по первичному ключу',
	'db_fkey_add'=>'Установка внешнего ключа таблицы БД',
	'db_fkey_drop'=>'Сброс внешнего ключа таблицы БД',
	'db_query'=>'Выполнение SQL-запроса к БД',
	'db_row_delete'=>'Удаление записи в БД',
	'db_row_delete_all'=>'Удаление нескольких записей в БД',
	'db_row_save'=>'Сохранение записи в БД',
	'db_row_insert'=>'Вставка записи в БД',
	'db_row_update'=>'Изменение записи в БД',
	'db_row_update_all'=>'Изменение нескольких записей в БД',
	'db_rows_insert'=>'Вставка записей в таблицу БД',
	'db_table_create'=>'Создание таблицы БД',
	'db_table_drop'=>'Удаление таблицы БД',
	'db_table_truncate'=>'Очистка таблицы БД',
	
	'deploy_data'=>array('description'=>'Генерация данных приложения', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'deploy_group'=>array('description'=>'Развёртывание группы таблиц приложения', 'fix_log'=>'required'),
	'deploy_group_before'=>array('description'=>'Подготовка к развёртыванию группы таблиц приложения', 'fix_log'=>'required'),
	'deploy_init'=>array('description'=>'Инициализация развёртывания приложения', 'fix_log'=>'required'),
	'deploy_settings'=>array('description'=>'Развёртывание настроек приложения', 'fix_log'=>'required'),
	
	'directory_init'=>'Инициализация справочника',
	
	'email_handling'=>'Обращение к Почтовому серверу',
	'email_init'=>'Инициализация почтового агента',
	'email_send'=>'Отправка e-mail сообщения',
	
	'esia_agreement'=>array('description'=>'Соглашение пользователя ЕСИА с Пользовательским соглашением', 'fix_log'=>'required'),
	'esia_done'=>array('description'=>'Отключение пользователя от ЕСИА', 'fix_log'=>'required'),
	'esia_handling'=>'Обращение к ЕСИА',
	'esia_login'=>array('description'=>'Авторизация пользователя через сервис ЕСИА', 'fix_log'=>'required'),
	'esia_logout'=>array('description'=>'Выход пользователя через сервис ЕСИА', 'fix_log'=>'required'),
	
	'file_delete'=>array('description'=>'Удаление файла с сервера', 'fix_log'=>'required', 'fix_flash'=>'disabled'),
	'file_download'=>array('description'=>'Загрузка файла на сервер', 'fix_log'=>'required', 'fix_flash'=>'disabled'),
	'file_save'=>array('description'=>'Сохранение файла на сервере', 'fix_log'=>'required', 'fix_flash'=>'disabled'),
	'file_upload'=>array('description'=>'Выгрузка файла пользователю', 'fix_log'=>'required', 'fix_flash'=>'disabled'),
	
	'house_general_list'=>array('description'=>'Получение общей информации по домам', 'fix_flash'=>'disabled'),
	'house_premise_list'=>array('description'=>'Получение информации по дому в разрезе помещений', 'fix_flash'=>'disabled'),
	
	'log_list'=>array('description'=>'Получение списка событий', 'fix_flash'=>'disabled'),
	
	'model_check_access'=>'Проверка прав доступа к модели данных',
	'model_init_access'=>'Расчёт прав доступа к модели данных',
	'model_search'=>'Поиск записи',
	
	'news_create'=>array('description'=>'Добавление новости', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'news_delete'=>array('description'=>'Удаление новости', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'news_list'=>array('description'=>'Получение списка новостей', 'fix_flash'=>'disabled'),
	'news_update'=>array('description'=>'Редактирование новости', 'fix_log'=>'required', 'fix_flash'=>'required'),
	
	'phone_handling'=>'Обращение к SMS-шлюзу',
	'phone_request'=>'Запрос к SMS-шлюзу',
	
	'receipt'=>array('description'=>'Просмотр квитанции на оплату', 'fix_log'=>'required'),
	
	'setting_general_update'=>array('description'=>'Редактирование общих настроек приложения', 'fix_log'=>'required',
		'fix_flash'=>'required'),
	'setting_section_update'=>array('description'=>'Редактирование блока настроек приложения', 'fix_log'=>'required'),
	
	'scheme_init'=>'Инициализация меню',
	'scheme_navigation'=>'Поиск страницы',
	'scheme_check_access'=>'Проверка прав доступа к странице',
	
	'subsidy_list'=>array('description'=>'Получение информации по субсидиям', 'fix_flash'=>'disabled'),
	'subsidy_payment_list'=>array('description'=>'Получение информации о выплатах по субсидии', 'fix_flash'=>'disabled'),
	
	'task'=>array('description'=>'Выполнение запланированной задачи', 'fix_log'=>'required', 'fix_flash'=>'disabled'),
	
	'user_assign'=>array('description'=>'Назначение роли пользователю', 'fix_log'=>'required', 'fix_flash'=>'disabled'),
	'user_auth'=>array('description'=>'Проверка авторизации пользователя', 'fix_log'=>'required', 'fix_flash'=>'disabled'),
	'user_create'=>array('description'=>'Создание пользователя', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'user_confirm'=>array('description'=>'Подтверждение пользователя', 'fix_log'=>'required', 'fix_flash'=>'disabled'),
	'user_delete'=>array('description'=>'Удаление пользователя', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'user_list'=>array('description'=>'Получение списка пользователей', 'fix_flash'=>'disabled'),
	'user_login'=>array('description'=>'Авторизация пользователя', 'fix_log'=>'required', 'fix_flash'=>'disabled'),
	'user_logout'=>array('description'=>'Выход пользователя', 'fix_log'=>'required'),
	'user_registration'=>array('description'=>'Регистрация пользователя', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'user_registration_confirm'=>array('description'=>'Подтверждение регистрации пользователя',
		'fix_log'=>'required', 'fix_flash'=>'required'),
	'user_restore'=>array('description'=>'Восстановление доступа', 'fix_log'=>'required'),
	'user_restore_check'=>array('description'=>'Проверка кода восстановления доступа', 'fix_flash'=>'disabled'),
	'user_restore_confirm'=>array('description'=>'Подтверждение восстановления доступа',
		'fix_log'=>'required', 'fix_flash'=>'required'),
	'user_revoke'=>array('description'=>'Лишение пользователя роли', 'fix_log'=>'required', 'fix_flash'=>'disabled'),
	'user_unlocked'=>array('description'=>'Разблокировка пользователя', 'fix_log'=>'required'),
	'user_update'=>array('description'=>'Редактирование профиля пользователя', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'user_pdelete'=>array('description'=>'Удаление личного профиля пользователя', 'fix_log'=>'required', 'fix_flash'=>'disabled'),
	'user_pupdate'=>array('description'=>'Редактирование личного профиля пользователя', 'fix_log'=>'required',
		'fix_flash'=>'required'),
	'user_setting'=>array('description'=>'Редактирование личных настроек пользователя', 'fix_log'=>'required',
		'fix_flash'=>'required'),
	
	'export_delete'=>array('description'=>'Удаление файла выгрузки', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'export_file_upload'=>array('description'=>'Выгрузка файла выгрузки пользователю', 'fix_log'=>'required',
		'fix_flash'=>'disabled'),
	'export_list'=>array('description'=>'Получение списка выгрузок', 'fix_flash'=>'disabled'),
	'export_package'=>array('description'=>'Формирование файла выгрузки данных в РГИС', 'fix_log'=>'required',
		'fix_flash'=>'required'),
	'role_change'=>array('description'=>'Редактирование полномочий роли', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'powers_change'=>array('description'=>'Редактирование списка уполномоченных организаций', 'fix_log'=>'required',
		'fix_flash'=>'required'),
	'import_activate'=>array('description'=>'Активация/деактивация файла выгрузки РГИС', 'fix_log'=>'required',
		'fix_flash'=>'required'),
	'import_delete'=>array('description'=>'Удаление файла выгрузки РГИС', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'import_file_upload'=>array('description'=>'Выгрузка файла выгрузки РГИС пользователю', 'fix_log'=>'required',
		'fix_flash'=>'disabled'),
	'import_list'=>array('description'=>'Получение списка выгрузок РГИС', 'fix_flash'=>'disabled'),
	'import_package'=>array('description'=>'Загрузка файла выгрузки данных из РГИС', 'fix_log'=>'required',
		'fix_flash'=>'required'),
	'reserve_create'=>array('description'=>'Создание файла резервной копии БД', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'reserve_delete'=>array('description'=>'Удаление файла резервной копии БД', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'reserve_list'=>array('description'=>'Получение списка файлов резервных копий БД', 'fix_flash'=>'disabled'),
	'reserve_restore'=>array('description'=>'Восстановление резервной копии БД', 'fix_log'=>'required', 'fix_flash'=>'required'),
	'backup_log'=>array('description'=>'Создание резервной копии логов приложения', 'fix_log'=>'required',
		'fix_flash'=>'disabled'),
	'backup_appeal'=>array('description'=>'Создание резервной копии обращений граждан', 'fix_log'=>'required',
		'fix_flash'=>'disabled'),
	'backup_user'=>array('description'=>'Создание резервной копии профилей пользователей', 'fix_log'=>'required',
		'fix_flash'=>'disabled'),
	'backup_export'=>array('description'=>'Экспорт резервных копий приложения в сетевую папку', 'fix_log'=>'required',
		'fix_flash'=>'disabled'),
	'cleaning'=>array('description'=>'Чистка данных приложения', 'fix_log'=>'required', 'fix_flash'=>'disabled'),
	'service_history_list_show'=>array('description'=>'Отображение истории изменения тарифа по услуге', 'fix_flash'=>'disabled')
);
