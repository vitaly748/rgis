<?php
/**
 * Параметры шаблонов атрибутов моделей по умолчанию.
 * Описание структуры возвращаемого массива:
 * 
 * 	array(
 * 		'user'=>array(// Название модели данных
 * 			// Название атрибута модели данных. Допускается задавать параметры сразу для нескольких атрибутов,
 * 			// перечислив их через запятую
 * 			'name'=>array(
 * 				'length_min'=>5,// Минимальная длина значения. Необязательный параметр. По умолчанию равно null
 * 				// Максимальная длина значения. Необязательный параметр. По умолчанию равно максимально
 * 				// допустимой длине для атрибута
 * 				'length_max'=>10, 
 * 				// Минимальное количество групп в шаблоне. Необязательный параметр. По умолчанию равно null
 * 				'groups_min'=>4,
 * 				// Количественное группирование символов значения атрибута. Необязательный параметр. По умолчанию равно null
 * 				'groups_separation'=>'243',
 * 				// Признак обратного направления расчёта количественного группирования (справа налево). Необязательный параметр.
 * 				// По умолчанию равно null
 * 				'separation_reverse'=>true,
 * 				// Признак автоматического включения развёрнутой информации о шаблоне в текст подсказки атрибута.
 * 				// Необязательный параметр. По умолчанию равно true
 * 				'tooltip_inclusion'=>true,
 * 				'groups'=>array(// Массив с информацией о группах, входящих в шаблон. Обязательный параметр
 * 					'digits'=>array(// Символьный идентификатор описания группы шаблона атрибута
 * 						// Символьный идентификатор описания группы шаблона атрибута. Необязательный параметр. По умолчанию равно null.
 * 						// Имеет приоритет перед ключевым значением
 * 						'name'=>'n',
 * 						// Символьный идентификатор статуса группы шаблона. Необязательный параметр.
 * 						// По умолчанию равно "significant" ("Значимая")
 * 						'state'=>'significant',
 * 						// Порядковый номер группы шаблона атрибута. Необязательный параметр. По умолчанию равно null
 * 						'pos'=>1,
 * 						// Минимальное количество символов в группе. Необязательный параметр. По умолчанию равно null
 * 						'length_min'=>2
 * 						// Максимальное количество символов в группе. Необязательный параметр. По умолчанию равно null
 * 						'length_max'=>2
 * 					)
 * 				),
 * 				// Сокращённая форма записи массива с информацией о группах, входящих в шаблон. Допускается также
 * 				// задавать в строке через запятую
 * 				'groups'=>array('digits', 'spaces'),
 * 			),
 * 			// Сокращённая форма записи шаблона атрибута модели. Допускается также задавать в строке через запятую
 * 			'name'=>array('digits', 'spaces'),
 * 			...
 * 		),
 * 		...
 * 	)
 * 
 * Сумма:
 * 	array(
 * 		'groups_separation'=>'3',
 * 		'separation_reverse'=>true,
 * 		'groups'=>array(
 * 			'n',
 * 			'sb'=>array('state'=>'decorative'),
 * 			'c'=>array('pos'=>1, 'length_max'=>1),
 * 			'd'=>array('pos'=>1, 'length_max'=>1),
 * 			'n'=>array('pos'=>2, 'length_max'=>2)
 * 		)
 * 	)
 */
return array(
	'account_connection'=>array(
		'account'=>'n',
		'payer'=>'',
		'date'=>array('length_max'=>10, 'groups'=>'n, s_date'),
		'sum'=>array(
			'groups_separation'=>'3',
			'separation_reverse'=>true,
			'groups'=>array(
				'n'=>array('length_max'=>11),
				'sb'=>array('state'=>'decorative', 'length_max'=>1),
				'c'=>array('pos'=>1, 'length_max'=>1),
				'd'=>array('pos'=>1, 'length_max'=>1),
				array('name'=>'n', 'pos'=>2, 'length_max'=>2)
			)
		)
	),
	'account_payment'=>array(
		'sum'=>array(
			'groups_separation'=>'3',
			'separation_reverse'=>true,
			'groups'=>array(
				'n'=>array('length_max'=>11),
				'sb'=>array('state'=>'decorative', 'length_max'=>1),
				'c'=>array('pos'=>1, 'length_max'=>1),
				'd'=>array('pos'=>1, 'length_max'=>1),
				array('name'=>'n', 'pos'=>2, 'length_max'=>2)
			)
		)
	),
	'appeal'=>array(
		'text, comment'=>array('length_max'=>4000, 'groups'=>'Aa, Rr, n, s, ssb'),
	),
	'bss'=>array(
		'host'=>'Aa, n, s_host',
		'port'=>array('length_max'=>4, 'groups'=>'n'),
		'name, asmx'=>'Aa, n, _'
	),
	'code'=>array(
		'captchacode'=>'Aa',
		'emailcode, phonecode'=>'n'
	),
	'counter'=>array(
		'new'=>array('groups'=>array(
			'n'=>array('length_max'=>5),
			'c'=>array('pos'=>1, 'length_max'=>1),
			'd'=>array('pos'=>1, 'length_max'=>1),
			array('name'=>'n', 'pos'=>2, 'length_max'=>3)
		))
	),
	'data'=>array(
		'quantity'=>array('groups_separation'=>'3', 'separation_reverse'=>true,
			'groups'=>array('n', 'sb'=>array('state'=>'decorative')))
	),
	'db'=>array(
		'host'=>'Aa, n, s_host',
		'port'=>array('length_max'=>4, 'groups'=>'n'),
		'name, login, password'=>''
	),
	'email'=>array(
		'host'=>'Aa, n, s_host',
		'port'=>array('length_max'=>4, 'groups'=>'n'),
		'protocol'=>'Aa, Rr, sb',
		'login, password'=>''
	),
	'file'=>array(
		'category'=>'Rr, sb',
		'name'=>''
	),
	'log'=>array(
		'id_user, id_event, id_error'=>'Aa, Rr, n, sb, _, -'
	),
	'login'=>array(
		'login'=>array('Aa, n', 'Aa, n, _, -, m, d'=>array('pos'=>1)),
		'password'=>'A, a, R, r, n, s',
	),
	'news'=>array(
		'title'=>'',
		'date'=>array('length_max'=>10, 'groups'=>'n, s_date'),
		'text'=>array('length_max'=>4000, 'groups'=>'Aa, Rr, n, s, ssb'),
		'state'=>'Rr, sb'
	),
	'phone'=>array(
		'host'=>'Aa, n, s_host',
		'port'=>array('length_max'=>4, 'groups'=>'n'),
		'name, login, password'=>''
	),
	'portal'=>array(
		'host'=>'Aa, n, s_host',
		'port'=>array('length_max'=>4, 'groups'=>'n'),
	),
	'setting_general'=>array(
		'state, registration, cache'=>'Rr, sb',
		'name'=>'Aa, Rr, n, sb, -, q',
		'retries, timeout, duration1, duration2, duration3, duration4'=>array('groups_separation'=>'3', 'separation_reverse'=>true,
			'groups'=>array('n', 'sb'=>array('state'=>'decorative')))
	),
	'user'=>array(
		'roles'=>'Aa, Rr, n, sb, _, -',
		'bsscode'=>'Aa, n',
		'state'=>'Rr, sb',
		'locked'=>array('length_max'=>16, 'groups'=>'n, s_date'),
		'login'=>array('length_min'=>4, 'length_max'=>32, 'groups'=>array('Aa, n', 'Aa, n, _, -, m, d'=>array('pos'=>1))),
		'password'=>array('length_min'=>5, 'length_max'=>32, 'groups_min'=>2, 'groups'=>'A, a, R, r, n, s'),
		'surname, name, patronymic'=>'Rr, -',
		'title'=>'Aa, Rr, n, sb, -, q',
		'address'=>'Rr, n, s_address',
		'email'=>'Aa, n, -, d, m, _',
		'phone'=>array('n', 's_phone'=>array('state'=>'decorative')),
		'snils'=>array('n', 's_phone'=>array('state'=>'decorative')),
		'restore'=>'Aa, n, _, -, m, d'
	)
);
