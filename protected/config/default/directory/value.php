<?php
/**
 * Параметры справочника значений по умолчанию.
 * Описание структуры возвращаемого массива:
 * 
 * 	array(
 * 		'theme'=>array(// Название группы значений
 * 			'1'=>'Значение 1',// Краткая форма записи в формате {Код значения}=>{Значение}
 * 			'2'=>array(// Расширенная форма записи
 * 				'code'=>'2',// Код значения. Необязательный параметр. По умолчанию равно ключевому значению
 * 				'description'=>'Значение 2',// Значение. Обязательный параметр
 * 				'param1'=>'5',// Дополнительный параметр 1. Необязательный параметр. По умолчанию равно null
 * 				'param2'=>'6',// Дополнительный параметр 2. Необязательный параметр. По умолчанию равно null
 * 				'childs'=>array(// Вложенные элементы. Необязательный параметр. По умолчанию равно array()
 * 					'2.1'=>'Значение 2.1',
 * 					...
 * 				)
 * 			),
 * 			...
 * 		),
 * 		...
 * 	)
 */
return array(
	'appeal_themeid'=>array(
		'1'=>array(
			'description'=>'Проблемы, относящиеся к придомовой территории',
			'childs'=>array(
				'1.1'=>array('description'=>'Не работает уличное освещение', 'param1'=>'complaint', 'param2'=>'security'),
				'1.2'=>array('description'=>'Ненадлежащее техническое состояние оборудования детских площадок',
					'param1'=>'complaint', 'param2'=>'security'),
				'1.3'=>array('description'=>'Ненадлежащее санитарное состояние дворовых территорий', 'param1'=>'complaint', 
					'param2'=>'security'),
				'1.4'=>array('description'=>'Локальные разрушения асфальтобетонного покрытия (ямы/выбоины)',
					'param1'=>'complaint', 'param2'=>'security'),
				'1.5'=>array('description'=>'Нарушение графика вывоза ТБО и КГМ (мусор)', 'param1'=>'complaint', 'param2'=>'security'),
				'1.6'=>array('description'=>'Некачественная уборка территории от снега и наледи', 'param1'=>'complaint',
					'param2'=>'security'),
				'1.7'=>array('description'=>'Отсутствие детских площадок во дворах', 'param1'=>'complaint', 'param2'=>'security'),
				'1.8'=>array('description'=>'Нехватка парковочных мест во дворе', 'param1'=>'complaint', 'param2'=>'security'),
				'1.9'=>array('description'=>'Отсутствие урн для мусора у подъездов многоквартирных домов', 'param1'=>'complaint',
					'param2'=>'security'),
				'1.10'=>array('description'=>'Опасные объекты во дворах жилых домов', 'param1'=>'complaint', 'param2'=>'security'),
				'1.11'=>array('description'=>'Нарушение порядка содержания зеленых насаждений', 'param1'=>'complaint',
					'param2'=>'security'),
				'1.12'=>array('description'=>'Неблагоустроенные парковочные места', 'param1'=>'complaint', 'param2'=>'security'),
				'1.13'=>array('description'=>'Неправильное размещение мусорных контейнеров', 'param1'=>'complaint',
					'param2'=>'security'),
				'1.14'=>array('description'=>'Перекрытие пожарных проездов', 'param1'=>'complaint', 'param2'=>'security'),
				'1.15'=>array('description'=>'Строительство опасных объектов вблизи жилых домов', 'param1'=>'complaint',
					'param2'=>'security'),
				'1.16'=>array('description'=>'Ненадлежащее состояние площадки для выгула домашних животных', 'param1'=>'complaint',
					'param2'=>'security'),
				'1.17'=>array('description'=>'Незалитый или некачественно залитый каток', 'param1'=>'complaint', 'param2'=>'security'),
				'1.18'=>array('description'=>'Брошенное разукомплектованное транспортное средство', 'param1'=>'complaint',
					'param2'=>'security'),
				'1.19'=>array('description'=>'Не работает освещение на придомовой территории (в.т.ч., на детских, спортивных/игровых '.
					'площадках, парковочных карманах/гостевых парковках)', 'param1'=>'complaint', 'param2'=>'security')
			)
		),
		'2'=>array(
			'description'=>'Проблемы, относящиеся к внутридомовой территории',
			'childs'=>array(
				'2.1'=>array('description'=>'Отсутствие отопления', 'param1'=>'complaint', 'param2'=>'security'),
				'2.2'=>array('description'=>'Отсутствие водоснабжения', 'param1'=>'complaint', 'param2'=>'security'),
				'2.3'=>array('description'=>'Нарушение температурного режима подачи воды', 'param1'=>'complaint', 'param2'=>'security'),
				'2.4'=>array('description'=>'Нарушение санитарного состояния подъездов и лифтов', 'param1'=>'complaint',
					'param2'=>'security'),
				'2.5'=>array('description'=>'Неисправность лифта', 'param1'=>'complaint', 'param2'=>'security'),
				'2.6'=>array('description'=>'Подъезды в плохом техническом состоянии', 'param1'=>'complaint', 'param2'=>'security'),
				'2.7'=>array('description'=>'Протечка кровли (крыши)', 'param1'=>'complaint', 'param2'=>'security'),
				'2.8'=>array('description'=>'Плохое качество водопроводной воды', 'param1'=>'complaint', 'param2'=>'security'),
				'2.9'=>array('description'=>'Антисанитария в жилых домах', 'param1'=>'complaint', 'param2'=>'security'),
				'2.10'=>array('description'=>'Насекомые и грызуны', 'param1'=>'complaint', 'param2'=>'security'),
				'2.11'=>array('description'=>'Отсутствие капитального ремонта', 'param1'=>'complaint', 'param2'=>'security'),
				'2.12'=>array('description'=>'Сломанный почтовый ящик', 'param1'=>'complaint', 'param2'=>'security'),
				'2.13'=>array('description'=>'Несанкционированные объявления, надписи и рисунки на фасадах', 'param1'=>'complaint',
					'param2'=>'security'),
				'2.14'=>array('description'=>'Низкая/высокая температура в местах общего пользования', 'param1'=>'complaint',
					'param2'=>'security'),
				'2.15'=>array('description'=>'Затопления квартир', 'param1'=>'complaint', 'param2'=>'security'),
				'2.16'=>array('description'=>'Прорыв трубы', 'param1'=>'complaint', 'param2'=>'security'),
				'2.17'=>array('description'=>'Угроза возникновения пожара', 'param1'=>'complaint', 'param2'=>'security'),
				'2.18'=>array('description'=>'Незаконные реконструкции', 'param1'=>'complaint', 'param2'=>'security'),
				'2.19'=>array('description'=>'Проблемы с дымоходами в многоквартирных домах', 'param1'=>'complaint', 'param2'=>'security'),
				'2.20'=>array('description'=>'Повреждение имущества при проведении ремонтных работ', 'param1'=>'complaint',
					'param2'=>'security'),
				'2.21'=>array('description'=>'Проблемы со стоками на загородных участках', 'param1'=>'complaint', 'param2'=>'security'),
				'2.22'=>array('description'=>'Не работает освещение в подъезде/лифте/подвале многоквартирного дома',
					'param1'=>'complaint', 'param2'=>'security'),
				'2.23'=>array('description'=>'Угроза падения льда с крыш зданий', 'param1'=>'complaint', 'param2'=>'security'),
				'2.24'=>array('description'=>'Угроза падения предметов с крыш зданий', 'param1'=>'complaint', 'param2'=>'security')
			)
		),
		'3'=>array(
			'description'=>'Проблемы, связанные с ошибками в квитанциях, оплате, договорах и т.п.',
			'childs'=>array(
				'3.1'=>array('description'=>'Нарушение договоров об аренде жилья', 'param1'=>'complaint', 'param2'=>'tasks'),
				'3.2'=>array('description'=>'Выселение собственников/нанимателей жилья', 'param1'=>'complaint', 'param2'=>'tasks'),
				'3.3'=>array('description'=>'Оплата за неиспользуемую ТВ-антенну в коммунальных счетах', 'param1'=>'complaint',
					'param2'=>'tasks'),
				'3.4'=>array('description'=>'Нарушения при выборе (смене) управляющей организации', 'param1'=>'complaint',
					'param2'=>'tasks'),
				'3.5'=>array('description'=>'Плата за неоказанные услуги (невыполненные работы)', 'param1'=>'complaint',
					'param2'=>'tasks'),
				'3.6'=>array('description'=>'Незаконное отключение коммунальных услуг', 'param1'=>'complaint', 'param2'=>'tasks'),
				'3.7'=>array('description'=>'Проблемы с поставщиками газа', 'param1'=>'complaint', 'param2'=>'tasks'),
				'3.8'=>array('description'=>'Расселение из аварийного дома', 'param1'=>'complaint', 'param2'=>'tasks'),
				'3.9'=>array('description'=>'Дополнительные платежи в управляющие компании', 'param1'=>'complaint', 'param2'=>'tasks'),
				'3.10'=>array('description'=>'Радиоточки в квитанциях на оплату квартиры', 'param1'=>'complaint', 'param2'=>'tasks'),
				'3.11'=>array('description'=>'Ошибки в квитанциях', 'param1'=>'complaint', 'param2'=>'tasks'),
				'3.12'=>array('description'=>'Проблемы с поставщиками электроэнергии', 'param1'=>'complaint', 'param2'=>'tasks'),
				'3.13'=>array('description'=>'Игнорирование обращений в управляющие компании', 'param1'=>'complaint', 'param2'=>'tasks')
			)
		),
		'4'=>array('description'=>'Другая тема', 'param1'=>'complaint', 'param2'=>'security'),
		'5'=>array(
			'description'=>'Оказание коммунальных услуг',
			'childs'=>array(
				'5.1'=>array('description'=>'Качество оказания коммунальных услуг', 'param1'=>'request', 'param2'=>'tasks'),
				'5.2'=>array('description'=>'Сроки оказания коммунальных услуг', 'param1'=>'request', 'param2'=>'tasks'),
				'5.3'=>array('description'=>'Цены (тарифы) на предоставляемые коммунальные услуги', 'param1'=>'request',
					'param2'=>'tasks'),
				'5.4'=>array('description'=>'Размер оплаты за предоставляемые коммунальные услуги', 'param1'=>'request',
					'param2'=>'tasks'),
				'5.5'=>array('description'=>'Объем и перечень предоставляемых коммунальных услуг', 'param1'=>'request',
					'param2'=>'tasks')
			)
		),
		'6'=>array(
			'description'=>'Оказание услуг по содержанию и ремонту',
			'childs'=>array(
				'6.1'=>array('description'=>'Качество оказания услуг по содержанию и ремонту', 'param1'=>'request', 'param2'=>'tasks'),
				'6.2'=>array('description'=>'Сроки оказания услуг по содержанию и ремонту', 'param1'=>'request', 'param2'=>'tasks'),
				'6.3'=>array('description'=>'Цены (тарифы) на предоставляемые услуги по содержанию и ремонту', 'param1'=>'request',
					'param2'=>'tasks'),
				'6.4'=>array('description'=>'Размер оплаты за предоставляемые услуги по содержанию и ремонту', 'param1'=>'request',
					'param2'=>'tasks'),
				'6.5'=>array('description'=>'Объем и перечень предоставляемых услуг по содержанию и ремонту', 'param1'=>'request',
					'param2'=>'tasks')
			)
		),
		'7'=>array(
			'description'=>'Предоставление информации',
			'childs'=>array(
				'7.1'=>array('description'=>'Информация об операциях по спец. счету (фонд капитального ремонта)', 'param1'=>'request',
					'param2'=>'tasks'),
				'7.2'=>array('description'=>'Информация о муниципальных программах сферы ЖКХ', 'param1'=>'request', 'param2'=>'tasks'),
				'7.3'=>array('description'=>'Информация о нормативных правовых актах органов местного самоуправления по ЖКХ',
					'param1'=>'request', 'param2'=>'tasks'),
				'7.4'=>array('description'=>'Информация о состоянии объектов коммунальной и инженерной инфраструктуры',
					'param1'=>'request', 'param2'=>'tasks'),
				'7.5'=>array('description'=>'Информация о лицах, осуществляющих эксплуатацию объектов ЖКХ', 'param1'=>'request',
					'param2'=>'tasks'),
				'7.6'=>array('description'=>'Информация о производственных программах ресурсоснабжающих организаций',
					'param1'=>'request', 'param2'=>'tasks'),
				'7.7'=>array('description'=>'Информация об инвестиционных программах ресурсоснабжающих организаций', 'param1'=>'request',
					'param2'=>'tasks'),
				'7.8'=>array('description'=>'Информация о соблюдении параметров качества товаров и услуг ресурсоснабжающих организаций',
					'param1'=>'request', 'param2'=>'tasks'),
				'7.9'=>array('description'=>'Информация о состоянии расчетов исполнителей коммунальных услуг с ресурсоснабжающими '.
					'организациями', 'param1'=>'request', 'param2'=>'tasks'),
				'7.10'=>array('description'=>'Информация о состоянии расчетов потребителей с исполнителями коммунальных услуг',
					'param1'=>'request', 'param2'=>'tasks'),
				'7.11'=>array('description'=>'Информация о внеплановой проверке деятельности управляющей организации органом местного '.
					'самоуправления', 'param1'=>'request', 'param2'=>'tasks'),
				'7.12'=>array('description'=>'Информация об участии органа местного самоуправления в общем собрании собственников '.
					'помещений',  'param1'=>'request', 'param2'=>'tasks')
			)
		),
		'8'=>array(
			'description'=>'Нарушения со стороны УК, ТСЖ, ЖСК, ЖК или иного СПК',
			'childs'=>array(
				'8.1'=>array('description'=>'Нарушение требований к порядку создания ТСЖ, ЖК, ЖСК или иного СПК', 'param1'=>'complaint',
					'param2'=>'tasks'),
				'8.2'=>array('description'=>'Нарушение требований к уставу ТСЖ, ЖК, ЖСК или иного СПК', 'param1'=>'complaint',
					'param2'=>'tasks'),
				'8.3'=>array('description'=>'Нарушение требований к порядку внесения изменений в устав ТСЖ, ЖК, ЖСК или иного СПК',
					'param1'=>'complaint', 'param2'=>'tasks'),
				'8.4'=>array('description'=>'Нарушение требований к порядку выбора управляющей организации', 'param1'=>'complaint',
					'param2'=>'tasks'),
				'8.5'=>array('description'=>'Нарушение требований к порядку заключения договоров с управляющей организацией',
					'param1'=>'complaint', 'param2'=>'tasks'),
				'8.6'=>array('description'=>'Нарушение требований к порядку содержания и осуществления текущего и капитального ремонта '.
					'общего имущества', 'param1'=>'complaint', 'param2'=>'tasks'),
				'8.7'=>array('description'=>'Нарушение управляющей организацией обязательств по управлению многоквартирным домом',
					'param1'=>'complaint', 'param2'=>'tasks'),
				'8.8'=>array('description'=>'Нарушение при применении предельных (макс.) индексов изменения размера платы граждан за '.
					'коммунальные услуги', 'param1'=>'complaint', 'param2'=>'tasks'),
				'8.9'=>array('description'=>'Нарушение требований к наймодателям или нанимателям в наемных домах социального '.
					'использования', 'param1'=>'complaint', 'param2'=>'tasks'),
				'8.10'=>array('description'=>'Нарушение требований к заключению и исполнению договоров найма в наемных домах '.
					'социального использования', 'param1'=>'complaint', 'param2'=>'tasks')
			)
		),
		'9'=>array(
			'description'=>'Оказание услуг по капитальному ремонту',
			'childs'=>array(
				'9.1'=>array('description'=>'Правомерность включения дома в региональную программу капитального ремонта',
					'param1'=>'request', 'param2'=>'tasks'),
				'9.2'=>array('description'=>'Правомерность исключения дома из региональной программы капитального ремонта',
					'param1'=>'request', 'param2'=>'tasks'),
				'9.3'=>array('description'=>'Правомерность видов работ, указанных в региональной программе капитального ремонта',
					'param1'=>'request', 'param2'=>'tasks'),
				'9.4'=>array('description'=>'Правомерность сроков проведения ремонта, указанных в региональной программе '.
					'капитального ремонта', 'param1'=>'request', 'param2'=>'tasks'),
				'9.5'=>array('description'=>'Правомерность направления платежных документов одновременно Фондом и УК, ТСЖ, ЖСК',
					'param1'=>'request', 'param2'=>'tasks')
			)
		)
	)
);
