<?php
/**
 * Параметры атрибутов моделей по умолчанию.
 * Описание структуры возвращаемого массива:
 * 
 * 	array(
 * 		'user'=>array(// Название модели данных
 * 			'name'=>array(// Название атрибута
 * 				'label'=>'Имя',// Метка атрибута. Необязательный параметр. По умолчанию равно имени атрибута
 * 				// Текст подсказки атрибута. Необязательный параметр. По умолчанию равно false. Допускается задавать также
 * 				// расширенную подсказку, используя разделитель Attribute::SEPARATOR_TOOLTIP. Например,
 * 				// 'Текст placeholder'.Attribute::SEPARATOR_TOOLTIP.'Текст для виджета Tooltip'
 * 				'tooltip'=>'Имя пользователя',
 * 				// Символьный идентификатор статуса атрибута. Необязательный параметр. По умолчанию равно 'required'
 * 				'state'=>'view',
 * 				// Признак служебного атрибута. Необязательный параметр. По умолчанию рано true
 * 				'service'=>true,
 * 				// Символьный идентификатор секции из конфигурационного файла соответствий для указания набора фиксированных значений
 * 				// атрибута. Необязательный параметр. По умолчанию равно false
 * 				'set'=>'user_state'
 * 			),
 * 			'surname'=>'Фамилия',// Упрощённая форма записи данных атрибута с указанием только метки
 * 			'patronymic',// Упрощённая форма записи данных атрибута с указанием только названия атрибута
 * 			...
 * 		),
 * 		...
 * 	)
 */
return array(
	'account'=>array(
		'account'=>'№ Л/С',
		'type'=>'Тип Л/С',
		'executor'=>'Исполнитель',
		'address'=>'Адрес',
		'area'=>'Площадь',
		'share'=>'Доля',
		'balance'=>'Баланс'
	),
	
	'account_connection'=>array(
		'type'=>array('label'=>'Тип лицевого счёта', 'set'=>'account_type'),
		'executor'=>'Исполнитель',
		'account'=>array('label'=>'Номер лицевого счёта',
				'tooltip'=>Attribute::SEPARATOR_TOOLTIP.'Введите номер лицевого счёта, доступ к которому Вы хотите получить'),
		'payer'=>array('label'=>'Плательщик',
				'tooltip'=>Attribute::SEPARATOR_TOOLTIP.'Укажите плательщика по лицевому счёту, как он вписан в квитанции на оплату'),
		'date'=>array('label'=>'Дата последнего платежа', 'tooltip'=>Attribute::SEPARATOR_TOOLTIP.
			'Введите дату последнего платежа по указанному лицевому счёту в формате "ДД.ММ.ГГГГ". Например, "29.04.2016"'),
		'sum'=>'Сумма последнего платежа'
	),
	
	'account_operation'=>array(
		'date'=>'Дата',
		'service'=>'Услуга',
		'operation'=>'Операция',
		'parish'=>'Приход',
		'spend'=>'Расход',
		'sum'=>'Сумма',
		'balance'=>'Баланс'
	),
	
	'account_payment'=>array(
		'id'=>'Лицевой счёт',
		'sum'=>'Сумма платежа',
		'type'=>array('label'=>'Способ оплаты', 'set'=>'account_payment_type'),
		'kind'=>'Вид Л/С',
		'recipient'=>'Получатель',
		'bank'=>'Банк',
		'inn'=>'ИНН',
		'kpp'=>'КПП',
		'bik'=>'БИК',
		'bank_account'=>'Р/с',
		'corr_account'=>'К/с'
	),
	
	'appeal'=>array(
		'id'=>array('label'=>'№', 'state'=>'view'),
		'theme'=>array('label' => 'Тема обращения', 'tooltip' => 'Выберите тему из списка'),
		'themeid'=>'Идентификатор темы обращения',
		'address'=>'Адрес дома по обращению',
		'addressid'=>array('label'=>'Идентификатор адреса по обращению', 'state'=>'update'),
		'responder'=>array('label'=>'Адресат обращения', 'set'=>'appeal_responder'),
		'category'=>array('label'=>'Категория обращения', 'set'=>'appeal_category'),
		'type'=>array('label'=>'Тип обращения', 'set'=>'appeal_type'),
		'timeout'=>array('label'=>'Срок<br>рассмотрения', 'state'=>'view'),
		'authorid'=>'Автор',
		'text'=>'Текст обращения',
		'files'=>array(
			'label'=>'Прикреплённые файлы',
			'tooltip'=>Attribute::SEPARATOR_TOOLTIP.'<strong>Требования к загружаемым файлам:</strong><ul>'.
				'<li>максимальный размер файла: 50 Mb</li>'.
				'<li>максимальное количество файлов: 10</li>'.
				'<li>допустимые форматы файлов: bmp, doc, docx, jpeg, pdf, png, txt, xls, xlsx</li></ul>',
			'state'=>'update'
		),
		'comment'=>'Ответ',
		'commentfiles'=>array('label'=>'Прикреплённые файлы', 'state'=>'update'),
		'answer'=>array('label'=>'Ответ', 'state'=>'view'),
		'answerfiles'=>array('label'=>'Прикреплённые файлы', 'state'=>'view'),
		'author'=>array('label'=>'Автор', 'state'=>'disabled'),
		'clerk'=>array('label'=>'Вышестоящий делопроизводитель', 'state'=>'disabled'),
		'state'=>array('label'=>'Статус', 'state'=>'view', 'set'=>'appeal_state'),
		'datecreated'=>array('label'=>'Дата создания', 'state'=>'view'),
		'dateaccepted'=>array('label'=>'Дата принятия', 'state'=>'view'),
		'datecompleted'=>array('label'=>'Дата ответа', 'state'=>'view')
	),
	
	'bss'=>array(
		'on'=>'Использовать подключение',
		'host'=>array('label'=>'Хост', 'tooltip'=>'Web-адрес сервера. Например, "localhost", "10.59.2.15", "life.com"'),
		'port'=>array('label'=>'Порт', 'state'=>'update'),
		'name'=>array('label'=>'Название веб-сервиса', 'tooltip'=>'Например, "tesths"')
	),
	
	'code'=>array(
		'captchacode'=>array('label'=>'Код подтверждения', 'tooltip'=>'Введите код с картинки'),
		'querycode'=>'Вопрос',
// 		'passwordcode'=>'Пароль', !!
		'passwordcode'=>'Старый пароль',
		'emailcode'=>'E-mail код',
		'phonecode'=>'SMS код'
	),
	
	'compensation'=>array(
		'snils'=>'СНИЛС',
		'address'=>'Адрес',
		'base'=>'Основание<br>для получения<br>компенсации',
		'perioddatestart'=>'Дата начала<br>периода расчёта',
		'perioddateend'=>'Дата окончания<br>периода расчёта',
		'datecalculation'=>'Дата<br>расчёта',
		'name'=>'Наименование<br>расхода',
		'familymembers'=>'Количество членов семьи,<br>на которых распространяется<br>предоставление компенсации расходов',
		'sumexpenses'=>'Размер<br>платы',
		'sumcompensation'=>'Размер<br>компенсации<br>расходов',
		'sumrecalculation'=>'Размер перерасчёта<br>за прошлые периоды',
		'sumcompensationprovided'=>'Размер<br>компенсационной<br>выплаты',
		'state'=>'Статус обработки'
	),
	
	'compensation_payment'=>array(
		'base'=>'Основание для<br>получения компенсации',
		'type'=>'Тип выплаты',
		'date'=>'Дата<br>выплаты',
		'sum'=>'Сумма<br>выплаты',
		'state'=>'Статус обработки'
	),
	
	'confirm'=>array(
		'agreement'=>array('label'=>'Я согласен с условиями Пользовательского соглашения', 'state'=>'update'),
		'secure'=>array('label'=>'Я согласен продолжить работу по каналам связи, защищенным с использованием алгоритмов '.
			'шифрования, несоответствующих ГОСТ Р 34.10-2001', 'state'=>'update'),
		'remember'=>array('label'=>'Запомнить мой выбор', 'state'=>'update')
	),
	
	'counter'=>array(
		'id'=>'Идентификатор',
		'counter'=>'Счётчик',
		'previous'=>'Предыдущее показание',
		'current'=>'Текущее показание',
		'new'=>'Новое показание'
	),
	
	'counter_history'=>array(
		'date'=>'Дата',
		'value'=>'Показание'
	),
	
	'data'=>array(
		'setting'=>'Настройки',
		'user'=>array('label'=>'Пользователи', 'set'=>'table_group_install_type'),
		'auth'=>array('label'=>'Ролевая схема', 'set'=>'table_group_install_type'),
		'scheme'=>array('label'=>'Схема сайта', 'set'=>'table_group_install_type'),
		'directory'=>array('label'=>'Справочники', 'set'=>'table_group_install_type'),
		'message'=>array('label'=>'Сообщения', 'set'=>'table_group_install_type'),
		'appeal'=>array('label'=>'Обращения', 'set'=>'table_group_install_type'),
		'log'=>array('label'=>'Логи', 'set'=>'table_group_install_type'),
		'file'=>array('label'=>'Файлы', 'set'=>'table_group_install_type'),
		'generation'=>array('label'=>'Cпособы генерации пользователей', 'state'=>'update'),
		'quantity'=>array('label'=>'Количество', 'tooltip'=>'Введите число от 1 до 50 000')
	),
	
	'db'=>array(
		'host'=>array('label'=>'Хост', 'tooltip'=>'Web-адрес сервера. Например, "localhost", "10.59.2.15", "life.com"'),
		'port'=>array('label'=>'Порт', 'state'=>'update'),
		'name'=>'Название БД',
		'login'=>'Логин',
		'password'=>array('label'=>'Пароль', 'state'=>'update')
	),
	
	'export'=>array(
		'provider'=>'Поставщик информации',
		'period'=>'Отчётный период',
		'date'=>'Дата формирования',
		'size'=>'Размер'
	),
	
	'house_general'=>array(
		'address'=>'Адрес',
		'credited'=>'Начислено',
		'paid'=>'Оплачено',
		'collectibility'=>'Собираемость'
	),
	
	'house_premise'=>array(
		'premise'=>'Помещение',
		'area'=>'Площадь',
		'credited'=>'Начислено',
		'paid'=>'Оплачено',
		'collectibility'=>'Собираемость'
	),
	
	'login'=>array(
		'login'=>'Логин',
		'password'=>'Пароль'
	),
	
	'email'=>array(
		'on'=>'Использовать подключение',
		'host'=>array('label'=>'Хост','tooltip'=>'Web-адрес сервера. Например, "localhost", "10.59.2.15", "life.com"'),
		'port'=>array('label'=>'Порт', 'state'=>'update'),
		'protocol'=>array('label'=>'Протокол безопасности', 'set'=>'email_protocol'),
		'login'=>'Логин',
		'password'=>array('label'=>'Пароль', 'state'=>'update')
	),
	
	'esia'=>array(
		'on'=>'Использовать подключение'
	),
	
	'fias'=>array(
		'on'=>'Использовать подключение'
	),
	
	'file'=>array(
		'category'=>array('label'=>'Категория', 'set'=>'file_category'),
		'name'=>'Название'
	),
	
	'import'=>array(
		'provider'=>'Поставщик информации',
		'period'=>'Отчётный период',
		'date'=>'Дата формирования',
		'size'=>'Размер',
		'file'=>'Файл выгрузки'
	),
	
	'log'=>array(
		'id_user'=>'Инициатор',
		'id_event'=>'Событие',
		'id_error'=>'Результат',
		'date'=>'Дата',
		'params'=>'Параметры события'
	),
	
	'news'=>array(
		'title'=>'Заголовок',
		'date'=>'Дата публикации',
		'text'=>'Текст',
		'state'=>array('label'=>'Статус', 'set'=>'news_state')
	),
	
	'portal'=>array(
		'on'=>'Использовать подключение',
		'host'=>array('label'=>'Хост', 'tooltip'=>'Web-адрес сервера. Например, "localhost", "10.59.2.15", "life.com"'),
		'port'=>array('label'=>'Порт', 'state'=>'update')
	),
	
	'phone'=>array(
		'on'=>'Использовать подключение',
		'host'=>array('label'=>'Хост', 'tooltip'=>'Web-адрес сервера. Например, "localhost", "10.59.2.15", "life.com"'),
		'port'=>array('label'=>'Порт', 'state'=>'update')
	),
	
	'reserve'=>array(
		'date'=>'Дата формирования',
		'size'=>'Размер'
	),
	
	'role'=>array(
		'name'=>array('label'=>'Роль', 'state'=>'update'),
		'group'=>array('label'=>'Сортировка по полномочиям', 'state'=>'update', 'set'=>'role_group'),
		'pages'=>array('label'=>'Полномочия роли', 'state'=>'update')
	),
	
	'service'=>array(
		'name'=>'Услуга',
		'cost'=>'Тариф'
	),
	
	'service_history'=>array(
		'date'=>'Дата',
		'cost'=>'Тариф'
	),
	
	'setting_general'=>array(
		'state'=>array('label'=>'Статус приложения', 'set'=>'application_state'),
		'name'=>'Название приложения',
		'registration'=>array('label'=>'Тип регистрации пользователей', 'set'=>'user_registration_type'),
		'retries'=>'Количество попыток ввода кодов доступа',
		'timeout'=>array('label'=>'Время действия кодов доступа', 'tooltip'=>Attribute::SEPARATOR_TOOLTIP.
			'Задаётся в секундах. Если равно 0, время действия кодов не будет ограничено'),
		'logdb'=>array('label'=>'Ведение логов в БД', 'state'=>'update'),
		'logfile'=>array('label'=>'Ведение логов в файлах', 'state'=>'update'),
		'cache'=>array('label'=>'Тип кэширования', 'set'=>'cache_type'),
		'duration1'=>array('label'=>'Время кэширования 1', 'tooltip'=>'Время хранения данных в кэше по схеме "Долго"'.
			Attribute::SEPARATOR_TOOLTIP.'Задаётся в секундах. Применяется для хранения мета-данных таблиц БД'),
		'duration2'=>array('label'=>'Время кэширования 2', 'tooltip'=>'Время хранения данных в кэше по схеме "Нормально"'.
			Attribute::SEPARATOR_TOOLTIP.'Задаётся в секундах. Применяется для хранения данных справочников и ролевой схемы'),
		'duration3'=>array('label'=>'Время кэширования 3', 'tooltip'=>'Время хранения данных в кэше по схеме "Быстро"'.
			Attribute::SEPARATOR_TOOLTIP.'Задаётся в секундах. Применяется для хранения настроек приложения и отдельных записей БД'),
		'duration4'=>array('label'=>'Время кэширования 4', 'tooltip'=>'Время хранения данных bss-запросов в кэше. '.
			'Задаётся в секундах.')
	),
		
	/* 'setting_gui'=>array(
		'name'=>'Название приложения',
		'state'=>'Текущий статус приложения',
		'log_file'=>'Ведение логов в файлах',
		'scheme_stack_type'=>'Тип стэка меню',
		'scheme_number_nested'=>'Число уровней бокового меню',
		'scheme_title_show'=>'Отображение заголовка раздела',
		'scheme_compression'=>'Компрессия бокового меню'
	), */
	
	'subsidy'=>array(
		'snils'=>'СНИЛС',
		'address'=>'Адрес',
		'base'=>'Основание<br>для получения<br>субсидии',
		'period'=>'Период, за<br>который выполнен<br>расчёт субсидии',
		'datecalculation'=>'Дата<br>расчёта',
		'familymembers'=>'Количество членов семьи,<br>на которых распространяется<br>предоставление субсидии',
		'sumexpenses'=>'Размер<br>фактических<br>расходов',
		'sumsubsidy'=>'Размер<br>субсидии',
		'sumrecalculation'=>'Размер перерасчёта<br>за прошлые периоды',
		'sumsubsidyprovided'=>'Размер<br>предоставляемой<br>субсидии',
		'state'=>'Статус обработки'
	),
	
	'payment'=>array(
		'snils'=>'СНИЛС',
		'base'=>'Основание для<br>получения субсидии',
		'type'=>'Тип выплаты',
		'date'=>'Дата<br>выплаты',
		'sum'=>'Сумма<br>выплаты',
		'state'=>'Статус обработки'
	),
	
	'user'=>array(
		'roles'=>'Группа',
		'bsscode'=>array('label'=>'Код доступа абонента', 'tooltip'=>'Код доступа к группе "Абонент"', 'state'=>'disabled',
			'service'=>false),
		'state'=>array('label'=>'Статус', 'set'=>'user_state'),
		'registration'=>array('label'=>'Дата регистрации', 'state'=>'view'),
		'locked'=>'Дата разблокировки',
		'delete'=>array('label'=>'Дата удаления', 'state'=>'view'),
		'login'=>array(
			'label'=>'Логин',
			'tooltip'=>Attribute::SEPARATOR_TOOLTIP.'<strong>Логин должен удовлетворять следующим правилам:</strong><ul>'.
				'<li>может содержать буквы английского алфавита, цифры и спецсимволы<br>(_-@.)</li>'.
				'<li>должен начинаться с буквы или цифры</li>'.
				'<li>быть в длину от 4 до 32 символов</li>'.
				'<li>быть уникальным в рамках сайта</li></ul>',
			'state'=>'critical'
		),
		'password'=>array(
			'label'=>'Пароль'.Table::SEPARATOR_WORDS.'Новый пароль',
			'tooltip'=>Attribute::SEPARATOR_TOOLTIP.'<strong>Пароль должен удовлетворять следующим правилам:</strong><ul>'.
				'<li>может содержать буквы английского и русского алфавитов, цифры и '.
					'спецсимволы<br>(`~!@#№$%^&*()-_=+\\|[{]};:\'",<.>/?)</li>'.
				'<li>должен содержать символы хотя бы из двух разных подгрупп</li>'.
				'<li>быть в длину от 5 до 32 символов</li></ul>',
			'state'=>'required',
			'service'=>false
		),
		'password2'=>'Повтор пароля',
		'surname'=>array('label'=>'Фамилия', 'tooltip'=>Attribute::SEPARATOR_TOOLTIP.
			'Для ввода допустимы только символы русского алфавита и дефис', 'state'=>'critical', 'service'=>false),
		'name'=>array('label'=>'Имя', 'state'=>'critical', 'service'=>false),
		'patronymic'=>array('label'=>'Отчество', 'state'=>'critical', 'service'=>false),
		'title'=>array('label'=>'Название организации', 'state'=>'critical', 'service'=>false),
		'powers'=>array('label'=>'Полномочия организации', 'state'=>'view', 'service'=>true),
		'address'=>array('label'=>'Почтовый адрес', 'tooltip'=>'Адрес получения корреспонденции', 'state'=>'update',
			'service'=>false),
		'addressid'=>array('label'=>'Идентификатор адреса', 'state'=>'update'),
		'email'=>array('label'=>'E-mail', 'tooltip'=>Attribute::SEPARATOR_TOOLTIP.
			'На данный e-mail адрес будет отправлена ссылка для активации учётной записи', 'state'=>'required', 'service'=>false),
		'phone'=>array('label'=>'Телефон', 'tooltip'=>'Номер мобильного телефона. Например, "8-951-555-5555"',
			'state'=>'update', 'service'=>false),
		'snils'=>array('label'=>'СНИЛС', 'state'=>'disabled'),
		'delivery'=>array('label'=>'Доставка уведомлений', 'state'=>'disabled'),
		'agreement'=>'Согласие с условиями Пользовательского соглашения',
		'secure'=>array('label'=>'Признак желания пользователя работать по сертификату повышенной безопасности',
			'state'=>'disabled'),
		'restore'=>'Логин или E-mail'
	),
	
	'user_conjugation'=>array(
		'id'=>'Идентификатор'
	),
	
	'user_setting'=>array(
		'delivery'=>array('label'=>'Доставка уведомлений', 'set'=>'user_delivery_type'),
		'esia'=>array('label'=>'Привязка к ЕСИА', 'tooltip'=>'Для привязки своего профиля к Единой системе '.
			'идентификации и авторизации используйте кнопку «Войти через ЕСИА» при авторизации на сайте', 'state'=>'view')
	)
);
