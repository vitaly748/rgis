<?php
/**
 * Настройки приложения по умолчанию.
 * Описание структуры возвращаемого массива:
 * 
 * 	array(
 * 		'general'=>array(//Название блока настроек
 * 			'application_state'=>'init_db',// Запись настройки в формате {Имя настройки}=>{Значение настройки}
 * 			...
 * 		),
 * 		...
 * 	)
 */
return array(
	'bss1'=>array(
		'on'=>false,
		'host'=>'',
		'port'=>'',
		'name'=>''
	),
	
	'bss2'=>array(
		'on'=>false,
		'host'=>'',
		'port'=>'',
		'name'=>''
	),
	
	'email'=>array(
		'on'=>false,
		'host'=>'',
		'port'=>'',
		'email_protocol'=>'disabled',
		'login'=>'',
		'password'=>''
	),
		
	'esia'=>array(
		'on'=>false
	),
	
	'fias'=>array(
		'on'=>false
	),
	
	'general'=>array(
		'application_state'=>'init_db',
		'name'=>'Открытый портал РГИС ЖКХ Пермского края',
		'user_registration_type'=>'confirm_data',
		'code_retries'=>4,
		'code_timeout'=>4 * 60 * 60,
		'log_db'=>true,
		'log_file'=>true
	),
	
	'gui'=>array(
		'scheme_stack_type'=>'lower',
		'scheme_number_nested'=>Scheme::MAX_NUMBER_NESTED,
		'scheme_title_show'=>true,
		'scheme_compression'=>false,
		'counter_li'=>false,
		'counter_ym'=>false
	),
	
	'portal'=>array(
		'on'=>false,
		'host'=>'',
		'port'=>''
	),
	
	'phone'=>array(
		'on'=>false,
		'host'=>'',
		'port'=>''
	)
);
