<?php
$key1 = 'name1.name2.name3';

$pattern1 = '';
$pattern2 = 'name3';
$pattern3 = 'name2.name3';
$pattern4 = 'name1.name2';
$pattern5 = 'name1.name2.*';
$pattern6 = 'name1.*.name3';
$pattern7 = '*.name2.name3';
$pattern8 = '*.name2.*';
$pattern9 = '*.*.name3';
$pattern10 = 'name1.nameX.name3';

$patterns1 = array($key1);
$patterns2 = array($pattern2);
$patterns3 = array($pattern5, $pattern6, $pattern7, $pattern8, $pattern9);
$patterns4 = array($pattern10, $pattern5, $pattern6);
$patterns5 = array($pattern5, $pattern10, $pattern6);
$patterns6 = array($pattern5, $pattern6, $pattern10);

//Тест 1 - Пустой шабон
self::assertFalse(Strings::checkKeyPattern($key1, $pattern1));

//Тест 2 - Точный шабон
self::assertEquals(Strings::checkKeyPattern($key1, $key1), $key1);

//Тест 3 - Точный шабон в массиве
self::assertEquals(Strings::checkKeyPattern($key1, $patterns1), $key1);

//Тест 4 - Последнее слово ключа
self::assertEquals(Strings::checkKeyPattern($key1, $pattern2), $pattern2);

//Тест 5 - Последнее слово ключа в массиве
self::assertEquals(Strings::checkKeyPattern($key1, $patterns2), $pattern2);

//Тест 6 - Два последних слова из ключа
self::assertEquals(Strings::checkKeyPattern($key1, $pattern3), $pattern3);

//Тест 7 - Два первых слова из ключа
self::assertFalse(Strings::checkKeyPattern($key1, $pattern4));

//Тест 8 - Звёздочка в конце
self::assertEquals(Strings::checkKeyPattern($key1, $pattern5), $pattern5);

//Тест 9 - Звёздочка в середине
self::assertEquals(Strings::checkKeyPattern($key1, $pattern6), $pattern6);

//Тест 10 - Звёздочка в начале
self::assertEquals(Strings::checkKeyPattern($key1, $pattern7), $pattern7);

//Тест 11 - Две звёздочки по краям
self::assertEquals(Strings::checkKeyPattern($key1, $pattern8), $pattern8);

//Тест 12 - Две звёздочки в начале
self::assertEquals(Strings::checkKeyPattern($key1, $pattern9), $pattern9);

//Тест 13 - Неверный шаблон
self::assertFalse(Strings::checkKeyPattern($key1, $pattern10));

//Тест 14 - Список нескольких верных шаблонов
self::assertEquals(Strings::checkKeyPattern($key1, $patterns3), $pattern5);

//Тест 15 - Список нескольких шаблонов c неверным шаблоном в начале
self::assertEquals(Strings::checkKeyPattern($key1, $patterns4), $pattern5);

//Тест 16 - Список нескольких шаблонов c неверным шаблоном в середине
self::assertEquals(Strings::checkKeyPattern($key1, $patterns5), $pattern5);

//Тест 17 - Список нескольких шаблонов c неверным шаблоном в конце
self::assertEquals(Strings::checkKeyPattern($key1, $patterns6), $pattern5);
