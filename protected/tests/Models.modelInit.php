<?php
class ModelInitTest1{
}

class ModelInitTest2{
	
	public $val = 1;
	
	public static function model(){
		$model = new ModelInitTest2;
		$model->val = 2;
		return $model;
	}
	
}

$class1 = new ModelInitTest1;
$class2 = new ModelInitTest2;
$class3 = ModelInitTest2::model();
 
$name1 = 'ModelInitTest1';
$name2 = 'ModelInitTestX';
$name3 = 'ModelInitTest2';

$array1 = array('model'=>$class1);
$array2 = array('model'=>null);

//Тест 1 - Подача объекта
Models::modelInit($class = &new ModelInitTest1);
self::assertEquals($class, $class1, false);

//Тест 2 - Подача названия класса
Models::modelInit($class = &$name1);
self::assertEquals($class, $class1, false);

//Тест 3 - Подача неверного названия класса
Models::modelInit($class = &$name2);
self::assertEquals($class, null);

//Тест 4 - Подача пустого массива
Models::modelInit($class = array());
self::assertEquals($class, array());

//Тест 5 - Подача объекта в массиве
Models::modelInit($class = array('model'=>$class1));
self::assertEquals($class, $array1, false);

//Тест 6 - Подача названия класса в массиве
Models::modelInit($class = array('model'=>$name1));
self::assertEquals($class, $array1, false);

//Тест 7 - Подача неверного названия класса в массиве
Models::modelInit($class = array('model'=>$name2));
self::assertEquals($class, $array2);

//Тест 8, 9 - Подача названия класса, содержащего метод model()
Models::modelInit($class = &$name3);
self::assertNotEquals($class, $class2, false);
self::assertEquals($class, $class3, false);
