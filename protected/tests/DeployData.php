<?php

class AttributeTest extends Attribute{
	
	public function getDataDef(){
		return $this->preparationDataDef();
	}
	
	public function getData(){
		return $this->preparationData();
	}
	
	public function model(){
		return DirectoryAttributeModel::model();
	}
	
}

class ConformityTest extends Conformity{

	public function getDataDef(){
		return $this->preparationDataDef();
	}

	public function getData(){
		return $this->preparationData();
	}

	public function model(){
		return DirectoryConformityModel::model();
	}

}

class EventTest extends Event{

	public function getDataDef(){
		return $this->preparationDataDef();
	}

	public function getData(){
		return $this->preparationData();
	}

	public function model(){
		return DirectoryEventModel::model();
	}

}

class LabelTest extends Label{

	public function getDataDef(){
		return $this->preparationDataDef();
	}

	public function getData(){
		return $this->preparationData();
	}

	public function model(){
		return DirectoryLabelModel::model();
	}

}

class ErrorEventTest extends ErrorEvent{

	public function getDataDef(){
		return $this->preparationDataDef();
	}

	public function getData(){
		return $this->preparationData();
	}

	public function model(){
		return DirectoryErrorEventModel::model();
	}

}

class ErrorAttributeTest extends ErrorAttribute{

	public function getDataDef(){
		return $this->preparationDataDef();
	}

	public function getData(){
		return $this->preparationData();
	}

	public function model(){
		return DirectoryErrorAttributeModel::model();
	}

}

class AttributePatternTest extends AttributePattern{
	
	public function getDataDef(){
		return $this->preparationDataDef();
	}
	
	public function getData(){
		return $this->preparationData();
	}
	
	public function model(){
		return DirectoryAttributePatternModel::model();
	}
	
}

class SchemeTest extends Scheme{
	
	public function getDataDef(){
		$this->preparationDataDef();
		
		return $this->getDataArray();
	}

	public function getData(){
		$this->preparationData();
		
		return $this->getDataArray();
	}
	
	public function getDataArray(){
		return array($this->_schemeItem, $this->_schemeItemChild, $this->_schemeItemStatic);
	}
	
}

class SettingTest extends Setting{

	public function getDataDef(){
		return $this->preparationDataDef();
	}

	public function getData(){
		return $this->preparationData();
	}

	public function model(){
		return SettingModel::model();
	}

}

$attribute_class = new AttributeTest;
$conformity_class = new ConformityTest;
$event_class = new EventTest;
$label_class = new LabelTest;
$error_event_class = new ErrorEventTest;
$error_attribute_class = new ErrorAttributeTest;
$attribute_pattern_class = new AttributePatternTest;
$scheme_class = new SchemeTest;
$setting_class = new SettingTest;

//Тест 1 - Соответствие данных справочника атрибутов
self::assertEquals($attribute_class->getDataDef(), $attribute_class->getData());

//Тест 2 - Соответствие данных справочника соответствий
self::assertEquals($conformity_class->getDataDef(), $conformity_class->getData());

//Тест 3 - Соответствие данных справочника событий
self::assertEquals($event_class->getDataDef(), $event_class->getData());

//Тест 4 - Соответствие данных справочника текстовых сообщений
self::assertEquals($label_class->getDataDef(), $label_class->getData());

//Тест 5 - Соответствие данных справочника ошибок событий
self::assertEquals($error_event_class->getDataDef(), $error_event_class->getData());

//Тест 6 - Соответствие данных справочника ошибок атрибутов
self::assertEquals($error_attribute_class->getDataDef(), $error_attribute_class->getData());

//Тест 7 - Соответствие данных справочника шаблонов атрибутов
// self::assertEquals($attribute_pattern_class->getDataDef(), $attribute_pattern_class->getData());

//Тест 8 - Соответствие данных схемы сайта
self::assertEquals($scheme_class->getDataDef(), $scheme_class->getData());

//Тест 9 - Соответствие данных настроек приложения
// self::assertEquals($setting_class->getDataDef(), $setting_class->getData());
