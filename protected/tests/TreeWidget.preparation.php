<?php
class PreparationTest1 extends TreeWidget{
	
	public $blocks;
	
	public $attributes;
	
	public $cells;
	
	public function __construct($levels, $blocks = false, $attributes = false, $cells = false){
		$this->levels = $levels;
		
		if ($blocks)
			$this->blocks = $blocks;
		
		if ($attributes)
			$this->attributes = $attributes;
		
		if ($cells)
			$this->cells = $cells;
	}
	
	protected $levelIdParams = array(
		'attributes'=>'name',
		'features'=>'type'
	);
	
}

class PreparationTest2 extends PreparationTest1{
	
	public $rootName = 'root-name';
	
	public $blockName = 'block-name';
	
}

class PreparationTest3 extends PreparationTest2{
	
	const CLASS_BLOCKS = 'class-block';
	
	public $id = 'preparation-test';
	
	public $htmlOptions = array('id'=>'root', 'for'=>'me', 'class'=>'tree');
	
}

class PreparationTest4 extends PreparationTest3{
	
	public $form = 'form1';
	
	public $attrFull = false;
	
}

class PreparationTest5 extends PreparationTest4{
	
	public $attrFull = true;
	
}

class PreparationTest6 extends PreparationTest1{
	
	const CLASS_ROOT = 'class-root';
	
	const CLASS_BLOCKS = 'class-block';
	
	const CLASS_ATTRIBUTES = 'class-attribute';
	
	const CLASS_CELLS = 'class-cell';
	
	public $htmlOptions = array('id'=>'id-root');
	
	protected $transitions = array('attributes'=>'blocks');
	
}

class PreparationTest7 extends PreparationTest6{
	
	public $id = 'preparation-test';
	
	protected $transitions = null;
	
}

class PreparationModelTest1 extends BaseModel{
	
	public $on;
	
	public $host;
	
	public $port;
	
}

$levels1 = array(
	'root'=>array(
		'form', 'model', 'name', 'blockName', TreeWidget::PARAM_ATTRIBUTES_FULL,
		TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions', 'rootName')
	),
	'blocks'=>array(
		'value1', 'value2',
		TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions', 'blockName')
	)
);
$levels2 = array_merge($levels1, array(
	'attributes'=>array(
		TreeWidget::PARAM_TYPE_LIMIT=>array('form', 'model', 'name')
	)
));
$levels3 = array_merge_recursive($levels2, array(
	'attributes'=>array(
		TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions')
	)
));
$levels4 = array(
	'root'=>array(TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions')),
	'blocks'=>array(TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions')),
	'attributes'=>array(TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions')),
	'cells'=>array(TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions')),
);
$levels5 = array_merge_recursive($levels4, array(
	'root'=>array(TreeWidget::PARAM_TYPE_LIMIT=>array('value')),
	'blocks'=>array(TreeWidget::PARAM_TYPE_LIMIT=>array('value')),
	'attributes'=>array(TreeWidget::PARAM_TYPE_LIMIT=>array('value')),
	'cells'=>array(TreeWidget::PARAM_TYPE_LIMIT=>array('value')),
));

$model1 = new PreparationModelTest1;
$model2 = 'PreparationModelTest1';

$blocks1 = 2;
$blocks2 = array('block1', 'block2');
$blocks3 = array('block1', 'block2'=>array('form'=>'form2'), TreeWidget::PARAM_ITEMS_PRIORITY=>1);
$blocks4 = array(
	'block1'=>array('model'=>$model1),
	'block2'=>array('form'=>'form2'),
	TreeWidget::PARAM_ITEMS_PRIORITY=>1
);
$blocks5 = array(
	'block1'=>array('model'=>$model2),
	'block2'=>array('form'=>'form2'),
	TreeWidget::PARAM_ITEMS_PRIORITY=>1
);
$blocks6 = array(
	'block1'=>array(
		'attributes'=>array(
			'attr1', 'attr2'
		)
	),
	'block2'=>array(
		'form'=>'form2'
	),
	TreeWidget::PARAM_ITEMS_PRIORITY=>1
);
$blocks7 = array(
	'block1'=>array(
		'model'=>$model1,
		'attributes'=>array(
			'attr1', 'attr2'
		)
	),
	'block2'=>array(
		'form'=>'form2'
	),
	TreeWidget::PARAM_ITEMS_PRIORITY=>1
);
$blocks8 = array(
	'block1'=>array(
		'model'=>$model1,
		'attributes'=>array(
			'host'=>'host', 'port', 'htmlOptions'=>array('class'=>'attr')
		)
	),
	'block2'=>array(
		'form'=>'form2'
	),
	TreeWidget::PARAM_ITEMS_PRIORITY=>1
);
$blocks9 = array(
	'block1'=>array(
		'htmlOptions'=>array('id'=>'id-block1'),
		'attributes'=>array(
			'attr1'=>array(
				'htmlOptions'=>array('id'=>'id-attr1'),
				'cells'=>array(
					'cell11'=>array(
						'htmlOptions'=>array('id'=>'id-cell11'),
					),
					'cell12'=>array(
						'htmlOptions'=>array('id'=>'id-cell12'),
					)
				)
			),
			'attr2'=>array(
				'htmlOptions'=>array('id'=>'id-attr2'),
				'cells'=>array(
					'cell21'=>array(
						'htmlOptions'=>array('id'=>'id-cell21'),
					)
				),
				'blocks'=>array(
					'block-child21'=>array(
						'htmlOptions'=>array('id'=>'id-block-child21'),
					),
					'block-child22'=>array(
						'htmlOptions'=>array('id'=>'id-block-child22'),
						'attributes'=>array(
							'attr-child221'=>array(
								'htmlOptions'=>array('id'=>'id-attr-child221'),
								'cells'=>array(
									'cell-child2211'=>array(
										'htmlOptions'=>array('id'=>'id-cell-child2211')
									)
								)
							)
						)
					)
				)
			),
			'attr3'=>array(
				'htmlOptions'=>array('id'=>'id-attr3'),
				'blocks'=>array(
					'block-child31'=>array(
						'htmlOptions'=>array('id'=>'id-block-child31'),
					),
					'block-child32'=>array(
						'htmlOptions'=>array('id'=>'id-block-child32'),
						'attributes'=>array(
							'attr-child321'=>array(
								'htmlOptions'=>array('id'=>'id-attr-child321'),
								'cells'=>array(
									'cell-child3211'=>array(
										'htmlOptions'=>array('id'=>'id-cell-child3211')
									)
								)
							)
						)
					)
				)
			)
		)
	)
);

$attributes1 = array(
	3=>array(
		'htmlOptions'=>array('id'=>'attr')
	)
);
$attributes2 = array(
	'2.attr-1'=>array(
		'htmlOptions'=>array('id'=>'attr')
	)
);
$attributes3 = array(
	'1.attr-1'=>array(
		'htmlOptions'=>array('id'=>'attr')
	)
);
$attributes4 = array(
	'attr2'=>array(
		'value'=>'attr2'
	),
	TreeWidget::PARAM_ITEMS_INHERIT_DATA=>2
);
$attributes5 = array(
	'block2.attr2'=>array(
		'value'=>'attr2'
	)
);
$attributes6 = array(
	'block1.attr2'=>array(
		'value'=>'attr2'
	),
	TreeWidget::PARAM_ITEMS_INHERIT_DATA=>2
);

$cells1 = array(
	'cell12'=>array(
		'value'=>'cell12'
	),
	TreeWidget::PARAM_ITEMS_INHERIT_DATA=>2
);
$cells2 = array(
	'attr4.cell12'=>array(
		'value'=>'cell12'
	)
);
$cells3 = array(
	'attr1.cell12'=>array(
		'value'=>'cell12'
	),
	'attr2.cell21'=>array(
		'value'=>'cell21'
	),
	TreeWidget::PARAM_ITEMS_INHERIT_DATA=>2
);

$class1 = new PreparationTest1($levels1, $blocks1);
$class2 = new PreparationTest1($levels1, $blocks2);
$class3 = new PreparationTest2($levels1, $blocks2);
$class4 = new PreparationTest3($levels1, $blocks2);
$class5 = new PreparationTest3($levels2, $blocks2);
$class6 = new PreparationTest4($levels2, $blocks3);
$class7 = new PreparationTest4($levels2, $blocks4);
$class8 = new PreparationTest4($levels2, $blocks5);
$class9 = new PreparationTest4($levels3, $blocks6);
$class10 = new PreparationTest4($levels3, $blocks7);
$class11 = new PreparationTest4($levels3, $blocks8);
$class12 = new PreparationTest5($levels3, $blocks8);
$class13 = new PreparationTest6($levels4, $blocks9);
$class14 = new PreparationTest7($levels4, false, $attributes1);
$class15 = new PreparationTest7($levels4, false, $attributes2);
$class16 = new PreparationTest7($levels4, false, $attributes3);
$class17 = new PreparationTest7($levels5, $blocks9);
$class18 = new PreparationTest7($levels5, $blocks9, $attributes4, $cells1);
$class19 = new PreparationTest7($levels5, $blocks9, $attributes5, $cells2);
$class20 = new PreparationTest7($levels5, $blocks9, $attributes6, $cells3);

//Тест 1 - Простой пример
self::assertEquals($class1->preparation(), array(
	'htmlOptions'=>array('id'=>'yw0'),
	'rootName'=>null,
	'id'=>'yw0',
	'blocks'=>array(
		1=>array(
			'htmlOptions'=>null,
			'blockName'=>null,
			'id'=>1
		),
		2=>array(
			'htmlOptions'=>null,
			'blockName'=>null,
			'id'=>2
		)
	)
));
Widget::resetCounter('PreparationTest1');

//Тест 2 - Присвоение идентификаторов блокам
self::assertEquals($class2->preparation(), array(
	'htmlOptions'=>array('id'=>'yw0'),
	'rootName'=>null,
	'id'=>'yw0',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>null,
			'blockName'=>null,
			'id'=>'block1'
		),
		'block2'=>array(
			'htmlOptions'=>null,
			'blockName'=>null,
			'id'=>'block2'
		)
	)
));
Widget::resetCounter();

//Тест 3 - Установка параметров rootName и blockName
self::assertEquals($class3->preparation(), array(
	'htmlOptions'=>array('id'=>'yw0'),
	'rootName'=>'root-name',
	'id'=>'yw0',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>null,
			'blockName'=>'block-name',
			'id'=>'block1'
		),
		'block2'=>array(
			'htmlOptions'=>null,
			'blockName'=>'block-name',
			'id'=>'block2'
		)
	)
));
Widget::resetCounter();

//Тест 4 - Добавление HTML-опций
self::assertEquals($class4->preparation(), array(
	'htmlOptions'=>array('id'=>'root', 'for'=>'me', 'class'=>'tree'),
	'rootName'=>'root-name',
	'id'=>'preparation-test',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block1'
		),
		'block2'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block2'
		)
	)
));
Widget::resetCounter();

//Тест 5 - Добавление третьего уровня
self::assertEquals($class5->preparation(), array(
	'htmlOptions'=>array('id'=>'root', 'for'=>'me', 'class'=>'tree'),
	'rootName'=>'root-name',
	'id'=>'preparation-test',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block1',
			'attributes'=>array(
				1=>array(
					'form'=>null,
					'model'=>null,
					'name'=>null,
					'id'=>1,
					'_fullness'=>Swamper::FULLNESS_NONE
				)
			)
		),
		'block2'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block2',
			'attributes'=>array(
				1=>array(
					'form'=>null,
					'model'=>null,
					'name'=>null,
					'id'=>1,
					'_fullness'=>Swamper::FULLNESS_NONE
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 6 - Установка параметра form
self::assertEquals($class6->preparation(), array(
	'htmlOptions'=>array('id'=>'root', 'for'=>'me', 'class'=>'tree'),
	'rootName'=>'root-name',
	'id'=>'preparation-test',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block1',
			'attributes'=>array(
				1=>array(
					'form'=>'form1',
					'model'=>null,
					'name'=>null,
					'id'=>1,
					'_fullness'=>Swamper::FULLNESS_NONE
				)
			)
		),
		'block2'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block2',
			'attributes'=>array(
				1=>array(
					'form'=>'form2',
					'model'=>null,
					'name'=>null,
					'id'=>1,
					'_fullness'=>Swamper::FULLNESS_NONE
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 7 - Применение модели
self::assertEquals($class7->preparation(), array(
	'htmlOptions'=>array('id'=>'root', 'for'=>'me', 'class'=>'tree'),
	'rootName'=>'root-name',
	'id'=>'preparation-test',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block1',
			'attributes'=>array(
				1=>array(
					'form'=>'form1',
					'model'=>$model1,
					'name'=>null,
					'id'=>1,
					'_fullness'=>Swamper::FULLNESS_NONE
				)
			)
		),
		'block2'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block2',
			'attributes'=>array(
				1=>array(
					'form'=>'form2',
					'model'=>null,
					'name'=>null,
					'id'=>1,
					'_fullness'=>Swamper::FULLNESS_NONE
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 8 - Применение модели, модель задана названием класса модели
self::assertEquals($class8->preparation(), array(
	'htmlOptions'=>array('id'=>'root', 'for'=>'me', 'class'=>'tree'),
	'rootName'=>'root-name',
	'id'=>'preparation-test',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block1',
			'attributes'=>array(
				1=>array(
					'form'=>'form1',
					'model'=>$model1,
					'name'=>false,
					'id'=>1,
					'_fullness'=>Swamper::FULLNESS_NONE
				)
			)
		),
		'block2'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block2',
			'attributes'=>array(
				1=>array(
					'form'=>'form2',
					'model'=>false,
					'name'=>false,
					'id'=>1,
					'_fullness'=>Swamper::FULLNESS_NONE
				)
			)
		)
	)
), false);
Widget::resetCounter();


//Тест 9 - Установка списка атрибутов, добавление параметра htmlOptions для уровня атрибутов
self::assertEquals($class9->preparation(), array(
	'htmlOptions'=>array('id'=>'root', 'for'=>'me', 'class'=>'tree'),
	'rootName'=>'root-name',
	'id'=>'preparation-test',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block1',
			'attributes'=>array(
				'attr1'=>array(
					'form'=>'form1',
					'model'=>null,
					'name'=>'attr1',
					'htmlOptions'=>null,
					'id'=>'attr1',
					'_fullness'=>Swamper::FULLNESS_NAME
				),
				'attr2'=>array(
					'form'=>'form1',
					'model'=>null,
					'name'=>'attr2',
					'htmlOptions'=>null,
					'id'=>'attr2',
					'_fullness'=>Swamper::FULLNESS_NAME
				)
			)
		),
		'block2'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block2',
			'attributes'=>array(
				1=>array(
					'form'=>'form2',
					'model'=>null,
					'name'=>null,
					'htmlOptions'=>null,
					'id'=>1,
					'_fullness'=>Swamper::FULLNESS_NONE
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 10 - Применение модели со списком неверных атрибутов
self::assertEquals($class10->preparation(), array(
	'htmlOptions'=>array('id'=>'root', 'for'=>'me', 'class'=>'tree'),
	'rootName'=>'root-name',
	'id'=>'preparation-test',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block1',
			'attributes'=>array(
				1=>array(
					'form'=>'form1',
					'model'=>$model1,
					'name'=>null,
					'htmlOptions'=>null,
					'id'=>1,
					'_fullness'=>Swamper::FULLNESS_NONE
				)
			)
		),
		'block2'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block2',
			'attributes'=>array(
				1=>array(
					'form'=>'form2',
					'model'=>null,
					'name'=>null,
					'htmlOptions'=>null,
					'id'=>1,
					'_fullness'=>Swamper::FULLNESS_NONE
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 11 - Применение модели со списком верных атрибутов
self::assertEquals($class11->preparation(), array(
	'htmlOptions'=>array('id'=>'root', 'for'=>'me', 'class'=>'tree'),
	'rootName'=>'root-name',
	'id'=>'preparation-test',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block1',
			'attributes'=>array(
				'host'=>array(
					'form'=>'form1',
					'model'=>$model1,
					'name'=>'host',
					'htmlOptions'=>array('class'=>'attr'),
					'id'=>'host',
					'_fullness'=>Swamper::FULLNESS_FORM
				),
				'port'=>array(
					'form'=>'form1',
					'model'=>$model1,
					'name'=>'port',
					'htmlOptions'=>array('class'=>'attr'),
					'id'=>'port',
					'_fullness'=>Swamper::FULLNESS_FORM
				)
			)
		),
		'block2'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block2',
			'attributes'=>array(
				1=>array(
					'form'=>'form2',
					'model'=>null,
					'name'=>null,
					'htmlOptions'=>null,
					'id'=>1,
					'_fullness'=>Swamper::FULLNESS_NONE
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 12 - Применение параметра attrFull
self::assertEquals($class12->preparation(), array(
	'htmlOptions'=>array('id'=>'root', 'for'=>'me', 'class'=>'tree'),
	'rootName'=>'root-name',
	'id'=>'preparation-test',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block1',
			'attributes'=>array(
				'on'=>array(
					'form'=>'form1',
					'model'=>$model1,
					'name'=>'on',
					'htmlOptions'=>array('class'=>'attr'),
					'id'=>'on',
					'_fullness'=>Swamper::FULLNESS_FORM
				),
				'host'=>array(
					'form'=>'form1',
					'model'=>$model1,
					'name'=>'host',
					'htmlOptions'=>array('class'=>'attr'),
					'id'=>'host',
					'_fullness'=>Swamper::FULLNESS_FORM
				),
				'port'=>array(
					'form'=>'form1',
					'model'=>$model1,
					'name'=>'port',
					'htmlOptions'=>array('class'=>'attr'),
					'id'=>'port',
					'_fullness'=>Swamper::FULLNESS_FORM
				)
			)
		),
		'block2'=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'blockName'=>'block-name',
			'id'=>'block2',
			'attributes'=>array(
				1=>array(
					'form'=>'form2',
					'model'=>null,
					'name'=>null,
					'htmlOptions'=>null,
					'id'=>1,
					'_fullness'=>Swamper::FULLNESS_NONE
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 13 - Использование нестандартных переходов transitions
self::assertEquals($class13->preparation(), array(
	'htmlOptions'=>array('id'=>'id-root', 'class'=>'class-root'),
	'id'=>'yw0',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>array('id'=>'id-block1', 'class'=>'class-block'),
			'id'=>'block1',
			'attributes'=>array(
				'attr1'=>array(
					'htmlOptions'=>array('id'=>'id-attr1', 'class'=>'class-attribute'),
					'id'=>'attr1',
					'cells'=>array(
						'cell11'=>array(
							'htmlOptions'=>array('id'=>'id-cell11', 'class'=>'class-cell'),
							'id'=>'cell11',
						),
						'cell12'=>array(
							'htmlOptions'=>array('id'=>'id-cell12', 'class'=>'class-cell'),
							'id'=>'cell12',
						)
					)
				),
				'attr2'=>array(
					'htmlOptions'=>array('id'=>'id-attr2', 'class'=>'class-attribute'),
					'id'=>'attr2',
					'blocks'=>array(
						'block-child21'=>array(
							'htmlOptions'=>array('id'=>'id-block-child21', 'class'=>'class-block'),
							'id'=>'block-child21',
							'attributes'=>array(
								'attr2'=>array(
									'htmlOptions'=>array('class'=>'class-attribute'),
									'id'=>'attr2',
									'cells'=>array(
										1=>array(
											'htmlOptions'=>array('class'=>'class-cell'),
											'id'=>1
										)
									)
								)
							)
						),
						'block-child22'=>array(
							'htmlOptions'=>array('id'=>'id-block-child22', 'class'=>'class-block'),
							'id'=>'block-child22',
							'attributes'=>array(
								'attr-child221'=>array(
									'htmlOptions'=>array('id'=>'id-attr-child221', 'class'=>'class-attribute'),
									'id'=>'attr-child221',
									'cells'=>array(
										'cell-child2211'=>array(
											'htmlOptions'=>array('id'=>'id-cell-child2211', 'class'=>'class-cell'),
											'id'=>'cell-child2211'
										)
									)
								)
							)
						)
					)
				),
				'attr3'=>array(
					'htmlOptions'=>array('id'=>'id-attr3', 'class'=>'class-attribute'),
					'id'=>'attr3',
					'blocks'=>array(
						'block-child31'=>array(
							'htmlOptions'=>array('id'=>'id-block-child31', 'class'=>'class-block'),
							'id'=>'block-child31',
							'attributes'=>array(
								'attr3'=>array(
									'htmlOptions'=>array('class'=>'class-attribute'),
									'id'=>'attr3',
									'cells'=>array(
										1=>array(
											'htmlOptions'=>array('class'=>'class-cell'),
											'id'=>1
										)
									)
								)
							)
						),
						'block-child32'=>array(
							'htmlOptions'=>array('id'=>'id-block-child32', 'class'=>'class-block'),
							'id'=>'block-child32',
							'attributes'=>array(
								'attr-child321'=>array(
									'htmlOptions'=>array('id'=>'id-attr-child321', 'class'=>'class-attribute'),
									'id'=>'attr-child321',
									'cells'=>array(
										'cell-child3211'=>array(
											'htmlOptions'=>array('id'=>'id-cell-child3211', 'class'=>'class-cell'),
											'id'=>'cell-child3211'
										)
									)
								)
							)
						)
					)
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 14 - Инициализация атрибута с цифровым ключом
self::assertEquals($class14->preparation(), array(
	'htmlOptions'=>array('id'=>'id-root', 'class'=>'class-root'),
	'id'=>'preparation-test',
	'blocks'=>array(
		1=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'id'=>1,
			'attributes'=>array(
				1=>array(
					'htmlOptions'=>array('id'=>'attr', 'class'=>'class-attribute'),
					'id'=>1,
					'cells'=>array(
						1=>array(
							'htmlOptions'=>array('class'=>'class-cell'),
							'id'=>1
						)
					)
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 15 - Инициализация атрибута с символьным неверным ключом
self::assertEquals($class15->preparation(), array(
	'htmlOptions'=>array('id'=>'id-root', 'class'=>'class-root'),
	'id'=>'preparation-test',
	'blocks'=>array(
		1=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'id'=>1,
			'attributes'=>array(
				1=>array(
					'htmlOptions'=>array('class'=>'class-attribute'),
					'id'=>1,
					'cells'=>array(
						1=>array(
							'htmlOptions'=>array('class'=>'class-cell'),
							'id'=>1
						)
					)
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 16 - Инициализация атрибута с символьным верным ключом
self::assertEquals($class16->preparation(), array(
	'htmlOptions'=>array('id'=>'id-root', 'class'=>'class-root'),
	'id'=>'preparation-test',
	'blocks'=>array(
		1=>array(
			'htmlOptions'=>array('class'=>'class-block'),
			'id'=>1,
			'attributes'=>array(
				'attr-1'=>array(
					'htmlOptions'=>array('id'=>'attr', 'class'=>'class-attribute'),
					'id'=>'attr-1',
					'cells'=>array(
						1=>array(
							'htmlOptions'=>array('class'=>'class-cell'),
							'id'=>1
						)
					)
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 17 - Отмена нестандартных переходов transitions
self::assertEquals($class17->preparation(), array(
	'htmlOptions'=>array('id'=>'id-root', 'class'=>'class-root'),
	'value'=>null,
	'id'=>'preparation-test',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>array('id'=>'id-block1', 'class'=>'class-block'),
			'value'=>null,
			'id'=>'block1',
			'attributes'=>array(
				'attr1'=>array(
					'htmlOptions'=>array('id'=>'id-attr1', 'class'=>'class-attribute'),
					'value'=>null,
					'id'=>'attr1',
					'cells'=>array(
						'cell11'=>array(
							'htmlOptions'=>array('id'=>'id-cell11', 'class'=>'class-cell'),
							'value'=>null,
							'id'=>'cell11',
						),
						'cell12'=>array(
							'htmlOptions'=>array('id'=>'id-cell12', 'class'=>'class-cell'),
							'value'=>null,
							'id'=>'cell12',
						)
					)
				),
				'attr2'=>array(
					'htmlOptions'=>array('id'=>'id-attr2', 'class'=>'class-attribute'),
					'value'=>null,
					'id'=>'attr2',
					'cells'=>array(
						'cell21'=>array(
							'htmlOptions'=>array('id'=>'id-cell21', 'class'=>'class-cell'),
							'value'=>null,
							'id'=>'cell21'
						)
					)
				),
				'attr3'=>array(
					'htmlOptions'=>array('id'=>'id-attr3', 'class'=>'class-attribute'),
					'value'=>null,
					'id'=>'attr3',
					'cells'=>array(
						1=>array(
							'htmlOptions'=>array('class'=>'class-cell'),
							'value'=>null,
							'id'=>1
						)
					)
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 18 - Инициализация атрибута и ячейки с символьным ключом
self::assertEquals($class18->preparation(), array(
	'htmlOptions'=>array('id'=>'id-root', 'class'=>'class-root'),
	'value'=>null,
	'id'=>'preparation-test',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>array('id'=>'id-block1', 'class'=>'class-block'),
			'value'=>null,
			'id'=>'block1',
			'attributes'=>array(
				'attr1'=>array(
					'htmlOptions'=>array('id'=>'id-attr1', 'class'=>'class-attribute'),
					'value'=>null,
					'id'=>'attr1',
					'cells'=>array(
						'cell11'=>array(
							'htmlOptions'=>array('id'=>'id-cell11', 'class'=>'class-cell'),
							'value'=>null,
							'id'=>'cell11',
						),
						'cell12'=>array(
							'htmlOptions'=>array('id'=>'id-cell12', 'class'=>'class-cell'),
							'value'=>'cell12',
							'id'=>'cell12'
						)
					)
				),
				'attr2'=>array(
					'htmlOptions'=>array('id'=>'id-attr2', 'class'=>'class-attribute'),
					'value'=>'attr2',
					'id'=>'attr2',
					'cells'=>array(
						'cell21'=>array(
							'htmlOptions'=>array('id'=>'id-cell21', 'class'=>'class-cell'),
							'value'=>null,
							'id'=>'cell21'
						),
						'cell12'=>array(
							'htmlOptions'=>array('class'=>'class-cell'),
							'value'=>'cell12',
							'id'=>'cell12'
						)
					)
				),
				'attr3'=>array(
					'htmlOptions'=>array('id'=>'id-attr3', 'class'=>'class-attribute'),
					'value'=>null,
					'id'=>'attr3',
					'cells'=>array(
						'cell12'=>array(
							'htmlOptions'=>array('class'=>'class-cell'),
							'value'=>'cell12',
							'id'=>'cell12'
						)
					)
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 19 - Использование неверных префиксов
self::assertEquals($class19->preparation(), array(
	'htmlOptions'=>array('id'=>'id-root', 'class'=>'class-root'),
	'value'=>null,
	'id'=>'preparation-test',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>array('id'=>'id-block1', 'class'=>'class-block'),
			'value'=>null,
			'id'=>'block1',
			'attributes'=>array(
				'attr1'=>array(
					'htmlOptions'=>array('id'=>'id-attr1', 'class'=>'class-attribute'),
					'value'=>null,
					'id'=>'attr1',
					'cells'=>array(
						'cell11'=>array(
							'htmlOptions'=>array('id'=>'id-cell11', 'class'=>'class-cell'),
							'value'=>null,
							'id'=>'cell11',
						),
						'cell12'=>array(
							'htmlOptions'=>array('id'=>'id-cell12', 'class'=>'class-cell'),
							'value'=>null,
							'id'=>'cell12',
						)
					)
				),
				'attr2'=>array(
					'htmlOptions'=>array('id'=>'id-attr2', 'class'=>'class-attribute'),
					'value'=>null,
					'id'=>'attr2',
					'cells'=>array(
						'cell21'=>array(
							'htmlOptions'=>array('id'=>'id-cell21', 'class'=>'class-cell'),
							'value'=>null,
							'id'=>'cell21'
						)
					)
				),
				'attr3'=>array(
					'htmlOptions'=>array('id'=>'id-attr3', 'class'=>'class-attribute'),
					'value'=>null,
					'id'=>'attr3',
					'cells'=>array(
						1=>array(
							'htmlOptions'=>array('class'=>'class-cell'),
							'value'=>null,
							'id'=>1
						)
					)
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 20 - Использование верных префиксов
self::assertEquals($class20->preparation(), array(
	'htmlOptions'=>array('id'=>'id-root', 'class'=>'class-root'),
	'value'=>null,
	'id'=>'preparation-test',
	'blocks'=>array(
		'block1'=>array(
			'htmlOptions'=>array('id'=>'id-block1', 'class'=>'class-block'),
			'value'=>null,
			'id'=>'block1',
			'attributes'=>array(
				'attr1'=>array(
					'htmlOptions'=>array('id'=>'id-attr1', 'class'=>'class-attribute'),
					'value'=>null,
					'id'=>'attr1',
					'cells'=>array(
						'cell11'=>array(
							'htmlOptions'=>array('id'=>'id-cell11', 'class'=>'class-cell'),
							'value'=>null,
							'id'=>'cell11',
						),
						'cell12'=>array(
							'htmlOptions'=>array('id'=>'id-cell12', 'class'=>'class-cell'),
							'value'=>'cell12',
							'id'=>'cell12',
						)
					)
				),
				'attr2'=>array(
					'htmlOptions'=>array('id'=>'id-attr2', 'class'=>'class-attribute'),
					'value'=>'attr2',
					'id'=>'attr2',
					'cells'=>array(
						'cell21'=>array(
							'htmlOptions'=>array('id'=>'id-cell21', 'class'=>'class-cell'),
							'value'=>'cell21',
							'id'=>'cell21'
						)
					)
				),
				'attr3'=>array(
					'htmlOptions'=>array('id'=>'id-attr3', 'class'=>'class-attribute'),
					'value'=>null,
					'id'=>'attr3',
					'cells'=>array(
						1=>array(
							'htmlOptions'=>array('class'=>'class-cell'),
							'value'=>null,
							'id'=>1
						)
					)
				)
			)
		)
	)
));
Widget::resetCounter();
