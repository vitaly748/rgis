<?php
Yii::import('widgets.NestedList.NestedList');

class NestedListTest1 extends NestedList{
	
	public function __construct($params = false){
		if (is_array($params))
			foreach ($params as $name=>$value)
				if (property_exists($this, $name))
					$this->$name = $value;
	}
	
	public function run(){
		return $this->preparationRelations($this->preparationNames($this->preparation()));
	}
	
}

class NestedListModelTest1 extends BaseModel{
	
	public $field1;
	
	public $field2;
	
}

$model1 = new NestedListModelTest1;

$params1 = array('blockConformity'=>'scheme_stack_type');
$params2 = array_merge($params1, array('itemModel'=>$model1, 'itemName'=>'field2', 'itemType'=>'checkbox'));
$params3 = array_merge($params2, array('blockConformityExceptions'=>
	array('upper'=>'$params[0]["itemName"] === "field2"')));
$params4 = array_merge($params2, array('blockConformityExceptions'=>
	array('upper, lower'=>'$params[0]["itemName"] === "field2"')));
$params5 = array_merge($params2, array('items'=>array('upper'=>array('blocks'=>array(
	array('itemName'=>'field1', 'blockConformity'=>'event_fix'))))));

$class1 = new NestedListTest1;
$class2 = new NestedListTest1(array('items'=>2, 'itemValue'=>2));
$class3 = new NestedListTest1($params1);
$class4 = new NestedListTest1($params2);
$class5 = new NestedListTest1($params3);
$class6 = new NestedListTest1($params4);
$class7 = new NestedListTest1($params5);

//Тест 1 - Запуск без параметров
self::assertEquals($class1->run(), array(
	'htmlOptions'=>array('id'=>'yw0', 'class'=>'nested-list'),
	'id'=>'yw0',
	'blocks'=>array(
		1=>array(
			'htmlOptions'=>array('class'=>'nl-block'),
			'blockConformity'=>null,
			'blockConformityExceptions'=>null,
			'id'=>1,
			'blockNeedId'=>null,
			'items'=>array(
				1=>array(
					'htmlOptions'=>array('class'=>'nl-item', 'uncheckValue'=>null, 'value'=>1),
					'itemType'=>NestedList::ITEM_TYPE_DEF,
					'itemValue'=>1,
					'itemChecked'=>null,
					'itemLabel'=>null,
					'widgetName'=>null,
					'widgetParams'=>null,
					'id'=>1,
					'itemForm'=>null,
					'itemModel'=>null,
					'itemName'=>'yw0-1',
					'itemNeedId'=>null,
					'_fullness'=>Swamper::FULLNESS_NAME
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 2 - Определение двух элементов
self::assertEquals($class2->run(), array(
	'htmlOptions'=>array('id'=>'yw0', 'class'=>'nested-list'),
	'id'=>'yw0',
	'blocks'=>array(
		1=>array(
			'htmlOptions'=>array('class'=>'nl-block'),
			'blockConformity'=>null,
			'blockConformityExceptions'=>null,
			'id'=>1,
			'blockNeedId'=>null,
			'items'=>array(
				1=>array(
					'htmlOptions'=>array('class'=>'nl-item', 'uncheckValue'=>null, 'value'=>2),
					'itemType'=>NestedList::ITEM_TYPE_DEF,
					'itemValue'=>2,
					'itemChecked'=>null,
					'itemLabel'=>null,
					'widgetName'=>null,
					'widgetParams'=>null,
					'id'=>1,
					'itemForm'=>null,
					'itemModel'=>null,
					'itemName'=>'yw0-1',
					'itemNeedId'=>null,
					'_fullness'=>Swamper::FULLNESS_NAME
				),
				2=>array(
					'htmlOptions'=>array('class'=>'nl-item', 'uncheckValue'=>null, 'value'=>2),
					'itemType'=>NestedList::ITEM_TYPE_DEF,
					'itemValue'=>2,
					'itemChecked'=>null,
					'itemLabel'=>null,
					'widgetName'=>null,
					'widgetParams'=>null,
					'id'=>2,
					'itemForm'=>null,
					'itemModel'=>null,
					'itemName'=>'yw0-1',
					'itemNeedId'=>null,
					'_fullness'=>Swamper::FULLNESS_NAME
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 3 - Применение блока соответствий "scheme_stack_type"
self::assertEquals($class3->run(), array(
	'htmlOptions'=>array('id'=>'yw0', 'class'=>'nested-list'),
	'id'=>'yw0',
	'blocks'=>array(
		1=>array(
			'htmlOptions'=>array('class'=>'nl-block'),
			'blockConformity'=>'scheme_stack_type',
			'blockConformityExceptions'=>null,
			'id'=>1,
			'blockNeedId'=>null,
			'items'=>array(
				'disabled'=>array(
					'htmlOptions'=>array('id'=>'yw0-1-disabled', 'class'=>'nl-item', 'uncheckValue'=>null, 'value'=>10),
					'itemType'=>NestedList::ITEM_TYPE_DEF,
					'itemValue'=>10,
					'itemChecked'=>null,
					'itemLabel'=>'Отключен',
					'widgetName'=>null,
					'widgetParams'=>null,
					'id'=>'disabled',
					'itemForm'=>null,
					'itemModel'=>null,
					'itemName'=>'yw0-1',
					'itemNeedId'=>null,
					'_fullness'=>Swamper::FULLNESS_NAME
				),
				'upper'=>array(
					'htmlOptions'=>array('id'=>'yw0-1-upper', 'class'=>'nl-item', 'uncheckValue'=>null, 'value'=>20),
					'itemType'=>NestedList::ITEM_TYPE_DEF,
					'itemValue'=>20,
					'itemChecked'=>null,
					'itemLabel'=>'Верхний',
					'widgetName'=>null,
					'widgetParams'=>null,
					'id'=>'upper',
					'itemForm'=>null,
					'itemModel'=>null,
					'itemName'=>'yw0-1',
					'itemNeedId'=>null,
					'_fullness'=>Swamper::FULLNESS_NAME
				),
				'lower'=>array(
					'htmlOptions'=>array('id'=>'yw0-1-lower', 'class'=>'nl-item', 'uncheckValue'=>null, 'value'=>30),
					'itemType'=>NestedList::ITEM_TYPE_DEF,
					'itemValue'=>30,
					'itemChecked'=>null,
					'itemLabel'=>'Нижний',
					'widgetName'=>null,
					'widgetParams'=>null,
					'id'=>'lower',
					'itemForm'=>null,
					'itemModel'=>null,
					'itemName'=>'yw0-1',
					'itemNeedId'=>null,
					'_fullness'=>Swamper::FULLNESS_NAME
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 4 - Применение модели данных
self::assertEquals($class4->run(), array(
	'htmlOptions'=>array('id'=>'yw0', 'class'=>'nested-list'),
	'id'=>'yw0',
	'blocks'=>array(
		1=>array(
			'htmlOptions'=>array('class'=>'nl-block'),
			'blockConformity'=>'scheme_stack_type',
			'blockConformityExceptions'=>null,
			'id'=>1,
			'blockNeedId'=>null,
			'items'=>array(
				'disabled'=>array(
					'htmlOptions'=>array('id'=>'yw0-1-disabled', 'class'=>'nl-item', 'uncheckValue'=>null, 'value'=>10),
					'itemType'=>'checkbox',
					'itemValue'=>10,
					'itemChecked'=>null,
					'itemLabel'=>'Отключен',
					'widgetName'=>null,
					'widgetParams'=>null,
					'id'=>'disabled',
					'itemForm'=>null,
					'itemModel'=>$model1,
					'itemName'=>'field2[]',
					'itemNeedId'=>null,
					'_fullness'=>Swamper::FULLNESS_MODEL
				),
				'upper'=>array(
					'htmlOptions'=>array('id'=>'yw0-1-upper', 'class'=>'nl-item', 'uncheckValue'=>null, 'value'=>20),
					'itemType'=>'checkbox',
					'itemValue'=>20,
					'itemChecked'=>null,
					'itemLabel'=>'Верхний',
					'widgetName'=>null,
					'widgetParams'=>null,
					'id'=>'upper',
					'itemForm'=>null,
					'itemModel'=>$model1,
					'itemName'=>'field2[]',
					'itemNeedId'=>null,
					'_fullness'=>Swamper::FULLNESS_MODEL
				),
				'lower'=>array(
					'htmlOptions'=>array('id'=>'yw0-1-lower', 'class'=>'nl-item', 'uncheckValue'=>null, 'value'=>30),
					'itemType'=>'checkbox',
					'itemValue'=>30,
					'itemChecked'=>null,
					'itemLabel'=>'Нижний',
					'widgetName'=>null,
					'widgetParams'=>null,
					'id'=>'lower',
					'itemForm'=>null,
					'itemModel'=>$model1,
					'itemName'=>'field2[]',
					'itemNeedId'=>null,
					'_fullness'=>Swamper::FULLNESS_MODEL
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 5 - Применение исключения для блока соответствий
self::assertEquals($class5->run(), array(
	'htmlOptions'=>array('id'=>'yw0', 'class'=>'nested-list'),
	'id'=>'yw0',
	'blocks'=>array(
		1=>array(
			'htmlOptions'=>array('class'=>'nl-block'),
			'blockConformity'=>'scheme_stack_type',
			'blockConformityExceptions'=>array('upper'=>'$params[0]["itemName"] === "field2"'),
			'id'=>1,
			'blockNeedId'=>null,
			'items'=>array(
				'disabled'=>array(
					'htmlOptions'=>array('id'=>'yw0-1-disabled', 'class'=>'nl-item', 'uncheckValue'=>null, 'value'=>10),
					'itemType'=>'checkbox',
					'itemValue'=>10,
					'itemChecked'=>null,
					'itemLabel'=>'Отключен',
					'widgetName'=>null,
					'widgetParams'=>null,
					'id'=>'disabled',
					'itemForm'=>null,
					'itemModel'=>$model1,
					'itemName'=>'field2[]',
					'itemNeedId'=>null,
					'_fullness'=>Swamper::FULLNESS_MODEL
				),
				'lower'=>array(
					'htmlOptions'=>array('id'=>'yw0-1-lower', 'class'=>'nl-item', 'uncheckValue'=>null, 'value'=>30),
					'itemType'=>'checkbox',
					'itemValue'=>30,
					'itemChecked'=>null,
					'itemLabel'=>'Нижний',
					'widgetName'=>null,
					'widgetParams'=>null,
					'id'=>'lower',
					'itemForm'=>null,
					'itemModel'=>$model1,
					'itemName'=>'field2[]',
					'itemNeedId'=>null,
					'_fullness'=>Swamper::FULLNESS_MODEL
				)
			)
		)
	)
));
Widget::resetCounter();

//Тест 6 - Применение двойного исключения для блока соответствий
self::assertEquals($class6->run(), array(
	'htmlOptions'=>array('id'=>'yw0', 'class'=>'nested-list'),
	'id'=>'yw0',
	'blocks'=>array(
		1=>array(
			'htmlOptions'=>array('class'=>'nl-block'),
			'blockConformity'=>'scheme_stack_type',
			'blockConformityExceptions'=>array('upper, lower'=>'$params[0]["itemName"] === "field2"'),
			'id'=>1,
			'blockNeedId'=>null,
			'items'=>array(
				'disabled'=>array(
					'htmlOptions'=>array('id'=>'yw0-1-disabled', 'class'=>'nl-item', 'uncheckValue'=>null, 'value'=>10),
					'itemType'=>'checkbox',
					'itemValue'=>10,
					'itemChecked'=>null,
					'itemLabel'=>'Отключен',
					'widgetName'=>null,
					'widgetParams'=>null,
					'id'=>'disabled',
					'itemForm'=>null,
					'itemModel'=>$model1,
					'itemName'=>'field2',
					'itemNeedId'=>null,
					'_fullness'=>Swamper::FULLNESS_MODEL
				),
			)
		)
	)
));
Widget::resetCounter();

//Тест 7 - Вложенный список
self::assertEquals($class7->run(), array(
	'htmlOptions'=>array('id'=>'yw0', 'class'=>'nested-list'),
	'id'=>'yw0',
	'blocks'=>array(
		1=>array(
			'htmlOptions'=>array('class'=>'nl-block'),
			'blockConformity'=>'scheme_stack_type',
			'blockConformityExceptions'=>null,
			'id'=>1,
			'blockNeedId'=>null,
			'items'=>array(
				'disabled'=>array(
					'htmlOptions'=>array('id'=>'yw0-1-disabled', 'class'=>'nl-item', 'uncheckValue'=>null, 'value'=>10),
					'itemType'=>'checkbox',
					'itemValue'=>10,
					'itemChecked'=>null,
					'itemLabel'=>'Отключен',
					'widgetName'=>null,
					'widgetParams'=>null,
					'id'=>'disabled',
					'itemForm'=>null,
					'itemModel'=>$model1,
					'itemName'=>'field2[]',
					'itemNeedId'=>null,
					'_fullness'=>Swamper::FULLNESS_MODEL
				),
				'upper'=>array(
					'htmlOptions'=>array('id'=>'yw0-1-upper', 'class'=>'nl-item', 'uncheckValue'=>null, 'value'=>20),
					'itemType'=>'checkbox',
					'itemValue'=>20,
					'itemChecked'=>null,
					'itemLabel'=>'Верхний',
					'widgetName'=>null,
					'widgetParams'=>null,
					'id'=>'upper',
					'itemForm'=>null,
					'itemModel'=>$model1,
					'itemName'=>'field2[]',
					'itemNeedId'=>null,
					'_fullness'=>Swamper::FULLNESS_MODEL,
					'blocks'=>array(
						1=>array(
							'htmlOptions'=>array('class'=>'nl-block'),
							'blockConformity'=>'event_fix',
							'blockConformityExceptions'=>null,
							'id'=>1,
							'blockNeedId'=>null,
							'items'=>array(
								'disabled'=>array(
									'htmlOptions'=>array('id'=>'yw0-1-upper-1-disabled', 'class'=>'nl-item',
									'uncheckValue'=>null, 'value'=>10),
									'itemType'=>NestedList::ITEM_TYPE_DEF,
									'itemValue'=>10,
									'itemChecked'=>null,
									'itemLabel'=>'Отключена',
									'widgetName'=>null,
									'widgetParams'=>null,
									'id'=>'disabled',
									'itemForm'=>null,
									'itemModel'=>$model1,
									'itemName'=>'field1',
									'itemNeedId'=>null,
									'_fullness'=>Swamper::FULLNESS_MODEL
								),
								'normal'=>array(
									'htmlOptions'=>array('id'=>'yw0-1-upper-1-normal', 'class'=>'nl-item',
									'uncheckValue'=>null, 'value'=>20),
									'itemType'=>NestedList::ITEM_TYPE_DEF,
									'itemValue'=>20,
									'itemChecked'=>null,
									'itemLabel'=>'По ошибке',
									'widgetName'=>null,
									'widgetParams'=>null,
									'id'=>'normal',
									'itemForm'=>null,
									'itemModel'=>$model1,
									'itemName'=>'field1',
									'itemNeedId'=>null,
									'_fullness'=>Swamper::FULLNESS_MODEL,
								),
								'required'=>array(
									'htmlOptions'=>array('id'=>'yw0-1-upper-1-required', 'class'=>'nl-item',
									'uncheckValue'=>null, 'value'=>30),
									'itemType'=>NestedList::ITEM_TYPE_DEF,
									'itemValue'=>30,
									'itemChecked'=>null,
									'itemLabel'=>'Обязательная',
									'widgetName'=>null,
									'widgetParams'=>null,
									'id'=>'required',
									'itemForm'=>null,
									'itemModel'=>$model1,
									'itemName'=>'field1',
									'itemNeedId'=>null,
									'_fullness'=>Swamper::FULLNESS_MODEL
								)
							)
						)
					)
				),
				'lower'=>array(
					'htmlOptions'=>array('id'=>'yw0-1-lower', 'class'=>'nl-item', 'uncheckValue'=>null, 'value'=>30),
					'itemType'=>'checkbox',
					'itemValue'=>30,
					'itemChecked'=>null,
					'itemLabel'=>'Нижний',
					'widgetName'=>null,
					'widgetParams'=>null,
					'id'=>'lower',
					'itemForm'=>null,
					'itemModel'=>$model1,
					'itemName'=>'field2[]',
					'itemNeedId'=>null,
					'_fullness'=>Swamper::FULLNESS_MODEL
				)
			)
		)
	)
));
Widget::resetCounter();
