<?php
$array1 = array('param1', 'param2', 'param3');
$array2 = array('param1', 'group1'=>'param2', 'param3');
$array3 = array('param1', 'group1'=>array('param21', 'param22'), 'param3');
$array4 = array('param1', 'group1'=>array('param21', 'param22'), 'param3', 'group2'=>array('param41', 'param42'));

//Тест 1 - Без группировки
self::assertEquals(Arrays::extensionNestedParams($array1), array(
	$array1,
	array()
));

//Тест 2 - С группировкой без упаковки в массив
self::assertEquals(Arrays::extensionNestedParams($array2), array(
	array('param1', 'param2', 'param3'),
	array('group1'=>array('param2'))
));

//Тест 3 - Стандартная группировка
self::assertEquals(Arrays::extensionNestedParams($array3), array(
	array('param1', 'param21', 'param22', 'param3'),
	array('group1'=>array('param21', 'param22'))
));

//Тест 4 - Группировка по двум группам 
self::assertEquals(Arrays::extensionNestedParams($array4), array(
	array('param1', 'param21', 'param22', 'param3', 'param41', 'param42'),
	array('group1'=>array('param21', 'param22'), 'group2'=>array('param41', 'param42'))
));
