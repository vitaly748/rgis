<?php
$array1 = array('name1'=>1, 'name2'=>2, 'name3'=>3);
$array2 = array('name1'=>1, 'name2, name3'=>2, 'name4'=>4);
$array3 = array('name1, name2'=>1, 'name3'=>3, 'name4, name5, name6'=>4);

//Тест 1 - Без расширения
self::assertEquals(Arrays::extensionParams($array1), $array1);

//Тест 2 - Простое расширение
self::assertEquals(Arrays::extensionParams($array2), array(
	'name1'=>1,
	'name2'=>2,
	'name3'=>2,
	'name4'=>4
));

//Тест 3 - Двойное расширение
self::assertEquals(Arrays::extensionParams($array3), array(
	'name1'=>1,
	'name2'=>1,
	'name3'=>3,
	'name4'=>4,
	'name5'=>4,
	'name6'=>4
));
