<?php
class PreparationChildsTest1 extends TreeWidget{
	
	public $blocks = 2;
	
	protected $levels = array(
		'root'=>array(
			'form', 'model', 'name',
			TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions', 'rootName')
		),
		'blocks'=>array(
			'value1', 'value2',
			TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions', 'blockName')
		)
	);
	
}

class PreparationChildsTest2 extends PreparationChildsTest1{

	public $blocks = array(
		'element1'=>array(
			'htmlOptions'=>array('id'=>2),
			'value1'=>33
		),
		'element2'=>array(
			'htmlOptions'=>array('class'=>'class-3'),
			'value1'=>44
		)
	);

}

class PreparationChildsTest3 extends PreparationChildsTest2{

	const CLASS_BLOCKS = 'block';
	
	const BLOCK_NAME_BLOCKS_DEF = 'block-name';

}

$class1 = new PreparationChildsTest1;
$class2 = new PreparationChildsTest2;
$class3 = new PreparationChildsTest3;

$params1 = array('value1'=>'11', 'value2'=>'22');
$params2 = array_merge($params1, array('blocks'=>1));
$params3 = array_merge($params1, array('blocks'=>array('root.element1')));
$params4 = array_merge($params1, array('blocks'=>array(
	'root.element1'=>array(
		'htmlOptions'=>array('id'=>1, 'class'=>'class-1')
	)
)));
$params5 = array_merge_recursive($params4, array('blocks'=>array(TreeWidget::PARAM_ITEMS_INHERIT_DATA=>2)));
$params6 = array_merge($params1, array('blocks'=>array(
	TreeWidget::PARAM_ITEMS_QU=>1,
	'root.element1'=>array(
		'htmlOptions'=>array('id'=>1, 'class'=>'class-1')
	)
)));
$params7 = array_merge_recursive($params3, array('blocks'=>array(TreeWidget::PARAM_ITEMS_INHERIT_DATA=>2)));

//Тест 1 - Простой пример
self::assertEquals($class1->preparationChilds('root', $params1, ''), array(
	'blocks'=>array(
		1=>array(
			'htmlOptions'=>null,
			'blockName'=>null,
			'id'=>1
		),
		2=>array(
			'htmlOptions'=>null,
			'blockName'=>null,
			'id'=>2
		)
	)
));

//Тест 2 - Ограничение количества элементов
self::assertEquals($class1->preparationChilds('root', $params2, ''), array(
	'blocks'=>array(
		1=>array(
			'htmlOptions'=>null,
			'blockName'=>null,
			'id'=>1
		)
	)
));

//Тест 3 - Установка имени элементу с неправильным префиксом
self::assertEquals($class1->preparationChilds('root', $params3, ''), array(
	'blocks'=>array(
		1=>array(
			'htmlOptions'=>null,
			'blockName'=>null,
			'id'=>1
		),
		2=>array(
			'htmlOptions'=>null,
			'blockName'=>null,
			'id'=>2
		)
	)
));

//Тест 4 - Установка имени элементу с правильным префиксом
self::assertEquals($class1->preparationChilds('root', $params3, 'root'), array(
	'blocks'=>array(
		'element1'=>array(
			'htmlOptions'=>null,
			'blockName'=>null,
			'id'=>'element1'
		),
		1=>array(
			'htmlOptions'=>null,
			'blockName'=>null,
			'id'=>1
		)
	)
));

//Тест 5 - Установка имени элементу с более длинным правильным префиксом, с
//установкой параметра htmlOptions
self::assertEquals($class1->preparationChilds('root', $params4, 'q.root'), array(
	'blocks'=>array(
		'element1'=>array(
			'htmlOptions'=>array('id'=>1, 'class'=>'class-1'),
			'blockName'=>null,
			'id'=>'element1'
		),
		1=>array(
			'htmlOptions'=>null,
			'blockName'=>null,
			'id'=>1
		)
	)
));

//Тест 6 - Установка имени элементу с более длинным правильным префиксом, с ограничением числа элементов и с
//установкой параметра htmlOptions
self::assertEquals($class1->preparationChilds('root', $params6, 'qq.w.root'), array(
	'blocks'=>array(
		'element1'=>array(
			'htmlOptions'=>array('id'=>1, 'class'=>'class-1'),
			'blockName'=>null,
			'id'=>'element1'
		)
	)
));

//Тест 7 - Переопределение параметра htmlOptions
self::assertEquals($class2->preparationChilds('root', $params4, 'root'), array(
	'blocks'=>array(
		'element1'=>array(
			'htmlOptions'=>array('id'=>1, 'class'=>'class-1'),
			'blockName'=>null,
			'id'=>'element1'
		)
	)
));

//Тест 8 - Применение параметра inherit_data
self::assertEquals($class2->preparationChilds('root', $params5, 'root'), array(
	'blocks'=>array(
		'element1'=>array(
			'htmlOptions'=>array('id'=>1, 'class'=>'class-1'),
			'blockName'=>null,
			'id'=>'element1'
		),
		'element2'=>array(
			'htmlOptions'=>array('class'=>'class-3'),
			'blockName'=>null,
			'id'=>'element2'
		)
	)
));

//Тест 9 - Переопределение параметра htmlOptions с ограничением числа элементов
self::assertEquals($class2->preparationChilds('root', $params6, 'root'), array(
	'blocks'=>array(
		'element1'=>array(
			'htmlOptions'=>array('id'=>1, 'class'=>'class-1'),
			'blockName'=>null,
			'id'=>'element1'
		)
	)
));

//Тест 10 - Смешивание HTML-опций
self::assertEquals($class3->preparationChilds('root', $params7, 'root'), array(
	'blocks'=>array(
		'element1'=>array(
			'htmlOptions'=>array('id'=>2, 'class'=>'block'),
			'blockName'=>'block-name',
			'id'=>'element1'
		),
		'element2'=>array(
			'htmlOptions'=>array('class'=>'class-3 block'),
			'blockName'=>'block-name',
			'id'=>'element2'
		)
	)
));
