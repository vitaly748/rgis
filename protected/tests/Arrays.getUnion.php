<?php
$name1 = 'test';
$name2 = 'common';
$name3 = 'parma';
$name4 = 'htmlOptions';
$name5 = 'common.parma';
$name6 = 'common.parma.htmlOptions';
$name7 = 'common.test';
$name8 = 'common.parma.test';
$name9 = 'common.parma.htmlOptions.test';
$name10 = 'common.htmlOptions';
$name11 = 'common.test.htmlOptions';
$name12 = 'common.test.htmlOptions.parma';
$name13 = 'common.htmlOptions.test.parma';

$array1 = array(
	'common.parma.htmlOptions.*'=>26,
	'common.parma.htmlOptions'=>25,
	'common.parma.*'=>24,
	'common.parma'=>23,
	'common.*'=>22,
	'common'=>21
);
$array2 = array_merge($array1, array(
	'*'=>20
));
$array3 = array_merge(array(
	'common.htmlOptions.*.parma'=>29,
	'common.*.htmlOptions.parma'=>28,
	'common.*.htmlOptions'=>27
), $array2);

//Тест 1
self::assertEquals(Arrays::getUnion($name1, $array1), Arrays::MERGE_PARAMS_UNION_LAST);

//Тест 2
self::assertEquals(Arrays::getUnion($name1, $array2), 20);

//Тест 3
self::assertEquals(Arrays::getUnion($name2, $array2), 21);

//Тест 4
self::assertEquals(Arrays::getUnion($name3, $array2), 20);

//Тест 5
self::assertEquals(Arrays::getUnion($name4, $array2), 20);

//Тест 6
self::assertEquals(Arrays::getUnion($name5, $array2), 23);

//Тест 7
self::assertEquals(Arrays::getUnion($name6, $array2), 25);

//Тест 8
self::assertEquals(Arrays::getUnion($name7, $array2), 22);

//Тест 9
self::assertEquals(Arrays::getUnion($name8, $array2), 24);

//Тест 10
self::assertEquals(Arrays::getUnion($name9, $array2), 26);

//Тест 11
self::assertEquals(Arrays::getUnion($name10, $array2), 22);

//Тест 12
self::assertEquals(Arrays::getUnion($name10, $array3), 22);

//Тест 13
self::assertEquals(Arrays::getUnion($name11, $array3), 27);

//Тест 14
self::assertEquals(Arrays::getUnion($name12, $array3), 28);

//Тест 15
self::assertEquals(Arrays::getUnion($name13, $array3), 29);
