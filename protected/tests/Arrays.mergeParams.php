<?php
class MergeParamsTest1{
	
	const MERGE_PARAMS_RULES = array('param2'=>Arrays::MERGE_PARAMS_UNION_FIRST);
	
}

class MergeParamsTest2{
	
	const PARAM_X_DEF = 'x';
	
	const PARAM_X_MODEL_DEF = array(1, 2, 3);
	
}

class MergeParamsTest3{
	
	const MERGE_PARAMS_RULES = array('param1'=>Arrays::MERGE_PARAMS_UNION_FIRST);
	
	public $param1 = 'qq';
	
	public $paramX = 5;
	
}

class MergeParamsTest4{
	
	public $htmlOptions = array('id'=>1, 'class'=>'class-1');
	
}

class MergeParamsTest5 extends TreeWidget{
	
	const FORM_DEF = 'form';
	
	const MODEL_DEF = 'model';
	
	const NAME_DEF = 'name';
	
}

class MergeParamsTest6 extends TreeWidget{
	
	const FORM_DEF = 'form';
	
	const MODEL_DEF = 'model';
	
	const CRYSTAL_DEF = 'crystal';
	
	public $htmlOptions = array('id'=>1, 'class'=>'class-1');
	
}

class MergeParamsTest7{
}

class MergeParamsTest8{
	
	public static function model(){
		return new MergeParamsTest8;
	}
	
}
$names1 = array('param1', 'param2', 'param3');
$names2 = array_merge($names1, array('paramX'));
$names3 = array_merge($names1, array('unique'=>array('param4')));
$names4 = array_merge($names1, array('htmlOptions'));
$names5 = array(TreeWidget::PARAM_TYPE_TRANS=>array('form', 'model'),
	TreeWidget::PARAM_TYPE_LIMIT=>'name', TreeWidget::PARAM_TYPE_FIX=>'crystal');
$names6 = array('form', 'model', TreeWidget::PARAM_TYPE_LIMIT=>array('name', 'crystal'));
$names7 = array_merge($names6, array('htmlOptions'));
$names8 = array('form', 'model', 'name');

$params1 = array('param1'=>'q', 'param2'=>'w');
$params2 = array('param2'=>'e', 'param3'=>10, 'param4'=>'r');
$params3 = array_merge($params1, array('htmlOptions'=>array('id'=>2, 'class'=>'class-2')));
$params4 = array_merge($params2, array('htmlOptions'=>array('id'=>3, 'class'=>'class-3')));
$params5 = array('form'=>'form', 'model'=>'MergeParamsTest7', 'name'=>'name');
$params6 = array_merge($params5, array('model'=>'MergeParamsTest8'));
$params7 = array_merge($params5, array('model'=>'MergeParamsTestX'));

$class1 = new MergeParamsTest7;
$class2 = MergeParamsTest8::model();

//Тест 1 - Стандартное объединение параметров
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names1,
	'sources'=>array($params1, $params2)
)), array(
	'param1'=>'q',
	'param2'=>'e',
	'param3'=>10
));

//Тест 2 - Включение параметра id
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names1,
	'sources'=>array($params1, $params2),
	'id'=>true
)), array(
	'param1'=>'q',
	'param2'=>'e',
	'param3'=>10,
	'id'=>null
));

//Тест 3 - Применение параметра union_def
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names1,
	'sources'=>array($params1, $params2),
	'union_def'=>Arrays::MERGE_PARAMS_UNION_SEPARATOR
)), array(
	'param1'=>'q',
	'param2'=>'w e',
	'param3'=>'10'
));

//Тест 4 - Автоматическое определение списка параметров
self::assertEquals(Arrays::mergeParams(array(
	'sources'=>array($params1, $params2)
)), array(
	'param1'=>'q',
	'param2'=>'e',
	'param3'=>10,
	'param4'=>'r'
));

//Тест 5 - Изменение схемы объединения для параметра param2
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names1,
	'sources'=>array($params1, $params2),
	'class'=>new MergeParamsTest1
)), array(
	'param1'=>'q',
	'param2'=>'w',
	'param3'=>10
));

//Тест 6 - Изменение схемы объединения для параметра param2, класс задаётся названием
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names1,
	'sources'=>array($params1, $params2),
	'class'=>'MergeParamsTest1'
)), array(
	'param1'=>'q',
	'param2'=>'w',
	'param3'=>10
));

//Тест 7 - Изменение схемы объединения для параметра param2, класс задаётся неправильным названием
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names1,
	'sources'=>array($params1, $params2),
	'class'=>'MergeParamsTestX'
)), array(
	'param1'=>'q',
	'param2'=>'e',
	'param3'=>10
));

//Тест 8 - Изменение схемы объединения для параметра param2 посредством параметра rules
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names1,
	'sources'=>array($params1, $params2),
	'rules'=>array('param2'=>Arrays::MERGE_PARAMS_UNION_FIRST)
)), array(
	'param1'=>'q',
	'param2'=>'w',
	'param3'=>10
));

//Тест 9 - Использование не заданного параметра
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names2,
	'sources'=>array($params1, $params2),
)), array(
	'param1'=>'q',
	'param2'=>'e',
	'param3'=>10,
	'paramX'=>null
));

//Тест 10 - Использование параметра по умолчанию c неправильно заданным классом
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names2,
	'sources'=>array($params1, $params2),
	'class'=>new MergeParamsTest1
)), array(
	'param1'=>'q',
	'param2'=>'w',
	'param3'=>10,
	'paramX'=>null
));

//Тест 11 - Использование параметра по умолчанию
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names2,
	'sources'=>array($params1, $params2),
	'class'=>new MergeParamsTest2
)), array(
	'param1'=>'q',
	'param2'=>'e',
	'param3'=>10,
	'paramX'=>MergeParamsTest2::PARAM_X_DEF
));

//Тест 12 - Использование параметра по умолчанию c суффиксом
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names2,
	'sources'=>array($params1, $params2),
	'class'=>new MergeParamsTest2,
	'suffix'=>'model'
)), array(
	'param1'=>'q',
	'param2'=>'e',
	'param3'=>10,
	'paramX'=>MergeParamsTest2::PARAM_X_MODEL_DEF
));

//Тест 13 - Применение параметра this
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names2,
	'sources'=>array($params1, $params2),
	'class'=>$class = new MergeParamsTest3,
	'this'=>true
)), array(
	'param1'=>$class->param1,
	'param2'=>'e',
	'param3'=>10,
	'paramX'=>$class->paramX
));

//Тест 14 - Применение параметра inherit
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names1,
	'sources'=>array($params1, $params2),
	'inherit'=>true
)), array(
	'param1'=>'q',
	'param2'=>'e',
	'param3'=>10,
	'param4'=>'r'
));

//Тест 15 - Применение вложенного списка названий параметров
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names3,
	'sources'=>array($params1, $params2)
)), array(
	array(
		'param1'=>'q',
		'param2'=>'e',
		'param3'=>10,
		'param4'=>'r'
	),
	'unique'=>array(
		'param4'=>'r'
	)
));

//Тест 16 - Применение рекурсивной схемы объединения параметров
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names4,
	'sources'=>array($params3, $params4)
)), array(
	'param1'=>'q',
	'param2'=>'e',
	'param3'=>10,
	'htmlOptions'=>array(
		'id'=>3,
		'class'=>'class-2 class-3'
	)
));

//Тест 17 - Применение рекурсивной схемы объединения параметров с переопределением правила htmlOptions
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names4,
	'sources'=>array($params3, $params4),
	'rules'=>array('htmlOptions'=>Arrays::MERGE_PARAMS_UNION_FIRST)
)), array(
	'param1'=>'q',
	'param2'=>'e',
	'param3'=>10,
	'htmlOptions'=>array(
		'id'=>2,
		'class'=>'class-2'
	)
));

//Тест 18 - Применение рекурсивной схемы объединения параметров с переопределением правила class
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names4,
	'sources'=>array($params3, $params4),
	'rules'=>array('class'=>Arrays::MERGE_PARAMS_UNION_FIRST)
)), array(
	'param1'=>'q',
	'param2'=>'e',
	'param3'=>10,
	'htmlOptions'=>array(
		'id'=>3,
		'class'=>'class-2'
	)
));

//Тест 19 - Применение рекурсивной схемы объединения параметров с участием класса
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names4,
	'sources'=>array($params3, $params4),
	'class'=>new MergeParamsTest4,
	'this'=>true
)), array(
	'param1'=>'q',
	'param2'=>'e',
	'param3'=>10,
	'htmlOptions'=>array(
		'id'=>3,
		'class'=>'class-1 class-2 class-3'
	)
));

//Тест 20 - Использование класса TreeWidget
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names5,
	'class'=>new MergeParamsTest5,
	'model'=>false
)), array(
	array(
		'form'=>'form',
		'model'=>'model',
		'name'=>'name',
		'id'=>null,
		'crystal'=>null
	),
	TreeWidget::PARAM_TYPE_FIX=>array(
		'name'=>'name',
		'id'=>null,
		'crystal'=>null
	),
	TreeWidget::PARAM_TYPE_TRANS=>array(
		'form'=>'form',
		'model'=>'model',
		'crystal'=>null
	)
));

//Тест 21 - Использование класса TreeWidget
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names6,
	'class'=>new MergeParamsTest5,
	'model'=>false
)), array(
	array(
		'form'=>'form',
		'model'=>'model',
		'name'=>'name',
		'crystal'=>null,
		'id'=>null
	),
	TreeWidget::PARAM_TYPE_FIX=>array(
		'name'=>'name',
		'crystal'=>null,
		'id'=>null
	),
	TreeWidget::PARAM_TYPE_TRANS=>array(
		'form'=>'form',
		'model'=>'model'
	)
));

//Тест 22 - Использование класса TreeWidget c применением рекурсивной схемы объединения параметров
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names7,
	'class'=>new MergeParamsTest6,
	'sources'=>array($params3, $params4),
	'this'=>true,
	'model'=>false
)), array(
	array(
		'form'=>'form',
		'model'=>'model',
		'name'=>null,
		'crystal'=>'crystal',
		'id'=>null,
		'htmlOptions'=>array(
			'id'=>3,
			'class'=>'class-1 class-2 class-3'
		)
	),
	TreeWidget::PARAM_TYPE_FIX=>array(
		'name'=>null,
		'crystal'=>'crystal',
		'id'=>null
	),
	TreeWidget::PARAM_TYPE_TRANS=>array(
		'form'=>'form',
		'model'=>'model',
		'htmlOptions'=>array(
			'id'=>3,
			'class'=>'class-1 class-2 class-3'
		)
	)
));

//Тест 23 - Использование параметра model
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names8,
	'sources'=>array($params1, $params5),
	'model'=>true
)), array(
	'form'=>'form',
	'model'=>$class1,
	'name'=>'name'
), false);

//Тест 24 - Использование параметра model, при этом класс модели содержит метов model()
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names8,
	'sources'=>array($params1, $params6),
	'model'=>true
)), array(
	'form'=>'form',
	'model'=>$class2,
	'name'=>'name'
), false);

//Тест 25 - Использование параметра model с неверно заданным именем класса
self::assertEquals(Arrays::mergeParams(array(
	'names'=>$names8,
	'sources'=>array($params1, $params7),
	'model'=>true
)), array(
	'form'=>'form',
	'model'=>false,
	'name'=>'name'
), false);
