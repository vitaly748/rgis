<?php
Yii::app()->asset->add(Arrays::forming(array('button', 'input', 'gridview',
	':fias'=>array(Arrays::FORMING_PARAMS_ENABLED=>Yii::app()->fias->activate()))));

$this->widget(Widget::alias('Flashes'),
	array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

if ($operator){
	echo Html::openTag('div', array('class'=>'filter'));
		
		echo Html::openTag('div', array('class'=>'filter-item'));
			echo Html::label('Адрес', 'filter-address');
			echo Html::hiddenField('filter-addressid', $house ? $house['id'] : '', array('class'=>'filter-field'));
			echo Html::textField('filter-address', $house ? $house['address'] : '',
				array('class'=>'fias-cleaning', 'style'=>'width: 400px;'));
		echo Html::closeTag('div');
		
		echo Html::openTag('div', array('class'=>'filter-buttons'));
			echo Html::link('', '#', array('class'=>'button search contur circle'));
			echo Html::link('', '#', array('class'=>'button clear contur circle'));
		echo Html::closeTag('div');
		
	echo Html::closeTag('div');
}

echo Html::openTag('div', array('class'=>'container-grid-view'));
	$this->renderPartial('generalGrid', array('model'=>$model));
	echo Html::tag('div', array('class'=>'curtain-loading'), '');
echo Html::closeTag('div');

$this->widget(Widget::alias('ButtonsTable'), array('params'=>array(
	'premise'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1)))));
