<?php
$items = Html::tag('div', array('class'=>'gv-table'), '{items}');

foreach (array('address', 'credited', 'paid', 'collectibility') as $name)
	$columns[] = array(
		'header'=>$model->getAttributeLabel($name),
		'name'=>$name,
		'headerHtmlOptions'=>array('class'=>'nowrap'),
		'htmlOptions'=>Html::forming(array('nowrap'=>$name != 'address', 'ta-center'=>$name != 'address'))
	);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'houses-grid',
	'dataProvider'=>$model->search(),
	'filterSelector'=>'input.filter-field',
	'ajaxType'=>'POST',
	'template'=>$items,
	'cssFile'=>false,
	'beforeAjaxUpdate'=>'gridviewBeforeAjaxUpdate',
	'afterAjaxUpdate'=>'gridviewAfterAjaxUpdate',
	'ajaxUpdateError'=>'gridviewErrorAjaxUpdate',
	'emptyText'=>Yii::app()->label->get(Yii::app()->request->isAjaxRequest ? 'wgv_empty' : 'wgv_loading'),
	'columns'=>$columns
));
