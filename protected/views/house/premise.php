<?php
Yii::app()->asset->add(array('gridview'));

$this->widget(Widget::alias('Flashes'),
	array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

echo Html::openTag('div', array('class'=>'filter'));
	
	echo Html::openTag('div', array('class'=>'filter-item fi-not-clean'));
		echo Html::label('Адрес', Widget::PREFIX_ID.'1');
		echo Html::hiddenField('filter-premise-house', $id, array('class'=>'filter-field'));
		$this->widget(Widget::alias('DropDownList'), array(
			'data'=>$id_houses ? $id_houses : array('empty'=>array('access'=>'view')),
			'name'=>'filter-house',
			'value'=>$id,
			'widthInitial'=>true,
			'readonly'=>true
		));
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'filter-buttons'));
		echo Html::link('', '#', array('class'=>'button search contur circle'));
	echo Html::closeTag('div');
	
echo Html::closeTag('div');

echo Html::openTag('div', array('class'=>'container-grid-view'));
	$this->renderPartial('premiseGrid', array('model'=>$model));
	echo Html::tag('div', array('class'=>'curtain-loading'), '');
echo Html::closeTag('div');
