<?php
Yii::app()->asset->add(array('gridview'));

$this->widget(Widget::alias('Flashes'),
	array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

echo Html::openTag('div', array('class'=>'container-grid-view'));
	$this->renderPartial('indexGrid', array('model'=>$model));
	echo Html::tag('div', array('class'=>'curtain-loading'), '');
echo Html::closeTag('div');

$this->widget(Widget::alias('ButtonsTable'), array('params'=>array(
	'create',
	'restore'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled '.
		Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm1', 'tabindex'=>-1)),
	'delete'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled '.
		Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm2', 'tabindex'=>-1))
)));

$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm', 'htmlOptions'=>array('id'=>'juid-confirm1'),
	'buttonOk'=>'yes', 'buttonCancel'=>'no', 'params'=>array('label'=>'reserve_restore')));

$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm', 'htmlOptions'=>array('id'=>'juid-confirm2'),
	'buttonOk'=>'yes', 'buttonCancel'=>'no'));
