migrateTextSuccess = 'Готово';
migrateTextError = 'Ошибка';
migrateTextRetryMax = 'Зависание';
migrateRetry = migrateRetryMax = 5;

function migrateCheck(){
	$.ajax({
		cache: false,
		complete: function(jsXHR, textStatus){
			if (textStatus !== 'success')
				migrateState.html(migrateTextError);
			else if (jsXHR.responseText === 'end')
				migrateState.html(migrateTextSuccess);
			else{
				var retry = true,
					value = parseInt(jsXHR.responseText);
				
				if (isNaN(value)){
					migrateState.html(migrateTextError + ': ' + jsXHR.responseText);
					retry = false;
				}else if (jsXHR.responseText === migrateCount){
					migrateRetry--;
					
					if (!migrateRetry){
						migrateState.html(migrateTextRetryMax);
						retry = false;
					}
				}else{
					migrateCount = jsXHR.responseText;
					migrateSpan.html(migrateCount);
					migrateRetry = migrateRetryMax;
				}
				
				if (retry)
					migrateCheck();
			}
		}
	});
}

$(window).load(function(){
	migrateDiv = $('div#migrate'),
	migrateSpan = migrateDiv.find('div.count span'),
	migrateState = migrateDiv.children('div.state');
	
	if (migrateSpan.length && migrateState.length){
		migrateCount = migrateSpan.html();
		
		migrateCheck();
	}
});
