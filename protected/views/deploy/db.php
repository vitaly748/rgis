<?php
/*
 * inherit_key(true)	inherit_data(1)	priority(0)	def(0)
 * 										2								1						
 * 										2								2						
 * 																								1
 * 										2														1
 * 										2								2						1
 * 																								2
 * 										2														2
 * 																		1						2
 * 										2								1						2
 * false							2								1						
 * false							2								2						
 * ...
 */
Yii::app()->asset->add(array('button', 'input'));

$form = $this->beginWidget('ActiveForm');
	
	$this->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model,
		'attrSeparation'=>array(
			array('host', 'port'),
			array('name', 'login', 'password')
		),
		'features'=>array('login.2.text, password.2.text'=>array('htmlOptions'=>array('autocomplete'=>'off')),
			TreeWidget::PARAM_ITEMS_DEF=>1)
	));
	
	$this->widget(Widget::alias('ButtonsTable'), array('model'=>$model));
	
$this->endWidget();
