<?php
Yii::app()->asset->add(array('button', 'input'));
$childs = Html::activeIds($model, array_slice($model->attributeNames(), 1));

$form = $this->beginWidget('ActiveForm', array('command'=>true));
	
	$this->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model,
		'attrSeparation'=>array(
			array('on'),
			array('host', 'port', 'protocol'),
			array('name', 'login', 'password', 'asmx')
		),
		'attributes'=>array(
			'on'=>array('cells'=>array(TreeWidget::PARAM_ITEMS_DEF=>2,
				array('htmlOptions'=>array('class'=>'ta-center'), 'features'=>array(
					'checkbox'=>array('htmlOptions'=>array('class'=>'toggle '.Html::CLASS_INPUT_CHILD_DNI.' '.
						Strings::mergeWords($childs, false, Html::PREFIX_INPUT_CHILD.'-'), 'uncheckValue'=>0)))),
				array('features'=>array('label'))
			)),
			'protocol'=>array('features'=>array(
				'2.widget'=>array('widgetName'=>'DropDownList', 'widgetParams'=>array(
					'widthInitial'=>true, 'readonly'=>true)),
				'2.error_static'
			)),
		),
		'features'=>array('login.2.text, password.2.text'=>array('htmlOptions'=>array('autocomplete'=>'off')),
			TreeWidget::PARAM_ITEMS_DEF=>1)
	));
	
	$this->widget(Widget::alias('ButtonsTable'), array('model'=>$model));
	
$this->endWidget();
