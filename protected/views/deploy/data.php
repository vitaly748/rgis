<?php
Yii::app()->asset->add(array('button', 'input'));

$new_data_ids = array();

foreach ($new_data as $name)
	$new_data_ids[] = "nl-$name.1.old";

$new_data_ids = implode(', ', $new_data_ids);

$form = $this->beginWidget('ActiveForm', array('command'=>true));
	
	$this->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model,
		'htmlOptions'=>array('class'=>'unrequired'),
		'id'=>'dt-main',
		'blockBorder'=>true,
		'blockIndentVer'=>false,
		'cells'=>array(array('htmlOptions'=>array('class'=>'va-top pr5'))),
		'attrException'=>array_merge(array('setting', 'generation', 'quantity'), Yii::app()->attribute->confirmAttributeNames),
		'attrSeparation'=>1,
		'blocks'=>array(array('blockBorder'=>false), TreeWidget::PARAM_ITEMS_PRIORITY=>2),
		'features'=>array('1.label'=>(array('htmlOptions'=>array('class'=>'title'))), '1.tooltip', '2.widget',
			TreeWidget::PARAM_ITEMS_PRIORITY=>2),
		'widgetName'=>'NestedList',
		'widgetParams'=>array(
			'blockConformity'=>'table_group_install_type',
			'blockConformityExceptions'=>array('generation'=>'$params[0]["itemName"] !== "user"'),
			'id'=>'!"nl-".$params[TreeWidget::PARAM_TYPE_FIX]["featureName"]',
			'items'=>array(TreeWidget::PARAM_ITEMS_CROSS_KEY=>false, TreeWidget::PARAM_ITEMS_INHERIT_KEY=>false,
				'generation'=>array(
					'blocks'=>array(array(
						'blockConformity'=>'user_generation_type',
						'itemType'=>'checkbox',
						'itemName'=>'generation',
						'items'=>array(
							TreeWidget::PARAM_ITEMS_CROSS_KEY=>false,
							TreeWidget::PARAM_ITEMS_CROSS_DATA=>false,
							TreeWidget::PARAM_ITEMS_INHERIT_KEY=>false,
							'rand'=>array(
								'htmlOptions'=>array('class'=>Html::PREFIX_INPUT_CHILD.'-'.Html::activeId($model, 'quantity')),
								'blocks'=>array(array(
									'itemType'=>'widget',
									'itemName'=>false,
									'widgetName'=>'DataTable',
									'widgetParams'=>array(
										'id'=>'dt-quantity',
										'attrFull'=>false,
										'attributes'=>array('quantity'),
										'features'=>array(
											TreeWidget::PARAM_ITEMS_DEF=>1,
											'2.text, 2.error'=>array('htmlOptions'=>array('class'=>'w30'))
										)
									)
								))
							)
						)
					))
				),
				$new_data_ids=>array('htmlOptions'=>array('disabled'=>'disabled'))
			)
		)
	));
	
	$this->widget(Widget::alias('ButtonsTable'), array('model'=>$model));
	
$this->endWidget();
