<?php
Yii::app()->asset->add(array('gridview'));

$postfix_id = ($type = Arrays::pop($params, 'type')) ? '-'.$type : '';

echo Html::openTag('div', array('class'=>'filter'));
	
	echo Html::openTag('div', array('class'=>'filter-item'));
		echo Html::label('ФИО', 'filter-user-title');
		echo Html::textField('filter-user-title', '', array('class'=>'filter-field'.$postfix_id));
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'filter-item'));
		echo Html::label('Группа', Widget::PREFIX_ID.'4');
		echo Html::hiddenField('filter-user-roles', '', array('class'=>'filter-field'.$postfix_id));
		$this->widget(Widget::alias('DropDownList'), array(
			'data'=>Yii::app()->user->getRoles(0, 'description', 'id', false,
				$params['aim'] == 'client' ? 'citizen, organization' : 'clerk, specialist'),
			'multiple'=>true,
			'widthMin'=>240,
			'widthMax'=>240,
			'readonly'=>true,
			'itemAllEnabled'=>true
		));
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'filter-buttons'));
		echo Html::link('', '#', array('class'=>'button search contur circle'));
		echo Html::link('', '#', array('class'=>'button clear contur circle'));
	echo Html::closeTag('div');
	
echo Html::closeTag('div');

echo Html::hiddenField('userid', '', array('id'=>'userid'));
echo Html::hiddenField('user', '', array('id'=>'user'));

echo Html::openTag('div', array('class'=>'container-container-grid-view'));
	echo Html::openTag('div', array('class'=>'container-grid-view'));
		require('userGrid.php');
		echo Html::tag('div', array('class'=>'curtain-loading'), '');
	echo Html::closeTag('div');
echo Html::closeTag('div');

$params_bt = array('select'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1)), 'cancel');

if (!empty($params['author_enabled']))
	$params_bt = array_merge(array('i'), $params_bt);

$this->widget(Widget::alias('ButtonsTable'), array('params'=>$params_bt));
