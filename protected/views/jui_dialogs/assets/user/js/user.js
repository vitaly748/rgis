function jdUserGVSetId(id){
	var gv = $('div.grid-view#' + id),
		juid = gv.closest('div.jui-dialog'),
		inputId = juid.children('input#userid'),
		inputTitle = juid.children('input#user'),
		keys = gv.yiiGridView('getSelection');
	
	inputId.val(keys ? keys[0] : '');
	inputTitle.val(keys ? gv.find('tr.selected td:first-child').html() : '');
}

$(window).load(function(){
	if (typeof appealParams !== 'undefined' && typeof wJuiDialogButtonClick !== 'undefined')
		$(document).on('click', 'div#juid-author a.button.i, div#juid-responder a.button.i', function(event){
			var juid = $(this).closest('div.jui-dialog'),
				inputId = juid.children('input#userid'),
				inputTitle = juid.children('input#user'),
				button = juid.find('.button.select'),
				gv = juid.find('div.grid-view');
			
			inputId.val(appealParams.userId);
			inputTitle.val(appealParams.userTitle);
			gv.find('tr.selected').removeClass('selected');
			
			if (typeof resetNeedId !== 'undifined')
				resetNeedId();
			
			wJuiDialogButtonClick(button, event);
		});
});
