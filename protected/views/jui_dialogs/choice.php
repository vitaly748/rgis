<?php
Yii::app()->asset->add();

$this->widget(Widget::alias('ValueList'), $params);

$this->widget(Widget::alias('ButtonsTable'), array('params'=>array(
	'select'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1)),
	'cancel'
)));
