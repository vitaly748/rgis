<?php
$data = UserModel::model()->search();
$items = Html::tag('div', array('class'=>'gv-table'), '{items}');
$controls = Html::tag('table', array('class'=>'gv-controls'), Html::tag('tr', array(),
	Html::tag('td', array('class'=>'gv-summary'), '{summary}').
	Html::tag('td', array('class'=>'gv-sizer'), '').
	Html::tag('td', array('class'=>'gv-pager'), '{pager}')
));

if (!isset($postfix_id)){
	preg_match('/(-[^-]+$)/u', $_GET['ajax'], $matches);
	
	$postfix_id = !empty($matches[0]) && in_array($matches[0], array('-author', '-responder')) ? $matches[0] : '';
}

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid'.$postfix_id,
	'dataProvider'=>$data,
	'filterSelector'=>'input.filter-field'.$postfix_id,
	'ajaxType'=>'POST',
	'template'=>Html::hiddenField('filter-size', 10, array('class'=>'filter-field'.$postfix_id)).
		(Yii::app()->request->isAjaxRequest ? $items.$controls : $items),
	'cssFile'=>false,
	'beforeAjaxUpdate'=>'gridviewBeforeAjaxUpdate',
	'afterAjaxUpdate'=>'gridviewAfterAjaxUpdate',
	'ajaxUpdateError'=>'gridviewErrorAjaxUpdate',
	'selectionChanged'=>'jdUserGVSetId',
	'emptyText'=>Yii::app()->label->get(Yii::app()->request->isAjaxRequest ? 'wgv_empty' : 'wgv_loading'),
	'summaryText'=>'Найдено: <strong>{count}</strong>',
	
	'columns'=>array(
		array(
			'header'=>'ФИО / Название',
			'name'=>'title',
		),
		array(
			'name'=>'roles',
			'htmlOptions'=>array('class'=>'ta-center')
		)
	),
	
	'pager'=>array(
		'cssFile'=>false,
		'hiddenPageCssClass'=>'dsp-none',
		'firstPageLabel'=>'',
		'prevPageLabel'=>'',
		'nextPageLabel'=>'',
		'lastPageLabel'=>'',
		'maxButtonCount'=>7,
		'header'=>''
	)
));
