<?php
Yii::app()->asset->add();

$label = isset($params['label']) ? $params['label'] :
	(($controller = Yii::app()->controller->id) == 'user' && mb_strpos(Yii::app()->controller->action->id, 'p') === 0 ?
	'profile' : $controller);

if ($label = Yii::app()->label->get('confirm_'.$label))
	echo Html::tag('div', array(), $label);

$this->widget(Widget::alias('ButtonsTable'), array('params'=>array('yes', 'no')));
