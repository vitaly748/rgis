<?php
Yii::app()->asset->add(array('button', 'input'));

$form = $this->beginWidget('ActiveForm', array('command'=>true));
	
	$this->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model,
		'attrSeparation'=>array(
			array('state', 'name', 'registration'),
			array('retries', 'timeout'),
			array('logdb', 'logfile'),
			array('cache', 'duration1', 'duration2', 'duration3', 'duration4')
		),
		'attributes'=>array(
			'state, registration, cache'=>array('features'=>array(
				'2.widget'=>array('widgetName'=>'DropDownList', 'widgetParams'=>array(
					'widthInitial'=>true, 'readonly'=>true)),
				'2.error_static'
			)),
			'timeout'=>array('features'=>array('1.label'=>array('htmlOptions'=>array('style'=>'width: 128px')), '1.tooltip')),
			'logdb, logfile'=>array('features'=>array('2.checkbox', '2.error'))
		)
	));
	
	$this->widget(Widget::alias('ButtonsTable'), array('form'=>$form, 'model'=>$model,
		'params'=>array('deploy'=>array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm')))));
	
$this->endWidget();

$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm',
	'htmlOptions'=>array('id'=>'juid-confirm'), 'buttonOk'=>'yes', 'buttonCancel'=>'no'));
