<?php
Yii::app()->asset->add();
?>

<div id="scheme-chain">
	<div class="limiter">
		
		<nav id="chain">
			
			<div id="curtain-left-trans"></div>
			
			<div id="overflow">
				
				<ul>
					<?php
					foreach ($levels as $level){
						$item = $level['active'];
						$content = $item['title_active'] ? $item['title_active'] : $item['title'];
						
						echo Html::openTag('li');
							echo Html::link($content, $item['_link']);
						echo Html::closeTag('li');
					}
					?>
				</ul>
				
			</div>
		</nav>
		
	</div>
</div>
