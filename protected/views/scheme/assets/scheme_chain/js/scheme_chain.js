function wSchemeChainRepaint(){
	if (typeof wSchemeChain == 'undefined'){
		var scheme = $('nav#chain'),
			ul = scheme.children('#overflow').children('ul'),
			marginCurrent = marginMax = parseInt(ul.css('margin-left')),
			ulWidth = 0;
			
		ul.children('li').each(function(){
			ulWidth += $(this).outerWidth(true);
		});
		
		var marginAim = marginMax + Math.min(scheme.innerWidth() - marginMax - ulWidth, 0);
		
		wSchemeChain = {
			state: 'init',
			scheme: scheme,
			ul: ul,
			marginCurrent: marginCurrent,
			marginMax: marginMax,
			marginAim: marginAim,
			ulWidth: ulWidth
		};
	}else
		wSchemeChain.marginAim = wSchemeChain.marginMax +
			Math.min(wSchemeChain.scheme.innerWidth() - wSchemeChain.marginMax - wSchemeChain.ulWidth, 0);
	
	if (wSchemeChain.marginCurrent != wSchemeChain.marginAim){
		wSchemeChain.ul.css('margin-left', wSchemeChain.marginAim + 'px');
		wSchemeChain.marginCurrent = wSchemeChain.marginAim;
	}
	
	wSchemeChain.state = 'work';
};

$(window).load(function(){
	wSchemeChainRepaint();
});

$(window).resize(function(){
	if (typeof wSchemeChain != 'undefined' && wSchemeChain.state === 'work')
		wSchemeChainRepaint();
});
