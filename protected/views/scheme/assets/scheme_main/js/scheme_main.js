function wSchemeMainRepaint(){
	if (typeof wSchemeMain == 'undefined'){
		var spaceRight = 30,
			moveAccelerationValue = 2,
			moveSpeedMax = 30,
			moveIntervalValue = 100,
			scheme = $('nav#main'),
			overflow = scheme.children('#overflow'),
			ul = overflow.children('ul'),
			curtainLeftTrans = scheme.children('#curtain-left-trans'),
			curtainRightTrans = scheme.children('#curtain-right-trans'),
			curtainRight = scheme.children('#curtain-right'),
			buttonLeft = curtainRight.children('#button-left'),
			buttonRight = curtainRight.children('#button-right'),
			active = ul.children('li.active'),
			schemeWidth = scheme.width(),
			borderLeft = curtainLeftTrans.length ? curtainLeftTrans.position().left + curtainLeftTrans.width() : 0,
			borderRight = schemeWidth - spaceRight - 1,
			borderRightWithCurtain = curtainLeftTrans.length ? curtainRightTrans.position().left - 1 : 0,
			ulWidth = 0;
		
		ul.children('li').each(function(){
			ulWidth += $(this).outerWidth(true);
		});
		
		var withCurtain = borderLeft + ulWidth - 1 > borderRight,
			range = (withCurtain ? borderRightWithCurtain : borderRight) - borderLeft + 1,
			marginMax = parseInt(ul.css('margin-left')),
			marginMin = withCurtain ? marginMax - (ulWidth - range) : marginMax,
			marginCurrent = marginMax,
			marginAim = marginCurrent;
		
		if (active.length)
			marginAim = marginMax + borderLeft + Math.round((range - active.width()) / 2) - active.position().left;
		else
			active = false;
		
		wSchemeMain = {
			state: 'init',
			scheme: scheme,
			ul: ul,
			active: active,
			curtainLeftTrans: curtainLeftTrans,
			curtainRightTrans: curtainRightTrans,
			curtainRight: curtainRight,
			buttonLeft: buttonLeft,
			buttonRight: buttonRight,
			spaceRight: spaceRight,
			spaceRightWithCurtain: curtainRightTrans.width() + curtainRight.outerWidth(true),
			schemeWidth: schemeWidth,
			borderLeft: borderLeft,
			borderRight: borderRight,
			borderRightWithCurtain: borderRightWithCurtain,
			ulWidth: ulWidth,
			withCurtain: withCurtain,
			marginMax: marginMax,
			marginMin: marginMin,
			marginCurrent: marginCurrent,
			marginAim: marginAim,
			range: range,
			moveLeft: false,
			moveRight: false,
			moveSpeed: 0,
			moveAcceleration: 0,
			moveCross: 0,
			moveAccelerationValue: moveAccelerationValue,
			moveSpeedMax: moveSpeedMax,
			moveIntervalValue: moveIntervalValue,
			intervalId: false
		};
	}else{
		wSchemeMain.schemeWidth = wSchemeMain.scheme.width();
		wSchemeMain.borderRight = wSchemeMain.schemeWidth - wSchemeMain.spaceRight - 1;
		wSchemeMain.borderRightWithCurtain = wSchemeMain.schemeWidth - wSchemeMain.spaceRightWithCurtain - 1;
		wSchemeMain.withCurtain = wSchemeMain.borderLeft + wSchemeMain.ulWidth - 1 > wSchemeMain.borderRight;
		wSchemeMain.range = (wSchemeMain.withCurtain ? wSchemeMain.borderRightWithCurtain : wSchemeMain.borderRight) -
			wSchemeMain.borderLeft + 1;
		wSchemeMain.marginMin = wSchemeMain.withCurtain ? wSchemeMain.marginMax - (wSchemeMain.ulWidth - wSchemeMain.range) :
			wSchemeMain.marginMax;
		
		if (wSchemeMain.intervalId){
			wSchemeMain.buttonLeft.removeClass('active');
			wSchemeMain.buttonRight.removeClass('active');
			wSchemeMain.moveLeft = wSchemeMain.moveRight = wSchemeMain.intervalId = false;
			wSchemeMain.moveSpeed = wSchemeMain.moveAcceleration = wSchemeMain.moveCross = 0;
			clearInterval(wSchemeMain.intervalId);
		}
	}
	
	if (wSchemeMain.withCurtain){
		wSchemeMain.curtainRightTrans.show();
		wSchemeMain.curtainRight.show();
	}else{
		wSchemeMain.curtainRightTrans.hide();
		wSchemeMain.curtainRight.hide();
	}
	
	wSchemeMain.marginAim = Math.max(Math.min(wSchemeMain.marginAim, wSchemeMain.marginMax), wSchemeMain.marginMin);
	
	if (wSchemeMain.marginCurrent != wSchemeMain.marginAim){
//		wSchemeMain.ul.css('margin-left', wSchemeMain.marginAim + 'px');
		wSchemeMain.marginCurrent = wSchemeMain.marginAim;
	}
	
	if (wSchemeMain.marginCurrent === wSchemeMain.marginMin)
		wSchemeMain.buttonRight.addClass('disabled');
	else
		wSchemeMain.buttonRight.removeClass('disabled');
	
	if (wSchemeMain.marginCurrent === wSchemeMain.marginMax)
		wSchemeMain.buttonLeft.addClass('disabled');
	else
		wSchemeMain.buttonLeft.removeClass('disabled');
	
	wSchemeMain.state = 'work';
}

function wSchemeMainMove(){
	var moveCross = !wSchemeMain.moveLeft && !wSchemeMain.moveRight ? 0 : (wSchemeMain.moveLeft ? 1 : -1);
	
	if (moveCross != wSchemeMain.moveCross){
		if (moveCross)
			wSchemeMain.moveAcceleration = moveCross * wSchemeMain.moveAccelerationValue;
		else
			wSchemeMain.moveAcceleration = (1 - 2 * (wSchemeMain.moveSpeed > 0)) * wSchemeMain.moveAccelerationValue;
		wSchemeMain.moveCross = moveCross;
	}
	
	if (wSchemeMain.moveSpeed){
		wSchemeMain.marginAim = wSchemeMain.marginCurrent + wSchemeMain.moveSpeed;
		wSchemeMain.marginAim = Math.max(Math.min(wSchemeMain.marginAim, wSchemeMain.marginMax), wSchemeMain.marginMin);
		
		if (wSchemeMain.marginAim === wSchemeMain.marginCurrent){
			wSchemeMain.buttonLeft.removeClass('active');
			wSchemeMain.buttonRight.removeClass('active');
			wSchemeMain.moveLeft = wSchemeMain.moveRight = wSchemeMain.intervalId = false;
			wSchemeMain.moveSpeed = wSchemeMain.moveAcceleration = wSchemeMain.moveCross = 0;
			clearInterval(wSchemeMain.intervalId);
		}else{
//			wSchemeMain.ul.css('margin-left', wSchemeMain.marginAim + 'px');
			wSchemeMain.marginCurrent = wSchemeMain.marginAim;
		}
	}
	
	if (wSchemeMain.moveAcceleration){
		var speedPositive = wSchemeMain.moveSpeed > 0;
		
		wSchemeMain.moveSpeed += wSchemeMain.moveAcceleration;
		
		if ((wSchemeMain.moveSpeed > 0) != speedPositive && !moveCross){
			wSchemeMain.moveSpeed = wSchemeMain.moveAcceleration = wSchemeMain.moveCross = 0;
			clearInterval(wSchemeMain.intervalId);
			wSchemeMain.intervalId = false;
		}else if (Math.abs(wSchemeMain.moveSpeed) > wSchemeMain.moveSpeedMax){
			wSchemeMain.moveSpeed = (1 - 2 * Number(wSchemeMain.moveSpeed < 0)) * wSchemeMain.moveSpeedMax;
			wSchemeMain.moveAcceleration = 0;
		}
	}
	
	if (wSchemeMain.marginCurrent === wSchemeMain.marginMin)
		wSchemeMain.buttonRight.addClass('disabled');
	else
		wSchemeMain.buttonRight.removeClass('disabled');
	
	if (wSchemeMain.marginCurrent === wSchemeMain.marginMax)
		wSchemeMain.buttonLeft.addClass('disabled');
	else
		wSchemeMain.buttonLeft.removeClass('disabled');
}

$(window).load(function(){
	wSchemeMainRepaint();
	
	wSchemeMain.buttonLeft.on('mousedown', function(event){
		if (!$(this).hasClass('disabled') && event.which === 1){
			$(this).addClass('active');
			wSchemeMain.moveLeft = true;
			
			if (!wSchemeMain.intervalId)
				wSchemeMain.intervalId = setInterval(wSchemeMainMove, wSchemeMain.moveIntervalValue);
		}
	});
	
	wSchemeMain.buttonLeft.on('mouseup', function(){
		if ($(this).hasClass('active')){
			$(this).removeClass('active');
			wSchemeMain.moveLeft = false;
		}
	});
	
	wSchemeMain.buttonLeft.on('mouseout', function(){
		if ($(this).hasClass('active')){
			$(this).removeClass('active');
			wSchemeMain.moveLeft = false;
		}
	});
	
	wSchemeMain.buttonRight.on('mousedown', function(event){
		if (!$(this).hasClass('disabled') && event.which === 1){
			$(this).addClass('active');
			wSchemeMain.moveRight = true;
			
			if (!wSchemeMain.intervalId)
				wSchemeMain.intervalId = setInterval(wSchemeMainMove, wSchemeMain.moveIntervalValue);
		}
	});
	
	wSchemeMain.buttonRight.on('mouseup', function(){
		if ($(this).hasClass('active')){
			$(this).removeClass('active');
			wSchemeMain.moveRight = false;
		}
	});
	
	wSchemeMain.buttonRight.on('mouseout', function(){
		if ($(this).hasClass('active')){
			$(this).removeClass('active');
			wSchemeMain.moveRight = false;
		}
	});
});

$(window).resize(function(){
	if (typeof wSchemeMain != 'undefined' && wSchemeMain.state === 'work')
		wSchemeMainRepaint();
});
