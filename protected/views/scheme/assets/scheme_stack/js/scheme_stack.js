function wSchemeStackRepaint(){
	if (typeof wSchemeStack == 'undefined'){
		var spaceRight = 26,
			moveAccelerationValue = 2,
			moveSpeedMax = 30,
			moveIntervalValue = 100,
			scheme = $('nav#stack'),
			overflow = scheme.children('#overflow'),
			curtainLeftTrans = scheme.children('#curtain-left-trans'),
			curtainRightTrans = scheme.children('#curtain-right-trans'),
			curtainRight = scheme.children('#curtain-right'),
			schemeWidth = scheme.width(),
			borderLeft = curtainLeftTrans.position().left + curtainLeftTrans.width(),
			borderRight = schemeWidth - spaceRight - 1,
			borderRightWithCurtain = curtainRightTrans.position().left - 1,
			curtainRightTransCurtains = curtainRightTrans.children('#curtain'),
			curtainRightCurtains = curtainRight.children('#curtain'),
			spaceRightWithCurtain = curtainRightTransCurtains.outerWidth(true) + curtainRightCurtains.outerWidth(true),
			ulList = [],
			schemeHeight = 0;
		
		overflow.children('ul').each(function(keyUl){
			var ul = $(this),
				curtainRightTransCurtain = $(curtainRightTransCurtains[keyUl]),
				curtainRightCurtain = $(curtainRightCurtains[keyUl]),
				buttonLeft = curtainRightCurtain.children('#button-left'),
				buttonRight = curtainRightCurtain.children('#button-right'),
				active = ul.children('li.active'),
				ulHeight = ul.innerHeight(),
				ulWidth = 0;
			
			curtainRightTransCurtain.height(ulHeight).css('top', schemeHeight + 'px');
			curtainRightCurtain.height(ulHeight).css('top', schemeHeight + 'px');
			schemeHeight += ulHeight;
			curtainRightTransCurtain.attr('for', keyUl);
			curtainRightCurtain.attr('for', keyUl);
			
			ul.children('li').each(function(){
				ulWidth += $(this).outerWidth(true);
			});
			
			var withCurtain = borderLeft + ulWidth - 1 > borderRight,
				range = (withCurtain ? borderRightWithCurtain : borderRight) - borderLeft + 1,
				marginMax = parseInt(ul.css('margin-left')),
				marginMin = withCurtain ? marginMax - (ulWidth - range) : marginMax,
				marginCurrent = marginMax,
				marginAim = marginCurrent;
			
			if (active.length)
				marginAim = marginMax + borderLeft + Math.round((range - active.width()) / 2) - active.position().left;
			else
				active = false;
			
			ulList.push({
				ul: ul,
				curtainRightTransCurtain: curtainRightTransCurtain,
				curtainRightCurtain: curtainRightCurtain,
				buttonLeft: buttonLeft,
				buttonRight: buttonRight,
				active: active,
				ulWidth: ulWidth,
				withCurtain: withCurtain,
				range: range,
				marginMax: marginMax,
				marginMin: marginMin,
				marginCurrent: marginCurrent,
				marginAim: marginAim,
				moveLeft: false,
				moveRight: false,
				moveSpeed: 0,
				moveAcceleration: 0,
				moveCross: 0
			});
		});
		
		curtainLeftTrans.height(schemeHeight);
		
		wSchemeStack = {
			state: 'init',
			scheme: scheme,
			curtainLeftTrans: curtainLeftTrans,
			curtainRightTrans: curtainRightTrans,
			curtainRight: curtainRight,
			spaceRight: spaceRight,
			spaceRightWithCurtain: spaceRightWithCurtain,
			schemeWidth: schemeWidth,
			borderLeft: borderLeft,
			borderRight: borderRight,
			borderRightWithCurtain: borderRightWithCurtain,
			ulList: ulList,
			moveAccelerationValue: moveAccelerationValue,
			moveSpeedMax: moveSpeedMax,
			moveIntervalValue: moveIntervalValue,
			intervalId: false
		};
	}else{
		wSchemeStack.schemeWidth = wSchemeStack.scheme.width();
		wSchemeStack.borderRight = wSchemeStack.schemeWidth - wSchemeStack.spaceRight - 1;
		wSchemeStack.borderRightWithCurtain = wSchemeStack.schemeWidth - wSchemeStack.spaceRightWithCurtain - 1;
		
		$.each(wSchemeStack.ulList, function(keyUl, ul){
			ul.withCurtain = wSchemeStack.borderLeft + ul.ulWidth - 1 > wSchemeStack.borderRight;
			ul.range = (ul.withCurtain ? wSchemeStack.borderRightWithCurtain : wSchemeStack.borderRight) -
				wSchemeStack.borderLeft + 1;
			ul.marginMin = ul.withCurtain ? ul.marginMax - (ul.ulWidth - ul.range) :
				ul.marginMax;
		});
		
		if (wSchemeStack.intervalId){
			$.each(wSchemeStack.ulList, function(keyUl, ul){
				ul.buttonLeft.removeClass('active');
				ul.buttonRight.removeClass('active');
				ul.moveLeft = ul.moveRight = false;
				ul.moveSpeed = ul.moveAcceleration = ul.moveCross = 0;
			});
			
			clearInterval(wSchemeStack.intervalId);
			wSchemeStack.intervalId = false;
		}
	}
	
	$.each(wSchemeStack.ulList, function(keyUl, ul){
		if (ul.withCurtain){
			ul.curtainRightTransCurtain.show();
			ul.curtainRightCurtain.show();
		}else{
			ul.curtainRightTransCurtain.hide();
			ul.curtainRightCurtain.hide();
		}
		
		ul.marginAim = Math.max(Math.min(ul.marginAim, ul.marginMax), ul.marginMin);
		
		if (ul.marginCurrent != ul.marginAim){
			ul.ul.css('margin-left', ul.marginAim + 'px');
			ul.marginCurrent = ul.marginAim;
		}
		
		if (ul.marginCurrent === ul.marginMin)
			ul.buttonRight.addClass('disabled');
		else
			ul.buttonRight.removeClass('disabled');
		
		if (ul.marginCurrent === ul.marginMax)
			ul.buttonLeft.addClass('disabled');
		else
			ul.buttonLeft.removeClass('disabled');
	});
	
	wSchemeStack.state = 'work';
}

function wSchemeStackMove(){
	var reset = 1;
	
	$.each(wSchemeStack.ulList, function(keyUl, ul){
		var resetUl = 1,
			moveCross = !ul.moveLeft && !ul.moveRight ? 0 : (ul.moveLeft ? 1 : -1);
		
		if (moveCross != ul.moveCross){
			if (moveCross)
				ul.moveAcceleration = moveCross * wSchemeStack.moveAccelerationValue;
			else
				ul.moveAcceleration = (1 - 2 * (ul.moveSpeed > 0)) * wSchemeStack.moveAccelerationValue;
			
			ul.moveCross = moveCross;
		}
		
		if (ul.moveSpeed){
			resetUl = 0;
			ul.marginAim = ul.marginCurrent + ul.moveSpeed;
			ul.marginAim = Math.max(Math.min(ul.marginAim, ul.marginMax), ul.marginMin);
			
			if (ul.marginAim === ul.marginCurrent){
				ul.buttonLeft.removeClass('active');
				ul.buttonRight.removeClass('active');
				ul.moveLeft = ul.moveRight = false;
				ul.moveSpeed = ul.moveAcceleration = ul.moveCross = 0;
				resetUl = 1;
			}else{
				ul.ul.css('margin-left', ul.marginAim + 'px');
				ul.marginCurrent = ul.marginAim;
			}
		}
		
		if (ul.moveAcceleration){
			var speedPositive = ul.moveSpeed > 0;
			
			resetUl = 0;
			ul.moveSpeed += ul.moveAcceleration;
			
			if ((ul.moveSpeed > 0) != speedPositive && !moveCross){
				ul.moveSpeed = ul.moveAcceleration = ul.moveCross = 0;
				resetUl = 1;
			}else if (Math.abs(ul.moveSpeed) > wSchemeStack.moveSpeedMax){
				ul.moveSpeed = (1 - 2 * Number(ul.moveSpeed < 0)) * wSchemeStack.moveSpeedMax;
				ul.moveAcceleration = 0;
			}
		}
		
		if (ul.marginCurrent === ul.marginMin)
			ul.buttonRight.addClass('disabled');
		else
			ul.buttonRight.removeClass('disabled');
		
		if (ul.marginCurrent === ul.marginMax)
			ul.buttonLeft.addClass('disabled');
		else
			ul.buttonLeft.removeClass('disabled');
		
		if (!resetUl)
			reset = 0;
	});
	
	if (reset){
		clearInterval(wSchemeStack.intervalId);
		wSchemeStack.intervalId = false;
	}
}

$(window).load(function(){
	wSchemeStackRepaint();
	
	$('nav#stack ul').on('mouseenter', function(){
		$('nav#stack ul').removeClass('hover');
		$(this).addClass('hover');
	});
	
	$('nav#stack').on('mouseleave', function(){
		$('nav#stack ul').removeClass('hover');
	});
	
	$('nav#stack #curtain-right-trans #curtain').on('mouseenter', function(){
		var ul = wSchemeStack.ulList[$(this).attr('for')];
		
		$('nav#stack ul').removeClass('hover');
		ul.ul.addClass('hover');
	});
	
	$('nav#stack #curtain-right #curtain').on('mouseenter', function(){
		var ul = wSchemeStack.ulList[$(this).attr('for')];
		
		$('nav#stack ul').removeClass('hover');
		ul.ul.addClass('hover');
	});
	
	$('nav#stack #button-left').on('mousedown', function(event){
		if (!$(this).hasClass('disabled') && event.which === 1){
			var ul = wSchemeStack.ulList[$(this).parent().attr('for')];
			
			$(this).addClass('active');
			ul.moveLeft = true;
			
			if (!wSchemeStack.intervalId)
				wSchemeStack.intervalId = setInterval(wSchemeStackMove, wSchemeStack.moveIntervalValue);
		}
	});
	
	$('nav#stack #button-left').on('mouseup', function(){
		if ($(this).hasClass('active')){
			var ul = wSchemeStack.ulList[$(this).parent().attr('for')];
			
			$(this).removeClass('active');
			ul.moveLeft = false;
		}
	});
	
	$('nav#stack #button-left').on('mouseout', function(){
		if ($(this).hasClass('active')){
			var ul = wSchemeStack.ulList[$(this).parent().attr('for')];
			
			$(this).removeClass('active');
			ul.moveLeft = false;
		}
	});
	
	$('nav#stack #button-right').on('mousedown', function(event){
		if (!$(this).hasClass('disabled') && event.which === 1){
			$(this).addClass('active');
			var ul = wSchemeStack.ulList[$(this).parent().attr('for')];
			
			ul.moveRight = true;
			
			if (!wSchemeStack.intervalId)
				wSchemeStack.intervalId = setInterval(wSchemeStackMove, wSchemeStack.moveIntervalValue);
		}
	});
	
	$('nav#stack #button-right').on('mouseup', function(){
		if ($(this).hasClass('active')){
			var ul = wSchemeStack.ulList[$(this).parent().attr('for')];
			
			$(this).removeClass('active');
			ul.moveRight = false;
		}
	});
	
	$('nav#stack #button-right').on('mouseout', function(){
		if ($(this).hasClass('active')){
			var ul = wSchemeStack.ulList[$(this).parent().attr('for')];
			
			$(this).removeClass('active');
			schemeMain.moveRight = false;
		}
	});
});

$(window).resize(function(){
	if (typeof wSchemeStack != 'undefined' && wSchemeStack.state === 'work')
		wSchemeStackRepaint();
});
