<?php
Yii::app()->asset->add();
?>

<div id="scheme-stack"<?=$this->_contentOffset ? ' class="content-offset"' : ''?>>
	
	<nav id="stack">
		
		<div id="curtain-left-trans"></div>
		
		<div id="overflow">
			
			<?php
			$qu_levels = count($levels);
			foreach ($levels as $key_level=>$level){
				$last_level = $key_level === $qu_levels - 1;
				
				echo Html::openTag('ul');
					
					foreach ($level as $key_item=>$item)
						if ($item['_visible']){
							$li_class = array();
							$header_tag = 0;
							
							if (!$item['_enabled'])
								$li_class[] = 'disabled';
							else{
								$link = $item['_link'];
								if ($key_item === 'active')
									$li_class[] = 'active';
							}
							
							echo Html::openTag('li', $li_class ? array('class'=>implode(' ', $li_class)) : array());
								
								if ($key_item !== 'active')
									$content = $item['title'];
								elseif ($last && $last_level){
									if ($this->titleShow){
										$content = $item['title'];
										$header_tag = 'h3';
									}else{
										$content = $item['title_active'] ? $item['title_active'] : $item['title'];
										$header_tag = 'h2';
									}
								}else{
									$content = $item['title_active'] ? $item['title_active'] : $item['title'];
									$header_tag = 'h3';
								}
								
								if ($header_tag)
									echo Html::openTag($header_tag);
									
									if (!empty($link))
										echo Html::openTag('a', array('href'=>$link));
										
										echo Html::tag('p', array(), $content);
										
									if (!empty($link))
										echo Html::closeTag('a');
									
								if ($header_tag)
									echo Html::closeTag($header_tag);
								
							echo Html::closeTag('li');
						}
					
				echo Html::closeTag('ul');
			}
			?>
			
		</div>
		
		<div id="curtain-right-trans">
			<?php
			for ($count = 0; $count < $qu_levels; $count++)
				echo Html::tag('div', array('id'=>'curtain'), '');
			?>
		</div>
		
		<div id="curtain-right">
			<?php
			for ($count = 0; $count < $qu_levels; $count++){
				echo Html::openTag('div', array('id'=>'curtain'));
					
					echo Html::tag('div', array('id'=>'button-left'), '');
					echo Html::tag('div', array('id'=>'button-right'), '');
					
				echo Html::closeTag('div');
			}
			?>
		</div>
		
	</nav>
	
</div>
