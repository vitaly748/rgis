<?php
Yii::app()->asset->add();
?>

<nav id="main">
	
<!-- 	<div id="curtain-left"></div>
	<div id="curtain-left-trans"></div> -->
	
	<div id="overflow">
		
		<ul>
			
			<?php
			foreach ($levels[0] as $key_item=>$item)
				if ($item['_visible']){
// 					$main = $item['route'] === Yii::app()->defaultController && !$item['id_static'];
					$main = false;
					$li_class = array();
					$link = false;
					
					if ($main)
						$li_class[] = 'main';
					
					if (!$item['_enabled'])
						$li_class[] = 'disabled';
					else{
						$link = $item['_link'];
						
						if ($key_item === 'active')
							$li_class[] = 'active';
					}
					
					echo Html::openTag('li', array('class'=>implode(' ', $li_class)));
						
						if ($link)
							echo Html::openTag('a', Html::forming(array('round3', 'href'=>$link,
								'target'=>!preg_match(Scheme::PATTERN_ROUTE, $link))));
						
						if ($main)
							$content = '';
						elseif ($key_item != 'active')
							$content = $item['title'];
						elseif ($last && $this->titleShow)
							$content = $item['title'];
						else
// 							$content = $item['title_active'] ? $item['title_active'] : $item['title'];
							$content = $item['title'];
						
						echo Html::tag('p', array(), $content);
							
						if ($link)
							echo Html::closeTag('a');
						
					echo Html::closeTag('li');
				}
			?>
			
		</ul>
		
	</div>
	
<!-- 	<div id="curtain-right-trans"></div>
	
	<div id="curtain-right">
		<div id="button-left"></div><div id="button-right"></div>
	</div> -->
	
</nav>
