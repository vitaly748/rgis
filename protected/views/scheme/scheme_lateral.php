<?php
Yii::app()->asset->add($this->compression ? array('scheme_compression') : false);

	function render_ul($levels, $last, $title_show, $key, $qu_levels){
		$level = $levels[$key];
		$last_level = $key === $qu_levels - 1;
		
		echo Html::openTag('ul', array('class'=>'level-'.($key + 1).$qu_levels));
			
			foreach ($level as $key_item=>$item)
				if ($item['_visible']){
					$link = false;
					$li_class = array();
					$round = false;
					if (!$item['_enabled'])
						$li_class[] = 'disabled';
					else{
						$link = $item['_link'];
						if ($key_item === 'active'){
							$li_class[] = 'active';
							/* if ($last_level)
								$round = true; */
						}
					}
					
					echo Html::openTag('li', $li_class ? array('class'=>implode(' ', $li_class)) : array());
						
						if (!empty($link))
							echo Html::openTag('a', Html::forming(array('href'=>$link, 'round3'=>$round)));
							
							if ($key_item !== 'active')
								$content = $item['title'];
							elseif ($last && $last_level && $title_show)
								$content = $item['title'];
							else
								$content = $item['title_active'] ? $item['title_active'] : $item['title'];
							
							echo Html::tag('p', array(), $content);
							
						if (!empty($link))
							echo Html::closeTag('a');
						
						if ($key_item === 'active' && !$last_level)
							render_ul($levels, $last, $title_show, $key + 1, $qu_levels);
						
					echo Html::closeTag('li');
				}
			
		echo Html::closeTag('ul');
	}
?>

<div id="scheme-lateral">
	
	<nav id="lateral">
		<?php
		render_ul($levels, $last, $this->titleShow, 0, count($levels));
		?>
	</nav>
	
</div>
