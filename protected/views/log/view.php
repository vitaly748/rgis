<?php
Yii::app()->asset->add(array('button'));

$this->widget(Widget::alias('DataTable'), array('featureModel'=>$model, 'htmlOptions'=>array('class'=>'unrequired'),
	TreeWidget::PARAM_ATTRIBUTES_FULL=>false,
	'attributes'=>array(
		'date, id_user, id_event, id_error'=>array('features'=>array(TreeWidget::PARAM_ITEMS_PRIORITY=>2,
			'1.label'=>array('htmlOptions'=>array('disabled'=>false)),
			'2.value'=>array('htmlOptions'=>array('class'=>'value-big value-wide'))
		)),
		'params'=>array('cells'=>array(TreeWidget::PARAM_ITEMS_INHERIT_DATA=>0,
			1=>array('htmlOptions'=>array('class'=>'cell-name dt-cell-top'), 'features'=>array('1.label')),
			2=>array('htmlOptions'=>array('class'=>'cell-value'), 'features'=>array(
				'2.widget'=>array(
					'widgetName'=>'Struct'
				)
			))
		))
	),
));

if ($next || $back){
	if ($back)
		$params_bt['back'] = array('type'=>'a', 'html_options'=>array('href'=>$this->createUrl('log/view', array('id'=>$back))));
	
	if ($next)
		$params_bt['next'] = array('type'=>'a', 'html_options'=>array('href'=>$this->createUrl('log/view', array('id'=>$next))));
	
	$this->widget(Widget::alias('ButtonsTable'), array('params'=>$params_bt));
}
