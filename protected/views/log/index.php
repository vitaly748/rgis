<?php
Yii::app()->asset->add(array('gridview'));

foreach (array(Yii::app()->event->get(), Yii::app()->errorEvent->get()) as $key=>$set){
	foreach ($set as $item){
		$group = is_numeric($item['name']) ? 'number' : reset(explode('_', $item['name']));
		
		if (isset($prev_group) && $group !== $prev_group)
			$data[$key][] = false;
		
		$prev_group = $group;
		$item_data = array('code'=>$item['id'],
			'description'=>preg_replace('/<a[^>]*>([^<]*)<\/a>/u', '$1', $item['description']));
		
		if ($item['name'] !== 'ok')
			$data[$key][] = $item_data;
		else
			$item_ok = $item_data;
	}
	
	unset($prev_group);
}

if (isset($item_ok))
	$data[1] = array_merge(array($item_ok, false), $data[1]);

$this->widget(Widget::alias('Flashes'),
	array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

echo Html::openTag('div', array('class'=>'filter'));
	
	echo Html::openTag('div', array('class'=>'filter-item'));
		echo Html::activeLabel($model, 'id_user');
		echo Html::hiddenField('filter-log-user', '', array('class'=>'filter-field'));
		echo Html::element(array('type'=>'text', 'model'=>$model, 'name'=>'id_user',
			'htmlOptions'=>array('class'=>'filter-field-text')));
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'filter-item'));
		echo Html::activeLabel($model, 'id_event');
		echo Html::hiddenField('filter-log-event', '', array('class'=>'filter-field'));
		$this->widget(Widget::alias('DropDownList'), array(
			'model'=>$model,
			'name'=>'id_event',
			'data'=>$data[0],
			'widthMin'=>400,
			'widthMax'=>400,
			'multiple'=>true,
			'itemAllEnabled'=>true
		));
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'filter-item'));
		echo Html::activeLabel($model, 'id_error');
		echo Html::hiddenField('filter-log-error', '', array('class'=>'filter-field'));
		$this->widget(Widget::alias('DropDownList'), array(
			'model'=>$model,
			'name'=>'id_error',
			'data'=>$data[1],
			'widthMin'=>400,
			'widthMax'=>400,
			'multiple'=>true,
			'itemAllEnabled'=>true
		));
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'filter-buttons'));
		echo Html::link('', '#', array('class'=>'button search contur circle'));
		echo Html::link('', '#', array('class'=>'button clear contur circle'));
	echo Html::closeTag('div');
	
echo Html::closeTag('div');

echo Html::openTag('div', array('class'=>'container-grid-view'));
	$this->renderPartial('indexGrid', array('model'=>$model));
	echo Html::tag('div', array('class'=>'curtain-loading'), '');
echo Html::closeTag('div');

$this->widget(Widget::alias('ButtonsTable'), array('params'=>array('view'=>
	array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1)))));
