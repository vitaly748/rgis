<?php
Yii::app()->asset->add(array('gridview'));

$this->widget(Widget::alias('Flashes'),
	array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

echo Html::openTag('div', array('class'=>'filter'));
	
	echo Html::openTag('div', array('class'=>'filter-item fi-not-clean'));
		echo Html::label('Услуга', Widget::PREFIX_ID.'1', array('disabled'=>!$id_services));
		echo Html::hiddenField('filter-history-service', $id, array('class'=>'filter-field'));
		$this->widget(Widget::alias('DropDownList'), array(
			'data'=>$id_services ? $id_services : array('empty'=>array('access'=>'view')),
			'name'=>'filter-service',
			'value'=>$id,
			'widthInitial'=>true,
			'readonly'=>true
		));
	echo Html::closeTag('div');
	
	if ($id_services){
		echo Html::openTag('div', array('class'=>'filter-buttons'));
			echo Html::link('', '#', array('class'=>'button search contur circle'));
		echo Html::closeTag('div');
	}
	
echo Html::closeTag('div');

if ($id_services){
	echo Html::openTag('div', array('class'=>'container-grid-view'));
		$this->renderPartial('historyGrid', array('model'=>$model));
		echo Html::tag('div', array('class'=>'curtain-loading'), '');
	echo Html::closeTag('div');
}elseif ($note = Yii::app()->label->get('note_services_empty'))
	echo Html::tag('div', array('class'=>'note'), Html::tag('div', array(), $note));
