<?php
$items = Html::tag('div', array('class'=>'gv-table'), '{items}');

foreach (array('name', 'cost') as $name)
	$columns[] = array(
		'header'=>$model->getAttributeLabel($name),
		'name'=>$name,
		'headerHtmlOptions'=>array('class'=>'nowrap'),
		'htmlOptions'=>(array('class'=>'nowrap ta-center'))
	);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'services-grid',
	'dataProvider'=>$model->search(),
	'filterSelector'=>'input.filter-field',
	'ajaxType'=>'POST',
	'template'=>$items,
	'cssFile'=>false,
	'beforeAjaxUpdate'=>'gridviewBeforeAjaxUpdate',
	'afterAjaxUpdate'=>'gridviewAfterAjaxUpdate',
	'ajaxUpdateError'=>'gridviewErrorAjaxUpdate',
	'emptyText'=>Yii::app()->label->get(Yii::app()->request->isAjaxRequest ? 'wgv_empty' : 'wgv_loading'),
	'columns'=>$columns
));
