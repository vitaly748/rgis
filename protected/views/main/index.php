<?php
Yii::app()->asset->add();

if ($this->technicalWork && !Yii::app()->user->authorized){
	if ($note = Yii::app()->label->get('note_technical_work'))
		echo Html::tag('div', array('class'=>'note'), Html::tag('div', array(), $note));
}else{
    if (Yii::app()->user->getState('confirm'))
        $this->widget(Widget::alias('JuiDialog'), array('action'=>'confirm', 'autoOpen'=>'true', 'closing'=>false,
            'htmlOptions'=>array('id'=>'juid-confirm'), 'buttonOk'=>false, 'buttonCancel'=>false));
    elseif (Yii::app()->user->isGuest){
        echo Html::tag('div', array('class'=>'note-main'), Yii::app()->label->get('note_main_guest'));

        if ($model && ($data = $model->search()) && ($rows = $data->getData())){
            echo Html::openTag('div', array('id'=>'news-main'));
                echo Html::tag('h1', array(), Yii::app()->label->get('insert_important'));
                echo Html::link('', '#', array('class' => 'news-up'));

                echo Html::openTag('ul', array('id'=>'news-main-list'));

                foreach ($rows as $row){
                    echo Html::openTag('li');
                        echo Html::openTag('a', array('href'=>$this->createUrl('news/view', array('id'=>$row['id']))));
                            echo Html::tag('div', array('class'=>'news-title'), $row['title']);
                        echo Html::closeTag('a');
                    echo Html::closeTag('li');
                }

                echo Html::closeTag('ul');

                echo Html::link('', '#', array('class' => 'news-down'));
            echo Html::closeTag('div');
        }
    }else{
        if (Yii::app()->user->checkAccessSet('bss1_payer, bss2_payer'))
            $note = sprintf(Yii::app()->label->get('note_main_repair'), $this->createUrl('account/index'));
        elseif (Yii::app()->user->checkAccessSet('account/connection'))
            $note = sprintf(Yii::app()->label->get('note_main_citizen'), $this->createUrl('account/connection'));

        $note = Yii::app()->label->get('note_main').(!empty($note) ? Html::tag('br').Html::tag('br').$note : '');

        if ($note)
            echo Html::tag('div', array('class'=>'note'), Html::tag('div', array(), $note));
	
        echo Html::openTag('div', array('id'=>'link-list'));

            $links = array();

            if (Yii::app()->user->checkAccess('appeal/index'))
                $links['appeal'] = array('Задать вопрос.'.Html::tag('br').'Вам ответит специалист', $this->createUrl('appeal/index'));

            $links['index'] = array('Информация об утвержденных указом губернатора Пермского края предельных (максимальных) индексах '.
                'изменения размера платы, вносимой гражданами за коммунальные услуги в муниципальных образованиях',
                'https://'.Yii::app()->portal->get('link').'/norms/front', true);
            $links['answer'] = array('Ответы на часто задаваемые вопросы', $this->createUrl('article/answer'));

            foreach ($links as $name=>$params){
                echo Html::openTag('a', Html::forming(array('href'=>Arrays::pop($params, 1), $name,
                    'target'=>Arrays::pop($params, 2) ? '_blank' : false)));
                        echo Html::openTag('table');
                            echo Html::openTag('tbody');
                                echo Html::openTag('tr');
                                    echo Html::tag('td', array('class'=>'ll-icon'), '');
                                    echo Html::openTag('td', array('class'=>'ll-title'));
                                        echo Html::tag('div', array(), Arrays::pop($params, 0));
                                    echo Html::closeTag('div');
                                echo Html::closeTag('tr');
                            echo Html::closeTag('tbody');
                        echo Html::closeTag('table');
                    echo Html::closeTag('a');
            }

        echo Html::closeTag('div');

        if ($model && ($data = $model->search()) && ($rows = $data->getData())){
            echo Html::tag('h4', array(), 'Новости');

            echo Html::openTag('ul', array('id'=>'news-list'));

                foreach ($rows as $row){
                    echo Html::openTag('li');
                        echo Html::openTag('a', array('href'=>$this->createUrl('news/view', array('id'=>$row['id']))));
                            echo Html::tag('div', array('class'=>'news-title'), $row['title']);
                            echo Html::tag('div', array('class'=>'news-date'),
                                Swamper::date($row['date'], Swamper::FORMAT_DATE_USER_WITHOUT_TIME));
                            echo Html::tag('div', array('class'=>'news-text'), Strings::multilineText($row['text']));
                        echo Html::closeTag('a');
                    echo Html::closeTag('li');
                }

            echo Html::closeTag('ul');
        }
    }
}
