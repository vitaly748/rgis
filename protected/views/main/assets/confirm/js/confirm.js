$(window).load(function(){
	setTimeout(function(){
		if (typeof confirmToggleId !== 'undefined'){
			var toggle = $('input#' + confirmToggleId),
				button = $('button.login');
			
			if (toggle.length && button.length)
				toggle.on('change', function(){
					buttonToggle(button, !!$(this).attr('checked'));
				});
		}
	}, 300);
});
