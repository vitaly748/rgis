newsPos = 0;
newsQu = 3;

function newsUpdate(step){
    var obj = $('#news-main');

    if (obj.length){
        var li = obj.find('li'),
            posMax = Math.max(li.length - newsQu, 0);

        step = step | 0;
        newsPos = Math.max(Math.min(newsPos + step, posMax), 0);
        li.hide();

        for (var count = 0; count < newsQu; count++){
            $(li[newsPos + count]).css('margin-top', count ? 50 : 0).show();
        }

        obj.find('a.news-up').css('visibility', newsPos > 0 ? 'visible' : 'hidden');
        obj.find('a.news-down').css('visibility', newsPos < posMax ? 'visible' : 'hidden');
    }
}

$(window).load(function(){
    newsUpdate();

    $('#news-main a.news-up, #news-main a.news-down').on('click', function(event){
        event.preventDefault();

        newsUpdate(-1 + 2 * $(this).hasClass('news-down'));
    })
});
