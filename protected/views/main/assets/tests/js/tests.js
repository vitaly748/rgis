$(window).load(function(){
	$('div.level div.title').on('click', function(){
		var level = $(this).parent(),
			childs = level.children('div.childs');
		
		if (childs.length === 1)
			if (level.hasClass('open'))
				childs.slideUp(function(){
					level.removeClass('open');
				});
			else
				childs.slideDown(function(){
					level.addClass('open');
				});
	});
});
