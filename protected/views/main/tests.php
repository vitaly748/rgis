<?php
Yii::app()->getClientScript()->registerCoreScript('jquery');
Yii::app()->asset->add();

$asserts = array();
	
	function error_state($error){
		$states = array(
			array('state'=>'ok', 'msg'=>'Успех'),
			array('state'=>'file', 'msg'=>'Файл теста не найден'),
			array('state'=>'error', 'msg'=>'Ошибка')
		);
		
		return !$error ? $states[0] : ($error === Test::ERROR_FILE ? $states[1] : $states[2]);
	}
	
echo Html::openTag('div', array('class'=>'level open'));
	$error_state = error_state($tests['error']);
	$qu_total = count($tests['tests']);
	$qu_ok = $qu_total - $tests['error'];
	
	$qu_total_tests = $qu_ok_tests = 0;
	foreach ($tests['tests'] as $test)
		if (isset($test['asserts'])){
			$qu_total_tests += $qu_total_test = count($test['asserts']);
			$qu_ok_tests += $qu_total_test - $test['error'];
		}
	
	echo Html::tag('div', array('class'=>'title'), 'Тестирование');
	
	echo Html::tag('div', array('class'=>'state '.$error_state['state']), $error_state['msg']);
	
	echo Html::tag('div', array('class'=>'quantity'), "($qu_ok_tests/$qu_total_tests - $qu_ok/$qu_total)");
	
	echo Html::tag('hr');
	
	echo Html::openTag('div', array('class'=>'childs'));
		
		foreach ($tests['tests'] as $name=>$test){
			
			echo Html::openTag('div', array('class'=>'level'.($test['error'] ? ' open' : '')));
				
				$error_state = error_state($test['error']);
				if (isset($test['asserts'])){
					$qu_total = count($test['asserts']);
					$qu_ok = $qu_total - $test['error'];
				}
				
				echo Html::tag('div', array('class'=>'title'), $name);
				
				echo Html::tag('div', array('class'=>'state '.$error_state['state']), $error_state['msg']);
				
				if (isset($test['asserts'])){
					
					echo Html::tag('div', array('class'=>'quantity'), "($qu_ok/$qu_total)");
					
					echo Html::openTag('div', array('class'=>'childs'));
						
						foreach ($test['asserts'] as $key_assert=>$assert){
							
							echo Html::openTag('div', array('class'=>'level'));
								
								$error_state = error_state($assert['error']);
								
								echo Html::tag('div', array('class'=>'title'), ++$key_assert.'. '.$assert['func']);
								
								echo Html::tag('div', array('class'=>'state '.$error_state['state']), $error_state['msg']);
								
								echo Html::tag('div', array('class'=>'line'), 'строка '.$assert['line']);
								
								echo Html::openTag('div', array('class'=>'childs'));
									
									if ($title = $assert['title'])
										echo Html::tag('div', array('class'=>'title-assert'), $title);
									
									foreach ($assert['args'] as $key=>$arg){
										
										echo Html::openTag('div', array('class'=>'arg'));
											
											Arrays::printPre($arg, false, false, $key || $title ?
												Arrays::PRINT_PRE_BORDER_TOP : Arrays::PRINT_PRE_BORDER_NONE);
											
										echo Html::closeTag('div');
									}
									
								echo Html::closeTag('div');
								
							echo Html::closeTag('div');
						}
						
					echo Html::closeTag('div');
				}
				
			echo Html::closeTag('div');
		}
		
	echo Html::closeTag('div');
	
echo Html::closeTag('div');
