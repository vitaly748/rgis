<?php
Yii::app()->asset->add();

if ($label = Yii::app()->label->get('note_confirm_'.(($agreement = $type === WebUser::CONFIRM_TYPE_AGREEMENT) ?
	'agreement' : 'secure')))
		echo Html::tag('div', array(), sprintf($label, Yii::app()->baseUrl.'/assets/'.($agreement ?
			'user_agreement.pdf' : 'instruction_secure.pdf')));

$form = $this->beginWidget('ActiveForm', array('action'=>$this->createUrl('main/confirm'), 'command'=>true));
	
	$this->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model, 'cells'=>array(
		TreeWidget::PARAM_ITEMS_INHERIT_DATA=>0,
		array('htmlOptions'=>array('class'=>'cell-name pl1'), 'features'=>array('checkbox')),
		array('htmlOptions'=>array('class'=>'cell-value pl1'), 'features'=>array('2.label'))
	)));
	
	$this->widget(Widget::alias('ButtonsTable'), array('params'=>array('login'=>'submit', 'cancel'=>'command')));
	
$this->endWidget();

Html::registerScriptParams('confirmToggleId', false, false, Html::activeId($model, $agreement ? 'agreement' : 'secure'));
