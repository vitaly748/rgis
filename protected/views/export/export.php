<?php
echo Html::openTag('div');
	$this->widget(Widget::alias('DataTable'), array('featureModel'=>$model,
		TreeWidget::PARAM_ATTRIBUTES_FULL=>false,
		'attributes'=>array(
			'provider'=>array('features'=>array(
				'2.widget'=>array('featureModel'=>false, 'featureValue'=>reset(array_keys($id_responders)), 'widgetName'=>'DropDownList',
					'widgetParams'=>array('readonly'=>true, 'widthMin'=>500, 'widthMax'=>500, 'data'=>$id_responders))
			)),
			'period'=>array('features'=>array(
				'2.widget'=>array('featureModel'=>false, 'featureValue'=>end(array_keys($id_dates)), 'widgetName'=>'DropDownList',
					'widgetParams'=>array('readonly'=>true, 'widthInitial'=>true, 'data'=>$id_dates))
			))
		)
	));
echo Html::closeTag('div');

$this->widget(Widget::alias('ButtonsTable'), array('params'=>array('ok', 'cancel')));
