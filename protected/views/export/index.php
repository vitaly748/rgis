<?php
Yii::app()->asset->add(array('gridview'));

$this->widget(Widget::alias('Flashes'),
	array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

echo Html::openTag('div', array('class'=>'container-grid-view'));
	$this->renderPartial('indexGrid', array('model'=>$model));
	echo Html::tag('div', array('class'=>'curtain-loading'), '');
echo Html::closeTag('div');

if ($id_responders){
	$form = $this->beginWidget('ActiveForm', array('action'=>$this->createUrl('export/export')));
		
		echo Html::activeHiddenField($model, 'provider');
		echo Html::activeHiddenField($model, 'period');
		
		$params_bt = array();
		
		if ($id_dates)
			$params_bt['export'] = array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-package'));
		
		$params_bt = array_merge($params_bt, array(
			'save'=>array('type'=>'a', 'html_options'=>array('href'=>$this->createUrl('export/file'), 'target'=>'_blank',
				'class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1)),
			'delete'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled '.
				Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm', 'tabindex'=>-1))
		));
		
		$this->widget(Widget::alias('ButtonsTable'), array('params'=>$params_bt));
		
	$this->endWidget();
	
	if ($id_dates)
		$this->widget(Widget::alias('JuiDialog'), array('action'=>'export', 'reset'=>false,
			'htmlOptions'=>array('id'=>'juid-package'), 'buttonOk'=>'ok', 'buttonCancel'=>'cancel', 'idFormSubmit'=>$this->getFormId(),
			'relations'=>array(
				'input[name=provider]:checked'=>'input#'.Html::activeId($model, 'provider'),
				'input[name=period]:checked'=>'input#'.Html::activeId($model, 'period')
			)));
	
	$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm',
		'htmlOptions'=>array('id'=>'juid-confirm'), 'buttonOk'=>'yes', 'buttonCancel'=>'no'));
}
