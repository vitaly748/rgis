<?php
$data = $model->search();
$items = Html::tag('div', array('class'=>'gv-table'), '{items}');
$sizer = $data->totalItemCount > 10 ? Html::tag('div', array('class'=>'sizer'), Html::tag('p', array(), 'Показывать по ').
	Html::radioButtonList('filter-size', $data->pagination->pageSize, $model::FILTER_SIZE_VALUES,
	array('class'=>'filter-field', 'container'=>'div', 'separator'=>false,
	'template'=>Html::tag('span', array(), '{input}{label}')))) : '';
$controls = Html::tag('table', array('class'=>'gv-controls'), Html::tag('tr', array(),
	Html::tag('td', array('class'=>'gv-summary'), '{summary}').
	Html::tag('td', array('class'=>'gv-sizer'), $sizer).
	Html::tag('td', array('class'=>'gv-pager'), '{pager}')
));

$names = Arrays::forming(array('provider'=>Yii::app()->user->checkAccess('developer'), 'period, date, size'=>true), true);

foreach ($names as $name)
	$columns[] = array(
		'header'=>$model->getAttributeLabel($name),
		'name'=>$name,
		'type'=>'raw',
		'headerHtmlOptions'=>array('class'=>'nowrap'),
		'htmlOptions'=>Html::forming(array('nowrap'=>$name != 'provider', 'ta-center'=>$name != 'provider'))
	);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'export-grid',
	'dataProvider'=>$data,
	'ajaxType'=>'POST',
	'template'=>(Yii::app()->request->isAjaxRequest ? ($data->itemCount > 24 ? $controls : '').$items.$controls : $items).
		(!empty($filter_id) ? Html::tag('div', array('id'=>Html::ID_FILTER_ID), $filter_id) : ''),
	'cssFile'=>false,
	'beforeAjaxUpdate'=>'gridviewBeforeAjaxUpdate',
	'afterAjaxUpdate'=>'gridviewAfterAjaxUpdate',
	'ajaxUpdateError'=>'gridviewErrorAjaxUpdate',
	'emptyText'=>Yii::app()->label->get(Yii::app()->request->isAjaxRequest ? 'wgv_empty' : 'wgv_loading'),
	'summaryText'=>'Найдено: <strong>{count}</strong>',
	
	'columns'=>$columns,
	
	'pager'=>array(
		'cssFile'=>false,
		'hiddenPageCssClass'=>'dsp-none',
		'firstPageLabel'=>'',
		'prevPageLabel'=>'',
		'nextPageLabel'=>'',
		'lastPageLabel'=>'',
		'maxButtonCount'=>7,
		'header'=>''
	)
));
