<?php
Yii::app()->asset->add(array('gridview'));

$this->widget(Widget::alias('Flashes'),
	array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

echo Html::openTag('div', array('class'=>'container-grid-view'));
	$this->renderPartial('indexGrid', array('model'=>$model));
	echo Html::tag('div', array('class'=>'curtain-loading'), '');
echo Html::closeTag('div');

if ($filter_id)
	echo Html::tag('div', array('id'=>Html::ID_FILTER_ID), json_encode(array('counter'=>$filter_id)));

$params_bt = Yii::app()->user->checkAccessSet('citizen, organization') ? array('connection') : array();

if (Yii::app()->user->checkAccessSet('bss1_payer, bss2_payer'))
	$params_bt = array_merge($params_bt, array(
		'service'=>array('html_options'=>array('href'=>$this->createUrl('service/index'),
			'class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1)),
		'operation'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1)),
		'payment'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1)),
		'disconnection'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled '.
			Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm', 'tabindex'=>-1)),
		'house'=>array('html_options'=>array('href'=>$this->createUrl('house/general'))),
		'counter'=>array('html_options'=>array('href'=>$this->createUrl('counter/index'),
			'class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1))
	));

if (Yii::app()->user->checkAccess('appeal/create'))
	$params_bt['appeal'] = array('html_options'=>array('href'=>$this->createUrl('appeal/create'),
		'class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1));

if (Yii::app()->user->checkAccess('subsidy/index'))
	$params_bt['subsidy'] = array('html_options'=>array('href'=>$this->createUrl('subsidy/index')));

if (Yii::app()->user->checkAccess('compensation/index'))
	$params_bt['compensation'] = array('html_options'=>array('href'=>$this->createUrl('compensation/index')));

if ($params_bt){
	$this->widget(Widget::alias('ButtonsTable'), array('params'=>$params_bt));
	
	$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm',
		'htmlOptions'=>array('id'=>'juid-confirm'), 'buttonOk'=>'yes', 'buttonCancel'=>'no'));
}
