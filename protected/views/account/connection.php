<?php
Yii::app()->asset->add();

$form = $this->beginWidget('ActiveForm');
	
	$this->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model,
		'attrSeparation'=>array(
			array('type', 'executor', 'account', 'payer'),
			array('date', 'sum'),
			array('captchacode')
		),
		'attributes'=>array(
			'type'=>array('features'=>array(
				'2.widget'=>array('widgetName'=>'DropDownList', 'widgetParams'=>array(
					'widthInitial'=>true, 'readonly'=>true)),
				'2.error_static'
			)),
			'executor'=>array('features'=>array(
				'1.label'=>array('htmlOptions'=>array('disabled'=>false)),
				'2.widget'=>array('widgetName'=>'DropDownList', 'widgetParams'=>array(
					'widthInitial'=>true, 'disabled'=>true, 'data'=>array(10=>'ТСЖ Максима Горького 77',
						20=>'ФОНД КАПИТАЛЬНОГО РЕМОНТА ПК')))
			)),
			'date'=>array('features'=>array(
				'2.widget'=>array('widgetName'=>'DateTimePicker','widgetParams'=>array('mode'=>'date')),
				'2.error'
			)),
			'captchacode'=>array('features'=>array(
				'2.text'=>array('htmlOptions'=>array('au`tocomplete'=>'off', 'style'=>'width: 306px')),
				'2.widget'=>array('widgetName'=>'Captcha'),
				'2.error'
			))
		)
	));
	
	$this->widget(Widget::alias('ButtonsTable'), array('form'=>$form, 'model'=>$model));
	
$this->endWidget();

Html::registerScriptParams('connectionParams', false, false,
	array('type'=>Html::activeName($model, 'type'), 'executor'=>Html::activeName($model, 'executor')));
