<?php
Yii::app()->asset->add();

$this->widget(Widget::alias('Flashes'),
	array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

$params_bt = array('back'=>array('type'=>'a', 'html_options'=>array('href'=>$this->createUrl('account/payment'))));

if ($id){
	echo Html::tag('object', array(), Html::tag('embed',
		array('src'=>$this->createUrl($this->route, array('id'=>$id, 'file'=>1)), 'class'=>'loading')));
	
	$params_bt['save'] = array('type'=>'a',
		'html_options'=>array('href'=>$this->createUrl('', array('id'=>$id, 'file'=>'')), 'target'=>'_blank'));
	$params_bt['print'] = array('html_options'=>array('href'=>'#'));
	$params_bt['receipt'] = array('html_options'=>array('href'=>'#'));
}

$this->widget(Widget::alias('ButtonsTable'), array('params'=>$params_bt));
