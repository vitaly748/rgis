<?php
$items = Html::tag('div', array('class'=>'gv-table'), '{items}');

foreach (array('account', 'type', 'executor', 'address', 'area', 'share', 'balance') as $name)
	$columns[] = array(
		'header'=>$model->getAttributeLabel($name),
		'name'=>$name,
		'type'=>'raw',
		'headerHtmlOptions'=>array('class'=>'nowrap'),
		'htmlOptions'=>Html::forming(array('nowrap, ta-center'=>!in_array($name, array('executor', 'address'))))
	);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'accounts-grid',
	'dataProvider'=>$model->search(),
	'ajaxType'=>'POST',
	'template'=>$items,
	'cssFile'=>false,
	'beforeAjaxUpdate'=>'gridviewBeforeAjaxUpdate',
	'afterAjaxUpdate'=>'gridviewAfterAjaxUpdate',
	'ajaxUpdateError'=>'gridviewErrorAjaxUpdate',
	'emptyText'=>Yii::app()->label->get(Yii::app()->request->isAjaxRequest ? 'wgv_empty' : 'wgv_loading'),
	'columns'=>$columns
));
