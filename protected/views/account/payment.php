<?php
Yii::app()->asset->add();

if ($note = Yii::app()->label->get('note_payment'))
	echo Html::tag('div', array('class'=>'note'), Html::tag('div', array(), $note));

$attributes = array(
	'id'=>array('features'=>array(
		'1.label',
		'2.widget'=>array('widgetName'=>'DropDownList', 'widgetParams'=>array(
			'data'=>Arrays::focussing($account_info),
			'widthInitial'=>true,
			'readonly'=>true
		)),
		'2.error'
	))
);

$kinds = array(
	0=>'Специальный',
	1=>'Котловой',
	2=>'Специальный РО',
	3=>'Специальный ТСЖ',
	4=>'Заблокированный'
);

foreach ($account_info as $id_conjugation=>$params){
	$html_options = Html::forming(array('toggle', 'toggle-'.$id_conjugation, 'dsp-none'=>$id != $id_conjugation));
	
	$attributes = array_merge($attributes, array(
		array('features'=>array(
			'1.label'=>array('featureValue'=>$model->getAttributeLabel('kind')),
			'2.value'=>array('htmlOptions'=>array('class'=>'value-big value-wide'),
				'featureValue'=>Arrays::pop($kinds, (int)$params['kind']))
		), 'htmlOptions'=>$html_options),
		array('features'=>array(
			'1.label'=>array('featureValue'=>$model->getAttributeLabel('recipient')),
			'2.value'=>array('htmlOptions'=>array('class'=>'value-big value-wide'), 'featureValue'=>$params['recipient'])
		), 'htmlOptions'=>$html_options),
		array('features'=>array(
			'1.label'=>array('featureValue'=>$model->getAttributeLabel('bank')),
			'2.value'=>array('htmlOptions'=>array('class'=>'value-big value-wide'), 'featureValue'=>$params['bank'])
		), 'htmlOptions'=>$html_options),
		array('features'=>array(
			'1.label'=>array('featureValue'=>$model->getAttributeLabel('inn')),
			'2.value'=>array('htmlOptions'=>array('class'=>'value-big value-wide'), 'featureValue'=>$params['inn'])
		), 'htmlOptions'=>$html_options),
		array('features'=>array(
			'1.label'=>array('featureValue'=>$model->getAttributeLabel('kpp')),
			'2.value'=>array('htmlOptions'=>array('class'=>'value-big value-wide'), 'featureValue'=>$params['kpp'])
		), 'htmlOptions'=>$html_options),
		array('features'=>array(
			'1.label'=>array('featureValue'=>$model->getAttributeLabel('bik')),
			'2.value'=>array('htmlOptions'=>array('class'=>'value-big value-wide'), 'featureValue'=>$params['bik'])
		), 'htmlOptions'=>$html_options),
		array('features'=>array(
			'1.label'=>array('featureValue'=>$model->getAttributeLabel('bank_account')),
			'2.value'=>array('htmlOptions'=>array('class'=>'value-big value-wide'), 'featureValue'=>$params['bank_account'])
		), 'htmlOptions'=>$html_options),
		array('features'=>array(
			'1.label'=>array('featureValue'=>$model->getAttributeLabel('corr_account')),
			'2.value'=>array('htmlOptions'=>array('class'=>'value-big value-wide'), 'featureValue'=>$params['corr_account'])
		), 'htmlOptions'=>$html_options)
	));
}

$attributes = array_merge($attributes, array(
	'sum'=>array('features'=>array(
		'1.label',
		'2.text'=>array('htmlOptions'=>array('style'=>'width: 163px', 'disabled'=>'disabled')),
		'2.error'
	))
));
$account = $account_info[$id];

$form = $this->beginWidget('ActiveForm');
	
	if ($insert = Yii::app()->label->get('insert_requisites'))
		echo Html::tag('h4', array('class'=>'mt0'), $insert);
	
	$this->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model,
		'htmlOptions'=>array('class'=>'unrequired'), TreeWidget::PARAM_ATTRIBUTES_FULL=>false, 'attributes'=>$attributes));
	
	echo Html::tag('br').Html::tag('br');
	
	if ($insert = Yii::app()->label->get('insert_payment_method'))
		echo Html::tag('h4', array('class'=>'mt0'), $insert);
	
	echo Html::openTag('div', array('id'=>'payment-type'));
		
		echo Html::openTag('ul');
			
			foreach (Yii::app()->conformity->get('account_payment_type', 'description', 'name') as $name=>$description){
				echo Html::openTag('li', array('class'=>'li-payment-type-'.$name));
					echo Html::element(array('name'=>Html::activeName($model, 'type'), 'type'=>'radio',
						'htmlOptions'=>Html::forming(array('id'=>'payment-type-'.$name, 'value'=>$name, 'checked'=>$name == $model->type))));
					
					echo Html::openTag('label', array('for'=>'payment-type-'.$name));
						echo Html::openTag('div', array('class'=>'pt-info'));
							echo Html::tag('div', array('class'=>'pt-title'), $description);
							
							$commission = Arrays::pop($payment_commission, "$name.{$account['type']}.{$account['kind']}");
							
							echo Html::openTag('div', Html::forming(array('pt-commission', 'dsp-none'=>!$commission)));
								echo 'Комиссия: '.Html::tag('span', array(), $commission).'%';
							echo Html::closeTag('div');
							
						echo Html::closeTag('div');
						echo Html::tag('div', array('class'=>'pt-icon'), '');
					echo Html::closeTag('label');
				echo Html::closeTag('li');
			}
			
		echo Html::closeTag('ul');
		
	echo Html::closeTag('div');
	
	$this->widget(Widget::alias('ButtonsTable'), array('params'=>array('next'=>array('type'=>'a',
		'html_options'=>array('href'=>'#', 'class'=>'disabled', 'tabindex'=>-1)))));
	
$this->endWidget();

Html::registerScriptParams('paymentParams', false, false, array(
	'accountInfo'=>$account_info,
	'printUrl'=>$print_url,
	'bilsysUrl'=>$bilsys_url,
	'paymentCode'=>$payment_code,
	'paymentCommission'=>$payment_commission,
	'payer'=>$payer,
	'inputAccountName'=>Html::activeName($model, 'id'),
	'inputTypeName'=>Html::activeName($model, 'type'),
	'inputSumId'=>Html::activeId($model, 'sum')
));
