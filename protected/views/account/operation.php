<?php
Yii::app()->asset->add(array('gridview'));

$this->widget(Widget::alias('Flashes'),
	array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

echo Html::openTag('div', array('class'=>'filter'));
	
	echo Html::openTag('div', array('class'=>'filter-item fi-not-clean'));
		echo Html::label('Лицевой счёт', Widget::PREFIX_ID.'1');
		echo Html::hiddenField('filter-operation-account', $id, array('class'=>'filter-field'));
		$this->widget(Widget::alias('DropDownList'), array(
			'data'=>$id_accounts ? $id_accounts : array('empty'=>array('access'=>'view')),
			'name'=>'filter-account',
			'value'=>$id,
			'widthInitial'=>true,
			'readonly'=>true
		));
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'filter-item'));
		echo Html::label('Услуга', Widget::PREFIX_ID.'2');
		echo Html::hiddenField('filter-operation-service', '', array('class'=>'filter-field'));
		$this->widget(Widget::alias('DropDownList'), array(
// 			'data'=>array('empty'=>array('access'=>'view')),
			'data'=>array(1=>'Не выбрано'),
			'name'=>'filter-service',
			'widthMin'=>240,
			'widthMax'=>240,
			'readonly'=>true,
			'itemEmptyEnabled'=>true
		));
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'filter-item'));
		echo Html::label('Начало периода', Widget::PREFIX_ID.'3');
		echo Html::hiddenField('filter-operation-date-from', '', array('class'=>'filter-field'));
		$this->widget(Widget::alias('DateTimePicker'), array(
			'name'=>'filter-date-from',
			'mode'=>'date'
		));
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'filter-item'));
		echo Html::label('Конец периода', Widget::PREFIX_ID.'5');
		echo Html::hiddenField('filter-operation-date-to', '', array('class'=>'filter-field'));
		$this->widget(Widget::alias('DateTimePicker'), array(
			'name'=>'filter-date-to',
			'mode'=>'date'
		));
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'filter-buttons'));
		echo Html::link('', '#', array('class'=>'button search contur circle'));
		echo Html::link('', '#', array('class'=>'button clear contur circle'));
	echo Html::closeTag('div');
	
echo Html::closeTag('div');

echo Html::openTag('div', array('class'=>'container-grid-view'));
	$this->renderPartial('operationGrid', array('model'=>$model));
	echo Html::tag('div', array('class'=>'curtain-loading'), '');
echo Html::closeTag('div');

$span = Html::tag('span', array('class'=>'dsp-none'), ' по услуге');
echo Html::tag('div', array('class'=>'operation-total'),
	'Баланс на начало периода'.$span.': '.Html::tag('strong', array('class'=>'total-start'), Numbers::format(0)).Html::tag('br').
	'Баланс на конец периода'.$span.': '.Html::tag('strong', array('class'=>'total-end'), Numbers::format(0)).Html::tag('br').
	'Итого к оплате'.$span.': '.Html::tag('strong', array('class'=>'total-pay'), Numbers::format(0)));

$this->widget(Widget::alias('ButtonsTable'), array('params'=>array('payment')));
