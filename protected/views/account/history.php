<?php
Yii::app()->asset->add();

$result = Yii::app()->bss->query('user_accounts', array(
	'id'=>Yii::app()->user->model->conjugations['bss'],
	'organization'=>Yii::app()->user->checkAccess('organization'),
	'type'=>0
));

if (!$account_list = Arrays::pop($result, 'account_list'))
	echo Html::tag('div', array(), 'Нет лицевых счетов');
else{
	$result2 = Yii::app()->bss->query('payment_history', array('id'=>$account_list[0]['id']));
	
	if (!$payment_list = Arrays::pop($result2, 'payment_list'))
		echo Html::tag('div', array(), 'Нет данных');
	else{
		echo Html::openTag('table', array('id'=>'accounts'));
			echo Html::openTag('thead');
				echo Html::tag('th', array(), 'Дата');
				echo Html::tag('th', array(), 'Тип');
				echo Html::tag('th', array(), 'Платёжная система');
				echo Html::tag('th', array(), 'Сумма платежа');
			echo Html::closeTag('thead');
			echo Html::openTag('tbody');
				
				foreach ($payment_list as $payment){
					echo Html::openTag('tr');
						
						echo Html::tag('td', array(), Swamper::date(Arrays::pop($payment, 'date'), Swamper::FORMAT_DATE_USER_WITHOUT_TIME));
						echo Html::tag('td', array(), Arrays::pop($payment, 'type'));
						echo Html::tag('td', array(), Arrays::pop($payment, 'payment_system'));
						echo Html::tag('td', array(), Numbers::format(Arrays::pop($payment, 'sum')));
						
					echo Html::closeTag('tr');
				}
			echo Html::closeTag('tbody');
		echo Html::closeTag('table');
	}
}
