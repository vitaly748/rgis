$(window).load(function(){
	setTimeout(function(){
		$('embed').removeClass('loading');
	}, 3000);
	
	$(document).on('click', 'a.button.print', function(event){
		event.preventDefault();
		
		var w = window.open($('embed').attr('src'), 'print',
			'left=0,top=0,width=800,height=700,toolbar=0,scrollbars=1,status=1');
		
		w.print();
	});
	
	$(document).on('click', 'a.button.receipt', function(event){
		var button = $(this);
		
		event.preventDefault();
		wFlashesToggle('ok', false);
		wFlashesToggle('error', false);
		buttonToggle(button, false);
		
		$.ajax({
			url: location + '/file/2',
			dataType: 'json',
			cache: false,
			success: function(respond, textStatus, jqXHR){
				if (!respond)
					jqXHR.status = 500;
			},
			complete: function(jsXHR, textStatus){
				var error = jsXHR.status != 200;
				
				if (typeof wFlashesSet != 'undefined')
					wFlashesSet(error ? 'error' : 'ok', error ?
						'Ошибка при отправке квитанции' : 'Квитанция отправлена на почту');
				
				buttonToggle(button, true);
			}
		});
	});
});
