$(window).load(function(){
	if (typeof connectionParams != 'undefined')
		$('input[name=' + shielding(connectionParams.type) + ']').on('change', function(){
			var inputs = $('input[name=' + shielding(connectionParams.executor) + ']'),
				input = inputs.filter('[value=' + $(this).val() + ']');
			
			inputs.attr('checked', false);
			input.attr('checked', true);
			wDropDownListTitleRepaint(input.closest('.drop-down-list'));
		})
});
