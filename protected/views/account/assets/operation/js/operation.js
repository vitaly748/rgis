accountOperationGVParams = null;

function gridviewAfterAjaxUpdateHandler(){
	var ddlId = 'ywp2',
		ddl = $('div.drop-down-list#' + ddlId),
		ul = ddl.children('ul'),
		items = ul.children('li.ddl-item'),
		valueCurrent = items.find('input[name=filter-service]:checked').val(),
		balanceStart = '0,00р',
		balanceEnd = '0,00р',
		service = true;
	
	if (accountOperationGVParams != gridviewParams){
		var itemEmpty = items.filter('.ddl-empty'),
			find = false;
		
		items.not('.ddl-empty').detach();
		
		if (gridviewParams.services){
			for (key in gridviewParams.services){
				var item = itemEmpty.clone(true).removeClass('ddl-empty').removeClass('active'),
					id = 'ddl-input-' + ddlId + '-' + key,
					value = gridviewParams.services[key],
					checked = value == valueCurrent;
				
				if (checked){
					item.addClass('active');
					find = true;
				}
				
				item.children('input').removeClass('ddl-input-empty').addClass('ddl-input').
					attr('id', id).attr('checked', checked).val(value);
				item.children('label').attr('for', id).html(value);
				ul.append(item);
			}
			
			if (valueCurrent && !find){
				itemEmpty.addClass('active').children('input').attr('checked', true);
				ddl.find('input.ddl-title').val(itemEmpty.children('label').html());
				valueCurrent = false;
			}
		}
		
		accountOperationGVParams = gridviewParams;
	}
	
	if (!valueCurrent || valueCurrent == 'ddl-input-empty'){
		if (accountOperationGVParams.bstart)
			balanceStart = accountOperationGVParams.bstart;
		
		if (accountOperationGVParams.bend)
			balanceEnd = accountOperationGVParams.bend;
		
		service = false;
	}else if (accountOperationGVParams.bservices && valueCurrent in accountOperationGVParams.bservices){
		balanceStart = accountOperationGVParams.bservices[valueCurrent][0];
		balanceEnd = accountOperationGVParams.bservices[valueCurrent][1];
	}
	
	$('div.operation-total strong.total-start').html(balanceStart);
	$('div.operation-total strong.total-end').html(balanceEnd);
	$('div.operation-total strong.total-pay').html(balanceEnd.substr(0, 1) == '-' ? balanceEnd.substr(1) : '0,00р');
	$('div.operation-total span').toggleClass('dsp-none', !service);
}

$(window).load(function(){
	$(document).on('click', '.button.payment', function(event){
		location = $(this).attr('href') + '/' + $('input[name=filter-operation-account]').val();
		event.preventDefault();
	});
});
