paymentClassToggle = 'toggle';
paymentPrefixToggle = paymentClassToggle + '-';
paymentAccountTypeBss2 = 2;
paymentTypeBank = 'bank';
paymentTypePrefixLi = 'li-payment-type';

function accountPaymentCheck(valueAccount, valueType){
	setTimeout(function(){
		var button = $('.button.next');
		
		if (button.length){
			var accountParams = paymentParams.accountInfo[valueAccount],
				inputSum = $('input#' + paymentParams.inputSumId).filter(':visible'),
				divError = inputSum.parent().children('div.errorMessage'),
				enabled = false;
			
			if (accountParams && accountParams.type === paymentAccountTypeBss2 &&
				accountParams.kind in paymentParams.paymentCode && inputSum.length && divError.length &&
				(valueType == paymentTypeBank || (inputSum.val() && !divError.is(':visible')))){
					enabled = true;
					button.attr('href', valueType === paymentTypeBank ? paymentParams.printUrl + '/' + valueAccount :
						sprintf(paymentParams.bilsysUrl, valueType, paymentParams.paymentCode[accountParams.kind],
						parseFloat(inputSum.val().replace(' ', '').replace(',', '.')) * 100, accountParams.account,
						paymentParams.payer));
				}
			
				buttonToggle(button, enabled);
				button.attr('target', enabled && valueType !== paymentTypeBank ? '_blank' : null);
		}
	}, 300);
}

$(window).load(function(){
	if (typeof paymentParams !== 'undefined'){
		var nameAccount = shielding(paymentParams.inputAccountName),
			nameType = shielding(paymentParams.inputTypeName),
			inputAccount = $('input[name=' + nameAccount + ']'),
			inputType = $('input[name=' + nameType + ']'),
			inputSum = $('input#' + paymentParams.inputSumId);
		
		inputAccount.on('change', function(){
			var value = parseInt($(this).val()),
				tr = $('div.data-table tr.' + paymentClassToggle),
				accountParams = paymentParams.accountInfo[value];
			
			if (value && tr.length){
				tr.addClass('dsp-none');
				tr.filter('.' + paymentPrefixToggle + value).removeClass('dsp-none');
				
				if (accountParams)
					inputSum.val(accountParams.sum);
			}
			
			$('div#payment-type > ul > li').each(function(){
				var name = parseId($(this), paymentTypePrefixLi),
					divCommission = $(this).find('div.pt-commission'),
					accountParams = paymentParams.accountInfo[value];
				
				if (name && name.length && divCommission.length && accountParams){
					var commission = name[0] in paymentParams.paymentCommission &&
						accountParams.type in paymentParams.paymentCommission[name[0]] &&
						accountParams.kind in paymentParams.paymentCommission[name[0]][accountParams.type] ?
						paymentParams.paymentCommission[name[0]][accountParams.type][accountParams.kind] : false;
					
					divCommission.children('span').html(commission);
					divCommission.toggleClass('dsp-none', !commission);
				}
			});
			
			accountPaymentCheck(value, inputType.filter(':checked').val());
		});
		
		inputType.on('change', function(){
			var value = $(this).val();
			
			inputSum.attr('disabled', value === paymentTypeBank);
			inputCheckLabel(inputSum);
			accountPaymentCheck(parseInt(inputAccount.filter(':checked').val()), value);
		});
		
		inputSum.on('change', function(){
			accountPaymentCheck(parseInt(inputAccount.filter(':checked').val()), inputType.filter(':checked').val());
		});
		
		accountPaymentCheck(parseInt(inputAccount.filter(':checked').val()), inputType.filter(':checked').val());
	}
});
