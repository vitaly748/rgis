<?php
$data_search = $model->search();
$data = $data_search['data'];
$items = Html::tag('div', array('class'=>'gv-table'), '{items}');
$sizer = $data->totalItemCount > 10 ? Html::tag('div', array('class'=>'sizer'), Html::tag('p', array(), 'Показывать по ').
	Html::radioButtonList('filter-size', $data->pagination->pageSize, $model::FILTER_SIZE_VALUES,
	array('class'=>'filter-field', 'container'=>'div', 'separator'=>false,
	'template'=>Html::tag('span', array(), '{input}{label}')))) : '';
$controls = Html::tag('table', array('class'=>'gv-controls'), Html::tag('tr', array(),
	Html::tag('td', array('class'=>'gv-summary'), '{summary}').
	Html::tag('td', array('class'=>'gv-sizer'), $sizer).
	Html::tag('td', array('class'=>'gv-pager'), '{pager}')
));

// foreach (array('date', 'service', 'operation', 'parish', 'spend', 'sum', 'balance') as $name)
foreach (array('date', 'service', 'operation', 'parish', 'spend') as $name)
	$columns[] = array(
		'header'=>$model->getAttributeLabel($name),
		'name'=>$name,
		'headerHtmlOptions'=>array('class'=>'nowrap'),
		'htmlOptions'=>Html::forming(array('nowrap'=>!in_array($name, array('service', 'operation')), 'ta-center'))
	);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'operations-grid',
	'dataProvider'=>$data,
	'filterSelector'=>'input.filter-field',
	'ajaxType'=>'POST',
	'template'=>(Yii::app()->request->isAjaxRequest ? ($data->itemCount > 24 ? $controls : '').$items.$controls : $items).
		(isset($data_search['params']) ?
		Html::tag('div', array('id'=>Html::ID_GV_PARAMS), json_encode($data_search['params'])) : ''),
	'cssFile'=>false,
	'beforeAjaxUpdate'=>'gridviewBeforeAjaxUpdate',
	'afterAjaxUpdate'=>'gridviewAfterAjaxUpdate',
	'ajaxUpdateError'=>'gridviewErrorAjaxUpdate',
	'emptyText'=>Yii::app()->label->get(Yii::app()->request->isAjaxRequest ? 'wgv_empty' : 'wgv_loading'),
	'summaryText'=>'Найдено: <strong>{count}</strong>',
	'selectableRows'=>0,
	'columns'=>$columns,
	
	'pager'=>array(
		'cssFile'=>false,
		'hiddenPageCssClass'=>'dsp-none',
		'firstPageLabel'=>'',
		'prevPageLabel'=>'',
		'nextPageLabel'=>'',
		'lastPageLabel'=>'',
		'maxButtonCount'=>7,
		'header'=>''
	)
));
