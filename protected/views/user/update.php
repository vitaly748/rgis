<?php
Yii::app()->asset->add(Arrays::forming(array('button', 'input',
	':fias'=>array(Arrays::FORMING_PARAMS_ENABLED=>Yii::app()->fias->activate()))));

$ddl_data = $model->accessValues['roles'];
$multiple = ($roles_view = Arrays::select($ddl_data, 'name', false, false, 'view, update', 'access')) !=
	($roles_radio = Arrays::select($ddl_data, 'name', false, false, true, 'radio'));
$create = $model->action == 'create';

if ($multiple && $roles_radio)
	$ddl_data = array_merge(Arrays::filterKey($ddl_data, $roles_radio), array(false),
		Arrays::filterKey($ddl_data, $roles_radio, false));

if ($action == 'registration' && ($note = Yii::app()->label->get('note_registration')))
	echo Html::tag('div', array('class'=>'note'), Html::tag('div', array(), $note));

$form = $this->beginWidget('ActiveForm');
	
	$this->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model,
		TreeWidget::PARAM_ATTRIBUTES_FULL=>false,
		'blocks'=>array(
			array(
				'attributes'=>array(
					'roles'=>array('features'=>array(
						'1.label'=>array('htmlOptions'=>array('disabled'=>false)),
						'2.widget'=>array('widgetName'=>'DropDownList', 'widgetParams'=>array(
							'data'=>$ddl_data,
							'multiple'=>$multiple,
							'widthInitial'=>!$multiple,
							'widthMin'=>$multiple ? 240 : false,
							'widthMax'=>$multiple ? 240 : false,
							'readonly'=>count($roles_view) < 10,
						)),
						'2.error_static'
					)),
					'bsscode'=>array('features'=>array('2.text'=>array('htmlOptions'=>array('autocomplete'=>'off')), '2.error')),
					'state'=>array('features'=>array(
						'1.label'=>array('htmlOptions'=>array('disabled'=>false)),
						'2.widget'=>array('widgetName'=>'DropDownList', 'widgetParams'=>array(
							'widthInitial'=>true,
							'readonly'=>true)),
						'2.error_static'
					)),
					'registration'=>array('features'=>array('1.label'=>array('htmlOptions'=>array('disabled'=>false)),
						'2.value'=>array('htmlOptions'=>array('class'=>'value-wide value-update')))),
					'locked'=>array('attrHidden'=>(int)$model->state !== $user_states['locked'], 'features'=>array(
						'2.widget'=>array('widgetName'=>'DateTimePicker'),
						'2.error'
					)),
					'delete'=>array('attrHidden'=>(int)$model->state !== $user_states['remote'] || !$model->delete,
						'features'=>array('1.label'=>array('htmlOptions'=>array('disabled'=>false)),
							'2.value'=>array('htmlOptions'=>array('class'=>'value-wide value-update')))),
				)
			),
			array(
				'attributes'=>array(
					'login'=>array('features'=>array('2.text'=>array('htmlOptions'=>array('autocomplete'=>'off')), '2.error')),
					'password'=>array('features'=>array(
						'1.label'=>array('featureModel'=>false, 'featureValue'=>$model->getAttributeLabel('password',
							(int)(!$create && $model->passwordHash)),
							'htmlOptions'=>Html::forming(array('required'=>$model->isAttributeRequired('password'),
							'for'=>Html::activeId($model, 'password')))),
						'1.tooltip',
						'2.password'=>array('htmlOptions'=>array('autocomplete'=>'off', 'style'=>'width: 390px', 'readonly'=>'readonly')),
						'2.widget'=>array('widgetName'=>'DifficultyMeter'),
						'2.error'
					)),
					'password2'=>array('attrHidden'=>!$create && !$model->password, 'features'=>array(
						'2.password'=>array('htmlOptions'=>array('autocomplete'=>'off', 'style'=>'width: 390px', 'readonly'=>'readonly')),
						'2.error')),
					'passwordcode'=>array('attrHidden'=>$create || !$model->password, 'features'=>array(
						'2.password'=>array('htmlOptions'=>array('autocomplete'=>'off', 'style'=>'width: 390px', 'readonly'=>'readonly')),
						'2.error')),
				)
			),
			array(
				'attributes'=>array(
					'surname, name, patronymic'=>array('attrHidden'=>!in_array(Arrays::select($ddl_data, 'code', 'citizen'),
						$model->roles), 'features'=>array(
							'1.label'=>array('htmlOptions'=>array('disabled'=>false))
						)
					)
				)
			),
			array(
				'attributes'=>array(
					'title'=>array('attrHidden'=>!in_array(Arrays::select($ddl_data, 'code', 'organization'), $model->roles),
						'features'=>array(
							'1.label'=>array('htmlOptions'=>array('disabled'=>false))
						)
					)
				)
			),
			array(
				'attributes'=>array(
					'address'=>array('features'=>array('2.hidden'=>array('featureName'=>'addressid'), '2.text', '2.error')),
					'email',
					'phone'
				)
			),
			array(
				'attributes'=>array(
					'captchacode'=>array('features'=>array(
						'2.text'=>array('htmlOptions'=>array('autocomplete'=>'off', 'style'=>'width: 306px')),
						'2.widget'=>array('widgetName'=>'Captcha'),
						'2.error'
					)),
					'emailcode, phonecode'=>array('attrHidden'=>true,
						'cells'=>array(array('features'=>array('1.hidden')), TreeWidget::PARAM_ITEMS_INHERIT_DATA=>0))
				)
			),
			array(
				'blockSeparate'=>true,
				'attributes'=>array(
					'agreement'=>array('cells'=>array(TreeWidget::PARAM_ITEMS_INHERIT_DATA=>0,
						array(
							'htmlOptions'=>array('class'=>'cell-name va-top pl1'),
							'features'=>array('checkbox')
						),
						array(
							'htmlOptions'=>array('class'=>'cell-value pl1'),
							'features'=>array(
								'2.label'=>array('featureModel'=>false, 'featureValue'=>sprintf(Yii::app()->label->get('label_agreement'),
									Yii::app()->baseUrl.'/assets/user_agreement.pdf'),
									'htmlOptions'=>array('for'=>Html::activeId($model, 'agreement'))),
								'2.error'
							)
						)
					))
				)
			)
		)
	));
	
	if (in_array($action, array('update', 'pupdate')))
		$params_bt[] = 'save';
	else
		$params_bt[] = 'ok';
	
	$params_bt['delete'] = array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm'));
	$this->widget(Widget::alias('ButtonsTable'), array('form'=>$form, 'model'=>$model, 'mergeParams'=>false,
		'params'=>$params_bt));
	
$this->endWidget();

if (!$create){
// 	if ($model->passwordHash)
	Html::registerScriptParams('passwordParams', false, false, Html::activeIds($model,
		Arrays::forming(array('password'=>true, 'password2'=>true, 'passwordcode'=>$model->passwordHash), true)));
	
	$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm',
		'htmlOptions'=>array('id'=>'juid-confirm'), 'buttonOk'=>'yes', 'buttonCancel'=>'no'));
}
