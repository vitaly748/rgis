<?php
Yii::app()->asset->add(array('gridview'));

$this->widget(Widget::alias('Flashes'),
	array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

echo Html::openTag('div', array('class'=>'filter'));
	
	echo Html::openTag('div', array('class'=>'filter-item'));
		echo Html::label('ФИО', 'filter-user-title');
		echo Html::textField('filter-user-title', '', array('class'=>'filter-field'));
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'filter-item'));
		echo Html::label('Группа', Widget::PREFIX_ID.'1');
		echo Html::hiddenField('filter-user-roles', '', array('class'=>'filter-field'));
		$this->widget(Widget::alias('DropDownList'), array(
			'data'=>Yii::app()->user->getRoles(0, 'description', 'id', 'guest'),
			'widthMin'=>250,
			'widthMax'=>250,
			'multiple'=>true,
			'readonly'=>true,
			'itemAllEnabled'=>true
		));
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'filter-item'));
		echo Html::label('Статус', Widget::PREFIX_ID.'2');
		echo Html::hiddenField('filter-user-state', '', array('class'=>'filter-field'));
		$this->widget(Widget::alias('DropDownList'), array(
			'data'=>Yii::app()->conformity->get('user_state', 'description', 'code', 'confirm_admin'),
			'widthMin'=>230,
			'widthMax'=>230,
			'multiple'=>true,
			'readonly'=>true,
			'itemAllEnabled'=>true
		));
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'filter-buttons'));
		echo Html::link('', '#', array('class'=>'button search contur circle'));
		echo Html::link('', '#', array('class'=>'button clear contur circle'));
	echo Html::closeTag('div');
	
echo Html::closeTag('div');

echo Html::openTag('div', array('class'=>'container-grid-view'));
	$this->renderPartial('indexGrid', array('model'=>$model));
	echo Html::tag('div', array('class'=>'curtain-loading'), '');
echo Html::closeTag('div');

$params_bt = array('view'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1)));

if (Yii::app()->user->checkAccess('developer') || Yii::app()->user->checkAccess('administrator')){
	$params_bt = array('create') + $params_bt;
	$params_bt['update'] = array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1));
	$params_bt['delete'] = array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled '.
		Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm', 'tabindex'=>-1));
}

$this->widget(Widget::alias('ButtonsTable'), array('params'=>$params_bt));

$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm',
	'htmlOptions'=>array('id'=>'juid-confirm'), 'buttonOk'=>'yes', 'buttonCancel'=>'no'));
