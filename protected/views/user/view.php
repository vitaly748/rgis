<?php
Yii::app()->asset->add(array('button'));

$roles = Yii::app()->user->getRoles($model->id, 'name');

$this->widget(Widget::alias('DataTable'), array('featureModel'=>$model, 'htmlOptions'=>array('class'=>'unrequired'),
	'attrSeparation'=>array(
		array('roles, state, registration, locked, delete'),
		array('login'),
		array('surname, name, patronymic, title', 'powers'),
		array('address, email, phone')
	),
	'attributes'=>array(
		'locked'=>array('attrHidden'=>(int)$model->state !== Yii::app()->conformity->get('user_state', 'code', 'locked')),
		'delete'=>array('attrHidden'=>(int)$model->state !== Yii::app()->conformity->get('user_state', 'code', 'remote')),
		'surname, name, patronymic'=>array('attrHidden'=>!in_array('citizen', $roles)),
		'title'=>array('attrHidden'=>!in_array('organization', $roles)),
		'powers'=>array('attrHidden'=>!in_array('organization', $roles) || !$model->powers)
	),
	'features'=>array(TreeWidget::PARAM_ITEMS_PRIORITY=>2,
		'1.label'=>array('htmlOptions'=>array('disabled'=>false)),
		'2.value'=>array('htmlOptions'=>array('class'=>'value-big value-wide'))
	)
));

$this->widget(Widget::alias('ButtonsTable'), array('model'=>$model,
	'params'=>array('delete'=>array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm')))));

$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm',
	'htmlOptions'=>array('id'=>'juid-confirm'), 'buttonOk'=>'yes', 'buttonCancel'=>'no'));
