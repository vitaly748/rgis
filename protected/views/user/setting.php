<?php
Yii::app()->asset->add(array('button'));

$form = $this->beginWidget('ActiveForm', array('command'=>true/* , 'htmlOptions'=>array('class'=>'w100') */));
	
	$this->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model,
		'htmlOptions'=>array('class'=>'unrequired'), TreeWidget::PARAM_ATTRIBUTES_FULL=>false,
		'attributes'=>array(
			'delivery'=>array('features'=>array(
				'2.widget'=>array('widgetName'=>'DropDownList', 'widgetParams'=>array('widthMin'=>300, 'widthMax'=>300,
					'readonly'=>true)),
				'2.error_static'
			)),
			'esia'=>array('features'=>array(
				'1.label'=>array('htmlOptions'=>array('disabled'=>false)),
				'2.value'=>array('featureModel'=>false, 'featureValue'=>Yii::app()->label->get('insert_is'.
					($model->esia ? '' : 'no')), 'htmlOptions'=>array('class'=>'value-big value-wide'))
			))
		)));
	
	$this->widget(Widget::alias('ButtonsTable'), array('form'=>$form, 'model'=>$model, 'mergeParams'=>false, 'params'=>array(
		'save', 'esiadone'=>array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm')))));
	
$this->endWidget();

if ($model->esia)
	$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm', 'htmlOptions'=>array('id'=>'juid-confirm'),
		'buttonOk'=>'yes', 'buttonCancel'=>'no', 'params'=>array('label'=>'user_esia')));
