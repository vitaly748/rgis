<?php
$data = $model->search();
$items = Html::tag('div', array('class'=>'gv-table'), '{items}');
$sizer = $data->totalItemCount > 10 ? Html::tag('div', array('class'=>'sizer'), Html::tag('p', array(), 'Показывать по ').
	Html::radioButtonList('filter-size', $data->pagination->pageSize, $model::FILTER_SIZE_VALUES,
	array('class'=>'filter-field', 'container'=>'div', 'separator'=>false,
	'template'=>Html::tag('span', array(), '{input}{label}')))) : '';
$controls = Html::tag('table', array('class'=>'gv-controls'), Html::tag('tr', array(),
	Html::tag('td', array('class'=>'gv-summary'), '{summary}').
	Html::tag('td', array('class'=>'gv-sizer'), $sizer).
	Html::tag('td', array('class'=>'gv-pager'), '{pager}')
));

if (($rows = $data->getData()) && ($roles_list = Yii::app()->user->rolesList) &&
	($developer = Arrays::pop($roles_list, 'developer.description')) &&
	($administrator = Arrays::pop($roles_list, 'administrator.description'))){
		$this_developer = Yii::app()->user->checkAccess('developer');
		$this_administrator = Yii::app()->user->checkAccess('administrator');
		$filter_id_delete = $filter_id_update = array();
		$state_remote = Yii::app()->conformity->get('user_state', 'description', 'remote');
		
		foreach ($rows as $row){
			list($id, $roles, $state) = Arrays::pop($row, 'id, roles, state');
			
			if ($roles){
				$roles = Strings::devisionWords($roles);
				$is_developer = in_array($developer, $roles);
				$is_administrator = in_array($administrator, $roles);
				
				if ($this_developer && $is_developer)
					$filter_id_delete[] = $id;
				elseif ($this_administrator)
					if (($is_developer || $is_administrator) && $id != Yii::app()->user->id)
						$filter_id_update[] = $filter_id_delete[] = $id;
				
				if ($state == $state_remote)
					$filter_id_delete[] = $id;
			}
		}
		
		$filter_id = json_encode(array('update'=>$filter_id_update, 'delete'=>$filter_id_delete));
	}

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$data,
	'filterSelector'=>'input.filter-field',
	'ajaxType'=>'POST',
	'template'=>(Yii::app()->request->isAjaxRequest ? ($data->itemCount > 24 ? $controls : '').$items.$controls : $items).
		(!empty($filter_id) ? Html::tag('div', array('id'=>Html::ID_FILTER_ID), $filter_id) : ''),
	'cssFile'=>false,
	'beforeAjaxUpdate'=>'gridviewBeforeAjaxUpdate',
	'afterAjaxUpdate'=>'gridviewAfterAjaxUpdate',
	'ajaxUpdateError'=>'gridviewErrorAjaxUpdate',
	'emptyText'=>Yii::app()->label->get(Yii::app()->request->isAjaxRequest ? 'wgv_empty' : 'wgv_loading'),
	'summaryText'=>'Найдено: <strong>{count}</strong>',
	
	'columns'=>array(
		array(
			'header'=>'ФИО / Название',
			'name'=>'title'
		),
		array(
			'name'=>'roles'
		),
		array(
			'name'=>'state',
			'htmlOptions'=>array('class'=>'nowrap ta-center')
		),
		array(
			'name'=>'registration',
			'htmlOptions'=>array('class'=>'nowrap ta-center')
		)
	),
	
	'pager'=>array(
		'cssFile'=>false,
		'hiddenPageCssClass'=>'dsp-none',
		'firstPageLabel'=>'',
		'prevPageLabel'=>'',
		'nextPageLabel'=>'',
		'lastPageLabel'=>'',
		'maxButtonCount'=>7,
		'header'=>''
	)
));
