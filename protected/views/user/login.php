<?php
Yii::app()->asset->add(array('button', 'flash_panel', 'input'));

$guest = Yii::app()->user->isGuest;
$login = Yii::app()->user->checkAccess('*/login');

if (!Yii::app()->user->getState('confirm')){
    echo Html::openTag('div', array('id'=>'profile', 'class'=>$login ? 'profile-login' : ''));
        if ($login){
            if ($this->route === 'main/index') {
                echo Html::link(Yii::app()->label->get('button_auth'), '#', Html::forming(
                    array('button-main', Html::PREFIX_BUTTON_STARTER_FP.'-profile', 'fix, '.Html::CLASS_BUTTON_FP_POPUP=>$guest)));

                echo Html::openTag('div', array('class'=>'flash-panel', 'id'=>Html::PREFIX_ID_FLASH_PANEL.'profile'));
                    $scheme_item = Yii::app()->scheme->getSchemeItem('*/login');

                    echo Html::tag('h2', array('class'=>'mb2'), $scheme_item['title']);

                    $form = $this->beginWidget('ActiveForm', array('id'=>'profile'));

                        $this->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model,
                            'htmlOptions'=>array('class'=>'unrequired'),
                            'attributes'=>array(
                                'login'=>array('features'=>array('2.text'=>array('htmlOptions'=>array('autocomplete'=>'off')), '2.error')),
                                'password'=>array('features'=>array('2.password'=>array('htmlOptions'=>array('autocomplete'=>'off')), '2.error'))
                            )
                        ));

                        if (Yii::app()->esia->on)
                            $params_bt['esia'] = array('html_options'=>array('href'=>$this->createUrl('esia/login')));

                        $params_bt['login'] = array('type'=>'ajax_submit', 'ajax_params'=>array('url'=>$scheme_item['_link'],
                            'dataType'=>'json', 'beforeSend'=>'loginBeforeSend', 'success'=>'loginSuccess', 'error'=>'loginError',
                            'context'=>'js:$(this)'));


                        $this->widget(Widget::alias('ButtonsTable'), array('form'=>$form, 'model'=>$model, 'mergeParams'=>false,
                            'params'=>$params_bt, 'horizontalAlign'=>'right'));

                        if (Yii::app()->email->on)
                            echo Html::link(Yii::app()->label->get('user_restore'), $this->createUrl('user/restore'),
                                array('class'=>'user-restore'));

                    $this->endWidget();

                    echo Html::tag('div', array('class'=>'curtain-loading'), '');
                echo Html::closeTag('div');
            }
        }else{
            echo Html::openTag('div', array('class'=>'profile-info'));

                echo Html::tag('div', array('class'=>'hello'), sprintf(Yii::app()->label->get('insert_hello'),
                    Strings::userName(Yii::app()->user->model, Strings::FORMAT_USER_NAME_INITIALS)));

                $scheme_item = Yii::app()->scheme->getSchemeItem('*/login');
                echo Html::tag('a', array('class'=>'link-profile', 'href'=>$this->createUrl('profile/view')),
                    Html::tag('span', array(), Yii::app()->label->get('insert_profile')));
                echo Html::tag('a', array('class'=>'link-logout', 'href'=>$this->createUrl('user/logout')),
                    Html::tag('span', array(), Yii::app()->label->get('insert_logout')));
    // 			echo Html::link(Yii::app()->label->get('insert_logout'), $this->createUrl('user/logout'), array('class'=>'link-logout'));
                /* echo Html::tag('h3', array(), Strings::userName(Yii::app()->user->model));

                echo Html::element(array('type'=>'value', 'model'=>Yii::app()->user->model, 'name'=>'roles',
                    'htmlOptions'=>array('class'=>'value-list value-big'))); */

            echo Html::closeTag('div');

            /* $this->widget(Widget::alias('ButtonsTable'), array('params'=>array('logout'=>array('html_options'=>array(
                'href'=>$this->createUrl('user/logout')))), 'mergeParams'=>false, 'class'=>'mono', 'horizontalAlign'=>'right')); */
        }
    echo Html::closeTag('div');
}
