<?php
Yii::app()->asset->add(array('button', 'input'));

if (!$code && ($note = Yii::app()->label->get('note_restore')))
	echo Html::tag('div', array('class'=>'note'), Html::tag('div', array(), $note));

$form = $this->beginWidget('ActiveForm');
	
	$this->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model,
		TreeWidget::PARAM_ATTRIBUTES_FULL=>false,
		'attributes'=>!$code ?
			array(
				'restore'=>array('features'=>array()),//!
				'captchacode'=>array('features'=>array(
					'2.text'=>array('htmlOptions'=>array('autocomplete'=>'off', 'style'=>'width: 306px')),
					'2.widget'=>array('widgetName'=>'Captcha'),
					'2.error'
				))
			) : array(
				'login'=>array('features'=>array('1.label'=>array('htmlOptions'=>array('disabled'=>false)),
					'2.value'=>array('htmlOptions'=>array('class'=>'value-wide value-update')))),
				'password'=>array('features'=>array(
					'2.password'=>array('htmlOptions'=>array('autocomplete'=>'off', 'style'=>'width: 405px', 'readonly'=>'readonly')),
					'2.widget'=>array('widgetName'=>'DifficultyMeter'),
					'2.error'
				)),
				'password2'=>array('features'=>array(
					'2.password'=>array('htmlOptions'=>array('autocomplete'=>'off', 'style'=>'width: 405px', 'readonly'=>'readonly')),
					'2.error'
				))
			)
	));
	
	$this->widget(Widget::alias('ButtonsTable'), array('form'=>$form, 'model'=>$model));
	
$this->endWidget();
