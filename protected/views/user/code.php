<?php
Yii::app()->asset->add();

$form = Yii::app()->controller->beginWidget('ActiveForm', array(
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'afterValidate'=>'js:function(form, data, hasError){
				console.log(hasError);
				console.log(data);
// 				if (!hasError)
// 					return false;
// 					return true;
			}'
	)
));
	
	Yii::app()->controller->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model,
		'attrException'=>array('captchacode'),
		'attributes'=>array(
			'emailcode, phonecode'=>array('attrHidden'=>!Arrays::pop($params, 'emailcode.visible'),
				'features'=>array('2.text'=>array('htmlOptions'=>array('autocomplete'=>'off')),
				'2.widget'=>array('widgetName'=>'Code'), '2.error')),
			'phonecode'=>array('attrHidden'=>!Arrays::pop($params, 'phonecode.visible'))
		)
	));
	
	Yii::app()->controller->widget(Widget::alias('ButtonsTable'), array('model'=>$model, 'params'=>array('ok', 'cancel')));
	
Yii::app()->controller->endWidget();
