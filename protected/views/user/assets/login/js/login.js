function loginBeforeSend(){
	var fp = $(this).closest('.flash-panel');
	
	fp.find('input').blur();
	fp.addClass('dsp-b').children('.curtain-loading').show();
}

function loginSuccess(data){
	if ('errors' in data){
		$(this).closest('.flash-panel').removeClass('dsp-b').children('.curtain-loading').hide();
		wActiveFormErrors($(this).closest('form'), data.errors);
	}else
		location = data.location;
}

function loginError(){
	$(this).closest('.flash-panel').removeClass('dsp-b').children('.curtain-loading').hide();
}

$(window).load(function(){
	$(document).on('click', 'a.button.esia', function(){
		$(this).closest('.flash-panel').children('.curtain-loading').show();
	});
});
