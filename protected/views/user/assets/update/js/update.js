$(window).load(function(){
	if (typeof wDropDownList != 'undefined'){
		var relations = {
				roles: {
					citizen: ['surname', 'name', 'patronymic'],
					organization: ['title'],
				},
				state: {
					locked: ['locked'],
					remote: ['delete']
				}
			},
			codes = {};
		
		for (id in wDropDownList){
			var pos = id.indexOf('_');
			
			if (pos > 0){
				var model = id.substring(0, pos + 1),
					attribute = id.substring(pos + 1),
					obj = $('div#' + id);
				
				for (relAttribute in relations)
					if (relAttribute == attribute){
						var codesAttribute = {},
							inputName = false;
						
						for (relName in relations[relAttribute]){
							var idInput = wDropDownList[id]['idItems'][relName],
								input = idInput ? $('input#' + idInput) : false;
							
							if (input && input.length){
								var trs = []
								
								relations[relAttribute][relName].forEach(function(relObj){
									relObj = $('#' + model + relObj);
									var html = relObj.html();
									
									if (relObj.length && (relObj.is('input') || (html && html.length > 1)))
										trs.push(relObj.closest('tr'));
								});
								
								if (trs.length){
									codesAttribute[input.val()] = trs;
									
									if (!inputName)
										inputName = input.attr('name');
								}
							}
						}
						
						if (inputName){
							codes[id] = codesAttribute;
							
							$('div.drop-down-list#' + id + ' input[name="' + inputName + '"]').on('change', function(){
								var value = $(this).val(),
									checked = $(this).attr('checked'),
									obj = $(this).closest('div.drop-down-list'),
									id = obj.attr('id'),
									multiple = obj.hasClass('multiple');
								
								if (id in codes)
									if (multiple){
										if (value in codes[id]){
											codes[id][value].forEach(function(tr){
												wDataTableAttrToggle(tr, checked);
											});
										}
									}else{
										for (code in codes[id]){
											codes[id][code].forEach(function(tr){
												wDataTableAttrToggle(tr, code == value);
											});
										}
									}
							});
						}
						
						break;
					}
			}
		}
	}
	
	if (typeof passwordParams != 'undefined'){
		var inputPassword = $('input#' + passwordParams.password),
			inputPasswordFloat = $('input#' + (passwordParams.password2 ? passwordParams.password2 : 'xxx') + 
				', input#' + (passwordParams.passwordcode ? passwordParams.passwordcode : 'xxx'));
		
		if (inputPassword.length && inputPasswordFloat.length){
			var tr = inputPasswordFloat.closest('tr');
			
			inputPassword.on('change', function(event, hype){
				if (hype == 2)
					return;
				
				wDataTableAttrToggle(tr, inputPassword.val().length);
			});
		}
	}
});
