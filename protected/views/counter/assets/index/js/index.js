function counterIndexRefresh(){
	var buttonOk = $('.ui-dialog .button.ok'),
		value = $(this).val();
	
	buttonOk.toggleClass('disabled', !value).attr('tabindex', -Number(!value));
}

$(window).load(function(){
	$('div.ui-dialog input[type=text]').on('change keyup', function(){
		var buttonOk = $(this).closest('.ui-dialog').find('.button.ok'),
			value = $(this).val();
		
		setTimeout(function(){
			buttonOk.toggleClass('disabled', !value).attr('tabindex', -Number(!value));
		}, 200);
	});
});
