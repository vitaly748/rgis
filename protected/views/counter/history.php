<?php
Yii::app()->asset->add(array('gridview'));

$this->widget(Widget::alias('Flashes'),
	array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

echo Html::openTag('div', array('class'=>'filter'));
	
	echo Html::openTag('div', array('class'=>'filter-item fi-not-clean'));
		echo Html::label('Счётчик', Widget::PREFIX_ID.'1', array('disabled'=>!$id_counters));
		echo Html::hiddenField('filter-history-counter', $id, array('class'=>'filter-field'));
		$this->widget(Widget::alias('DropDownList'), array(
			'data'=>$id_counters ? $id_counters : array('empty'=>array('access'=>'view')),
			'name'=>'filter-counter',
			'value'=>$id,
			'widthMin'=>190,
			'widthMax'=>190,
			'readonly'=>true
		));
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'filter-item'));
		echo Html::label('Начало периода', Widget::PREFIX_ID.'2');
		echo Html::hiddenField('filter-history-date-from', '', array('class'=>'filter-field'));
		$this->widget(Widget::alias('DateTimePicker'), array(
			'name'=>'filter-date-from',
			'mode'=>'date',
			'htmlOptions'=>array('disabled'=>!$id_counters)
		));
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'filter-item'));
		echo Html::label('Конец периода', Widget::PREFIX_ID.'4');
		echo Html::hiddenField('filter-history-date-to', '', array('class'=>'filter-field'));
		$this->widget(Widget::alias('DateTimePicker'), array(
			'name'=>'filter-date-to',
			'mode'=>'date',
			'htmlOptions'=>array('disabled'=>!$id_counters)
		));
	echo Html::closeTag('div');
	
	if ($id_counters){
		echo Html::openTag('div', array('class'=>'filter-buttons'));
			echo Html::link('', '#', array('class'=>'button search contur circle'));
			echo Html::link('', '#', array('class'=>'button clear contur circle'));
		echo Html::closeTag('div');
	}
	
echo Html::closeTag('div');

if ($id_counters){
	echo Html::openTag('div', array('class'=>'container-grid-view'));
		$this->renderPartial('historyGrid', array('model'=>$model));
		echo Html::tag('div', array('class'=>'curtain-loading'), '');
	echo Html::closeTag('div');
}elseif ($note = Yii::app()->label->get('note_counters_empty'))
	echo Html::tag('div', array('class'=>'note'), Html::tag('div', array(), $note));
