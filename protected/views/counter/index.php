<?php
Yii::app()->asset->add(array('gridview'));

if (!Yii::app()->user->getFlashes(false))
	$this->widget(Widget::alias('Flashes'),
		array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

echo Html::openTag('div', array('class'=>'filter'));
	
	echo Html::openTag('div', array('class'=>'filter-item fi-not-clean'));
		echo Html::label('Лицевой счёт', Widget::PREFIX_ID.'1', array('disabled'=>!$id_addresses));
		echo Html::hiddenField('filter-counter-address', $id, array('class'=>'filter-field'));
		$this->widget(Widget::alias('DropDownList'), array(
			'data'=>$id_addresses ? $id_addresses : array('empty'=>array('access'=>'view')),
			'name'=>'filter-address',
			'value'=>$id,
			'widthInitial'=>true,
			'readonly'=>true
		));
	echo Html::closeTag('div');
	
	if ($id_addresses){
		echo Html::openTag('div', array('class'=>'filter-buttons'));
			echo Html::link('', '#', array('class'=>'button search contur circle'));
		echo Html::closeTag('div');
	}
	
echo Html::closeTag('div');

if ($id_addresses){
	echo Html::openTag('div', array('class'=>'container-grid-view'));
		$this->renderPartial('indexGrid', array('model'=>$model));
		echo Html::tag('div', array('class'=>'curtain-loading'), '');
	echo Html::closeTag('div');
	
	$form = $this->beginWidget('ActiveForm', array('action'=>$this->createUrl('counter/reading')));
		
		echo Html::activeHiddenField($model, 'id', array('class'=>Html::CLASS_NEED_ID));
		echo Html::activeHiddenField($model, 'new');
		
		$this->widget(Widget::alias('ButtonsTable'), array('params'=>array(
			'reading'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled '.
				Html::PREFIX_BUTTON_STARTER_JD.'-juid-reading', 'tabindex'=>-1)),
			'history'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1))
		)));
		
	$this->endWidget();
	
	$this->widget(Widget::alias('JuiDialog'), array('action'=>'reading', 'reset'=>false,
		'htmlOptions'=>array('id'=>'juid-reading'), 'buttonOk'=>'ok', 'buttonCancel'=>'cancel', 'idFormSubmit'=>$this->getFormId(),
		'relations'=>array('input[type=text]'=>'input#'.Html::activeId($model, 'new'))));
}elseif ($note = Yii::app()->label->get('note_counters_empty'))
	echo Html::tag('div', array('class'=>'note'), Html::tag('div', array(), $note));
