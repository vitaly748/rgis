<?php
echo Html::openTag('div');
	echo Html::element(array('type'=>'text', 'model'=>$model, 'name'=>'new'));
echo Html::closeTag('div');

$this->widget(Widget::alias('ButtonsTable'), array('params'=>array(
	'ok'=>array('html_options'=>array('class'=>'disabled', 'tabindex'=>-1)), 'cancel')));
