<?php
$data = $model->search();
$items = Html::tag('div', array('class'=>'gv-table'), '{items}');
$sizer = $data->totalItemCount > 10 ? Html::tag('div', array('class'=>'sizer'), Html::tag('p', array(), 'Показывать по ').
	Html::radioButtonList('filter-size', $data->pagination->pageSize, $model::FILTER_SIZE_VALUES,
	array('class'=>'filter-field', 'container'=>'div', 'separator'=>false,
	'template'=>Html::tag('span', array(), '{input}{label}')))) : '';
$controls = Html::tag('table', array('class'=>'gv-controls'), Html::tag('tr', array(),
	Html::tag('td', array('class'=>'gv-summary'), '{summary}').
	Html::tag('td', array('class'=>'gv-sizer'), $sizer).
	Html::tag('td', array('class'=>'gv-pager'), '{pager}')
));

if ($rows = $data->getData()){
	$filter_id = array();
	$state_registered = Yii::app()->conformity->get('appeal_state', 'description', 'registered');
	$editor = Yii::app()->user->checkAccessSet('developer, administrator');
	
	foreach ($rows as $row){
		list($id, $state) = Arrays::pop($row, 'id, state');
		
		if ($state !== $state_registered || (!$editor && !$row['own']))
			$filter_id[] = $id;
	}
	
	$filter_id = json_encode(array('update'=>$filter_id, 'revoke'=>$filter_id));
}

foreach (array('id', 'theme', 'author', 'state', 'datecreated', 'dateaccepted', 'datecompleted', 'timeout') as $name)
	$columns[] = array(
		'header'=>$model->getAttributeLabel($name),
		'name'=>$name,
		'type'=>'raw',
		'headerHtmlOptions'=>array('class'=>'nowrap'),
		'htmlOptions'=>Html::forming(array('nowrap'=>!in_array($name, array('theme', 'author', 'state')),
			'ta-center'=>$name != 'theme', 'importance'=>$name == 'timeout'))
	);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'appeals-grid',
	'dataProvider'=>$data,
	'filterSelector'=>'input.filter-field',
	'ajaxType'=>'POST',
	'template'=>(Yii::app()->request->isAjaxRequest ? ($data->itemCount > 24 ? $controls : '').$items.$controls : $items).
		(!empty($filter_id) ? Html::tag('div', array('id'=>Html::ID_FILTER_ID), $filter_id) : ''),
	'cssFile'=>false,
	'rowCssClassExpression'=>Yii::app()->user->checkAccessSet('developer, administrator, clerk, specialist') ?
		'($data["addressee"] ? "red" : ($row % 2 ? "even" : "odd")).($data["importance"] > 0 ? " importance".$data["importance"] : '.
		'(in_array($data["state_code"], array('.implode(', ', Yii::app()->conformity->get('appeal_state', 'code', false, false,
		'done, denied')).')) ? " completed" : ""))' : null,
	'beforeAjaxUpdate'=>'gridviewBeforeAjaxUpdate',
	'afterAjaxUpdate'=>'gridviewAfterAjaxUpdate',
	'ajaxUpdateError'=>'gridviewErrorAjaxUpdate',
	'emptyText'=>Yii::app()->label->get(Yii::app()->request->isAjaxRequest ? 'wgv_empty' : 'wgv_loading'),
	'summaryText'=>'Найдено: <strong>{count}</strong>',
	'pager'=>array(
		'cssFile'=>false,
		'hiddenPageCssClass'=>'dsp-none',
		'firstPageLabel'=>'',
		'prevPageLabel'=>'',
		'nextPageLabel'=>'',
		'lastPageLabel'=>'',
		'maxButtonCount'=>7,
		'header'=>''
	),
	'columns'=>$columns
));
