appealUpdateResponderClientIdPrefix = 'client';
appealUpdateNote = null;
appealUpdateNoteText = null;
appealUpdateThemePlaceholder = null;
appealUpdateThemePlaceholderUser = 'Введите тему или выберите из списка';

function appealUpdateSetThemes(active){
	var inputResponder = $('form div.drop-down-list#' + appealParams.inputResponderId + ' input.ddl-input:checked'),
		responder = inputResponder.length ? inputResponder.val() : false,
		inputThemeId = $('form input#' + appealParams.inputThemeidId),
		themeId = inputThemeId.length ? inputThemeId.val() : false,
		themes = $('div.jui-dialog#juid-theme div.value-list a.vl-last'),
		resetTheme = false;
	
	if (themes.length){
		themes.removeClass('vl-disabled');
		
		if (responder && responder in appealParams.themeParams)
			themes.each(function(){
				var code = $(this).attr('href');
				
				if (code && !(code in appealParams.themeParams[responder])){
					$(this).addClass('vl-disabled');
					
					if (code == themeId){
						$(this).removeClass('vl-checking');
						resetTheme = true;
					}
				}
			});
	}
	
	if (resetTheme && inputThemeId.length){
		inputThemeId.val('');
		appealUpdateCheckTheme(active);
	}
}

function appealUpdateCheckTheme(active){
	var inputThemeId = $('form input#' + appealParams.inputThemeidId),
		themeId = inputThemeId.length ? inputThemeId.val() : false,
		userTheme = themeId == appealParams.codeUserTheme;
	
	if (inputThemeId.length){
		var inputTheme = inputThemeId.parent().children('input[type=text]');
		
		if (inputTheme.length){
			var theme = $('div.jui-dialog#juid-theme div.value-list a.vl-last[href="' + themeId + '"] p.vl-description');
			
			if (active){
				inputTheme.val(userTheme || !theme.length ? '' : theme.html());
				
				if (!userTheme)
					inputTheme.change();
				else
					inputTheme.focus();
			}
			
			inputTheme.attr('disabled', !userTheme);
			
			if (appealUpdateNote){
				var noteAppend = '';
				
				if (themeId){
					var inputResponder = $('form div.drop-down-list#' + appealParams.inputResponderId +
						' input.ddl-input:checked'),
						responder = inputResponder.length ? inputResponder.val() : false;
					
					if (responder && responder in appealParams.themeParams &&
						themeId in appealParams.themeParams[responder] && appealParams.themeParams[responder][themeId])
							noteAppend = appealParams.themeParams[responder][themeId];
				}
				
				appealUpdateNote.html(appealUpdateNoteText + ' ' + noteAppend);
			}
			
			if (appealUpdateThemePlaceholder)
				inputTheme.attr('placeholder', userTheme ? appealUpdateThemePlaceholderUser : appealUpdateThemePlaceholder);
		}
	}
}

$(window).load(function(){
	if (typeof appealParams !== 'undefined'){
		var inputThemeId = $('form input#' + appealParams.inputThemeidId),
			inputAuthorid = $('form input#' + appealParams.inputAuthoridId),
			inputResponder = $('form div.drop-down-list#' + appealParams.inputResponderId),
			inputResponderid = $('form input#' + appealParams.inputResponderidId);
		
		appealUpdateNote = $('div.note > div');
		
		if (appealUpdateNote.length)
			appealUpdateNoteText = appealUpdateNote.html();
		
		if (inputAuthorid.length){
			var divAuthoridText = inputAuthorid.parent().children('div.value'),
				inputJDTitle = $('div#juid-author input#user');
			
			if (divAuthoridText.length && inputJDTitle.length){
				inputAuthorid.on('change', function(){
					var title = inputJDTitle.val();
					
					divAuthoridText.html(!title ? appealParams.insertEmpty :
						title + (inputAuthorid.val() == appealParams.userId ? appealParams.insertI : ''));
				});
			}
		}
		
		if (inputResponder.length && inputResponderid.length)
			setTimeout(function(){
				var responderUl = inputResponder.children('ul'),
					needBorder = responderUl.children('li.ddl-item').length > 0,
					inputJDTitle = $('div#juid-responder input#user');
				
				appealUpdateSetThemes();
				
				if (needBorder){
					var responderLiHtml = outer(responderUl.children('li.ddl-item:last-child')[0]);
					
					inputResponderid.on('change', function(){
						var value = -parseInt($(this).val()),
							title = inputJDTitle.val();
						
						if (value){
							var input = responderUl.find('li.ddl-item input[value=' + value + ']');
							
							if (!input.length){
								var liNew = $(responderLiHtml);
								
								if (needBorder){
									responderUl.append($('<li class="ddl-border"></li>'));
									needBorder = false;
								}
								
								responderUl.append(liNew);
								input = liNew.children('input');
								input.val(value).attr('id', appealUpdateResponderClientIdPrefix + value);
								liNew.children('label').attr('for', appealUpdateResponderClientIdPrefix + value).
									html(inputJDTitle.val());
							}
							
							responderUl.children('li.ddl-item').removeClass('active').
								children('input').attr('checked', false);
							input.attr('checked', true).parent().addClass('active');
							wDropDownListTitleRepaint(inputResponder);
							
							if (inputResponder.hasClass('disabled'))
								inputResponder.removeClass('disabled').removeClass('ddl-locked').children('div.ddl-header').
									children('input').attr('disabled', false);
							
							appealUpdateSetThemes(true);
						}
					});
				}
				
				inputResponder.on('change', 'input[name=' + shielding(appealParams.inputResponderName) + ']', function(){
					appealUpdateSetThemes(true);
				});
			}, 100);
		
		if (inputThemeId.length){
			var inputTheme = inputThemeId.parent().children('input[type=text]');
			
			if (inputTheme.length)
				appealUpdateThemePlaceholder = inputTheme.attr('placeholder');
			
			setTimeout(function(){
				appealUpdateCheckTheme();
			}, 100);
			
			inputThemeId.on('change', function(){
				appealUpdateCheckTheme(true);
			});
		}
	}
});
