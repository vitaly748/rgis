<?php
Yii::app()->asset->add(array('button'));

$access_attributes = $model->accessAttributes;
$category_update = Arrays::pop($access_attributes, 'category') == 'update';
$attribute_html_options = Html::forming(array('value-big', 'value-wide', 'value-update'=>$category_update));
$stages = $model->stage;
$state_desc = Yii::app()->conformity->get('appeal_stage_state', 'description', 'code');
$state_name = Yii::app()->conformity->get('appeal_stage_state', 'name', 'code');
$history = $model->checkAccess('history', false);
$comment = Arrays::pop($access_attributes, 'comment');

if ($importance && $operator)
	foreach (Appeal::TIME_BORDER as $key=>$border)
		if ($importance == $key + 1){
			$model->timeout .= $importance == count(Appeal::TIME_BORDER) ? ' (просрочено)' :
				' (менее '.ceil($border / (24 * 60 * 60)).' дней)';
			
			break;
		}

$form = $this->beginWidget('ActiveForm', array('command'=>true, 'htmlOptions'=>array('class'=>'w100')));
	
	echo Html::openTag('div', array('class'=>'note'));
		echo Html::openTag('div');
			
			echo Html::openTag('div', array('class'=>'appeal-title'));
				echo Html::tag('div', array('class'=>'appeal-id'), '№ '.$model->id);
				echo Html::tag('div', array('class'=>'appeal-state '.Yii::app()->conformity->get('appeal_state', 'name', $model->state)),
					Yii::app()->conformity->get('appeal_state', 'description', $model->state));
			echo Html::closeTag('div');
			
			$this->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model,
				'htmlOptions'=>array('class'=>'unrequired'), TreeWidget::PARAM_ATTRIBUTES_FULL=>false,
				'attributes'=>array(
					'theme'=>array('features'=>array(
						'1.label'=>array('htmlOptions'=>array('disabled'=>false)),
						'2.value'=>array('featureModel'=>false, 'featureValue'=>$model->themeid.'. '.$model->theme,
							'htmlOptions'=>$attribute_html_options)
					)),
					'authorid'=>array('features'=>array(
						'1.label'=>array('htmlOptions'=>array('disabled'=>false)),
						'2.value'=>array('featureModel'=>false, 'featureValue'=>Html::link(Strings::userName($model->author,
							Strings::FORMAT_USER_NAME_FULL).($model->authorid == Yii::app()->user->id ?
							Yii::app()->label->get('insert_i') : ''), $this->createUrl('user/view', array('id'=>$model->authorid)),
							array('target'=>'_blank')), 'htmlOptions'=>$attribute_html_options)
					)),
					'address'=>array('features'=>array(
						'1.label'=>array('htmlOptions'=>array('disabled'=>false)),
						'2.value'=>array('htmlOptions'=>$attribute_html_options)
					)),
					'responder'=>array('features'=>array(
						'1.label'=>array('htmlOptions'=>array('disabled'=>false)),
						'2.value'=>$model->responder < 0 && $user_responder ? array('featureModel'=>false,
							'featureValue'=>$operator ?
							Html::link(Strings::userName($user_responder, Strings::FORMAT_USER_NAME_FULL), $this->createUrl('user/view',
							array('id'=>$user_responder->id)), array('target'=>'_blank')) :
							Strings::userName($user_responder, Strings::FORMAT_USER_NAME_FULL),
							'htmlOptions'=>$attribute_html_options) : array('htmlOptions'=>$attribute_html_options)
					)),
					'category, type'=>array('features'=>$category_update ?
						array(
							'2.widget'=>array('widgetName'=>'DropDownList', 'widgetParams'=>array('widthMin'=>300, 'widthMax'=>300,
								'readonly'=>true)),
							'2.error_static'
						) : 
						array(
							'1.label'=>array('htmlOptions'=>array('disabled'=>false)),
							'2.value'=>array('htmlOptions'=>$attribute_html_options)
						)
					),
					'timeout'=>array('features'=>array(
						'1.label'=>array('htmlOptions'=>array('disabled'=>false)),
						'2.value'=>array('htmlOptions'=>Html::forming(array('value-big', 'value-wide',
							'value-update'=>$category_update, 'importance'.$importance=>$importance && $operator)))
					))
				)
			));
			
		echo Html::closeTag('div');
	echo Html::closeTag('div');
	
	echo Html::openTag('div', array('class'=>'appeal-dialog'));
		
		foreach ($stages as $stage){
			$name = $state_name[$stage->state];
			
			if (($left = ($name == 'created' && $model->responder > 0) || $name == 'answered') ||
				in_array($name, array('requested', 'completed', 'denied'))){
					echo Html::openTag('div', array('class'=>$left ? 'appeal-text-container' : 'appeal-answer-container'));
						if ($left)
							echo Html::tag('div', array('class'=>'icon'), '');
						
						echo Html::openTag('div', array('class'=>'content round3'));
							
							echo Html::tag('div', array('class'=>'appeal-stage-date'), Html::date($stage->date));
							
							echo Html::tag('div', array('class'=>'appeal-text'), Strings::multilineText($stage->text));
							
							if ($files = $stage->file){
								$file_data = array();
								
								foreach ($files as $file)
									$file_data[$file->id] = $file->name;
								
								$this->widget(Widget::alias('FileList'), array('value'=>$file_data));
							}
							
						echo Html::closeTag('div');
						
						if (!$left)
							echo Html::tag('div', array('class'=>'icon'), '');
					echo Html::closeTag('div');
				}
		}
		
	echo Html::closeTag('div');
	
	if ($history || $comment){
		echo Html::openTag('div', array('class'=>'appeal-manage'));
			
			if ($history){
				if (count($stages) > 1){
					$roles = Yii::app()->user->getRoles(0, 'id', 'name');
					
					echo Html::openTag('div', array('class'=>'appeal-history'));
						
						foreach ($stages as $key=>$stage)
							if (($model->responder > 0 && $key) || ($model->responder < 0 && $key > 1)){
								echo Html::openTag('div', array('class'=>'appeal-history-item'));
									echo Html::openTag('div', array('class'=>'appeal-history-title'));
										echo Html::tag('div', array('class'=>'appeal-history-date'), Html::date($stage->date));
										echo Html::tag('div', array('class'=>'appeal-history-state '.
											$state_name[$stage->state]),
											$state_desc[$stage->state]);
									echo Html::closeTag('div');
									
									if ($stage->id_owner == Yii::app()->user->id)
										$owner = Yii::app()->user->model;
									elseif ($user_responder && $stage->id_owner == $user_responder->id)
										$owner = $user_responder;
									else
										$owner = $stage->owner;
									
									if ($owner){
										$owner->initRoles();
										
										echo Html::openTag('div', array('class'=>'appeal-history-initiator'));
											echo Html::openTag('table');
												echo Html::openTag('tbody');
													echo Html::openTag('tr');
														echo Html::openTag('td', array('class'=>'appeal-history-user'));
															echo Strings::userName($owner);
														echo Html::closeTag('td');
														echo Html::openTag('td', array('class'=>'appeal-history-role'));
															echo '('.Yii::app()->user->getRoles(0, 'description', 
																in_array($roles['clerk'], $owner->roles) ? 'clerk' : 
																(in_array($roles['specialist'], $owner->roles) ? 'specialist' :
																(in_array($roles['citizen'], $owner->roles) ? 'citizen' : 'organization'))).')';
														echo Html::closeTag('td');
													echo Html::closeTag('tr');
												echo Html::closeTag('tbody');
											echo Html::closeTag('table');
										echo Html::closeTag('div');
									}
									
									if ($stage->text)
										echo Html::tag('div', array('class'=>'appeal-history-text'), Strings::multilineText($stage->text));
									
									if ($files = $stage->file){
										$file_data = array();
										
										foreach ($files as $file)
											$file_data[$file->id] = $file->name;
										
										$this->widget(Widget::alias('FileList'), array('value'=>$file_data));
									}
									
								echo Html::closeTag('div');
							}
						
					echo Html::closeTag('div');
				}
				
				echo Html::openTag('div', array('class'=>'appeal-link'));
					echo Html::openTag('div');
						echo Html::tag('a', array('class'=>'appeal-link-mkd', 'href'=>$link = 'http://'.Yii::app()->portal->get('link').
							(!empty($house['id_bars']) ? '/gkh/house?id='.$house['id_bars'] : '/gkh/home'),
							'target'=>'_blank'), Html::tag('p', array(), Yii::app()->label->get('insert_link_mkd')));
						echo Html::tag('a', array('class'=>'appeal-link-repair', 'href'=>$link,
							'target'=>'_blank'), Html::tag('p', array(), Yii::app()->label->get('insert_link_repair')));
					echo Html::closeTag('div');
					echo Html::openTag('div');
						echo Html::tag('a', array('class'=>'appeal-link-money', 'href'=>$this->createUrl('house/general',
							!empty($house['id_bss']) ? array('id'=>$house['id_bss']) : array()),
							'target'=>'_blank'), Html::tag('p', array(), Yii::app()->label->get('insert_link_money')));
						echo Html::tag('a', array('class'=>'appeal-link-activity', 'href'=>$link,
							'target'=>'_blank'), Html::tag('p', array(), Yii::app()->label->get('insert_link_activity')));
					echo Html::closeTag('div');
				echo Html::closeTag('div');
			}
			
			if ($comment)
				$this->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model,
					'htmlOptions'=>array('class'=>'unrequired'), TreeWidget::PARAM_ATTRIBUTES_FULL=>false,
					'attributes'=>array(
						'comment'=>array('cells'=>array(TreeWidget::PARAM_ITEMS_INHERIT_DATA=>0,
							1=>array('htmlOptions'=>array('class'=>'cell-name dt-cell-top')),
							2=>array('htmlOptions'=>array('class'=>'cell-value'), 'features'=>array('2.textarea', '2.error'))
						)),
						'commentfiles'=>array('cells'=>array(TreeWidget::PARAM_ITEMS_INHERIT_DATA=>0,
							1=>array('htmlOptions'=>array('class'=>'cell-name dt-cell-top')),
							2=>array('htmlOptions'=>array('class'=>'cell-value'), 'features'=>array('2.widget'=>array('widgetName'=>'FileLoader')))
						)),
					)
				));
			
			if ($model->checkAccess('redirect', false))
				echo Html::hiddenField('partnerid');
			
		echo Html::closeTag('div');
	}
	
	$params_bt = array(
		'update',
		'revoke'=>array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm1')),
		'accept'
	);
	
	if ($importance < 3){
		if ($model->state !== Yii::app()->conformity->get('appeal_state', 'code', 'extended')){
			$params_bt['redirect'] = array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-operator'));
			$params_bt[] = 'return';
		}
		
		$params_bt['request'] = array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm2'));
		$params_bt['complete'] = array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm3'));
		$params_bt['deny'] = array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm3'));
	}
	
	$params_bt[] = 'answer';
	
	if ($rollback = ($model->responder > 0 && count($stages) > 1) || ($model->responder < 0 && count($stages) > 3))
		$params_bt['rollback'] = array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm4'));
	
	if ($extend = $model->state === Yii::app()->conformity->get('appeal_state', 'code', 'confirmed'))
		$params_bt['extend'] = array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm5'));
	
	$this->widget(Widget::alias('ButtonsTable'), array('model'=>$model, 'params'=>$params_bt));
	
$this->endWidget();

$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm', 'htmlOptions'=>array('id'=>'juid-confirm1'),
	'buttonOk'=>'yes', 'buttonCancel'=>'no'));

$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm', 'htmlOptions'=>array('id'=>'juid-confirm2'),
	'buttonOk'=>'yes', 'buttonCancel'=>'no', 'params'=>array('label'=>'appeal_request')));

$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm', 'htmlOptions'=>array('id'=>'juid-confirm3'),
	'buttonOk'=>'yes', 'buttonCancel'=>'no', 'params'=>array('label'=>'appeal_answer')));

if ($rollback)
	$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm', 'htmlOptions'=>array('id'=>'juid-confirm4'),
		'buttonOk'=>'yes', 'buttonCancel'=>'no', 'params'=>array('label'=>'appeal_rollback')));

if ($extend)
	$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm', 'htmlOptions'=>array('id'=>'juid-confirm5'),
		'buttonOk'=>'yes', 'buttonCancel'=>'no', 'params'=>array('label'=>'appeal_extend')));

if ($model->checkAccess('redirect', false))
	$this->widget(Widget::alias('JuiDialog'), array('view'=>'user', 'title'=>Yii::app()->label->get('title_jd_partner'),
		'params'=>array('aim'=>'operator'), 'reset'=>false, 'htmlOptions'=>array('id'=>'juid-operator'),
		'buttonOk'=>'select', 'buttonCancel'=>'cancel',
		'relations'=>array('input#userid'=>'form input[name=partnerid]')
	));
