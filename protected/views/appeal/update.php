<?php
Yii::app()->asset->add(Arrays::forming(array('button', 'input',
	':fias'=>array(Arrays::FORMING_PARAMS_ENABLED=>Yii::app()->fias->activate()))));

$this->widget(Widget::alias('Flashes'),
	array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

$id_input_themeid = Html::activeId($model, 'themeid');
$insert_i = Yii::app()->label->get('insert_i');
$insert_empty = Yii::app()->label->get('insert_empty');
$user_id = Yii::app()->user->id;

$responder_features = array('2.hidden'=>array('featureModel'=>false, 'featureName'=>'responderid'),
	'2.widget'=>array('widgetName'=>'DropDownList', 'widgetParams'=>array('widthMin'=>
	($responder_ext = Yii::app()->user->checkAccessSet('developer, administrator, clerk')) ? 366 : 475,
	'widthMax'=>$responder_ext ? 366 : 475, 'readonly'=>true, 'data'=>$model->responder < 0 &&
	($user = UserModel::model()->findByPk(-$model->responder, '', array(), true, false, false)) ?
	array_merge($model->accessValues['responder'], array(false, array('code'=>$model->responder,
	'description'=>Strings::userName($user, Strings::FORMAT_USER_NAME_FULL)))) : null
)));

if ($responder_ext)
	$responder_features = array_merge(
// 		array('2.hidden'=>array('featureModel'=>false, 'featureName'=>'responderid')),
		$responder_features,
		array('2.link'=>array('featureModel'=>false, 'featureName'=>Yii::app()->label->get('button_choice'),
			'htmlOptions'=>array('class'=>'button choice va-middle '.Html::PREFIX_BUTTON_STARTER_JD.'-juid-responder',
			'style'=>'margin-left: 8px')))
	);

if ($note = Yii::app()->label->get('note_appeal'))
	echo Html::tag('div', array('class'=>'note'), Html::tag('div', array(), $note));

$form = $this->beginWidget('ActiveForm');
	
	$this->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model,
		TreeWidget::PARAM_ATTRIBUTES_FULL=>false,
		'attributes'=>array(
			'authorid'=>array('features'=>array(
				'1.label'=>array('htmlOptions'=>array('disabled'=>false)),
				'2.hidden',
				'2.value'=>array('featureModel'=>false, 'featureValue'=>$model->authorid ?
					Strings::userName($model->author, Strings::FORMAT_USER_NAME_FULL). 
					($model->authorid == $user_id ? $insert_i : '') : $insert_empty,
					'htmlOptions'=>array('class'=>'value-wide value-update dsp-ib')),
				'2.link'=>array('featureModel'=>false, 'featureName'=>Yii::app()->label->get('button_choice'),
					'htmlOptions'=>array('class'=>'button choice va-middle '.Html::PREFIX_BUTTON_STARTER_JD.'-juid-author',
						'style'=>'margin-left: 8px')),
				'2.error'
			)),
			'theme'=>array('features'=>array(
				'1.label'=>array('htmlOptions'=>array('for'=>false)),
				'2.hidden'=>array('featureName'=>'themeid'),
				'2.text'=>array('htmlOptions'=>array('class'=>'va-middle', 'style'=>'width: 366px', 'disabled'=>'disabled',
					'autocomplete'=>'off')),
				'2.link'=>array('featureModel'=>false, 'featureName'=>Yii::app()->label->get('button_choice'),
					'htmlOptions'=>array('class'=>'button choice va-middle '.Html::PREFIX_BUTTON_STARTER_JD.'-juid-theme',
					'style'=>'margin-left: 8px')),
				'2.error'
			)),
			'address'=>array('features'=>array('2.hidden'=>array('featureName'=>'addressid'), '2.text', '2.error')),
			'responder'=>array('features'=>array_merge($responder_features, array('2.error_static'))),
			'text'=>array('cells'=>array(TreeWidget::PARAM_ITEMS_INHERIT_DATA=>0,
				1=>array('htmlOptions'=>array('class'=>'cell-name dt-cell-top')),
				2=>array('htmlOptions'=>array('class'=>'cell-value'), 'features'=>array('2.textarea', '2.error'))
			)),
			'files'=>array('cells'=>array(TreeWidget::PARAM_ITEMS_INHERIT_DATA=>0,
				1=>array('htmlOptions'=>array('class'=>'cell-name dt-cell-top')),
				2=>array('htmlOptions'=>array('class'=>'cell-value'),
					'features'=>array('2.widget'=>array('widgetName'=>'FileLoader')))
			)),
			'captchacode'=>array('features'=>array(
				'2.text'=>array('htmlOptions'=>array('autocomplete'=>'off', 'style'=>'width: 306px')),
				'2.widget'=>array('widgetName'=>'Captcha'),
				'2.error'
			))
		)
	));
	
	if ($model->action == 'update')
		$params_bt['save'] = 'submit';
	else
		$params_bt[] = 'ok';
	
	$params_bt['revoke'] = array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm'));
	
	$this->widget(Widget::alias('ButtonsTable'), array('form'=>$form, 'model'=>$model, 'mergeParams'=>false,
		'params'=>$params_bt));
	
$this->endWidget();

if (in_array('authorid', $model->allowedAttributeNames))
	$this->widget(Widget::alias('JuiDialog'), array('view'=>'user', 'title'=>Yii::app()->label->get('title_jd_author'),
		'params'=>array('aim'=>'client', 'author_enabled'=>$author_enabled, 'type'=>'author'), 'reset'=>false,
		'htmlOptions'=>array('id'=>'juid-author'), 'buttonOk'=>'select', 'buttonCancel'=>'cancel',
		'relations'=>array('input#userid'=>'form input#'.Html::activeId($model, 'authorid'))
	));

if ($responder_ext)
	$this->widget(Widget::alias('JuiDialog'), array('view'=>'user', 'title'=>Yii::app()->label->get('title_jd_responder'),
		'params'=>array('aim'=>'client', 'type'=>'responder'), 'reset'=>false,
		'htmlOptions'=>array('id'=>'juid-responder'), 'buttonOk'=>'select', 'buttonCancel'=>'cancel',
		'relations'=>array('input#userid'=>'form input#responderid')
	));

$this->widget(Widget::alias('JuiDialog'), array('view'=>'choice', 'title'=>Yii::app()->label->get('title_jd_theme'),
	'params'=>array('model'=>$model, 'name'=>'themeid'), 'htmlOptions'=>array('id'=>'juid-theme'),
	'buttonOk'=>'select', 'buttonCancel'=>'cancel',
	'relations'=>array('input#'.$id_input_themeid=>'form input#'.$id_input_themeid)
));

if ($model->id)
	$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm',
		'htmlOptions'=>array('id'=>'juid-confirm'), 'buttonOk'=>'yes', 'buttonCancel'=>'no'));

Html::registerScriptParams('appealParams', false, false, array(
	'inputThemeidId'=>$id_input_themeid,
	'inputAuthoridId'=>Html::activeId($model, 'authorid'),
	'inputResponderId'=>Html::activeId($model, 'responder'),
	'inputResponderName'=>Html::activeName($model, 'responder'),
	'inputResponderidId'=>'responderid',
	'userId'=>$user_id,
	'userTitle'=>Strings::userName(Yii::app()->user->model, Strings::FORMAT_USER_NAME_FULL),
	'codeUserTheme'=>Appeal::CODE_USER_THEME,
	'insertI'=>$insert_i,
	'insertEmpty'=>$insert_empty,
	'themeParams'=>$model->getThemeParams()
));
