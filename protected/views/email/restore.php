<?php
$link_site = Yii::app()->getBaseUrl(true);
$name_site = Yii::app()->setting->get('name');
?>
Здравствуйте.
<br>
Был получен запрос на изменения пароля для Вашей учётной записи на сайте 
<a href="<?=$link_site?>"><?=$link_site?></a> (<?=$name_site?>).
<br><br>
Для продолжения пройдите, пожалуйста, по ссылке
<br>
<a href="<?=$link?>"><?=$link?></a>
<br><br>
Если это были не Вы, просто проигнорируйте это письмо.
