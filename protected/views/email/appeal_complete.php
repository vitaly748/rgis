<?php
$link_site = Yii::app()->getBaseUrl(true);
$app_name = Yii::app()->setting->get('general', 'name');
?>
Здравствуйте, <?=$title?>.
<br>
На сайте <a href="<?=$link_site?>"><?=$app_name?></a> по обращению «<?=$theme?>» был подготовлен окончательный ответ.
<br>
Вы можете ознакомиться с ним, пройдя по ссылке <a href="<?=$link?>"><?=$link?></a>.
