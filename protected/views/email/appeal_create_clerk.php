<?php
$link_site = Yii::app()->getBaseUrl(true);
$app_name = Yii::app()->setting->get('general', 'name');
?>
Здравствуйте, <?=$title?>.
<br>
На сайте <a href="<?=$link_site?>"><?=$app_name?></a> было сформировано обращение «<?=$theme?>», адресованное вам.
<br>
Перейдите, пожалуйста, по ссылке <a href="<?=$link?>"><?=$link?></a> и дайте ответ.
