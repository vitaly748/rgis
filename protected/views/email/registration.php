<?php
$link_site = Yii::app()->getBaseUrl(true);
$app_name = Yii::app()->setting->get('general', 'name');
?>
Уважаемый(ая) <?=$fio?>, спасибо Вам за регистрацию на сайте <a href="<?=$link_site?>"><?=$app_name?></a>.
<br><br>
Ваш логин: <?=$login?>.
<br>
Храните свой пароль в безопасном месте. В случае его утери, Вы можете восстановить его, перейдя по ссылке
<a href="<?=$link?>"><?=$link?></a>.
