<?php
$link_site = Yii::app()->getBaseUrl(true);
$app_name = Yii::app()->setting->get('general', 'name');
?>
Здравствуйте, <?=$title?>.
<br>
Вами было создано обращение <a href="<?=$link?>">«<?=$theme?>»</a> на сайте <a href="<?=$link_site?>"><?=$app_name?></a>.
<br>
В ближайшее время оно будет рассмотрено нашими специалистами. По итогам рассмотрения вы будете уведомлены по электронной почте.
