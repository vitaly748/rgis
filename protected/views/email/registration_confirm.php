<?php
$link_site = Yii::app()->getBaseUrl(true);
$name_site = Yii::app()->setting->get('name');
?>
Здравствуйте.
<br>
Ваш e-mail был указан при регистрации на сайте <a href="<?=$link_site?>"><?=$link_site?></a> (<?=$name_site?>).
<br><br>
Для завершения процедуры регистрации пройдите, пожалуйста, по ссылке
<br>
<a href="<?=$link?>"><?=$link?></a>
<br><br>
Если Вы не проходили регистрацию на данном сайте, просто проигнорируйте это письмо.
