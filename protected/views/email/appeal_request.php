<?php
$link_site = Yii::app()->getBaseUrl(true);
$app_name = Yii::app()->setting->get('general', 'name');
?>
Здравствуйте, <?=$title?>.
<br>
На сайте <a href="<?=$link_site?>"><?=$app_name?></a> по обращению «<?=$theme?>» требуется дополнительная информация.
<br>
Просим вас пройти по ссылке <a href="<?=$link?>"><?=$link?></a> и ввести недостающую информацию.
<br>
Ход обработки данного обращения приостановлен до момента получения вашего ответа.
