<?php
Yii::app()->asset->add();

$items = array(
	'instruction_secure.pdf'=>'Организация защиты передаваемой в ГИС ЖКХ информации',
	'instruction_cryptopro.pdf'=>'Инструкция по подключению через КроптоПро',
	'instruction_sputnik.pdf'=>'Инструкция по подключению через Спутник',
	'user_agreement.pdf'=>'Пользовательское соглашение'
);

echo Html::openTag('ul', array('class'=>'link-docs'));
	
	foreach ($items as $name=>$description){
		echo Html::openTag('li');
			echo Html::link($description, Yii::app()->baseUrl.'/assets/'.$name, array('target'=>'_blank'));
		echo Html::closeTag('li');
	}
	
echo Html::closeTag('ul');
