function manageRoleReloadName(){
	var name = $('input[name=filter-name]').val(),
		input = $('ul.roles-pages input[type=checkbox]');
	
	if (input.length){
		input.attr('checked', false);
		
		if (name in rolesParams.rolesPages)
			rolesParams.rolesPages[name].forEach(function(val){
				input.filter('[value="' + val + '"]').attr('checked', true);
			});
	}
}

function manageRoleReloadGroup(){
	var group = $('input[name=filter-group]').val(),
		li = $('ul.roles-pages li');
	
	if (li.length){
		li.addClass('dsp-none');
		
		if (group)
			group.split(',').forEach(function(val){
				li.filter('.group-' + val).removeClass('dsp-none');
			});
	}
}

$(window).load(function(){
	if (typeof rolesParams !== 'undifined')
		setTimeout(function(){
			$('input[name=filter-name]').on('change', function(){
				manageRoleReloadName();
			});
			
			$('input[name=filter-group]').on('change', function(){
				manageRoleReloadGroup();
			});
		}, 200);
});
