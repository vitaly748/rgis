<?php
Yii::app()->asset->add(array('gridview'));

echo Html::openTag('div', array('class'=>'container-grid-view'));
	
	$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'table-state-grid',
		'dataProvider'=>new CArrayDataProvider($data),
		'cssFile'=>false,
		'rowCssClassExpression'=>'($row % 2 ? "even " : "odd ").($data["state"] ? "completed" : "importance2")',
		'template'=>Html::tag('div', array('class'=>'gv-table'), '{items}'),
		'selectableRows'=>0,
		'columns'=>array(
			array(
				'header'=>'Табличная группа',
				'headerHtmlOptions'=>array('class'=>'nowrap'),
				'htmlOptions'=>array('class'=>'nowrap ta-center'),
				'name'=>'name',
			),
			array(
				'header'=>'Статус',
				'headerHtmlOptions'=>array('class'=>'nowrap'),
				'htmlOptions'=>array('class'=>'nowrap ta-center importance'),
				'name'=>'state_description',
			)
		)
	));
	
echo Html::closeTag('div');
