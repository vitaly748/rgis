<?php
Yii::app()->asset->add(array('gridview'));

$form = $this->beginWidget('ActiveForm');
	
	echo Html::openTag('div', array('class'=>'filter'));
		
		echo Html::openTag('div', array('class'=>'filter-item fi-not-clean'));
			echo Html::label($model->getAttributeLabel('name'), Html::activeId($model, 'name'));
			echo Html::hiddenField('filter-name', $model->name);
			$this->widget(Widget::alias('DropDownList'), array(
				'form'=>$form,
				'model'=>$model,
				'name'=>'name',
				'data'=>$roles,
				'widthInitial'=>true,
				'readonly'=>true
			));
		echo Html::closeTag('div');
		
		echo Html::openTag('div', array('class'=>'filter-item fi-not-clean'));
			echo Html::label($model->getAttributeLabel('group'), Html::activeId($model, 'group'));
			echo Html::hiddenField('filter-group', implode(',', $model->group));
			$this->widget(Widget::alias('DropDownList'), array(
				'form'=>$form,
				'model'=>$model,
				'name'=>'group',
				'multiple'=>true,
				'itemAllEnabled'=>true,
				'widthInitial'=>true,
				'readonly'=>true
			));
		echo Html::closeTag('div');
		
	echo Html::closeTag('div');
	
	echo Html::tag('h5', array(), $model->getAttributeLabel('pages'));
	
	echo Html::openTag('ul', array('class'=>'roles-pages'));
		
		$count = 1;
		
		foreach ($pages as $name=>$description)
			echo Html::tag('li', array('class'=>'group-'.$groups[mb_strpos($name, 'appeal') === 0 ? 'appeal' : 'lk']),
				Html::element(array('type'=>'checkbox', 'model'=>$model, 'name'=>'pages', 'value'=>$name,
				'htmlOptions'=>array('id'=>Html::activeId($model, 'pages').'-'.$count++, 'value'=>$name), 'label'=>$description)));
		
	echo Html::closeTag('ul');
	
	echo Html::closeTag('br');
	
	$this->widget(Widget::alias('ButtonsTable'), array('params'=>array('save')));

$this->endWidget();

Html::registerScriptParams('rolesParams', false, false, array(
	'rolesPages'=>$roles_pages
));
