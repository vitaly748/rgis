<?php
Yii::app()->asset->add(array('gridview'));

$items = Html::tag('div', array('class'=>'gv-table'), '{items}');
$id = 1;

foreach (static::ORGANIZATION as $code=>$power)
	$organizations[] = array(
		'id'=>$id++,
		'name'=>$power['name'],
		'powers'=>$power['power'],
		'relevance'=>Html::checkBox('powers[]', in_array($code, $powers), array('value'=>$code))
	);

$form = $this->beginWidget('ActiveForm');
	
	echo Html::openTag('div', array('class'=>'container-grid-view'));
		
		$this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'powers-grid',
			'dataProvider'=>new CArrayDataProvider($organizations),
			'cssFile'=>false,
			'template'=>$items,
			'selectableRows'=>0,
			'columns'=>array(
				array(
					'header'=>'Организация',
					'headerHtmlOptions'=>array('class'=>'nowrap'),
					'name'=>'name',
				),
				array(
					'header'=>'Краткое наименование полномочия',
					'headerHtmlOptions'=>array('class'=>'nowrap'),
					'name'=>'powers',
				),
				array(
					'header'=>'Актуальность',
					'headerHtmlOptions'=>array('class'=>'nowrap'),
					'type'=>'raw',
					'name'=>'relevance',
					'htmlOptions'=>array('class'=>'ta-center')
				)
			)
		));
		
	echo Html::closeTag('div');
	
	echo Html::closeTag('br');
	
	$this->widget(Widget::alias('ButtonsTable'), array('params'=>array('save')));
	
$this->endWidget();
