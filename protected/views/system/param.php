<?php
echo "<?php
/**
 * Параметры приложения. Все параметры обязательны
 * Описание структуры возвращаемого массива:
 * 	array(
 * 		'db'=>array(// Параметры подключения к БД
 *			'host'=>'localhost',// HTTP-адрес сервера БД
 * 			'port'=>'5432',// Порт подключения
 * 			'name'=>'lkz',// Название БД
 * 			'login'=>'sa',// Логин
 * 			'password'=>'00000000'// Пароль
 *		),
 *		'developer'=>'127.0.0.1'// IP-адрес подключения разработчика
 *		'cache'=>array(// Параметры кэша
 *			'cache_type'=>'file',// Тип кэширования
 *			'duration1'=>60 * 60 * 24 * 30,// Время хранения данных в кэше по схеме \"Долго\"
 *			'duration2'=>60 * 60 * 24,// Время хранения данных в кэше по схеме \"Нормально\"
 *			'duration3'=>60 * 30,// Время хранения данных в кэше по схеме \"Быстро\"
 *			'duration4'=>60 * 30// Время хранения данных bss-запросов в кэше в секундах
 *		)
 * 	)
 */
return array(
	'db'=>array(
		'host'=>'".$db['host']."',
		'port'=>'".$db['port']."',
		'name'=>'".$db['name']."',
		'login'=>'".$db['login']."',
		'password'=>'".$db['password']."'
	),
	'developer'=>'$developer',
	'cache'=>array(
		'cache_type'=>'".$cache['cache_type']."',
		'duration1'=>{$cache['duration1']},
		'duration2'=>{$cache['duration2']},
		'duration3'=>{$cache['duration3']},
		'duration4'=>{$cache['duration4']}
	)
);";

