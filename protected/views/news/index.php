<?php
Yii::app()->asset->add(array('gridview'));

$this->widget(Widget::alias('Flashes'),
	array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

if (!Yii::app()->user->isGuest){
	echo Html::openTag('div', array('class'=>'filter'));
		
		echo Html::openTag('div', array('class'=>'filter-item'));
			echo Html::label('Тема', 'filter-news-title');
			echo Html::textField('filter-news-title', '', array('class'=>'filter-field'));
		echo Html::closeTag('div');
		
		echo Html::openTag('div', array('class'=>'filter-item'));
			echo Html::label('Текст', 'filter-news-text');
			echo Html::textField('filter-news-text', '', array('class'=>'filter-field'));
		echo Html::closeTag('div');
		
		if (Yii::app()->user->checkAccessSet('developer, administrator, redactor')){
			echo Html::openTag('div', array('class'=>'filter-item'));
				echo Html::label('Статус', Widget::PREFIX_ID.'1');
				echo Html::hiddenField('filter-news-state', '', array('class'=>'filter-field'));
				$this->widget(Widget::alias('DropDownList'), array(
					'data'=>$data = Yii::app()->conformity->get('news_state', 'description', 'code'),
					'widthMin'=>170,
					'widthMax'=>170,
					'multiple'=>true,
					'readonly'=>true,
					'itemAllEnabled'=>true
				));
			echo Html::closeTag('div');
		}
		
		echo Html::openTag('div', array('class'=>'filter-buttons'));
			echo Html::link('', '#', array('class'=>'button search contur circle'));
			echo Html::link('', '#', array('class'=>'button clear contur circle'));
		echo Html::closeTag('div');
		
	echo Html::closeTag('div');
}

echo Html::openTag('div', array('class'=>'container-grid-view'));
	$this->renderPartial('indexGrid', array('model'=>$model));
	echo Html::tag('div', array('class'=>'curtain-loading'), '');
echo Html::closeTag('div');

$params_bt = array(
	'create'=>array(),
	'view'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1)),
	'update'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1)),
	'delete'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled '.
		Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm', 'tabindex'=>-1))
);

foreach ($params_bt as $name=>$params)
	if (!Yii::app()->user->checkAccess('news/'.$name))
		unset($params_bt[$name]);

$this->widget(Widget::alias('ButtonsTable'), array('params'=>$params_bt));

if (isset($params_bt['delete']))
	$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm',
		'htmlOptions'=>array('id'=>'juid-confirm'), 'buttonOk'=>'yes', 'buttonCancel'=>'no'));
