<?php
Yii::app()->asset->add();

$this->widget(Widget::alias('Flashes'),
	array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

$form = $this->beginWidget('ActiveForm');
	
	$this->widget(Widget::alias('DataTable'), array('featureForm'=>$form, 'featureModel'=>$model,
		TreeWidget::PARAM_ATTRIBUTES_FULL=>false,
		'attributes'=>array(
			'title',
			'text'=>array('cells'=>array(TreeWidget::PARAM_ITEMS_INHERIT_DATA=>0,
				1=>array('htmlOptions'=>array('class'=>'cell-name dt-cell-top')),
				2=>array('htmlOptions'=>array('class'=>'cell-value'), 'features'=>array('2.textarea', '2.error'))
			)),
			'date'=>array('features'=>array(
				'2.widget'=>array('widgetName'=>'DateTimePicker','widgetParams'=>array('mode'=>'date')),
				'2.error'
			)),
			'state'=>array('features'=>array(
				'2.widget'=>array('widgetName'=>'DropDownList', 'widgetParams'=>array('widthInitial'=>true, 'readonly'=>true)),
				'2.error_static'
			)),
		)
	));
	
	if ($model->action == 'update')
		$params_bt['save'] = 'submit';
	else
		$params_bt[] = 'ok';
	
	$params_bt['delete'] = array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm'));
	
	$this->widget(Widget::alias('ButtonsTable'), array('form'=>$form, 'model'=>$model, 'mergeParams'=>false,
		'params'=>$params_bt));
	
$this->endWidget();

$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm',
	'htmlOptions'=>array('id'=>'juid-confirm'), 'buttonOk'=>'yes', 'buttonCancel'=>'no'));
