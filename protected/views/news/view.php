<?php
Yii::app()->asset->add();

$is_guest = Yii::app()->user->isGuest;

foreach (array('title', 'text', 'date', 'state') as $attribute)
	$attributes[$attribute] = array(
		'cells'=>Arrays::forming(array(
			array(
				Arrays::FORMING_PARAMS_VALUE=>array(
					'htmlOptions'=>Html::forming(array('dt-cell', 'cell-name', 'dt-cell-top'=>$attribute == 'text')),
					'features'=>array(
						'label'=>array('htmlOptions'=>array('disabled'=>false))
					)
				),
				Arrays::FORMING_PARAMS_ENABLED=>$redactor,
				Arrays::FORMING_PARAMS_NAME=>1
			),
			array(
				'features'=>array(
					'value'=>array('htmlOptions'=>Html::forming(array('value-big', 'value-wide'=>$attribute !== 'text')))
				)
			)
		)) + array(TreeWidget::PARAM_ITEMS_INHERIT_KEY=>false)
	);

if ($is_guest)
    echo Html::link('', '/', array('class' => 'news-left'));

$this->widget(Widget::alias('DataTable'), array('featureModel'=>$model, 'htmlOptions'=>array('class'=>'unrequired' . ($is_guest ? ' news-main' : '')),
	TreeWidget::PARAM_ATTRIBUTES_FULL=>false, 'attributes'=>$attributes));

$this->widget(Widget::alias('ButtonsTable'), array('model'=>$model,
	'params'=>array('delete'=>array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm')))));

$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm',
	'htmlOptions'=>array('id'=>'juid-confirm'), 'buttonOk'=>'yes', 'buttonCancel'=>'no'));
