<?php
$data = $model->search();
$items = Html::tag('div', array('class'=>'gv-table'), '{items}');
$sizer = $data->totalItemCount > 10 ? Html::tag('div', array('class'=>'sizer'), Html::tag('p', array(), 'Показывать по ').
	Html::radioButtonList('filter-size', $data->pagination->pageSize, $model::FILTER_SIZE_VALUES,
	array('class'=>'filter-field', 'container'=>'div', 'separator'=>false,
	'template'=>Html::tag('span', array(), '{input}{label}')))) : '';
$controls = Html::tag('table', array('class'=>'gv-controls'), Html::tag('tr', array(),
	Html::tag('td', array('class'=>'gv-summary'), '{summary}').
	Html::tag('td', array('class'=>'gv-sizer'), $sizer).
	Html::tag('td', array('class'=>'gv-pager'), '{pager}')
));

if ($rows = $data->getData()){
	$filter_id = array();
	$state_remote = Yii::app()->conformity->get('news_state', 'description', 'remote');
	
	foreach ($rows as $row){
		list($id, $state) = Arrays::pop($row, 'id, state');
		
		if ($state === $state_remote)
			$filter_id[] = $id;
 	}
	
	$filter_id = json_encode(array('delete'=>$filter_id));
}

$columns_name = array('title', 'date');

if (Yii::app()->user->checkAccessSet('developer, administrator, redactor'))
	$columns_name[] = 'state';

foreach ($columns_name as $name)
	$columns[] = array(
		'header'=>$model->getAttributeLabel($name),
		'name'=>$name,
		'type'=>'raw',
		'headerHtmlOptions'=>array('class'=>'nowrap'),
		'htmlOptions'=>Html::forming(array('nowrap'=>$name != 'title', 'ta-center'=>$name != 'title'))
	);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'news-grid',
	'dataProvider'=>$data,
	'filterSelector'=>'input.filter-field',
	'ajaxType'=>'POST',
	'template'=>(Yii::app()->request->isAjaxRequest ? ($data->itemCount > 24 ? $controls : '').$items.$controls : $items).
		(!empty($filter_id) ? Html::tag('div', array('id'=>Html::ID_FILTER_ID), $filter_id) : ''),
	'cssFile'=>false,
	'beforeAjaxUpdate'=>'gridviewBeforeAjaxUpdate',
	'afterAjaxUpdate'=>'gridviewAfterAjaxUpdate',
	'ajaxUpdateError'=>'gridviewErrorAjaxUpdate',
	'emptyText'=>Yii::app()->label->get(Yii::app()->request->isAjaxRequest ? 'wgv_empty' : 'wgv_loading'),
	'summaryText'=>'Найдено: <strong>{count}</strong>',
	'pager'=>array(
		'cssFile'=>false,
		'hiddenPageCssClass'=>'dsp-none',
		'firstPageLabel'=>'',
		'prevPageLabel'=>'',
		'nextPageLabel'=>'',
		'lastPageLabel'=>'',
		'maxButtonCount'=>7,
		'header'=>''
	),
	'columns'=>$columns
));
