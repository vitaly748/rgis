<?php
Yii::app()->asset->add(array('gridview'));

$this->widget(Widget::alias('Flashes'),
	array('flashes'=>array('error'=>array('text'=>'error', 'html_options'=>array('class'=>'dsp-none')))));

echo Html::openTag('div', array('class'=>'container-grid-view'));
	$this->renderPartial('indexGrid', array('model'=>$model));
	echo Html::tag('div', array('class'=>'curtain-loading'), '');
echo Html::closeTag('div');

$form = $this->beginWidget('ActiveForm', array('action'=>$this->createUrl('import/import')));
	
	echo Html::activeHiddenField($model, 'file');
	
	$this->widget(Widget::alias('ButtonsTable'), array('params'=>array(
		'import'=>array('html_options'=>array('class'=>Html::PREFIX_BUTTON_STARTER_JD.'-juid-package')),
		'save'=>array('type'=>'a', 'html_options'=>array('href'=>$this->createUrl('import/file'), 'target'=>'_blank',
			'class'=>Html::CLASS_NEED_ID.' disabled', 'tabindex'=>-1)),
		'activate'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled '.
			Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm1', 'tabindex'=>-1)),
		'delete'=>array('html_options'=>array('class'=>Html::CLASS_NEED_ID.' disabled '.
			Html::PREFIX_BUTTON_STARTER_JD.'-juid-confirm2', 'tabindex'=>-1))
	)));
	
$this->endWidget();

$this->widget(Widget::alias('JuiDialog'), array('action'=>'import', 'reset'=>false,
	'htmlOptions'=>array('id'=>'juid-package'), 'buttonOk'=>'ok', 'buttonCancel'=>'cancel', 'idFormSubmit'=>$this->getFormId(),
	'relations'=>array('input[name=file]'=>'input#'.Html::activeId($model, 'file'))));

$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm', 'htmlOptions'=>array('id'=>'juid-confirm1'),
	'buttonOk'=>'yes', 'buttonCancel'=>'no', 'params'=>array('label'=>'import_activate')));

$this->widget(Widget::alias('JuiDialog'), array('view'=>'confirm',
	'htmlOptions'=>array('id'=>'juid-confirm2'), 'buttonOk'=>'yes', 'buttonCancel'=>'no'));
