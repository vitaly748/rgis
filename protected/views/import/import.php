<?php
echo Html::openTag('div');
	$this->widget(Widget::alias('DataTable'), array('featureModel'=>$model,
		TreeWidget::PARAM_ATTRIBUTES_FULL=>false,
		'attributes'=>array(
			'provider'=>array('features'=>array(
				'1.label'=>array('htmlOptions'=>array('disabled'=>false)),
				'2.value'=>array('featureModel'=>false, 'featureValue'=>ImportController::PROVIDER,
					'htmlOptions'=>array('class'=>'value-wide value-update'))
			)),
			'file'=>array('features'=>array('2.widget'=>array(
				'featureModel'=>false, 'widgetName'=>'FileLoader', 'widgetParams'=>
					array('multiple'=>false, 'mimeTypes'=>Arrays::filterKey(Files::MIME_TYPES, 'zip')))))
		)
	));
echo Html::closeTag('div');

$this->widget(Widget::alias('ButtonsTable'), array('params'=>array(
	'ok'=>array('html_options'=>array('class'=>'disabled', 'tabindex'=>-1)), 'cancel')));
