fpPrefixId = 'fp-';
fpPrefixStarter = 'starter-fp';
fpClassButtonPopup = 'fp-popup';

function fpGet(obj){
	var panels = [];
	
	parseId(obj, fpPrefixStarter).forEach(function(id){
		var panel = $('div.flash-panel#' + fpPrefixId + id);
		
		if (panel.length)
			panels.push(panel);
	});

	return panels;
}

function fpToggle(button, show){
	fpGet(button).forEach(function(panel){
		if (show){
			panel.show();
			$('div.flashes').hide();
			
			if (typeof wButtonsTable != 'undefined' && panel.find('div.buttons-table').length)
				wButtonsTableRepaint();
		}else{
			button.removeClass('fixed');
			panel.hide();
		}
	});
}

$(window).load(function(){
	$(document).on('click', function(event){
		var buttons = $('.button.fix, .button-main.fix');
		
		if (buttons.length){
			var target = $(event.target),
				targetButton = target.closest('.button, .button-main'),
				targetPanel = target.closest('.flash-panel');
			
			buttons.each(function(key, button){
				var button = $(this),
					fixed = button.hasClass('fixed');
				
				if (targetButton.length && targetButton[0] == this){
					fpToggle(button, fixed);
					event.preventDefault();
				}else if (fixed && button.hasClass(fpClassButtonPopup)){
					var hit = targetPanel.length &&
						!fpGet(button).every(function(panel){
							return targetPanel[0] != panel[0];
						});
					
					if (!hit)
						fpToggle(button, false);						
				}
			});
		}
		
	});
	
	$(document).on('mouseenter mouseleave', '.button, .button-main', function(event){
		if (!$(this).hasClass('fix'))
			fpToggle($(this), event.type == 'mouseenter');
	});
});
