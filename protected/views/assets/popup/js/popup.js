popupList = {};

function popupAdd(className, funcEnter, funcDrop){
	if (className)
		popupList[className] = {enter: funcEnter, drop: funcDrop};
}

$(window).load(function(){
	$(document).on('click focusin', function(event){
		var target = $(event.target),
			click = event.type == 'click';
		
		for (var className in popupList){
			var funcs = popupList[className],
				objTarget = target.closest('.' + className);
			
			$('.' + className).each(function(){
				var obj = $(this);
				
				if (objTarget.length && objTarget[0] == obj[0]){
					if (click)
						if (funcs.enter)
							funcs.enter(objTarget, target);
						else
							obj.addClass('active');
				}else if (obj.hasClass('active'))
					if (funcs.drop)
						funcs.drop(obj);
					else
						obj.removeClass('active');
			});
		};
	});
	
	$(document).on('keydown', function(event){
		if (event.which == 27)
			for (var className in popupList){
				var funcDrop = popupList[className].drop;
				
				$('.' + className).each(function(){
					if ($(this).hasClass('active'))
						if (funcDrop)
							funcDrop($(this));
						else
							$(this).removeClass('active');
				});
			}
	});
	
	$('.popup').each(function(){
		var name = objMainClass($(this));
		
		if (!(name in popupList)){
			var func = classToWName(name);
			
			popupList[name] = {enter: window['w' + func + 'PopupEnter'], drop: window['w' + func + 'PopupDrop']};
		}
	});
});
