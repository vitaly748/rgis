classStarterClick = 'starter-click';
classTargetClick = 'target-click';
prefixClassHref = 'href';
classNeedId = 'need-id';

function getParent(obj){
	var id = obj.attr('for');
	
	if (id){
		var parent = $('#' + id);
		
		if (parent.length)
			return parent;
	}
	
	return false;
}

function getChild(obj){
	var id = obj.attr('id');
	
	if (id){
		var child = $('[for=' + id + ']');
		
		if (child.length)
			return child;
	}
	
	return false;
}

function parseId(obj, prefix){
	var classes = typeof obj == 'object' ? obj.attr('class') : obj,
		prefixLen = prefix.length,
		ids = [];
	
	if (classes)
		classes.split(/\s+/).forEach(function(name){
			if (name.indexOf(prefix) === 0)
				ids.push(name.length > prefixLen ? name.substring(prefixLen + 1) : true);
		});
	
	return ids;
}

function shielding(string){
	return string.replace(/[\[\]\(\)#;:&,.+*~'"!^$=\>|/]/g, '\\$&');
}

function preparationRender(obj, func){
	var visible = obj.is(':visible');
	
	if (!visible){
		var prev = obj.prev(),
			parent = obj.parent();
		
		$('body').append(obj);
		obj.addClass('dsp-ib');
	}
	
	func(obj);
	
	if (!visible){
		obj.removeClass('dsp-ib');
		if (prev.length)
			prev.after(obj);
		else
			parent.prepend(obj);
	}
}

function textWidth(text, obj){
	var el = $('<label></label'),
		value;
	
	$('body').append(el);
	
	if (obj){
		if (value = obj.css('font-family'))
			el.css('font-family', value);
		
		if (value = obj.css('font-size'))
			el.css('font-size', value);
		
		if (value = obj.css('font-width'))
			el.css('font-width', value);
	}
	
	el.html(text);
	value = el.width();
	el.remove();
	
	return value;
}

function cssPxToInt(value){
	return value === 'none' ? 0 : parseInt(value);
}

function objMainClass(obj){
	var classes = obj.attr('class');
	
	if (classes)
		classes = classes.split(/\s+/);
	else
		return false;
	
	var name = false;
	
	for (var key in classes){
		var word = classes[key];
		
		if (word !== 'popup')
			if (word.indexOf('-') !== -1){
				name = word;
				break;
			}else if (!name)
				name = word;
	}
	
	return name;
}

function classToWName(className){
	var name = '';
	
	className.split('-').forEach(function(word){
		name += word.charAt(0).toUpperCase() + word.slice(1);
	});
	
	return name;
}

function arrayClean(arr){
	var result = [];
	
	arr.forEach(function(value){
		if (value)
			result.push(value);
	});
	
	return result;
}

function arrayLength(arr){
	var length = 0;
	
	for (var key in arr)
		length++;
	
	return length;
}

function getNeedIdRoot(){
	var uid = $('div.ui-dialog:visible');
	
	return uid.length ? uid : $('body');
}

function setNeedId(id){
	var buttonEnabled = typeof buttonSetNeedId !== 'undefined';
	
	if (buttonEnabled)
		buttonSetNeedId(id);
	
	getNeedIdRoot().find('.' + classNeedId).each(function(){
		if (!buttonEnabled || !$(this).hasClass('button'))
			if ($(this).is('input'))
				$(this).val(id);
			else
				$(this).attr('id', id);
	});
}

function resetNeedId(){
	var buttonEnabled = typeof buttonResetNeedId !== 'undefined';
	
	if (buttonEnabled)
		buttonResetNeedId();
	
	getNeedIdRoot().find('.' + classNeedId).each(function(){
		if (!buttonEnabled || !$(this).hasClass('button'))
			if ($(this).is('input'))
				$(this).val('');
			else
				$(this).attr('id', false);
	});
}

function randomLimit(min, max){
	var min = min || 0,
		max = max || 9;
	
	return Math.floor(Math.random() * (max - min)) + min;
}

function uniqid(len){
	var len = len || 8;
	
	do{
		id = '';
		
		for (var count = 0; count < len; count++)
			id = id + randomLimit();
		
		id = parseInt(id);
	}while (!id);
	
	return id;
};

function sprintf(){
	var len = arguments.length,
		count = 1,
		s = '';
	
	if (len > 0){
		s = arguments[0];
		
		while (count < len)
			s = s.replace(/%s/, arguments[count++]);
	}
	
	return s;
}

function outer(obj){
	return $($('<div></div>').html($(obj).clone())).html();
}

$.xhrPool = [];

$.xhrPool.abortAll = function(){
	$(this).each(function(idx, jqXHR){
		jqXHR.abort();
	});
	
	$.xhrPool.length = 0;
};

$.ajaxSetup({
	beforeSend: function(jqXHR){
		$.xhrPool.push(jqXHR);
	},
	complete: function(jqXHR){
		var index = $.xhrPool.indexOf(jqXHR);
		
		if (index > -1)
			$.xhrPool.splice(index, 1);
    }
});

$(window).load(function(){
	$(document).on('change', '.setter', function(){
		var matches = $(this).attr('class').match(/\s+set\-id\-(\w+)/);
		
		if (matches){
			var input = $('input#' + matches[1]);
			
			if (input.length)
				input.val($(this).hasClass('active')).change();
		}
	});
	
	$(document).on('click', '.' + classStarterClick, function(event){
		parseId($(this), classTargetClick).forEach(function(selector){
			setTimeout(function(){
				$(selector).click();
			}, 100);			
		});
		
		event.preventDefault();
	});
});
