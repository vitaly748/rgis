$(window).load(function(){
	var input = $('input[id$=address]');
	
	if (input.length && typeof fiasParams !== 'undefined'){
		fiasParams.input = input;
		fiasParams.inputId = $('input[id$=addressid]');
		fiasParams.inputName = typeof inputName !== 'undefined' ? inputName(input) : false;
		
		if (!fiasParams.inputId.length)
			fiasParams.inputId = false;
		
		fiasParams.input.suggestions({
			serviceUrl: fiasParams.url,
			token: fiasParams.token,
			type: fiasParams.type,
			count: fiasParams.count,
			autoSelectFirst: true,
			onSelect: function(suggestion){
				if (fiasParams.inputId){
					var valOld = fiasParams.inputId.val(),
						valNew = suggestion.data.fias_level == 8 || (suggestion.data.fias_level == 7 && suggestion.data.house) ?
							suggestion.data.fias_id : '';
					
					if (valNew != valOld)
						fiasParams.inputId.val(valNew).change();
					
					if (fiasParams.input.hasClass('fias-cleaning') && !valNew)
						fiasParams.input.val('');
				}
				
				if (fiasParams.inputName && !fiasParams.validationSkip)
					wActiveFormValidateAttribute(fiasParams.inputName);
			}
		});
		
		if (!fiasParams.post && fiasParams.input.val() && fiasParams.inputId && !fiasParams.inputId.val()){
			fiasParams.validationSkip = true;
			
			setTimeout(function(){
				fiasParams.input.focus();
				
				setTimeout(function(){
					fiasParams.input.blur();
				}, 10);
			}, 10);
			
			setTimeout(function(){
				fiasParams.validationSkip = false;
			}, 3000);
		}
	}
});
