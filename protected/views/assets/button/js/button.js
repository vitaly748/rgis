buttonPrefixStarterSubmit = 'starter-submit';
buttonPrefixStarterCommand = 'starter-command';
buttonIdCommandDef = 'form-command';
buttonVarNeedId = 'needIdFilter';
buttonPatternClearId = /\/?\d+$/;

function buttonStarterSubmit(obj){
	var form = obj instanceof Object ? obj.closest('form') : $('form#' + obj);
	
	if (form.length)
		form.submit();
}

function buttonCheckStarters(button, skipCheckJD, event){
	if (!skipCheckJD && typeof prefixStarterJD != 'undefined' && parseId(button, prefixStarterJD).length){
		if (event)
			event.preventDefault();
		
		return true;
	}
	
	var starterSubmit = parseId(button, buttonPrefixStarterSubmit),
		starterCommand = parseId(button, buttonPrefixStarterCommand);
	
	if (starterSubmit.length){
		buttonStarterSubmit(starterSubmit[0] === true ? button : starterSubmit[0]);
		
		if (event)
			event.preventDefault();
		
		return true;
	}
	
	if (starterCommand.length){
		var input = $('input#' + (starterCommand[0] === true ? buttonIdCommandDef : starterCommand[0]));
		
		if (input.length){
			var classes = button.attr('class').split(/\s+/),
				index = classes.indexOf('button') + 1;
			
			if (index && index < classes.length){
				var value = classes[index],
					jd = button.closest('div.ui-dialog-content');
				
				if (jd.length){
					var idJd = jd.attr('id');
					
					if (idJd)
						value = idJd.replace('-', '_') + '/' + value;
				}
				
				input.val(value);
				buttonStarterSubmit(input);
			}
		}
		
		if (event)
			event.preventDefault();
		
		return true;
	}
}

function buttonToggle(button, enabled){
	button.attr('tabindex', -Number(!enabled)).toggleClass('disabled', !enabled);
}

function buttonSetNeedId(id){
	var filter = typeof buttonVarNeedId !== 'undefined' ? window[buttonVarNeedId] : false;
	
	getNeedIdRoot().find('.button.' + classNeedId).each(function(){
		var classes = $(this).attr('class').split(/\s+/),
			index = classes.indexOf('button') + 1,
			href = $(this).attr('href');
		
		if (href && index && index < classes.length){
			var type = classes[index],
				set = !filter || !(type in filter) || filter[type].indexOf(id) === -1;
			
			$(this).attr('href', href.replace(buttonPatternClearId, '') + (set ? '/' + id : ''));
			buttonToggle($(this), set);
		}
	});
}

function buttonResetNeedId(){
	getNeedIdRoot().find('.button.' + classNeedId).each(function(){
		buttonToggle($(this), false);
		$(this).attr('href', $(this).attr('href').replace(buttonPatternClearId, ''));
	});
}

$(window).load(function(){
	$(document).on('click', '.button, .button-main', function(event){
		if (!$(this).hasClass('disabled')){
			if ($(this).hasClass('fix'))
				$(this).toggleClass('fixed');
			
			buttonCheckStarters($(this), false, event);
		}else
			event.preventDefault();
	});
});
