inputPrefixChild = 'input-child';
inputClassChildDNI = 'input-child-dni';
inputClassChildDisabled = 'input-child-disabled';
inputClassLabelHoverDisabled = 'input-label-hover-disabled';
inputPattern = {};
inputPatternGroupSet = [];
inputEditable = false;
inputEditableTimer = false;
inputEditableText = false;
inputInvalidCharObj = false;
inputInvalidCharTimer = false;

$.fn.getCursorPosition = function(ext){
	var input = $(this)[0];
	
	if (!input)
		return false;
	
	if ('selectionStart' in input)
		return !ext ? input.selectionStart : [input.selectionStart, input.selectionEnd - input.selectionStart];
	else if (document.selection){
		var range = document.selection.createRange(),
			len = range.text.length;
		
		range.moveStart('character', -input.value.length);
		
		var start = range.text.length - len;
		
		return !ext ? start : [start, len];
	}else
    	return false;
}

$.fn.setCursorPosition = function(pos){
	var input = $(this)[0];
	
	if (!input)
		return false;
	
	if (input.setSelectionRange)
		input.setSelectionRange(pos, pos);
	else if (input.createTextRange){
		var range = input.createTextRange();
		
		range.collapse(true);
		range.moveEnd('character', pos);
		range.moveStart('character', pos);
		range.select();
	}else
		return false;
}

function inputCheckLabel(obj){
	var label = getChild(obj);
	
	if (label){
		var isInput = obj.is('input') || obj.is('textarea'),
			disabled = isInput ? !!obj.attr('disabled') : obj.hasClass('disabled');
		
		label.attr('disabled', disabled).change();
	}
}

function inputCheckChilds(input){
	var disabled = (input.attr('disabled') && !input.hasClass(inputClassChildDNI)) || !input.attr('checked');
	
	parseId(input, inputPrefixChild).forEach(function(id){
		var child = $('#' + id);
		
		if (child.length)
			if (!child.hasClass(inputClassChildDisabled)){
				var isInput = child.is('input') || child.is('textarea'),
					childDisabled = isInput ? !!child.attr('disabled') : child.hasClass('disabled');
				
				if (disabled != childDisabled){
					if (isInput)
						child.attr('disabled', disabled);
					else{
						child.toggleClass('disabled', disabled);
						child.find('div input').attr('disabled', disabled);
					}
					
					inputCheckLabel(child);
				}
			}
	});
}

function inputChRSpan(obj){
	var span = obj.is('span'),
		res = span ? obj.prev() : obj.next();
	
	if (!res.length)
		return false;
	else if ((span && res.is('input')) || (!span && res.is('span')))
		return res;
	else
		res = span ? res.prev() : res.next();
	
	return res.length && ((span && res.is('input')) || (!span && res.is('span'))) ? res : false;
}

function inputChRRepaint(span){
	if (span.hasClass('toggle'))
		span.removeClass('toggle').addClass('toggle');
}

function inputLabelAssistant(label){
	var parent = getParent(label),
		assistant;
	
	if (parent){
		if (parent.is('input') || parent.is('textarea')){
			if (['checkbox', 'radio'].indexOf(parent.attr('type')) != -1)
				assistant = inputChRSpan(parent);
		}else{
			var name = objMainClass(parent);
			
			if (name){
				name = 'w' + classToWName(name) + 'LabelAssistant';
				
				if (window[name])
					assistant = window[name](parent);
			}
		}
	}
	
	return assistant ? assistant : (parent ? parent : false);
}

function inputEditableCheck(){
	if (inputEditable && inputEditable in inputPattern){
		var input = $('input#' + inputEditable);
		
		if (input.length){
			if (input.is(':focus')){
				if (input.val() !== inputEditableText){
					input.change();
					inputEditableText = input.val();
				}
				
				return;
			}
			
			input.removeClass('editable');
		}
	}
	
	clearTimeout(inputEditableTimer);
	inputEditableTimer = inputEditable = false;
}

function inputInvalidCharShow(input){
	if (typeof inputInvalidCharMessage != 'undefined'){
		var obj = input.parent().children('.errorMessage');
		
		if (obj.length){
			if (inputInvalidCharObj)
				if (inputInvalidCharObj[0] !== obj[0])
					inputInvalidCharHide();
				else
					clearTimeout(inputInvalidCharTimer);
			
			obj.html(inputInvalidCharMessage).show().parent('div').addClass('error');
			inputInvalidCharObj = obj;
			inputInvalidCharTimer = setTimeout(inputInvalidCharHide, 3000);
		}
	}
}

function inputInvalidCharHide(){
	if (inputInvalidCharTimer){
		clearTimeout(inputInvalidCharTimer);
		inputInvalidCharTimer = false;
	}
	
	if (inputInvalidCharObj){
		if (inputInvalidCharObj.is(':visible') && inputInvalidCharObj.html() === inputInvalidCharMessage)
			inputInvalidCharObj.hide().parent('div').removeClass('error');
		
		inputInvalidCharObj = false;
	}
}

function inputCheckGroups(id, value){
	var input = $('input#' + id);
	
	if (id in inputPattern && input.length){
		var pattern = inputPattern[id];
		
		if (typeof value != 'string')
			value = input.val();
		
		if ('groups' in pattern && 'groups_min' in pattern){
			if (typeof inputPattern[id].groups_significant == 'undefined'){
				var idSets = [];
				
				pattern['groups'].forEach(function(group){
					if (group['state'] == 'significant' && group['id'] in inputPatternGroupSet &&
						idSets.indexOf(group['id']) == -1)
							idSets.push(group['id']);
				});
				
				inputPattern[id].groups_significant = idSets;
				inputPattern[id].groups_significant_qu = idSets.length;
			}
			
			var find = 0;
			
			inputPattern[id].groups_significant.forEach(function(idSet){
				if (new RegExp('[' + inputPatternGroupSet[idSet] + ']').test(value))
					find++;
			});
			
			inputPattern[id].groups_significant_find = find;
		}
	}
}

function inputName(input){
	var name = input.attr('name');
	
	if (!name)
		return false;
	
	var re = /\[([\w\d_]+)\]$/,
		result = re.exec(name);
	
	return result && result[1] ? result[1] : false;
}

$(window).load(function(){
	$(document).on('click', 'span.checkbox, span.radio', function(){
		var input = inputChRSpan($(this));
		
		if (input){
			var type = ['checkbox', 'radio'].indexOf(input.attr('type'));
			
			if (type != -1 && !input.attr('disabled')){
				var checked = input.attr('checked');
				
				if (!checked || !type)
					input.attr('checked', !checked);
				
				input.focus().change();
			}
		}
	});
	
	$(document).on('mouseenter mouseleave', 'label', function(event){
		var obj = inputLabelAssistant($(this));
		
		if (obj && !obj.hasClass(inputClassLabelHoverDisabled)){
			obj.toggleClass('hover', event.type == 'mouseenter');
			
			if (obj.is('span') && (obj.hasClass('checkbox') || obj.hasClass('radio')))
				inputChRRepaint(obj);
		}
	});
	
	$(document).on('change', 'input[type=checkbox], input[type=radio]', function(){
		var inputs = $(this).attr('type') == 'checkbox' ? $(this) : 
				$('input[type=radio][name=' + shielding($(this).attr('name')) + ']');
		
		if (inputs.length > 1 && $(this).attr('disabled') && $(this).attr('checked')){
			var obj = $(this);
			
			inputs.each(function(){
				if ($(this)[0] !== obj[0] && !$(this).attr('disabled')){
					$(this).attr('checked', true);
					obj.attr('checked', false);
					
					return false;
				}
			});
		}
		
		inputs.each(function(){
			inputCheckChilds($(this));
			inputCheckLabel($(this));
		});
	});
	
	$(document).on('focus', 'input[type=text], input[type=password], textarea', function(){
		if ($(this).attr('readonly'))
			if ($(this).attr('type') === 'password')
				$(this).attr('readonly', false);
			else
				return;
		
		var id = $(this).attr('id');
		
		if (id && id in inputPattern){
			if (inputEditable)
				$('input#' + inputEditable).removeClass('editable');
			
			inputEditable = id;
			inputEditableText = $(this).val();
			$(this).addClass('editable');
			
			if (!inputEditableTimer)
				inputEditableTimer = setInterval(inputEditableCheck, 300);
		}
	});
	
	$(document).on('keypress change', 'input[type=text], input[type=password], textarea', function(event, hype){
		if (hype)
			return;
		
		var input = $(this),
			id = input.attr('id'),
			keypressed = event.type == 'keypress';
		
		if (id && id in inputPattern && id === inputEditable){
			var pattern = inputPattern[id],
				regexp = pattern['regexp'];
			
			if (!regexp || (keypressed && (!event.which || event.which === 8 || event.which === 13)))
				return;
			
			var value = input.val(),
				pos = input.getCursorPosition(true),
				len = value.length,
				lenActual = pos[1] ? len - pos[1] : len,
				correct = valueInsert = false,
				valueDiff = valueLeft = valueRight = '';
			
			regexp = new RegExp(regexp);
			
			if (event.which){
				value = value.slice(0, pos[0]) + String.fromCharCode(event.which) + value.slice(pos[0] + pos[1]);
				pos[0]++;
				len++;
				lenActual++;
			}
			
			if (value && !regexp.test(value)){
				if (value !== inputEditableText){
					var diff = len - inputEditableText.length;
					
					if (diff > 0){
						if (pos[0] >= diff){
							valueLeft = value.substr(0, pos[0] - diff);
							valueRight = value.substr(pos[0]);
							
							if (valueLeft + valueRight === inputEditableText)
								valueDiff = value.substr(pos[0] - diff, diff);
						}
					}else if (!diff && value.slice(pos[0]) === inputEditableText.slice(pos[0])){
						var cur = pos[0] - 1;
						valueRight = value.slice(pos[0]);
						
						while (cur >= 0){
							if (value[cur] !== inputEditableText[cur])
								valueDiff = value[cur] + valueDiff;
							else
								break;
							
							cur--;
						}
						
						if (valueDiff){
							valueLeft = value.substr(0, pos[0] - valueDiff.length);
							
							if (valueLeft && valueLeft !== inputEditableText.slice(0, valueLeft.length))
								valueLeft = '';
						}
					}
				}
				
				if (valueDiff)
					while (true){
						value = valueLeft + valueDiff + valueRight;
						
						if (!valueDiff || regexp.test(value))
							break;
						else{
							valueDiff = valueDiff.slice(0, -1);
							correct = true;
						}
					}
				
				while (value)
					if (regexp.test(value))
						break;
					else{
						value = value.slice(0, -1);
						correct = true;
					}
				
				len = value.length;
			}
			
			if (correct)
				inputInvalidCharShow(input);
			else
				inputInvalidCharHide();
			
			if ('groups' in pattern){
				var groupsSeparation = pattern['groups_separation'],
					delimiter = pattern['delimiter'],
					decoration = pattern['decoration'];
				
				if (value && groupsSeparation && delimiter)
					if (!decoration || !new RegExp('[' + decoration + ']').test(value)){
						groupsSeparation = groupsSeparation.split('');
						
						var delimiterExists = value.indexOf(delimiter) !== -1,
							format = !delimiterExists || !decoration || (!correct && len < inputEditableText.length),
							lenSeparation = groupsSeparation.length,
							countPath = countSeparation = lengthGroup = 0;
						
						if (!format){
							var paths = value.split(delimiter);
							format = true;
							
							for (key in paths)
								paths[key] = paths[key].length;
							
							while (countPath < paths.length - 1){
								if (countSeparation < lenSeparation)
									lengthGroup = groupsSeparation[countSeparation++];
								
								if (paths[countPath] < lengthGroup){
									format = false;
									break;
								}else if (paths[countPath] > lengthGroup)
									paths.splice(countPath + 1, 0, paths[countPath] - lengthGroup);
								
								countPath++;
							}
						}
						
						if (format){
							var valueStack = delimiterExists ? value.replace(new RegExp(delimiter, 'g'), '') : value,
								valueNew = '',
								reverse = pattern['separation_reverse'],
								indexBorder = valueStack.search(/[.,]/),
								residue = false;
							countSeparation = 0;
							
							if (indexBorder !== -1){
								residue = valueStack.substr(indexBorder);
								valueStack = valueStack.substr(0, indexBorder);
							}
							
							if (reverse)
								valueStack = valueStack.split('').reverse().join('');
							
							while (valueStack){
								if (countSeparation < lenSeparation)
									lengthGroup = groupsSeparation[countSeparation++];
								
								if (valueNew)
									valueNew += delimiter;
								
								valueNew += valueStack.substr(0, lengthGroup);
								valueStack = valueStack.substr(lengthGroup);
							}
							
							if (reverse)
								valueNew = valueNew.split('').reverse().join('');
							
							if (indexBorder !== -1)
								valueNew += residue;
							
							if (value !== valueNew){
								value = valueNew;
								correct = true;
								len = value.length;
							}
						}
					}
				
				inputCheckGroups(id, value);
			}
			
			var valueChange = inputEditableText != value;
			
			if (valueChange)
				inputEditableText = value;
			
			if (correct && (valueChange || !keypressed))
				input.val(value);
			
			if (input.is(':focus') && ((keypressed && correct && valueChange) || (!keypressed && (correct || valueChange))))
				input.setCursorPosition(Math.max(Math.min(pos[0] + len - lenActual, len + 1), 0));
			
			if (keypressed){
				if (valueChange)
					setTimeout(function(){
						input.trigger('change', [1]);
					}, 100);
				
				if (!valueChange || (correct && valueChange))
					return false;
			}
		}
	});
	
	$('input[type=text], input[type=password], textarea').on('focusout', function(){
		var value = $(this).val();
		
		$(this).val(value.length ? '' : '1').trigger('change', [2]).val(value).trigger('change', [2]);
	});
	
	$(document).on('click', function(event){
		if (inputEditable){
			var input = $('input#' + inputEditable);
			
			if (input.length && input[0] !== event.target)
				inputEditableCheck();
		}
	});
	
	$('input, textarea').each(function(){
		if (['checkbox', 'radio'].indexOf($(this).attr('type')) != -1)
			inputCheckChilds($(this));
		
		inputCheckLabel($(this));
	});
	
	for (id in inputPattern)
		inputCheckGroups(id);
});
