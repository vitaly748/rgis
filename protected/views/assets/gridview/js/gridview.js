gridviewIdFilterId = 'filter-id';
gridviewIdGVParams = 'gv-params';
gridviewParams = null;

function gridviewBeforeAjaxUpdate(){
	setTimeout(function(){
		$("div.curtain-loading").show();
	}, 200);
	
	if (typeof resetNeedId !== 'undifined')
		resetNeedId();
}

function gridviewAfterAjaxUpdate(){
	gridviewFilterRepaint();
	
	setTimeout(function(){
		$("div.curtain-loading").hide();
	}, 100);
	
	if (typeof wFlashesToggle != 'undefined' && typeof gridviewUpdated != 'undefined')
		wFlashesToggle('error', false);
	
	gridviewUpdated = true;
	
	var filterId = $('div#' + gridviewIdFilterId),
		gvParams = $('div#' + gridviewIdGVParams);
	
	if (filterId.length && typeof buttonVarNeedId !== 'undefined'){
		window[buttonVarNeedId] = JSON.parse(filterId.html());
		filterId.detach();
	}
	
	if (gvParams.length){
		gridviewParams = JSON.parse(gvParams.html());
		gvParams.detach();
	}
	
	if (typeof gridviewAfterAjaxUpdateHandler !== 'undefined')
		gridviewAfterAjaxUpdateHandler();
}

function gridviewErrorAjaxUpdate(xhr){
	var text = xhr.responseText;
	
	if (typeof wFlashesSet != 'undefined')
		wFlashesSet('error', text ? text : 'Ошибка');
	
	gridviewUpdated = true;
	
	$('.grid-view').find('td').html('&nbsp;');
	$("div.curtain-loading").hide();
}

function gridviewFilterRepaint(){
	var filter = $('div.filter'),
		filterItems = filter.children('div.filter-item'),
		filterLabels = filterItems.children('label'),
		buttonSearch = filter.find('a.button.search');
	
	if (filterLabels.length){
		var filterTop = filter.position().top;
		
		if (filterItems.last().position().top == filterTop)
			filterLabels.css('width', 'initial');
		else{
			var widthMax = 0;
			
			filterLabels.each(function(){
				var width = textWidth($(this).html()) + 5;
				
				if (width > widthMax)
					widthMax = width;
			});
			
			filterLabels.css('width', widthMax + 'px');
		}
		
		if (buttonSearch.length){
			var buttonTop = buttonSearch.position().top;
			
			filter.css('height', buttonTop == filterTop ? 'initial' : buttonTop - filterTop + 39 + 'px');
		}
	}
}

$(window).load(function(){
	var gridview = $('.grid-view'),
		gridviewDatepicker = {};
	
	setTimeout(function(){
		gridviewFilterRepaint();
		
		$(window).resize(function(){
			gridviewFilterRepaint()
		});
		
		if (gridview.length){
			gridview.each(function(){
				var parent = $(this).parent();
				
				if ($(this).is(':visible') && parent.hasClass('container-grid-view') &&
					parent.children('div.curtain-loading').length && $(this).find('td').length < 2)
						$.fn.yiiGridView.update($(this).attr('id'));
			});
			
			$('div.filter .button').on('click', function(event){
				event.preventDefault();
				
				var button = $(this),
					filter = button.closest('div.filter'),
					gridview = filter.parent().find('.grid-view');
				
				if (gridview.length){
					if (button.hasClass('clear')){
						filter.children('.filter-item').not('.fi-not-clean').each(function(){
							var filterItem = $(this);
							
							filterItem.find('input[class^=filter-field], input.filter-field-text, input.hasDatepicker').val('');
							
							filterItem.find('div.drop-down-list').each(function(){
								$(this).find('input.' + wddlClassInput + ':checked').attr('checked', false);
								wDropDownListTitleRepaint($(this));
							});
						});
					}
					
					$.fn.yiiGridView.update(gridview.attr('id'));
				}
			});
			
			$(document).on('click touchstart', function(event){
				if (event.type == 'touchstart')
					$(event.target).trigger('click');
				else{
					var target = $(event.target),
					td = target.closest('.grid-view table.items td');
					
					if (td.length && !td.hasClass('button-column')){
						var obj = target.closest('.grid-view'),
							id = obj.attr('id'),
							href = parseId(obj, prefixClassHref),
							rowId = parseInt($.fn.yiiGridView.getSelection(id)[0]);
						
						if (href.length && rowId)
							setTimeout(function(){
								$.xhrPool.abortAll();
								location = href[0] + rowId;
							}, 100);
						else if (typeof setNeedId !== 'undifined')
							if (rowId)
								setNeedId(rowId);
							else
								resetNeedId();
					}
				}
			});
		}
		
		$('div.filter div.drop-down-list').on('change', function(){
			var obj = $(this),
				input = obj.closest('.filter-item').children('input[type=hidden]');
			
			if (!obj.hasClass('active') && input.length){
				var value = [];
				
				obj.find('input.' + wddlClassInput + ':checked').each(function(){
					value.push($(this).val());
				});
				
				value = value.join(',');
				
				if (input.val() != value)
					input.val(value).change();
			}
		});
		
		$('div.filter input.filter-field-text').on('keypress focusout', function(event){
			if (event.type == 'keypress' && event.which !== 13)
				return;
			
			var input = $(this).closest('.filter-item').children('input[type=hidden]')
				value = $(this).val();
			
			if (input.length && input.val() != value)
				input.val(value).change();
		});
		
		$('div.filter input.hasDatepicker').on('change', function(){
			var obj = $(this),
				id = obj.attr('id'),
				input = obj.closest('.filter-item').children('input[type=hidden]');
			
			if (!(id in gridviewDatepicker) || !gridviewDatepicker[id])
				gridviewDatepicker[id] = setInterval(function(){
					if (!$('div.ui-datepicker:visible').length){
						clearInterval(gridviewDatepicker[id]);
						gridviewDatepicker[id] = false;
						
						if (input.val() != obj.val())
							input.val(obj.val()).change();
					}
				}, 100)
		});
	}, 200);
});
