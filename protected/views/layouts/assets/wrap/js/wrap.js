function wrapSendSecureResult(result){
	$.ajax({
		url: '/main/secure',
		data: {id: result ? 1 : 0},
		dataType: 'json',
		cache: false
	});
}

$(window).load(function(){
	var statistics = $('div.statistics');
	
	if (statistics.length){
		var statisticsLi = statistics.find('a.liveinternet img'),
			statisticsYm = statistics.find('a.yandexmetrika img');
		
		if (statisticsLi.length)
			statisticsLi.attr('src', '//counter.yadro.ru/hit?t14.1;r' + escape(document.referrer) +
				((typeof(screen) == 'undefined') ? '' : ';s' + screen.width + '*' + screen.height + '*' +
				(screen.colorDepth ? screen.colorDepth : screen.pixelDepth)) + ';u' + escape(document.URL) + ';' + Math.random());
		
		if (statisticsYm.length)
			(function (d, w, c){
				(w[c] = w[c] || []).push(function(){
					try{
						w.yaCounter43649619 = new Ya.Metrika({
							id: 43649619,
							clickmap: true,
							trackLinks: true,
							accurateTrackBounce: true
						});
					}catch(e){
					}
				});
				
				var n = d.getElementsByTagName("script")[0],
					s = d.createElement('script'),
					f = function(){
						n.parentNode.insertBefore(s, n);
					};
				
				s.type = 'text/javascript';
				s.async = true;
				s.src = 'https://mc.yandex.ru/metrika/watch.js';
				
				if (w.opera == "[object Opera]")
					d.addEventListener("DOMContentLoaded", f, false);
				else
					f();
			})(document, window, "yandex_metrika_callbacks");
	}
	
	if (typeof secureUrl !== 'undefined'){
		var send = false;
		
		$.ajax({
			url: secureUrl,
			dataType: 'jsonp',
			cache: false,
			complete: function(jsXHR, textStatus){
				send = true;
				wrapSendSecureResult(jsXHR.status === 200);
			}
		});
		
		setTimeout(function(){
			if (!send)
				wrapSendSecureResult(false);
		}, 5000);
	}
});
