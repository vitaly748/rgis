<!DOCTYPE html>

<?php
Yii::app()->getClientScript()->registerCoreScript('jquery');
Yii::app()->asset->add(array('wrap', 'content', 'popup'));

$console = $this->widget(Widget::alias('Console'), array(), true);
$flashes = $this->widget(Widget::alias('Flashes'), array(), true);
$profile = $this->getProfile();

echo Html::openTag('html');
	
	echo Html::openTag('head');
		
		echo Html::metaTag('IE=edge, chrome=1', null, 'X-UA-Compatible');
		echo Html::metaTag(null, null, null, array('charset'=>Yii::app()->charset));
		echo Html::metaTag(Yii::app()->language, 'language', null);
		echo Html::tag('title', array(), Yii::app()->setting->get('name'));
		echo Html::tag('link', array('rel'=>'shortcut icon', 'href'=>Yii::app()->homeUrl.'icon.ico'));
		
	echo Html::closeTag('head');
	
	echo Html::openTag('body');
		
		echo $console;
		
		echo Html::openTag('header');
			
			echo Html::openTag('div', array('id'=>'headline'));
				
				echo Html::openTag('div', array('class'=>'limiter'));
					
					echo Html::openTag('a', array('class'=>'logo', 'href'=>'/'));
						echo Html::openTag('div', array('class'=>'logo-frame'));
							echo Html::tag('div', array('class'=>'logo-title'), Yii::app()->setting->get('name'));
						echo Html::closeTag('div');
					echo Html::closeTag('a');
					
					echo $profile;
					
				echo Html::closeTag('div');
				
			echo Html::closeTag('div');
			
			echo Html::openTag('div', array('id'=>'scheme-main'));
				
				echo Html::tag('div', array('class'=>'limiter'), Yii::app()->scheme->render('main', true));
				
			echo Html::closeTag('div');
			
			Yii::app()->scheme->render('chain');
			
		echo Html::closeTag('header');
		
		echo Html::openTag('div', array('id'=>'base', 'class'=>'limiter'));
			
			Yii::app()->scheme->render('lateral');
			Yii::app()->scheme->render('stack');
			Yii::app()->scheme->render('title');
			
			echo Html::openTag('div', Html::forming(array(
				'id'=>'content',
				'content-offset'=>$offset = Yii::app()->scheme->contentOffset,
				'centering'=>!$offset && Yii::app()->controller->route !== 'main/index'
			)));

					echo $flashes;
					echo $content;
					
			echo Html::closeTag('div');
			
		echo Html::closeTag('div');
		
		echo Html::openTag('footer');
			echo Html::openTag('div', array('class'=>'limiter'));
				echo Html::tag('div', array('class'=>'left'), '');
				
				echo Html::openTag('div', array('class'=>'right'));
					if ($this->working){
						echo Html::openTag('div', array('class'=>'statistics'));
							if (Yii::app()->setting->get('gui', 'counter_li'))
								echo Html::tag('a', array('class'=>'liveinternet', 'target'=>'_blank', 'rel'=>'nofollow',
									'href'=>'//www.liveinternet.ru/click'), Html::tag('img', array('border'=>'0', 'width'=>'88', 'height'=>'31',
									'title'=>'LiveInternet: показано число просмотров за 24 часа, посетителей за 24 часа и за сегодня', 'alt'=>'')));
							
							if (Yii::app()->setting->get('gui', 'counter_ym'))
								echo Html::tag('a', array('class'=>'yandexmetrika', 'target'=>'_blank', 'rel'=>'nofollow',
									'href'=>'https://metrika.yandex.ru/stat/?id=43649619&amp;from=informer'),
									Html::tag('img', array('border'=>'0', 'width'=>'88', 'height'=>'31',
									'src'=>'https://informer.yandex.ru/informer/43649619/3_0_FFFFFFFF_EFEFEFFF_0_pageviews', 'alt'=>'Яндекс.Метрика',
									'title'=>'Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)',
									'class'=>'ym-advanced-informer', 'data-cid'=>'43649619', 'data-lang'=>'ru'
								)));
						echo Html::closeTag('div');
					}
				echo Html::closeTag('div');
			echo Html::closeTag('div');
		echo Html::closeTag('footer');
		
	echo Html::closeTag('body');
	
echo Html::closeTag('html');

if ($this->working && Html::SECURE_HOST && Yii::app()->user->isGuest && Html::secureCheck() === null)
	Html::registerScriptParams('secureUrl', false, false, Html::changeSecureUrl($this->createAbsoluteUrl('main/secure')));
