<?php
$items = Html::tag('div', array('class'=>'gv-table'), '{items}');

foreach ($model->allowedAttributeNames as $name)
	$columns[] = array(
		'header'=>$model->getAttributeLabel($name),
		'name'=>$name,
		'headerHtmlOptions'=>array('class'=>'nowrap'),
		'htmlOptions'=>array('class'=>'nowrap ta-center')
	);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'subsidies-grid',
	'dataProvider'=>$model->search(),
	'ajaxType'=>'POST',
	'template'=>$items,
	'cssFile'=>false,
	'beforeAjaxUpdate'=>'gridviewBeforeAjaxUpdate',
	'afterAjaxUpdate'=>'gridviewAfterAjaxUpdate',
	'ajaxUpdateError'=>'gridviewErrorAjaxUpdate',
	'emptyText'=>Yii::app()->label->get(Yii::app()->request->isAjaxRequest ? 'wgv_empty' : 'wgv_loading'),
	'selectableRows'=>0,
	'columns'=>$columns
));
