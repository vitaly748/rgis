<?php
/**
 * Модель данных новостной ленты
 * @author v.zubov
 */
class NewsModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var string Заголовок новости
	 */
	public $title;
	
	/**
	 * @var string Дата публикации новости
	 */
	public $date;
	
	/**
	 * @var string Текст новости
	 */
	public $text;
	
	/**
	 * @var integer Код статуса новости
	 */
	public $state;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('title, text', 'safe'),
		array('date', 'date'),
		array('state', 'numerical', 'integerOnly'=>true)
	);
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Функция поиска
	 * @return CommandDataProvider Объект-поставщик данных
	 */
	public function search(){
		$main_page = Yii::app()->controller->id == 'main';
		$params_provider = array(
			'pagination'=>array('pageSize'=>isset($_POST['filter-size']) ? $_POST['filter-size'] : static::FILTER_SIZE_DEF),
			'sort'=>array(
				'defaultOrder'=>'date_num DESC, id DESC',
				'attributes'=>array(
					'title', 'text', 'state',
					'date'=>array(
						'asc'=>'date_num',
						'desc'=>'date_num DESC'
					)
				)
			)
		);
		
		if (!Yii::app()->request->isAjaxRequest && !$main_page)
			$data = new CommandDataProvider($this, $params_provider);
		else{
			$params_fix = Arrays::pop($_POST, 'filter-news-title, filter-news-text, filter-news-state', false, true);
			list($filter_title, $filter_text, $filter_state) = array_values($params_fix);
			
			try{
				$where = array();
				
				if ($filter_title){
					preg_match_all('/[A-Za-zА-яЁё\d.]+/u', $filter_title, $matches);
					
					if (!empty($matches[0]))
						$where[] = 'LOWER("title") '.(count($matches[0]) == 1 ? "LIKE '%".mb_strtolower(reset($matches[0]))."%'" :
							"SIMILAR TO '%(".mb_strtolower(implode('|', $matches[0])).")%'");
				}
				
				if ($filter_text){
					preg_match_all('/[A-Za-zА-яЁё\d.]+/u', $filter_text, $matches);
					
					if (!empty($matches[0]))
						$where[] = 'LOWER("text") '.(count($matches[0]) == 1 ? "LIKE '%".mb_strtolower(reset($matches[0]))."%'" :
							"SIMILAR TO '%(".mb_strtolower(implode('|', $matches[0])).")%'");
				}
				
				if ($filter_state)
					$where[] = '"state_num" IN ('.$filter_state.')';
				
				if (!Yii::app()->user->checkAccessSet('developer, administrator, redactor') || $main_page)
					$where[] = '"date_num" <= \''.date(Swamper::FORMAT_DATE_SYSTEM).'\' AND "state_num" = '.
						Yii::app()->conformity->get('news_state', 'code', 'publication');
				
				$where = !$where ? false : (count($where) == 1 ? reset($where) : array_merge(array('and'), $where));
				
				$params_provider['criteria'] = Yii::app()->db->createCommand()->
					from('('.
						Yii::app()->db->createCommand()->
							select(array('n.id', 'n.title', 'n.text', 'TO_CHAR("n"."date", \'DD.MM.YYYY\') AS "date"',
								'n.date date_num', 'd.description state', 'n.state state_num'))->
							from('{{news}} n')->
							leftJoin('{{directory_conformity}} d', '"d"."section"=\'news_state\' AND "d"."code"="n"."state"')->
							text.') "t"')->
					where($where);
				
				$params_provider['countCriteria'] = $where ?
					Yii::app()->db->createCommand()->
						from('('.
							Yii::app()->db->createCommand()->
								select(array('n.title', 'n.text', 'n.date date_num', 'n.state state_num'))->
								from('{{news}} n')->
								text.') "t"')->
						where($where)
					:
					Yii::app()->db->createCommand()->
						from('{{news}}');
							
				$data = new CommandDataProvider($this, $params_provider);
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('news_list', $params_fix, $ok ? 'ok' : 'news_list');
		}
		
		return $data;
	}
	
}
