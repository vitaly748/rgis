<?php
/**
 * Модель данных ведения логов событий
 * @author v.zubov
 */
class NotificationModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Идентификатор пользователя, вызвавшего событие
	 */
	public $id_addressee;
	
	/**
	 * @var integer Идентификатор сообщения уведомления из справочника уведомлений
	 */
	public $id_message;
	
	/**
	 * @var integer Идентификатор объекта, связанного с уведомлением
	 */
	public $id_subject;
	
	/**
	 * @var integer Код статуса уведомления
	 */
	public $state;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'addressee'=>array(self::BELONGS_TO, 'UserModel', 'id_addressee'),
			'message'=>array(self::BELONGS_TO, 'DirectoryNotificationMessageModel', 'id_message'),
		);
	}
	
}
