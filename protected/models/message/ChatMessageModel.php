<?php
/**
 * Модель данных сообщения чата пользователей
 * @author v.zubov
 */
class ChatMessageModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Идентификатор чата
	 */
	public $id_chat;
	
	/**
	 * @var integer Идентификатор пользователя-отправителя сообщения
	 */
	public $id_owner;
	
	/**
	 * @var integer Идентификатор пользователя-получателя сообщения
	 */
	public $id_addressee;
	
	/**
	 * @var string Дата и время отправления сообщения
	 */
	public $date;
	
	/**
	 * @var string Текст сообщения
	 */
	public $message;
	
	/**
	 * @var integer Код статуса участника чата
	 */
	public $state;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'owner'=>array(self::BELONGS_TO, 'UserModel', 'id_owner'),
			'addressee'=>array(self::BELONGS_TO, 'UserModel', 'id_addressee'),
			'chat'=>array(self::BELONGS_TO, 'ChatModel', 'id_chat'),
			'file'=>array(self::BELONGS_TO, 'FileModel', 'id_file'),
		);
	}
	
}
