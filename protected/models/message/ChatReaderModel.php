<?php
/**
 * Модель данных участника чата пользователей
 * @author v.zubov
 */
class ChatReaderModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Идентификатор чата
	 */
	public $id_chat;
	
	/**
	 * @var integer Идентификатор пользователя-участника чата
	 */
	public $id_user;
	
	/**
	 * @var integer Код статуса участника чата
	 */
	public $state;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'user'=>array(self::BELONGS_TO, 'UserModel', 'id_user'),
			'chat'=>array(self::BELONGS_TO, 'ChatModel', 'id_chat')
		);
	}
	
}
