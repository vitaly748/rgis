<?php
/**
 * Модель данных с информацией о целостности табличной схемы приложения или о параметрах генерации данных БД
 * @author v.zubov
 */
class DataModel extends BaseModel{
	
	/**
	 * @var boolean Признак целостности для группы таблиц "setting"
	 */
	public $setting;
	
	/**
	 * @var integer Код типа установки для группы таблиц "user"
	 */
	public $user;
	
	/**
	 * @var integer Код типа установки для группы таблиц "auth"
	 */
	public $auth;
	
	/**
	 * @var integer Код типа установки для группы таблиц "scheme"
	 */
	public $scheme;
	
	/**
	 * @var integer Код типа установки для группы таблиц "directory"
	 */
	public $directory;
	
	/**
	 * @var integer Код типа установки для группы таблиц "message"
	 */
	public $message;
	
	/**
	 * @var integer Код типа установки для группы таблиц "appeal"
	 */
	public $appeal;
	
	/**
	 * @var integer Код типа установки для группы таблиц "log"
	 */
	public $log;
	
	/**
	 * @var integer Код типа установки для группы таблиц "file"
	 */
	public $file;
	
	/**
	 * @var array Список кодов применяемых способов генерации профилей пользователей
	 */
	public $generation = array();
	
	/**
	 * @var integer Количество генерируемых случайным образом профилей пользователей
	 */
	public $quantity;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('user, auth, scheme, directory, message, appeal, log, file', 'numerical', 'integerOnly'=>true),
		array('generation', 'type', 'type'=>'array'),
		array('quantity', 'numerical', 'integerOnly'=>true, 'min'=>1, 'max'=>50000, 'on'=>'deploy_generation')
	);
	
}
