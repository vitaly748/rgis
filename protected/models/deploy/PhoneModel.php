<?php
/**
 * Модель данных подключения к SMS-шлюзу
 * @author v.zubov
 */
class PhoneModel extends BaseModel{
	
	/**
	 * @var boolean Признак использования подключения
	 */
	public $on;
	
	/**
	 * @var string HTTP-адрес сервера
	 */
	public $host;
	
	/**
	 * @var integer Порт подключения
	 */
	public $port;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('on', 'boolean'),
		array('host', 'host', 'on'=>'on'),
		array('host', 'connect', 'on'=>'on'),
		array('port', 'numerical', 'integerOnly'=>true, 'on'=>'on')
	);
	
	/**
	 * Функция проверки "connect", осуществляет тестовое подключение к SMS-шлюзу с заданными параметрами
	 * @param string $attribute Название атрибута
	 * @param array $params Список параметров проверки
	 */
	public function connect($attribute, $params){
		if (!$this->hasErrors()){
			Yii::app()->phone->set($this->attributes);
			
			if (!($result = Yii::app()->phone->query('test', false, false)) || $result[0]['code'])
				$this->addError($attribute, $params['message']);
		}
	}
	
}
