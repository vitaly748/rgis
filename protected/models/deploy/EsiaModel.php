<?php
/**
 * Модель данных подключения к EСИА
 * @author v.zubov
 */
class EsiaModel extends BaseModel{
	
	/**
	 * @var boolean Признак использования подключения
	 */
	public $on;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('on', 'boolean')
	);
	
}
