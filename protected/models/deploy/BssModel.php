<?php
/**
 * Модель данных подключения к Биллинговому центру
 * @author v.zubov
 */
class BssModel extends BaseModel{
	
	/**
	 * @var boolean Признак использования подключения
	 */
	public $on;
	
	/**
	 * @var string HTTP-адрес сервера
	 */
	public $host;
	
	/**
	 * @var integer Порт подключения
	 */
	public $port;
	
	/**
	 * @var string Название веб-сервиса
	 */
	public $name;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('on', 'boolean'),
		array('host', 'host', 'on'=>'on'),
		array('port', 'numerical', 'integerOnly'=>true, 'on'=>'on'),
		array('name', 'connect', 'on'=>'on')
	);
	
	/**
	 * @var string Название связанного компонента
	 */
	protected $_componentName; 
	
	/**
	 * Переопределение конструктора
	 * @param string $scenario Название сценария. По умолчанию равно false
	 * @param string $component_name Название связанного компонента. По умолчанию равно false. Если равно false, то за название
	 * принимается символьный идентификатор данного класса
	 * @see BaseModel::__construct()
	 */
	public function __construct($scenario = false, $component_name = false){
		parent::__construct($scenario);
		
		$this->_componentName = $component_name ? $component_name : Strings::nameModify(__CLASS__);
	}
	
	/**
	 * Функция проверки "connect", осуществляет тестовое подключение к Биллинговому центру с заданными параметрами
	 * @param string $attribute Название атрибута
	 * @param array $params Список параметров проверки
	 */
	public function connect($attribute, $params){
		if (!$this->hasErrors()){
			Yii::app()->{$this->_componentName}->set($this->attributes);
			
			if (!($result = Yii::app()->{$this->_componentName}->query('test', false, false)) || $result['error']['code'])
				$this->addError($attribute, $params['message']);
		}
	}
	
}
