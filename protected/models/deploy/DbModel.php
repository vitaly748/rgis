<?php
/**
 * Модель данных подключения к БД
 * @author v.zubov
 */
class DbModel extends BaseModel{
	
	/**
	 * @var string HTTP-адрес сервера БД
	 */
	public $host;
	
	/**
	 * @var integer Порт подключения к серверу БД
	 */
	public $port;
	
	/**
	 * @var string Название БД
	 */
	public $name;
	
	/**
	 * @var string Логин
	 */
	public $login;
	
	/**
	 * @var string Пароль
	 */
	public $password;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('host', 'connect'),
		array('port', 'numerical', 'integerOnly'=>true),
		array('name, login, password', 'safe')
	);
	
	/**
	 * Функция проверки "connect", осуществляет тестовое подключение к БД с заданными параметрами
	 * @param string $attribute Название атрибута
	 * @param array $params Список параметров проверки
	 */
	public function connect($attribute, $params){
		if (!$this->hasErrors()){
			$connect = Yii::app()->db->testCfg($this->attributes);
			
			if ($connect !== DbConnection::ERROR_NONE){
				if ($connect === DbConnection::ERROR_HOST)
					$this->addError($attribute, Yii::app()->errorAttribute->get($this->modelName, $attribute, 'host'));
				elseif ($connect === DbConnection::ERROR_DB)
					$this->addError('name', Yii::app()->errorAttribute->get($this->modelName, 'name', 'exists'));
				elseif ($connect === DbConnection::ERROR_AUTH)
					$this->addError('login', Yii::app()->errorAttribute->get($this->modelName, 'login', 'auth'));
				else
					$this->addError($attribute, $params['message']);
			}
		}
	}
	
}
