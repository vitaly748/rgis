<?php
/**
 * Модель данных подключения к ФИАС-агенту
 * @author v.zubov
 */
class FiasModel extends BaseModel{
	
	/**
	 * @var boolean Признак использования подключения
	 */
	public $on;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('on', 'boolean')
	);
	
}
