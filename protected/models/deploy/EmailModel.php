<?php
/**
 * Модель данных подключения к Почтовому серверу
 * @author v.zubov
 */
class EmailModel extends BaseModel{
	
	/**
	 * @var boolean Признак использования подключения
	 */
	public $on;
	
	/**
	 * @var string HTTP-адрес сервера
	 */
	public $host;
	
	/**
	 * @var integer Порт подключения
	 */
	public $port;
	
	/**
	 * @var integer Протокол безопасности
	 */
	public $protocol;
	
	/**
	 * @var string Логин
	 */
	public $login;
	
	/**
	 * @var string Пароль
	 */
	public $password;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('on', 'boolean'),
		array('host', 'host', 'on'=>'on'),
		array('host', 'send', 'on'=>'on'),
		array('port, protocol', 'numerical', 'integerOnly'=>true, 'on'=>'on'),
		array('login, password', 'safe', 'on'=>'on')
	);
	
	/**
	 * Функция проверки "send", осуществляет отправку тестового сообщения на этот же адрес
	 * @param string $attribute Название атрибута
	 * @param array $params Список параметров проверки
	 */
	public function send($attribute, $params){
		if (!$this->hasErrors()){
			$attributes = $this->attributes;
			$attributes['email_protocol'] = Yii::app()->conformity->get('email_protocol', 'name', (int)$attributes['protocol']);
			
			Yii::app()->email->set($attributes);
			$result = Yii::app()->email->send($this->login, 'test', array('text'=>'test'), false);
			
			if ($result !== Email::ERROR_NONE){
				if ($result === Email::ERROR_INIT)
					$this->addError($attribute, Yii::app()->errorAttribute->get($this->modelName, '', 'init'));
				elseif ($result === Email::ERROR_HOST)
					$this->addError($attribute, Yii::app()->errorAttribute->get($this->modelName, $attribute, 'host'));
				elseif ($result === Email::ERROR_AUTH)
					$this->addError('login', Yii::app()->errorAttribute->get($this->modelName, 'login', 'auth'));
				elseif ($result === Email::ERROR_ADDRESS_CORRECT)
					$this->addError('login', Yii::app()->errorAttribute->get($this->modelName, '', 'email'));
				else
					$this->addError($attribute, $params['message']);
			}
		}
	}
	
}
