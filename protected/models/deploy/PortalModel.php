<?php
/**
 * Модель данных подключения к Основному порталу
 * @author v.zubov
 */
class PortalModel extends BaseModel{
	
	/**
	 * @var boolean Признак использования подключения
	 */
	public $on;
	
	/**
	 * @var string HTTP-адрес сервера
	 */
	public $host;
	
	/**
	 * @var integer Порт подключения
	 */
	public $port;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('on', 'boolean'),
		array('host', 'host', 'on'=>'on'),
		array('port', 'numerical', 'integerOnly'=>true, 'on'=>'on'),
	);
	
}
