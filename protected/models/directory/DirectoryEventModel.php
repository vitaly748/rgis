<?php
/**
 * Модель данных справочника событий приложения
 * @author v.zubov
 */
class DirectoryEventModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var string Символьный идентификатор события
	 */
	public $name;
	
	/**
	 * @var string Описание события
	 */
	public $description;
	
	/**
	 * @var integer Код типа фиксации события в логах: "Без фиксации", "Нормальная", "Обязательная"
	 */
	public $fix_log;

	/**
	 * @var integer Код типа фиксации события в флэш-сообщениях
	 */
	public $fix_flash;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'log'=>array(self::HAS_MANY, 'LogModel', 'id_event'),
			'notification_message'=>array(self::HAS_MANY, 'DirectoryNotificationMessageModel', 'id_event')
		);
	}
	
}
