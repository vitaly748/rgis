<?php
/**
 * Модель данных справочника значений
 * @author v.zubov
 */
class DirectoryValueModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Идентификатор родительского элемента
	 */
	public $id_parent;
	
	/**
	 * @var string Название группы значений
	 */
	public $name;
	
	/**
	 * @var string Код значения
	 */
	public $code;
	
	/**
	 * @var integer Описание значения
	 */
	public $description;
	
	/**
	 * @var string Дополнительный параметр 1
	 */
	public $param1;
	
	/**
	 * @var string Дополнительный параметр 2
	 */
	public $param2;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
}
