<?php
/**
 * Модель данных для просмотра информации по субсидиям пользователя
 * @author v.zubov
 */
class SubsidyModel extends BaseModel{
	
	/**
	 * @var array Ассоциативный массив кодов типов льгот
	 */
	const TYPE_LGOT = array(
		'subsidy'=>1,
		'compensation'=>2
	);
	
	public $pers_snils;
	public $lgot_id;
	public $lgot_fias;
	public $lgot_kv;
	public $lgot_kom;
	public $lgot_type_reg;
	public $lgot_type_lgot;
	public $lgot_from;
	public $lgot_to;
	public $lgot_state;
	public $calc_id;
	public $calc_state;
	public $calc_period;
	public $calc_d;
	public $calc_family_size;
	public $calc_fact_ras;
	public $calc_sum_subs;
	public $calc_sum_pere;
	public $calc_sum_final;
	public $dec_id;
	public $dec_n;
	public $dec_d;
	public $dec_type;
	public $dec_osn;
	public $dec_from;
	
	/* public $snils;
	public $address;
	public $base;
	public $period;
	public $datecalculation;
	public $familymembers;
	public $sumexpenses;
	public $sumsubsidy;
	public $sumrecalculation;
	public $sumsubsidyprovided;
	public $state; */
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
		);
	}
	
	/**
	 * Функция поиска
	 * @return CArrayDataProvider Объект-поставщик данных
	 */
	public function search(){
		$sn_data = WebUser::PREFIX_DATA.$this->modelName;
		$data = array();
		
		if (!Yii::app()->request->isAjaxRequest)
			Yii::app()->user->setState($sn_data, null);
		elseif (!Yii::app()->user->hasState($sn_data)){
			$params_fix = array();
			
			try{
				if (!$user = Yii::app()->user->model)
					throw new CHttpException(500, 'user not found');
				
				if (!$type_lgot = Arrays::pop($tl = static::TYPE_LGOT, ($controller_name = Yii::app()->controller->id)))
					throw new CHttpException(500, 'type lgot not defined');
				
				if ($user->snils){
					$rows = Yii::app()->db->createCommand()->
						from('{{subsidy}} t')->
						leftJoin('{{payment}} p', '"p"."lgot_id" = "t"."lgot_id"')->
						leftJoin('{{fias_address}} f', '"f"."id" = "t"."lgot_fias"')->
						where('"t"."pers_snils" = \''.$user->snils.'\' AND "t"."lgot_type_lgot" = \''.$type_lgot."'")->
						queryAll();
					
					if ($rows){
						$id = 1;
						
						foreach ($rows as $row)
							$data[] = $controller_name == 'subsidy' ? array(
								'id'=>$id++,
								'snils'=>$user->snils,
								'address'=>$row['address'].($row['lgot_kv'] ? ', '.$row['lgot_kv'] : ''),
								'base'=>$row['dec_osn'],
								'period'=>$row['calc_period'],
								'datecalculation'=>$row['calc_d'],
								'familymembers'=>$row['calc_family_size'],
								'sumexpenses'=>$row['calc_fact_ras'],
								'sumsubsidy'=>$row['calc_sum_subs'],
								'sumrecalculation'=>$row['calc_sum_pere'],
								'sumsubsidyprovided'=>$row['calc_sum_final'],
								'state'=>$row['calc_state']
							) : array(
								'id'=>$id++,
								'snils'=>$user->snils,
								'address'=>$row['address'].($row['lgot_kv'] ? ', '.$row['lgot_kv'] : ''),
								'base'=>$row['dec_osn'],
								'perioddatestart'=>$row['lgot_from'],
								'perioddateend'=>$row['lgot_to'],
								'datecalculation'=>$row['calc_d'],
								'name'=>'',
								'familymembers'=>$row['calc_family_size'],
								'sumexpenses'=>$row['calc_fact_ras'],
								'sumcompensation'=>$row['calc_sum_subs'],
								'sumrecalculation'=>$row['calc_sum_pere'],
								'sumcompensationprovided'=>$row['calc_sum_final'],
								'state'=>$row['calc_state']
							);
					}
				}
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('subsidy_list', $params_fix, $ok ? 'ok' : 'subsidy_list');
			
			Yii::app()->user->setState($sn_data, $data);
		}else
			$data = Yii::app()->user->getState($sn_data);
		
		return new CArrayDataProvider($data, array(
			'pagination'=>array('pageSize'=>1000),
			'sort'=>array(
				'defaultOrder'=>'account, base',
				'attributes'=>array('account', 'address', 'base', 'familymembers', 'state',
					'period'=>array(
						'asc'=>'period_num',
						'desc'=>'period_num DESC'
					),
					'datecalculation'=>array(
						'asc'=>'datecalculation_num',
						'desc'=>'datecalculation_num DESC'
					),
					'sumexpenses'=>array(
						'asc'=>'sumexpenses_num',
						'desc'=>'sumexpenses_num DESC'
					),
					'sumsubsidy'=>array(
						'asc'=>'sumsubsidy_num',
						'desc'=>'sumsubsidy_num DESC'
					),
					'sumrecalculation'=>array(
						'asc'=>'sumrecalculation_num',
						'desc'=>'sumrecalculation_num DESC'
					),
					'sumsubsidyprovided'=>array(
						'asc'=>'sumsubsidyprovided_num',
						'desc'=>'sumsubsidyprovided_num DESC'
					)
				)
			)
		));
	}
	
}
