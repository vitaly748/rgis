<?php
/**
 * Модель данных справочника групп шаблонов атрибутов
 * @author v.zubov
 */
class DirectoryAttributePatternGroupModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Идентификатор шаблона атрибута
	 */
	public $id_pattern;
	
	/**
	 * @var integer Идентификатор описания группы шаблона атрибута
	 */
	public $id_set;
	
	/**
	 * @var integer Статус группы шаблона атрибута
	 */
	public $state;
	
	/**
	 * @var integer Порядковый номер группы шаблона атрибута
	 */
	public $pos;
	
	/**
	 * @var integer Минимальное количество символов в группе
	 */
	public $length_min;
	
	/**
	 * @var integer Максимальное количество символов в группе
	 */
	public $length_max;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'pattern'=>array(self::BELONGS_TO, 'DirectoryAttributePatternModel', 'id_pattern'),
			'set'=>array(self::BELONGS_TO, 'DirectoryAttributePatternGroupSetModel', 'id_set')
		);
	}
	
}
