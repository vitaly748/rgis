<?php
/**
 * Модель данных справочника описаний групп шаблонов атрибутов
 * @author v.zubov
 */
class DirectoryAttributePatternGroupSetModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var string Описание группы шаблона атрибута
	 */
	public $description;
	
	/**
	 * @var string Набор символов, входящих в группу шаблона атрибута
	 */
	public $set;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'group'=>array(self::HAS_MANY, 'DirectoryAttributePatternGroupModel', 'id_set')
		);
	}
	
}
