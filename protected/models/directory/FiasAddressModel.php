<?php
/**
 * Модель данных для просмотра информации по субсидиям пользователя
 * @author v.zubov
 */
class FiasAddressModel extends BaseModel{
	
	public $id;
	public $address;
	public $id_bss;
	public $id_bars;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
		);
	}
	
}
