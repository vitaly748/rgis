<?php
/**
 * Модель данных справочника сообщений приложения
 * @author v.zubov
 */
class DirectoryLabelModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var string Символьный идентификатор сообщения
	 */
	public $name;
	
	/**
	 * @var string Описание сообщения
	 */
	public $description;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
}
