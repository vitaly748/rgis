<?php
/**
 * Модель данных справочника соответствий
 * @author v.zubov
 */
class DirectoryConformityModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var string Название блока соответствий
	 */
	public $section;
	
	/**
	 * @var integer Цифровой идентификатор соответствия
	 */
	public $code;
	
	/**
	 * @var string Символьный идентификатор соответствия
	 */
	public $name;
	
	/**
	 * @var string Описание соответствия
	 */
	public $description;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
}
