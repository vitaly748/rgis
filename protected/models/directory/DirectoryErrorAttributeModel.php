<?php
/**
 * Модель данных справочника ошибок атрибутов моделей данных
 * @author v.zubov
 */
class DirectoryErrorAttributeModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Идентификатор атрибута модели
	 */
	public $id_attribute;
	
	/**
	 * @var boolean Признак описания валидатора с учётом модели
	 */
	public $model;
	
	/**
	 * @var boolean Признак описания валидатора с учётом атрибута модели
	 */
	public $attribute;
	
	/**
	 * @var string Название проверки
	 */
	public $validator;
	
	/**
	 * @var string Описание ошибки
	 */
	public $message;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'attribute'=>array(self::BELONGS_TO, 'DirectoryAttributeModel', 'id_attribute')
		);
	}
	
}
