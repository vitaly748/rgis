<?php
/**
 * Модель данных справочника атрибутов
 * @author v.zubov
 */
class DirectoryAttributeModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Идентификатор набора фиксированных значений из справочника соответствий
	 */
	public $id_set;
	
	/**
	 * @var string Название модели данных
	 */
	public $model;
	
	/**
	 * @var string Название атрибута модели
	 */
	public $attribute;
	
	/**
	 * @var string Метка атрибута
	 */
	public $label;
	
	/**
	 * @var string Текст подсказки атрибута
	 */
	public $tooltip;
	
	/**
	 * @var integer Статус атрибута
	 */
	public $state;
	
	/**
	 * @var boolean Признак служебного атрибута. Статус служебного атрибута изменить нельзя
	 */
	public $service;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'access_attribute'=>array(self::HAS_MANY, 'AuthAccessAttributeModel', 'id_attribute'),
			'access_attribute_conditional'=>array(self::HAS_MANY, 'AuthAccessAttributeModel', 'id_attribute_conditional'),
			'set'=>array(self::BELONGS_TO, 'DirectoryConformityModel', 'id_set'),
			'pattern'=>array(self::HAS_ONE, 'DirectoryAttributePatternModel', 'id_attribute')
		);
	}
	
}
