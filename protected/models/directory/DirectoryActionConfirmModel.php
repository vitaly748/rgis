<?php
/**
 * Модель данных справочника типов подтверждений действий над моделями данных
 * @author v.zubov
 */
class DirectoryActionConfirmModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Идентификатор первого атрибута модели
	 */
	public $id_attribute;
	
	/**
	 * @var integer Цифровой идентификатор действия над моделью из справочника соответствий
	 */
	public $action;
	
	/**
	 * @var integer Цифровой идентификатор типа подтверждения действия над моделью
	 */
	public $type;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'attribute'=>array(self::BELONGS_TO, 'DirectoryAttributeModel', 'id_attribute')
		);
	}
	
}
