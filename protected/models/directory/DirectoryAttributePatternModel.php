<?php
/**
 * Модель данных справочника шаблонов атрибутов
 * @author v.zubov
 */
class DirectoryAttributePatternModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Идентификатор атрибута
	 */
	public $id_attribute;
	
	/**
	 * @var integer Минимальная длина значения
	 */
	public $length_min;
	
	/**
	 * @var integer Максимальная длина значения
	 */
	public $length_max;
	
	/**
	 * @var integer Минимальное количество групп в шаблоне
	 */
	public $groups_min;
	
	/**
	 * @var string Количественное группирование символов значения атрибута
	 */
	public $groups_separation;
	
	/**
	 * @var boolean Признак обратного направления расчёта количественного группирования (справа налево)
	 */
	public $separation_reverse;
	
	/**
	 * @var boolean Признак автоматического включения развёрнутой информации о шаблоне в текст подсказки атрибута
	 */
	public $tooltip_inclusion;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'attribute'=>array(self::BELONGS_TO, 'DirectoryAttributeModel', 'id_attribute'),
			'group'=>array(self::HAS_MANY, 'DirectoryAttributePatternGroupModel', 'id_pattern')
		);
	}
	
}
