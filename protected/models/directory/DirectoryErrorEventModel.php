<?php
/**
 * Модель данных справочника ошибок событий приложения
 * @author v.zubov
 */
class DirectoryErrorEventModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var string Символьный идентификатор ошибки
	 */
	public $name;
	
	/**
	 * @var string Описание ошибки
	 */
	public $description;
	
	/**
	 * @var integer Код серверной ошибки
	 */
	public $exception;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'log'=>array(self::HAS_MANY, 'LogModel', 'id_error'),
			'notification_message'=>array(self::HAS_MANY, 'DirectoryNotificationMessageModel', 'id_error')
		);
	}
	
}
