<?php
/**
 * Модель данных справочника текстов уведомлений
 * @author v.zubov
 */
class DirectoryNotificationMessageModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var string Идентификатор события
	 */
	public $id_event;
	
	/**
	 * @var string Идентификатор ошибки
	 */
	public $id_error;
	
	/**
	 * @var string Текст уведомления
	 */
	public $message;
	
	/**
	 * @var integer Код статуса уведомления
	 */
	public $state;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'event'=>array(self::BELONGS_TO, 'DirectoryEventModel', 'id_event'),
			'error'=>array(self::BELONGS_TO, 'DirectoryErrorEventModel', 'id_error'),
			'notification'=>array(self::HAS_MANY, 'NotificationModel', 'id_message')
		);
	}
	
}
