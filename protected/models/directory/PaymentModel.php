<?php
/**
 * Модель данных для просмотра информации по выплатам по субсидии
 * @author v.zubov
 */
class PaymentModel extends BaseModel{
	
	public $lgot_id;
	public $pay_id;
	public $pay_type;
	public $pay_date;
	public $pay_sum;
	public $pay_state;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Функция поиска
	 * @return CArrayDataProvider Объект-поставщик данных
	 */
	public function search(){
		$sn_data = WebUser::PREFIX_DATA.$this->modelName;
		$data = array();
		
		if (!Yii::app()->request->isAjaxRequest)
			Yii::app()->user->setState($sn_data, null);
		elseif (!Yii::app()->user->hasState($sn_data)){
			$params_fix = array();
			
			try{
				if (!$user = Yii::app()->user->model)
					throw new CHttpException(500, 'user not found');
				
				if (!$type_lgot = Arrays::pop($tl = SubsidyModel::TYPE_LGOT, ($controller_name = Yii::app()->controller->id)))
					throw new CHttpException(500, 'type lgot not defined');
				
				if ($user->snils){
					$rows = Yii::app()->db->createCommand()->
						from('{{subsidy}} t')->
						leftJoin('{{payment}} p', '"p"."lgot_id" = "t"."lgot_id"')->
						where("pers_snils = '{$user->snils}'")->
						where('"t"."pers_snils" = \''.$user->snils.'\' AND "t"."lgot_type_lgot" = \''.$type_lgot."'")->
						queryAll();
					
					if ($rows){
						$id = 1;
						
						foreach ($rows as $row)
							$data[] = array(
								'id'=>$id++,
								'snils'=>$user->snils,
								'base'=>$row['dec_osn'],
								'type'=>$row['pay_type'],
								'date'=>$row['pay_date'],
								'sum'=>$row['pay_sum'],
								'state'=>$row['pay_state']
							);
					}
				}
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('subsidy_payment_list', $params_fix, $ok ? 'ok' : 'subsidy_payment_list');
			
			Yii::app()->user->setState($sn_data, $data);
		}else
			$data = Yii::app()->user->getState($sn_data);
		
		return new CArrayDataProvider($data, array(
			'pagination'=>array('pageSize'=>isset($_POST['filter-size']) ? $_POST['filter-size'] : static::FILTER_SIZE_DEF),
			'sort'=>array('defaultOrder'=>'date_num', 'attributes'=>array('base', 'type', 'state',
				'date'=>array(
					'asc'=>'date_num',
					'desc'=>'date_num DESC'
				),
				'sum'=>array(
					'asc'=>'sum_num',
					'desc'=>'sum_num DESC'
				)
			))
		));
	}
	
}
