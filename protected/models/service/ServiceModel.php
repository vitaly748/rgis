<?php
/**
 * Модель данных для просмотра информации по услугам, привязанным к лицевому счёту
 * @author v.zubov
 */
class ServiceModel extends BaseModel{
	
	/**
	 * @var string Название услуги
	 */
	public $name;
	
	/**
	 * @var float Тариф
	 */
	public $cost;
	
	/**
	 * Функция поиска
	 * @return CArrayDataProvider Объект-поставщик данных
	 */
	public function search(){
		$sn_data = WebUser::PREFIX_DATA.$this->modelName;
		$sn_filter = WebUser::PREFIX_FILTER.$this->modelName;
		$sn_set = WebUser::PREFIX_SET.'id_accounts';
		$data = array();
		
		if (!Yii::app()->request->isAjaxRequest)
			Yii::app()->user->setState($sn_data, null);	
		else{
			$filter_account = Arrays::pop($_POST, 'filter-service-account');
			
			if (!Yii::app()->user->hasState($sn_data) || Yii::app()->user->getState($sn_filter) != $filter_account){
				$params_fix = array('filter'=>array('filter_account'=>$filter_account));
				
				try{
					if (!Yii::app()->user->hasState($sn_set))
						throw new CHttpException(500, 'not set id_accounts');
					
					$id_accounts = $params_fix['id_accounts'] = Yii::app()->user->getState($sn_set);
					
					if (!$id_accounts)
						throw new CHttpException(500, 'empty id_accounts');
					
					if (!isset($id_accounts[$filter_account]))
						$filter_account = $params_fix['filter_account_set'] = reset(array_keys($id_accounts));
					
					$services = $params_fix['services'] = Arrays::pop($id_accounts[$filter_account], 'services');
					
					foreach ($services as $service_info){
						list($id, $name) = Arrays::pop($service_info, 'id, name');
						$cost_num = (float)(($costs = Arrays::pop($service_info, 'costs')) ? Arrays::pop(end($costs), 'cost') : 0);
						
						$data[] = array(
							'id'=>$id,
							'name'=>$name,
							'cost_num'=>$cost_num,
							'cost'=>Numbers::format($cost_num)
						);
					}
				}catch (CException $e){
					$params_fix['msg'] = $e->getMessage();
				}
				
				$ok = !isset($e);
				
				Yii::app()->log->fix('account_service_list_show', $params_fix, $ok ? 'ok' :
					(empty($id_accounts) ? 'application' : 'account_service_list_show'));
				
				if ($ok){
					Yii::app()->user->setState($sn_data, $data);
					Yii::app()->user->setState($sn_filter, $filter_account);
				}
			}else
				$data = Yii::app()->user->getState($sn_data);
		}
		
		return new CArrayDataProvider($data, array(
			'pagination'=>array('pageSize'=>1000),
			'sort'=>array('defaultOrder'=>'name', 'attributes'=>array(
				'name',
				'cost'=>array(
					'asc'=>'cost_num',
					'desc'=>'cost_num DESC'
				)
			))
		));
	}
	
}
