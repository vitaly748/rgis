<?php
/**
 * Модель данных для просмотра истории изменения тарифа по услуге
 * @author v.zubov
 */
class ServiceHistoryModel extends BaseModel{
	
	/**
	 * @var string Дата ввода
	 */
	public $date;
	
	/**
	 * @var float Показание
	 */
	public $cost;
	
	/**
	 * Функция поиска
	 * @return CArrayDataProvider Объект-поставщик данных
	 */
	public function search(){
		$sn_data = WebUser::PREFIX_DATA.$this->modelName;
		$sn_filter = WebUser::PREFIX_FILTER.$this->modelName;
		$sn_set = WebUser::PREFIX_SET.'id_services';
		$data = array();
		
		if (!Yii::app()->request->isAjaxRequest)
			Yii::app()->user->setState($sn_data, null);	
		else{
			$filter_service = Arrays::pop($_POST, 'filter-history-service');
			
			if (!Yii::app()->user->hasState($sn_data) || Yii::app()->user->getState($sn_filter) != $filter_service){
				$params_fix = array('filter'=>array('filter_service'=>$filter_service));
				
				try{
					if (!Yii::app()->user->hasState($sn_set))
						throw new CHttpException(500, 'not set id_services');
					
					$id_services = $params_fix['id_services'] = Yii::app()->user->getState($sn_set);
					
					if (!$id_services)
						throw new CHttpException(500, 'empty id_services');
					
					if (!isset($id_services[$filter_service]))
						$filter_service = $params_fix['filter_service_set'] = reset(array_keys($id_services));
					
					$costs = $params_fix['costs'] = Arrays::pop($id_services[$filter_service], 'costs');
					$id = 1;
					
					foreach ($costs as $cost_info){
						list($date, $cost) = Arrays::pop($cost_info, 'date, cost');
						
						$data[] = array(
							'id'=>$id++,
							'date_num'=>$date_num = mb_substr($date, 0, 10),
							'date'=>Swamper::date($date_num, Swamper::FORMAT_DATE_USER_WITHOUT_TIME),
							'cost_num'=>$cost_num = (float)$cost,
							'cost'=>Numbers::format($cost_num)
						);
					}
				}catch (CException $e){
					$params_fix['msg'] = $e->getMessage();
				}
				
				$ok = !isset($e);
				
				Yii::app()->log->fix('service_history_list_show', $params_fix, $ok ? 'ok' :
					(empty($id_services) ? 'application' : 'service_history_list_show'));
				
				if ($ok){
					Yii::app()->user->setState($sn_data, $data);
					Yii::app()->user->setState($sn_filter, $filter_service);
				}
			}else
				$data = Yii::app()->user->getState($sn_data);
		}
		
		return new CArrayDataProvider($data, array(
			'pagination'=>array('pageSize'=>1000),
			'sort'=>array('defaultOrder'=>'name', 'attributes'=>array(
				'date'=>array(
					'asc'=>'date_num',
					'desc'=>'date_num DESC'
				),
				'cost'=>array(
					'asc'=>'cost_num',
					'desc'=>'cost_num DESC'
				)
			))
		));
	}
	
}
