<?php
/**
 * Модель данных для просмотра общей информации по домам
 * @author v.zubov
 */
class HouseGeneralModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор дома
	 */
	public $id;
	
	/**
	 * @var string Адрес дома, связанного с лицевым счётом
	 */
	public $address;
	
	/**
	 * @var float Начислено по дому
	 */
	public $credited;
	
	/**
	 * @var float Оплачено по дому
	 */
	public $paid;
	
	/**
	 * @var float Процент собираемости
	 */
	public $collectibility;
	
	/**
	 * Функция поиска
	 * @return CArrayDataProvider Объект-поставщик данных
	 */
	public function search(){
		$sn_data = WebUser::PREFIX_DATA.$this->modelName;
		$sn_filter = WebUser::PREFIX_FILTER.$this->modelName;
		$data = array();
		
		if (!Yii::app()->request->isAjaxRequest)
			Yii::app()->user->setState($sn_data, null);	
		else{
			$filter_address = $filter_key = Arrays::pop($_POST, 'filter-addressid');
			
			if (!Yii::app()->user->hasState($sn_data) || Yii::app()->user->getState($sn_filter) != $filter_key){
				$params_fix = array('filter'=>array('filter_address'=>$filter_address), 'houses'=>array());
				
				try{
					if ($filter_address){
						$row = Yii::app()->db->createCommand()->
							select(array('address', 'id_bss'))->
							from('{{fias_address}}')->
							where("id = '$filter_address'")->
							queryRow();
						
						if ($row)
							$params_fix['houses'][$row['id_bss']] = $row['address'];
					}else{
						$accounts = Yii::app()->user->accounts;
						$roles_external = WebUser::ROLES_EXTERNAL;
						
						foreach (array('bss2') as $component)
							if ($roles_bss = Arrays::pop($roles_external, $component))
								foreach ($roles_bss as $role_bss)
									if ($accounts_role = Arrays::pop($accounts, $role_bss))
										foreach ($accounts_role as $account)
											if (($id = Arrays::pop($account, 'id_house')) && !isset($params_fix['houses'][$id]))
												$params_fix['houses'][$id] = Swamper::address($account);
					}
					
					foreach ($params_fix['houses'] as $id_house=>$address){
						$result = Yii::app()->bss2->query('accrual_house', array('id'=>$id_house));
						
						if (!$result)
							throw new CHttpException(500, 'empty result');
						
						$params_fix['requests'][$id_house] = $result;
						$data[] = array(
							'id'=>$id_house,
							'address'=>$address,
							'credited_num'=>$credited_num = (float)Arrays::pop($result, 'credited'),
							'credited'=>Numbers::format($credited_num),
							'paid_num'=>$paid_num = (float)Arrays::pop($result, 'paid'),
							'paid'=>Numbers::format($paid_num),
							'collectibility_num'=>$collectibility_num = $credited_num ? 100 * $paid_num / $credited_num : 100, 
							'collectibility'=>Numbers::format($collectibility_num, 2, Swamper::UNIT_PERCENT) 
						);
					}
				}catch (CException $e){
					$params_fix['msg'] = $e->getMessage();
				}
				
				$ok = !isset($e);
				
				Yii::app()->log->fix('house_general_list', $params_fix, $ok ? 'ok' :
					($params_fix['houses'] && empty($result) ? 'agent_request' : 'house_general_list'));
				
				if ($ok){
					Yii::app()->user->setState($sn_data, $data);
					Yii::app()->user->setState($sn_filter, $filter_key);
				}
			}else
				$data = Yii::app()->user->getState($sn_data);
		}
		
		return new CArrayDataProvider($data, array(
			'pagination'=>array('pageSize'=>10000),
			'sort'=>array(
				'attributes'=>array(
					'address',
					'credited'=>array(
						'asc'=>'credited_num',
						'desc'=>'credited_num DESC'
					),
					'paid'=>array(
						'asc'=>'paid_num',
						'desc'=>'paid_num DESC'
					),
					'collectibility'=>array(
						'asc'=>'collectibility_num',
						'desc'=>'collectibility_num DESC'
					)
				)
			)
		));
	}
	
}
