<?php
/**
 * Модель данных для просмотра информации по дому в разрезе помещений
 * @author v.zubov
 */
class HousePremiseModel extends BaseModel{
	
	/**
	 * @var string Название помещения
	 */
	public $premise;
	
	/**
	 * @var float Площадь помещения
	 */
	public $area;
	
	/**
	 * @var float Начислено по помещению
	 */
	public $credited;
	
	/**
	 * @var float Оплачено по помещению
	 */
	public $paid;
	
	/**
	 * @var float Процент собираемости
	 */
	public $collectibility;
	
	/**
	 * Функция поиска
	 * @return CArrayDataProvider Объект-поставщик данных
	 */
	public function search(){
		$sn_data = WebUser::PREFIX_DATA.$this->modelName;
		$sn_filter = WebUser::PREFIX_FILTER.$this->modelName;
		$sn_set = WebUser::PREFIX_SET.'id_houses';
		$data = array();
		
		if (!Yii::app()->request->isAjaxRequest)
			Yii::app()->user->setState($sn_data, null);	
		else{
			$filter_house = Arrays::pop($_POST, 'filter-premise-house');
			
			if (!Yii::app()->user->hasState($sn_data) || Yii::app()->user->getState($sn_filter) != $filter_house){
				$params_fix = array('filter'=>array('filter_house'=>$filter_house));
				
				try{
					if (!Yii::app()->user->hasState($sn_set))
						throw new CHttpException(500, 'not set id_houses');
					
					$id_houses = $params_fix['id_houses'] = Yii::app()->user->getState($sn_set);
					
					if ($id_houses){
						if (!isset($id_houses[$filter_house]))
							$filter_house = $params_fix['filter_house_set'] = reset(array_keys($id_houses));
						
						$component = Arrays::pop($id_houses[$filter_house], 'component');
						
						if (!$component)
							throw new CHttpException(500, 'filter_house incorrect');
						
						if (!$result = Yii::app()->$component->query('accrual_premise', array('id'=>$filter_house)))
							throw new CHttpException(500, 'empty result');
						
						$params_fix['result'] = $result;
						$id = 1;
						
						foreach ($result['premises'] as $premise_info){
							list($premise, $area, $credited, $paid) = Arrays::pop($premise_info, 'premise, area, credited, paid');
							$premise_num = 0;
							
							if (preg_match_all('/[\d]+/u', $premise, $matches) && isset($matches[0][0]))
								$premise_num = (int)$matches[0][0];
							
							$data[] = array(
								'id'=>$id++,
								'premise_num'=>$premise_num,
								'premise'=>$premise,
								'area'=>$area ? Swamper::unit($area, Swamper::UNIT_METER, 2, false, '2') : '',
								'credited_num'=>$credited_num = (float)$credited,
								'credited'=>Numbers::format($credited_num),
								'paid_num'=>$paid_num = (float)$paid,
								'paid'=>Numbers::format($paid_num),
								'collectibility_num'=>$collectibility_num = $credited_num ? 100 * $paid_num / $credited_num : 100,
								'collectibility'=>Numbers::format($collectibility_num, 2, Swamper::UNIT_PERCENT)
							);
						}
					}
				}catch (CException $e){
					$params_fix['msg'] = $e->getMessage();
				}
				
				$ok = !isset($e);
				
				Yii::app()->log->fix('house_premise_list', $params_fix, $ok ? 'ok' :
					(!empty($id_houses) && empty($result) ? 'agent_request' : 'house_premise_list'));
				
				if ($ok){
					Yii::app()->user->setState($sn_data, $data);
					Yii::app()->user->setState($sn_filter, $filter_house);
				}
			}else
				$data = Yii::app()->user->getState($sn_data);
		}
		
		return new CArrayDataProvider($data, array(
			'pagination'=>array('pageSize'=>isset($_POST['filter-size']) ? $_POST['filter-size'] : static::FILTER_SIZE_DEF),
			'sort'=>array('defaultOrder'=>false, 'attributes'=>array('area',
				'premise'=>array(
					'asc'=>'premise_num, premise',
					'desc'=>'premise_num DESC, premise DESC'
				),
				'credited'=>array(
					'asc'=>'credited_num',
					'desc'=>'credited_num DESC'
				),
				'paid'=>array(
					'asc'=>'paid_num',
					'desc'=>'paid_num DESC'
				),
				'collectibility'=>array(
					'asc'=>'collectibility_num',
					'desc'=>'collectibility_num DESC'
				)
			))
		));
	}
	
}
