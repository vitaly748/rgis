<?php
/**
 * Модель данных общих настроек приложения
 * @author v.zubov
 */
class SettingGeneralModel extends BaseModel{
	
	/**
	 * @var array Список допустимых действий над данной моделью
	 */
	const ACTIONS = array('update', 'deploy');
	
	/**
	 * @var integer Код статуса приложения
	 */
	public $state;
	
	/**
	 * @var string Название приложения
	 */
	public $name;
	
	/**
	 * @var integer Код типа регистрации пользователей
	 */
	public $registration;
	
	/**
	 * @var integer Количество попыток ввода кодов доступа
	 */
	public $retries;
	
	/**
	 * @var integer Время действия кодов доступа в секундах
	 */
	public $timeout;
	
	/**
	 * @var boolean Признак сохранения логов в БД
	 */
	public $logdb;
	
	/**
	 * @var boolean Признак сохранения логов в файлах
	 */
	public $logfile;
	
	/**
	 * @var integer Код типа кэширования
	 */
	public $cache;
	
	/**
	 * @var integer Вреия хранения данных в кэше в секундах по схеме "Долго"
	 */
	public $duration1;
	
	/**
	 * @var integer Вреия хранения данных в кэше в секундах по схеме "Нормально"
	 */
	public $duration2;
	
	/**
	 * @var integer Вреия хранения данных в кэше в секундах по схеме "Быстро"
	 */
	public $duration3;
	
	/**
	 * @var integer Вреия хранения данных в кэше в секундах по схеме "Быстро"
	 */
	public $duration4;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('timeout, duration1, duration2, duration3, duration4', 'numerical', 'integerOnly'=>true,
			'min'=>0, 'max'=>356 * 24 * 60 * 60),// 31 536 000
		array('state, registration, cache', 'numerical', 'integerOnly'=>true),
		array('logdb, logfile', 'boolean'),
		array('name', 'swearword'),
		array('retries', 'numerical', 'integerOnly'=>true, 'min'=>0, 'max'=>10)
	);
	
}
