<?php
/**
 * Модель данных настроек приложения
 * @author v.zubov
 */
class SettingModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var string Название блока параметров
	 */
	public $section;
	
	/**
	 * @var string Название параметра
	 */
	public $name;
	
	/**
	 * @var string Значение параметра
	 */
	public $value;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
}
