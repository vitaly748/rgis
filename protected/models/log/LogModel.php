<?php
/**
 * Модель данных ведения логов событий
 * @author v.zubov
 */
class LogModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Идентификатор пользователя, вызвавшего событие
	 */
	public $id_user;
	
	/**
	 * @var integer Идентификатор события
	 */
	public $id_event;
	
	/**
	 * @var integer Идентификатор ошибки
	 */
	public $id_error;
	
	/**
	 * @var string Дата и время наступления события
	 */
	public $date;
	
	/**
	 * @var string Параметры события
	 */
	public $params;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'event'=>array(self::BELONGS_TO, 'DirectoryEventModel', 'id_event'),
			'error'=>array(self::BELONGS_TO, 'DirectoryErrorEventModel', 'id_error')
		);
	}
	
	/**
	 * Функция поиска
	 * @return CommandDataProvider Объект-поставщик данных
	 */
	public function search(){
		$params_provider = array(
			'pagination'=>array('pageSize'=>isset($_POST['filter-size']) ? $_POST['filter-size'] : static::FILTER_SIZE_DEF),
			'sort'=>array(
				'defaultOrder'=>'date_num DESC, id DESC',
				'attributes'=>array(
					'date'=>array('asc'=>'date_num, id', 'desc'=>'date_num DESC, id DESC'),
					'id_user',
					'id_event',
					'id_error'
				)
			)
		);
		
		if (!Yii::app()->request->isAjaxRequest)
			$data = new CommandDataProvider($this, $params_provider);
		else{
			$params_fix = Arrays::pop($_POST, 'filter-log-user, filter-log-event, filter-log-error', false, true);
			list($filter_user, $filter_event, $filter_error) = array_values($params_fix);
			$controller = Yii::app()->controller;
			
			try{
				$roles = Yii::app()->user->getRoles(0, 'description', 'name', false, 'citizen, organization');
				$where = array();
				$select_t3_count = $select_t_count = array();
				
				$select_initiator =
					'(CASE '.
						'WHEN "t3"."id_user" > 0 AND "t2"."user" IS NOT NULL THEN "t2"."user" '.
						'WHEN "t3"."id_user" = 0 THEN \''.Yii::app()->label->get('user_system')."' ".
						'WHEN "t3"."id_user" IS NULL THEN \''.Yii::app()->authPhp->roles['guest']->description."' ".
						"ELSE '".Yii::app()->label->get('user_stranger')."' ".
					'END) AS "id_user"';
				
				$select_user = Yii::app()->db->createCommand()->
					select(array(
						't1.id',
						'(CASE '.
							'WHEN "t1"."roles_desc" LIKE \'%'.$roles['citizen'].'%\' THEN '.
								'CONCAT("t1"."surname", \' \', "t1"."name", \' \', "t1"."patronymic") '.
							'WHEN "t1"."roles_desc" LIKE \'%'.$roles['organization'].'%\' THEN "t1"."title" '.
							'ELSE SUBSTRING("t1"."roles_desc" FROM \'^\w*\') '.
						'END) AS "user"'
					))->
					from('('.
						Yii::app()->db->createCommand()->
							select(array(
								'u.id',
								'u.surname',
								'u.name',
								'u.patronymic',
								'u.title',
								'STRING_AGG("ai"."description", \',\') AS "roles_desc"'
							))->
							from('{{user}} u')->
							leftJoin('{{auth_assignment}} aa', '"aa"."userid" = "u"."id"')->
							leftJoin('{{auth_item}} ai', '"ai"."name" = "aa"."itemname"')->
							group('u.id')->
							text.
					') "t1"')->
					text;
				
				$select_t3 = array(
					'l.id',
					'l.id_user',
					'l.id_event id_event_num',
					'e.description id_event',
					'l.id_error id_error_num',
					'ee.description id_error',
					'l.date AS date_num',
					'TO_CHAR("l"."date", \'DD.MM.YYYY HH24:MI:SS\') AS "date"'
				);
				
				$select_t = array(
					't3.id', 
					$select_initiator,
					't3.id_event_num',
					't3.id_event',
					't3.id_error_num',
					't3.id_error',
					't3.date_num',
					't3.date'
				);
				
				if ($filter_user){
					preg_match_all('/[A-Za-zА-яЁё\d.]+/u', $filter_user, $matches);
					
					if ($filter_user = !empty($matches[0])){
						$where[] = 'LOWER("id_user") '.(count($matches[0]) == 1 ? "LIKE '%".mb_strtolower(reset($matches[0]))."%'" :
							"SIMILAR TO '%(".mb_strtolower(implode('|', $matches[0])).")%'");
						$select_t3_count[] = 'l.id_user';
						$select_t_count[] = $select_initiator;
					}
				}
				
				if ($filter_event){
					$where[] = '"id_event_num" IN ('.$filter_event.')';
					$select_t3_count[] = 'l.id_event id_event_num';
					$select_t_count[] = 't3.id_event_num';
				}
				
				if ($filter_error){
					$where[] = '"id_error_num" IN ('.$filter_error.')';
					$select_t3_count[] = 'l.id_error id_error_num';
					$select_t_count[] = 't3.id_error_num';
				}
				
				$where = !$where ? false : (count($where) == 1 ? reset($where) : array_merge(array('and'), $where));
				
				$params_provider['criteria'] = Yii::app()->db->createCommand()->
					from('('.
						Yii::app()->db->createCommand()->
							select($select_t)->
							from('('.
								Yii::app()->db->createCommand()->
									select($select_t3)->
									from('{{log}} l')->
										leftJoin('{{directory_event}} e', '"e"."id" = "l"."id_event"')->
										leftJoin('{{directory_error_event}} ee', '"ee"."id" = "l"."id_error"')->
									text.
							') "t3"')->
							leftJoin('('.$select_user.') "t2"', '"t2"."id" = "t3"."id_user"')->
							text.
					') "t"')->
					where($where);
				
				$params_provider['countCriteria'] = $filter_user ?
					Yii::app()->db->createCommand()->
						from('('.
							Yii::app()->db->createCommand()->
								select($select_t_count)->
								from('('.
									Yii::app()->db->createCommand()->
										select($select_t3_count)->
										from('{{log}} l')->
										text.
								') "t3"')->
								leftJoin('('.$select_user.') "t2"', '"t2"."id" = "t3"."id_user"')->
								text.
						') "t"')->
						where($where)
					: ($filter_event || $filter_error ?
						Yii::app()->db->createCommand()->
							from('('.
								Yii::app()->db->createCommand()->
									select($select_t3_count)->
									from('{{log}} l')->
									text.
							') "t"')->
							where($where)
						:
						Yii::app()->db->createCommand()->from('{{log}}'));
				
				$data = new CommandDataProvider($this, $params_provider);
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('log_list', $params_fix, $ok ? 'ok' : 'log_list');
		}
		
		return $data;
	}
	
}
