<?php
/**
 * Модель данных пользователей
 * @author v.zubov
 */
class UserModel extends BaseModel{
	
	/**
	 * @var array Список допустимых действий над данной моделью
	 */
	const ACTIONS = array('list', 'create', 'delete', 'view', 'update', 'restore');
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var array Список цифровых идентификаторов ролей данного пользователя. По умолчанию равно array()
	 */
	public $roles = array();
	
	/**
	 * @var string Код доступа абонента к лицевым счетам
	 */
	public $bsscode;
	
	/**
	 * @var integer Код статуса пользователя
	 */
	public $state;
	
	/**
	 * @var string Дата регистрации пользователя
	 */
	public $registration;
	
	/**
	 * @var string Дата блокировки пользователя
	 */
	public $locked;
	
	/**
	 * @var string Дата удаления пользователя
	 */
	public $delete;
	
	/**
	 * @var string Логин
	 */
	public $login;
	
	/**
	 * @var string Пароль
	 */
	public $password;
	
	/**
	 * @var string Повтор пароля
	 */
	public $password2;
	
	/**
	 * @var string Фамилия пользователя
	 */
	public $surname;
	
	/**
	 * @var string Имя пользователя
	 */
	public $name;
	
	/**
	 * @var string Отчество пользователя
	 */
	public $patronymic;
	
	/**
	 * @var string Название организации
	 */
	public $title;
	
	/**
	 * @var string Полномочия организации
	 */
	public $powers;
	
	/**
	 * @var string Адрес пользователя
	 */
	public $address;
	
	/**
	 * @var string Идентификатор адреса
	 */
	public $addressid;
	
	/**
	 * @var string E-mail адрес пользователя
	 */
	public $email;
	
	/**
	 * @var string Телефон пользователя
	 */
	public $phone;
	
	/**
	 * @var string Телефон пользователя
	 */
	public $snils;
	
	/**
	 * @var integer Цифровой идентификатор способа доставки уведомлений пользователю
	 */
	public $delivery;
	
	/**
	 * @var boolean Признак согласия пользователя с условиями пользовательского соглашения
	 */
	public $agreement;
	
	/**
	 * @var boolean Признак желания пользователя работать по сертификату повышенной безопасности
	 */
	public $secure;
	
	/**
	 * @var string Логин или E-mail пользователя, затребовавшего восстановление пароля
	 */
	public $restore;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('bsscode, login, addressid', 'safe'),
		array('name, surname, patronymic, title', 'swearword'),
		array('roles', 'type', 'type'=>'array'),
		array('state', 'numerical', 'integerOnly'=>true),
		array('locked', 'date'),
		array('login, email, phone', 'unique', 'caseSensitive'=>false, 'on'=>BaseModel::LIMITER_SCENARIO),
		array('login', 'unique', 'caseSensitive'=>false, 'attributeName'=>'email', 'on'=>BaseModel::LIMITER_SCENARIO),
		array('password', 'difficulty'),
		array('password2', 'compare', 'compareAttribute'=>'password'),
		array('passwordcode', 'password'),
		array('address', 'fias'),
		array('email', 'email'),
		array('phone', 'phone'),
		array('agreement, secure', 'boolean'),
		array('restore', 'checkRestore')
	);
	
	/**
	 * @var array Ассоциативный массив с информацией о сопряжениях пользователя с внешними системами в формате
	 *  array(
	 * 		{Символьный идентификатор роли, связанной с внешней системой}=>array(
	 * 			{Внешний  идентификатор пользователя}=>{Объект сопряжения UserConjugationModel},
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	private $_conjugations;
	
	/**
	 * @var boolean Признак рабочего статуса данного пользователя
	 */
	private $_working;
	
	/**
	 * @var string Зашифрованный хэш пароля
	 */
	private $_passwordHash;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'conjugation'=>array(self::HAS_MANY, 'UserConjugationModel', 'id_owner', 'order'=>'id_role, id_external'),
			'code'=>array(self::HAS_MANY, 'UserCodeModel', 'id_owner'),
			'log'=>array(self::HAS_MANY, 'LogModel', 'id_user'),
			'chat'=>array(self::HAS_MANY, 'ChatModel', 'id_owner'),
			'chat_reader'=>array(self::HAS_MANY, 'ChatReaderModel', 'id_user'),
			'message'=>array(self::HAS_MANY, 'ChatMessageModel', 'id_owner'),
			'message_addressee'=>array(self::HAS_MANY, 'ChatMessageModel', 'id_addressee'),
			'notification'=>array(self::HAS_MANY, 'NotificationModel', 'id_addressee'),
			'auth_assignment'=>array(self::HAS_MANY, 'AuthAssignmentModel', 'userid'),
			'auth_item'=>array(self::MANY_MANY, 'AuthItemModel', '{{auth_assignment}}(userid, itemname)')
		);
	}
	
	/**
	 * Функция проверки "checkRestore", осуществляет проверку введённого логина или e-mail при восстановлении доступа
	 * @param string $attribute Название атрибута
	 * @param array $params Список параметров проверки
	 */
	public function checkRestore($attribute, $params){
		if (!$this->hasErrors())
			if ($model = UserModel::model()->find('LOWER(login) = :restore OR LOWER(email) = :restore',
				array(':restore'=>mb_strtolower($this->$attribute))))
					$this->setAttributes($model->attributes, false);
				else
					$this->addError($attribute, $params['message']);
	}
	
	/**
	 * Получение ограничителей статусов атрибутов для текущего действия модели
	 * @return array Ассоциативный массив в формате {Название атрибута}=>{Символьный идентификатор максимального статуса атрибута}
	 */
	public function limiterStates(){
		if ($this->action != 'restore'){
			$create = $this->action === 'create';
			$registration = $create && Yii::app()->controller->action->id === 'registration';
			$id_roles = Yii::app()->user->getRoles(0, 'id', 'name', false, array('citizen', 'organization', 'esia'));
			$esia = $this->id && in_array($id_roles['esia'], $this->roles);
			
			if ($this->post){
				$state_locked = Yii::app()->conformity->get('user_state', 'code', 'locked');
				
				if (empty($this->post['roles']))
					$citizen = $organization = false;
				else{
					$citizen = in_array($id_roles['citizen'], $this->post['roles']);
					$organization = in_array($id_roles['organization'], $this->post['roles']);
				}
			}
			
			return array(
				'bsscode'=>$create ? 'update' : 'disabled',
				'state'=>$registration ? 'disabled' : 'critical',
				'registration'=>$create ? 'disabled' : 'view',
				'locked'=>$registration ? 'disabled' :
					(!$this->post || (isset($this->post['state']) && (int)$this->post['state'] === $state_locked) ? 'critical' : 'view'),
				'delete'=>$create ? 'disabled' : 'view',
				'login'=>!$esia ? 'critical' : 'update',
				'password'=>$create || (!$this->_passwordHash && !$esia) ? 'critical' :
					(!$this->post || !empty($this->post['password']) ? 'update' : 'disabled'),
				'password2'=>$create || (!$this->_passwordHash && !$esia) ? 'critical' :
					(!$this->post || !empty($this->post['password']) ? 'critical' : 'disabled'),
				'passwordcode'=>$create || !$this->_passwordHash ? 'disabled' :
					(!$this->post || !empty($this->post['password']) ? 'critical' : 'disabled'),
				'name, surname, patronymic'=>!$this->post || $citizen ? 'critical' : 'disabled',
				'title'=>!$this->post || $organization ? 'critical' : 'disabled',
				'agreement'=>$registration ? 'critical' : 'disabled',
				'restore'=>'disabled'
			);
		}else{
			$code = isset(Yii::app()->controller->actionParams['code']);
			
			return array_merge(array_fill_keys($this->classAttributeNames, 'disabled'), array(
				'login'=>$code ? 'view' : 'disabled',
				'restore'=>$code ? 'disabled' : 'critical',
				'password, password2'=>$code ? 'critical' : 'disabled'
			));
		}
	}
	
	/**
	 * Получение ограничителей сценариев
	 * @return array Ассоциативный массив в формате
	 * 	array(
	 * 		{Название атрибута}=>array(
	 * 			{Символьный идентификатор валидатора}=>{Признак применения валидатора},
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	public function limiterScenario(){
		return array(
			'login'=>array(
				'unique'=>$this->isNewRecord || (isset($this->post['login']) && $this->post['login'] !== $this->login)
			),
			'email'=>array(
				'unique'=>$this->isNewRecord || (isset($this->post['email']) && $this->post['email'] !== $this->email)
			),
			'phone'=>array(
				'unique'=>$this->isNewRecord || (isset($this->post['phone']) && $this->post['phone'] !== $this->phone)
			)
		);
	}
	
	/**
	 * Определение порядковых номеров меток атрибутов в зависимости от текущего действия над моделью
	 * @return array Ассоциативный массив номеров атрибутов
	 */
	public function attributeLabelsFilter(){
		return array('password'=>array('update, restore'=>1));
	}
	
	/**
	 * Разблокировка пользователя
	 * @result boolean Успешность выполнения
	 */
	public function unlocked(){
		$params_fix = array('id_user'=>$this->id);
		
		try{
			if ($error_search = !$params_fix['id_user'])
				throw new CHttpException(500);
			
			$params_fix['state'] = Yii::app()->conformity->get('user_state', 'name', $this->state, false, false, 'code');
			
			if ($error_state = $params_fix['state'] !== 'locked')
				throw new CHttpException(500);
			
			$this->state = Yii::app()->conformity->get('user_state', 'code', 'working');
			$this->locked = null;
			$ok = $this->update();
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !empty($ok);
		
		Yii::app()->log->fix('user_unlocked', $params_fix, $ok ? 'ok' : ($error_search ? 'model_search' :
			($error_state ? 'model_state' : 'application')), 0);
		
		return $ok;
	}
	
	/**
	 * Метод-геттер для определения _conjugations
	 * @return array Ассоциативный массив идентификаторов пользователя во внешних системах
	 */
	public function getConjugations(){
		if ($this->_conjugations === null){
			$this->_conjugations = array();
			
			if ($conjugations = $this->conjugation){
				$roles_external = Yii::app()->user->getRoles(0, 'name', 'id', false,
					call_user_func_array('array_merge', WebUser::ROLES_EXTERNAL));
				
				foreach ($this->conjugation as $conjugation)
					if (isset($roles_external[$conjugation->id_role]))
						$this->_conjugations[$roles_external[$conjugation->id_role]][$conjugation->id_external] = $conjugation;
			}
		}
		
		return  $this->_conjugations;
	}
	
	/**
	 * Метод-сеттер для установки _conjugations
	 */
	public function setConjugations($conjugations){
		$this->_conjugations = $conjugations;
	}
	
	/**
	 * Метод-геттер для определения _working
	 * @return boolean Признак рабочего статуса данного пользователя
	 */
	public function getWorking(){
		if ($this->_working === null)
			$this->_working = $this->state === Yii::app()->conformity->get('user_state', 'code', 'working');
		
		return $this->_working;
	}
	
	/**
	 * Сокрытие пароля. Значение пароля будет перенесено в специальную переменную _passwordHash
	 */
	public function hidePassword(){
		$this->_passwordHash = $this->password;
		$this->password = null;
	}
	
	/**
	 * Метод-геттер для определения _passwordHash
	 * @return string Зашифрованный хэш пароля
	 */
	public function getPasswordHash(){
		return $this->_passwordHash;
	}
	
	/**
	 * Инициализация атрибута roles, содержащего список цифровых идентификаторов ролей данного пользователя
	 */
	public function initRoles(){
		$this->roles = $this->id ? Yii::app()->user->getRoles($this->id, 'id') : array();
	}
	
	/**
	 * Проверка блокировки пользователя
	 * @param boolean $unlocked Признак разблокировки в случае истечения срока блокировки. По умолчанию равно true
	 * @return boolean Признак блокировки пользователя
	 */
	public function checkLocked($unlocked = true){
		$locked = $this->state === Yii::app()->conformity->get('user_state', 'code', 'locked');
		
		if ($locked && $unlocked && $this->locked <= Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM) && $this->unlocked())
			$locked = false;
		
		return $locked;
	}
	
	/**
	 * Переопрееделение стандартного метода findByPk, выполняющего поиск записи с заданным первичным ключом
	 * @param mixed $pk Первичный ключ
	 * @param mixed $condition Условие критерия поиска. По умолчанию равно ''
	 * @param array $params Параметры критерия поиска. По умолчанию равно array()
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @param array $init_roles Признак необходимости инициализации атрибута roles. По умолчанию равно true
	 * @param array $check_locked Признак необходимости проверки блокировки пользователя. По умолчанию равно true
	 * @return boolean|CActiveRecord|null Найденная запись или null
	 * @see BaseModel::findByPk()
	 */
	public function findByPk($pk, $condition = '', $params = array(), $fix = true, $init_roles = true, $check_locked = true){
		$model = parent::findByPk($pk, $condition, $params, $fix);
		
		if ($model){
			if ($init_roles)
				$model->initRoles();
			
			if ($check_locked)
				$model->checkLocked();
		}
		
		return $model;
	}
	
	/**
	 * Функция поиска
	 * @return CommandDataProvider Объект-поставщик данных
	 */
	public function search(){
		$params_provider = array(
			'pagination'=>array('pageSize'=>isset($_POST['filter-size']) ? $_POST['filter-size'] : static::FILTER_SIZE_DEF),
			'sort'=>array('defaultOrder'=>'roles, title, id', 'attributes'=>array('title', 'roles', 'state',
			'registration'=>array('asc'=>'registration_num', 'desc'=>'registration_num DESC'))));
		
		if (!Yii::app()->request->isAjaxRequest)
			$data = new CommandDataProvider($this, $params_provider);
		else{
			$params_fix = Arrays::pop($_POST, 'filter-user-title, filter-user-state, filter-user-roles', false, true);
			list($filter_title, $filter_state, $filter_roles) = array_values($params_fix);
			$controller = Yii::app()->controller;
			
			if ($controller->id == 'appeal'){
				$roles_access = implode(',', Yii::app()->user->getRoles(0, 'id', false, false,
					in_array($controller->action->id, array('create', 'update')) ? 'citizen, organization' : 'clerk, specialist'));
				
				if (!$filter_roles)
					$filter_roles = $roles_access;
			}
			
			try{
				$roles = Yii::app()->user->rolesList;
				$roles_id = Arrays::facing($roles, 'id');
				list($citizen_desc, $organization_desc) = Arrays::pop($roles, 'citizen.description, organization.description');
				$select_title = 
					'CASE WHEN "t1"."roles_desc" LIKE \'%'.$citizen_desc.'%\' THEN CONCAT("t1"."surname", \' \', '.
									'"t1"."name", \' \', "t1"."patronymic") '.
							 'WHEN "t1"."roles_desc" LIKE \'%'.$organization_desc.'%\' THEN "t1"."title" '.
							 'WHEN "t1"."surname" <> \'\' THEN CONCAT("t1"."surname", \' \', "t1"."name", \' \', "t1"."patronymic") '.
							 'WHEN "t1"."title" <> \'\' THEN "t1"."title" '.
							 'ELSE SUBSTRING("t1"."roles_desc" FROM \'^\w*\') '.
					'END AS "title"';
				$select = array('t0.surname surname', 't0.name name', 't0.patronymic patronymic', 't0.title title');
				$select_t0 = array_merge($select, array('t0.id id', 't0.state state',
					'STRING_AGG("auth_item"."description", \', \' ORDER BY "auth_item"."id") AS "roles_desc"', 't0.registration'));
				$select_t1 = array('t1.id id', $select_title, 't1.state state_code', 'directory_conformity.description state',
					't1.roles_desc roles', 't1.registration registration_num',
					'TO_CHAR("t1"."registration", \'DD.MM.YYYY\') AS "registration"');
				$where = $where_count = array();
				$join = array(
					'auth_assignment'=>'"*"."userid" = "t"."id"',
					'auth_assignment$'=>'"*"."userid" = "t0"."id"',
					'auth_item'=>'"*"."name" = "auth_assignment"."itemname"',
					'directory_conformity'=>'"*"."section" = \'user_state\' AND "*"."code" = "t1"."state"'
				);
				
				if (!empty($roles_access))
					$join['auth_item'] .= ' AND "*"."id" IN ('.$roles_access.')';
				
				if ($filter_title){
					preg_match_all('/[A-Za-zА-яЁё\d]+/u', $filter_title, $matches);
					
					if ($filter_title = !empty($matches[0])){
						$select_t0_count = array_merge($select, array('STRING_AGG("auth_item"."description", \',\') AS "roles_desc"'));
						$select_t1_count = array($select_title);
						$where_t0_count = false;
						$where[] = $where_count[] = 'LOWER("t"."title") '.(count($matches[0]) == 1 ?
							"LIKE '%".mb_strtolower(reset($matches[0]))."%'" : "SIMILAR TO '%(".mb_strtolower(implode('|', $matches[0])).")%'");
					}
				}
				
				if ($filter_state){
					$where[] = '"t"."state_code" IN ('.$filter_state.')';
					
					if ($filter_title)
						$where_t0_count = '"t0"."state" IN ('.$filter_state.')';
					else
						$where_count[] = '"t"."state" IN ('.$filter_state.')';
				}
				
				if ($filter_roles){
					if ($role_alone = mb_strpos($filter_roles, ',') === false)
						$condition_roles = '"t"."roles" LIKE \'%'.Arrays::pop($roles_id, $filter_roles.'.description')."%'";
					else{
						$select_t0[] = $add0 = 'ARRAY_AGG("auth_item"."id") AS "roles_id"';
						$select_t1[] = $add1 = 't1.roles_id roles_id';
						$condition_roles = '"t"."roles_id" && ARRAY['.$filter_roles.']';
					}
					
					$where[] = $condition_roles;
					
					if ($filter_title){
						if ($role_alone)
							$select_t1_count[] = 't1.roles_desc roles';
						else{
							$select_t0_count[] = $add0;
							$select_t1_count[] = $add1;
						}
						
						$where_count[] = $condition_roles;
					}else
						$where_count[] = '"auth_assignment"."itemname" IN (\''.implode("', '", Arrays::select($roles_id, 'name', false,
							false, Arrays::setType(Strings::devisionWords($filter_roles), 'integer')))."')";
				}
				
				$where = !$where ? false : (count($where) == 1 ? reset($where) : array_merge(array('and'), $where));
				$where_count = !$where_count ? false :
					(count($where_count) == 1 ? reset($where_count) : array_merge(array('and'), $where_count));
				
				$params_provider['criteria'] = Yii::app()->db->createCommand()->
					from('('.
						Yii::app()->db->createCommand()->
							select($select_t1)->
							from('('.
								Yii::app()->db->createCommand()->
									select($select_t0)->
									from('{{user}} t0')->
										leftJoin($join, 'auth_assignment$')->
										leftJoin($join, 'auth_item')->
									group('t0.id')->
									text.
								') AS "t1"')->
							leftJoin($join, 'directory_conformity')->
							text.
						') AS "t"')->
					where($where);
				
				$params_provider['countCriteria'] = $filter_title ?
					Yii::app()->db->createCommand()->
						from('('.
							Yii::app()->db->createCommand()->
								select($select_t1_count)->
								from('('.
									Yii::app()->db->createCommand()->
										select($select_t0_count)->
										from('{{user}} t0')->
											leftJoin($join, 'auth_assignment$')->
											leftJoin($join, 'auth_item')->
										where($where_t0_count)->
										group('t0.id')->
										text.
									') AS "t1"')->
								text.
							') AS "t"')->
						where($where_count) :
					Yii::app()->db->createCommand()->
						from('{{user}} t')->
						where($where_count)->
						leftJoin($filter_roles ? $join : false, 'auth_assignment');
				
				$data = new CommandDataProvider($this, $params_provider);
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('user_list', $params_fix, $ok ? 'ok' : 'user_list');
		}
		
		return $data;
	}
	
}
