<?php
/**
 * Модель данных, используемая для осуществления подтверждения введённых данных основной модели
 * @author v.zubov
 */
class CodeModel extends BaseModel{
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('emailcode, phonecode', 'safe')
	);
	
}
