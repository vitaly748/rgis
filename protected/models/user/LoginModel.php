<?php
/**
 * Модель данных авторизации
 * @author v.zubov
 */
class LoginModel extends BaseModel{
	
	/**
	 * @var string Логин
	 */
	public $login;
	
	/**
	 * @var string Пароль
	 */
	public $password;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('login, password', 'safe'),
		array('password', 'authenticate')
	);
	
	/**
	 * @var UserIdentity Экземпляр класса идентификации
	 */
	protected $_identity;
	
	/**
	 * Метод-геттер для определения _identity
	 * @return UserIdentity Экземпляр класса идентификации
	 */
	public function getIdentity(){
		if ($this->_identity === null){
			$this->_identity = new UserIdentity($this->login, $this->password);
			$this->_identity->authenticate();
		}
		
		return $this->_identity;
	}
	
	/**
	 * Функция проверки "authenticate", осуществляет проверку авторизации пользователя
	 * @param string $attribute Название атрибута
	 * @param array $params Список параметров проверки
	 */
	public function authenticate($attribute, $params){
		if ($this->identity->errorCode)
			switch ($this->identity->errorCode){
				case UserIdentity::ERROR_USERNAME_INVALID:
				case UserIdentity::ERROR_PASSWORD_INVALID:
				case UserIdentity::ERROR_CONFIRM:
					$this->addError('password', Yii::app()->errorAttribute->get($this->modelName, 'password', 'invalid'));
					
					break;
				case UserIdentity::ERROR_PRIVATE:
					$this->addError('login', Yii::app()->errorAttribute->get($this->modelName, 'login', 'private'));
					
					break;
				case UserIdentity::ERROR_LOCKED:
					$this->addError('login', sprintf(Yii::app()->errorAttribute->get($this->modelName, 'login', 'locked'),
						Swamper::date($this->identity->user['locked'])));
					
					break;
				case UserIdentity::ERROR_APPLICATION:
					$this->addError('login', Yii::app()->errorAttribute->get($this->modelName, 'login', 'application'));
			}
	}
	
	/**
	 * Получение данных профиля авторизующегося пользователя
	 * @return UserModel Данные профиля
	 */
	public function getUser(){
		return $this->identity->user;
	}
	
}
