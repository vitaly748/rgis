<?php
/**
 * Модель данных запроса подтверждений пользователя
 * @author v.zubov
 */
class ConfirmModel extends BaseModel{
	
	/**
	 * @var array Список допустимых действий над данной моделью
	 */
	const ACTIONS = array('update', 'juid_confirm/cancel');
	
	/**
	 * @var boolean Признак соглашения с Пользовательским соглашением
	 */
	public $agreement;
	
	/**
	 * @var boolean Признак соглашения с работой по неГОСТ-сертификату
	 */
	public $secure;
	
	/**
	 * @var boolean Признак запоминания соглашения с работой по неГОСТ-сертификату
	 */
	public $remember;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('agreement, secure, remember', 'safe')
	);
	
	/**
	 * Получение ограничителей статусов атрибутов для текущего действия модели
	 * @return array Ассоциативный массив в формате {Название атрибута}=>{Символьный идентификатор максимального статуса атрибута}
	 */
	public function limiterStates(){
		$type = ($state = Yii::app()->user->getState('confirm')) ? $state['type'] : false;
		
		return array(
			'agreement'=>$type === WebUser::CONFIRM_TYPE_AGREEMENT ? 'update' : 'disabled',
			'secure, remember'=>$type === WebUser::CONFIRM_TYPE_SECURE ? 'update' : 'disabled'
		);
	}
	
}
