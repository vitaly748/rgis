<?php
/**
 * Модель данных сопряжений пользователей
 * @author v.zubov
 */
class UserConjugationModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Идентификатор пользователя
	 */
	public $id_owner;
	
	/**
	 * @var integer Идентификатор пользователя во внешней системе
	 */
	public $id_external;
	
	/**
	 * @var integer Идентификатор роли, навязываемой данным сопряжением
	 */
	public $id_role;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'owner'=>array(self::BELONGS_TO, 'UserModel', 'id_owner'),
			'role'=>array(self::BELONGS_TO, 'AuthItemModel', array('id_role'=>'id'))
		);
	}
	
}
