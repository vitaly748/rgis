<?php
/**
 * Модель данных настроек профиля пользователя
 * @author v.zubov
 */
class UserSettingModel extends BaseModel{
	
	/**
	 * @var array Список допустимых действий над данной моделью
	 */
	const ACTIONS = array('view', 'update', 'esiadone');
	
	/**
	 * @var integer Цифровой идентификатор способа доставки уведомлений пользователю
	 */
	public $delivery;
	
	/**
	 * @var boolean Признак привязки к ЕСИА
	 */
	public $esia;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('delivery', 'numerical', 'integerOnly'=>true)
	);
	
}
