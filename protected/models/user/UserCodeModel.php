<?php
/**
 * Модель данных кодов подтверждений
 * @author v.zubov
 */
class UserCodeModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Идентификатор пользователя
	 */
	public $id_owner;
	
	/**
	 * @var integer Код подтверждения операции
	 */
	public $code;
	
	/**
	 * @var integer Код типа агента доставки кода подтверждения
	 */
	public $type;
	
	/**
	 * @var string Дата отправки кода
	 */
	public $date;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'owner'=>array(self::BELONGS_TO, 'UserModel', 'id_owner')
		);
	}
	
}
