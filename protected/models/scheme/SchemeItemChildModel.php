<?php
/**
 * Модель данных связей элементов схемы
 * @author v.zubov
 */
class SchemeItemChildModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор элемента схемы
	 */
	public $id_item;
	
	/**
	 * @var integer Идентификатор родительского элемента схемы
	 */
	public $id_parent;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'item'=>array(self::BELONGS_TO, 'SchemeItemModel', 'id_item'),
			'parent'=>array(self::BELONGS_TO, 'SchemeItemModel', 'id_parent')
		);
	}
	
}
