<?php
/**
 * Модель данных элементов схемы
 * @author v.zubov
 */
class SchemeItemModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Идентификатор статичного раздела
	 */
	public $id_static;
	
	/**
	 * @var string Маршрут страницы (расширенный)
	 */
	public $route;
	
	/**
	 * @var string Заголовок раздела
	 */
	public $title;
	
	/**
	 * @var string Расширенный заголовок раздела. Применяется, когда раздел активен
	 */
	public $title_active;
	
	/**
	 * @var integer Код статуса раздела: "Рабочий", "Заблокированный", "Невидимый"
	 */
	public $state;
	
	/**
	 * @var string PHP-выражение, определяющее доступность страницы
	 */
	public $enabled;
	
	/**
	 * @var string PHP-выражение, определяющее видимость страницы. Невидимая страница также является и недоступной
	 */
	public $visible;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'item'=>array(self::HAS_ONE, 'SchemeItemChildModel', 'id_item'),
			'parent'=>array(self::HAS_ONE, 'SchemeItemChildModel', 'id_parent'),
			'static'=>array(self::BELONGS_TO, 'SchemeItemStaticModel', 'id_static')
		);
	}
	
}
