<?php
/**
 * Модель данных файлов выгрузки РГИС
 * @author v.zubov
 */
class ImportModel extends ExportModel{
	
	/**
	 * @var string Имя загруженного файла
	 */
	public $file;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('file', 'safe')
	);
	
	/**
	 * Функция поиска
	 * @return CArrayDataProvider Объект-поставщик данных
	 */
	public function search(){
		$sn_data = WebUser::PREFIX_DATA.$this->modelName;
		$data = array();
		
		if (!Yii::app()->request->isAjaxRequest)
			Yii::app()->user->setState($sn_data, null);	
		elseif (!Yii::app()->user->hasState($sn_data)){
			$params_fix = array();
			
			try{
				if ($files = Yii::app()->file->get(false, true))
					foreach ($files as $file)
						$data[] = array(
							'id'=>$file->id,
							'provider'=>ImportController::PROVIDER,
							'period_num'=>$period_num = strtotime(mb_substr($file->name, 0, 7)),
							'period'=>Swamper::date($period_num, Swamper::FORMAT_DATE_USER_WITHOUT_DAY_FULL),
							'date_num'=>$file->date,
							'date'=>Swamper::date($file->date, Swamper::FORMAT_DATE_USER),
							'size'=>Swamper::fileSize(Yii::app()->file->filesize($file->id)),
							'activate'=>$file->category
						);
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('import_list', $params_fix, $ok ? 'ok' : 'import_list');
			
			if ($ok)
				Yii::app()->user->setState($sn_data, $data);
		}else
			$data = Yii::app()->user->getState($sn_data);
		
		return new CArrayDataProvider($data, array(
			'pagination'=>array('pageSize'=>isset($_POST['filter-size']) ? $_POST['filter-size'] : static::FILTER_SIZE_DEF),
			'sort'=>array(
				'defaultOrder'=>'provider, period_num DESC',
				'attributes'=>array(
					'provider', 'size',
					'period'=>array(
						'asc'=>'period_num',
						'desc'=>'period_num DESC'
					),
					'date'=>array(
						'asc'=>'date_num',
						'desc'=>'date_num DESC'
					)
				)
			)
		));
	}
	
}
