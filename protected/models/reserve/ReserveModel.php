<?php
/**
 * Модель данных файлов резервных копий Базы данных
 * @author v.zubov
 */
class ReserveModel extends ExportModel{
	
	/**
	 * @var string Дата формирования файла резервной копии
	 */
	public $date;
	
	/**
	 * @var integer Размер файла резервной копии
	 */
	public $size;
	
	/**
	 * Функция поиска
	 * @return CArrayDataProvider Объект-поставщик данных
	 */
	public function search(){
		$sn_data = WebUser::PREFIX_DATA.$this->modelName;
		$data = array();
		
		if (!Yii::app()->request->isAjaxRequest)
			Yii::app()->user->setState($sn_data, null);	
		elseif (!Yii::app()->user->hasState($sn_data)){
			$params_fix = array();
			
			try{
				if ($files = Yii::app()->file->get(false, true))
					foreach ($files as $file)
						$data[] = array(
							'id'=>$file->id,
							'date_num'=>$file->date,
							'date'=>Swamper::date($file->date, Swamper::FORMAT_DATE_RGIS),
							'size'=>Swamper::fileSize(Yii::app()->file->filesize($file->id))
						);
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('reserve_list', $params_fix, $ok ? 'ok' : 'reserve_list');
			
			if ($ok)
				Yii::app()->user->setState($sn_data, $data);
		}else
			$data = Yii::app()->user->getState($sn_data);
		
		return new CArrayDataProvider($data, array(
			'pagination'=>array('pageSize'=>isset($_POST['filter-size']) ? $_POST['filter-size'] : static::FILTER_SIZE_DEF),
			'sort'=>array(
				'defaultOrder'=>'date_num DESC',
				'attributes'=>array(
					'date'=>array(
						'asc'=>'date_num',
						'desc'=>'date_num DESC'
					),
					'size'
				)
			)
		));
	}
	
}
