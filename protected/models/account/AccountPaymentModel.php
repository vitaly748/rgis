<?php
/**
 * Модель данных для выбора способа оплаты по лицевому счёту
 * @author v.zubov
 */
class AccountPaymentModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор лицевого счёта
	 */
	public $id;
	
	/**
	 * @var float Сумма платежа
	 */
	public $sum;
	
	/**
	 * @var integer Цифровой идентификатор способа оплаты
	 */
	public $type;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('id, type', 'numerical', 'integerOnly'=>true),
		array('sum', 'numerical')
	);
	
}
