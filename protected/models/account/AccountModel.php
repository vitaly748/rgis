<?php
/**
 * Модель данных для просмотра информации по лицевым счетам пользователя
 * @author v.zubov
 */
class AccountModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор лицевого счёта
	 */
	public $id;
	
	/**
	 * @var string Тип лицевого счёта
	 */
	public $type;
	
	/**
	 * @var string Исполнитель
	 */
	public $executor;
	
	/**
	 * @var string Адрес помещения, связанного с лицевым счётом
	 */
	public $address;
	
	/**
	 * @var float Площадь помещения
	 */
	public $area;
	
	/**
	 * @var float Доля собственности в помещении
	 */
	public $share;
	
	/**
	 * @var float Текущий баланс
	 */
	public $balance;
	
	/**
	 * Функция поиска
	 * @return CArrayDataProvider Объект-поставщик данных
	 */
	public function search(){
		$sn_data = WebUser::PREFIX_DATA.$this->modelName;
		$data = array();
		
		if (!Yii::app()->request->isAjaxRequest)
			Yii::app()->user->setState($sn_data, null);	
		else if (!Yii::app()->user->hasState($sn_data)){
			if ($accounts = Yii::app()->user->accounts)
				foreach ($accounts as $accounts_role)
					foreach ($accounts_role as $account)
						$data[] = array(
							'id'=>Arrays::pop($account, 'id_conjugation'),
							'account'=>Arrays::pop($account, 'id'),
							'type'=>($type = Arrays::pop($account, 'type')) ? Yii::app()->conformity->get('account_type', 'description',
								$type == Bss1::ACCOUNT_TYPE_BSS1 ? 'bss1' : 'bss2') : '',
							'executor'=>Arrays::pop($account, 'recipient'),
							'address'=>Swamper::address($account, true),
							'area'=>($area = Arrays::pop($account, 'area')) ? Swamper::unit($area, Swamper::UNIT_METER, 2, false, '2') : '',
							'share'=>Arrays::pop($account, 'share'),
							'balance_num'=>$balance_num = (float)Arrays::pop($account, 'balance'),
							'balance'=>Numbers::format($balance_num)
						);
			
			Yii::app()->user->setState($sn_data, $data);
		}else
			$data = Yii::app()->user->getState($sn_data);
		
		return new CArrayDataProvider($data, array(
			'pagination'=>array('pageSize'=>1000),
			'sort'=>array(
				'defaultOrder'=>'type, account',
				'attributes'=>array(
					'account', 'type', 'executor', 'address', 'area', 'share',
					'balance'=>array(
						'asc'=>'balance_num',
						'desc'=>'balance_num DESC'
					)
				)
			)
		));
	}
	
}
