<?php
/**
 * Модель данных для просмотра операций по лицевому счёту
 * @author v.zubov
 */
class AccountOperationModel extends BaseModel{
	
	/**
	 * @var string Дата транзакции
	 */
	public $date;
	
	/**
	 * @var string Название услуги транзакции
	 */
	public $service;
	
	/**
	 * @var string Название операции транзакции
	 */
	public $operation;
	
	/**
	 * @var float Сумма
	 */
	public $sum;
	
	/**
	 * @var float Баланс
	 */
	public $balance;
	
	/**
	 * Функция поиска
	 * @return CArrayDataProvider Объект-поставщик данных
	 */
	public function search(){
		$sn_data = WebUser::PREFIX_DATA.$this->modelName;
		$sn_filter = WebUser::PREFIX_FILTER.$this->modelName;
		$sn_set = WebUser::PREFIX_SET.'id_accounts';
		$data = array();
		
		if (!Yii::app()->request->isAjaxRequest)
			Yii::app()->user->setState($sn_data, null);	
		else{
			list($filter_account, $filter_service, $filter_date_from, $filter_date_to) =
				Arrays::pop($_POST,
				'filter-operation-account, filter-operation-service, filter-operation-date-from, filter-operation-date-to');
			
			foreach (array('filter_date_from', 'filter_date_to') as $name)
				if ($$name)
					if (!preg_match('/^[\d.]+/u', $$name, $matches) || !isset($matches[0]))
						$$name = false;
					else
						$$name = $matches[0];
			
			$filter_key = $filter_account.$filter_date_from.$filter_date_to;
			
			if (!Yii::app()->user->hasState($sn_data) || Yii::app()->user->getState($sn_filter) != $filter_key){
				$params_fix = array('filter'=>array(
					'filter_account'=>$filter_account,
					'filter_date_from'=>$filter_date_from,
					'filter_date_to'=>$filter_date_to
				));
				
				try{
					if (!Yii::app()->user->hasState($sn_set))
						throw new CHttpException(500, 'not set id_accounts');
					
					$id_accounts = $params_fix['id_accounts'] = Yii::app()->user->getState($sn_set);
					
					if (!$id_accounts)
						throw new CHttpException(500, 'empty id_accounts');
					
					if (!isset($id_accounts[$filter_account]))
						$filter_account = $params_fix['filter_account_set'] = reset(array_keys($id_accounts));
					
					list($account, $component) = Arrays::pop($id_accounts[$filter_account], 'account, component');
					
					if (!$account || !$component)
						throw new CHttpException(500, 'filter_account incorrect');
					
					$result = Yii::app()->$component->query('operation_list', array(
						'id'=>$account,
						'date_start'=>$filter_date_from ?
							Swamper::date($filter_date_from, Swamper::FORMAT_DATE_SYSTEM_WITHOUT_TIME) : false,
						'date_end'=>$filter_date_to ?
							Swamper::date($filter_date_to, Swamper::FORMAT_DATE_SYSTEM_WITHOUT_TIME) : false
					));
					
					if (!$result)
						throw new CHttpException(500, 'empty result');
					
					$params_fix['result'] = $result;
					$id = 1;
					$balance = (float)Arrays::pop($result, 'balance_start');
					$services = array();
					
					foreach ($result['operations'] as $operation_info){
						list($date, $service, $operation, $parish, $spend) =
							Arrays::pop($operation_info, 'date, service, operation, parish, spend');
						
						$parish_real = (float)$spend;
						$spend_real = (float)$parish;
						
						if ($service && !in_array($service, $services))
							$services[] = $service;
						
						if ($parish_real < 0 || $spend_real < 0)
							list($parish_real, $spend_real) = array(-$spend_real, -$parish_real);
						
						$data[] = array(
							'id'=>$id++,
							'date_num'=>$date_num = mb_substr($date, 0, 10),
							'date'=>Swamper::date($date_num, Swamper::FORMAT_DATE_USER_WITHOUT_TIME),
							'service'=>$service,
							'operation'=>$operation,
							'parish_num'=>$parish_real,
							'parish'=>Numbers::format($parish_real),
							'spend_num'=>$spend_real,
							'spend'=>Numbers::format($spend_real),
							'sum_num'=>$sum_num = (float)($spend - $parish),
							'sum'=>Numbers::format($sum_num),
							'balance_num'=>$balance = $balance + $sum_num,
							'balance'=>Numbers::format($balance),
						);
					}
					
					sort($services);
				}catch (CException $e){
					$params_fix['msg'] = $e->getMessage();
				}
				
				$ok = !isset($e);
				
				Yii::app()->log->fix('account_operation_list', $params_fix, $ok ? 'ok' :
					(empty($id_accounts) ? 'application' : (empty($result) ? 'agent_request' : 'account_operation_list')));
				
				if ($ok){
					Yii::app()->user->setState($sn_data, $data);
					Yii::app()->user->setState($sn_filter, $filter_key);
				}
			}else
				$data = Yii::app()->user->getState($sn_data);
			
			if ($filter_service && !empty($services) && !in_array($filter_service, $services))
				$filter_service = false;
			
			if ($filter_service)
				foreach ($data as $key=>$value)
					if ($value['service'] != $filter_service)
						unset($data[$key]);
		}
		
		$data = array('data'=>new CArrayDataProvider($data, array(
			'pagination'=>array('pageSize'=>isset($_POST['filter-size']) ? $_POST['filter-size'] : static::FILTER_SIZE_DEF),
			'sort'=>array('defaultOrder'=>'date_num', 'attributes'=>array(
				'date'=>array(
					'asc'=>'date_num, id',
					'desc'=>'date_num DESC, id DESC'
				),
				'service',
				'operation',
				'parish'=>array(
					'asc'=>'parish_num',
					'desc'=>'parish_num DESC'
				),
				'spend'=>array(
					'asc'=>'spend_num',
					'desc'=>'spend_num DESC'
				),
				'sum'=>array(
					'asc'=>'sum_num',
					'desc'=>'sum_num DESC'
				),
				'balance'=>array(
					'asc'=>'balance_num',
					'desc'=>'balance_num DESC'
				)))
		)));
		
		if (!empty($ok)){
			$data['params'] = array(
				'services'=>$services,
				'bstart'=>Numbers::format((float)Arrays::pop($result, 'balance_start')),
				'bend'=>Numbers::format((float)Arrays::pop($result, 'balance_end'))
			);
			
			if ($services_balance = Arrays::pop($result, 'services_balance'))
				foreach ($services_balance as $service_balance)
					$data['params']['bservices'][$service_balance['service']] = array(
						Numbers::format((float)$service_balance['balance_start']), Numbers::format((float)$service_balance['balance_end']));
		}
		
		return $data;
	}
	
}
