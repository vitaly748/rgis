<?php
/**
 * Модель данных первого шага сопряжения учётных записей с Биллинговым центром
 * @author v.zubov
 */
class AccountConnectionModel extends BaseModel{
	
	/**
	 * @var integer Код типа лицевого счёта
	 */
	public $type;
	
	/**
	 * @var string Исполнитель
	 */
	public $executor;
	
	/**
	 * @var integer Номер лицевого счёта
	 */
	public $account;
	
	/**
	 * @var string Плательщик по лицевому счёту
	 */
	public $payer;
	
	/**
	 * @var string Дата последнего платежа по лицевому счёту
	 */
	public $date;
	
	/**
	 * @var float Сумма последнего платежа по лицевому счёту
	 */
	public $sum;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('type, account', 'numerical', 'integerOnly'=>true),
		array('payer', 'safe'),
		array('date', 'date'),
		array('sum', 'numerical'),
		array('account', 'checkAccount'),
		array('sum', 'checkPayment')
	);
	
	/**
	 * Функция проверки "checkAccount", осуществляет проверку номера лицевого счёта
	 * @param string $attribute Название атрибута
	 * @param array $params Список параметров проверки
	 */
	public function checkAccount($attribute, $params){
		if ($this->hasErrors())
			return;
		
		$params_fix = array('account'=>$this->removeDecoration($attribute, false), 'payer'=>$this->payer);
		
		try{
			if ($component = Yii::app()->conformity->get('account_type', 'name', (int)$this->type))
				$params_fix['component'] = $component;
			else
				throw new CHttpException(500, 'invalid type account');
			
			if (($conjugations = Yii::app()->user->model->conjugations) &&
				($role_conjugations = Arrays::pop($conjugations, $params_fix['component'].'_payer')) &&
				isset($role_conjugations[$params_fix['account']])){
					$this->addError('account', Yii::app()->errorAttribute->get($this->modelName, 'account', 'exists'));
					throw new CHttpException(500, 'validation');
				}
			
			if ($result = Yii::app()->$component->query('user_search',
				array('id'=>$params_fix['account'], 'type'=>Bss1::CONSUMER_TYPE_PAYER)))
					$params_fix['result'] = $result;
// 			Arrays::printPre($result, 1);
			if (!$result)
				$this->addError($attribute, Yii::app()->errorAttribute->get($this->modelName, $attribute, 'application'));
			elseif (!$result['error']['code']){
				if ($result['id'] === 1)
					$this->addError($attribute, Yii::app()->errorAttribute->get($this->modelName, $attribute, 'empty'));
				else{
					foreach (array($result['title'], $params_fix['payer']) as $key=>$value){
						$payers[$key] = mb_strtolower(trim(preg_replace(
							array('/\s\s+/u', '/[\'"]/u', '/ё/u'), array(' ', '', 'е'), $value)));
						
						if ($result['subject_law'] === Bss1::LAW_TYPE_NATURAL && (preg_match('/^[а-я-]+\s[а-я]($|\.|\s)/u', $payers[$key])))
							$initials = true;
					}
					
					if (isset($initials)){
						foreach ($payers as &$payer){
							$payer = Strings::userName($payer, Strings::FORMAT_USER_NAME_INITIALS2);
							$lengths[] = mb_strlen($payer);
						}
						unset($payer);
						
						$length_min = min($lengths);
						
						foreach ($payers as &$payer)
							$payer = mb_substr($payer, 0, $length_min);
						unset($payer);
					}
					
					if ($payers[0] !== $payers[1])
						$this->addError('payer', Yii::app()->errorAttribute->get($this->modelName, 'payer', 'incorrect'));
				}
			}elseif ($result['error']['code'] == 10)
				$this->addError($attribute, $params['message']);
			elseif ($result['error']['code'] == 11)
				$this->addError($attribute, Yii::app()->errorAttribute->get($this->modelName, $attribute, 'empty'));
			
			if ($this->hasErrors())
				throw new CHttpException(500, 'validation');
		}catch (CException $e){
			if (($params_fix['msg'] = $e->getMessage()) == 'validation')
				$params_fix['errors'] = $this->errors;
			
			if (!$this->hasErrors())
				$this->addError($attribute, Yii::app()->errorAttribute->get($this->modelName, $attribute, 'application'));
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('account_connection_check_account', $params_fix, $ok ? 'ok' : 'account_connection_check_account');
	}
	
	/**
	 * Функция проверки "checkPayment", осуществляет проверку последнего платежа по лицевому счёту
	 * @param string $attribute Название атрибута
	 * @param array $params Список параметров проверки
	 */
	public function checkPayment($attribute, $params){
		if ($this->hasErrors())
			return;
		
		$params_fix = array('account'=>$this->removeDecoration('account', false), 'date'=>$this->removeDecoration('date', false),
			'sum'=>(float)$this->removeDecoration('sum', false));
		
		try{
			if ($component = Yii::app()->conformity->get('account_type', 'name', (int)$this->type))
				$params_fix['component'] = $component;
			else
				throw new CHttpException(500, 'invalid type account');
			
			if ($result = Yii::app()->$component->query('operation_last', array('id'=>$params_fix['account'])))
				$params_fix['result'] = $result;
			
			if (!$result)
				$this->addError($attribute, Yii::app()->errorAttribute->get($this->modelName, $attribute, 'application'));
			elseif ($result['error']['code'] == 10)
				$this->addError('account', Yii::app()->errorAttribute->get($this->modelName, 'account', 'checkAccount'));
			elseif ($result['error']['code'] == 11 || empty($result['sums']))
				$this->addError($attribute, Yii::app()->errorAttribute->get($this->modelName, $attribute, 'empty'));
			else{
				$time = strtotime($params_fix['date']);
				$time_max = strtotime($result['date']);
				$time_min = $time_max - 3 * 24 * 60 * 60;
				
				if ($time < $time_min || $time > $time_max)
					$this->addError($attribute, $params['message']);
				else{
					$total = 0;
					$sum_ok = false;
					
					foreach ($result['sums'] as $sum){
						if ($sum == $params_fix['sum']){
							$sum_ok = true;
							break;
						}
						
						$total += $sum;
					}
					
					if (!$sum_ok && $total == $params_fix['sum'])
						$sum_ok = true;
					
					if (!$sum_ok)
						$this->addError($attribute, $params['message']);
				}
			}
			
			if ($this->hasErrors())
				throw new CHttpException(500, 'validation');
		}catch (CException $e){
			if (($params_fix['msg'] = $e->getMessage()) == 'validation')
				$params_fix['errors'] = $this->errors;
			
			if (!$this->hasErrors())
				$this->addError($attribute, Yii::app()->errorAttribute->get($this->modelName, $attribute, 'application'));
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('account_connection_check_payment', $params_fix, $ok ? 'ok' : 'account_connection_check_payment');
	}
	
}
