<?php
/**
 * Модель данных элементов авторизации
 * @author v.zubov
 */
class AuthItemModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var string Символьный идентификатор элемента авторизации
	 */
	public $name;
	
	/**
	 * @var integer Код типа элемента авторизации
	 */
	public $type;
	
	/**
	 * @var integer Название элемента авторизации
	 */
	public $description;
	
	/**
	 * @var string Текст бизнес-правила элемента авторизации
	 */
	public $bizrule;
	
	/**
	 * @var array Параметры бизнес-правила элемента авторизации
	 */
	public $data;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'assignment'=>array(self::HAS_MANY, 'AuthAssignmentModel', 'itemname'),
			'parent'=>array(self::HAS_MANY, 'AuthItemChildModel', 'child'),
			'child'=>array(self::HAS_MANY, 'AuthItemChildModel', 'parent'),
			'access_attribute_owner'=>array(self::HAS_MANY, 'AuthAccessAttributeModel', 'owner'),
			'access_attribute_operator'=>array(self::HAS_MANY, 'AuthAccessAttributeModel', 'operator'),
			'conjugation'=>array(self::HAS_MANY, 'UserConjugationModel', 'id_role')
		);
	}
	
}
