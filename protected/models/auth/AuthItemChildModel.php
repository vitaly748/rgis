<?php
/**
 * Модель данных наследований элементов авторизации
 * @author v.zubov
 */
class AuthItemChildModel extends BaseModel{
	
	/**
	 * @var string Символьный идентификатор родительского элемента авторизации
	 */
	public $parent;
	
	/**
	 * @var string Символьный идентификатор дочернего элемента авторизации
	 */
	public $child;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'parent'=>array(self::BELONGS_TO, 'AuthItemModel', 'parent'),
			'child'=>array(self::BELONGS_TO, 'AuthItemModel', 'child')
		);
	}
	
}
