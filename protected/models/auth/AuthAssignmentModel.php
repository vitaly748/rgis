<?php
/**
 * Модель данных назначений элементов авторизации
 * @author v.zubov
 */
class AuthAssignmentModel extends BaseModel{
	
	/**
	 * @var string Символьный идентификатор элемента авторизации
	 */
	public $itemname;
	
	/**
	 * @var integer Идентификатор пользователя
	 */
	public $userid;
	
	/**
	 * @var string Текст бизнес-правила назначения элемента авторизации
	 */
	public $bizrule;
	
	/**
	 * @var array Параметры бизнес-правила назначения элемента авторизации
	 */
	public $data;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'item'=>array(self::BELONGS_TO, 'AuthItemModel', 'itemname'),
			'user'=>array(self::BELONGS_TO, 'UserModel', 'userid')
		);
	}
	
}
