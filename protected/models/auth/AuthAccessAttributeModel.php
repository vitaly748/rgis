<?php
/**
 * Модель данных доступа к моделям данных
 * @author v.zubov
 */
class AuthAccessAttributeModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Идентификатор атрибута модели
	 */
	public $id_attribute;
	
	/**
	 * @var integer Идентификатор условного атрибута модели
	 */
	public $id_attribute_conditional;
	
	/**
	 * @var boolean Признак обращения непосредственно к модели, а не к её конкретному атрибуту
	 */
	public $model;
	
	/**
	 * @var integer Цифровой идентификатор роли или учётной записи оператора модели. Идентификатор учётной записи задаётся с минусом
	 */
	public $operator;
	
	/**
	 * @var integer Цифровой идентификатор роли или учётной записи владельца модели. Идентификатор учётной записи задаётся с минусом
	 */
	public $owner;
	
	/**
	 * @var integer Идентификатор фиксированного значения атрибута из справочника соответствий
	 */
	public $value;
	
	/**
	 * @var integer Идентификатор фиксированного значения условного атрибута из справочника соответствий
	 */
	public $value_conditional;
	
	/**
	 * @var integer Цифровой идентификатор действия над моделью или над атрибутом модели из справочника соответствий
	 */
	public $action;
	
	/**
	 * @var boolean Признак сброса права доступа на выполнение данного действия над моделью или над атрибутом модели
	 */
	public $drop;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'attribute'=>array(self::BELONGS_TO, 'DirectoryAttributeModel', 'id_attribute'),
			'attribute_conditional'=>array(self::BELONGS_TO, 'DirectoryAttributeModel', 'id_attribute_conditional'),
			'operator_role'=>array(self::BELONGS_TO, 'AuthItemModel', 'operator'),
			'owner_role'=>array(self::BELONGS_TO, 'AuthItemModel', 'owner')
		);
	}
	
}
