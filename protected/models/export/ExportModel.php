<?php
/**
 * Модель данных файлов выгрузки
 * @author v.zubov
 */
class ExportModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var string Поставщик информации
	 */
	public $provider;
	
	/**
	 * @var string Отчётный период
	 */
	public $period;
	
	/**
	 * @var string Дата формирования файла выгрузки
	 */
	public $date;
	
	/**
	 * @var integer Размер файла выгрузки
	 */
	public $size;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('provider, period', 'safe')
	);
	
	/**
	 * Функция поиска
	 * @return CArrayDataProvider Объект-поставщик данных
	 */
	public function search(){
		$sn_data = WebUser::PREFIX_DATA.$this->modelName;
		$sn_set = WebUser::PREFIX_SET.'id_responders';
		$data = array();
		
		if (!Yii::app()->request->isAjaxRequest)
			Yii::app()->user->setState($sn_data, null);	
		elseif (!Yii::app()->user->hasState($sn_data)){
			$params_fix = array();
			
			try{
				if (!Yii::app()->user->hasState($sn_set))
					throw new CHttpException(500, 'not set id_responders');
				
				$id_responders = $params_fix['id_responders'] = Yii::app()->user->getState($sn_set);
				
				if ($id_responders && ($files = Yii::app()->file->get(false, true, Yii::app()->user->checkAccess('developer') ?
					false : array('source'=>reset(array_keys($id_responders))))))
						foreach ($files as $file)
							if ($provider = Arrays::pop($id_responders, $file->source))
								$data[] = array(
									'id'=>$file->id,
									'provider'=>$provider,
									'period_num'=>$period_num = strtotime(mb_substr($file->name, 0, 7)),
									'period'=>Swamper::date($period_num, Swamper::FORMAT_DATE_USER_WITHOUT_DAY_FULL),
									'date_num'=>$file->date,
									'date'=>Swamper::date($file->date, Swamper::FORMAT_DATE_USER),
									'size'=>Swamper::fileSize(Yii::app()->file->filesize($file->id))
								);
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('export_list', $params_fix, $ok ? 'ok' : (empty($id_responders) ? 'application' : 'export_list'));
			
			if ($ok)
				Yii::app()->user->setState($sn_data, $data);
		}else
			$data = Yii::app()->user->getState($sn_data);
		
		return new CArrayDataProvider($data, array(
			'pagination'=>array('pageSize'=>isset($_POST['filter-size']) ? $_POST['filter-size'] : static::FILTER_SIZE_DEF),
			'sort'=>array(
				'defaultOrder'=>'provider, period_num DESC',
				'attributes'=>array(
					'provider', 'size',
					'period'=>array(
						'asc'=>'period_num',
						'desc'=>'period_num DESC'
					),
					'date'=>array(
						'asc'=>'date_num',
						'desc'=>'date_num DESC'
					)
				)
			)
		));
	}
	
}
