<?php
/**
 * Модель данных редактирования полномочий ролей пользователей
 * @author v.zubov
 */
class RoleModel extends BaseModel{
	
	/**
	 * @var string Название роли
	 */
	public $name;
	
	/**
	 * @var string Группа роли
	 */
	public $group = array();
	
	/**
	 * @var array Список доступных страниц для текущей роли
	 */
	public $pages = array();
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('name', 'safe'),
		array('group, pages', 'type', 'type'=>'array')
	);
	
}
