<?php
/**
 * Модель данных обращений
 * @author v.zubov
 */
class AppealModel extends BaseModel{
	
	/**
	 * @var array Список допустимых действий над данной моделью
	 */
	const ACTIONS = array('list', 'create', 'view', 'update', 'revoke', 'history', 'accept', 'redirect', 'return', 'request',
		'answer', 'complete', 'deny', 'rollback', 'extend');
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var string Тема обращения
	 */
	public $theme;
	
	/**
	 * @var string Идентификатор темы обращения
	 */
	public $themeid;
	
	/**
	 * @var string Адрес по обращению
	 */
	public $address;
	
	/**
	 * @var string Идентификатор адреса по обращению
	 */
	public $addressid;
	
	/**
	 * @var integer Идентификатор ответчика
	 */
	public $responder;
	
	/**
	 * @var integer Идентификатор категории обращения
	 */
	public $category;
	
	/**
	 * @var integer Идентификатор типа обращения
	 */
	public $type;
	
	/**
	 * @var string Дата окончания обработки обращения
	 */
	public $timeout;
	
	/**
	 * @var integer Идентификатор автора обращения
	 */
	public $authorid;
	
	/**
	 * @var string Текст обращения
	 */
	public $text;
	
	/**
	 * @var string Имена прикреплённых файлов
	 */
	public $files;
	
	/**
	 * @var string Комментарий к действию по обработке обращения
	 */
	public $comment;
	
	/**
	 * @var string Имена прикреплённых к комментарию файлов
	 */
	public $commentfiles;
	
	/**
	 * @var string Ответ на обращение
	 */
	public $answer;
	
	/**
	 * @var string Имена прикреплённых к ответу файлов
	 */
	public $answerfiles;
	
	/**
	 * @var UserModel Автор обращения
	 */
	public $author;
	
	/**
	 * @var integer Идентификатор пользователя, являющегося вышестоящим Делопроизводителем в ходе обработки обращения
	 */
	public $clerk;
	
	/**
	 * @var integer Идентификатор статуса обработки обращения
	 */
	public $state;
	
	/**
	 * @var string Дата создания обращения
	 */
	public $datecreated;
	
	/**
	 * @var string Дата регистрации обращения
	 */
	public $dateaccepted;
	
	/**
	 * @var string Дата завершинея обработки обращения
	 */
	public $datecompleted;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
		array('themeid, addressid, files, commentfiles', 'safe'),
		array('theme, text, comment', 'swearword'),
		array('responder, category, type', 'numerical', 'integerOnly'=>true),
		array('authorid', 'checkAuthor'),
		array('responder', 'checkResponder'),
		array('address', 'fias')
	);
	
	/**
	 * @var array Список атрибутов, имеющих фиксированный набор значений, но при этом допускающих отрицательные значения
	 */
	protected $_unsetAttributeNames = array('responder');
	
	/**
	 * @var boolean Признак того, что текущий пользователь является свидетелем хода обработки данного обращения
	 */
	protected $_witness;
	
	/**
	 * @var array Ассоциативный массив с информацией о наборах ограниченных тем обращений в зависимости от адресата. Задаётся в
	 * фомате
	 * 	array(
	 * 		{Символьный идентификатор адресата}=>array(
	 * 			{Код значения},
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	protected $themeLimits = array(
		'repair'=>array('9.1', '9.2', '9.3', '9.4', '9.5'),
		'tsg_gor77'=>array('1.1', '1.2', '1.3', '1.4', '1.5', '1.6', '1.7', '1.8', '1.9', '1.10', '1.11', '1.12', '1.13', '1.14',
			'1.15', '1.16', '1.17', '1.18', '1.19', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '2.7', '2.8', '2.9', '2.10', '2.11',
			'2.12', '2.13', '2.14', '2.15', '2.16', '2.17', '2.18', '2.19', '2.20', '2.21', '2.22', '2.23', '2.24', '3.1', '3.2',
			'3.3', '3.4', '3.5', '3.6', '3.7', '3.8', '3.9', '3.10', '3.11', '3.12', '3.13', '4', '5.1', '5.2', '5.3', '5.4', '5.5',
			'6.1', '6.2', '6.3', '6.4', '6.5', '7.1', '7.2', '7.3', '7.4', '7.5', '7.6', '7.7', '7.8', '7.9', '7.10', '7.11', '7.12',
			'8.1', '8.2', '8.3', '8.4', '8.5', '8.6', '8.7', '8.8', '8.9', '8.10')
	);
	
	/**
	 * @var array Ассоциативный массив дополнительных подсказок пользователя в зависимости от темы обращения. Задаётся в фомате
	 * 	array(
	 * 		{Код значения}=>{Дополнительная подсказка},
	 * 		...
	 * 	)
	 */
	protected $themeNotes = array(
		'9.3'=>'Так же укажите вид работ, необходимых дому.',
		'9.4'=>'Так же укажите причины переноса срока на более ранний либо на более поздний период',
	);
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'stage'=>array(self::HAS_MANY, 'AppealStageModel', 'id_appeal', 'order'=>'stage.date')
		);
	}
	
	/**
	 * Получение ограничителей статусов атрибутов для текущего действия модели
	 * @return array Ассоциативный массив в формате {Название атрибута}=>{Символьный идентификатор максимального статуса атрибута}
	 */
	public function limiterStates(){
		$state = Yii::app()->conformity->get('appeal_state', 'name', $this->state);
		
		return array(
			'theme, themeid, address, addressid, responder, authorid, text, files'=>
				$this->command !== 'accept' ? 'critical' : 'view',
			'category, type'=>$state == 'registered' && $this->command ? 'critical' : 'view',
			'comment, commentfiles'=>$state == 'extended' ? 'critical' :
				(in_array($this->command, array('request', 'answer', 'complete', 'deny')) ? 'critical' : 'update')
		);
	}
	
	/**
	 * Метод-геттер для определения _addressee
	 * @return boolean|UserModel Адресат текущей модели или false
	 */
	public function getAddressee(){
		if ($this->_addressee === null){
			$this->_addressee = false;
			
			if ($this->id && in_array(Yii::app()->conformity->get('appeal_state', 'name', $this->state),
				array('confirmed', 'extended')) && ($stages = $this->stage)){
					$count = count($stages) - 1;
					$stage_extended = Yii::app()->conformity->get('appeal_stage_state', 'code', 'extended');
					
					while ($count && $stages[$count]->state === $stage_extended)
						$count--;
					
					$id_addressee = $stages[$count]->id_owner;
					
					if ($id_addressee == Yii::app()->user->id)
						$this->_addressee = Yii::app()->user->model;
					else{
						$this->_addressee = UserModel::model()->findByPk($id_addressee, '', array(), true, false, false);
						
						Yii::app()->log->fix('model_search', array('model'=>'user', 'id'=>$id_addressee),
							$this->_addressee ? 'ok' : 'model_search');
					}
				}
		}
		
		return $this->_addressee;
	}
	
	/**
	 * Метод-геттер для определения _witness
	 * @return boolean Признак того, что текущий пользователь является свидетелем хода обработки данного обращения
	 */
	public function getWitness(){
		if ($this->_witness === null){
			$this->_witness = false;
			
			if ($this->id && ($stages = $this->stage) && ($id_user = Yii::app()->user->id))
				foreach ($stages as $stage)
					if ($stage->id_owner == $id_user){
						$this->_witness = true;
						break;
					}
		}
		
		return $this->_witness;
	}
	
	/**
	 * Функция проверки "checkAuthor", осуществляет проверку роли автора по обращению
	 * @param string $attribute Название атрибута
	 * @param array $params Список параметров проверки
	 */
	public function checkAuthor($attribute, $params){
		if ($this->hasErrors())
			return;
		
		if (!Yii::app()->user->checkAccessSet('citizen, organization, clerk', false, array('id_user'=>$this->$attribute)))
			$this->addError($attribute, $params['message']);
	}
	
	/**
	 * Функция проверки "checkResponder", осуществляет проверку роли адресата по обращению
	 * @param string $attribute Название атрибута
	 * @param array $params Список параметров проверки
	 */
	public function checkResponder($attribute, $params){
		if ($this->hasErrors())
			return;
		
		if ((($positive = $this->$attribute > 0) && !Yii::app()->user->checkAccessSet('citizen, organization', false,
			array('id_user'=>$this->authorid))) || (!$positive && (!Yii::app()->user->checkAccessSet('citizen, organization', false,
			array('id_user'=>-$this->$attribute)) || !Yii::app()->user->checkAccess('clerk', array('id_user'=>$this->authorid)))))
				$this->addError($attribute, $params['message']);
	}
	
	/**
	 * Инициализация стандартных атрибутов модели state, authorid, author, _owner, text
	 */
	public function initAttributes(){
		if ($this->id && ($stages = $this->stage)){
			$this->state = Appeal::STATE_CONFORMITY[end($stages)->state];
			$this->authorid = reset($stages)->id_owner;
			$this->text = reset($stages)->text;
		}
		
		if ($this->authorid)
			if ($this->authorid == Yii::app()->user->id)
				$this->author = $this->_owner = Yii::app()->user->model;
			else{
				$this->author = $this->_owner = UserModel::model()->findByPk($this->authorid, '', array(), true, false, false);
				
				Yii::app()->log->fix('model_search', array('model'=>'user', 'id'=>$this->authorid),
					$this->author ? 'ok' : 'model_search');
			}
	}
	
	/**
	 * Инициализация атрибута files, содержащего идентификаторы прикреплённых файлов
	 */
	public function initFiles(){
		if ($this->id && $this->action != 'view' && ($stages = $this->stage) && ($files = reset($stages)->file)){
			$files_value = $files_state_new = array();
			
			foreach ($files as $file){
				$files_value[] = $file_id = File::PREFIX_SAVED_FILES.$file->id;
				$files_state_new[$file_id] = array('name'=>$file->name, 'size'=>Yii::app()->file->filesize($file->id));
			}
			
			$this->files = implode(',', $files_value);
				
			if ($files_state = Yii::app()->user->getState('files'))
				$files_state_new = $files_state + $files_state_new;
			
			Yii::app()->user->setState('files', $files_state_new);
		}
	}
	
	/**
	 * Инициализация атрибута clerk, содержащего идентификатор пользователя, являющегося вышестоящим Делопроизводителем в ходе
	 * обработки обращения
	 */
	public function initClerk(){
		if (in_array(Yii::app()->conformity->get('appeal_state', 'name', $this->state), array('confirmed', 'extended')) &&
			($stages = $this->stage) && count($stages) > 2){
				$states_code = Yii::app()->conformity->get('appeal_stage_state', 'code', 'name', false,
					'redirected, returned, requested, answered, extended');
				$clerks = array();
				
				foreach ($stages as $count=>$stage)
					if (in_array($stage->state, array($states_code['redirected'], $states_code['requested']))){
						$count2 = $count - 1;
						
						while ($count2 && $stages[$count2]->state === $states_code['extended'])
							$count2--;
						
						if ($count2 && Yii::app()->user->checkAccessSet('clerk', false, array('id_user'=>$stages[$count2]->id_owner)))
							$clerks[] = $stages[$count2]->id_owner;
					}elseif (in_array($stage->state, array($states_code['returned'], $states_code['answered'])))
						array_pop($clerks);
				
				$this->clerk = $clerks ? array_pop($clerks) : false;
			}
	}
	
	/**
	 * Переопрееделение стандартного метода findByPk, выполняющего поиск записи с заданным первичным ключом
	 * @param mixed $pk Первичный ключ
	 * @param mixed $condition Условие критерия поиска. По умолчанию равно ''
	 * @param array $params Параметры критерия поиска. По умолчанию равно array()
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @param array $init_attributes Признак необходимости инициализации стандартных атрибутов модели. По умолчанию равно true
	 * @param array $init_files Признак необходимости инициализации атрибута files. По умолчанию равно true
	 * @param array $init_clerk Признак необходимости инициализации атрибута clerk. По умолчанию равно true
	 * @return boolean|CActiveRecord|null Найденная запись или null
	 * @see BaseModel::findByPk()
	 */
	public function findByPk($pk, $condition = '', $params = array(), $fix = true, $init_attributes = true, $init_files = true,
		$init_clerk = true){
			$this->with('stage');
			
			if (Yii::app()->controller->action->id == 'view')
				$this->with('stage.owner');
			
			$model = parent::findByPk($pk, $condition, $params, $fix);
			
			if ($model){
				$model->scenario = '';
				
				if ($init_attributes)
					$model->initAttributes();
				
				if ($init_files)
					$model->initFiles();
				
				if ($init_clerk)
					$model->initClerk();
			}
			
			return $model;
		}
	
	/**
	 * Функция поиска
	 * @return CommandDataProvider Объект-поставщик данных
	 */
	public function search(){
		$params_provider = array(
			'pagination'=>array('pageSize'=>isset($_POST['filter-size']) ? $_POST['filter-size'] :
				reset($fs = static::FILTER_SIZE_VALUES)),
			'sort'=>array(
				'defaultOrder'=>'addressee DESC, importance DESC, timeout_num', 
				'attributes'=>array(
					'id', 'theme', 'author', 'state',
					'datecreated'=>array(
						'asc'=>'datecreated_num',
						'desc'=>'datecreated_num DESC'
					),
					'dateaccepted'=>array(
						'asc'=>'dateaccepted_num',
						'desc'=>'dateaccepted_num DESC'
					),
					'datecompleted'=>array(
						'asc'=>'datecompleted_num',
						'desc'=>'datecompleted_num DESC'
					),
					'timeout'=>array(
						'asc'=>'timeout_num',
						'desc'=>'timeout_num DESC'
					)
				)
			)
		);
		
		if (!Yii::app()->request->isAjaxRequest)
			$data = new CommandDataProvider($this, $params_provider);
		else{
			$params_fix = Arrays::pop($_POST, 'filter-appeal-theme, filter-appeal-author, filter-appeal-state', false, true);
			list($filter_theme, $filter_author, $filter_state) = array_values($params_fix);
			
			try{
				$states = Yii::app()->conformity->get('appeal_state', 'description', 'code');
				$stage_states = Yii::app()->conformity->get('appeal_stage_state', 'code', 'name');
				$roles = Yii::app()->user->getRoles(0, 'description', 'name', false, 'citizen, organization');
				$where = array();
				$id_user = Yii::app()->user->id;
				$case_state_code = $case_state = '(CASE ';
				$case_time_border = '(CASE ';
				
				foreach (Appeal::STATE_CONFORMITY as $code_stage_appeal=>$code_appeal)
					if (!in_array($code_stage_appeal, Appeal::STATE_STAGE_CONFIRMED)){
						$case_state_code .= 'WHEN "t4"."state" = '.$code_stage_appeal." THEN $code_appeal ";
						$case_state .= 'WHEN "t4"."state" = '.$code_stage_appeal." THEN '{$states[$code_appeal]}' ";
					}
				
				for ($count = count(Appeal::TIME_BORDER) - 1; $count >= 0; $count--)
					$case_time_border .= 'WHEN "a"."timeout" <= \''.Swamper::date(time() + Appeal::TIME_BORDER[$count],
							Swamper::FORMAT_DATE_SYSTEM)."' THEN ".($count + 1).' ';
				
				$case_state_code .= 'ELSE '.Appeal::STATE_CONFIRMED.' END) AS "state_code"';
				$case_state .= 'ELSE \''.$states[Appeal::STATE_CONFIRMED].'\' END) AS "state"';
				$case_time_border .= 'ELSE 0 END) AS "importance"';
				
				$select_count = array('t4.theme', $case_state_code, '("t4"."own" = '.$id_user.') AS "own"',
					'("t4"."state" IN ('.implode(', ', Arrays::filterKey($stage_states,
					'accepted, redirected, returned, requested, answered')).') AND "t4"."addressee" = '.
					$id_user.') AS "addressee"', '(ARRAY['.$id_user.'] && "t4"."owners") AS "witness"');
				$select = array_merge($select_count, array('t4.id', 't4.timeout', 't4.timeout_num', '(CASE WHEN "t4"."state" IN ('.
					$stage_states['revoked'].', '.$stage_states['completed'].', '.$stage_states['denied'].
					') THEN -1 ELSE "t4"."importance" END) AS "importance"', 't3.author', $case_state,
					'TO_CHAR("t4"."datecreated", \'DD.MM.YYYY HH24:MI\') AS "datecreated"', 't4.datecreated datecreated_num',
					'(CASE WHEN "t4"."state" IN ('.$stage_states['created'].', '.$stage_states['revoked'].') THEN NULL ELSE '.
					'TO_CHAR("t4"."dateaccepted", \'DD.MM.YYYY HH24:MI\') END) AS "dateaccepted"',
					'(CASE WHEN "t4"."state" IN ('.$stage_states['created'].', '.$stage_states['revoked'].') THEN NULL ELSE '.
					'"t4"."dateaccepted" END) AS "dateaccepted_num"',
					'(CASE WHEN "t4"."state" IN ('.$stage_states['completed'].', '.$stage_states['denied'].') THEN '.
					'TO_CHAR("t4"."datecompleted", \'DD.MM.YYYY HH24:MI\') ELSE NULL END) AS "datecompleted"',
					'(CASE WHEN "t4"."state" IN ('.$stage_states['completed'].', '.$stage_states['denied'].') THEN '.
					'"t4"."datecompleted" ELSE NULL END) AS "datecompleted_num"'));
				
				if ($filter_theme){
					preg_match_all('/[A-Za-zА-яЁё\d.]+/u', $filter_theme, $matches);
					
					if ($filter_theme = !empty($matches[0]))
						$where[] = 'LOWER("theme") '.(count($matches[0]) == 1 ? "LIKE '%".mb_strtolower(reset($matches[0]))."%'" :
							"SIMILAR TO '%(".mb_strtolower(implode('|', $matches[0])).")%'");
				}
				
				if ($filter_author){
					preg_match_all('/[A-Za-zА-яЁё]+/u', $filter_author, $matches);
					
					if ($filter_author = !empty($matches[0])){
						$where[] = 'LOWER("author") '.(count($matches[0]) == 1 ? "LIKE '%".mb_strtolower(reset($matches[0]))."%'" :
							"SIMILAR TO '%(".mb_strtolower(implode('|', $matches[0])).")%'");
						$select_count[] = 't3.author';
					}
				}
				
				if ($filter_state)
					$where[] = '"state_code" IN ('.$filter_state.')';
				
				if ($filter_operator = !Yii::app()->user->checkAccessSet('developer, administrator, clerk'))
					$where[] = 'witness';
				
				$where = !$where ? false : (count($where) == 1 ? reset($where) : array_merge(array('and'), $where));
				
				$params_provider['criteria'] = Yii::app()->db->createCommand()->
					from('('.
						Yii::app()->db->createCommand()->
							select($select)->
							from('('.
								Yii::app()->db->createCommand()->
									select(array('t1.id', 't1.timeout', 't1.timeout_num', 't1.importance', 't1.theme',
										'"t1"."states"[("t1"."qu")] AS "state"', '"t1"."dates"[(1)] AS "datecreated"',
										'"t1"."dates"[(2)] AS "dateaccepted"', '"t1"."dates"[("t1"."qu")] AS "datecompleted"', 't1.owners owners',
										'"t1"."owners"[(1)] AS "own"', '"t1"."owners"[("t1"."qu")] AS "addressee"'))->
									from('('.
										Yii::app()->db->createCommand()->
											select(array('a.id', 'TO_CHAR("a"."timeout", \'DD.MM.YYYY\') AS "timeout"', 'a.timeout timeout_num',
												$case_time_border, 'CONCAT("a"."themeid", \'. \', "a"."theme") AS "theme"', 'COUNT("s"."id") AS "qu"',
												'ARRAY_AGG("s"."state" ORDER BY "s"."date") AS "states"',
												'ARRAY_AGG("s"."date" ORDER BY "s"."date") AS "dates"',
												'ARRAY_AGG("s"."id_owner" ORDER BY "s"."date") AS "owners"'))->
											from('{{appeal}} a')->
											leftJoin('{{appeal_stage}} s', '"s"."id_appeal" = "a"."id" AND "s"."state" <> 100')->
											group('a.id')->
											text.') "t1"')->
									text.') "t4"')->
							leftJoin('('.
								Yii::app()->db->createCommand()->
									select(array('t2.id', '(CASE WHEN "t2"."roles_desc" LIKE \'%'.$roles['citizen'].'%\' THEN '.
										'CONCAT("t2"."surname", \' \', "t2"."name", \' \', "t2"."patronymic") '.
										'WHEN "t2"."roles_desc" LIKE \'%'.$roles['organization'].'%\' THEN "t2"."title" '.
										'ELSE SUBSTRING("t2"."roles_desc" FROM \'^\w*\') END) AS "author"'))->
									from('('.
										Yii::app()->db->createCommand()->
											select(array('u.id', 'u.surname', 'u.name', 'u.patronymic', 'u.title',
												'STRING_AGG("ai"."description", \',\') AS "roles_desc"'))->
											from('{{user}} u')->
											leftJoin('{{auth_assignment}} aa', '"aa"."userid" = "u"."id"')->
											leftJoin('{{auth_item}} ai', '"ai"."name" = "aa"."itemname"')->
											group('u.id')->
											text.') "t2"')->
									text.') "t3"', '"t3"."id" = "t4"."own"')->
							text.') "t"')->
					where($where);
				
				$params_provider['countCriteria'] = $filter_state || $filter_operator || $filter_author ?
					Yii::app()->db->createCommand()->
						from('('.
							Yii::app()->db->createCommand()->
								select($select_count)->
								from('('.
									Yii::app()->db->createCommand()->
										select(array('t1.theme', '"t1"."states"[("t1"."qu")] AS "state"', 't1.owners owners',
											'"t1"."owners"[(1)] AS "own"', '"t1"."owners"[("t1"."qu")] AS "addressee"'))->
										from('('.
											Yii::app()->db->createCommand()->
												select(array('CONCAT("a"."themeid", \'. \', "a"."theme") AS "theme"', 'COUNT("s"."id") AS "qu"',
													'ARRAY_AGG("s"."state" ORDER BY "s"."date") AS "states"',
													'ARRAY_AGG("s"."id_owner" ORDER BY "s"."date") AS "owners"'))->
												from('{{appeal}} a')->
												leftJoin('{{appeal_stage}} s', '"s"."id_appeal" = "a"."id" AND "s"."state" <> 100')->
												group('a.id')->
												text.') "t1"')->
										text.') "t4"')->
								leftJoin($filter_author ? '('.
									Yii::app()->db->createCommand()->
										select(array('t2.id', '(CASE WHEN "t2"."roles_desc" LIKE \'%'.$roles['citizen'].'%\' THEN '.
											'CONCAT("t2"."surname", \' \', "t2"."name", \' \', "t2"."patronymic") '.
											'WHEN "t2"."roles_desc" LIKE \'%'.$roles['organization'].'%\' THEN "t2"."title" '.
											'ELSE SUBSTRING("t2"."roles_desc" FROM \'^\w*\') END) AS "author"'))->
										from('('.
											Yii::app()->db->createCommand()->
												select(array('u.id', 'u.surname', 'u.name', 'u.patronymic', 'u.title',
													'STRING_AGG("ai"."description", \',\') AS "roles_desc"'))->
												from('{{user}} u')->
												leftJoin('{{auth_assignment}} aa', '"aa"."userid" = "u"."id"')->
												leftJoin('{{auth_item}} ai', '"ai"."name" = "aa"."itemname"')->
												group('u.id')->
												text.') "t2"')->
										text.') "t3"' : false, '"t3"."id" = "t4"."own"')->
								text.') "t"')->
						where($where)
					:
					($filter_theme ?
							Yii::app()->db->createCommand()->
								from('('.
									Yii::app()->db->createCommand()->
									select(array('CONCAT("a"."themeid", \'. \', "a"."theme") AS "theme"'))->
									from('{{appeal}} a')->
								text.') "t"')->
								where($where)
					:
							Yii::app()->db->createCommand()->
							from('{{appeal}}')->
							where($where));
				
				$data = new CommandDataProvider($this, $params_provider);
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('appeal_list', $params_fix, $ok ? 'ok' : 'appeal_list');
		}
		
		return $data;
	}
	
	/**
	 * Определение параметров тем
	 * @return array Ассоциативный массив параметров в формате
	 * 	array(
	 * 		{Цифровой идентификатор адресата}=>array(
	 * 			{Код значения}=>{Дополнительная подсказка},
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	public function getThemeParams(){
		$params = array();
		
		if ($responders = Yii::app()->conformity->get('appeal_responder', 'code', 'name'))
			foreach ($this->themeLimits as $responder=>$themes)
				foreach ($themes as $theme)
					$params[$responders[$responder]][$theme] = Arrays::pop($this->themeNotes, $theme, false, false, false, false);
		
		return $params;
	}
	
	/**
	 * Переопределение стандартного метода beforeValidate, выполняющего предпроверку данных модели
	 * @see CModel::beforeValidate()
	 */
	protected function beforeValidate(){
		if (!parent::beforeValidate())
			return false;
		
		if ($this->themeid && $this->responder &&
			($responder = Yii::app()->conformity->get('appeal_responder', 'name', (int)$this->responder))
			&& ($values = Arrays::pop($this->themeLimits, $responder)) && !in_array($this->themeid, $values))
				$this->addError('theme', Yii::app()->errorAttribute->get($this, 'theme', 'invalid'));
		
		return !$this->hasErrors();
	}
	
}
