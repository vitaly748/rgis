<?php
/**
 * Модель данных состояния обращений
 * @author v.zubov
 */
class AppealStageModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Идентификатор обращения
	 */
	public $id_appeal;
	
	/**
	 * @var integer Идентификатор инициатора состояния
	 */
	public $id_owner;
	
	/**
	 * @var string Текст обращения
	 */
	public $text;
	
	/**
	 * @var integer Цифровой идентификатор состояния
	 */
	public $state;
	
	/**
	 * @var string Дата установки состояния
	 */
	public $date;

	/**
	 * @var array Список моделей прикреплённых файлов
	 */
	protected $_file;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'appeal'=>array(self::BELONGS_TO, 'AppealModel', 'id_appeal'),
			'owner'=>array(self::BELONGS_TO, 'UserModel', 'id_owner')
		);
	}
	
	/**
	 * Метод-геттер для определения _file
	 * @return array Список моделей прикреплённых файлов
	 */
	public function getFile(){
		if ($this->_file === null && $this->id)
			$this->_file = Yii::app()->file->get(false, true, array('source'=>$this->id));
		
		return $this->_file;
	}
	
}
