<?php
/**
 * Модель данных для просмотра информации по счётчикам
 * @author v.zubov
 */
class CounterModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор счётчика
	 */
	public $id;
	
	/**
	 * @var string Название счётчика
	 */
	public $counter;
	
	/**
	 * @var float Предыдущее показание
	 */
	public $previous;
	
	/**
	 * @var float Последнее показание
	 */
	public $current;
	
	/**
	 * @var float Новое показание
	 */
	public $new;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array(
			array('id', 'numerical', 'integerOnly'=>true),
			array('new', 'numerical')
	);
	
	/**
	 * Функция поиска
	 * @return CArrayDataProvider Объект-поставщик данных
	 */
	public function search(){
		$sn_data = WebUser::PREFIX_DATA.$this->modelName;
		$sn_filter = WebUser::PREFIX_FILTER.$this->modelName;
		$sn_set = WebUser::PREFIX_SET.'id_addresses';
		$data = array();
		
		if (!Yii::app()->request->isAjaxRequest)
			Yii::app()->user->setState($sn_data, null);	
		else{
			$filter_address = Arrays::pop($_POST, 'filter-counter-address');
			
			if (!Yii::app()->user->hasState($sn_data) || Yii::app()->user->getState($sn_filter) != $filter_address){
				$params_fix = array('filter'=>array('filter_address'=>$filter_address));
				
				try{
					if (!Yii::app()->user->hasState($sn_set))
						throw new CHttpException(500, 'not set id_addresses');
					
					$id_addresses = $params_fix['id_addresses'] = Yii::app()->user->getState($sn_set);
					
					if (!$id_addresses)
						throw new CHttpException(500, 'empty id_addresses');
					
					if (!isset($id_addresses[$filter_address]))
						$filter_address = $params_fix['filter_address_set'] = reset(array_keys($id_addresses));
					
					$counters = Arrays::pop($id_addresses[$filter_address], 'counters');
					
					if (!$counters)
						throw new CHttpException(500, 'filter_address incorrect');
					
					$params_fix['counters'] = $counters;
					
					foreach ($counters as $counter_info){
						list($id, $counter, $previous, $current) =
							Arrays::pop($counter_info, 'id, counter, reading_previous, reading_current');
						
						$data[] = array(
							'id'=>$id,
							'counter'=>$counter,
							'previous'=>Numbers::format($previous, 3, ''),
							'current'=>Numbers::format($current, 3, '')
						);
					}
				}catch (CException $e){
					$params_fix['msg'] = $e->getMessage();
				}
				
				$ok = !isset($e);
				
				Yii::app()->log->fix('counter_list_show', $params_fix, $ok ? 'ok' :
					(empty($id_addresses) ? 'application' : 'counter_list_show'));
				
				if ($ok){
					Yii::app()->user->setState($sn_data, $data);
					Yii::app()->user->setState($sn_filter, $filter_address);
				}
			}else
				$data = Yii::app()->user->getState($sn_data);
		}
		
		return new CArrayDataProvider($data, array(
			'pagination'=>array('pageSize'=>1000),
			'sort'=>array('defaultOrder'=>'counter', 'attributes'=>array('counter', 'previous', 'current'))
		));
	}
	
}
