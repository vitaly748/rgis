<?php
/**
 * Модель данных для просмотра истории ввода показаний счётчиков
 * @author v.zubov
 */
class CounterHistoryModel extends BaseModel{
	
	/**
	 * @var string Дата ввода
	 */
	public $date;
	
	/**
	 * @var float Показание
	 */
	public $value;
	
	/**
	 * Функция поиска
	 * @return CArrayDataProvider Объект-поставщик данных
	 */
	public function search(){
		$sn_data = WebUser::PREFIX_DATA.$this->modelName;
		$sn_filter = WebUser::PREFIX_FILTER.$this->modelName;
		$sn_set = WebUser::PREFIX_SET.'id_counters';
		$data = array();
		
		if (!Yii::app()->request->isAjaxRequest)
			Yii::app()->user->setState($sn_data, null);	
		else{
			list($filter_counter, $filter_date_from, $filter_date_to) =
				Arrays::pop($_POST, 'filter-history-counter, filter-history-date-from, filter-history-date-to');
			
			foreach (array('filter_date_from', 'filter_date_to') as $name)
				if ($$name)
					if (!preg_match('/^[\d.]+/u', $$name, $matches) || !isset($matches[0]))
						$$name = false;
					else
						$$name = $matches[0];
			
			$filter_key = $filter_counter.$filter_date_from.$filter_date_to;
			
			if (!Yii::app()->user->hasState($sn_data) || Yii::app()->user->getState($sn_filter) != $filter_key){
				$params_fix = array('filter'=>array(
					'filter_counter'=>$filter_counter,
					'filter_date_from'=>$filter_date_from,
					'filter_date_to'=>$filter_date_to
				));
				
				try{
					if (!Yii::app()->user->hasState($sn_set))
						throw new CHttpException(500, 'not set id_counters');
					
					$id_counters = $params_fix['id_counters'] = Yii::app()->user->getState($sn_set);
					
					if (!$id_counters)
						throw new CHttpException(500, 'empty id_counters');
					
					if (!isset($id_counters[$filter_counter]))
						$filter_counter = $params_fix['filter_counter_set'] = reset(array_keys($id_counters));
					
					$component = Arrays::pop($id_counters[$filter_counter], 'component');
					
					if (!$component)
						throw new CHttpException(500, 'filter_counter incorrect');
					
					$result = Yii::app()->$component->query('reading_list', array(
						'id'=>$filter_counter,
						'date_start'=>$filter_date_from ?
							Swamper::date($filter_date_from, Swamper::FORMAT_DATE_SYSTEM_WITHOUT_TIME) : false,
						'date_end'=>$filter_date_to ?
							Swamper::date($filter_date_to, Swamper::FORMAT_DATE_SYSTEM_WITHOUT_TIME) : false
					));
					
					if (!$result)
						throw new CHttpException(500, 'empty result');
					
					$params_fix['result'] = $result;
					$id = 1;
					
					foreach ($result['readings'] as $reading){
						list($date, $value) = Arrays::pop($reading, 'date, value');
						
						$data[] = array(
							'id'=>$id++,
							'date_num'=>$date_num = mb_substr($date, 0, 10),
							'date'=>Swamper::date($date_num, Swamper::FORMAT_DATE_USER_WITHOUT_TIME),
							'value'=>$value
						);
					}
				}catch (CException $e){
					$params_fix['msg'] = $e->getMessage();
				}
				
				$ok = !isset($e);
				
				Yii::app()->log->fix('counter_history_list', $params_fix, $ok ? 'ok' :
					(empty($id_counters) ? 'application' : (empty($result) ? 'agent_request' : 'counter_history_list')));
				
				if ($ok){
					Yii::app()->user->setState($sn_data, $data);
					Yii::app()->user->setState($sn_filter, $filter_key);
				}
			}else
				$data = Yii::app()->user->getState($sn_data);
		}
		
		return new CArrayDataProvider($data, array(
			'pagination'=>array('pageSize'=>isset($_POST['filter-size']) ? $_POST['filter-size'] : static::FILTER_SIZE_DEF),
			'sort'=>array('defaultOrder'=>'date_num', 'attributes'=>array(
				'date'=>array(
					'asc'=>'date_num',
					'desc'=>'date_num DESC'
				),
				'value'
			))
		));
	}
	
}
