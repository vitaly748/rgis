<?php
/**
 * Модель данных таблицы файлов
 * @author v.zubov
 */
class FileModel extends BaseModel{
	
	/**
	 * @var integer Идентификатор
	 */
	public $id;
	
	/**
	 * @var integer Код типа источника файла
	 */
	public $type;
	
	/**
	 * @var integer Идентификатор источника файла
	 */
	public $source;
	
	/**
	 * @var integer Код категории файла
	 */
	public $category;
	
	/**
	 * @var string Название файла
	 */
	public $name;
	
	/**
	 * @var string Дата создания файла
	 */
	public $date;
	
	/**
	 * Переопределение стандартного статичного конструктора модели
	 * @param string $className Название класса модели. По умолчанию равно __CLASS__
	 * @return object Экземпляр модели данных
	 * @see CActiveRecord::model()
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	/**
	 * Переопределение стандартного метода relations, выдающего ассоциативный массив связей данной модели
	 * с другими моделями
	 * @return array Массив связей
	 * @see CActiveRecord::relations()
	 */
	public function relations(){
		return array(
			'appeal'=>array(self::BELONGS_TO, 'AppealStageModel', 'source'),
			'message'=>array(self::BELONGS_TO, 'ChatMessageModel', 'source'),
			'scheme'=>array(self::BELONGS_TO, 'SchemeItemStaticModel', 'source')
		);
	}
	
}
