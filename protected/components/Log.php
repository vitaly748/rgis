<?php
/**
 * Класс контроля событий, произошедших в рамках приложения
 * @author v.zubov
 */
class Log extends ComponentDeploy{
	
	/**
	 * @var string Псевдоним пути к файлам лога
	 */
	const DIR_LOG = 'application.logs';
	
	/**
	 * @var string Псевдоним пути к папке файла конфигурации
	 */
	const DIR_REPORT = 'application.config';
	
	/**
	 * @var string Шаблон имени файлов лога
	 */
	const FN_LOG = '%s.txt';
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_REPORT = 'report.php';
	
	/**
	 * @var array Ассоциативный массив ключей параметров запущенных фиксаций. Применяется для пресечения циклических фиксаций
	 */
	protected $_listFix = array();
	
	/**
	 * @var array Ассоциативный массив параметров отправки отчётов о сбоях. Задаётся в формате:
	 * 	array(
	 * 		{Символьный идентификатор критической ошибки}=>{Время игнорирования повторной отправки в секундах},
	 * 	)
	 */
	protected $_reportParams = array(
		//'agent_request'=>2 * 60 * 60,
		'backup_log'=>0,
		'backup_appeal'=>0,
		'backup_user'=>0,
		'backup_export'=>0,
		'cleaning'=>0,
		'reserve_create'=>0
	);
	
	protected $_userNames = array();
	
	/**
	 * Инициализация компонента
	 */
	public function init(){
	}
	
	/**
	 * Фиксация события приложения
	 * @param string $event Символьный идентификатор события
	 * @param array $params Ассоциативный массив параметров события. По умолчанию равно array()
	 * @param string $error Символьный идентификатор ошибки события. По умолчанию равно "ок"
	 * @param boolean|integer|null $id_user Идентификатор пользователя-инициатора события. По умолчанию равно false.
	 * Если равно 0, инициатором будет являться "Система", null - "Гость", false - текущий пользователь 
	 * @throws CHttpException
	 */
	public function fix($event, $params = array(), $error = 'ok', $id_user = false){
		$key = serialize(array($event, array_slice($params, 0, 1)));
		$controller = Yii::app()->controller;
		$user = Yii::app()->user;
		
		if (!$controller || $controller->componentDef || isset($this->_listFix[$key]))
			return;
		else
			$this->_listFix[$key] = 0;
		
		$event_params = Yii::app()->event->get($event);
		$error_params = Yii::app()->errorEvent->get($error);
		$error_params['description'] = $this->preparationDesc($error_params['description'], $params);
		$success = $error === 'ok';
		$fix_log = $event_params['fix_log'] == 'required' ||
			($event_params['fix_log'] == 'by_success' && $success) || ($event_params['fix_log'] == 'by_error' && !$success);
		$fix_flash = !$error_params['exception'] && ($event_params['fix_flash'] == 'required' ||
			($event_params['fix_flash'] == 'by_success' && $success) || ($event_params['fix_flash'] == 'by_error' && !$success));
		$params_user_id = isset($params['user']) ? ($params['user'] instanceof UserModel ? $params['user']->id :
			Arrays::pop($params['user'], 'id')) : false;
		$id_user = $id_user !== false ? $id_user : ($params_user_id ? $params_user_id : ($user->isGuest ? null : $user->id));
		
		if (($id_user === null || !$controller->dbActive) && ($remote_addr = Html::getUserAddr()))
			$params['user_ip'] = $remote_addr;
		
		if ($fix_log){
			if ($controller->dbActive && Yii::app()->setting->get('log_db')){
				$model = $this->model(true);
				$model->id_user = $id_user;
				$model->date = Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM);
				$model->id_event = $event_params['id'];
				$model->id_error = $error_params['id'];
				
				if ($params)
					$model->params = json_encode($params);
				
				$model->save(false, null, false);
			}
			
			if (Yii::app()->setting->get('log_file'))
				$this->writeToFile($event_params['description'], $error_params['description'], false, $params, $id_user);
		}
		
		if ($fix_flash){
			if ($success)
				$description = Yii::app()->label->get('ok_'.$event);
			
			$user->setFlash($success ? 'ok' : 'error',
				empty($description) ? $error_params['description'] : $description);
		}
		
		if (!$success)
			$this->report($error_params, isset($model) ? $model->id : false);
		
		unset($this->_listFix[$key]);
		
		if ($error_params['exception'])
			if ($event_params['name'] == 'scheme_check_access' && $user->isGuest &&
				$controller->route !== Yii::app()->defaultController)
					$controller->redirect($controller->createUrl(Yii::app()->defaultController));
				else
					throw new CHttpException($error_params['exception'], $error_params['description']);
	}
	
	/**
	 * Фиксация события приложения в файле
	 * @param integer|string $event Цифровой идентификатор или описание события
	 * @param integer|string $error Цифровой идентификатор или описание ошибки
	 * @param boolean|integer|string $time Строка даты, штамп времени или false. По умолчанию равно false. Если равно false, то
	 * за основу возьмутся текущие дата и время
	 * @param array $params Ассоциативный массив параметров события. По умолчанию равно array()
	 * @param integer|null $id_user Идентификатор пользователя-инициатора события. По умолчанию равно null.
	 * Если равно 0, инициатором будет являться "Система", null - "Гость"
	 * @return boolean Успешность выполнения
	 */
	public function writeToFile($event, $error, $time = false, $params = array(), $id_user = null){
		$component_def_old = Yii::app()->controller->componentDef;
		Yii::app()->controller->componentDef = true;
		
		if (!is_string($event)){
			$events = Arrays::select(Yii::app()->event->get(), 'description', 'id');
			
			if (isset($events[$event]))
				$event = $events[$event];
		}
		
		if (!is_string($error)){
			$errors = Arrays::select(Yii::app()->errorEvent->get(), 'description', 'id');
			
			if (isset($errors[$error]))
				$error = $errors[$error];
		}
		
		$path = Yii::getPathOfAlias(self::DIR_LOG).DIRECTORY_SEPARATOR.sprintf(self::FN_LOG,
			Swamper::date($time, Swamper::FORMAT_DATE_SYSTEM_WITHOUT_TIME));
		$date = Swamper::date($time, Swamper::FORMAT_DATE_SYSTEM);
		$params_file = $params ? print_r($params, true) : '';
		$params_user_id = isset($params['user']) ? ($params['user'] instanceof UserModel ? $params['user']->id :
			Arrays::pop($params['user'], 'id')) : false;
		$user_name = '?';
		
		if ($id_user > 0){
			if (isset($this->_userNames[$id_user]))
				$user_name = $this->_userNames[$id_user];
			elseif ($id_user === Yii::app()->user->id && ($model_user = Yii::app()->user->getModel()))
				$user_name = $this->_userNames[$id_user] = Strings::userName($model_user);
			elseif ($params_user_id && $params_user_id === $id_user)
				$user_name = $this->_userNames[$id_user] = Strings::userName($params['user']);
			elseif ($model_user = UserModel::model()->findByPk($id_user))
				$user_name = $this->_userNames[$id_user] = Strings::userName($model_user);
			else
				$user_name = $this->_userNames[$id_user] = Yii::app()->label->get('user_system');
		}elseif ($id_user < 0)
			$user_name = Yii::app()->label->get('user_stranger');
		elseif ($id_user === null)
			$user_name = Yii::app()->authPhp->roles[Yii::app()->user->checkAccess('developer') ? 'developer' : 'guest']->description;
		elseif ($id_user === 0)
			$user_name = Yii::app()->label->get('user_system');
		
		Yii::app()->controller->componentDef = $component_def_old;
		
		return !!file_put_contents($path, $date.PHP_EOL.$user_name.PHP_EOL.$event.PHP_EOL.
				$error.PHP_EOL.$params_file.PHP_EOL.PHP_EOL, FILE_APPEND);
	}
	
	/**
	 * Подготовка к развёртыванию
	 * @param string $section Название группы таблиц
	 * @param string $type Символьный идентификатор типа предстоящего развёртывания
	 * @param array $params Ассоциативный массив с параметрами развёртывания приложения
	 * @throws CHttpException
	 */
	public function beforeDeployCompute($section, $type, $params){
		if ($type === 'standart')
			Files::clear(Yii::getPathOfAlias(static::DIR_LOG));
		elseif ($type === 'old'){
			$removed_users = $params['user']['type'] === 'old' ?
				Yii::app()->getComponent($params['auth']['component'])->removedUsers : false;
			
			if ($params['user']['type'] !== 'old' || $removed_users)
				if (!$this::model()->updateAll(array('id_user'=>-1), $removed_users ?
					new DbCriteria(array('in'=>array('id_user', $removed_users))) : 'id_user > 0'))
						throw new CHttpException(500);
			
			if ($params['directory']['type'] === 'standart' && !Yii::app()->table->dropForeignKeys($section))
				throw new CHttpException(500);
		}
		
		parent::beforeDeployCompute($section, $type, $params);
	}
	
	/**
	 * Развёртывание
	 * @param string $section Название группы таблиц
	 * @param string $type Символьный идентификатор типа развёртывания
	 * @param array $params Ассоциативный массив с параметрами развёртывания приложения
	 * @throws CHttpException
	 */
	public function deployCompute($section, $type, $params){
		parent::deployCompute($section, $type, $params);
		
		if ($type === 'old' && $params['directory']['type'] === 'standart' && !Yii::app()->table->addForeignKeys($section))
			throw new CHttpException(500);
	}
	
	/**
	 * Отправка разработчику отчёта о сбое
	 * @param array $error_params Ассоциативный массив с информацией об ошибке
	 * @param boolean|integer $id_event Идентификатор события с ошибкой в БД или false. По умолчанию равно false
	 */
	public function report($error_params, $id_event = false){
		if (isset($error_params['name'], $this->_reportParams[$error_params['name']]) && Yii::app()->controller->deployed &&
			Yii::app()->email->on && ($user = UserModel::model()->findByAttributes(array('login'=>'developer'))) &&
			($email = $user->email)){
				$path = Yii::getPathOfAlias(static::DIR_REPORT).DIRECTORY_SEPARATOR.static::FN_REPORT;
				$data = !is_file($path) ? array() : file($path, FILE_IGNORE_NEW_LINES);
				$time = time();
				$time_old = 0;
				
				foreach ($data as $key=>$string){
					$string = explode('-', $string);
					
					if (count($string) == 2 && $string[0] == $error_params['name']){
						$time_old = $string[1];
						unset($data[$key]);
						break;
					}
				}
				
				if ($time - $time_old > $this->_reportParams[$error_params['name']]){
					array_unshift($data, $error_params['name'].'-'.$time);
					file_put_contents($path, implode("\r\n", $data));
					Yii::app()->email->send($email, 'report', array('description'=>$error_params['description'],
						'link'=>$id_event ? Yii::app()->controller->createAbsoluteUrl('log/view', array('id'=>$id_event)) : false));
				}
			}
	}
	
	/**
	 * Подготовка сообщения об ошибке к выводу, подстановка в сообщение значений параметров из массива параметров.
	 * В сообщении названия параметров должны быть обрамлены в фигурные скобки 
	 * @param string $description Исходная строка сообщения об ошибке
	 * @param array $params Массив параметров события
	 * @return string Сообщение об ошибке с подставленными параметрами
	 */
	protected function preparationDesc($description, $params){
		preg_match_all('/\{(\w+)\}/u', $description, $matches);
		
		if (!empty($matches[1]))
			for ($count = 0; $count < count($matches[0]); $count++)
				if (isset($params[$matches[1][$count]]))
					$description = str_replace($params[$matches[0][$count]], $params[$matches[1][$count]], $description);
		
		return $description;
	}
	
}
