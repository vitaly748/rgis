<?php
/**
 * Компонент для работы с ресурсами приложения
 * @author v.zubov
 */
class Asset extends Component{
	
	/**
	 * @var string Название папки ресурсов
	 */
	const DIR_ASSETS = 'assets';
	
	/**
	 * @var string Название папки CSS-файлов внутри папки ресурсов
	 */
	const DIR_CSS = 'css';
	
	/**
	 * @var string Название папки JS-файлов внутри папки ресурсов
	 */
	const DIR_JS = 'js';
	
	/**
	 * @var string Название папки изображений внутри папки ресурсов
	 */
	const DIR_IMG = 'img';
	
	/**
	 * @var string Название папки представлений
	 */
	const DIR_VIEWS = 'views';
	
	/**
	 * @var string Название папки виджетов
	 */
	const DIR_WIDGETS = 'widgets';
	
	/**
	 * @var string Псевдоним дирректории ресурса в файле ресурса. В процессе подготовки файла будет замещён на реальное
	 * значение
	 */
	const ALIAS_ASSET = '$asset';
	
	/**
	 * @var integer Максимальная глубина просмотра стэка вызовов при поиске файла текущего представления
	 */
	public $maxDepthView = 3;
	
	/**
	 * @var boolean Признак замещения существующих ресурсов
	 */
	public $forceCopy = false;
	
	/**
	 * @var array Ассоциативный массив привязанных к текущему скрипту ресурсов в виде
	 * 	array(
	 * 		{Символьный идентификатор представления}=>array(
	 * 			{Символьный идентификатор ресурса}=>{Дирректория опубликованного ресурса},
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	private $_paths = array();
	
	/**
	 * Конструктор компонента
	 */
	public function init(){
	}
	
	/**
	 * Подключение ресурсов для текущего представления.
	 * Функция ищет файл текущего представления в стеке вызовов не глубже maxDepthView уровня.
	 * Ресурсы ищутся в папках "../assets" и "assets", относительно папки, содержащей текущее представление.
	 * По умолчанию подключаются ресурсы с именем текущего представления.
	 * Подключаемые ресурсы публикуются в папку assets основной дирректории приложения.
	 * @param array|boolean $additionals Список имён дополнительных ресурсов или false. По умолчанию равно false
	 * @param array|boolean $exceptions Список имён исключаемых ресурсов или false. По умолчанию равно false
	 * @return array|boolean Ассоциативный массив путей публикаций ресурсов в формате
	 * 	array(
	 *	 	{Символьный идентификатор ресурса}=>{Дирректория опубликованного ресурса},
	 *		...
	 *	)
	 */
	public function add($additionals = false, $exceptions = false){
		if ($file_view = $this->getFileView())
			$info = pathinfo($file_view);
		
		if (!isset($info))
			return false;
		
		$names[] = $info['filename'] != mb_strtolower($info['filename']) ?
			Strings::nameModify($info['filename']) : $info['filename'];
		
		$additionals = is_array($additionals) ? array_merge(array('main'), $additionals) : array('main');
		
		if (is_array($additionals))
			$names = array_merge($names, $additionals);
		
		$dirs = array(dirname($info['dirname']), $info['dirname']);
		
		foreach ($dirs as $key=>$dir)
			$dirs[$key] = $dir.'/'.self::DIR_ASSETS.'/';
		
		$assets_keys = array();
		$asset_manager = Yii::app()->assetManager;
		
		foreach (array_unique($names) as $name)
			foreach ($dirs as $dir){
				$dir .= $name;
				
				if (is_dir($dir)){
					$publish = is_dir($asset_manager->getPublishedPath($dir));
					
					if ($asset = Yii::app()->assetManager->publish($dir, false, -1, $this->forceCopy)){
						foreach (array(self::DIR_CSS, self::DIR_JS) as $dir_res){
							$dir_web = $asset.'/'.$dir_res;
							$dir_local = Yii::getPathOfAlias('webroot');
							
							if (Yii::app()->getBaseUrl())
								$dir_local = dirname($dir_local);
							
							$dir_local .= $dir_web;
							
							if (is_dir($dir_local)){
								foreach (glob($dir_local.'/*.*') as $fn){
									$info2 = pathinfo($fn);
									$path = $dir_web.'/'.$info2['basename'];
									
									if ($info2['extension'] === self::DIR_CSS)
										Yii::app()->clientScript->registerCssFile($path);
									elseif ($info2['extension'] === self::DIR_JS)
										Yii::app()->clientScript->registerScriptFile($path);
								}
								
								if ($dir_res === self::DIR_CSS && (!$publish || $this->forceCopy))
									$this->processingAsset($dir_local, $asset);
							}
						}
						
						$assets[$key = isset($assets_keys[$name]) ? $name.'2' : $name] = $asset;
						$assets_keys[] = $key;
					}
				}
			}
		
		if (!empty($assets)){
			$view_name = Strings::getFileNameFromPath($file_view);
			
			if (isset($this->_paths[$view_name]))
				$view_name .= '2';
			$this->_paths[$view_name] = $assets;
			
			return $assets;
		}else
			return false;
	}
	
	/**
	 * Метод-геттер для определения _paths
	 * @param boolean|string $view_name Символьный идентификатор представления или false. По умолчанию равно false.
	 * Если равен false, то будет использовано текущее представление
	 * @return array|boolean Ассоциативный массив путей публикаций ресурсов для заданного представления
	 */
	public function getPaths($view_name = false){
		if (!$view_name)
			$view_name = Strings::getFileNameFromPath($this->getFileView());
		return isset($this->_paths[$view_name]) ? $this->_paths[$view_name] : false;
	}
	
	/**
	 * Определение дирректории файла текущего представления
	 * @return boolean|string Дирректория файла представления или false в случае ошибки
	 */
	private function getFileView(){
		$trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
		
		for ($count = 0; $count < min(count($trace), $this->maxDepthView); $count++){
			$file = $trace[$count]['file'];
			
			if (mb_strpos($file, self::DIR_VIEWS) !== false || mb_strpos($file, self::DIR_WIDGETS) !== false)
				return $file;
		}
		
		return false;
	}
	
	/**
	 * Обработка файлов ресурсов в заданной папке перед их публикацией, подстановка реальных значений вместо
	 * псевдонимов ALIAS_ASSET 
	 * @param sting $dir_local Локальный адрес каталога ресурсов
	 * @param string $dir_web Внешний адрес каталога ресурсов
	 */
	private function processingAsset($dir_local, $dir_web){
		$needle = array(self::ALIAS_ASSET);
		$replace = array($dir_web);
		
		foreach (scandir($dir_local) as $fn)
			if ($fn != '.' && $fn != '..'){
				$path = $dir_local.'/'.$fn;
				if (is_dir($path))
					$this->processingAsset($path, $dir_web);
				else{
					$content = str_replace($needle, $replace, file_get_contents($path));
					file_put_contents($path, $content);
				}
			}
	}
	
}
