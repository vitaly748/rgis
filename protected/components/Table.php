<?php
/**
 * Компонент для работы с табличными схемами в БД
 * @author v.zubov
 */
class Table extends Component{
	
	/**
	 * @var string Псевдоним пути к файлу конфигурации
	 */
	const DIR_CFG = 'application.config';
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'table.php';
	
	/**
	 * @var integer Код зоны таблиц "Таблицы приложения"
	 */
	const TABLE_AREA_PROJECT = 0;
	
	/**
	 * @var integer Код зоны таблиц "Сторонние таблицы"
	 */
	const TABLE_AREA_OTHER = 1;
	
	/**
	 * @var integer Код зоны таблиц "Все таблицы"
	 */
	const TABLE_AREA_ALL = 2;
	
	/**
	 * @var string Разделитель функциональных строк при записи в одно поле
	 */
	const SEPARATOR_WORDS = '|';
	
	/**
	 * @var string Символ, обозначающий выбор всех атрибутов таблицы при поисковых запросах
	 */
	const SELECT_ALL = '*';
	
	/**
	 * @var array Конфигурация табличной схемы приложения в виде ассоциативного массива
	 */
	private $_cfg;
	
	/**
	 * @var object Экземпляр активного объекта подключения к БД
	 */
	private $_client;
	
	/**
	 * @var array Двухуровневый массив, определяющий группировки таблиц. Задаётся в формате:
	 * 	array(
	 * 		{Название группы}=>array(
	 * 			{Название таблицы, входящей в данную группу},
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	private $_groups;
	
	/**
	 * @var array Ассоциативный массив проверок таблиц
	 * @see Table::checkTable()
	 */
	private $_checkTables = array();
	
	/**
	 * Конструктор компонента
	 */
	public function init(){
		$this->getCfgFromFile();
		$this->_client = Yii::app()->db;
	}
	
	/**
	 * Метод-геттер для определения _cfg
	 * @return array|null Массив параметров конфигурации или null
	 */
	public function getCfg(){
		return $this->_cfg;
	}
	
	/**
	 * Метод-геттер для определения _groups
	 * @return array|null Массив группировок таблиц или null
	 */
	public function getGroups(){
		return $this->_groups;
	}
	
	/**
	 * Метод-геттер для определения _client
	 * @return null|object Экземпляр объекта подключения к БД или null
	 */
	public function getClient(){
		return $this->_client;
	}
	
	/**
	 * Метод-сеттер для установки _client
	 * @param CDbConnection $client Экземпляр объекта подключения к БД
	 */
	public function setClient($client){
		if ($client instanceof CDbConnection)
			$this->_client = $client;
	}
	
	/**
	 * Получение списка названий всех таблиц приложения, найденных в БД
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return array|boolean Список названий таблиц или false
	 */
	public function tableExistingNames($fix = true){
		$sql = "
			SELECT t.table_name
			FROM information_schema.tables t
			WHERE t.table_schema = 'public'
		";
		
		if ($this->client)
			if ($names = $this->client->createCommand($sql)->queryColumn(array(), $fix)){
				$prefix = $this->client->tablePrefix;
				$len = mb_strlen($prefix);
				
				foreach ($names as $key=>$name)
					if (mb_strpos($name, $prefix) === 0)
						$names[$key] = mb_substr($name, $len);
				
				return $names;
			}
		
		return false;
	}
	
	/**
	 * Проверка существования таблицы
	 * @param string $table Название таблицы
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Признак существования таблицы
	 */
	public function tableExists($table, $fix = true){
		$sql = "
			SELECT t.table_name
			FROM information_schema.tables t
			WHERE t.table_schema = 'public' AND t.table_name = '{{{$table}}}'
		";
		
		if (!$this->client)
			return false;
		
		return (boolean)$this->client->createCommand($sql)->queryColumn(array(), $fix);
	}
	
	/**
	 * Создание таблицы
	 * @param string $table Название таблицы
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 */
	public function createTable($table, $fix = true){
		if ($this->client)
			if ($columns = $this->getCfgTable($table)){
				foreach ($columns as $column=>$params){
					if ($params['primary'])
						$keys_primary[] = $column;
					
					if ($params['foreign_table'])
						$keys_foreign[] = "CONSTRAINT ".$this->getForeignKeys($table, $column)." FOREIGN KEY ($column) ".
							"REFERENCES {{{$params['foreign_table']}}} ({$params['foreign_name']}) ".
							"ON UPDATE {$params['update']} ON DELETE {$params['delete']}";
					
					$columns[$column] = $params['type'].($params['len'] ? '('.$params['len'].')' : '').
						($params['not_null'] ? ' NOT NULL' : '');
				}
				
				if (!empty($keys_primary))
					$columns[] = 'CONSTRAINT '.$this->getPrimaryKey($table).' PRIMARY KEY ('.
						implode(', ', $keys_primary).')';
				
				if (!empty($keys_foreign))
					foreach ($keys_foreign as $key)
						$columns[] = $key;
				
				return $this->client->createCommand()->createTable($table, $columns, null, $fix);
			}
		
		return false;
	}
	
	/**
	 * Создание групп таблиц
	 * @param array|string $groups Название группы или список названий групп таблиц
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 */
	public function createGroupsTables($groups, $fix = true){
		foreach (is_array($groups) ? $groups : array($groups) as $group){
			if (!$tables = $this->_groups[$group])
				return false;
			
			foreach ($tables as $table)
				if (!$this->createTable($table, $fix))
					return false;
		}
		
		return true;
	}
	
	/**
	 * Определение названия первичного ключа таблицы
	 * @param string $table Название таблицы
	 * @return string Название ключа
	 */
	public function getPrimaryKey($table){
		return "pk_{{{$table}}}";
	}
	
	/**
	 * Определение названий внешних ключей таблицы
	 * @param string $table Название таблицы
	 * @param string $column Название столбца таблицы, для которого нужно определить внешний ключ или false.
	 * По умолчанию равно false. Если равно false, то будут определены все зарегистрированные внешние ключи таблицы
	 * @return array|string Название ключа или список названий ключей
	 */
	public function getForeignKeys($table, $column = false){
		$keys_foreign = array();
		
		if ($column)
			$keys_foreign[] = $column;
		else{
			if (!$columns = $this->getCfgTable($table))
				return false;
			
			foreach ($columns as $key=>$params)
				if ($params['foreign_table'])
					$keys_foreign[] = $key;
		}
		
		foreach ($keys_foreign as $key=>$value)
			$keys_foreign[$key] = "fk_{{{$table}}}_$value";
		
		return $column ? $keys_foreign[0] : $keys_foreign;
	}
	
	/**
	 * Удаление таблицы
	 * @param string $table Название таблицы
	 * @param boolean $check_exists Признак необходимости предварительной проверки существования таблицы.
	 * По умолчанию равно true
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 */
	public function dropTable($table, $check_exists = true, $fix = true){
		if (!$exists = !$check_exists || $this->tableExists($table, $fix))
			return true;
		
		if (!$this->client)
			return false;
		
		return $this->client->createCommand()->dropTable($table, $fix);
	}
	
	/**
	 * Удаление групп таблиц
	 * @param array|string $groups Название группы или список названий групп таблиц
	 * @param boolean $check_exists Признак необходимости предварительной проверки существования таблиц.
	 * По умолчанию равно true
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 */
	public function dropGroupsTables($groups, $check_exists = true, $fix = true){
		foreach (is_array($groups) ? array_reverse($groups) : array($groups) as $group){
			if (!$tables = $this->_groups[$group])
				return false;
			
			foreach (array_reverse($tables) as $table)
				if (!$this->dropTable($table, $check_exists, $fix))
					return false;
		}
		
		return true;
	}
	
	/**
	 * Удаление таблиц
	 * @param integer $area Код зоны воздействия: "Таблицы приложения", "Сторонние таблицы" или "Все таблицы".
	 * По умолчанию равно "Все таблицы"
	 * @param boolean $setting_save Признак необходимости сохранения таблицы настроек. По умолчанию равно false
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 */
	public function dropAreaTables($area = self::TABLE_AREA_ALL, $setting_save = false, $fix = true){
		if ($area !== self::TABLE_AREA_OTHER){
			$groups = array_keys($this->get(true, false, $setting_save ? array('setting') : array()));
			
			if (!$this->dropGroupsTables($groups, true, $fix))
				return false;
		}
		
		if ($area === self::TABLE_AREA_PROJECT || (!$table_existing_names = $this->tableExistingNames($fix)))
			return true;
		
		$tables = $this->get();
		
		foreach ($table_existing_names as $table_existing_name)
			if (!in_array($table_existing_name, $tables) && !$this->dropTable($table_existing_name, false, $fix))
				return false;
		
		return true;
	}
	
	/**
	 * Установка внешних ключей таблицы
	 * @param array|string $tables Название таблицы или список названий таблиц
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 */
	public function addForeignKeys($tables, $fix = true){
		if (!$this->client)
			return false;
		
		$command = $this->client->createCommand();
		
		foreach (is_array($tables) ? $tables : array($tables) as $table){
			if (!$columns = $this->getCfgTable($table))
				return false;
			
			foreach ($columns as $column=>$params)
				if ($params['foreign_table'])
					if (!$command->addForeignKey($this->getForeignKeys($table, $column), $table, $column,
						$params['foreign_table'], $params['foreign_name'], $params['delete'], $params['update'], $fix))
							return false;
		}
		
		return true;
	}
	
	/**
	 * Сброс внешних ключей таблицы
	 * @param array|string $tables Название таблицы или список названий таблиц
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 */
	public function dropForeignKeys($tables, $fix = true){
		if (!$this->client)
			return false;
		
		$command = $this->client->createCommand();
		
		foreach (is_array($tables) ? $tables : array($tables) as $table)
			if ($keys = $this->getForeignKeys($table))
				foreach ($keys as $key)
					if (!$command->dropForeignKey($key, $table, $fix))
						return false;
		
		return true;
	}
	
	/**
	 * Проверка таблицы БД на соответствие её описанию в конфигурационном файле
	 * @param string $table Название таблицы
	 * @param boolean $refresh Признак необходимости обновления результата в случае, если он уже занесён в буфер. По умолчанию
	 * равно false
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean|integer Успешность проверки. Если равно false, значит таблица не существует. Если равно 0 - таблица
	 * существует, но не соответствует конфигурации
	 */
	public function checkTable($table, $refresh = false, $fix = true){
		if (isset($this->_checkTables[$table]) && !$refresh)
			$check = $this->_checkTables[$table];
		else{
			if (!$cfg = $this->getCfgTable($table))
				$check = 0;
			elseif ($info = $this->getInfoTable($table, $fix)){
				$check = true;
				
				foreach (array_values($cfg) as $key_col=>$col_params)
					if (!isset($info[$key_col]) || $col_params['name'] !== $info[$key_col]['name'] ||
						mb_strtolower($col_params['type']) !== $info[$key_col]['type'] || $col_params['len'] !== $info[$key_col]['len'] ||
						$col_params['not_null'] !== $info[$key_col]['not_null'])
							$check = 0;
			}else
				$check = false;
			
			$this->_checkTables[$table] = $check;
		}
		
		return $check;
	}
	
	/**
	 * Проверка соответствия текущей табличной схемы в БД конфигурационным данным
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return array|boolean Ассоциативный массив с информацией о корректности групп табличной схемы или false в
	 * случае ошибки
	 */
	public function checkTables($fix = true){
		$state_tables = array();
		
		try{
			$groups = $this->get(true, false, Yii::app()->controller->id === 'deploy' ? array('setting') : array());
			
			foreach ($groups as $group=>$tables)
				if (property_exists('DataModel', $group)){
					$state_tables[$group] = true;
					
					foreach ($tables as $table)
						if (!$this->checkTable($table, false, $fix)){
							$state_tables[$group] = false;
							break;
						}
				}
		}catch (CException $e){
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_check_tables', array('state'=>$state_tables), $ok ? 'ok' : 'db_check_tables');
		
		return $ok ? $state_tables : false;
	}
	
	/**
	 * Генерация данных
	 * @param DataModel $model Модель данных с параметрами развёртывания приложения
	 * @param array $state_tables Ассоциативный массив с информацией о корректности групп табличной схемы
	 * @return boolean Успешность выполнения
	 */
	public function deploy($model, $state_tables){
		try{
			if (!$model instanceof DataModel || !$state_tables || !$this->client)
				throw new CHttpException(500);
			
			$group_components = array('auth'=>'authDb', 'message'=>'notification');
			$install_types = Yii::app()->conformity->get('table_group_install_type', 'name', 'code');
			$params_fix = array('generation'=>$model->generation, 'quantity'=>$model->quantity);
			
			foreach ($state_tables as $name=>$check)
				$params_fix[$name] = array(
					'type'=>$install_types[$model->$name],
					'component'=>isset($group_components[$name]) ? $group_components[$name] : $name,
					'check'=>$check
				);
			
			$transaction = $this->client->beginTransaction();
			set_time_limit(0);
// 			$this->dropAreaTables(self::TABLE_AREA_OTHER);
			
			foreach (array_reverse($state_tables) as $name=>$check)
				if (!Yii::app()->$params_fix[$name]['component']->beforeDeploy($name, $params_fix[$name]['type'], $params_fix))
					throw new CHttpException(500);
			
			foreach ($state_tables as $name=>$check)
				if (!Yii::app()->$params_fix[$name]['component']->deploy($name, $params_fix[$name]['type'], $params_fix))
					throw new CHttpException(500);
			
			$transaction->commit();
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
			
			if (!empty($transaction))
				$transaction->rollback();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('deploy_data', $params_fix, $ok ? 'ok' : 'deploy_data');
		
		return $ok;
	}
	
	/**
	 * Возвращает выборочно список таблиц или их групп
	 * @param boolean $with_groups Признак выборки групп таблиц. По умолчанию равно false
	 * @param boolean $detail Признак возвращения подробной информации. По умолчанию равно false. Задаётся в формате
	 * 	array(
	 * 		{Название группы или таблицы}=>{Параметры},
	 * 		...
	 * 	)
	 * Если равно false, то возвратит просто список названий
	 * @param array $exception_groups Список названий исключаемых групп таблиц. По умолчанию равно array()
	 * @param array $exception_tables Список названий исключаемых таблиц. По умолчанию равно array()
	 * @return array Список выборочной информации о группах таблиц или таблицах приложения
	 */
	public function get($with_groups = false, $detail = false, $exception_groups = array(), $exception_tables = array()){
		$groups = $this->_groups;
		
		foreach ($groups as $group=>$group_tables)
			if (in_array($group, $exception_groups))
				unset($groups[$group]);
			else{
				$tables_detail = array();
				
				foreach ($group_tables as $table_key=>$table_name)
					if (in_array($table_name, $exception_tables))
						unset($groups[$group][$table_key]);
					elseif ($detail)
						$tables_detail[$table_name] = $this->_cfg[$table_name];
				
				if ($tables_detail)
					$groups[$group] = $tables_detail;
			}
		
		if ($with_groups)
			return $groups;
		
		$tables = array();
		
		foreach ($groups as $group_tables)
			if ($detail)
				$tables += $group_tables;
			else
				$tables = array_merge($tables, $group_tables);
		
		return $tables;
	}
	
	/**
	 * Получение параметров таблицы из конфигурации
	 * @param string $table Название таблицы
	 * @return array|boolean Ассоциативный массив параметров таблицы или false
	 */
	public function getCfgTable($table){
		if ($cfg = $this->_cfg)
			if (!empty($cfg[$table])){
				$params = array();
				
				foreach ($cfg[$table] as $col_name=>$col_params){
					preg_match('/^(?:\s*([\w\s]+?)\s*|\s*([\w\s]+?)\s*\((\d+)\).*)$/u',
						is_array($col_params) ? $col_params['type'] : $col_params, $matches);
					$type = count($matches) == 2 ? $matches[1] : $matches[2];
					
					$params[$col_name] = array(
						'name'=>$col_name,
						'type'=>$type === 'pk' ? 'SERIAL' : $type,
						'len'=>isset($col_params['len']) ? $col_params['len'] :
							(count($matches) == 4 ? (int)$matches[3] : null),
						'primary'=>isset($col_params['primary']) ? $col_params['primary'] : $type === 'pk',
						'not_null'=>isset($col_params['not_null']) ? $col_params['not_null'] : $type === 'pk',
						'foreign_table'=>isset($col_params['foreign_table']) ? $col_params['foreign_table'] : false,
						'foreign_name'=>isset($col_params['foreign_name']) ? $col_params['foreign_name'] : 'id',
						'update'=>isset($col_params['update']) ? mb_strtoupper($col_params['update']) : 'CASCADE',
						'delete'=>isset($col_params['delete']) ? mb_strtoupper($col_params['delete']) : 'CASCADE'
					);
				}
				
				return $params;
			}
		
		return false;
	}
	
	/**
	 * Получение и обработка параметров табличной схемы приложения из конфигурационного файла
	 */
	private function getCfgFromFile(){
		$path = Yii::getPathOfAlias(self::DIR_CFG).'/'.self::FN_CFG;
		
		if (!is_file($path))
			return;
		
		$data = require $path;
		
		if (!$data || !is_array($data))
			return;
		
		$this->_cfg = $this->_groups = array();
		
		foreach ($data as $group=>$tables)
			foreach ($tables as $table=>$params){
				$this->_cfg[$table] = $params;
				$this->_groups[$group][] = $table;
			}
	}
	
	/**
	 * Получение параметров таблицы БД
	 * @param string $table Название таблицы
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return array|boolean Ассоциативный массив параметров таблицы или false
	 */
	private function getInfoTable($table, $fix = true){
		$sql = "
			SELECT c.column_name AS name, c.data_type AS type, c.character_maximum_length AS len,
				c.column_default AS default, c.is_nullable AS is_nullable
			FROM information_schema.columns c
			WHERE c.table_schema = 'public' AND c.table_name = '{{{$table}}}'
			ORDER BY c.ordinal_position
		";
		
		if (!$this->client)
			return false;
		
		if (!$rows = $this->client->createCommand($sql)->queryAll(true, array(), $fix))
			return false;
		
		foreach ($rows as $key=>$row){
			if (mb_strpos($row['default'], 'nextval') === 0)
				$rows[$key]['type'] = 'serial';
			
			$rows[$key]['not_null'] = $rows[$key]['is_nullable'] === 'NO';
		}
		
		return $rows;
	}
	
}
