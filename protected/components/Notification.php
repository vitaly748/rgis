<?php
/**
 * Компонент для работы с системой уведомлений пользователей
 * @author v.zubov
 */
class Notification extends ComponentDeploy{
	
	/**
	 * Конструктор компонента
	 */
	public function init(){
	}
	
	/**
	 * Подготовка к развёртыванию
	 * @param string $section Название группы таблиц
	 * @param string $type Символьный идентификатор типа предстоящего развёртывания
	 * @param array $params Ассоциативный массив с параметрами развёртывания приложения
	 * @throws CHttpException
	 */
	public function beforeDeployCompute($section, $type, $params){
		if ($type === 'old'){
			$removed_users = ($user_old = $params['user']['type'] === 'old') ?
				Yii::app()->getComponent($params['auth']['component'])->removedUsers : false;
			
			if (!$user_old || $removed_users){
				$attributes = array('chat'=>array('id_owner'), 'chat_reader'=>array('id_user'),
					'chat_message'=>array('id_owner', 'id_addressee'));
				
				foreach (Yii::app()->table->groups[$section] as $table)
					if (isset($attributes[$table])){
						$model = call_user_func(Strings::nameModify($table, Strings::FORMAT_NAME_MODIFY_UPPER, true, true).'::model');
						
						foreach ($attributes[$table] as $attribute)
							if (!$model->updateAll(array($attribute=>null), $removed_users ?
								new DbCriteria(array('in'=>array($attribute, $removed_users))) : ''))
									throw new CHttpException(500);
					}
			}
			
			if (!$user_old){
				$drop_keys = array('chat', 'chat_reader', 'chat_message');
				
				if (!Yii::app()->table->dropTable('notification'))
					throw new CHttpException(500);
			}elseif ($params['directory']['type'] === 'standart')
				$drop_keys[] = 'notification';
			
			if (isset($drop_keys) && !Yii::app()->table->dropForeignKeys(array_unique($drop_keys)))
				throw new CHttpException(500);
		}
		
		parent::beforeDeployCompute($section, $type, $params);
	}
	
	/**
	 * Развёртывание
	 * @param string $section Название группы таблиц
	 * @param string $type Символьный идентификатор типа развёртывания
	 * @param array $params Ассоциативный массив с параметрами развёртывания приложения
	 * @throws CHttpException
	 */
	public function deployCompute($section, $type, $params){
		parent::deployCompute($section, $type, $params);
		
		if ($type === 'old'){
			if ($params['user']['type'] !== 'old'){
				$add_keys = array('chat', 'chat_reader', 'chat_message');
				
				if (!Yii::app()->table->createTable('notification'))
					throw new CHttpException(500);
			}elseif ($params['directory']['type'] === 'standart')
				$add_keys[] = 'notification';
			
			if (isset($add_keys) && !Yii::app()->table->addForeignKeys(array_unique($add_keys)))
				throw new CHttpException(500);
		}
	}
	
}
