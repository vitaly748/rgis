<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CActiveDataProvider. В отличие от родителя в основе расчётов лежит
 * не CDbCriteria, а CDbCommand
 * @author v.zubov
 * @see CActiveDataProvider
 */
class CommandDataProvider extends CActiveDataProvider{
	
	/**
	 * @var string Имя параметра записи, содержащего её идентификатор
	 * @see CArrayDataProvider::keyField
	 */
	public $keyField = 'id';
	
	/**
	 * @var CDbCommand Сформированные параметры поиска записей
	 * @see CActiveDataProvider::_criteria
	 */
	private $_criteria;
	
	/**
	 * @var CDbCommand Сформированные параметры подсчёта количества записей
	 * @see CActiveDataProvider::_countCriteria
	 */
	private $_countCriteria;
	
	/**
	 * Переопределение стандартного метода getCriteria, возвращающего критерии поиска записей
	 * @return CDbCommand Сформированные параметры поиска записей
	 * @see CActiveDataProvider::getCriteria()
	 */
	public function getCriteria(){
		return $this->_criteria;
	}
	
	/**
	 * Переопределение стандартного метода setCriteria, устанавливающего критерии поиска записей
	 * @param CDbCommand $value Сформированные параметры поиска записей
	 * @see CActiveDataProvider::setCriteria()
	 */
	public function setCriteria($value){
		$this->_criteria = $value instanceof CDbCommand ? $value : null;
	}
	
	/**
	 * Переопределение стандартного метода getCountCriteria, возвращающего критерии подсчёта количества записей
	 * @return CDbCommand Сформированные параметры подсчёта количества записей
	 * @see CActiveDataProvider::getCountCriteria()
	 */
	public function getCountCriteria(){
		if ($this->_countCriteria === null && $this->criteria)
			$this->countCriteria = clone $this->criteria;
		
		return $this->_countCriteria;
	}
	
	/**
	 * Переопределение стандартного метода setCountCriteria, устанавливающего критерии подсчёта количества записей
	 * @param CDbCommand $value Сформированные параметры подсчёта количества записей
	 * @see CActiveDataProvider::setCountCriteria()
	 */
	public function setCountCriteria($value){
		$this->_countCriteria = null;
		
		if ($value instanceof CDbCommand){
			$this->_countCriteria = $value;
			$this->_countCriteria->select = 'COUNT(*)';
		}
	}
	
	/**
	 * Переопределение стандартного метода fetchData, осуществляющего поиск записей, удовлетворяющих условиям поиска
	 * @return array Список найденных записей
	 * @see CActiveDataProvider::fetchData()
	 */
	protected function fetchData(){
		if ($pagination = $this->pagination){
			$pagination->setItemCount($this->getTotalItemCount());
			
			if ($this->criteria)
				$pagination->applyLimit($this->criteria);
		}
		
		if (($sort = $this->sort) && $this->criteria){
			$sort->applyOrder($this->criteria);
			$this->criteria->order = str_replace('"', '', $this->criteria->order);
		}
		
		return $this->criteria ? $this->criteria->queryAll() : array();
	}
	
	/**
	 * Переопределение стандартного метода fetchKeys, определяющего идентификаторы найденных записей
	 * @return array Список идентификаторов
	 * @see CActiveDataProvider::fetchKeys()
	 */
	protected function fetchKeys(){
		if ($this->keyField === false)
			return array_keys($this->rawData);
		
		$keys = array();
		
		foreach ($this->getData() as $i=>$data)
			$keys[$i] = is_object($data) ? $data->{$this->keyField} : $data[$this->keyField];
		
		return $keys;
	}
	
	/**
	 * Переопределение стандартного метода calculateTotalItemCount, определяющего общее количество записей, удовлетворяющих
	 * условиям поиска
	 * @return integer Количество записей
	 * @see CActiveDataProvider::calculateTotalItemCount()
	 */
	protected function calculateTotalItemCount(){
		return $this->countCriteria ? (int)$this->countCriteria->queryScalar() : 0;
	}
	
}
