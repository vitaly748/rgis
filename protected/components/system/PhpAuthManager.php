<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CPhpAuthManager
 * @author v.zubov
 * @see CPhpAuthManager
 */
class PhpAuthManager extends CPhpAuthManager{
	
	/**
	 * @var string Псевдоним пути к файлу конфигурации
	 */
	const DIR_CFG = 'application.config.default';
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'auth.php';
	
	/**
	 * @var array Данные из конфигурационного файла
	 */
	private $_cfg;
	
	/**
	 * Конструктор компонента
	 * @see CPhpAuthManager::init()
	 */
	public function init(){
		if ($this->authFile === null)
			$this->authFile = Yii::getPathOfAlias(self::DIR_CFG).'/'.self::FN_CFG;
		
		parent::init();
	}
	
	/**
	 * Метод-геттер для определения _cfg
	 * @return array Данные из конфигурационного файла
	 */
	public function getCfg(){
		if ($this->_cfg === null){
			$data = array();
			
			if (is_file($this->authFile)){
				$data = require($this->authFile);
				
				foreach ($data as $name=>$features)
					if (is_int($features))
						$data[$name] = array('type'=>$features, 'description'=>null, 'bizRule'=>null, 'data'=>null);
			}
			
			$this->_cfg = $data;
		}
		
		return $this->_cfg;
	}
	
	/**
	 * Переопределение стандартного метода executeBizRule, осуществляющего проверку бизнес-правил
	 * @param string $bizRule Бизнес-правило для выполнения
	 * @param array $params Список параметров бизнес-правила
	 * @param mixed $data Дополнительные параметры
	 * @see CAuthManager::executeBizRule()
	 * @return boolean Успешность выполнения проверки
	 */
	public function executeBizRule($bizRule, $params, $data){
		return parent::executeBizRule($bizRule ? "return $bizRule;" : $bizRule, $params, $data);
	}
	
	/**
	 * Переопределение стандартного метода loadFromFile, осуществляющего загрузку конфигурационного массива из файла
	 * @return array Конфигурационный массив
	 * @see CPhpAuthManager::loadFromFile()
	 */
	protected function loadFromFile($file){
		return $this->cfg;
	}
	
}
