<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CPgsqlCommandBuilder
 * @author v.zubov
 * @see CPgsqlCommandBuilder
 */
class PgsqlCommandBuilder extends CPgsqlCommandBuilder{
	
	/**
	 * Переопределение стандартного метода getLastInsertID, возвращающего первичный ключ последней вставленной записи
	 * @param mixed $table Название таблицы
	 * @return mixed Первичный ключ
	 * @see CDbCommandBuilder::getLastInsertID() 
	 */
	public function getLastInsertID($table) {
		$id = parent::getLastInsertID($table);
		
		if (is_numeric($id))
			$id = (int)$id;
		
		return $id;
	}
	
}
