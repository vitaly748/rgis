<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CDummyCache
 * @author v.zubov
 * @see CDummyCache
 */
class DummyCache extends CDummyCache{
	
	/**
	 * Расширенный вариант метода get, подготавливающего получение данных из кэша, с указанием типа данных
	 * @param string $type Тип кэшируемых данных
	 * @param string $id Ключ, идентифицирующий данные
	 * @return mixed Данные из кэша
	 * @see CDummyCache::get()
	 */
	public function getExt($type, $id){
		return parent::get($id);
	}
	
	/**
	 * Расширенный вариант метода set, подготавливающего сохранение данных в кэше, с указанием типа данных
	 * @param string $type Тип кэшируемых данных
	 * @param string $id Ключ, идентифицирующий данные
	 * @param mixed $value Данные
	 * @param integer $expire Время хранения данных в секундах. По умолчанию равно 0
	 * @param ICacheDependency $dependency Зависимость
	 * @return boolean Успешность выполнения
	 * @see CDummyCache::set()
	 */
	public function setExt($type, $id, $value, $expire = 0, $dependency = null){
		return parent::set($id, $value, $expire, $dependency);
	}
	
	/**
	 * Расширенный вариант метода delete, подготавливающего удаление данных из кэша, с указанием типа данных
	 * @param string $type Тип кэшируемых данных
	 * @param string $id Ключ, идентифицирующий данные
	 * @return boolean Успешность выполнения
	 * @see CDummyCache::delete()
	 */
	public function deleteExt($type, $id){
		return parent::delete($id);
	}
	
}
