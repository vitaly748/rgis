<?php
/**
 * Базовый класс для компонентов-справочников
 * @author v.zubov
 */
class ComponentDirectory extends ComponentDeploy{
	
	/**
	 * @var string Псевдоним пути к файлу конфигурации
	 */
	const DIR_CFG = 'application.config.default.directory';
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = null;
	
	/**
	 * @var string Префикс названий таблиц справочников
	 */
	const PREFIX_TABLES = 'directory_';
	
	/**
	 * @var array Данные, полученные из конфигурационных файлов
	 */
	protected $_dataDef;
	
	/**
	 * @var array Данные, полученные из БД
	 */
	protected $_data;
	
	/**
	 * @var boolean Признак использования данных по умолчанию
	 */
	private $_componentDef = false;
	
	/**
	 * Конструктор компонента
	 */
	public function init(){
	}
	
	/**
	 * Выборка и получение данных
	 * @param [mixed] Ключ для фильтрации очередного уровня массива справочных данных или false
	 * @return array|boolean Выбранные данные или false
	 * @see Arrays::getSelective()
	 */
	public function get(){
		return Arrays::getSelective($this->getData(), func_get_args());
	}
	
	/**
	 * Метод-геттер для получения актуальных данных
	 * @return Спавочные данные
	 */
	protected function getData(){
		if ($this->_data === null){
			$params_fix = array('name'=>Strings::nameModify(get_class($this)), 'def'=>$def = Yii::app()->controller->componentDef ||
				$this->_componentDef || !Yii::app()->controller->dbActive);
			$fix = $this->_dataDef === null || !$def;
			
			try{
				if ($def){
					if ($this->_dataDef === null)
						$this->_dataDef = $this->preparationDataDef();
				}else{
					$this->_componentDef = true;
					$this->_data = $this->preparationData();
					$this->_componentDef = false;
				}
				
				$data = $def ? $this->_dataDef : $this->_data;
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e) && $data;
			
			if ($fix)
				Yii::app()->log->fix('directory_init', $params_fix, $ok ? 'ok' : 'directory_init');
			
			return $data;
		}
		
		return $this->_data;
	}
	
	/**
	 * Подготовка данных из конфигурационных файлов
	 * @return array|null Массив данных справочника
	 */
	protected function preparationDataDef(){
		$path = Yii::getPathOfAlias(self::DIR_CFG).'/'.static::FN_CFG;
		
		if (is_file($path))
			$cfg = require $path;
		
		return !empty($cfg) ? $this->preparation($cfg) : null;
	}
	
	/**
	 * Подготовка данных из БД
	 * @param array|boolean|string $with Название или список названий символьных идентификаторов табличных связей,
	 * участвующих в запросе на получение данных или false. По умолчанию равно false
	 * @return array|null Массив данных справочника или null
	 */
	protected function preparationData($with = false){
		if (!$model = $this->model())
			return null;
		
		if ($with)
			$model->with($with);
		
		$rows = $model->findAll(array('order'=>'t.id'));
		
		return $rows ? $this->preparation($rows) : null;
	}
	
	/**
	 * Обработка исходных данных справочника. Заглушка.
	 * @param array $data Набор исходных данных
	 * @return mixed Массив данных справочника
	 */
	protected function preparation($data){
		return $data;
	}
	
	/**
	 * Развёртывание
	 * @param string $section Название группы таблиц
	 * @param string $type Символьный идентификатор типа развёртывания
	 * @param array $params Ассоциативный массив с параметрами развёртывания приложения
	 * @throws CHttpException
	 */
	public function deployCompute($section, $type, $params){
		parent::deployCompute($section, $type, $params);
		
		if ($type === 'standart'){
			$prefix_len = mb_strlen(self::PREFIX_TABLES);
			
			foreach (Yii::app()->table->groups[$section] as $table){
				$component = Strings::nameModify(mb_substr($table, $prefix_len), Strings::FORMAT_NAME_MODIFY_UPPER, false);
				
				if (Yii::app()->hasComponent($component) && !Yii::app()->$component->deploy($table))
					throw new CHttpException(500);
			}
		}
	}
	
}
