<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CUrlManager
 * @author v.zubov
 * @see CUrlManager
 */
class UrlManager extends CUrlManager{
	
	/**
	 * @var array Список подготовленных правил проверки
	 */
	protected $_rules = array();
	
	/**
	 * Переопределение стандартного метода addRules, добавляющего в буфер правила проверки URL
	 * @param array $rules Ассоциативный массив новых правил проверки
	 * @param boolean $append Признак добавления правил в конец уже существующего списка. По умолчанию равно true
	 * @see CUrlManager::addRules()
	 */
	public function addRules($rules, $append = true){
		if ($append)
			foreach ($rules as $pattern=>$route)
				$this->_rules[] = $this->createUrlRule($route, $pattern);
		else
			foreach (array_reverse($rules) as $pattern=>$route)
				array_unshift($this->_rules, $this->createUrlRule($route, $pattern));
	}
	
	/**
	 * Переопределение стандартного метода createUrl, формирующего URL-адрес
	 * @param string $route Маршрут приложения
	 * @param array $params Список GET-параметров. По умолчанию равно array()
	 * @param string $ampersand Символ объеденения параметров в адресе. По умолчанию равно "&"
	 * @return string URL-адрес
	 * @see CUrlManager::createUrl()
	 */
	public function createUrl($route, $params = array(), $ampersand = '&'){
		unset($params[$this->routeVar]);
		
		foreach ($params as $i=>$param)
			if ($param === null)
				$params[$i] = '';
		
		if (isset($params['#'])){
			$anchor = '#'.$params['#'];
			unset($params['#']);
		}else
			$anchor = '';
		
		$route = trim($route, '/');
		
		foreach ($this->_rules as $i=>$rule){
			if (is_array($rule))
				$this->_rules[$i] = $rule = Yii::createComponent($rule);
			
			if (($url = $rule->createUrl($this, $route, $params, $ampersand)) !== false)
				if ($rule->hasHostInfo)
					return $url === '' ? '/'.$anchor : $url.$anchor;
				else
					return $this->getBaseUrl().'/'.$url.$anchor;
		}
		
		return $this->createUrlDefault($route, $params, $ampersand).$anchor;
	}
	
	/**
	 * Переопределение стандартного метода parseUrl, осуществляющего парсинг пользовательского URL-адреса
	 * @param CHttpRequest $request Компонент приложения, содержащий запрос пользователя
	 * @return string Маршрут приложения с параметрами
	 * @see CUrlManager::parseUrl()
	 */
	public function parseUrl($request){
		if ($this->getUrlFormat() === self::PATH_FORMAT){
			$rawPathInfo = $request->getPathInfo();
			$pathInfo = $this->removeUrlSuffix($rawPathInfo, $this->urlSuffix);
			
			foreach ($this->_rules as $i=>$rule){
				if (is_array($rule))
					$this->_rules[$i] = $rule = Yii::createComponent($rule);
				
				if (($r = $rule->parseUrl($this, $request, $pathInfo, $rawPathInfo)) !== false)
					return isset($_GET[$this->routeVar]) ? $_GET[$this->routeVar] : $r;
			}
			
			if ($this->useStrictParsing)
				throw new CHttpException(404, Yii::t('yii', 'Unable to resolve the request "{route}".', array('{route}'=>$pathInfo)));
			else
				return $pathInfo;
		}elseif (isset($_GET[$this->routeVar]))
			return $_GET[$this->routeVar];
		elseif (isset($_POST[$this->routeVar]))
		 return $_POST[$this->routeVar];
		else
			return '';
	}
	
	/**
	 * Переопределение стандартного метода processRules, подготавливающего правила проверки URL-адреса
	 * @see CUrlManager::processRules()
	 */
	protected function processRules(){
		if (empty($this->rules) || $this->getUrlFormat() === static::GET_FORMAT)
			return;
		
		if ($this->cacheID !== false && ($cache = Yii::app()->getComponent($this->cacheID)) !== null){
			$key = Cache::PREFIX_SYSTEM.'url_rules';
			$hash = md5(serialize($this->rules));
			
			if (($data = $cache->getExt('system', $key)) !== false && isset($data[1]) && $data[1] === $hash){
				$this->_rules = $data[0];
				return;
			}
		}
		
		foreach ($this->rules as $pattern=>$route)
			$this->_rules[] = $this->createUrlRule($route, $pattern);
		
		if (isset($cache))
			$cache->setExt('system', $key, array($this->_rules, $hash), Yii::app()->param->get('cache', 'duration1'));
	}
	
}
