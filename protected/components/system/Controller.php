<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CController
 * @author v.zubov
 * @see CController
 */
class Controller extends CController{
	
	/**
	 * @var string Псевдоним пути к файлу веб-конфигурации приложения 
	 */
	const DIR_CFG_WEB = 'webroot';
	
	/**
	 * @var string Имя файла веб-конфигурации приложения
	 */
	const FN_CFG_WEB = 'web.config';
	
	/**
	 * @var string Маршрут доступа к приватной зоне приложения
	 */
	const ROUTE_PRIVATE = 'main/private';
	
	/**
	 * @var Список названий контроллеров с последовательным прохождением действий
	 */
	const CHAIN_CONTROLLERS = array('deploy');
	
	/**
	 * @var integer Код режима работы компонента "Схема сайта" "Отключенный"
	 */
	const SCHEME_MODE_DISABLED = 0;
	
	/**
	 * @var integer Код режима работы компонента "Схема сайта" "Скрытый" - без фиксации событий и отображения
	 */
	const SCHEME_MODE_HIDDEN = 1;
	
	/**
	 * @var integer Код режима работы компонента "Схема сайта" "Безопасный" - без фиксации событий
	 */
	const SCHEME_MODE_SAFE = 2;
	
	/**
	 * @var integer Код режима работы компонента "Схема сайта" "Нормальный"
	 */
	const SCHEME_MODE_NORMAL = 3;
	
	/**
	 * @var integer Код режима работы компонента "Схема сайта"
	 */
	public $schemeMode = self::SCHEME_MODE_NORMAL;
	
	/**
	 * @var string HTML-код содержимого консоли
	 */
	public $console;
	
	/**
	 * @var boolean Признак использования компонентами приложения данных по умолчанию
	 */
	public $componentDef = false;
	
	/**
	 * @var string Символьный идентификатор представления активного всплывающего онка
	 */
	public $juid;
	
	/**
	 * @var array Ассоциативный массив с информацией о классах внешних действий приложения
	 */
	protected $_actions = array(
		'captcha'=>'CaptchaAction',
		'code'=>'CodeAction',
		'login'=>'LoginAction',
		'reserve'=>'ReserveAction'
	);
	
	/**
	 * @var string Маршрут текущего запроса
	 */
	protected $_route;
	
	/**
	 * @var boolean Признак активности подключения к БД
	 */
	protected $_dbActive;
	
	/**
	 * @var integer Код текущего статуса приложения
	 */
	protected $_stateCode;
	
	/**
	 * @var string Символьный идентификатор текущего статуса приложения
	 */
	protected $_state;
	
	/**
	 * @var boolean Признак режима "Технические работы"
	 */
	protected $_technicalWork;
	
	/**
	 * @var boolean Признак развёрнутости приложения
	 */
	protected $_deployed;
	
	/**
	 * @var boolean Признак рабочего статуса приложения
	 */
	protected $_working;
	
	/**
	 * @var boolean Признак нахождения приложения в зоне приватного доступа
	 */
	protected $_privateArea;
	
	/**
	 * @var boolean Признак приватного входа на сайт
	 */
	protected $_privateEnter;
	
	/**
	 * @var array Список символьных идентификаторов действий раздела сайта последовательного прохождения
	 */
	protected $_chainStates;
	
	/**
	 * Первичная обработка запроса пользователя вызванным контроллером приложения
	 * @param $action Название вызванного действия контроллера
	 * @return boolean Признак успешности первичной обработки
	 */
	protected function beforeAction($action){
// 		require 'q.e';
// 		Arrays::printPre(Html::getUserAddr(), 1);
// 		Arrays::printPre(Yii::app()->table->dropAreaTables(), 1);
// 		Arrays::printPre(Hashing::encode('pwdCITIZEN6', Hashing::key('citizen')), 1);
		
		if (!parent::beforeAction($action))
			return false;
		
		$this->getDbActive();
// 		$this->testButton();
		
		if (Yii::app()->errorHandler->error)
			return true;
		
// 		Arrays::printPre(Yii::app()->bss1->query('operation_list', array('id'=>'1770010')), 1);
// 		Arrays::printPre(Yii::app()->bss2->query('account_info', array('id'=>2320531)), 1);
// 		Arrays::printPre(Yii::app()->bss2->query('operation_last', array('id'=>1001)), 1);
// 		Arrays::printPre(Yii::app()->bss2->formingUrl('operation_last', array('id'=>1463347)), 1);
// 		Arrays::printPre(Yii::app()->bss2->formingUrl('account_info', array('id'=>2320531)), 1);
		
		if (!Yii::app()->request->isAjaxRequest){
			if (YII_DEBUG && Yii::app()->test->active){
				$this->schemeMode = self::SCHEME_MODE_DISABLED;
				$this->layout = false;
				$this->render('/main/tests', array('tests'=>Yii::app()->test->result));
				Yii::app()->end();
			}
			
			if (Yii::app()->user->isGuest){
				if (Html::isSecure() && $this->route !== 'main/secure')
					$this->redirect(Html::changeSecureUrl(false, false));
				
				if (Yii::app()->user->getState('confirm') && !in_array($this->route,
					array(Yii::app()->defaultController, 'main/secure', 'main/confirm')))
						$this->redirect($this->createUrl(Yii::app()->defaultController));
			}elseif (!Html::isSecure() && Html::secureCheck() && $this->route !== 'esia/logout')
				$this->redirect(Html::changeSecureUrl());
			
			if (!$this->getDbActive() && $this->route === Yii::app()->defaultController && !$this->checkWebConfig()){
				$this->schemeMode = self::SCHEME_MODE_DISABLED;
				$this->renderText(null);
				Yii::app()->end();
			}
			
			if (!$this->getDeployed() && $this->id !== 'deploy' && Yii::app()->user->checkAccess('developer'))
				$this->redirect($this->createUrl('deploy/'.mb_substr($this->getState(), mb_strlen(Setting::PREFIX_DEPLOY))));
			
			Yii::app()->user->checkLogout();
			
			if (Yii::app()->user->isGuest){
				if (!$this->technicalWork)
					$this->schemeMode = self::SCHEME_MODE_HIDDEN;
				elseif (!Yii::app()->user->authorized) {
                    $this->render('/main/index');
                    Yii::app()->end();
                }
			}
		}
		
		Yii::app()->scheme;
		
// 		Arrays::printPre(Yii::app()->scheme->schemeItem, 1);
		
		CValidator::$builtInValidators = array_merge(CValidator::$builtInValidators, array(
			'captcha'=>'CaptchaValidator',
			'code'=>'CodeValidator',
			'compare'=>'CompareValidator',
			'date'=>'DateValidator',
			'difficulty'=>'DifficultyValidator',
			'email'=>'EmailValidator',
			'fias'=>'FiasValidator',
			'host'=>'HostValidator',
			'length'=>'StringValidator',
			'match'=>'RegularExpressionValidator',
			'numerical'=>'NumberValidator',
			'password'=>'PasswordValidator',
			'phone'=>'PhoneValidator',
			'required'=>'RequiredValidator',
			'swearword'=>'SwearwordRegularExpressionValidator',
			'unique'=>'UniqueValidator'
		));
		
		/* if (!Yii::app()->request->isAjaxRequest){
			ob_start();
//				Yii::app()->bss1->set(array('on'=>true, 'host'=>'192.168.60.26', 'name'=>'tesths'));
				Yii::app()->bss1->test();
				Yii::app()->bss2->test();
			$this->console = ob_get_clean();
		} */
		
		if (Yii::app()->user->flashOnly){
			$this->renderText(null);
			Yii::app()->end();
		}
		
		return true;
	}
	
	/**
	 * Переопределение стандартного метода invalidActionParams, вызываемого, когда действию передаётся некорректный
	 * набор параметров
	 * @param CAction $action Экземпляр вызываемого действия
	 * @see CController::invalidActionParams()
	 */
	public function invalidActionParams($action){
		throw new CHttpException(400);
	}
	
	/**
	 * Переопределение стандартного метода actions, возвращающего информацию о внешних классах приложения
	 * @param boolean|string Символьный идентификатор действия, класс которого нужно получить или false, если нужно получить
	 * полный список классов
	 * @return array|boolean Ассоциативный массив с информацией о внешних классах приложения, название класса для
	 * конкретного действия или false, если действие не найдено
	 * @see CController::actions()
	 */
	public function actions($action = false){
		return !$action ? $this->_actions : (isset($this->_actions[$action]) ? $this->_actions[$action] : false);
	}
	
	/**
	 * Генерация HTML-идентификатора формы данных в формате
	 * 	form-{Контроллер}-{Действие}[-{Значение параметра name}]
	 * @param string $name Название формы. По умолчанию равно false. Если не является строкой, то будет заменено
	 * на 'main'
	 * @return string HTML-идентификатор формы
	 */
	public function getFormId($name = false){
		if (!is_string($name))
			$name = $this->juid ? 'juid' : 'main';
		
		return "form-{$this->id}-{$this->action->id}".($name ? '-'.$name : '');
	}
	
	/**
	 * Переопределение стандартного метода getRoute, возвращающего маршрут текущего запроса
	 * @return string Маршрут запроса
	 * @see CController::getRoute()
	 */
	public function getRoute(){
		if ($this->_route === null){
			$this->_route = parent::getRoute();
			
			if ($this->_privateEnter = $this->_route === self::ROUTE_PRIVATE)
				$this->_route = Yii::app()->defaultController;
		}
		
		return $this->_route;
	}
	
	/**
	 * Метод-геттер для определения _dbActive. Осуществляет так же активацию базовых компонентов приложения
	 * @return boolean Признак активности подключения к БД
	 */
	public function getDbActive(){
		if ($this->_dbActive === null){
			$this->componentDef = true;
			
			$this->_dbActive = Yii::app()->db->active && $this->getDeployed();
			Yii::app()->setting;
			Yii::app()->user->authManager;
			
			if ($this->_dbActive)
				Yii::app()->log->model(true);
			
			$this->componentDef = false;
			
			Cache::init();
		}
		
		return $this->_dbActive;
	}
	
	/**
	 * Метод-геттер для определения _stateCode
	 * @return integer Код текущего статуса приложения
	 */
	public function getStateCode(){
		if ($this->_stateCode === null)
			$this->_stateCode = Yii::app()->conformity->get('application_state', 'code', $this->state);
		
		return $this->_stateCode;
	}
	
	/**
	 * Метод-геттер для определения _state
	 * @return string Символьный идентификатор текущего статуса приложения
	 */
	public function getState(){
		if ($this->_state === null)
			$this->_state = Yii::app()->setting->get('application_state');
		
		return $this->_state;
	}
	
	/**
	 * Метод-cеттер для установки _state
	 * @param string @state Символьный идентификатор текущего статуса приложения
	 * @return boolean Признак установки статуса
	 */
	public function setState($state){
		if (($code = Yii::app()->conformity->get('application_state', 'code', $state)) && $this->_state !== $state){
			$this->_state = $state;
			$this->_stateCode = $code;
			$this->_technicalWork = $this->_deployed = $this->_working = $this->_privateArea = null;
		}
		
		return (boolean)$code;
	}
	
	/**
	 * Метод-геттер для определения _technicalWork
	 * @return boolean Признак режима "Технические работы"
	 */
	public function getTechnicalWork(){
		if ($this->_technicalWork === null)
			$this->_technicalWork = !$this->getDeployed() || $this->state === 'locked';
		
		return $this->_technicalWork;
	}
	
	/**
	 * Метод-геттер для определения _deployed
	 * @return boolean Признак развёрнутости приложения
	 */
	public function getDeployed(){
		if ($this->_deployed === null)
			$this->_deployed = $this->stateCode >
				Yii::app()->conformity->get('application_state', 'code', 'init_data');
		
		return $this->_deployed;
	}
	
	/**
	 * Метод-геттер для определения _working
	 * @return boolean Признак рабочего статуса приложения
	 */
	public function getWorking(){
		if ($this->_working === null)
			$this->_working = $this->state === 'working';
		
		return $this->_working;
	}
	
	/**
	 * Метод-геттер для определения _privateArea
	 * @return boolean Признак нахождения приложения в зоне приватного доступа
	 */
	public function getPrivateArea(){
		if ($this->_privateArea === null)
			$this->_privateArea = in_array($this->state, array('locked', 'card'));
		
		return $this->_privateArea;
	}
	
	/**
	 * Метод-геттер для определения _privateEnter
	 * @return boolean Признак приватного входа на сайт
	 */
	public function getPrivateEnter(){
		if ($this->_privateEnter === null)
			$this->route;
		
		return $this->_privateEnter;
	}
	
	/**
	 * Генерация HTML-кода блока авторизации
	 * @return boolean|string HTML-код панели профиля пользователя или false
	 */
	public function getProfile(){
		if (!$this->getDeployed() || ($this->privateArea && (!$this->privateEnter && !Yii::app()->user->private)) ||
			(!Yii::app()->user->checkAccess('*/login') && !Yii::app()->user->checkAccess('user/logout')))
				return false;
		
		return $this->createAction('login')->run();
	}
	
	/**
	 * Метод-геттер для определения _chainStates
	 * @return array Список символьных идентификаторов действий раздела сайта последовательного прохождения
	 */
	public function getChainStates(){
		if ($this->_chainStates === null){
			$this->_chainStates = false;
			
			if (in_array($this->id, self::CHAIN_CONTROLLERS, true))
				if ($navigator = Yii::app()->scheme->navigator)
					if ($items = end($navigator))
						foreach ($items as $item)
							$this->_chainStates[] = end(explode('/', $item['route']));
		}
		
		return $this->_chainStates;
	}
	
	/**
	 * Обновление текущей страницы без параметров
	 * @param boolean $get_params Признак включения в адрес запроса GET-параметров. По умолчанию равно false
	 */
	public function refreshWithoutParams($get_params = false){
		$this->redirect($this->createUrl($this->route, $get_params ? $this->actionParams : array()));
	}
	
	/**
	 * Перенаправление браузера на другую страницу приложения в зависимости от прав доступа пользователя
	 * @param array $params_urls Ассоциативный массив параметров url-адресов возможных переходов в порядке уменьшения приоритета.
	 * Задаётся в формате:
	 *  array(
	 *  	'user/index',// Простой путь
	 *  	'user/view'=>array('id'=>4),// Путь с указанием GET-параметров
	 *  )
	 * @param boolean $wrap Признак необходимости рендеринга обёртки страницы в случае отказа перенаправления. По
	 * умолчанию равно false
	 * @param boolean $terminate Признак завершения обработки HTTP-запроса в случае отказа перенаправления. По умолчанию
	 * равно false
	 */
	public function redirectAccessible($params_urls, $wrap = false, $terminate = false){
		foreach ($params_urls as $key=>$params_url){
			list($route, $params) = is_string($key) ? array($key, $params_url) : array($params_url, null);
			
			if (Yii::app()->user->checkAccess($route))
				$this->redirect($this->createUrl($route, (array)$params));
		}
		
		if ($wrap)
			$this->renderText(null);
		
		if ($terminate)
			Yii::app()->end();
	}
	
	/**
	 * Тестирование кнопок
	 */
	protected function testButton(){
		$this->schemeMode = self::SCHEME_MODE_DISABLED;
		$this->render('/main/test_button');
		Yii::app()->end();		
	}
	
	/**
	 * Проверка наличия файла веб-конфигурации приложения. Если его нет, будет предпринята попытка его создания
	 * @return boolean Успешность выполнения
	 */
	protected function checkWebConfig(){
		$path = Yii::getPathOfAlias(self::DIR_CFG_WEB).'/'.self::FN_CFG_WEB;
		
		if (is_file($path))
			return true;
		
		$content = $this->renderPartial('/system/web', array('rewrite_base'=>Yii::app()->getBaseUrl()), true);
		$ok = file_put_contents($path, $content);
		
		Yii::app()->log->fix('cfg_web_create', array(
			'path'=>$path,
			'file_name'=>self::FN_CFG_WEB
		), $ok ? 'ok' : 'file_create');
		
		return $ok;
	}
	
}
