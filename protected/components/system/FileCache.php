<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CFileCache
 * @author v.zubov
 * @see CFileCache
 */
class FileCache extends CFileCache{
	
	/**
	 * @var array Список шаблонов поиска файлов записей кэша. Ключи элементов соответствуют кодам типов очистки 
	 */
	const PATTERN_FN_CLEANING = array(
		//sql
		'insert'=>'{'.Cache::PREFIX_MODEL.'*%1$s*,'.Cache::PREFIX_RECORD.'*%1$s*}',
		'update'=>'{'.Cache::PREFIX_MODEL.'*%1$s*,'.Cache::PREFIX_RECORD.'%1$s*%2$s,'.Cache::PREFIX_RECORD.'%3$s*%1$s*}',
		//bss
		'code_drop'=>'code_check%1$s',
		'reading_add'=>'{reading_list%1$s,counter_list*}'
	);
	
	/**
	 * @var string Префикс ключей записей кэша
	 */
	public $keyPrefix = '';
	
	/**
	 * @var boolean Признак необходимости хэширования ключей записей кэша
	 */
	public $hashKey = false;
	
	/**
	 * @var array Ассоциативный массив соответствий ключей записей их типам в формате {Ключ записи}=>{Тип записи}
	 */
	protected $_idTypes = array();
	
	/**
	 * @var array Ассоциативный массив ключей параметров запущенных чисток. Применяется для пресечения циклических чисток
	 */
	protected $_listCleaning = array();
	
	/**
	 * Конструктор компонента
	 */
	public function init(){
		parent::init();
	}
	
	/**
	 * Расширенный вариант метода get, подготавливающего получение данных из кэша, с указанием типа данных
	 * @param string $type Тип кэшируемых данных
	 * @param string $id Ключ, идентифицирующий данные
	 * @return mixed Данные из кэша
	 * @see CCache::get()
	 */
	public function getExt($type, $id){
		$this->_idTypes[$id] = $type;
		
		return parent::get($id);
	}
	
	/**
	 * Расширенный вариант метода set, подготавливающего сохранение данных в кэше, с указанием типа данных
	 * @param string $type Тип кэшируемых данных
	 * @param string $id Ключ, идентифицирующий данные
	 * @param mixed $value Данные
	 * @param integer $expire Время хранения данных в секундах. По умолчанию равно 0
	 * @param ICacheDependency $dependency Зависимость
	 * @return boolean Успешность выполнения
	 * @see CCache::set()
	 */
	public function setExt($type, $id, $value, $expire = 0, $dependency = null){
		$this->_idTypes[$id] = $type;
		
		return parent::set($id, $value, $expire, $dependency);
	}
	
	/**
	 * Расширенный вариант метода delete, подготавливающего удаление данных из кэша, с указанием типа данных
	 * @param string $type Тип кэшируемых данных
	 * @param string $id Ключ, идентифицирующий данные
	 * @return boolean Успешность выполнения
	 * @see CCache::delete()
	 */
	public function deleteExt($type, $id){
		$this->_idTypes[$id] = $type;
		
		return parent::delete($id);
	}
	
	/**
	 * Переопределение стандартного метода flushValues, удаляющего данные из кэша
	 * @return boolean Успешность выполнения
	 * @see CFileCache::flushValues()
	 */
	protected function flushValues(){
		$this->gc(false, Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR.'cache');
		
		return true;
	}
	
	/**
	 * Переопределение стандартного метода setValue, сохраняющего данные в кэше
	 * @param string $key Ключ, идентифицирующий данные
	 * @param string $value Строка данных
	 * @param integer $expire Время хранения данных в секундах
	 * @return boolean Успешность выполнения
	 * @see CFileCache::setValue()
	 */
	protected function setValue($key, $value, $expire){
		$params_fix = array('type'=>Arrays::pop($this->_idTypes, $key), 'key'=>$key);
		
		try{
			$result = parent::setValue($key, $value, $expire);
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('cache_set', $params_fix, $ok ? 'ok' : 'cache_set');
		
		return $ok ? $result : false;
	}
	
	/**
	 * Переопределение стандартного метода getCacheFile, возвращающего путь к файлу кэша
	 * @param string $key Ключ, идентифицирующий данные
	 * @return string Путь к файлу
	 * @see CFileCache::getCacheFile()
	 */
	protected function getCacheFile($key){
		$this->cachePath = Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR.'cache';
		
		if ($type = Arrays::pop($this->_idTypes, $key))
			$this->cachePath .= DIRECTORY_SEPARATOR.$type;
		
		if (!is_dir($this->cachePath))
			mkdir($this->cachePath, 0777, true);
		
		return parent::getCacheFile($key);
	}
	
	/**
	 * Очистка записей кэша по шаблону
	 * @param string $type Тип кэшируемых данных
	 * @param string $pattern Символьный идентификатор шаблона очистки кэша
	 * @param array $params Параметры шаблона. По умолчанию равно false
	 * @return boolean Успешность выполнения
	 */
	public function cleaning($type, $pattern, $params = false){
		$key = serialize(func_get_args());
		
		if (isset($this->_listCleaning[$key]))
			return true;
		else
			$this->_listCleaning[$key] = 0;
		
		$params_fix = array('type'=>$type, 'pattern'=>$pattern, 'params'=>$params, 'removed_files'=>array());
		
		try{
			if (!in_array($pattern, array_keys(static::PATTERN_FN_CLEANING)))
				throw new CHttpException(500, 'pattern name is incorrect');
			
			$params_func[] = static::PATTERN_FN_CLEANING[$pattern];
			
			foreach (array_merge($params, array('*')) as $param)
				$params_func[] = sprintf(Cache::PATTERN_PARAM, $param);
			
			$pattern = Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.$type.DIRECTORY_SEPARATOR.
				call_user_func_array('sprintf', $params_func).$this->cacheFileSuffix;
			
			foreach (glob($pattern, GLOB_BRACE) as $fn){
				$params_fix['removed_files'][] = $fn;
				@unlink($fn);
			}
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('cache_cleaning', $params_fix, $ok ? 'ok' : 'cache_cleaning');
		
		return $ok;
	}
	
}
