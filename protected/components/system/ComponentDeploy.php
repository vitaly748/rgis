<?php
/**
 * Класс, предоставляющий функционал для компонентов, участвующих в развёртывании приложения
 * @author v.zubov
 */
class ComponentDeploy extends Component{
	
	/**
	 * Подготовка к развёртыванию группы таблиц приложения
	 * @param string $section Название группы таблиц
	 * @param string $type Символьный идентификатор типа предстоящего развёртывания
	 * @param array $params Ассоциативный массив с параметрами развёртывания приложения
	 * @return boolean Успешность выполнения
	 */
	public function beforeDeploy($section, $type, $params){
		try{
			$this->beforeDeployCompute($section, $type, $params);
		}catch (CException $e){
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('deploy_group_before', array(
			'section'=>$section,
			'type'=>$type,
			'params'=>$params
		), $ok ? 'ok' : 'deploy_group_before');
		
		return $ok;
	}
	
	/**
	 * Подготовка к развёртыванию
	 * @param string $section Название группы таблиц
	 * @param string $type Символьный идентификатор типа предстоящего развёртывания
	 * @param array $params Ассоциативный массив с параметрами развёртывания приложения
	 * @throws CHttpException
	 */
	public function beforeDeployCompute($section, $type, $params){
		if ($type === 'standart' && !Yii::app()->table->dropGroupsTables($section))
			throw new CHttpException(500);
	}
	
	/**
	 * Развёртывание группы таблиц приложения
	 * @param string $section Название группы таблиц
	 * @param string $type Символьный идентификатор типа развёртывания
	 * @param array $params Ассоциативный массив с параметрами развёртывания приложения
	 * @return boolean Успешность выполнения
	 */
	public function deploy($section, $type, $params){
		Swamper::timer($tick);
		
		try{
			$this->deployCompute($section, $type, $params);
		}catch (CException $e){
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('deploy_group', array(
			'section'=>$section,
			'type'=>$type,
			'params'=>$params,
			'timer'=>Swamper::timer($tick)
		), $ok ? 'ok' : 'deploy_group');
		
		return $ok;
	}
	
	/**
	 * Развёртывание
	 * @param string $section Название группы таблиц
	 * @param string $type Символьный идентификатор типа развёртывания
	 * @param array $params Ассоциативный массив с параметрами развёртывания приложения
	 * @throws CHttpException
	 */
	public function deployCompute($section, $type, $params){
		if ($type === 'standart' && !Yii::app()->table->createGroupsTables($section))
			throw new CHttpException(500);
	}
	
}
