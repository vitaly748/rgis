<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CDbConnection
 * @author v.zubov
 * @see CDbConnection
 */
class DbConnection extends CDbConnection{
	
	/**
	 * @var array Ассоциативный массив параметров подключения по умолчанию
	 */
	const PARAMS_DEF = array(
		'host'=>false,// HTTP-адрес сервера БД
		'port'=>false,// Порт подключения
		'name'=>false,// Название БД
		'login'=>false,// Логин
		'password'=>false// Пароль
	);
	
	/**
	 * @var integer Код ошибки подключения к БД "Без ошибок"
	 */
	const	ERROR_NONE = 0;
	
	/**
	 * @var integer Код ошибки подключения к БД "Неверный адрес"
	 */
	const	ERROR_HOST = 1;
	
	/**
	 * @var integer Код ошибки подключения к БД "Ошибка при подключении"
	 */
	const	ERROR_CONNECT = 2;
	
	/**
	 * @var integer Код ошибки подключения к БД "Неверное название БД"
	 */
	const	ERROR_DB = 3;
	
	/**
	 * @var integer Код ошибки подключения к БД "Неверная авторизация"
	 */
	const	ERROR_AUTH = 4;
	
	/**
	 * @var integer Код ошибки подключения к БД "Неверная конфигурация"
	 */
	const	ERROR_CFG = 5;
	
	/**
	 * @var array Конфигурация подключения к БД в виде ассоциативного массива
	 */
	protected $_cfg;
	
	/**
	 * Конструктор компонента
	 */
	public function init(){
		$this->driverMap['pgsql'] = 'PgsqlSchema';
		
		if ($cfg = Yii::app()->param->get('db'))
			$this->applyCfg($cfg);
	}
	
	/**
	 * Применение заданной конфигурации
	 * @param array $cfg Список настроек
	 * @return boolean Успешность подключения
	 */
	public function applyCfg($cfg){
		if ($this->checkCfg($cfg)){
			$this->connectionString = $this->formingConnectionString($cfg);
			$this->username = $cfg['login'];
			$this->password = $cfg['password'];
			
			try{
				parent::init();
				$this->_cfg = $cfg;
			}catch (CException $e){
			}
		}
		
		return $this->active;
	}
	
	/**
	 * Выборка и получение данных конфигурации
	 * @param boolean|string $name Название параметра или false. По умолчанию равно false
	 * @return array|boolean|string Выбранные данные конфигурации или false
	 */
	public function get($name = false){
		if (!$name)
			return $this->_cfg;
		
		return isset($this->_cfg[$name]) ? $this->_cfg[$name] : false;
	}
	
	/**
	 * Тестирование заданной конфигурации подключения к БД
	 * @param array $cfg Список настроек
	 * @return integer Код ошибки подключения
	 */
	public function testCfg($cfg){
		if (!$this->checkCfg($cfg))
			return self::ERROR_CFG;
		
		$db = new CDbConnection($this->formingConnectionString($cfg), $cfg['login'], $cfg['password']);
		
		try{
			$db->init();
		}catch (CException $e){
			$msg = explode(': ', $e->getMessage());
			
			if (!isset($msg[2]))
				return self::ERROR_CONNECT;
			elseif (mb_strpos($msg[2], 'Unknown host') !== false)
				return self::ERROR_HOST;
			elseif (mb_strpos($msg[2], 'database') !== false)
				return self::ERROR_DB;
			elseif (mb_strpos($msg[2], 'password authentication') !== false || mb_strpos($msg[2], 'no password supplied') !== false)
				return self::ERROR_AUTH;
			else
				return self::ERROR_CONNECT;
		}
		
		return self::ERROR_NONE;
	}
	
	/**
	 * Сохранение настроек подключения к БД
	 * @param array $cfg Список настроек
	 * @return boolean Успешность выполнения
	 */
	public function setCfg($cfg){
		return $this->checkCfg($cfg) && Yii::app()->param->set('db', $cfg);
	}
	
	/**
	 * Переопределение стандартного метода createCommand, подготавливающего SQL-запросы к БД
	 * @param mixed $query Текст запроса к БД
	 * @see CDbConnection::createCommand()
	 */
	public function createCommand($query = null){
		return $this->active ? new DbCommand($this, $query) : false;
	}
	
	/**
	 * Проверка конфигурации подключения на наличие всех необходимых параметров
	 * @param array $cfg Список настроек
	 * @return boolean Успешность проверки
	 */
	private function checkCfg($cfg){
		return is_array($cfg) && !array_diff(array_keys(self::PARAMS_DEF), array_keys($cfg));
	}
	
	/**
	 * Формирование строки подключения
	 * @param array $cfg Список настроек
	 * @return string Строка подключения
	 */
	private function formingConnectionString($cfg){
		$port = $cfg['port'] ? "port={$cfg['port']};" : '';
		
		return "pgsql:host={$cfg['host']};{$port}dbname={$cfg['name']}";
	}
	
}
