<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CPgsqlSchema
 * @author v.zubov
 * @see CPgsqlSchema
 */
class PgsqlSchema extends CPgsqlSchema{
	
	/**
	 * @var array Список названий всех таблиц в активной БД
	 */
	private $_tableNames = array();
	
	/**
	 * @var array Список мета-данных таблиц
	 */
	private $_tables = array();
	
	/**
	 * @var CDbConnection Экземпляр активного подключения к БД
	 */
	private $_connection;
	
	/**
	 * @var CDbCommandBuilder Экземпляр конструктора команд для активного подключения
	 */
	private $_builder;
	
	/**
	 * @var array Список названий некэшируемых таблиц
	 */
	private $_cacheExclude = array();
	
	/**
	 * Переопределение стандартного конструктора
	 * @param CDbConnection $conn Экземпляр активного подключения к БД
	 * @see CDbSchema::__construct()
	 */
	public function __construct($conn){
		$this->_connection = $conn;
		
		foreach ($conn->schemaCachingExclude as $name)
			$this->_cacheExclude[$name] = true;
	}
	
	/**
	 * Переопределение стандартного метода getDbConnection, возвращающего активное подключение к БД
	 * @return CDbConnection Экземпляр активного подключения к БД
	 * @see CDbSchema::getDbConnection()
	 */
	public function getDbConnection(){
		return $this->_connection;
	}
	
	/**
	 * Переопределение стандартного метода getTable, возвращающего мета-данные таблицы БД
	 * @param string $name Название таблицы
	 * @param boolean $refresh Признак принудительного обновления данных. По умолчанию равно false
	 * @return CDbTableSchema|null Мета-данные таблицы или null, если таблица не найдена
	 * @see CDbSchema::getTable()
	 */
	public function getTable($name, $refresh = false){
		if ($refresh === false && isset($this->_tables[$name]))
			return $this->_tables[$name];
		else{
			if ($this->_connection->tablePrefix !== null && strpos($name, '{{') !== false)
				$realName = preg_replace('/\{\{(.*?)\}\}/', $this->_connection->tablePrefix.'$1', $name);
			else
				$realName = $name;
			
			if ($this->_connection->queryCachingDuration > 0){
				$qcDuration = $this->_connection->queryCachingDuration;
				$this->_connection->queryCachingDuration = 0;
			}
			
			if (!isset($this->_cacheExclude[$name]) && ($duration = $this->_connection->schemaCachingDuration) > 0 &&
				$this->_connection->schemaCacheID !== false &&
				($cache = Yii::app()->getComponent($this->_connection->schemaCacheID))!==null){
					$key = Cache::PREFIX_META.sprintf(Cache::PATTERN_PARAM, str_replace(array('{', '}'), '', $name));
					$table = $cache->getExt('sql', $key);
					
					if ($refresh === true || $table === false){
						$table = $this->loadTable($realName);
						
						if ($table !== null)
							$cache->setExt('sql', $key, $table, $duration);
					}
					
					$this->_tables[$name] = $table;
				}else
					$this->_tables[$name] = $table = $this->loadTable($realName);
			
			if (isset($qcDuration))
				$this->_connection->queryCachingDuration = $qcDuration;
			
			return $table;
		}
	}
	
	/**
	 * Переопределение стандартного метода getTableNames, возвращающего названия всех таблиц заданной схемы доступа в активной БД
	 * @param string $schema Название схемы доступа. По умолчанию равно ""
	 * @return array Список названий таблиц
	 * @see CDbSchema::getTableNames()
	 */
	public function getTableNames($schema = ''){
		if (!isset($this->_tableNames[$schema]))
			$this->_tableNames[$schema] = $this->findTableNames($schema);
		
		return $this->_tableNames[$schema];
	}
	
	/**
	 * Переопределение стандартного метода getCommandBuilder, возвращающего экземпляр конструктора команд для активного подключения
	 * @return CDbCommandBuilder Экземпляр конструктора команд
	 * @see CDbSchema::getCommandBuilder()
	 */
	public function getCommandBuilder(){
		if ($this->_builder !== null)
			return $this->_builder;
		else
			return $this->_builder = $this->createCommandBuilder();
	}
	
	/**
	 * Переопределение стандартного метода refresh, обновляюющего мета-данные таблиц
	 * @see CDbSchema::refresh()
	 */
	public function refresh(){
		if (($duration = $this->_connection->schemaCachingDuration) > 0 && $this->_connection->schemaCacheID !== false &&
			($cache = Yii::app()->getComponent($this->_connection->schemaCacheID)) !== null)
				foreach (array_keys($this->_tables) as $name)
					if (!isset($this->_cacheExclude[$name])){
						$key = Cache::PREFIX_META.str_replace(array('{', '}'), '', $name);
						$cache->deleteExt('sql', $key);
					}
		
		$this->_tables = array();
		$this->_tableNames = array();
		$this->_builder = null;
	}
	
	/**
	 * Переопределение стандартного метода compareTableNames, сравнивающего названия таблиц
	 * @param string $name1 Название первой таблицы
	 * @param string $name2 Название второй таблицы
	 * @return boolean Результат сравнения
	 * @see CDbSchema::compareTableNames()
	 */
	public function compareTableNames($name1, $name2){
		$name1 = str_replace(array('"', '`', "'"), '', $name1);
		$name2 = str_replace(array('"', '`', "'"), '', $name2);
		
		if (($pos = strrpos($name1, '.')) !== false)
			$name1 = substr($name1, $pos + 1);
		
		if (($pos = strrpos($name2, '.')) !== false)
			$name2 = substr($name2, $pos + 1);
		
		if ($this->_connection->tablePrefix !== null){
			if (strpos($name1, '{') !== false)
				$name1 = $this->_connection->tablePrefix.str_replace(array('{', '}'), '', $name1);
			
			if (strpos($name2, '{') !== false)
				$name2 = $this->_connection->tablePrefix.str_replace(array('{','}'), '', $name2);
		}
		
		return $name1 === $name2;
	}
	
	/**
	 * Переопределение стандартного метода createCommandBuilder, возвращающего экземпляр SQL-командопостроителя
	 * @return PgsqlCommandBuilder Экземпляр командопостроителя
	 */
	protected function createCommandBuilder(){
		return new PgsqlCommandBuilder($this);
	}
	
}
