<?php
/**
 * Базовый класс для виджетов с многоуровневой структурой параметров
 * @author v.zubov
 */
class TreeWidget extends Widget{
	
	/**
	 * @var string Ключ элемента массива параметров, определяющего набор наследуемых параметров
	 */
	const PARAM_TYPE_TRANS = 'trans';
	
	/**
	 * @var string Ключ элемента массива параметров, определяющего набор ненаследуемых параметров
	 */
	const PARAM_TYPE_LIMIT = 'limit';
	
	/**
	 * @var string Ключ элемента массива параметров, определяющего набор фиксируемых на текущем уровне параметров
	 */
	const PARAM_TYPE_FIX = 'fix';
	
	/**
	 * @var string Название параметра, задающего признак использования всех атрибутов модели данных
	 */
	const PARAM_ATTRIBUTES_FULL = 'attrFull';
	
	/**
	 * @var string Название параметра, задающего список ненужных атрибутов
	 */
	const PARAM_ATTRIBUTES_EXCEPTION = 'attrException';
	
	/**
	 * @var string Ключ элемента массива параметров уровня, задающего количество элементов
	 */
	const PARAM_ITEMS_QU = 'qu';
	
	/**
	 * @var string Ключ элемента массива параметров уровня, задающего признак наследования ключей элементов.
	 * По умолчанию равно true
	 */
	const PARAM_ITEMS_INHERIT_KEY = 'inherit_key';
	
	/**
	 * @var string Ключ элемента массива параметров уровня, задающего наследование элементов: 0 - не наследовать,
	 * 1 - наследовать только с индексами имеющимися в привилегированном уровне, 2 - наследовать все. По умолчанию
	 * равно 1
	 */
	const PARAM_ITEMS_INHERIT_DATA = 'inherit_data';
	
	/**
	 * @var string Ключ элемента массива параметров уровня, задающего приоритет массива параметров
	 * класса: 0, 1 или 2. По умолчанию равно 0
	 */
	const PARAM_ITEMS_PRIORITY = 'priority';
	
	/**
	 * @var string Ключ элемента массива параметров уровня, задающего признак стандартного (слева направо)
	 * объединения ключей массивов параметров. По умолчанию равно true
	 */
	const PARAM_ITEMS_CROSS_KEY = 'cross_key';
	
	/**
	 * @var string Ключ элемента массива параметров уровня, задающего признак стандартного (слева направо)
	 * объединения данных массивов параметров. По умолчанию равно true
	 */
	const PARAM_ITEMS_CROSS_DATA = 'cross_data';
	
	/**
	 * @var string Ключ элемента массива параметров уровня, задающего признак необходимости включения в расчёты
	 * массива параметров по умолчанию, если таковой имеется, а так же позицию массива параметров, с которым он
	 * будет объединён: 0, 1 или 2. По умолчанию равно 0
	 */
	const PARAM_ITEMS_DEF = 'def';
	
	/**
	 * @var string Префикс функций подготовки параметров уровня
	 */
	const PREFIX_PREPARATION_METHODS = 'preparation';
	
	/**
	 * @var string Префикс константы пользовательского класса, определяющей обязательный HTML-класс элемента
	 * определённого уровня
	 */
	const PREFIX_HTML_OPTIONS_CLASS = 'CLASS_';
	
	/**
	 * @var string Постфикс константы пользовательского класса, определяющей массив параметров некоторого уровня
	 * по умолчанию 
	 */
	const POSTFIX_PARAMS_ITEMS_DEF = '_LEVEL_DEF';
	
	/**
	 * @var boolean Признак автоматического распознавания уровней, содержащих модели данных. Для таких уровней будет
	 * расчитан параметр "_fullness"
	 */
	const FULLNESS_CHECK = true;
	
	/**
	 * @var array Список названий уровней, для элементов которых нужно автоматически устанавливать
	 * HTML-идентификатор в случае его отсутствия
	 */
	const HTML_OPTIONS_ID_FILLING = array('root');
	
	/**
	 * @var string Обязательный HTML-класс элемента определённого уровня
	 */
// 	const CLASS_ROOT = 'tree-widget'
	
	/**
	 * @var array|number Параметры некоторого уровня по умолчанию. Могут задавать количество элементов, общие
	 * параметры для всех элементов уровня и параметры для конкретного элемента уровня. Примеры:
	 * 	2											// Упрощённый формат, задаётся количество элементов
	 * 	array(								// Расширенный формат
	 * 		'qu'=>2,						// Количество элементов
	 * 		0=>'name1',					// Символьный идентификатор элемента
	 * 		1=>array(						// Параметры элемента
	 * 			'param1'=>value1,
	 * 			...
	 * 		),
	 * 		'name1'=>array(			// Символьный идентификатор и параметры элемента
	 * 			'param1'=>value1,
	 * 			...
	 * 		),
	 * 		'param1'=>value1		// Общий параметр для всех элементов на уровне, должен быть определён в levels[level]
	 * 	)
	 * Эти параметры будут преобразованы к следующей структуре:
	 * 	array(
	 * 		'qu'=>1,							// Количество элементов
	 * 		'common'=>array(),		// Общие параметры для всех элементов уровня
	 * 		'items'=>array(				// Параметры для конкретных элементов уровня
	 * 			'name1'=>array(
	 * 				'param1'=>value1,
	 * 				...
	 * 			),
	 * 			...
	 * 		)
	 * 	)
	 */
// 	public $blocks;
	
	/**
	 * @var array Список названий уровней, на которых возможно прерывание дальнейшего расчёта в случае отсутствия элементов
	 */
	public $breakLevels;
	
	/**
	 * @var array Ассоциативный массив описания уровней и соответствующих им параметров. Индексы являются названиями
	 * уровней, а значения - массивами, содержащими названия параметров, связанных с данным уровнем. Параметры
	 * могут быть трёх типов:
	 *
	 * 	Тип						Ключ					Наследование дочерним уровнем			Фиксируются в параметрах текущего уровня
	 * trans		PARAM_TYPE_TRANS									+																				-
	 * limit		PARAM_TYPE_LIMIT									-																				+
	 * fix			PARAM_TYPE_FIX										+																				+
	 *
	 * Для объявления параметров определённого типа нужно записать их в виде ассоциативного массива, где ключом
	 * будет название типа, а значением - массив, содержащий названия параметров данного типа. Если представителем
	 * некоторого типа является всего один параметр, допускается записывать его без обёртки в массив. Тип trans
	 * принимается по умолчанию, поэтому параметры этого типа можно записывать без обёртки в ассоциативный массив
	 * типа. Пример:
	 *
	 * 	array(
	 * 		'root'=>array(
	 * 			'featureForm', 'featureModel', 'featureName',// trans
	 *	 		'limit'=>array('htmlOptions', 'gada2')
	 * 		),
	 * 		'blocks'=>array(
	 * 			'limit'=>array('htmlOptions', 'type', 'hidden'),
	 * 			'fix'=>array('htm', 'rele')
	 * 		),
	 * 		'attributes'=>array(
	 * 			'cells', 'features',
	 * 			'fix'=>'attries'
	 * 		)
	 * 	)
	 */
	protected $levels;
	
	/**
	 * @var array Список возможных нестандартных переходов между уровнями. Задаётся в виде ассоциативного массива,
	 * где ключ - название уровня, с которого возможен переход, а значение - название уровня, на который возможен
	 * переход. Например, array('attributes'=>'blocks', 'cells'=>'rows')
	 */
	protected $transitions;
	
	/**
	 * @var array Список определения ведущих параметров на уровне. Применяется для организации сопоставления
	 * значений идентификатора элемента уровня и одного из его параметров. При этом, если одно из двух значений не
	 * задано, оно получает значение второго. Упрощённый формат:
	 * 	array(
	 * 		'blocks'=>'style',// {Уровень}=>{Название параметра}
	 * 		...
	 * 	)
	 * Сложный формат позволяет задавать исключения по префиксу текущего уровня рассмотрения:
	 * 	array(
	 * 		'blocks'=>array(// Уровень
	 * 			'id'=>'style',// Название ведущего параметра
	 * 			'exceptions'=>'name1.name2.name3',// Исключающий префикс. Краткая запись. Необязательный параметр
	 * 			'exceptions'=>array(// Список исключающих префиксов
	 * 				'name1.name2.name3',// Прификс элемента структуры
	 * 				...
	 * 			),
	 * 				...
	 * 			)
	 * 		),
	 * 		...
	 * 	)
	 */
	protected $levelIdParams;
	
	/**
	 * @var array Список названий уровней
	 */
	private $_levelNames;
	
	/**
	 * @var array Список названий уровней, работающих с моделями данных
	 */
	private $_modelLevels;
	
	/**
	 * 
	 * @var boolean Признак наступления зацикленности при подготовке структуры данных виджета
	 */
	private $_loop;
	
	/**
	 * Подготовка развёрнутой структуры параметров
	 * @param boolean|string $level Название текущего уровня или false. По умолчанию равно false
	 * @param boolean|array $params_struct Параметры текущего уровня или false. По умолчанию равно false
	 * @param string $prefix Префикс текущего уровня. По умолчанию равно ''
	 * @return array|boolean Структура параметров согласно массива levels или false
	 */
	public function preparation($level = false, $params_struct = false, $prefix = ''){
		$params = false;
		
		if ($levels = $this->levels){
			if (!$level)
				$level = $this->levelNext();
			
			if (in_array($level, $this->levelNames)){
				if (!$params_struct){
					$params_struct = Arrays::mergeParams(array(
						Arrays::MERGE_PARAMS_NAMES=>$levels[$level],
						Arrays::MERGE_PARAMS_SUFFIX=>$level,
						Arrays::MERGE_PARAMS_CLASS=>$this,
						Arrays::MERGE_PARAMS_THIS=>true,
						Arrays::MERGE_PARAMS_MODEL=>true
					));
					
					if ($params_struct){
						$params_struct[0]['id'] = $params_struct[self::PARAM_TYPE_FIX]['id'] = $prefix = $this->id;
						$this->preparationTreeHtmlOptions($level, $params_struct);
					}
				}
				
				$method = Strings::nameModify(self::PREFIX_PREPARATION_METHODS.'_'.$level,
					Strings::FORMAT_NAME_MODIFY_UPPER, false);
				if (method_exists($this, $method))
					$this->$method($params_struct);
				
				$params = $params_struct[self::PARAM_TYPE_FIX];
				
				if ($childs = $this->preparationChilds($level, $params_struct[self::PARAM_TYPE_TRANS], $prefix))
					$params = array_merge($params, $childs);
			}
		}
		
		return $params;
	}
	
	/**
	 * Расчёт параметров для дочерних элементов структуры
	 * @param string $level Название текущего уровня
	 * @param array $params_struct Параметры текущего уровня. Обычно это значение params_merge['trans']
	 * @param string $prefix Префикс текущего уровня. По умолчанию равно ''
	 * @return array|boolean Ассоциативный массив параметров дочерних элементов или false
	 */
	public function preparationChilds($level, $params_struct, $prefix = ''){
		if (!$levels = $this->levels)
			return false;
		
		$level_next = $this->levelNext($level);
		$level_transit = isset($this->transitions[$level]) ? $this->transitions[$level] : false;
		
		if ($level_transit)
			if (isset($params_struct[$level_transit])){
				if ($level_next && isset($params_struct[$level_next]))
					unset($params_struct[$level_next]);
				$level_next = $level_transit;
				$this->_loop = true;
			}else
				$level_transit = false;
		
		if (!$level_next)
			return false;
		
		$params_level = Arrays::pop($params_struct, $level_next, true);
		
		if (!isset($this->$level_next))
			$params_class = false;
		else{
			$params_class = $this->$level_next;
			
			if ($this->_loop)
				if (!is_array($params_class))
					$params_class = false;
				else{
					$param_keys = array(self::PARAM_ITEMS_QU, self::PARAM_ITEMS_INHERIT_KEY, self::PARAM_ITEMS_INHERIT_DATA,
						self::PARAM_ITEMS_PRIORITY, self::PARAM_ITEMS_CROSS_KEY, self::PARAM_ITEMS_CROSS_DATA,
						self::POSTFIX_PARAMS_ITEMS_DEF);
					foreach ($params_class as $key_param=>$param)
						if (!is_string($key_param) || in_array($key_param, $param_keys) ||
							mb_strpos($key_param, '.') === false)
								unset($params_class[$key_param]);
						
					if (!$params_class)
						$params_class = false;
				}
		}
		
// 		Arrays::printPre($prefix);
		
		/* if ($prefix === 'ywp0.1'){
			Arrays::printPre($level_next);
			Arrays::printPre($params_class);
			Arrays::printPre($params_struct);
			Arrays::printPre($params_level);
		} */
		
		$items_inherit_key_level = Arrays::pop($params_level, self::PARAM_ITEMS_INHERIT_KEY, true);
		$items_inherit_key_class = Arrays::pop($params_class, self::PARAM_ITEMS_INHERIT_KEY, true);
		$items_inherit_key = $items_inherit_key_level !== null ? $items_inherit_key_level :
			($items_inherit_key_class !== null ? $items_inherit_key_class : true);
		
		$items_inherit_data_level = Arrays::pop($params_level, self::PARAM_ITEMS_INHERIT_DATA, true);
		$items_inherit_data_class = Arrays::pop($params_class, self::PARAM_ITEMS_INHERIT_DATA, true);
		$items_inherit_data = $items_inherit_data_level !== null ? $items_inherit_data_level :
			($items_inherit_data_class !== null ? $items_inherit_data_class : 1);
		
		$items_priority_level = Arrays::pop($params_level, self::PARAM_ITEMS_PRIORITY, true);
		$items_priority_class = Arrays::pop($params_class, self::PARAM_ITEMS_PRIORITY, true);
		$items_priority = $items_priority_level !== null ? $items_priority_level : $items_priority_class;
		
		$items_cross_key_level = Arrays::pop($params_level, self::PARAM_ITEMS_CROSS_KEY, true);
		$items_cross_key_class = Arrays::pop($params_class, self::PARAM_ITEMS_CROSS_KEY, true);
		$items_cross_key = $items_cross_key_level !== null ? $items_cross_key_level :
			($items_cross_key_class !== null ? $items_cross_key_class : true);
		
		$items_cross_data_level = Arrays::pop($params_level, self::PARAM_ITEMS_CROSS_DATA, true);
		$items_cross_data_class = Arrays::pop($params_class, self::PARAM_ITEMS_CROSS_DATA, true);
		$items_cross_data = $items_cross_data_level !== null ? $items_cross_data_level :
			($items_cross_data_class !== null ? $items_cross_data_class : true);
		
		$items_def_level = Arrays::pop($params_level, self::PARAM_ITEMS_DEF, true);
		$items_def_class = Arrays::pop($params_class, self::PARAM_ITEMS_DEF, true);
		$items_def = $items_def_level !== null ? $items_def_level :
			($items_def_class !== null ? $items_def_class : 0);
		
		$sources = array($params_struct, $params_level);
		
		array_splice($sources, $items_priority, 0, array($params_class));
		
		if ($items_def !== false){
			$const = 'static::'.mb_strtoupper($level_next).self::POSTFIX_PARAMS_ITEMS_DEF;
			
			if (defined($const))
				if ($params_def = constant($const))
					if ($sources[$items_def]){ 
						$params_def = Arrays::mergeParams(array(
							Arrays::MERGE_PARAMS_SOURCES=>array(
								array('data'=>$sources[$items_def]), array('data'=>$params_def)),
							Arrays::MERGE_PARAMS_RULES=>array(
								'data'=>Arrays::MERGE_PARAMS_UNION_RECURSION,
								'data.*'=>Arrays::MERGE_PARAMS_UNION_RECURSION,
							)
						));
						
						if ($params_def = $params_def['data'])
							$sources[$items_def] = $params_def;
					}else
						$sources[$items_def] = $params_def;
		}
		
		$level_names = $levels[$level_next];
		$names_extension = reset(Arrays::extensionNestedParams($level_names));
		$check_name = false;
		$inherit_params_key = false;
		
		if ($params_struct)
			$names_extension = array_merge($names_extension, array_keys($params_struct));
		
		foreach ($sources as $key_source=>$source){
			$params = array();
			
			if ($source)
				if (is_int($source)){
					$params[self::PARAM_ITEMS_QU] = $source;
				}elseif (is_array($source)){
					$source = Arrays::extensionParams($source);
					
					foreach ($source as $key=>$item){
						if (is_string($key))
							if ($key === self::PARAM_ITEMS_QU)
								$params[self::PARAM_ITEMS_QU] = $item;
							elseif (in_array($key, $names_extension))
								$params['common'][$key] = $item;
							else
								$check_name = true;
						elseif (is_string($item)){
							$key = $item;
							$item = array();
							$check_name = true;
						}else
							$params['items'][] = $item;
						
						if ($check_name){
							if ($name = $this->checkKeyPrefix($key, $prefix))
								if ($name !== '*')
									$params['items'][$name] = $item;
								elseif (is_array($item))
									$params['common'] = isset($params['common']) ? array_merge($params['common'], $item) : $item;
							
							$check_name = false;
						}
					}
				}
			
			if (isset($params['items'])){
				if ($inherit_params_key !== false && $items_inherit_data < 2){
					if (!$items_inherit_data)
						unset($sources[$inherit_params_key]['items']);
					else{
						foreach (array_keys($sources[$inherit_params_key]['items']) as $item_key)
							if (!isset($params['items'][$item_key]))
								unset($sources[$inherit_params_key]['items'][$item_key]);
						
						if (!$sources[$inherit_params_key]['items'])
							unset($sources[$inherit_params_key]['items']);
					}
				}
				$inherit_params_key = $key_source;
			}
			
			$sources[$key_source] = $params;
		}
		
		$item_keys = $items = array();
		
		foreach ($items_cross_key ? $sources : array_reverse($sources) as $source)
			if (isset($source['items']) && !($item_keys && !$items_inherit_key))
				$item_keys = array_merge($source['items'], $item_keys);
		
		foreach (array_keys($item_keys) as $item_key){
			$item_sources = array();
			
			foreach ($items_cross_data ? $sources : array_reverse($sources) as $source){
				if (isset($source['common']))
					$item_sources[] = $source['common'];
				
				if (isset($source['items'][$item_key]))
					$item_sources[] = $source['items'][$item_key];
			}
			
			$items[$item_key] = Arrays::mergeParams(array(
				Arrays::MERGE_PARAMS_SOURCES=>$item_sources,
				Arrays::MERGE_PARAMS_RULES=>array('htmlOptions'=>Arrays::MERGE_PARAMS_UNION_LAST),
				Arrays::MERGE_PARAMS_MODEL=>true,
				Arrays::MERGE_PARAMS_RULES=>array('htmlOptions'=>Arrays::MERGE_PARAMS_UNION_LAST)
			));
		}
		
		$params_merge = Arrays::mergeParams(array(
			Arrays::MERGE_PARAMS_NAMES=>array(self::PARAM_ITEMS_QU, 'common'),
			Arrays::MERGE_PARAMS_SOURCES=>$sources,
			Arrays::MERGE_PARAMS_RULES=>array(
				'common'=>Arrays::MERGE_PARAMS_UNION_RECURSION,
				'common.htmlOptions'=>Arrays::MERGE_PARAMS_UNION_LAST
			),
			Arrays::MERGE_PARAMS_MODEL=>true
		));
		
		$model = Arrays::pop(Swamper::dataFullnessCorrect($params_merge['common']), 'model');
		$attr_full = Arrays::pop($params_merge['common'], self::PARAM_ATTRIBUTES_FULL);
		
		if ($attr_full === null)
			$attr_full = true;
		
		$attribute_names = $level_next === 'attributes' && $model instanceof CModel ? $model->allowedAttributeNames : false;
		
		if ($attribute_names && ($attr_exception = Arrays::pop($params_merge['common'], self::PARAM_ATTRIBUTES_EXCEPTION)))
			$attribute_names = array_diff($attribute_names, $attr_exception);
		
		$childs = $attribute_names && $attr_full ? array_fill_keys($attribute_names, $params_merge['common']) : array();
		
		foreach ($items as $key=>$item)
			if (is_string($key)){
				if ($attribute_names === false || in_array($key, $attribute_names))
					$childs[$key] = $item;
			}else
				$childs[] = $item;
		
		$qu_childs = count($childs);
		$qu = max($params_merge[self::PARAM_ITEMS_QU] ? $params_merge[self::PARAM_ITEMS_QU] : $qu_childs,
			!$this->breakLevels || !in_array($level_next, (array)$this->breakLevels));
		
		if ($qu_childs > $qu)
			array_splice($childs, $qu - $qu_childs);
		elseif ($qu_childs < $qu)
			$childs = array_merge($childs, array_fill(0, $qu - $qu_childs, $params_merge['common']));
		
		$result = array();
		$level_id_params = $level_id_param = isset($this->levelIdParams[$level_next]) ? 
			$this->levelIdParams[$level_next] : false;
		$count_id = 1;
		
		/* if ($prefix === 'yw0.3')
			Arrays::printPre($childs, 1); */
		
		foreach ($childs as $key_child=>$child){
			$child = Arrays::mergeParams(array(
				Arrays::MERGE_PARAMS_NAMES=>$level_names,
				Arrays::MERGE_PARAMS_INHERIT=>true,
				Arrays::MERGE_PARAMS_SUFFIX=>$level_next,
				Arrays::MERGE_PARAMS_SOURCES=>array($child),
				Arrays::MERGE_PARAMS_CLASS=>$this
			));
			
			$key_child = $child[0]['id'] ? $child[0]['id'] : (is_string($key_child) ? $key_child : false);
			
			if (is_array($level_id_params))
				$level_id_param = empty($level_id_params['exceptions']) ||
					!Strings::checkKeyPattern("$prefix.$key_child", $level_id_params['exceptions']) ?
					$level_id_params['id'] : false;
			
			if ($level_id_param){
				if (!empty($child[0][$level_id_param])){
					if (!$key_child)
						$key_child = $child[0][$level_id_param];
				}else{
					if (is_string($key_child))
						$child[0][$level_id_param] =
							$child[$this->getParamType($level_next, $level_id_param)][$level_id_param] = $key_child;
				}
			}
			
			if (!$key_child)
				$key_child = $count_id++;
			
			if (!$child[0]['id'])
				$child[0]['id'] = $child[self::PARAM_TYPE_FIX]['id'] = $key_child;
			
			if ($this->modelLevels && in_array($level_next, $this->modelLevels))
				$child[0]['_fullness'] = $child[self::PARAM_TYPE_FIX]['_fullness'] =
					Swamper::dataFullness($child[self::PARAM_TYPE_FIX]);
			
			$this->preparationTreeHtmlOptions($level_next, $child, $prefix);
			
			/* if ($prefix === 'dt-quantity.1.quantity.2')
				Arrays::printPre($child); */
			
			$result[$key_child] = $this->preparation($level_next, $child, $prefix.($prefix ? '.' : '').$key_child);
		}
		
		return array($level_next=>$result);
	}
	
	/**
	 * Метод-геттер для определения _levelNames
	 * @return Список названий уровней
	 */
	protected function getLevelNames(){
		if ($this->_levelNames === null)
			$this->_levelNames = is_array($this->levels) ? array_keys($this->levels) : false;
		
		return $this->_levelNames;
	}
	
	/**
	 * Метод-геттер для определения _modelLevels
	 * @return Список названий уровней, работающих с моделями данных
	 */
	protected function getModelLevels(){
		if ($this->_modelLevels === null){
			$this->_modelLevels = false;
			
			if (static::FULLNESS_CHECK)
				if ($level_names = $this->levelNames){
					$this->_modelLevels = array();
					foreach ($level_names as $level){
						$names = array_merge(
							isset($this->levels[$level][self::PARAM_TYPE_FIX]) ?
								$this->levels[$level][self::PARAM_TYPE_FIX] : array(),
							isset($this->levels[$level][self::PARAM_TYPE_LIMIT]) ?
								$this->levels[$level][self::PARAM_TYPE_LIMIT] : array()
						);
						if (Swamper::dataFullnessCorrect($names, true))
							$this->_modelLevels[] = $level;
					}
				}
		}
		
		return $this->_modelLevels;
	}
	
	/**
	 * Функция специальной обработки параметров уровня, определяется в дочернем классе. Название формируется по
	 * схеме PREFIX_PREPARATION_METHODS.{Название уровня}
	 * @param array $params Параметры уровня
	 */
	/* protected function preparationXxx(&$params){
		$params['featureModel'] .= ' ok!';
	} */
	
	/**
	 * Возвращает название следующего за указанным уровня
	 * @param boolean|string $level Название текущего уровня или false. По умолчанию равен false. Если равен false,
	 * то возвращается название первого уровня
	 * @return boolean|string Название следующего уровня или false
	 */
	private function levelNext($level = false){
		if ($names = $this->levelNames){
			if ($level === false)
				return $names[0];
			else{
				$key = array_search($level, $names);
				if ($key !== false)
					$key++;
				return isset($names[$key]) ? $names[$key] : false;
			}
		}else
			return false;
	}
	
	/**
	 * Подготовка HTML-опций элемента структуры: изменение id и class параметров
	 * @param string $level Название уровня элемента
	 * @param array $params Параметры элемента
	 * @param boolean|string $prefix Префикс текущего уровня или false. По умолчанию равно false
	 */
	private function preparationTreeHtmlOptions($level, &$params, $prefix = false){
		if (!array_key_exists('htmlOptions', $params[0]))
			return;
		
		if (empty($params[0]['htmlOptions']['id'])){
			$id_filling = array_unique(array_merge(
				self::HTML_OPTIONS_ID_FILLING ? self::HTML_OPTIONS_ID_FILLING : array(),
				static::HTML_OPTIONS_ID_FILLING ? static::HTML_OPTIONS_ID_FILLING : array()
			));
			if (!$filling = in_array($level, $id_filling))
				foreach ($params[TreeWidget::PARAM_TYPE_FIX] as $name=>$value)
					if (mb_strpos($name, 'NeedId') !== false){
						$filling = $value;
						break;
					}
			
			if ($filling){
				if ($prefix)
					$prefix = str_replace('.', '-', $prefix).'-';
				$params[0]['htmlOptions']['id'] = $prefix.$params[0]['id'];
			}
		}
		
		$const = 'static::'.self::PREFIX_HTML_OPTIONS_CLASS.mb_strtoupper($level);
		if (defined($const))
			if ($class = constant($const))
				Html::mergeClasses($params[0]['htmlOptions'], $class, true);
		
		if (array_key_exists('htmlOptions', $params[self::PARAM_TYPE_FIX]))
			$params[self::PARAM_TYPE_FIX]['htmlOptions'] = $params[0]['htmlOptions'];
		
		if (array_key_exists('htmlOptions', $params[self::PARAM_TYPE_TRANS]))
			$params[self::PARAM_TYPE_TRANS]['htmlOptions'] = $params[0]['htmlOptions'];
	}
	
	/**
	 * Определение типа параметра по его названию
	 * @param string $level Название уровня параметра
	 * @param string $param Название параметра
	 * @return boolean|string Название типа параметра или false
	 */
	private function getParamType($level, $param){
		$levels = $this->levels;
		if (!isset($levels[$level]))
			return false;
		
		$params = $levels[$level];
		
		return (isset($params[self::PARAM_TYPE_FIX]) && in_array($param, $params[self::PARAM_TYPE_FIX])) ||
			(isset($params[self::PARAM_TYPE_LIMIT]) && in_array($param, $params[self::PARAM_TYPE_LIMIT])) ?
			self::PARAM_TYPE_FIX : self::PARAM_TYPE_TRANS;
	}
	
	/**
	 * Проверяет соответствие ключа элемента массива параметров уровня текущему префиксу функции preparationChilds.
	 * Ключ может содержать звёздочки в любой позиции, звёздочка соответствует любому слову
	 * @param string $key Ключ элеманта массива параметров в формате [name3.[name2.]].name1
	 * @param string $prefix Текущий префикс в формате [name3.[name2.]].name1
	 * @return boolean|string Название параметра или false
	 */
	private function checkKeyPrefix($key, $prefix){
		$key_words = explode('.', $key);
		$name = array_pop($key_words);
		
		return !$key_words || Strings::checkKeyPattern($prefix, implode('.', $key_words)) ? $name : false;
	}
	
}
