<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CDbAuthManager
 * @author v.zubov
 * @see CDbAuthManager
 */
class DbAuthManager extends CDbAuthManager{
	
	/**
	 * @var Название таблицы БД для хранения элементов авторизации
	 */
	public $itemTable = 'auth_item';
	
	/**
	 * @var Название таблицы БД для хранения связей элементов авторизации
	 */
	public $itemChildTable = 'auth_item_child';
	
	/**
	 * @var Название таблицы БД для хранения назначений ролей пользователям
	 */
	public $assignmentTable = 'auth_assignment';
	
	/**
	 * @var array Ассоциативный массив с информацией об элементах авторизации, названия элементов вынесены в индекс
	 */
	protected $_items;
	
	/**
	 * @var array Ассоциативный массив связей дочерних элементов авторизации в формате
	 * 	array(
	 * 		{Название дочернего элемента}=>array(
	 * 			{Название родительского элемента},
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
// 	protected $_childs;
	
	/**
	 * @var array Ассоциативный массив с информацией о назначенных ролях пользователей в формате
	 * 	array(
	 * 		{Цифровой идентификатор пользователя}=>array(
	 * 			{Название элемента доступа},
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
// 	protected $_assignments;
	
	/**
	 * @var array Ассоциативный массив связей родительских элементов авторизации в формате
	 * 	array(
	 * 		{Название родительского элемента}=>array(
	 * 			{Название дочернего элемента},
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
// 	protected $_parents;
	
	/**
	 * @var array Список идентификаторов пользователей, которых необходимо удалить. Применяется при развёртывании приложения
	 */
	protected $_removedUsers;
	
	/**
	 * @var array Ассоциативный массив ролей пользователей в формате:
	 * 	array(
	 * 		5=>array(// Цифровой идентификатор пользователя
	 * 			'clerk',// Символьный идентификатор роли пользователя
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 * Применяется при развёртывании приложения
	 */
	protected $_usersRoles;
	
	/**
	 * @var array Список базовых ролей пользователей. Применяется при развёртывании приложения
	 */
	protected $_baseRoles;
	
	/**
	 * Конструктор компонента
	 * @see CDbAuthManager::init()
	 */
	public function init(){
		parent::init();
		
		if (Yii::app()->controller->dbActive)
			$this->_items = AuthItemModel::model()->findAllSelective('name', false, true, array('order'=>'id'));
		
		/* if (Yii::app()->controller->dbActive){
			$items = AuthItemModel::model()->with('parent', 'child')->findAll(array('order'=>'id'));
// 			$assignment = AuthAssignmentModel::model()->findAll();
			$assignment = array_merge(AuthAssignmentModel::model()->findAll(array('limit'=>8000)),
				AuthAssignmentModel::model()->findAll(array('offset'=>8000, 'limit'=>8000)));
			
			if ($items && $assignment){
				foreach ($items as $item){
					foreach ($item->parent as $row)
						$this->_parents[$row->parent][$row->child] = $this->_childs[$row->child][$row->parent] = 0;
					
					foreach ($item->child as $row)
						$this->_parents[$row->parent][$row->child] = $this->_childs[$row->child][$row->parent] = 0;
					
					$item = $item->attributes;
					$item['data'] = $item['data'] === 'N;' || ($data = @unserialize($item['data'])) === false ? null : $data;
					$this->_items[$item['name']] = $item;
				}
				
				foreach ($assignment as $row)
					$this->_assignments[$row->userid][] = $row->itemname;
			}
			
			if ($this->_parents)
				foreach ($this->_parents as $parent=>$childs)
					$this->_parents[$parent] = array_keys($childs);
			
			if ($this->_childs)
				foreach ($this->_childs as $child=>$parents)
					$this->_childs[$child] = array_keys($parents);
		} */
	}
	
	/**
	 * Метод-геттер для определения _items
	 * return array Ассоциативный массив с информацией об элементах авторизации
	 */
	public function getItems(){
		return $this->_items;
	}
	
	/**
	 * Метод-геттер для определения _childs
	 * return array Ассоциативный массив связей дочерних элементов авторизации
	 */
	/* public function getChilds(){
		return $this->_childs;
	} */
	
	/**
	 * Метод-геттер для определения _assignments
	 * return array Ассоциативный массив с информацией о назначенных ролях пользователей
	 */
	/* public function getAssignments(){
		return $this->_assignments;
	} */
	
	/**
	 * Метод-геттер для определения _parents
	 * return array Ассоциативный массив связей родительских элементов авторизации
	 */
	/* public function getParents(){
		return $this->_parents;
	} */
	
	/**
	 * Метод-геттер для определения _removedUsers
	 * return array Список идентификаторов пользователей, которых необходимо удалить
	 */
	public function getRemovedUsers(){
		return $this->_removedUsers;
	}
	
	/**
	 * Метод-геттер для определения _baseRoles
	 * return array Список базовых ролей пользователей
	 */
	public function getBaseRoles(){
		if ($this->_baseRoles === null)
			$this->_baseRoles = array_values(array_diff(array_keys(Yii::app()->authPhp->roles), array('guest')));
		
		return $this->_baseRoles;
	}
	
	/**
	 * Подготовка к развёртыванию
	 * @param string $section Название группы таблиц
	 * @param string $type Символьный идентификатор типа предстоящего развёртывания
	 * @param array $params Ассоциативный массив с параметрами развёртывания приложения
	 * @return boolean Успешность выполнения
	 */
	public function beforeDeploy($section, $type, $params){
		try{
			if ($type === 'standart'){
				if ($params['user']['type'] === 'old' && $params[$section]['check'])
					if ($assignments = AuthAssignmentModel::model()->findAllSelective(false, 'userid, itemname')){
						$base_roles = $this->getBaseRoles();
						
						foreach ($assignments as $assignment)
							if (in_array($assignment['itemname'], $base_roles, true))
								$this->_usersRoles[$assignment['userid']][] = $assignment['itemname'];
							else
								$this->_removedUsers[] = $assignment['userid'];
						
						if ($this->_removedUsers && $this->_usersRoles)
							$this->_removedUsers = array_diff($this->_removedUsers, array_keys($this->_usersRoles));
					}
				
				if (!Yii::app()->table->dropGroupsTables($section))
					throw new CHttpException(500);
			}else{
				if ($params['user']['type'] !== 'old'){
					$roles = Yii::app()->user->getRoles(0);
					
					if ($roles && ($base_roles = $this->getBaseRoles()))
						$this->_baseRoles = array_intersect($base_roles, $roles);
					
					if (!Yii::app()->table->dropTable('auth_assignment') ||
						!AuthAccessAttributeModel::model()->deleteAll('operator < 0 OR owner < 0'))
							throw new CHttpException(500);
				}
				
				if ($params['directory']['type'] === 'standart' && !Yii::app()->table->dropForeignKeys('auth_access_attribute'))
					throw new CHttpException(500);
			}
		}catch (CException $e){
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('deploy_group_before', array(
			'section'=>$section,
			'type'=>$type,
			'params'=>$params
		), $ok ? 'ok' : 'deploy_group_before');
		
		return $ok;
	}
	
	/**
	 * Развёртывание группы таблиц "auth", отвечающей за ролевую схему приложения.
	 * Таблица зависимостей от группы таблиц "user":
	 * 	auth	user	cfg		dev		base	rand	removed_users		user_roles	base_roles
	 * 	--------------------------------------------------------------------------
	 * 	new		new		+			+			+			+
	 * 	new		old		+												+								+
	 * 	old		new					+			+			+																	+
	 * 	old		old
	 * @param string $section Название группы таблиц
	 * @param string $type Символьный идентификатор типа развёртывания
	 * @param array $params Ассоциативный массив с параметрами развёртывания приложения
	 * @return boolean Успешность выполнения
	 */
	public function deploy($section, $type, $params){
		Swamper::timer($tick);
		
		try{
			if ($type === 'standart'){
				if (!Yii::app()->table->createGroupsTables($section) || (!$cfg = Yii::app()->authPhp->cfg))
					throw new CHttpException(500);
				
				$rows = array(array('name', 'type', 'description', 'bizrule', 'data'));
				
				foreach ($cfg as $name=>$data){
					$rows[] = array($name, $data['type'], $data['description'],
						$name === 'developer' ? null : $data['bizRule'], $data['data'] ? $data['data'] : 'N;');
					
					if (!empty($data['children']))
						$childrens[$name] = $data['children'];
				}
				
				if (!Yii::app()->db->createCommand()->insert('auth_item', $rows))
					throw new CHttpException(500);
				
				if (isset($childrens)){
					$rows = array(array('parent', 'child'));
					
					foreach ($childrens as $parent=>$childs)
						foreach ($childs as $child)
							$rows[] = array($parent, $child);
					
					if (!Yii::app()->db->createCommand()->insert('auth_item_child', $rows))
						throw new CHttpException(500);
				}
				
				if (!Yii::app()->accessAttribute->deploy('auth_access_attribute'))
					throw new CHttpException(500);
			}else{
				if ($params['user']['type'] !== 'old' && !Yii::app()->table->createTable('auth_assignment'))
					throw new CHttpException(500);
				
				if ($params['directory']['type'] === 'standart' && !Yii::app()->table->addForeignKeys('auth_access_attribute'))
					throw new CHttpException(500);
			}
			
			if ($params['user']['type'] !== 'old'){
				$id = 1;
				$base_roles = array_merge(array_diff($this->getBaseRoles(), array('bss1_payer', 'bss2_payer', 'esia')), array('payer'));
				
				if (($key = array_search('developer', $base_roles)) !== false){
					$assignments[] = array('citizen', $id, 'N;');
					$assignments[] = array('developer', $id++, 'N;');
					unset($base_roles[$key]);
				}
				
				if ($params['user']['type'] === 'generation' && $params['generation'] && $base_roles){
					if (in_array(Yii::app()->conformity->get('user_generation_type', 'code', 'base'), $params['generation']))
						foreach ($base_roles as $role){
							if (!in_array($role, array('citizen', 'organization')))
								$assignments[] = array('citizen', $id, 'N;');
							
							$assignments[] = array($role === 'payer' ? 'bss2_payer' : $role, $id++, 'N;');
						}
					
					if (in_array(Yii::app()->conformity->get('user_generation_type', 'code', 'rand'), $params['generation']))
						for ($count = 0; $count < $params['quantity']; $count++)
							$assignments[] = array('citizen', $id++, 'N;');
				}
			}elseif ($type === 'standart' && $this->_usersRoles)
				foreach ($this->_usersRoles as $id=>$roles)
					foreach ($roles as $role)
						$assignments[] = array($role, $id, 'N;');
			
			if (!empty($assignments)){
				array_unshift($assignments, array('itemname', 'userid', 'data'));
				
				if (!Yii::app()->db->createCommand()->insert('auth_assignment', $assignments))
					throw new CHttpException(500);
			}
		}catch (CException $e){
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('deploy_group', array(
			'section'=>$section,
			'type'=>$type,
			'params'=>$params,
			'timer'=>Swamper::timer($tick)
		), $ok ? 'ok' : 'deploy_group');
		
		return $ok;
	}
	
	/**
	 * Переопределение стандартного метода executeBizRule, осуществляющего проверку бизнес-правил
	 * @param string $bizRule Бизнес-правило для выполнения
	 * @param array $params Список параметров бизнес-правила
	 * @param mixed $data Дополнительные параметры
	 * @see CAuthManager::executeBizRule()
	 * @return boolean Результат проверки
	 */
	public function executeBizRule($bizRule, $params, $data){
		return parent::executeBizRule($bizRule ? "return $bizRule;" : $bizRule, $params, $data);
	}
	
	/**
	 * Переопределение стандартного метода addItemChild, осуществляющего регистрацию дочернего элемента доступа
	 * @param string $itemName Название родительского элемента доступа
	 * @param string $childName Название дочернего элемента доступа
	 * @return boolean Успешность выполнения
	 * @throws CException
	 * @see CDbAuthManager::addItemChild()
	 */
	/* public function addItemChild($itemName, $childName){
		if ($itemName === $childName)
			throw new CException(Yii::t('yii', 'Cannot add "{name}" as a child of itself.', array('{name}'=>$itemName)));
		
		if (($item = Arrays::pop($this->_items, $itemName)) && ($child = Arrays::pop($this->_items, $childName))){
			$parentType = $item['type'];
			$childType = $child['type'];
			
			$this->checkItemChildType($parentType, $childType);
			
			if ($this->detectLoop($itemName, $childName))
				throw new CException(Yii::t('yii', 'Cannot add "{child}" as a child of "{name}". A loop has been detected.',
					array('{child}'=>$childName, '{name}'=>$itemName)));
			
			$this->db->createCommand()->insert($this->itemChildTable, array('parent'=>$itemName, 'child'=>$childName));
			
			return true;
		}else
			throw new CException(Yii::t('yii', 'Either "{parent}" or "{child}" does not exist.',
				array('{child}'=>$childName, '{parent}'=>$itemName)));
	} */
	
	/**
	 * Переопределение стандартного метода hasItemChild, проверяющего соответствие дочернего и родительского элементов доступа
	 * @param string $itemName Название родительского элемента доступа
	 * @param string $childName Название дочернего элемента доступа
	 * @return boolean Результат проверки
	 * @see CDbAuthManager::hasItemChild()
	 */
	/* public function hasItemChild($itemName, $childName){
		return ($parents = Arrays::pop($this->_childs, $childName)) && in_array($itemName, $parents, true);
	} */
	
	/**
	 * Переопределение стандартного метода getItemChildren, возвращающего дочерние элементы доступа для заданных родительских
	 * @param mixed $names Название родительского элемента доступа. Можно задавать сразу несколько элементов в массиве или
	 * в строке через запятую
	 * @return array|boolean Ассоциативный массив дочерних элементов в формате
	 * {Название элемента доступа}=>{Объект элемента доступа (CAuthItem)} или false в случае ошибки
	 * @see CDbAuthManager::getItemChildren()
	 */
	/* public function getItemChildren($names){
		if (is_string($names))
			$names = Strings::devisionWords($names);
		elseif (!is_array($names))
			return false;
		
		$children = array();
		
		foreach ($names as $name)
			if ($parent = Arrays::pop($this->_parents, $name))
				foreach ($parent as $item_name)
					if ($item = Arrays::pop($this->_items, $item_name))
						$children[$item_name] = new CAuthItem($this, $item_name, $item['type'], $item['description'],
							$item['bizrule'], $item['data']);
		
		return $children;
	} */
	
	/**
	 * Переопределение стандартного метода assign, привязывающего к некоторому пользователю элемент доступа
	 * @param string $itemName Название элемента доступа
	 * @param mixed $userId Цифровой идентификатор пользователя
	 * @param string $bizRule Бизнес-правило для выполнения дополнительной проверки
	 * @param mixed $data Дополнительные параметры
	 * @return boolean|CAuthAssignment Объект назначения элемента доступа или false в случае ошибки
	 * @see CDbAuthManager::assign()
	 */
	/* public function assign($itemName, $userId, $bizRule = null, $data = null){
		if ($assignment = parent::assign($itemName, $userId, $bizRule, $data)){///!!getAuthItem
			$this->_assignments[$userId][] = $itemName;
			
			return $assignment;
		}else
			return false;
	} */
	
	/**
	 * Переопределение стандартного метода revoke, отвязывающего от некоторого пользователя элемент доступа
	 * @param string $itemName Название элемента доступа
	 * @param mixed $userId Цифровой идентификатор пользователя
	 * @return boolean Успешность выполнения
	 * @see CDbAuthManager::revoke()
	 */
	/* public function revoke($itemName, $userId){
		if ($result = parent::revoke($itemName, $userId))
			if (($assignments = Arrays::pop($this->_assignments, $userId)) &&
				($key = array_search($itemName, $assignments)) !== false){
					unset($this->_assignments[$userId][$key]);
					
					if (count($assignments) == 1)
						unset($this->_assignments[$userId]);
				}
		
		return $result;
	} */
	
	/**
	 * Переопределение стандартного метода isAssigned, проверяющего привязку некоторого пользователя к элементу доступа
	 * @param string $itemName Название элемента доступа
	 * @param mixed $userId Цифровой идентификатор пользователя
	 * @return boolean Результат проверки
	 * @see CDbAuthManager::isAssigned()
	 */
	/* public function isAssigned($itemName, $userId){
		return ($assignments = Arrays::pop($this->_assignments, $userId)) && in_array($itemName, $assignments);
	} */
	
	/**
	 * Переопределение стандартного метода getAuthAssignment, возвращающего объект назначения элемента доступа
	 * @param string $itemName Название элемента доступа
	 * @param mixed $userId Цифровой идентификатор пользователя
	 * @return CAuthAssignment|null Объект назначения данного элемента доступа или null в случае ошибки
	 * @see CDbAuthManager::getAuthAssignment()
	 */
	/* public function getAuthAssignment($itemName, $userId){
		return $this->isAssigned($itemName, $userId) ? new CAuthAssignment($this, $itemName, $userId) : null;
	} */
	
	/**
	 * Переопределение стандартного метода getAuthAssignments, возвращающего объекты назначения элементов доступа, привязанных
	 * к данному пользователю
	 * @param mixed $userId Цифровой идентификатор пользователя
	 * @return array Список объектов назначения
	 * @see CDbAuthManager::getAuthAssignments()
	 */
	/* public function getAuthAssignments($userId){
		$assignments = array();
		
		if ($names = Arrays::pop($this->_assignments, $userId))
			foreach ($names as $name)
				$assignments[$name] = new CAuthAssignment($this, $name, $userId);
		
		return $assignments;
	} */
	
	/**
	 * Переопределение стандартного метода getAuthItems, возвращающего список объектов элементов доступа с фильтром
	 * по типу и привязанному пользователю 
	 * @param integer $type Код типа элемента доступа. По умолчанию равно null
	 * @param mixed $userId Цифровой идентификатор пользователя. По умолчанию равно null
	 * @return array Список объектов элементов доступа (CAuthItem)
	 * @see CDbAuthManager::getAuthItems()
	 */
	/* public function getAuthItems($type = null, $userId = null){
		$items = array();
		
		if ($recs = $this->_items){
			if ($userId)
				if ($assignments = Arrays::pop($this->_assignments, $userId))
					$recs = Arrays::filterKey($recs, $assignments);
				else
					$recs = array();
			
			foreach ($recs as $name=>$features)
				if ($type === null || $features['type'] === $type)
					$items[$name] = new CAuthItem($this, $name, $features['type'], $features['description'],
						$features['bizrule'], $features['data']);
		}
		
		return $items;
	} */
	
	/**
	 * Переопределение стандартного метода createAuthItem, создающего элемент доступа с заданными параметрами
	 * @param string $name Название элемента доступа
	 * @param integer $type Код типа элемента доступа
	 * @param string $description Описание элемента доступа. По умолчанию равно ''
	 * @param string $bizRule Бизнес-правило для выполнения дополнительной проверки. По умолчанию равно null
	 * @param mixed $data Дополнительные параметры. По умолчанию равно null
	 * @return boolean|CAuthItem Объект элемента доступа или false в случае ошибки
	 * @see CDbAuthManager::createAuthItem()
	 */
	/* public function createAuthItem($name, $type, $description = '', $bizRule = null, $data = null){
		$builder = Yii::app()->db->getCommandBuilder();
		$table = "{{{$this->itemTable}}}";
		$command = $builder->createInsertCommand($table, array(
			'name'=>$name,
			'type'=>$type,
			'description'=>$description,
			'bizrule'=>$bizRule,
			'data'=>serialize($data)
		));
		
		if ($command->execute()){
			$this->_items[$name] = array(
				'id'=>$builder->getLastInsertID($table),
				'name'=>$name,
				'type'=>$type,
				'description'=>$description,
				'bizrule'=>$bizRule,
				'data'=>$data
			);
			
			return new CAuthItem($this, $name, $type, $description, $bizRule, $data);
		}else
			return false;
	} */
	
	/**
	 * Переопределение стандартного метода removeAuthItem, удаляющего элемент доступа
	 * @param string $name Название элемента доступа
	 * @return boolean Успешность выполнения
	 * @see CDbAuthManager::removeAuthItem()
	 */
	/* public function removeAuthItem($name){
		if ($result = parent::removeAuthItem($name)){
			unset($this->_items[$name]);
			unset($this->_childs[$name]);
			unset($this->_parents[$name]);
			
			foreach ($this->_childs as $child=>$parents)
				if (($key = array_search($name, $parents)) !== false){
					unset($this->_childs[$child][$key]);
					
					if (count($parents) == 1)
						unset($this->_childs[$child]);
				}
			
			foreach ($this->_parents as $parent=>$childs)
				if (($key = array_search($name, $childs)) !== false){
					unset($this->_parents[$parent][$key]);
					
					if (count($childs) == 1)
						unset($this->_parents[$parent]);
				}
			
			foreach ($this->_assignments as $id=>$assignments)
				if (($key = array_search($name, $assignments)) !== false){
					unset($this->_assignments[$id][$key]);
					
					if (count($assignments) == 1)
						unset($this->_assignments[$id]);
				}
		}
		
		return $result;
	} */
	
	/**
	 * Переопределение стандартного метода getAuthItem, возвращающего элемент доступа
	 * @param string $name Название элемента доступа
	 * @return CAuthItem|null Объект элемента доступа или null в случае ошибки
	 * @see CDbAuthManager::getAuthItem()
	 */
	/* public function getAuthItem($name){
		return ($item = Arrays::pop($this->_items, $name)) ?
			new CAuthItem($this, $name, $item['type'], $item['description'], $item['bizrule'], $item['data']) : null;
	} */
	
	/**
	 * Переопределение стандартного метода saveAuthItem, сохраняющего изменения элемента доступа
	 * @param CAuthItem $item Объект элемента доступа
	 * @param string $oldName Старое название элемента доступа. По умолчанию равно null
	 * @see CDbAuthManager::saveAuthItem()
	 */
	/* public function saveAuthItem($item, $oldName = null){
		parent::saveAuthItem($item, $oldName);
		
		$name = $item->getName();
		
		if ($rec = Arrays::pop($this->_items, $oldName ? $oldName : $name)){
			if ($oldName){
				unset($this->_items[$oldName]);
				
				if ($parents = Arrays::pop($this->_childs, $oldName, true))
					$this->_childs[$name] = $parents;
				
				if ($childs = Arrays::pop($this->_parents, $oldName, true))
					$this->_parents[$name] = $childs;
				
				foreach ($this->_childs as $child=>$parents)
					if (($key = array_search($oldName, $parents)) !== false)
						$this->_childs[$child][$key] = $name;
				
				foreach ($this->_parents as $parent=>$childs)
					if (($key = array_search($oldName, $childs)) !== false)
						$this->_parents[$parent][$key] = $name;
				
				foreach ($this->_assignments as $id=>$assignments)
					if (($key = array_search($oldName, $assignments)) !== false)
						$this->_assignments[$id][$key] = $name;
			}
			
			$this->_items[$name] = array(
				'id'=>$rec['id'],
				'name'=>$name,
				'type'=>$item->getType(),
				'description'=>$item->getDescription(),
				'bizrule'=>$item->getBizRule(),
				'data'=>$item->getData()
			);
		}
	} */
	
	/**
	 * Переопределение стандартного метода clearAll, очищающего все авторизационные данные
	 * @see CDbAuthManager::clearAll()
	 */
	/* public function clearAll(){
		parent::clearAll();
		
		$this->_items = $this->_childs = $this->_parents = array();
	} */
	
	/**
	 * Переопределение стандартного метода clearAuthAssignments, очищающего список всех назначений пользователей
	 * @see CDbAuthManager::clearAuthAssignments()
	 */
	/* public function clearAuthAssignments(){
		parent::clearAuthAssignments();
		
		$this->_assignments = array();
	} */
	
	/**
	 * Переопределение стандартного метода checkAccessRecursive, осуществляющего рекурсивную проверку права доступа
	 * @param string $itemName Название элемента доступа
	 * @param mixed $userId Цифровой идентификатор пользователя
	 * @param array $params Ассоциативный массив параметров для расчёта бизнес-правил
	 * @param array $assignments Список полномочий пользователя в формате
	 * {Название элемента доступа}=>{Объект назначения элемента доступа (CAuthAssignment)}
	 * @return boolean Результат проверки
	 * @see CDbAuthManager::checkAccessRecursive()
	 */
	/* protected function checkAccessRecursive($itemName, $userId, $params, $assignments){
		if (($item = $this->getAuthItem($itemName)) === null)
			return false;
		
		Yii::trace('Checking permission "'.$item->getName().'"', 'system.web.auth.CDbAuthManager');
		
		if (!isset($params['userId']))
			$params['userId'] = $userId;
		
		if ($this->executeBizRule($item->getBizRule(), $params, $item->getData())){
			if (in_array($itemName, $this->defaultRoles))
				return true;
			
			if (isset($assignments[$itemName])){
				$assignment = $assignments[$itemName];
				
				if ($this->executeBizRule($assignment->getBizRule(), $params, $assignment->getData()))
					return true;
			}
			
			if ($parents = Arrays::pop($this->_childs, $itemName))
				foreach ($parents as $parent)
					if ($this->checkAccessRecursive($parent, $userId, $params, $assignments))
						return true;
		}
		
		return false;
	} */
	
}
