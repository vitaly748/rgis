<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CComponent
 * @author v.zubov
 * @see CComponent
 */
class Component extends CComponent{
	
	/**
	 * Получение объекта модели данных для текущего компонента
	 * @param boolean $new Признак применения стандартного конструктора. По умолчанию равно false
	 * @return boolean|object Объект модели данных или false
	 */
	public function model($new = false){
		return BaseModel::getModel(($this instanceof ComponentDirectory ? 'Directory' : '').get_class($this).'Model', $new);
	}
	
}
