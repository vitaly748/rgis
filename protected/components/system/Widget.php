<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CWidget
 * @author v.zubov
 * @see CWidget
 */
class Widget extends CWidget{
	
	/**
	 * @var string Псевдоним пути к папке виджетов
	 */
	const DIR_WIDGETS = 'application.components.widgets';
	
	/**
	 * @var string Префикс автоматически генерируемых HTML-идентификаторов виджетов 
	 */
	const PREFIX_ID = 'ywp';
	
	/**
	 * @var array Список названий свойств с более привилегированной политикой присвоения. При передаче параметров
	 * от виджета к виджету эти параметры будут автоматически включаться в набор
	 */
	const TRANSITION_PROPERTIES = array('form', 'model', 'name', 'value');
	
	/**
	 * Название параметра дочернего класса, позволяющего задать список альтернативных названий свойств при
	 * передаче параметров от виджета к виджету. Например: 
	 * 	array('form'=>'featureForm', 'model'=>'featureModel')
	 * Если предполагается единый префикс для всех альтернативных параметров, то допускается указать просто префикс
	 * 
	 */
	const NAME_ALTERNATIVE_PROPERTIES = 'ALTERNATIVE_PROPERTIES';
	
	/**
	 * @var string Символьный идентификатор виджета
	 */
	private $_id;
	
	/**
	 * @var integer Счётчик виджетов
	 */
	private static $_counter = 0;
	
	/**
	 * Переопределение стандартного метода установки данных класса с целью добавлеления возможности подачи на вход
	 * параметров без необходимости их фильтрации на предмет существования в данном классе
	 * @param string $name Название параметра
	 * @param mixed $value Значение параметра
	 * @see CComponent::__set()
	 */
	public function __set($name, $value){
		if (property_exists($this, $name) || method_exists($this, 'set'.$name))
			parent::__set($name, $value);
		else{
			$const = 'static::'.self::NAME_ALTERNATIVE_PROPERTIES;
			
			if (defined($const))
				if ($properties = constant($const)){
					$alternative_name = is_string($properties) ? $properties.ucfirst($name) :
						(isset($properties[$name]) ? $properties[$name] : false);
					
					if ($alternative_name && property_exists($this, $alternative_name))
						$this->$alternative_name = $value;
				}
		}
	}
	
	/**
	 * Переопределение стандартного метода getId, возвращающего идентификатор виджета
	 * @param boolean $autoGenerate Признак автоматической генерации идентификатора
	 * @return string Идентификатор виджета
	 * @see CWidget::getId()
	 */
	public function getId($autoGenerate = true){
		if ($this->_id === null && $autoGenerate)
			$this->_id = self::getIdCounter();
		
		return $this->_id;
	}
	
	/**
	 * Переопределение стандартного метода setId, устанавливающего идентификатор виджета
	 * @param string $value Новый идентификатор виджета
	 */
	public function setId($value){
		$this->_id = $value;
	}
	
	/**
	 * Регистрация параметров данного виджета в ассоциативном массиве параметров виджета в скриптах
	 * @param array $params Параметры данного виджета
	 * @param array $params_base Базовые параметры виджета. По умолчанию равно false
	 * @param boolean|string $section Название блока добавления параметров или false. По умолчанию равно false
	 */
	public function registerScriptParams($params, $params_base = false, $section = false){
		Html::registerScriptParams(get_class($this), !empty($this->htmlOptions['id']) ? $this->htmlOptions['id'] : $this->id,
			$params, $params_base, $section, Html::PREFIX_SCRIPT_WIDGET);
	}
	
	/**
	 * Генерация идентификатора виджета с счётчиком
	 * @return integer Счётчик виджетов
	 */
	public static function getIdCounter(){
		return self::PREFIX_ID.self::$_counter++;
	}
	
	/**
	 * Сброс счётчика виджетов
	 */
	public static function resetCounter(){
		self::$_counter = 0;
	}
	
	/**
	 * Определение псевдонима пути виджета
	 * @param string $name Название класса виджета
	 * @return string Псевдоним пути виджета
	 */
	public static function alias($name){
		if (mb_strpos($name, '.') === false && !@class_exists($name))
			$name = self::DIR_WIDGETS.".$name.$name";
		
		return $name;
	}
	
	/**
	 * Подготовка HTML-опций для основного блока виджета
	 * @return array Ассоциативный массив HTML-опций
	 */
	protected function preparationHtmlOptions(){
		$html_options = !empty($this->htmlOptions) ? $this->htmlOptions : array();
		
		if (empty($html_options['id']))
			$html_options['id'] = $this->id;
		
		return $this->htmlOptions = Html::mergeClasses($html_options,
			Strings::nameModify(get_class($this), Strings::FORMAT_NAME_MODIFY_MIDDLE));
	}
	
}
