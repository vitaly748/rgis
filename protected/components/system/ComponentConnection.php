<?php
/**
 * Базовый класс для компонентов-подключений
 * @author v.zubov
 */
class ComponentConnection extends Component{
	
	/**
	 * @var array Конфигурация подключения
	 */
	protected $_cfg;
	
	/**
	 * @var boolean Признак использования подключения
	 */
	protected $_on;
	
	/**
	 * Конструктор компонента. Инициализация конфигурации подключения
	 */
	public function init(){
		$this->_cfg = Yii::app()->setting->get($this);
	}
	
	/**
	 * Выборка и получение данных конфигурации
	 * @param boolean|string $name Название параметра или false. По умолчанию равно false
	 * @return array|boolean|string Выбранные данные конфигурации или false
	 */
	public function get($name = false){
		if (!$name)
			return $this->_cfg;
		
		return isset($this->_cfg[$name]) ? $this->_cfg[$name] : false;
	}
	
	/**
	 * Установка данных конфигурации
	 * @param array $cfg Ассоциативный массив параметров
	 */
	public function set($cfg){
		if ($cfg && is_array($cfg))
			if (!$this->_cfg)
				$this->_cfg = $cfg;
			else
				foreach ($this->_cfg as $name=>$value)
					if (array_key_exists($name, $cfg))
						$this->_cfg[$name] = $cfg[$name];
	}
	
	/**
	 * Метод-геттер для определения _on
	 * @return boolean Признак использования подключения к Биллинговому центру
	 */
	public function getOn(){
		if ($this->_on === null)
			$this->_on = isset($this->_cfg['on']) ? $this->_cfg['on'] : false;
		
		return $this->_on;
	}
	
	/**
	 * Перенос данных из конфигурационного массива в БД
	 * @return boolean Успешность выполнения
	 */
	public function deploy(){
		return Yii::app()->setting->deploySection(mb_strtolower(get_class($this)), $this->_cfg);
	}
	
}
