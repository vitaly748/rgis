<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CUserIdentity
 * @author v.zubov
 * @see CUserIdentity
 */
class UserIdentity extends CUserIdentity{
	
	/**
	 * @var integer Код ошибки авторизации "Нет прав на приватную авторизацию"
	 */
	const ERROR_PRIVATE = 3;
	
	/**
	 * @var integer Код ошибки авторизации "Профиль пользователя ожидает активацию"
	 */
	const ERROR_CONFIRM = 4;
	
	/**
	 * @var integer Код ошибки авторизации "Пользователь заблокирован"
	 */
	const ERROR_LOCKED = 5;
	
	/**
	 * @var integer Код ошибки авторизации "Не удаётся проверить авторизацию"
	 */
	const ERROR_APPLICATION = 6;
	
	/**
	 * @var integer Код ошибки
	 */
	public $errorCode = self::ERROR_NONE;
	
	/**
	 * @var integer Идентификатор пользователя
	 */
	protected $_id;
	
	/**
	 * @var UserModel Модель данных профиля авторизующегося пользователя
	 */
	protected $_user;
	
	/**
	 * Переопределение стандартного конструктора
	 * @param integer|string $username Логин или идентификатор пользователя
	 * @param string $password Пароль пользователя или false. По умолчанию равно false
	 * @see CUserIdentity::__construct()
	 */
	public function __construct($username, $password = false){
		if (is_int($username))
			$this->_id = $username;
		else{
			$this->username = $username;
			$this->password = $password;
		}
	}
	
	/**
	 * Переопределение стандартного метода authenticate, проверяющего авторизацию пользователя
	 * @return boolean Успешность авторизации
	 * @see CUserIdentity::authenticate()
	 */
	public function authenticate(){
		$params_fix = array('login'=>$this->username);
		
		try{
			$model = UserModel::model()->find('LOWER(login) = :login', array(':login'=>mb_strtolower($this->username)));
			
			if ($model){
				$model->initRoles();
				$model->checkLocked();
				$this->_user = $model;
				$params_fix['user'] = $model->attributes;
				
				if (Hashing::check($this->password, $model)){
					if (Yii::app()->controller->privateArea && !Yii::app()->authDb->checkAccess('private', $model->id)){
						$this->errorCode = self::ERROR_PRIVATE;
						throw new CHttpException(403, 'error private access');
					}else{
						$state = Yii::app()->conformity->get('user_state', 'name', $model->state, false, false, 'code');
						
						if (mb_strpos($state, 'confirm') === 0){
							$this->errorCode = self::ERROR_CONFIRM;
							throw new CHttpException(403, 'error confirm account');
						}elseif ($state === 'locked'){
							$this->errorCode = self::ERROR_LOCKED;
							throw new CHttpException(403, 'error locked account');
						}elseif ($state != 'working'){
							$this->errorCode = self::ERROR_USERNAME_INVALID;
							throw new CHttpException(404, 'error username');
						}
					}
				}else{
					$this->errorCode = self::ERROR_PASSWORD_INVALID;
					throw new CHttpException(403, 'error password');
				}
			}else{
				$this->errorCode = self::ERROR_USERNAME_INVALID;
				throw new CHttpException(404, 'error username');
			}
			
			$this->_id = $model->id;
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		$params_fix['error_code'] = $this->errorCode;
		
		Yii::app()->log->fix('user_auth', $params_fix, $ok ? 'ok' : 'user_auth', $model ? $model->id : 0);
		
		return $ok;
	}
	
	/**
	 * Метод-геттер для определения _id
	 * @return integer Идентификатор пользователя
	 * @see CUserIdentity::getId()
	 */
	public function getId(){
		return $this->_id;
	}
	
	/**
	 * Метод-геттер для определения _user
	 * @return UserModel Данные профиля авторизующегося пользователя
	 */
	public function getUser(){
		return $this->_user;
	}
	
	/**
	 * Определение наличия ошибок идентификации
	 * @return boolean Признак наличия ошибок
	 */
	/* public function hasErrors(){
		return $this->errorCode !== self::ERROR_NONE;
	} */
	
}
