<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CDbCommand
 * @author v.zubov
 * @see CDbCommand
 */
class DbCommand extends CDbCommand{
	
	/**
	 * @var integer Максимальное количество строк, добавляемых за один раз
	 */
	const MAX_ROW_INSERT = 50;
	
	/**
	 * @var string Шаблон поиска идентификатора записи в условии поиска
	 */
	const PATTERN_ID_CONDITIONS = '/id\s*=\s([:\w]+)/u';
	
	/**
	 * @var string Псевдоним таблицы. Нужен только для совместимости с классом CDbCriteria
	 * @see CDbCriteria::alias
	 */
	public $alias;
	
	/**
	 * @var array Ассоциативный массив параметров SQL-запроса
	 * @see CDbCommand::_query
	 */
	protected $_query;
	
	/**
	 * Переопределение стандартного метода createTable, создающего таблицы
	 * @param string $table Название создаваемой таблицы
	 * @param array $columns Ассоциативный массив с информацией о столбцах таблицы
	 * @param string $options Дополнительные опции
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 * @see CDbCommand::createTable()
	 */
	public function createTable($table, $columns, $options = null, $fix = true){
		$params_fix = array('table'=>$table, 'columns'=>$columns, 'options'=>$options);
		
		try{
			parent::createTable("{{{$table}}}", $columns, $options);
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_table_create', $params_fix, $ok ? 'ok' : 'db_sql');
		
		return $ok;
	}
	
	/**
	 * Переопределение стандартного метода dropTable, удаляющего таблицы
	 * @param string $table Название удаляемой таблицы
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 * @see CDbCommand::dropTable()
	 */
	public function dropTable($table, $fix = true){
		$params_fix = array('table'=>$table);
		
		try{
			parent::dropTable("{{{$table}}}");
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_table_drop', $params_fix, $ok ? 'ok' : 'db_sql');
		
		return $ok;
	}
	
	/**
	 * Переопределение стандартного метода truncateTable, очищающего таблицы
	 * @param string $table Название удаляемой таблицы
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 * @see CDbCommand::truncateTable()
	 */
	public function truncateTable($table, $fix = true){
		$params_fix = array('table'=>$table);
		
		try{
			parent::truncateTable("{{{$table}}}");
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_table_truncate', $params_fix, $ok ? 'ok' : 'db_sql');
		
		return $ok;
	}
	
	/**
	 * Переопределение стандартного метода insert, вставляющего строки в таблицы
	 * @param string $table Название изменяемой таблицы
	 * @param array $columns Массив с информацией о столбцах таблицы. Допускается как стандартный формат для
	 * вставки одной строки:
	 * 	array(
	 * 		'attribute1'=>value1,
	 * 		...
	 *	)
	 * Так и расширенный для вставки нескольких строк:
	 *	array(
	 *		array('attribute1', 'attribute2', ...),// Названия изменяемых атрибутов
	 *		array(value1, value2, ...),// Значения изменяемых атрибутов строки 1
	 *		array(value1, value2, ...),// Значения изменяемых атрибутов строки 2
	 * 		...
	 *	)
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 * @see CDbCommand::insert()
	 */
	public function insert($table, $columns, $fix = true){
		$params_fix = array('table'=>$table, 'columns'=>$columns);
		
		try{
			if (is_numeric(reset(array_keys($columns)))){
				$names = array_shift($columns);
				$format = "INSERT INTO {{{$table}}} (".implode(', ', $names).") VALUES (%s)";
				$bufer = array();
				$cur_bufer = $cur_column = 0;
				
				foreach ($columns as $column){
					foreach ($column as $key_feature=>$feature){
						$bufer[$cur_bufer]['names'][$cur_column][] = $name = ':'.$names[$key_feature].$cur_column;
						$bufer[$cur_bufer]['values'][$name] = $feature;
					}
					
					$bufer[$cur_bufer]['names'][$cur_column] = implode(', ', $bufer[$cur_bufer]['names'][$cur_column]);
					
					if (++$cur_column == self::MAX_ROW_INSERT){
						$cur_column = 0;
						$cur_bufer++;
					}
				}
				
				foreach ($bufer as $bufer_columns)
					$this->setText(sprintf($format, implode('), (', $bufer_columns['names'])))->
						bindValues($bufer_columns['values'])->execute();
				
				$result = true;
			}else
				$result = parent::insert("{{{$table}}}", $columns);
			
			if (!empty($this->connection->queryCacheID) &&
				($cache = Yii::app()->getComponent($this->connection->queryCacheID)) !== null && $cache instanceof FileCache &&
				!$cache->cleaning('sql', 'insert', array($table)))
					throw new CHttpException(500, 'cache_cleaning');
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_rows_insert', $params_fix, $ok ? 'ok' : (!isset($result) ? 'db_sql' : 'cache_cleaning'));
		
		return $ok;
	}
	
	/**
	 * Переопределение стандартного метода update, изменяющего строки в таблице. Используется так же для установки
	 * SQL-параметра "update"
	 * @param string $table Название изменяемой таблицы
	 * @param array $columns Ассоциативный массив с новыми данными атрибутов. По умолчанию равно false. Если равен false,
	 * то будет установлен SQL-параметр "update"
	 * @param mixed $conditions Условие поиска. По умолчанию равно ''
	 * @param array $params Массив параметров поиска. По умолчанию равно array()
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean|DbCommand Успешность выполнения или экземпляр текущего класса, если функция используется для установки
	 * SQL-параметра "update"
	 * @see CDbCommand::update()
	 */
	public function update($table, $columns = false, $conditions = '', $params = array(), $fix = true){
		if (!$columns){
			$this->_query['update'] = $this->connection->quoteTableName("{{{$table}}}");
			
			return $this;
		}
		
		$params_fix = array('table'=>$table, 'columns'=>$columns, 'conditions'=>$conditions, 'params'=>$params,
			'id'=>$id = $this->findIdInConditions($conditions, $params));
		
		try{
			$result = parent::update("{{{$table}}}", $columns, $conditions, $params);
			
			if (!empty($this->connection->queryCacheID) &&
				($cache = Yii::app()->getComponent($this->connection->queryCacheID)) !== null && $cache instanceof FileCache &&
				!$cache->cleaning('sql', $id ? 'update' : 'insert', array($table, $id)))
					throw new CHttpException(500, 'cache_cleaning');
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_row_update', $params_fix, $ok ? 'ok' : (!isset($result) ? 'db_sql' : 'cache_cleaning'));
		
		return $ok;
	}
	
	/**
	 * Выборочное изменение значений таблицы по типу ассоциативного массива. Для таблицы определяется пара столбцов, содержащих
	 * названия параметров и значения параметров, затем по названию параметров меняются необходимые значения
	 * @param string $table Название изменяемой таблицы
	 * @param string $column_name Название столбца таблицы, играющего роль названия параметров 
	 * @param array|string $column_value Название столбца таблицы, играющего роль значения параметров. Можно задать несколько
	 * столбцов значений параметров, указав их названия в виде массива или строки через запятую 
	 * @param array $data Ассоциативный массив новых значение в формате {Значение в колонке названия параметра}=>{Значение в
	 * колонке значения параметра}. Если нужно указать несколько значений для каждого параметра, то данный параметр должен
	 * представлять собой двухмерный массив в формате:
	 * 	array(
	 * 		{Значение в колонке названия параметра}=>array(
	 * 			{Значение в первой колонке значения параметра},
	 * 			{Значение во второй колонке значения параметра},
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 * @param mixed $conditions Условие поиска. По умолчанию равно ''
	 * @param array $params Массив параметров поиска. По умолчанию равно array()
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 */
	public function updateValues($table, $column_name, $column_value, $data, $conditions = '', $params = array(), $fix = true){
		$params_fix = array('table'=>$table, 'column_name'=>$column_name, 'column_value'=>$column_value, 'data'=>$data);
		
		try{
			if (!$table || !$column_name || !$column_value || !$data)
				throw new CHttpException(500, 'empty param');
			
			if (is_string($column_value))
				$column_value = Strings::devisionWords($column_value);
			
			$names = array($column_name);
			$where = "{{{$table}}}.$column_name = data.$column_name";
			
			foreach ($column_value as $column){
				$set[$column] = 'data.'.$column;
				$names[] = $column;
			}
			
			foreach ($data as $name=>$value){
				$data_columns[0][] = $name;
				
				foreach (is_array($value) ? array_values($value) : array($value) as $key=>$value_column)
					$data_columns[$key + 1][] = $value_column;
			}
			
			foreach ($data_columns as $key=>$values)
				$data_columns[$key] = "UNNEST(ARRAY['".implode("', '", $values)."']) AS {$names[$key]}";
			
			if ($conditions)
				$where = array('and', $where, $conditions);
			
			$result = $this->
				update($table)->
				set($set)->
				from('('.
					$this->connection->createCommand()->
						select(implode(', ', $data_columns))->text.
					') AS data')->
				where($where)->
				execute($params);
			
			if (!empty($this->connection->queryCacheID) &&
				($cache = Yii::app()->getComponent($this->connection->queryCacheID)) !== null && $cache instanceof FileCache &&
				!$cache->cleaning('sql', 'insert', array($table)))
					throw new CHttpException(500, 'cache_cleaning');
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_row_update', $params_fix, $ok ? 'ok' : (!isset($result) ? 'db_sql' : 'cache_cleaning'));
		
		return $ok;
	}
	
	/**
	 * Установка SQL-параметра "set"
	 * @param array $columns Ассоциативный массив устанавливаемых параметров
	 * @return DbCommand Экземпляр текущего класса
	 */
	public function set($columns){
		if (is_array($columns)){
			foreach ($columns as $name=>$value)
				$this->_query['set'][] = "$name = $value";
		}
		
		return $this;
	}
	
	/**
	 * Переопределение стандартного метода delete, удаляющего строки из таблицы
	 * @param string $table Название изменяемой таблицы
	 * @param mixed $conditions Условие поиска. По умолчанию равно ''
	 * @param array $params Массив параметров поиска. По умолчанию равно array()
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 * @see CDbCommand::delete()
	 */
	public function delete($table, $conditions = '', $params = array(), $fix = true){
		$params_fix = array('table'=>$table, 'conditions'=>$conditions, 'params'=>$params,
			'id'=>$id = $this->findIdInConditions($conditions, $params));
		
		try{
			$result = parent::delete("{{{$table}}}", $conditions, $params);
			
			if (!empty($this->connection->queryCacheID) &&
				($cache = Yii::app()->getComponent($this->connection->queryCacheID)) !== null && $cache instanceof FileCache &&
				!$cache->cleaning('sql', $id ? 'update' : 'insert', array($table, $id)))
					throw new CHttpException(500, 'cache_cleaning');
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_row_delete', $params_fix, $ok ? 'ok' : (!isset($result) ? 'db_sql' : 'cache_cleaning'));
		
		return $ok;
	}
	
	/**
	 * Переопределение стандартного метода queryAll, выполяющего SQL-запросы по получению строк
	 * @param boolean $fetchAssociative Признак ассоциативности финального массива. По умолчанию равно true
	 * @param array $params Массив параметров поиска. По умолчанию равно array()
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно false
	 * @return array|boolean Массив найденных строк или false в случае ошибки
	 * @throws CHttpException
	 * @see CDbCommand::queryAll()
	 */
	public function queryAll($fetchAssociative = true, $params = array(), $fix = false){
		$params_fix = array('sql'=>$this->getText(), 'fetch_associative'=>$fetchAssociative, 'params'=>$params);
		
		try{
			$result = parent::queryAll($fetchAssociative, $params);
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_query', $params_fix, $ok ? 'ok' : 'db_sql');
		
		if (!$ok)
			throw new CHttpException(500, $params_fix['msg']);
		
		return $result;
	}
	
	/**
	 * Переопределение стандартного метода queryColumn, выполняющего SQL-запросы и получающего первый столбец результата
	 * @param array $params Массив параметров поиска. По умолчанию равно array()
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return array|boolean Массив с данными найденного столбца или false в случае ошибки
	 * @throws CHttpException
	 * @see CDbCommand::queryColumn()
	 */
	public function queryColumn($params = array(), $fix = true){
		$params_fix = array('sql'=>$this->getText(), 'params'=>$params);
		
		try{
			$result = parent::queryColumn($params);
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_query', $params_fix, $ok ? 'ok' : 'db_sql');
		
		if (!$ok)
			throw new CHttpException(500, $params_fix['msg']);
		
		return $result;
	}
	
	/**
	 * Переопределение стандартного метода queryRow, выполняющего SQL-запросы и получающего первую строку результата
	 * @param boolean $fetchAssociative Признак ассоциативности финального массива. По умолчанию равно true
	 * @param array $params Массив параметров поиска. По умолчанию равно array()
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return array|boolean Массив с данными найденной строки или false в случае ошибки
	 * @throws CHttpException
	 * @see CDbCommand::queryRow()
	 */
	public function queryRow($fetchAssociative = true, $params = array(), $fix = true){
		$params_fix = array('sql'=>$this->getText(), 'fetch_associative'=>$fetchAssociative, 'params'=>$params);
		
		try{
			$result = parent::queryRow($fetchAssociative, $params);
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_query', $params_fix, $ok ? 'ok' : 'db_sql');
		
		if (!$ok)
			throw new CHttpException(500, $params_fix['msg']);
		
		return $result;
	}
	
	/**
	 * Переопределение стандартного метода buildQuery, генерирующего строку SQL-запроса
	 * @param array $query Ассоциативный массив параметров SQL-запроса
	 * @return string Текст SQL-запроса
	 * @see CDbCommand::buildQuery
	 */
	public function buildQuery($query){
		if ($update = Arrays::pop($this->_query, 'update')){
			$sql = "UPDATE $update";
			
			if ($set = Arrays::pop($this->_query, 'set'))
				$sql .= ' SET '.implode(', ', $set);
		}else{
			$sql = !empty($query['distinct']) ? 'SELECT DISTINCT' : 'SELECT';
			$sql .= ' '.(!empty($query['select']) ? $query['select'] : '*');
		}
		
		if (!empty($query['from']))
			$sql .= "\nFROM ".$query['from'];
		
		if (!empty($query['join']))
			$sql .= "\n".(is_array($query['join']) ? implode("\n", $query['join']) : $query['join']);
		
		if (!empty($query['where']))
			$sql .= "\nWHERE ".$query['where'];
		
		if (!empty($query['group']))
			$sql .= "\nGROUP BY ".$query['group'];
		
		if (!empty($query['having']))
			 $sql.= "\nHAVING ".$query['having'];
		
		if (!empty($query['union']))
			$sql .= "\nUNION (\n".(is_array($query['union']) ? implode("\n) UNION (\n", $query['union']) : $query['union']) . ')';
		
		if (!empty($query['order']))
			$sql .= "\nORDER BY ".$query['order'];
		
		$limit = isset($query['limit']) ? (int)$query['limit'] : -1;
		$offset = isset($query['offset']) ? (int)$query['offset'] : -1;
		
		if ($limit >= 0 || $offset > 0)
			$sql = $this->connection->getCommandBuilder()->applyLimit($sql, $limit, $offset);
		
		return $sql;
	}
	
	/**
	 * Переопределение стандартного метода addForeignKey, выполняющего добавление внешнего ключа таблицы
	 * @param string $name Название внешнего ключа
	 * @param string $table Название таблицы
	 * @param string $columns Список названий колонок, входящих в состав ключа
	 * @param string $refTable Название внешней таблицы
	 * @param string $refColumns Список названий колонок внешней таблицы, входящих в состав ключа
	 * @param string $delete Тип ключа при удалении данных. По умолчанию равно null
	 * @param string $update Тип ключа при изменении данных. По умолчанию равно null
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 * @see CDbCommand::addForeignKey()
	 */
	public function addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete = null, $update = null,
		$fix = true){
			$params_fix = array('name'=>$name, 'table'=>$table, 'columns'=>$columns, 'refTable'=>$refTable,
				'refColumns'=>$refColumns, 'delete'=>$delete, 'update'=>$update);
			
			try{
				parent::addForeignKey($name, "{{{$table}}}", $columns, "{{{$refTable}}}", $refColumns, $delete, $update);
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			if ($fix)
				Yii::app()->log->fix('db_fkey_add', $params_fix, $ok ? 'ok' : 'db_sql');
			
			return $ok;
		}
	
	/**
	 * Переопределение стандартного метода dropForeignKey, выполняющего сброс внешнего ключа таблицы
	 * @param string $name Название внешнего ключа
	 * @param string $table Название таблицы
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 * @see CDbCommand::dropForeignKey()
	 */
	public function dropForeignKey($name, $table, $fix = true){
		$params_fix = array('name'=>$name, 'table'=>$table);
		
		try{
			parent::dropForeignKey($name, "{{{$table}}}");
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_fkey_drop', $params_fix, $ok ? 'ok' : 'db_sql');
		
		return $ok;
	}
	
	/**
	 * Переопределение стандартного метода execute, выполняющего SQL-запросы
	 * @param array $params Массив параметров поиска. По умолчанию равно array()
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно false
	 * @return boolean|integer Количество изменённых строк или false в случае ошибки
	 * @throws CHttpException
	 * @see CDbCommand::execute()
	 */
	public function execute($params = array(), $fix = false){
		$params_fix = array('sql'=>$this->getText(), 'params'=>$params);
		
		try{
			$result = parent::execute($params);
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_query', $params_fix, $ok ? 'ok' : 'db_sql');
		
		if (!$ok)
			throw new CHttpException(500, $params_fix['msg']);
		
		return $result;
	}
	
	/**
	 * Переопределение стандартного метода from, заполняющего блок FROM SQL-запроса
	 * @param mixed $tables Название таблицы или таблиц, участвующих в SQL-запросе
	 * @return DbCommand Ссылка на данный объект
	 * @see CDbCommand::from()
	 */
	public function from($tables){
		$tables = preg_replace('/^([\w\d]+)/u', '{{$1}}', $tables);
		
		return parent::from($tables);
	}
	
	/**
	 * Переопределение стандартного метода leftJoin, заполняющего блок LEFT OUTER JOIN SQL-запроса
	 * @param array|boolean|string $table Название прикрепляемой таблицы. Может задаваться в виде ассоциативного массива
	 * связей сопряжения в формате:
	 * 	array(
	 * 		{Название таблицы}=>{Условия сопряжения},
	 * 		...
	 * 	)
	 * В этом случае параметр $conditions должен содержать название таблицы из этого массива. Если в массиве должно содержаться
	 * несколько связей с одной и той же таблицей, можно использовать в её названии мнимый символ "$". В строке условия можно
	 * использовать псевдоним "*" данной таблицы
	 * @param mixed $conditions Условия сопряжения таблиц или название таблицы
	 * @param array $params Массив параметров сопряжения. По умолчанию равно array()
	 * @return DbCommand Ссылка на данный объект
	 * @see CDbCommand::leftJoin()
	 */
	public function leftJoin($table, $conditions = false, $params = array()){
		if (!$table)
			return $this;
		
		if (is_array($table)){
			$name = $conditions;
			$alias = str_replace('$', '', $name);
			$conditions = str_replace('*', $alias, $table[$name]);
			$table = "{{{$alias}}} $alias";
		}
		
		return parent::leftJoin($table, $conditions, $params);
	}
	
	/**
	 * Поиск идентификатора записи в условии запроса
	 * @param mixed $conditions Условие запроса
	 * @param array $params Параметры условия запроса
	 * @return boolean|integer Идентификатор записи или false
	 */
	protected function findIdInConditions($conditions, $params){
		if (is_string($conditions)){
			preg_match_all(self::PATTERN_ID_CONDITIONS, $conditions, $matches);
			
			if (isset($matches[1]) && count($matches[1]) == 1)
				if (mb_strpos($id = $matches[1][0], ':') !== false)
					$id = Arrays::pop($params, $id);
		}
		
		return !empty($id) ? (int)$id : false;
	}
	
}
