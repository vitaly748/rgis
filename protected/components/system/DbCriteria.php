<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CDbCriteria
 * @author v.zubov
 * @see CDbCriteria
 */
class DbCriteria extends CDbCriteria{
	
	/**
	 * Переопределение стандартного конструктора
	 * @param array $data Ассоциативный массив параметров критерия
	 */
	public function __construct($data = array()){
		foreach ($data as $name=>$value)
			if (property_exists($this, $name))
				$this->$name = $value;
			elseif ($name == 'between')
				$this->addBetweenCondition($value[0], $value[1], $value[2], isset($value[3]) ? $value[3] : 'AND');
			elseif ($name == 'column')
				$this->addColumnCondition($value[0], isset($value[1]) ? $value[1] : 'AND', isset($value[2]) ? $value[2] : 'AND');
			elseif ($name == 'cond')
				$this->addCondition($value[0], isset($value[1]) ? $value[1] : 'AND');
			elseif ($name == 'in')
				$this->addInCondition($value[0], $value[1], isset($value[2]) ? $value[2] : 'AND');
			elseif ($name == 'notin')
				$this->addNotInCondition($value[0], $value[1], isset($value[2]) ? $value[2] : 'AND');
			elseif ($name == 'search')
				$this->addSearchCondition($value[0], $value[1], isset($value[2]) ? $value[2] : true,
					isset($value[3]) ? $value[3] : 'AND', isset($value[4]) ? $value[4] : 'LIKE');
	}
	
}
