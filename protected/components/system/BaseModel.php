<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CActiveRecord. Класс является базовым для всех моделей
 * данных приложения
 * @author v.zubov
 * @see CActiveRecord
 */
class BaseModel extends CActiveRecord{
	
	/**
	 * @var array Список допустимых действий над данной моделью
	 */
	const ACTIONS = array('list', 'create', 'delete', 'view', 'update');
	
	/**
	 * @var array Ассоциативный массив псевдонимов действий над данной моделью данных для определения сценария в формате:
	 * 	array(
	 * 		{Символьный идентификатор действия}=>{Псевдонимы данного действия через запятую},
	 * 		...
	 * 	)
	 */
	const ACTION_SCENARIO_ALIASES = array(
		'list'=>'search, index',
		'create'=>'insert, registration',
		'delete'=>'remove',
		'update'=>'edit'
	);
	
	/**
	 * @var array Ассоциативный массив псевдонимов действий над моделями данных для определения команды в формате:
	 * 	array(
	 * 		{Символьный идентификатор действия}=>{Псевдонимы данного действия через запятую},
	 * 		...
	 * 	)
	 */
	const ACTION_COMMAND_ALIASES = array(
		'create'=>'ok, news',
		'update'=>'back, next, ok, cancel, login, esia, save'
	);
	
	/**
	 * @var string Действие над моделью данных по умолчанию
	 */
	const ACTION_DEF = 'update';
	
	/**
	 * @var array Положение символьных идентификаторов действий над атрибутами моделей в порядке увеличения приоритета
	 */
	const ACTION_WEIGHT = array(0=>0, 'view'=>1, 'update'=>2);
	
	/**
	 * @var array Список атрибутов, требующих подтверждения при создании или изменении
	 */
	const CONFIRM_MODIFY_ATTRIBUTES = array('password', 'email', 'phone');
	
	/**
	 * @var array Список атрибутов, подтверждение которых проходит при участии компонентов-подключений
	 */
	const CONFIRM_CONNECTION_ATTRIBUTES = array('email', 'phone');
	
	/**
	 * @var array Список атрибутов, подтверждение которых осуществляется в всплывающих окнах
	 */
	const CONFIRM_JUID_ATTRIBUTES = array('query', 'password', 'email', 'phone');
	
	/**
	 * @var integer Код режима использования шаблона по умолчанию для редактируемых полей. По умолчанию равно 2. Возможные
	 * варианты: 0 - не использовать, 1- использовать только для шаблонизированных полей, 2 - использовать для всех редактируемых
	 * полей
	 */
	const PATTERN_DEF_MODE = 1;
	
	/**
	 * @var string Регулярное выражение шаблона допустимого значения атрибутов модели данных по умолчанию
	 */
	const PATTERN_REGEXP_DEF = '0-9A-Za-zА-яЁё`~!@#№$%^&*()\\-_=+\\\\\\|\\[{\\]};:\'"«»,<.>\\/? ';
	
	/**
	 * @var integer Максимальная длина шаблона значения атрибута по умолчанию
	 */
	const PATTERN_MAX_LEN_DEF = 256;
	
	/**
	 * @var integer Код режима установки POST-данных "Простой" - без сброса результатов расчёта прав доступа
	 */
	const SET_POST_MODE_EASY = 0;
	
	/**
	 * @var integer Код режима установки POST-данных "Обычный" - сброс результатов расчёта прав доступа только в случае наличия
	 * POST-данных
	 */
	const SET_POST_MODE_NORMAL = 1;
	
	/**
	 * @var integer Код режима установки POST-данных "Агрессивный" - с принудительным сбросом результатов расчёта прав доступа
	 */
	const SET_POST_MODE_AGRESSIVE = 2;
	
	/**
	 * @var array Ассоциативный массив соответствий сообщений валидаторов. Задаётся в формате:
	 * 	array(
	 * 		{Символьный идентификатор валидитора}=>array(
	 * 			{Название атрибута класса валидатора, в который следует записать сообщение}=>
	 * 				{Символьный идентификатор сообщения из справочника ошибок атрибутов моделей},
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	const MESSAGE_RELATIONS = array(
		'captcha'=>array('message'=>'code_invalid', 'messageRefresh'=>'code_refresh'),
		'code'=>array('message'=>'code_invalid', 'messageDisabled'=>'code_disabled', 'messageRefresh'=>'code_refresh'),
		'length'=>array('tooShort'=>'length_tooshort', 'tooLong'=>'length_toolong'),
		'numerical'=>array('tooSmall'=>'numerical_toosmall', 'tooBig'=>'numerical_toobig')
	);
	
	/**
	 * @var string Постфикс названий атрибутов, предназначенных для ввода кодов подтверждения
	 */
	const POSTFIX_CONFIRM_ATTRIBUTES = 'code';
	
	/**
	 * @var string Постфикс названий операций доступа, позволяющих пропускать определённый тип подтверждений
	 */
	const POSTFIX_CONFIRM_SKIP = 'skip';
	
	/**
	 * @var string Ключевое слово для параметра "on" в массиве правил проверки моделей, указывающее, что для расчёта
	 * признака применения валидатора нужно использовать метод "limiterScanario"
	 */
	const LIMITER_SCENARIO = 'limiter';
	
	/**
	 * @var array Список названий атрибутов моделей, используемых для отображения даты
	 */
	const ATTRIBUTES_DATE = array('registration', 'locked', 'delete');
	
	/**
	 * @var array Набор допустимых значений фильтра количества элементов в списке
	 */
	const FILTER_SIZE_VALUES = array(10=>10, 40=>40, 70=>70, 100=>100);
	
	/**
	 * @var integer Значение фильтра количества элементов в списке по умолчанию
	 */
	const FILTER_SIZE_DEF = 40;
	
	/**
	 * @var boolean Признак кэширования данной модели
	 */
	const CACHING = true;
	
	/**
	 * @var string Код подтверждения c капчи
	 */
	public $captchacode;
	
	/**
	 * @var boolean Признак подтверждения вопроса
	 */
	public $querycode;
	
	/**
	 * @var string Пароль текущего пользователя
	 */
	public $passwordcode;
	
	/**
	 * @var string Код подтверждения из e-mail сообщения
	 */
	public $emailcode;
	
	/**
	 * @var string Код подтверждения из SMS сообщения
	 */
	public $phonecode;
	
	/**
	 * @var boolean Признак принадлежности данной модели данных к классу CActiveRecord
	 */
	protected $_ar;
	
	/**
	 * @var string Символьный идентификатор текущей модели данных
	 */
	protected $_modelName;
	
	/**
	 * @var array Список атрибутов модели. Для моделей типа CActiveRecord возвращаются только атрибуты, связанные с БД
	 */
	protected $_attributeNames;
	
	/**
	 * @var array Список меток атрибутов модели
	 */
	protected $_attributeLabels;
	
	/**
	 * @var array Список HTML-кода подсказок к атрибутам модели
	 */
	protected $_attributeTooltips;
	
	/**
	 * @var array Список параметров шаблона атрибутов
	 */
	protected $_attributePatterns;
	
	/**
	 * @var array Список статусов атрибутов модели. Определяется согласно справочнику атрибутов и параметрам класса модели
	 */
	protected $_attributeStates;
	
	/**
	 * @var array Правила проверки атрибутов
	 */
	protected $_rules = array();
	
	/**
	 * @var array Расчитанные правила проверки атрибутов
	 */
	protected $_rulesCompute;
	
	/**
	 * @var array Список безопасных атрибутов модели
	 */
	protected $_safeAttributeNames;
	
	/**
	 * @var array Список названий доступных к чтению атрибутов данной модели для текущего пользователя
	 */
	protected $_allowedAttributeNames;
	
	/**
	 * @var array Список небезопасных атрибутов данной модели
	 */
	protected $_unsafeAttributeNames;
	
	/**
	 * @var array Список всех атрибутов модели
	 */
	protected $_classAttributeNames;
	
	/**
	 * @var array Список атрибутов-списков модели
	 */
	protected $_multipleAttributeNames;
	
	/**
	 * @var array Список атрибутов, имеющих фиксированный набор значений, но при этом допускающих отрицательные значения
	 */
	protected $_unsetAttributeNames;
	
	/**
	 * @var boolean|UserModel Владелец текущей модели
	 */
	protected $_owner;
	
	/**
	 * @var boolean|UserModel Адресат текущей модели
	 */
	protected $_addressee;
	
	/**
	 * @var string Символьный идентификатор текущего действия над моделью
	 */
	protected $_action;
	
	/**
	 * @var boolean|string Символьный идентификатор вызываемой команды над моделью или false
	 */
	protected $_command;
	
	/**
	 * @var array Список доступных действий над данной моделью для текущего пользователя в формате
	 * 	array(
	 * 		{Название действия ("create", "delete", "view", "update" и т.д.)}=>{Признак доступности данного действия},
	 * 		...
	 * )
	 */
	protected $_actions;
	
	/**
	 * @var array Ассоциативный массив с информацией о доступности атрибутов данной модели текущему пользователю
	 * в формате:
	 * 	array(
	 * 		{Название атрибута}=>{Доступное действие ("view", "update" или false)},
	 * 		...
	 * 	)
	 * Доступ к действию "update" предполагает так же доступ к действию "view"
	 */
	protected $_accessAttributes;
	
	/**
	 * @var array Ассоциативный массив с информацией о доступности значений атрибутов, имеющих фиксированный набор возможных
	 * значений, данной модели текущему пользователю в формате:
	 * 	array(
	 * 		{Название атрибута}=>array(
	 * 			{Символьный идентификатор значения}=>array(
	 * 				'id'=>5,// Идентификатор значения в БД. Обязательный параметр
	 * 				'code'=>50,// Цифровой идентификатор значения. Обязательный параметр
	 * 				'name'=>'init',// Символьный идентификатор значения. Обязательный параметр
	 * 				'description'=>'Инициализация',// Описание значения. Обязательный параметр
	 * 				// Признак использования значения в режиме "radiobutton". Необязательный параметр. По умолчанию равно false
	 * 				'radio'=>null,
	 * 				'access'=>'view'// Доступное действие ("view", "update" или false). Обязательный параметр
	 * 			),
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 * Доступ к действию "update" предполагает так же доступ к действию "view"
	 */
	protected $_accessValues;
	
	/**
	 * @var array Список символьных идентификаторов необходимых типов проверок данной модели данных
	 */
	protected $_confirmTypes;
	
	/**
	 * @var array|boolean Ассоциативный массив параметров кодов подтверждения введённых данных текущей модели или false
	 * в случае отсутствия необходимости подтверждения кодами 
	 */
	protected $_confirmParams;
	
	/**
	 * @var boolean Результат проверки права доступа на совершение текущей операции над данной моделью для
	 * текущего пользователя
	 */
	protected $_access;
	
	/**
	 * @var array Ассоциативный массив предыдущих значений атрибутов модели
	 */
	protected $_prevValues;
	
	/**
	 * @var array|boolean Информация по данной модели из массива $_POST или false
	 */
	protected $_post;
	
	/**
	 * Переопределение стандартного конструктора
	 * @param string $scenario Название сценария. По умолчанию равно false. Если равно false, то устанавливается значение по
	 * умолчанию: для CActiveRecord - "insert", для других - ""
	 * @see CActiveRecord::__construct()
	 */
	public function __construct($scenario = false){
		if ($scenario === false)
			$scenario = $this->getAr() ? 'insert' : '';
		
		if ($this->getAr())
			parent::__construct($scenario);
		else{
			$this->setScenario($scenario);
			$this->init();
			$this->attachBehaviors($this->behaviors());
			$this->afterConstruct();
		}
	}
	
	/**
	 * Переопределение стандартного метода getMetaData, возвращающего мета-данные для текущей модели данных
	 * @return CActiveRecordMetaData Мета-данные
	 * @see CActiveRecord::getMetaData()
	 */
	public function getMetaData(){
		return $this->getAr() ? parent::getMetaData() : false;
	}
	
	/**
	 * Переопределение стандартного метода tabelName, возвращающего название таблицы, соответствующей данной модели
	 * @return string Название таблицы
	 * @see CActiveRecord::tableName()
	 */
	public function tableName(){
		return $this->getAr() ? '{{'.$this->getModelName().'}}' : false;
	}
	
	/**
	 * Переопределение стандартного метода attributeNames, возвращающего список атрибутов модели. Для моделей типа CActiveRecord
	 * возвращаются только атрибуты, связанные с БД
	 * @return array Список атрибутов
	 * @see CActiveRecord::attributeNames()
	 */
	public function attributeNames(){
		if ($this->_attributeNames === null)
			$this->_attributeNames = $this->getAr() ? parent::attributeNames() : $this->classAttributeNames;
		
		return $this->_attributeNames;
	}
	
	/**
	 * Переопределение стандартного метода attributeLabels, возвращающего метки атрибутов
	 * @return array|boolean Ассоциативный массив меток в формате {Название атрибута}=>{Метка атрибута} или false
	 * @see CModel::attributeLabels()
	 */
	public function attributeLabels(){
		if ($this->_attributeLabels === null)
			$this->_attributeLabels = Yii::app()->attribute->get($this->getModelName(), false, 'label');
		
		return $this->_attributeLabels;
	}
	
	/**
	 * Определение порядковых номеров меток атрибутов в зависимости от текущего действия над моделью
	 * @return array Ассоциативный массив номеров атрибутов в формате:
	 * 	array(
	 * 		{Название атрибута}=>array(
	 * 			{Название действия или действий через запятую}=>{Номер метки},
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	public function attributeLabelsFilter(){
		return array();
	}
	
	/**
	 * Получение метки для конкретного атрибута модели
	 * @param string $attribute Название атрибута. Для запроса метки атрибута для связанной модели можно задавать составное
	 * значение. Например, "addressee.conjugaion.id_owner"
	 * @param integer $num Номер метки атрибута. По умолчанию равно false. Если равно false, номер будет определён в зависимости
	 * от текущего действия над моделью. Если равно null, будет получена строка меток атрибута без обработки
	 * @return string Метка атрибута
	 * @see CModel::generateAttributeLabel()
	 * @see BaseModel::attributeLabels()
	 */
	public function getAttributeLabel($attribute, $num = false){
		$labels = $this->attributeLabels();
		
		if (isset($labels[$attribute]))
			$label = $labels[$attribute];
		else{
			if (!$this->getAr() || mb_strpos($attribute, '.') === false)
				return $this->generateAttributeLabel($attribute);
			else{
				$segs = explode('.', $attribute);
				$attribute = array_pop($segs);
				$model = $this;
				
				foreach ($segs as $seg){
					$relations = $model->getMetaData()->relations;
					
					if (isset($relations[$seg]))
						$model = CActiveRecord::model($relations[$seg]->className);
					else
						break;
				}
				
				$label = $model->getAttributeLabel($attribute);
			}
		}
		
		if ($this->_action === null || $num === null || mb_strpos($label, Table::SEPARATOR_WORDS) === false)
			return $label;
		
		$labels = explode(Table::SEPARATOR_WORDS, $label);
		
		if ($num === false && ($labels_filter = $this->attributeLabelsFilter()) && isset($labels_filter[$attribute])){
			$label_filter = Arrays::extensionParams($labels_filter[$attribute]);
			$num = Arrays::pop($label_filter, $this->getAction());
		}
		
		return isset($labels[$num]) ? $labels[$num] : reset($labels);
	}
	
	/**
	 * Получение метки для конкретного атрибута модели. Статичный вариант
	 * @param string $model Символьный идентификатор модели
	 * @param string $attribute Название атрибута
	 * @param integer $num Номер метки атрибута. По умолчанию равно false
	 * @return string Метка атрибута
	 */
	public static function getAttributeLabelStatic($model, $attribute, $num = false){
		if (!$label = Yii::app()->attribute->get($model, $attribute, 'label'))
			return false;
		
		if (mb_strpos($label, Table::SEPARATOR_WORDS) === false)
			return $label;
		
		$labels = explode(Table::SEPARATOR_WORDS, $label);
		
		return isset($labels[$num]) ? $labels[$num] : reset($labels);
	}
	
	/**
	 * Переопределение стандартного метода getAttributes, возвращающего значения атрибутов модели
	 * @param mixed $names Названия атрибутов, значения которых необходимо вернуть. По умолчанию равно null
	 * @return array Ассоциативный массив значений атрибутов
	 * @see CActiveRecord::getAttributes()
	 */
	public function getAttributes($names = null){
		if ($this->getAr())
			return parent::getAttributes($names);
		else{
			$values = array();
			
			foreach ($this->attributeNames() as $name)
				$values[$name] = $this->$name;
			
			if (is_array($names)){
				$values2 = array();
				
				foreach ($names as $name)
					$values2[$name] = isset($values[$name]) ? $values[$name] : null;
				
				return $values2;
			}else
				return $values;
		}
	}
	
	/**
	 * Переопределение стандартного метода save, сохраняющего данные моделей в БД
	 * @param boolean $runValidation Признак необходимости проведения проверки перед началом процесса сохранения.
	 * По умолчанию равен true
	 * @param array $attributes Список атрибутов, которые необходимо сохранить. По умолчанию равен null
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 * @see CActiveRecord::save()
	 */
	public function save($runValidation = true, $attributes = null, $fix = true){
		$params_fix = array('name'=>$this->getModelName(), 'run_validation'=>$runValidation, 'attributes'=>$attributes);
		
		try{
			parent::save($runValidation, $attributes);
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_row_save', $params_fix, $ok ? 'ok' : 'db_sql');
		
		return $ok;
	}
	
	/**
	 * Переопределение стандартного метода insert, вставляющего данные модели в БД
	 * @param array $attributes Ассоциативный массив названий изменяемых атрибутов модели
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 * @throws CHttpException
	 * @see CActiveRecord::insert()
	 */
	public function insert($attributes = null, $fix = true){
		$params_fix = array('name'=>$this->getModelName(), 'attributes'=>$attributes);
		
		try{
			$result = parent::insert($attributes);
			
			if (($connection = $this->getDbConnection()) && $connection->queryCacheID &&
				($cache = Yii::app()->getComponent($connection->queryCacheID)) !== null && $cache instanceof FileCache &&
				!$cache->cleaning('sql', 'insert', array($params_fix['name'])))
					throw new CHttpException(500, 'cache_cleaning');
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_row_insert', $params_fix, $ok ? 'ok' : (!isset($result) ? 'db_sql' : 'cache_cleaning'));
		
		if (!$ok)
			throw new CHttpException(500, $params_fix['msg']);
		
		return $ok;
	}
	
	/**
	 * Переопределение стандартного метода update, изменяющего данные модели в БД
	 * @param array $attributes Ассоциативный массив названий изменяемых атрибутов модели
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 * @throws CHttpException
	 * @see CActiveRecord::update()
	 */
	public function update($attributes = null, $fix = true){
		$params_fix = array('name'=>$this->getModelName(), 'attributes'=>$attributes, 'id'=>isset($this->id) ? $this->id : false);
		
		try{
			$result = parent::update($attributes);
			
			if (($connection = $this->getDbConnection()) && $connection->queryCacheID && $params_fix['id'] &&
				($cache = Yii::app()->getComponent($connection->queryCacheID)) !== null && $cache instanceof FileCache &&
				!$cache->cleaning('sql', 'update', array($params_fix['name'], $params_fix['id'])))
					throw new CHttpException(500, 'cache_cleaning');
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_row_update', $params_fix, $ok ? 'ok' : (!isset($result) ? 'db_sql' : 'cache_cleaning'));
		
		if (!$ok)
			throw new CHttpException(500, $params_fix['msg']);
		
		return $ok;
	}
	
	/**
	 * Переопределение стандартного метода delete, удаляющего модели в БД
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 * @see CActiveRecord::delete()
	 */
	public function delete($fix = true){
		$params_fix = array('name'=>$this->getModelName(), 'id'=>isset($this->id) ? $this->id : false);
		
		try{
			$result = parent::delete();
			
			if (($connection = $this->getDbConnection()) && $connection->queryCacheID && $params_fix['id'] &&
				($cache = Yii::app()->getComponent($connection->queryCacheID)) !== null && $cache instanceof FileCache &&
				!$cache->cleaning('sql', 'update', array($params_fix['name'], $params_fix['id'])))
					throw new CHttpException(500, 'cache_cleaning');
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_row_delete', $params_fix, $ok ? 'ok' : (!isset($result) ? 'db_sql' : 'cache_cleaning'));
		
		if (!$ok)
			throw new CHttpException(500, $params_fix['msg']);
		
		return $ok;
	}
	
	/**
	 * Переопределение стандартного метода findAll, выполняющего поиск записей в таблице модели
	 * @param mixed $condition Условие поиска или критерий. По умолчанию равно ''
	 * @param array $params Массив параметров поиска. По умолчанию равно array()
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return array|boolean Набор строчек результата или false в случае ошибки
	 * @see CActiveRecord::findAll()
	 */
	public function findAll($condition = '', $params = array(), $fix = true){
		$params_fix = array('name'=>$this->getModelName(), 'condition'=>$condition, 'params'=>$params);
		
		try{
			if (static::CACHING && $this->checkCondition($condition) && ($connection = $this->getDbConnection()) &&
				($duration = $connection->queryCachingDuration) > 0 && $connection->queryCacheID &&
				($cache = Yii::app()->getComponent($connection->queryCacheID)) !== null){
					if ($this->getModelName() == 'setting' && ($duration3 = Yii::app()->param->get('cache', 'duration3')))
						$duration = $duration3;
					
					$cacheKey = Cache::PREFIX_MODEL.$this->getCacheKeyNames();
					$result = $cache->getExt('sql', $cacheKey);
				}
			
			if (!isset($cacheKey) || $result === false){
				$result = parent::findAll($condition, $params);
				
				if (isset($cacheKey))
					$cache->setExt('sql', $cacheKey, $result, $duration, $connection->queryCachingDependency);
			}
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_find_all', $params_fix, $ok ? 'ok' : 'db_sql');
		
		return $ok ? $result : false;
	}
	
	/**
	 * Выборочный поиск в БД с помощью стандартной функции findAll
	 * @param boolean|string $id Название атрибута модели, выступающего в качестве ключа строки финального
	 * ассоциативного массива или false. По умолчанию равно false
	 * @param array|boolean|string $value Название атрибута модели, выступающего в качестве значения элемента финального
	 * ассоциативного массива или false. По умолчанию равно false. Допускается задавать срузу несколько атрибутов в виде массива
	 * или строки с разделителем ","
	 * @param boolean $array Признак необходимости перевода объектов моделей результата в массив. По умолчанию
	 * равно false
	 * @param array|string $condition Строка с условиями поиска или ассоциативный массив с параметрами условия поиска.
	 * По умолчанию равно ''
	 * @param array $params Массив параметров поиска. По умолчанию равно array()
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return array|boolean Набор строчек результата или false в случае ошибки
	 */
	public function findAllSelective($id = false, $value = false, $array = false, $condition = '', $params = array(), $fix = true){
		$params_fix = array('name'=>$this->getModelName(), 'id'=>$id, 'value'=>$value, 'condition'=>$condition, 'params'=>$params);
		$result = array();
		
		try{
			if ($value){
				if (!is_array($value))
					$value = Strings::devisionWords($value);
				
				if (!$condition instanceof CDbCriteria)
					$condition = $this->getCommandBuilder()->createCriteria($condition, $params);
				
				$condition->select = Arrays::forming(array_merge(array($id), $value));
				$params = array();
				
				if (count($value) == 1)
					$value = reset($value);
			}
			
			if (static::CACHING && $this->checkCondition($condition) && ($connection = $this->getDbConnection()) &&
				($duration = $connection->queryCachingDuration) > 0 && $connection->queryCacheID &&
				($cache = Yii::app()->getComponent($connection->queryCacheID)) !== null){
					if ($this->getModelName() == 'setting' && ($duration3 = Yii::app()->param->get('cache', 'duration3')))
						$duration = $duration3;
					
					$cacheKey = Cache::PREFIX_MODEL.$this->getCacheKeyNames();
					$rows = $cache->getExt('sql', $cacheKey);
				}
			
			if (!isset($cacheKey) || $rows === false){
				$rows = parent::findAll($condition, $params);
				
				if (isset($cacheKey))
					$cache->setExt('sql', $cacheKey, $rows, $duration, $connection->queryCachingDependency);
			}
			
			if (!$id && !$value && !$array)
				$result = $rows;
			elseif ($id)
				if ($value)
					foreach ($rows as $row){
						$attributes = $row->attributes;
						$result[$row->$id] = !is_array($value) ? $row->$value : Arrays::pop($attributes, $value, false, true);
					}
				elseif ($array)
					foreach ($rows as $row)
						$result[$row->$id] = $row->attributes;
				else
					foreach ($rows as $row)
						$result[$row->$id] = $row;
			elseif ($value)
				foreach ($rows as $row){
					$attributes = $row->attributes;
					$result[] = !is_array($value) ? $row->$value : Arrays::pop($attributes, $value, false, true);
				}
			else
				foreach ($rows as $key=>$row)
					$result[$key] = $row->attributes;
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_find_all', $params_fix, $ok ? 'ok' : 'db_sql');
		
		return $ok ? $result : false;
	}
	
	/**
	 * Переопределение стандартного метода findByPk, осуществляющего поиск записи по первичному ключу
	 * @param mixed $pk Первичный ключ записи
	 * @param array|CDbCriteria|string $condition Условия поиска. По умолчанию равно ''
	 * @param array $params Массив параметров поиска. По умолчанию равно array()
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean|CActiveRecord|null Найденная запись или false в случае ошибки. Если запись не найдена - null
	 * @see CActiveRecord::findByPk()
	 */
	public function findByPk($pk, $condition = '', $params = array(), $fix = true){
		$params_fix = array('name'=>$this->getModelName(), 'pk'=>$pk, 'condition'=>$condition, 'params'=>$params);
		
		try{
			if (static::CACHING && !is_array($pk) && ($connection = $this->getDbConnection()) &&
				($duration = Yii::app()->param->get('cache', 'duration3')) > 0 && $connection->queryCacheID &&
				($cache = Yii::app()->getComponent($connection->queryCacheID)) !== null && !$cache instanceof CDummyCache){
					$cacheKey = Cache::PREFIX_RECORD.$this->getCacheKeyNames().sprintf(Cache::PATTERN_PARAM, $pk);
					$result = $cache->getExt('sql', $cacheKey);
				}
			
			if (!isset($cacheKey) || $result === false){
				$result = parent::findByPk($pk, $condition, $params);
				
				if (isset($cacheKey))
					$cache->setExt('sql', $cacheKey, $result, $duration, $connection->queryCachingDependency);
			}
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_find_bypk', $params_fix, $ok ? 'ok' : 'db_sql');
		
		return $ok ? $result : false;
	}
	
	/**
	 * Переопределение стандартного метода updateAll, изменяющего данные нескольких моделей в БД
	 * @param array $attributes Ассоциативный массив изменяемых параметров модели
	 * @param string $condition Условие поиска. По умолчанию равно ''
	 * @param array $params Массив параметров поиска. По умолчанию равно array()
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 * @see CActiveRecord::updateAll()
	 */
	public function updateAll($attributes, $condition = '', $params = array(), $fix = true){
		$params_fix = array('name'=>$this->getModelName(), 'attributes'=>$attributes, 'condition'=>$condition, 'params'=>$params);
		
		try{
			$result = parent::updateAll($attributes, $condition, $params);
			
			if (($connection = $this->getDbConnection()) && $connection->queryCacheID &&
				($cache = Yii::app()->getComponent($connection->queryCacheID)) !== null && $cache instanceof FileCache &&
				!$cache->cleaning('sql', 'insert', array($params_fix['name'])))
					throw new CHttpException(500, 'cache_cleaning');
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_row_update_all', $params_fix, $ok ? 'ok' : (!isset($result) ? 'db_sql' : 'cache_cleaning'));
		
		return $ok;
	}
	
	/**
	 * Переопределение стандартного метода deleteAll, удаляющего несколько моделей в БД
	 * @param string $condition Условие поиска. По умолчанию равно ''
	 * @param array $params Массив параметров поиска. По умолчанию равно array()
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 * @see CActiveRecord::deleteAll()
	 */
	public function deleteAll($condition = '', $params = array(), $fix = true){
		$params_fix = array('name'=>$this->getModelName(), 'condition'=>$condition, 'params'=>$params);
		
		try{
			$result = parent::deleteAll($condition, $params);
			
			if (($connection = $this->getDbConnection()) && $connection->queryCacheID &&
				($cache = Yii::app()->getComponent($connection->queryCacheID)) !== null && $cache instanceof FileCache &&
				!$cache->cleaning('sql', 'insert', array($params_fix['name'])))
					throw new CHttpException(500, 'cache_cleaning');
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('db_row_delete_all', $params_fix, $ok ? 'ok' : (!isset($result) ? 'db_sql' : 'cache_cleaning'));
		
		return $ok;
	}
	
	/**
	 * Переопределение стандартного метода rules, возвращающего список правил проверки атрибутов данной модели
	 * @return array Обработанные правила проверки атрибутов модели с учётом статуса атрибутов, прав доступа текущего
	 * пользователя к ним и т.д.
	 * @see CModel::rules()
	 */
	public function rules(){
		if ($this->_rulesCompute === null){
			$this->_rulesCompute = array();
			$class_rules = $this->rules;
			$access_attributes = $this->accessAttributes;
			$state_attributes = $this->attributeStates;
			
			if ($this->getModelName() !== 'confirm' && ($confirm_types = $this->confirmTypes[$this->action]))
				foreach ($confirm_types as $confirm_type)
					$class_rules[] = array($confirm_type.static::POSTFIX_CONFIRM_ATTRIBUTES,
						($is_captcha = $confirm_type == 'captcha') ? 'captcha' : 'code', 'enableClientValidation'=>$is_captcha);
			
			if ($class_rules && $access_attributes && $state_attributes){
				$pattern_params_attributes = $this->attributePatterns();
				$pattern_attributes = Yii::app()->attributePattern->get($this);
				$limiter_scenario = Arrays::extensionParams($this->limiterScenario());
				
				foreach ($state_attributes as $attribute=>$state)
					$state_attributes[$attribute] = $access_attributes[$attribute] !== 'update' ? 'unsafe' :
						($state === 'update' ? 'safe' : 'required');
				
				foreach ($class_rules as $rule)
					if (($attributes = Arrays::pop($rule, 0, true)) && ($validator = Arrays::pop($rule, 1, true))){
						$on = Arrays::pop($rule, 'on', true);
						$except = Arrays::pop($rule, 'except', true);
						$limiter = $on === static::LIMITER_SCENARIO;
						
						if ($on && is_string($on) && !$limiter)
							$on = Strings::devisionWords($on);
						
						if ($except && is_string($except))
							$except = Strings::devisionWords($except);
						
						foreach (Strings::devisionWords($attributes) as $attribute)
							if (isset($state_attributes[$attribute])){
								$validator_on = $on;
								
								if ($limiter)
									if (Arrays::pop($limiter_scenario, "$attribute.$validator"))
										$validator_on = null;
									else
										continue;
								
								if ($validator === 'unsafe' && !$validator_on && !$except)
									unset($params_rules[$attribute], $state_attributes[$attribute]);
								else{
									$params_rule = array('on'=>$validator_on, 'except'=>$except, 'params'=>$rule);
									$validator_attribute = $validator === 'required' && $state_attributes[$attribute] === 'safe' ?
										'safe' : $validator;
									
									if (!$insert = !isset($params_rules[$attribute][$validator_attribute]))
										if (!in_array($params_rule, $params_rules[$attribute][$validator_attribute], true)){
											$insert = true;
											
											foreach ($params_rules[$attribute][$validator_attribute] as &$validator_other)
												if ($rule === $validator_other['params']){
													$validator_other['on'] = $validator_on && $validator_other['on'] ?
														array_unique(array_merge($validator_on, $validator_other['on'])) : false;
													$validator_other['except'] = $except && $validator_other['except'] ?
														array_unique(array_intersect($except, $validator_other['except'])) : false;
													$insert = false;
												}
											unset($validator_other);
										}
									
									if ($insert)
										$params_rules[$attribute][$validator_attribute][] = $params_rule;
								}
							}
					}
				
				foreach ($params_rules as $attribute=>&$validators){
					if (($state = $state_attributes[$attribute]) === 'unsafe'){
						unset($params_rules[$attribute]);
						continue;
					}
					
					$text_field = !isset($validators['numerical']) && !isset($validators['boolean']) && !isset($validators['date']) &&
						((!$type = Arrays::pop($validators, 'type.0.params.type')) || $type === 'string');
					$length_min = $length_max = false;
					
					if ($state === 'required' && !isset($validators['required']) &&
						($range = $this->getValidatorsRange($validators, array('required', 'unsafe')))){
							$validators = array_merge(array('required'=>array(array(
								'on'=>$range['on'],
								'except'=>$range['except'],
								'params'=>array()
							))), $validators);
							
							unset($validators['safe']);
						}
					
					if ($pattern_attribute = Arrays::pop($pattern_attributes, $attribute))
						list($length_min, $length_max) = array($pattern_attribute['length_min'], $pattern_attribute['length_max']);
					
					if ($validator_len = Arrays::pop($validators, 'length.0')){
						if ($min = Arrays::pop($validator_len, 'min'))
							$length_min = max($length_min, $min);
						
						if ($max = Arrays::pop($validator_len, 'max'))
							$length_max = min($length_max, $max);
						
						if ($min != $length_min)
							$validators['length'][0]['params']['min'] = (int)$length_min;
						
						if ($max != $length_max)
							$validators['length'][0]['params']['max'] = (int)$length_max;
					}elseif ($range = $this->getValidatorsRange($validators, array('unsafe'))){
						if (!$length_max && $text_field)
							$length_max = static::PATTERN_MAX_LEN_DEF;
						
						if ($length_min || $length_max){
							$validators['length'][0] = array(
								'on'=>$range['on'],
								'except'=>$range['except'],
								'params'=>array()
							);
							
							if ($length_min)
								$validators['length'][0]['params']['min'] = $length_min;
							
							if ($length_max)
								$validators['length'][0]['params']['max'] = $length_max;
						}
					}
					
					if ($pattern_params = Arrays::pop($pattern_params_attributes, $attribute)){
						if (!isset($validators['match']) && $text_field &&
							($range = $this->getValidatorsRange($validators, array('unsafe'))))
								$validators['match'][0] = array(
									'on'=>$range['on'],
									'except'=>$range['except'],
									'params'=>array(
										'pattern'=>"/{$pattern_params['regexp']}/u",
										'enableClientValidation'=>false
									)
								);
						
						if ($decoration = Arrays::pop($pattern_params, 'delimiter').Arrays::pop($pattern_params, 'decoration')){
							if (($validator_numerical = Arrays::pop($validators, 'numerical.0')) &&
								!isset($validator_numerical['params']['decoration']))
									$validators['numerical'][0]['params']['decoration'] = $decoration;
							
							if (($validator_phone = Arrays::pop($validators, 'phone.0')) &&
								!isset($validator_phone['params']['decoration']))
									$validators['phone'][0]['params']['decoration'] = $decoration;
						}
					}
					
					if (($safe = Arrays::pop($validators, 'safe.0')) &&
						($range = $this->getValidatorsRange($validators, array('safe', 'unsafe'))) &&
						((!$safe['except'] && !$range['except']) || ($safe['except'] === $range['except'])))
							if ($safe['on'] && $range['on']){
								if ($diff = array_diff($safe['on'], $range['on']))
									$validators['safe'][0]['on'] = $diff;
								else
									unset($validators['safe']);
							}elseif (!$range['on'])
								unset($validators['safe']);
				}
				unset($validators);
				
				$weight_attributes = array_flip(array_reverse(array_keys($state_attributes)));
				$qu_weight = count($weight_attributes);
				$data_rules = $data_attributes = array();
				$validators_standart = array_keys(CValidator::$builtInValidators);
				
				foreach ($params_rules as $attribute=>$validators)
					foreach ($validators as $validator_name=>$validators_group)
						foreach ($validators_group as $validator){
							if ($messages = $this->getValidatorMessage($attribute, $validator_name))
								$validator['params'] = $validator['params'] ? array_merge($validator['params'], $messages) : $messages;
							
							$weight_validator = !in_array($validator_name, $validators_standart) ? 0 : 1000;
							$weight = $weight_attributes[$attribute];
							$data_rule = array($validator_name);
							
							if ($validator['params'])
								$data_rule = array_merge($data_rule, $validator['params']);
							
							if ($validator['on'])
								$data_rule['on'] = $validator['on'];
							
							if ($validator['except'])
								$data_rule['except'] = $validator['except'];
							
							if (($key = array_search($data_rule, $data_rules, true)) === false){
								$data_rules[] = $data_rule;
								$data_attributes[] = array(array($weight), $weight_validator + $weight);
							}else{
								$data_attributes[$key][0][] = $weight;
								$data_attributes[$key][1] += $qu_weight + $weight;
							}
						}
				
				Arrays::sort($data_attributes, 1, true);
				$weight_attributes = array_flip($weight_attributes);
				
				foreach ($data_attributes as $key=>$params){
					$names = array();
					rsort($params[0]);
					
					foreach ($params[0] as $weight)
						$names[] = $weight_attributes[$weight];
					
					$this->_rulesCompute[] = array_merge(array(implode(', ', $names)), $data_rules[$key]);
				}
			}
		}
		
		return $this->_rulesCompute;
	}
	
	/**
	 * Переопределение стандартного метода beforeValidate, выполняющего предпроверку данных модели
	 * @see CModel::beforeValidate()
	 */
	protected function beforeValidate(){
		if (!parent::beforeValidate())
			return false;
		
		$values_id = Yii::app()->value->dataId;
		
		foreach ($this->allowedAttributeNames as $attribute)
			if ($values = Arrays::pop($values_id, $this->getAttributeValueId($attribute))){
				$value = $this->$attribute;
				$attribute_text = mb_substr($attribute, 0, -2);
				
				if ($value){
					if (!$value_text = Arrays::pop($values, $value.',description', false, false, false, ','))
						$this->addError($attribute_text, Yii::app()->errorAttribute->get($this, $attribute_text, 'invalid'));
					elseif ($this->getModelName() != 'appeal' || $value != Appeal::CODE_USER_THEME)
						$this->$attribute_text = $value_text;
				}else
					$this->$attribute_text = '';
			}
		
		return !$this->hasErrors();
	}
	
	/**
	 * Переопределение стандартного метода afterValidate, выполняющего постпроверку данных модели
	 * @see CModel::afterValidate()
	 */
	protected function afterValidate(){
		parent::afterValidate();
		
		if (!$this->hasErrors()){
			$safe_attributes = $this->safeAttributeNames;
			
			foreach ($this->accessValues as $attribute=>$values)
				if (in_array($attribute, $safe_attributes)){
					$value = $this->$attribute;
					$value_prev = $this->prevValues[$attribute];
					$invalid = false;
					
					if ($value !== $value_prev)
						if ($value && $value_prev && is_array($value) != is_array($value_prev))
							$invalid = true;
						else{
							$value_prev_arr = (array)$value_prev;
							$value = (array)$value;
							
							if (!$values_merge = array_merge($value_prev_arr, $value))
								$invalid = true;
							else{
								$values_changed = array_diff($values_merge, array_intersect($value_prev_arr, $value));
								
								if ($values_changed && $this->_unsetAttributeNames && in_array($attribute, $this->_unsetAttributeNames))
									foreach ($values_changed as $key=>$value_change)
										if ($value_change < 0)
											unset($values_changed[$key]);
								
								if ($values_changed){
									$values_update = array();
									
									foreach ($values as $value_params)
										if ($value_params['access'] == 'update')
											$values_update[] = $value_params['code'];
									
									if (!$values_update || array_diff($values_changed, $values_update))
										$invalid = true;
								}
							}
							
							if (!$invalid && ($values_radio = Arrays::select($values, 'code', false, false, true, 'radio')) &&
								count(array_intersect($values_radio, $value)) > 1)
									$invalid = true;
						}
						
						if ($invalid){
							$this->addError($attribute, Yii::app()->errorAttribute->get($this, $attribute, 'invalid'));
							$this->$attribute = $value_prev;
						}
				}
		}
	}
	
	/**
	 * Переопределение стандартного метода getValidators, возвращающего список объектов валидаторов для заданного атрибута модели
	 * данных
	 * @param string $attribute Название атрибута, валидаторы для которого необходимо получить. По умолчанию равно null. Если равно
	 * null, то вернутся валидаторы для всех атрибутов
	 * @see CModel::getValidators()
	 */
	public function getValidators($attribute = null){
		$validators = array();
		$scenario = $this->getScenario();
		
		foreach ($this->createValidators() as $validator)
			if ($validator->applyTo($scenario) && ($attribute === null || in_array($attribute, $validator->attributes, true)))
				$validators[] = $validator;
			
			return $validators;
	}
	
	/**
	 * Переопределение стандартного метода setAttributes, выполняющего групповое присвоение значений атрибутам модели
	 * @param array $values Ассоциативный массив значений атрибутов
	 * @param boolean $safeOnly Признак присвоения значений только безопасным атрибутам. По умолчанию равно true
	 * @see CModel::setAttributes()
	 */
	public function setAttributes($values, $safeOnly = true){
		$this->_prevValues = $this->getAttributes($this->classAttributeNames);
		
		if (!is_array($values))
			return;
		
		$attributes = array_flip($safeOnly ? $this->getSafeAttributeNames() : $this->classAttributeNames);
		
		foreach ($values as $name=>$value)
			if (isset($attributes[$name]))
				$this->$name = $value;
			elseif ($safeOnly)
				$this->onUnsafeAttribute($name, $value);
	}
	
	/**
	 * Переопределение стандартного метода-сеттера для установки _scenario
	 * @param string $value Символьный идентификатор сценария
	 * @see CModel::setScenario()
	 */
	public function setScenario($value){
		parent::setScenario($value);
		
		$this->setAction(null, true);
	}
	
	/**
	 * Переопределение стандартного метода getSafeAttributeNames, возвращающего список безопасных атрибутов модели
	 * @return array Список атрибутов
	 * @see CModel::getSafeAttributeNames()
	 */
	public function getSafeAttributeNames(){
		if ($this->_safeAttributeNames === null)
			$this->_safeAttributeNames = parent::getSafeAttributeNames();
		
		return $this->_safeAttributeNames;
	}
	
	/**
	 * Метод-геттер для определения _ar
	 * @return boolean Признак принадлежности данной модели данных к классу CActiveRecord
	 */
	public function getAr(){
		if ($this->_ar === null)
			$this->_ar = ($tables = Yii::app()->table->get()) ? in_array($this->getModelName(), $tables) : false;
		
		return $this->_ar;
	}
	
	/**
	 * Метод-геттер для определения _modelName
	 * @return string Символьный идентификатор текущей модели данных
	 */
	public function getModelName(){
		if ($this->_modelName === null)
			$this->_modelName = Strings::nameModify(get_class($this));
		
		return $this->_modelName;
	}
	
	/**
	 * Получение списка подсказок
	 * @return array|boolean Ассоциативный массив подсказок в формате {Название атрибута}=>{Подсказка атрибута} или false
	 */
	public function attributeTooltips(){
		if ($this->_attributeTooltips === null)
			$this->_attributeTooltips = Yii::app()->attribute->get($this->getModelName(), false, 'tooltip');
		
		return $this->_attributeTooltips;
	}
	
	/**
	 * Получение подсказки для конкретного атрибута модели
	 * @param string Название атрибута
	 * @return boolean|string Подсказка или false
	 */
	public function getAttributeTooltip($attribute){
		$tooltips = $this->attributeTooltips();
		
		return isset($tooltips[$attribute]) ? $tooltips[$attribute] : false;
	}
	
	/**
	 * Получение заполнителя поля ввода атрибута модели
	 * @param string Название атрибута
	 * @return boolean|string Заполнитель или false
	 */
	public function getAttributePlaceholder($attribute){
		if (!$tooltip = $this->getAttributeTooltip($attribute))
			return false;
		else
			return ($pos = mb_strpos($tooltip, Attribute::SEPARATOR_TOOLTIP)) !== false ?
				mb_substr($tooltip, 0, $pos) : $tooltip;
	}
	
	/**
	 * Получение текста подсказки атрибута модели для виджета Tooltip
	 * @param string Название атрибута
	 * @return boolean|string Текст подсказки или false
	 */
	public function getAttributeTooltipText($attribute){
		return ($tooltip = $this->getAttributeTooltip($attribute)) &&
			($pos = mb_strpos($tooltip, Attribute::SEPARATOR_TOOLTIP)) !== false ?
			mb_substr($tooltip, $pos + mb_strlen(Attribute::SEPARATOR_TOOLTIP)) : false;
	}
	
	/**
	 * Получение списка параметров всех шаблонных атрибутов
	 * @return array|boolean Список параметров шаблонов значений атрибутов или false. Формат массива:
	 * 	array(
				'groups_separation'=>'342',// Количественное группирование символов значения атрибута. Необязательный параметр
				'groups_min'=>3,// Минимальное количество групп в шаблоне. Необязательный параметр
				// Признак обратного направления расчёта количественного группирования. Необязательный параметр
				'separation_reverse'=>true,
				'delimiter'=>'-',// Разделитель символов значения атрибута при количественном группировании. Необязательный параметр
				'decoration'=>' .-()',// Набор декоративных символов значения атрибута. Необязательный параметр
				'regexp'=>'^([A-z]*|[A-z]+[0-9]*)$',// Регулярное выражение для проверки значения атрибута
				'groups'=>array(// Список групп, входящих в шаблон. Необязательный параметр
					array(
						'id'=>1,
						'state'=>'significant'
					),
					...
				),
				'sets'=>array(// Список регулярных выражений групп, входящих в шаблон. Необязательный параметр
					1=>'A-z',
					2=>'0-9'
				)
			)
	 */
	public function attributePatterns(){
		if ($this->_attributePatterns === null){
			$this->_attributePatterns = array();
			
			foreach ($this->getClassAttributeNames() as $attribute)
				if ((!$pattern = Yii::app()->attributePattern->get($this, $attribute)) || !$pattern['groups'])
					$this->_attributePatterns[$attribute] = static::PATTERN_DEF_MODE < 1 + !$pattern ? false :
						array('regexp'=>'^['.static::PATTERN_REGEXP_DEF.']*$');
				else{
					Arrays::sort($pattern['groups'], 'pos');
					$delimiter = $decoration = false;
					$groups = $sets = $groups_params = array();
					
					foreach ($pattern['groups'] as $group)
						if ($regexp = Yii::app()->attributePatternGroupSet->get($id_set = $group['id_set'], 'regexp')){
							$groups[] = array('id'=>$id_set, 'state'=>$group['state']);
							$sets[$id_set] = $regexp;
							$length_min = (int)($group['length_min'] !== null ? $group['length_min'] : $group['state'] !== 'decorative');
							
							if ($group['state'] === 'decorative')
								$decoration .= $regexp;
							
							if (!$stack = Arrays::pop($groups_params, $pos = $group['pos']))
								$groups_params[$pos] = array(
									'regexp'=>$regexp,
									'length_min'=>$length_min,
									'length_max'=>(int)$group['length_max']
								);
							else
								$groups_params[$pos] = array(
									'regexp'=>$stack['regexp'].$regexp,
									'length_min'=>min($length_min, $stack['length_min']),
									'length_max'=>$group['length_max'] && $stack['length_max'] ?
										max($group['length_max'], $stack['length_max']) : 0
								);
						}
					
					$qu_groups = count($groups_params);
					$groups_regexp = array_fill(0, $qu_groups, '');
					
					foreach (array_values($groups_params) as $key_group=>$params){
						$qu_chars = ','.($params['length_max'] ? $params['length_max'] : '');
						$qu_chars = array('0'.$qu_chars, $params['length_min'].$qu_chars);
						
						foreach ($qu_chars as $key_qu=>$qu_char)
							$qu_chars[$key_qu] = $qu_char == '0,' ? '*' : ($qu_char == '0,1' ? '?' : ($qu_char == '1,' ? '+' :
								($key_qu && $params['length_min'] === $params['length_max'] ?
								($params['length_min'] !== 1 ? "{{$params['length_min']}}" : '') : "{{$qu_char}}")));
						
						for ($count = $key_group; $count < $qu_groups; $count++)
							$groups_regexp[$count] .= "[{$params['regexp']}]".$qu_chars[$count != $key_group];
					}
					
					$regexp = '^'.($qu_groups == 1 ? $groups_regexp[0] : '('.implode('|', $groups_regexp).')').'$';
					
					if ($pattern['groups_separation'] && $decoration){
						$pos = (int)($decoration[0] === '\\');
						$delimiter = $decoration[$pos];
						$decoration = mb_substr($decoration, $pos + 1);
					}
					
					$this->_attributePatterns[$attribute] = Arrays::forming(array(
						'groups_separation'=>$pattern['groups_separation'],
						'groups_min'=>$pattern['groups_min'],
						'separation_reverse'=>$pattern['separation_reverse'],
						'delimiter'=>$delimiter,
						'decoration'=>$decoration,
						'regexp'=>$regexp,
						'groups'=>$groups,
						'sets'=>$sets
					));
				}
		}
		
		return $this->_attributePatterns;
	}
	
	/**
	 * Получение параметров шаблона конкретного атрибут модели
	 * @param string $attribute Название атрибута
	 * @param string $addition Дополнительные символы шаблона. По умолчанию равно false
	 * @return array|boolean Параметры шаблона
	 */
	public function getAttributePattern($attribute = false, $addition = false){
		$patterns = $this->attributePatterns();
		$pattern = isset($patterns[$attribute]) ? $patterns[$attribute] : false;
		
		if ($pattern && $addition)
			$pattern['regexp'] = str_replace(']', $addition.']', $pattern['regexp']);
		
		return $pattern;
	}
	
	/**
	 * Метод-геттер для определения _attributeStates
	 * @return array Список статусов атрибутов модели. Определяется согласно справочнику атрибутов и параметрам класса модели
	 */
	public function getAttributeStates(){
		if ($this->_attributeStates === null)
			if ($this->_attributeStates = Yii::app()->attribute->get($this, false, 'state')){
				$attributes_unsafe = $this->_rules ? $this->unsafeAttributeNames : false;
				$limiter_states = Arrays::extensionParams($this->limiterStates());
				$state_codes = Yii::app()->conformity->get('attribute_state', 'code', 'name');
				$state_names = array_flip($state_codes);
				$confirm_attributes = Yii::app()->attribute->confirmAttributeNames;
				
				foreach ($this->_attributeStates as $attribute=>&$state){
					$states = array();
					
					if ($attributes_unsafe && in_array($attribute, $attributes_unsafe))
						$states[] = in_array($attribute, $confirm_attributes) ? 'disabled' : 'view';
					
					if ($limiter_state = Arrays::pop($limiter_states, $attribute))
						$states[] = $limiter_state;
					
					if ($states){
						$states[] = $state;
						$state = $state_names[min(Arrays::filterKey($state_codes, $states))];
					}
				}
				unset($state);
			}
		
		return $this->_attributeStates;
	}
	
	/**
	 * Метод-геттер для определения _rules
	 * @return array Правила проверки атрибутов
	 */
	public function getRules(){
		return $this->_rules;
	}
	
	/**
	 * Метод-геттер для определения _allowedAttributeNames
	 * @return array Список названий доступных к чтению атрибутов данной модели
	 */
	public function getAllowedAttributeNames(){
		if ($this->_allowedAttributeNames === null){
			$this->_allowedAttributeNames = array();
			
			if ($access_attributes = $this->accessAttributes)
				foreach ($access_attributes as $attribute=>$access)
					if ($access)
						$this->_allowedAttributeNames[] = $attribute;
		}
		
		return $this->_allowedAttributeNames;
	}
	
	/**
	 * Метод-геттер для определения _unsafeAttributeNames
	 * @return array Список небезопасных атрибутов данной модели
	 */
	public function getUnsafeAttributeNames(){
		if ($this->_unsafeAttributeNames === null){
			$this->_unsafeAttributeNames = $this->classAttributeNames;
			
			if ($rules = $this->rules)
				foreach ($rules as $rule)
					if (($names = Arrays::pop($rule, 0)) && ($validator = Arrays::pop($rule, 1)) && $validator !== 'unsafe')
						$this->_unsafeAttributeNames = array_diff($this->_unsafeAttributeNames, Strings::devisionWords($names));
		}
		
		return $this->_unsafeAttributeNames;
	}
	
	/**
	 * Метод-геттер для определения _classAttributeNames
	 * @return array Список всех атрибутов модели
	 */
	public function getClassAttributeNames(){
		if ($this->_classAttributeNames === null){
			$this->_classAttributeNames = array();
			
			if ($class = new ReflectionClass(get_class($this)))
				foreach ($class->getProperties() as $property)
					if ($property->isPublic() && !$property->isStatic())
						$this->_classAttributeNames[] = $property->getName();
		}
		
		return $this->_classAttributeNames;
	}
	
	/**
	 * Метод-геттер для определения _multipleAttributeNames
	 * @return array Список атрибутов-списков модели
	 */
	public function getMultipleAttributeNames(){
		if ($this->_multipleAttributeNames === null){
			$this->_multipleAttributeNames = array();
			$class = get_class($this);//!!!!!!!!!!!!!!!!!!!
			$model = new $class;
			
			foreach ($this->classAttributeNames as $attribute)
				if (is_array($model->$attribute))
					$this->_multipleAttributeNames[] = $attribute;
		}
		
		return $this->_multipleAttributeNames;
	}
	
	/**
	 * Метод-геттер для определения _owner
	 * @return boolean|UserModel Владелец текущей модели или false
	 */
	public function getOwner(){
		if ($this->_owner === null){
			$this->_owner = false;
			
			if ($this->getAr())
				if ($this->getModelName() == 'user'){
					if ($this->id)
						$this->_owner = $this;
				}elseif ($relations = $this->relations())
					foreach ($relations as $name=>$params)
						if (Arrays::pop($params, 0) === self::BELONGS_TO && Arrays::pop($params, 2) === 'id_owner' && $this->id_owner){
							$this->_owner = $this->id_owner == Yii::app()->user->id ? Yii::app()->user->model : $this->$name;
							break;
						}
		}
		
		return $this->_owner;
	}
	
	/**
	 * Метод-геттер для определения _addressee
	 * @return boolean|UserModel Адресат текущей модели или false
	 */
	public function getAddressee(){
		if ($this->_addressee === null){
			$this->_addressee = false;
			
			if ($this->getAr() && ($relations = $this->relations()))
				foreach ($relations as $name=>$params)
					if (Arrays::pop($params, 0) === self::BELONGS_TO && Arrays::pop($params, 2) === 'id_addressee' && $this->id_addressee){
						$this->_addressee = $this->id_addressee == Yii::app()->user->id ? Yii::app()->user->model : $this->$name;
						break;
					}
		}
		
		return $this->_addressee;
	}
	
	/**
	 * Метод-геттер для определения _action
	 * @return string Символьный идентификатор текущего действия над моделью
	 */
	public function getAction(){
		if ($this->_action === null){
			$this->_action = false;
			$aliases = Strings::devisionWords(static::ACTION_SCENARIO_ALIASES);
			$actions = array_merge(static::ACTIONS, $aliases);
			$actions_aliases = Arrays::extensionParams(array_flip(static::ACTION_SCENARIO_ALIASES));
			
			foreach (array($this->scenario, Yii::app()->controller->action->id) as $source)
				if ($source)
					foreach ($actions as $name)
						if (mb_strpos($source, $name) !== false){
							$this->_action = in_array($name, $aliases) ? $actions_aliases[$name] : $name;
							break 2;
						}
			
			if (!$this->_action)
				$this->_action = static::ACTION_DEF;
		}
		
		return $this->_action;
	}
	
	/**
	 * Метод-сеттер для установки _action
	 * @param string $value Символьный идентификатор действия
	 * @param boolean $agressive Признак агрессивной установки, без проверки правильности. По умолчанию равно false
	 */
	public function setAction($value, $agressive = false){
		if ($agressive || in_array($value, static::ACTIONS)){
			$this->_action = $value;
			
			$this->_attributeStates = $this->_rulesCompute = $this->_safeAttributeNames = $this->_allowedAttributeNames =
				$this->_actions = $this->_accessAttributes = $this->_accessValues = $this->_access =
				$this->_confirmTypes = $this->_confirmParams = null;
		}
	}
	
	/**
	 * Метод-геттер для определения _command
	 * @return boolean|string Символьный идентификатор вызываемой команды над моделью или false
	 */
	public function getCommand(){
		if ($this->_command === null)
			$this->_command = ($command = Arrays::pop($_POST, Html::ID_COMMAND_DEF)) ? $command : false;
		
		return $this->_command;
	}
	
	/**
	 * Метод-геттер для определения _actions. Также инициализирует _accessAttributes, _accessValues и _confirmTypes. Определение
	 * прав доступа к модели данных, её атрибутам и их значениям для текущего пользователя, а так же типов подтверждений
	 * действий данной модели
	 * @return array Список доступных действий над данной моделью для текущего пользователя
	 */
	public function getActions(){
		if ($this->_actions === null){
			$params = array('model'=>$this->getModelName());
			
			try{
				$attribute_states = $this->attributeStates;
				$access_data = Yii::app()->accessAttribute->get($this->getModelName());
				
				if (!$attribute_states || !$access_data || !static::ACTIONS)
					throw new CHttpException(500);
				
				$attribute_values = Yii::app()->attribute->get($this->getModelName(), false, 'values');
				$attributes = array_keys($attribute_states);
				$access_actions = array_fill_keys(static::ACTIONS, false);
				$access_attributes = array_fill_keys($attributes, false);
				$access_values = $confirm_attributes = array();
				$attributes = array_diff($attributes, Yii::app()->attribute->confirmAttributeNames);
				$confirm_types = array_fill_keys(static::ACTIONS, array());
				$user = Yii::app()->user;
				$operators = Yii::app()->user->getRoles($user->id);
				
				if (isset($access_actions['list']))
					$access_actions['list'] = true;
				
				foreach ($attribute_states as $attribute=>$state){
					if ($values = Arrays::pop($attribute_values, $attribute))
						foreach ($values as $value)
							$access_values[$attribute][$value['name']] = array_merge($value, array('access'=>false));
					
					if (in_array($attribute, static::CONFIRM_MODIFY_ATTRIBUTES) && $state == 'critical')
						if ($attribute == 'password'){
							if (!empty($this->post[$attribute]) && !Hashing::check($this->post[$attribute], $this))
								$confirm_attributes[] = $attribute;
						}elseif (!$this->post || empty($this->post[$attribute]) ||
							(isset($this->post[$attribute]) && $this->$attribute != $this->post[$attribute]))
								$confirm_attributes[] = $attribute;
				}
				
				if ($id_user = $user->id)
					$operators[] = $id_user;
				
				if ($this->owner){
					if ($this->owner->id === $id_user)
						$operators[] = AccessAttribute::KEY_OWNER;
					
					$owners = $user->getRoles($this->owner->id);
					$owners[] = $this->owner->id;
				}else
					$owners = array(AccessAttribute::KEY_NONE);
				
				if (($addressee = $this->getAddressee()) && $addressee->id === $id_user)
					$operators[] = AccessAttribute::KEY_ADDRESSEE;
				
				if (method_exists($this, 'getWitness') && $this->getWitness())
					$operators[] = AccessAttribute::KEY_WITNESS;
				
				while (list($operator, $operator_data) = isset($access_data[0]) ?
					array(0, Arrays::pop($access_data, 0, true)) : each($access_data))
						if (!$operator || !array_diff(Strings::devisionWords($operator, false, '&'), $operators))
							while (list($owner, $owner_data) = isset($operator_data[0]) ?
								array(0, Arrays::pop($operator_data, 0, true)) : each($operator_data))
									if (!$owner || in_array($owner, $owners))
										while (list($attribute, $attribute_data) = isset($owner_data[0]) ?
											array(0, Arrays::pop($owner_data, 0, true)) : each($owner_data))
												if (!$attribute || in_array($attribute, $attributes)){
													if ($attribute && $attribute_states[$attribute] === 'disabled')
														continue;
													
													while (list($value, $value_data) = isset($attribute_data[0]) ?
														array(0, Arrays::pop($attribute_data, 0, true)) : each($attribute_data))
															if (!$value || ($attribute && isset($access_values[$attribute]) &&
																in_array($value, array_keys($access_values[$attribute]))))
																	while (list($attribute_cond, $attribute_cond_data) = isset($value_data[0]) ?
																		array(0, Arrays::pop($value_data, 0, true)) : each($value_data))
																			if (!$attribute_cond || in_array($attribute_cond, $attributes))
																				while (list($value_cond, $value_cond_data) = isset($attribute_cond_data[0]) ?
																					array(0, Arrays::pop($attribute_cond_data, 0, true)) : each($attribute_cond_data))
																						if (!$attribute_cond || (isset($access_values[$attribute_cond]) && $value_cond &&
																							in_array($value_cond, array_keys($access_values[$attribute_cond])) &&
																							$this->$attribute_cond === $access_values[$attribute_cond][$value_cond]['code']) ||
																							(!isset($access_values[$attribute_cond]) &&
																							@(bool)$this->$attribute_cond === @(bool)$value_cond))
																								foreach ($value_cond_data['actions'] as $action=>$enabled)
																									if (in_array($action, $attribute || $value ?
																										array('view', 'update') : static::ACTIONS)){
																											if ($attribute && $attribute_states[$attribute] === 'view' &&
																												$action === 'update' && $enabled)
																													$action = 'view';
																											
																											if ($value)
																												$this->changeAction($access_values[$attribute][$value]['access'],
																													$action, $enabled);
																											elseif ($attribute){
																												$this->changeAction($access_attributes[$attribute], $action, $enabled);
																												
																												if (isset($access_values[$attribute]))
																													foreach ($access_values[$attribute] as $access_value=>$features)
																														$this->changeAction(
																															$access_values[$attribute][$access_value]['access'], $action,
																															$enabled);
																											}else{
																												$this->changeAction($access_actions, $action, $enabled);
																												
																												if (in_array($action, array('view', 'update')))
																													foreach ($access_attributes as $access_attribute=>$features)
																														if (in_array($access_attribute, $attributes) &&
																															$attribute_states[$access_attribute] !== 'disabled'){
																																$access_action =
																																	$attribute_states[$access_attribute] === 'view' &&
																																	$action === 'update' && $enabled ? 'view' : $action;
																																
																																$this->changeAction($access_attributes[$access_attribute],
																																	$access_action, $enabled);
																																
																																if (isset($access_values[$access_attribute]))
																																	foreach ($access_values[$access_attribute] as
																																		$access_value=>$features)
																																			$this->changeAction($access_values[$access_attribute]
																																				[$access_value]['access'], $access_action, $enabled);
																															}
																											}
																										}
												}
				
				foreach ($access_values as $attribute=>$values){
					$access_value = false;
					
					foreach ($values as $value=>$features)
						if (static::ACTION_WEIGHT[$features['access']] > static::ACTION_WEIGHT[$access_value])
							$access_value = $features['access'];
					
					if (static::ACTION_WEIGHT[$access_attributes[$attribute]] > static::ACTION_WEIGHT[$access_value])
						$access_attributes[$attribute] = $access_value;
				}
				
				$access_attribute = false;
				
				foreach ($access_attributes as $attribute=>$access)
					if (static::ACTION_WEIGHT[$access] > static::ACTION_WEIGHT[$access_attribute])
						$access_attribute = $access;
				
				if ($access_attribute !== 'update')
					$this->changeAction($access_actions, $access_attribute ? 'update' : 'view', false, true);
				
				$confirm_types_def = Yii::app()->actionConfirm->get($this->getModelName(), false, 'type');
				$confirm_action = $this->command && isset($access_actions[$this->command]) ? $this->command : $this->action;
				$type_codes = Yii::app()->conformity->get('model_action_confirm_type', 'code', 'name', 'none');
				
				foreach ($access_actions as $action=>$access)
					if ($access){
						if (Yii::app()->controller->action->id == 'registration' &&
							in_array(Yii::app()->setting->get('user_registration_type'), array('simple', 'confirm_admin')))
								$types = array();
							else{
								$types = $confirm_attributes;
								
								if (($type_def = Arrays::pop($confirm_types_def, $action)) && $type_def != 'none' &&
									!in_array($type_def, $types) && ($type_def != 'query' || !$types))
										$types[] = $type_def;
							}
						
						if ($types){
							$confirm_skip = Yii::app()->user->confirmSkip;
							
							foreach ($types as $key=>$type)
								if (($confirm_skip && $type_codes[$type] <= $type_codes[$confirm_skip]) ||
									($type == 'password' && $user->isGuest))
										unset($types[$key]);
							
							foreach ($types as $type)
								if (($type == 'captcha' && !$user->checkAccess('*/captcha')) ||
									(in_array($type, static::CONFIRM_CONNECTION_ATTRIBUTES) &&
									(!Yii::app()->$type->on || !$user->checkAccess('*/code') ||
									($this->getModelName() != 'user' && ($user->isGuest || !$user->$type)))))
											$access_actions[$action] = false;
						}
						
						$confirm_types[$action] = $types;
						
						if ($action == $confirm_action && $access_actions[$action] && !isset($_GET['code'])){//!!!
							$all_types = !$this->post && array_intersect(static::CONFIRM_JUID_ATTRIBUTES, $types);
							
							foreach (array_keys($type_codes) as $type)
								if (in_array($type, $types) || $all_types)
									$access_attributes[$type.static::POSTFIX_CONFIRM_ATTRIBUTES] = 'update';
						}
						
						//!!
						if ($this->getModelName() == 'user' && $action == 'update' && $attribute_states['passwordcode'] !== 'disabled' &&
							($id_user == $this->id || !$user->checkAccess('password'.static::POSTFIX_CONFIRM_SKIP)))
								$access_attributes['passwordcode'] = 'update';
					}
			}catch (CException $e){
				$params['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('model_init_access', $params, $ok ? 'ok' : 'model_init_access');
			
			list($this->_actions, $this->_accessAttributes, $this->_accessValues, $this->_confirmTypes) =
				$ok ? array($access_actions, $access_attributes, $access_values, $confirm_types) : false;
		}
		
		return $this->_actions;
	}
	
	/**
	 * Метод-геттер для определения _accessAttributes
	 * @return array Ассоциативный массив с информацией о доступности атрибутов данной модели текущему пользователю
	 */
	public function getAccessAttributes(){
		if ($this->_accessAttributes === null)
			$this->actions;
		
		return $this->_accessAttributes;
	}
	
	/**
	 * Метод-геттер для определения _accessValues
	 * @return array Ассоциативный массив с информацией о доступности значений атрибутов данной модели текущему пользователю
	 */
	public function getAccessValues(){
		if ($this->_accessValues === null)
			$this->actions;
		
		return $this->_accessValues;
	}
	
	/**
	 * Установка права доступа к заданному фиксированному значению заданного атрибута модели
	 * @param string $attribute Название атрибута модели, имеющего набор фиксированных значений
	 * @param string $value Символьный идентификатор фиксированного значения атрибута модели
	 * @param boolean|string $access Новое значение права доступа. По умолчанию равно false.
	 */
	public function setAccessValue($attribute, $value, $access = false){
		if (($values = $this->getAccessValues()) && isset($values[$attribute][$value])){
			if (!in_array($access, array(false, 'view', 'update')))
				$access = false;
			
			$this->_accessValues[$attribute][$value]['access'] = $access;
		}
	}
	
	/**
	 * Метод-геттер для определения _confirmTypes
	 * @return array Список символьных идентификаторов необходимых типов проверок данной модели данных
	 */
	public function getConfirmTypes(){
		if ($this->_confirmTypes === null)
			$this->actions;
		
		return $this->_confirmTypes;
	}
	
	/**
	 * Метод-геттер для определения _confirmParams
	 * @return array|boolean Ассоциативный массив параметров кодов подтверждения введённых данных текущей модели или false
	 * в случае отсутствия необходимости подтверждения кодами
	 * Формат массива:
	 * 	array(
	 * 		'open'=>false,// Признак открытия диалога ввода кодов
	 * 		'attributes'=>array(// Параметры атрибутов
	 * 			{Название атрибута с кодом}=>array(
	 * 				'value'=>'234',// Значение атрибута
	 * 				'error'=>'Ошибка',// Сообщение об ошибке в значении атрибута
	 * 				'visible'=>true,// Признак видимости атрибута на форме подтверждения
	 * 				'timer'=>234211,// Время действия кода в секундах
	 * 				'count'=>2// Количество неудачных попыток ввода
	 * 			),
	 * 			...
	 * 		)
	 * 	)
	 */
	public function getConfirmParams(){
		if ($this->_confirmParams === null)
			if (!$confirm_types = array_diff($this->confirmTypes, array('captcha')))
				$this->_confirmParams = false;
			else{
				$confirm_attributes = Strings::mergeWords($confirm_types, false, false, static::POSTFIX_CONFIRM_ATTRIBUTES, false);
				$errors = $this->errors;
				$errors_confirm = Arrays::pop($errors, implode(', ', $confirm_attributes), true, true);
				$this->_confirmParams['open'] = $this->post && !$errors;
				
				foreach ($confirm_attributes as $confirm_attribute)
					$this->_confirmParams['attributes'][$confirm_attribute] = array(
						'value'=>$this->$confirm_attribute,
						'error'=>!$errors && isset($errors_confirm[$confirm_attribute]) ? reset($errors_confirm[$confirm_attribute]) : null,
						'visible'=>!$this->$confirm_attribute || isset($errors_confirm[$confirm_attribute]),
// 						'timer'=>0,
// 						'count'=>0
					);
			}
		
		return $this->_confirmParams;
	}
	
	/**
	 * Метод-геттер для определения _access
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Результат проверки права доступа на совершение текущего действия над данной моделью для
	 * текущего пользователя
	 */
	public function getAccess($fix = true){
		if ($this->_access === null)
			$this->_access = $this->checkAccess($this->command ? $this->command : $this->action, $fix);
		
		return $this->_access;
	}
	
	/**
	 * Проверка права доступа на совершение данного действия над данной моделью для текущего пользователя
	 * @param string $action Символьный идентификатор действия над моделью, доступ к которому проверяется
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean Результат проверки права доступа
	 */
	public function checkAccess($action, $fix = true){
		if (!$access = !empty($this->actions[$action]))
			foreach (static::ACTION_COMMAND_ALIASES as $name=>$aliases)
				if (in_array($action, Strings::devisionWords($aliases), true))
					if ($access = !empty($this->actions[$name])){
						$action_alias = $action;
						$action = $name;
						break;
					}
		
		if ($fix)
			Yii::app()->log->fix('model_check_access', Arrays::forming(array(
				'model_name'=>$this->getModelName(),
				'model'=>$this->getAttributes($this->classAttributeNames),
				'actions'=>$this->actions,
				'action'=>$action,
				'action_alias'=>isset($action_alias) ? $action_alias : false
			)), $access ? 'ok' : 'model_access');
		
		return $access;
	}
	
	/**
	 * Метод-геттер для определения _prevValues
	 * @return array Ассоциативный массив предыдущих значений атрибутов модели
	 */
	public function getPrevValues(){
		return $this->_prevValues;
	}
	
	/**
	 * Метод-геттер для определения _post
	 * @return array|boolean Информация по данной модели из массива $_POST или false
	 */
	public function getPost(){
		if ($this->_post === null){
			if (!$post = Arrays::pop($_POST, get_class($this)))
				$this->_post = false;
			else{
				$this->_post = array_merge(array_fill_keys($this->classAttributeNames, null), $post);
				
				foreach ($this->_post as $name=>&$value){
					if ($value === false || $value === '')
						$value = null;
					
					if (in_array($name, $this->multipleAttributeNames))
						$value = (array)$value;
				}
				unset($value);
			}
		}
		
		return $this->_post;
	}
	
	/**
	 * Метод-сеттер для установки _post
	 * @param boolean $mode Код режима установки. По умолчанию равно self::SET_POST_MODE_NORMAL
	 */
	public function setPost($value, $mode = self::SET_POST_MODE_NORMAL){
		if ($mode == self::SET_POST_MODE_AGRESSIVE || ($mode == self::SET_POST_MODE_NORMAL && $this->_post))
			$this->setAction($this->action);
		
		$this->_post = $value;
	}
	
	/**
	 * Удаление декоративных символов в атрибутах модели 
	 * @param array|boolean|string $attributes Название атрибута или список атрибутов, чистку которых необходимо провести, или
	 * false. По умолчанию равно false. Если равно false, будет призведена чистка всех доступных для чтения атрибутов модели
	 * @param boolean $set Признак необходимости установки значений атрибутов. По умолчанию равно true
	 * @param boolean $allowed Признак обработки только доступных для чтения атрибутов. По умолчанию равно true
	 * @return mixed Ассоциативный массив очищенных значений атрибутов
	 */
	public function removeDecoration($attributes = false, $set = true, $allowed = true){
		$allowed_names = $allowed ? $this->getAllowedAttributeNames() : $this->getClassAttributeNames();
		$attributes_check = !$attributes ? $allowed_names : array_intersect((array)$attributes, $allowed_names);
		$result = array();
		
		foreach ($attributes_check as $attribute){
			if (($value = $this->$attribute) && ($pattern = $this->getAttributePattern($attribute)) &&
				($decoration = Arrays::pop($pattern, 'delimiter').Arrays::pop($pattern, 'decoration')))
					$value = preg_replace("/[$decoration]/u", '', $value);
			
			if (in_array($attribute, array('sum', 'new')))
				$value = str_replace(',', '.', $value);
			elseif ($attribute == 'phone' && $value)
				$value = '8'.mb_substr($value, 1);
			elseif ($attribute == 'address')
				$value = trim($value); 
			
			if ($set)
				$this->$attribute = $value;
			
			$result[$attribute] = $value;
		}
		
		return !is_string($attributes) ? $result : ($result ? reset($result) : false);
	}
	
	/**
	 * Добавление декоративных символов в атрибутах модели
	 * @param array|boolean|string $attributes_check Список атрибутов или атрибута , декорирование которых необходимо провести.
	 * По умолчанию равно false.
	 * Если равно false, будет призведено декорирование всех доступных для чтения атрибутов модели
	 * @param boolean $set Признак необходимости установки значений атрибутов. По умолчанию равно true
	 * @param boolean $remove Признак необходимости проведения предварительного удаления декорирования. По умолчанию равно true
	 * @return mixed Ассоциативный массив декарированных значений атрибутов
	 */
	public function addDecoration($attributes = false, $remove = true, $set = true){
		$allowed_names = $this->allowedAttributeNames;
		$attributes_check = !$attributes ? $allowed_names : array_intersect((array)$attributes, $allowed_names);
		$result = array();
		
		if ($attributes_check && $remove)
			$this->removeDecoration($attributes_check, $set);
		
		foreach ($attributes_check as $attribute)
			if (($value = $this->$attribute) && ($pattern = $this->getAttributePattern($attribute)) &&
				($separation = Arrays::pop($pattern, 'groups_separation')) && ($delimiter = Arrays::pop($pattern, 'delimiter'))){
					$separation = str_split($separation);
					$reverse = Arrays::pop($pattern, 'separation_reverse');
					$value_new = '';
					
					if ($reverse)
						$value = strrev($value);
					
					while ($value){
						if ($value_new)
							$value_new .= $delimiter;
						
						if ($separation)
							$length_group = array_shift($separation);
						
						$value_new .= mb_substr($value, 0, $length_group);
						$value = mb_substr($value, $length_group);
					}
					
					if ($reverse)
						$value_new = strrev($value_new);
					
					if ($set)
						$this->$attribute = $value_new;
					
					$result[$attribute] = $value_new;
				}
		
		return !is_string($attributes) ? $result : ($result ? reset($result) : false);
	}
	
	/**
	 * Получение ограничителей статусов атрибутов для текущего действия модели
	 * @return array Ассоциативный массив в формате {Название атрибута}=>{Символьный идентификатор максимального статуса атрибута}.
	 * Допускается группировать атрибуты, записывая их названия в ключах через запятую
	 */
	public function limiterStates(){
		return array();
	}
	
	/**
	 * Получение ограничителей сценариев
	 * @return array Ассоциативный массив в формате
	 * 	array(
	 * 		{Название атрибута}=>array(
	 * 			{Символьный идентификатор валидатора}=>{Признак применения валидатора},
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	public function limiterScenario(){
		return array();
	}
	
	/**
	 * Получение объекта модели данных по названию её класса
	 * @param boolean $new Признак применения стандартного конструктора. По умолчанию равно false
	 * @return boolean|CModel Объект модели данных или false
	 */
	public static function getModel($name, $new = false){
		if (!@class_exists($name))
			return false;
		
		return !$new && method_exists($name, 'model') ? $name::model() : new $name;
	}
	
	/**
	 * Инициализация модели данных
	 * @param array|object|string $source Экземпляр объекта, название его класса или массив, содержащий элемент
	 * с ключом model, значением которого является объект или название класса. Если объект модели не создан, будет
	 * предпринята попытка его создать по названию класса. В случае ошибки элемент будет заменён на false
	 */
	public static function modelInit(&$source){
		$is_array = $in_array = is_array($source);
		
		if ($in_array){
			foreach (array_keys($source) as $key)
				if (mb_stripos($key, 'model') !== false){
					$model_name = $key;
					break;
				}
			
			$in_array = isset($model_name);
		}
		
		$model = $in_array ? $source[$model_name] : $source;
		
		if (!is_object($model)){
			if (is_string($model) && @class_exists($model)){
				$model = self::getModel($model);
				
				if ($in_array)
					$source[$model_name] = $model;
				else
					$source = $model;
			}elseif ($in_array)
				$source[$model_name] = null;
			elseif (!$is_array)
				$source = null;
		}
	}
	
	/**
	 * Определение идентификатора группы значений из справочника значений для заданного атрибута
	 * @param string $attribute Название атрибута
	 * @return string Идентификатор группы значений
	 */
	public function getAttributeValueId($attribute){
		return $this->getModelName().'_'.$attribute;
	}
	
	/**
	 * Определение диапазонов сценариев, в рамках которых действуют данные правила проверки, для функции rules()
	 * @param array $validators Список ассоциативных массивов с параметрами валидаторов
	 * @param array $except_validators Список символьных идентификаторов валидаторов, не участвующих в процессе. По умолчанию
	 * равно false
	 * @return array|boolean Список диапазонов сценариев или false
	 */
	protected function getValidatorsRange($validators, $except_validators = false){
		if (!$validators || !is_array($validators))
			return false;
		
		if ($except_validators && (!$validators = Arrays::filterKey($validators, $except_validators, false)))
			return false;
		
		foreach ($validators as $validator_name=>$validators_group)
			foreach ($validators_group as $validator){
				$on = !isset($on) ? ($validator['on'] ? $validator['on'] : false) :
					($on && $validator['on'] ? array_merge($on, $validator['on']) : false);
				$except = !isset($except) ? ($validator['except'] ? $validator['except'] : false) :
					($except && $validator['except'] ? array_intersect($except, $validator['except']) : false);
			}
		
		if (($on = !empty($on) ? array_unique($on) : false) && ($except = !empty($except) ? array_unique($except) : false))
			$on = array_diff($on, $except);
		
		return array('on'=>$on, 'except'=>$except);
	}
	
	/**
	 * Получение наборов сообщений об ошибках для данного валидатора для функции rules()
	 * @param string $attribute Название атрибута модели данных
	 * @param string $validator Символьный идентификатор валидатора
	 * @return array|boolean Ассоциативный массив сообщений из справочника ошибок для данного валидатора или false
	 */
	protected function getValidatorMessage($attribute, $validator){
		if (!($relations = Arrays::pop($mr = static::MESSAGE_RELATIONS, $validator)) || !isset($relations['message']))
			if ($message = Yii::app()->errorAttribute->get($this->getModelName(), $attribute, $validator))
				$result['message'] = $message;
		
		if ($relations)
			foreach ($relations as $attribute=>$name)
				if ($message = Yii::app()->errorAttribute->get($this->getModelName(), $attribute, $name))
					$result[$attribute] = $message;
		
		return isset($result) ? $result : false;
	}
	
	/**
	 * Изменение прав доступа к некоторому действию для модели или её атрибута для функции getActions()
	 * @param array|boolean|string $rec Переменная хранящая права доступа. Изменения будут внесены непосредственно в эту переменную
	 * @param string $action Символьный идентификатор изменяемого действия
	 * @param boolean $enabled Признак доступности изменяемого действия
	 * @param boolean $agressive Признак агрессивного изменения прав доступа. По умолчанию равно true. Если равно false, то
	 * будет разрешена только установка новых прав
	 */
	protected function changeAction(&$rec, $action, $enabled, $agressive = true){
		$is_array = is_array($rec);
		$params = $is_array ? $rec : $this->transferAction($rec);
		
		if ($enabled !== $params[$action] && ($agressive || $enabled)){
			$params[$action] = $enabled;
			
			if ($action === 'update' && $enabled)
				$params['view'] = true;
			elseif ($action === 'view' && !$enabled)
				$params['update'] = false;
			
			$rec = $is_array ? $params : $this->transferAction($params);
		}
	}
	
	/**
	 * Смена формы представления прав доступа к атрубуту модели данных (из массива в строку и наоборот) для функции changeAction()
	 * @param array|string $action Права доступа
	 * @return array|string Права доступа в противоположной форме представления
	 */
	protected function transferAction($action){
		return is_array($action) ? ($action['update'] ? 'update' : ($action['view'] ? 'view' : false)) :
			array('view'=>(bool)$action, 'update'=>$action === 'update');
	}
	
	/**
	 * Определение строки символьных идентификаторов моделей, участвующих в запросе к БД, являющейся частью названия записи кэша
	 * @return string Строка символьных идентификаторов моделей 
	 */
	protected function getCacheKeyNames(){
		$model_names[] = $this->getModelName();
		$names = '';
		
		if (($with = $this->getDbCriteria()->with) && ($relations = $this->relations()))
			foreach ($with as $name)
				if ($name = Arrays::pop($relations, $name.'.1'))
					$model_names[] = Strings::nameModify($name);
		
		foreach (array_unique($model_names) as $model_name)
			$names .= sprintf(Cache::PATTERN_PARAM, $model_name);
		
		return $names;
	}
	
	/**
	 * Определение ограниченности в условиях запроса
	 * @param array|CDbCriterioa|string $condition Условия запроса
	 * @return boolean Признак отсутствия ограничения
	 */
	protected function checkCondition(&$condition){
		if (!$condition)
			return true;
		
		if (is_array($condition))
			$params = $condition;
		elseif ($is_criteria = $condition instanceof CDbCriteria)
			$params = (array)$condition;
		else
			return false;
		
		if (($select = Arrays::pop($params, 'select')) && $select !== Table::SELECT_ALL)
			if (!is_array($select) || count(array_diff($this->attributeNames(), $select)) > 4)
				return false;
			elseif ($is_criteria)
				$condition->select = Table::SELECT_ALL;
			else
				$condition['select'] = Table::SELECT_ALL;
		
		if (Arrays::pop($params, 'distinct') ||
			Arrays::pop($params, 'condition') ||
			(($limit = Arrays::pop($params, 'limit')) && $limit > 0) ||
			(($offset = Arrays::pop($params, 'offset')) && $offset > 0) ||
			Arrays::pop($params, 'group') ||
			Arrays::pop($params, 'join'))
				return false;
		
		return true;
	}
	
}
