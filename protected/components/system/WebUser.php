<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CWebUser
 * @author v.zubov
 * @see CWebUser
 */
class WebUser extends CWebUser{
	
	/**
	 * @var string Псевдоним пути к файлу конфигурации
	 */
	const DIR_CFG = 'application.config';
	
	/**
	 * @var string Имя файла с набором данных для генерации
	 */
	const FN_GENERATION = 'generation.php';
	
	/**
	 * @var string Префикс паролей пользователея по умолчанию
	 */
	const PREFIX_PASSWORD = 'pwd';
	
	/**
	 * @var string Префикс блоков данных моделей данных, хранимых в сессии
	 */
	const PREFIX_DATA = 'data_';
	
	/**
	 * @var string Префикс фильтров блоков данных моделей данных, хранимых в сессии
	 */
	const PREFIX_FILTER = 'filter_';
	
	/**
	 * @var string Префикс наборов данных, хранимых в сессии
	 */
	const PREFIX_SET = 'set_';
	
	/**
	 * @var array ФИО главного разработчика приложения в виде ассоциативного массива 
	 */
	const DEVELOPER_FIO = array(
		'name'=>'Виталий',
		'surname'=>'Зубов',
		'patronymic'=>'Васильевич'
	);
	
	/**
	 * @var string Пароль пользователя с ролью "Разработчик" по умолчанию
	 */
	const DEVELOPER_PASSWORD = 'warkL32';
	
	/**
	 * @var string E-mail пользователя с ролью "Разработчик" по умолчанию
	 */
	const DEVELOPER_EMAIL = 'vitaly748@gmail.com';
	
	/**
	 * @var array Список символьных идентификаторов взаимозаменяемых ролей пользователей
	 */
	const ROLES_RADIO = array('citizen', 'organization');
	
	/**
	 * @var array Ассоциативный массив ролей пользователей, связанных с внешними системами, в формате:
	 * 	array(
	 * 		{Символьный идентификатор внешней системы}=>array(
	 * 			{Символьный идентификатор роли},
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	const ROLES_EXTERNAL = array('bss1'=>array('bss1_payer'), 'bss2'=>array('bss2_payer'), 'esia'=>array('esia'));
	
	/**
	 * @var string Сценарий, устанавливаемый модели пользователя по умолчанию
	 */
	const MODEL_SCENARIO_DEF = 'update';
	
	/**
	 * @var integer Код типа подтверждения "Пользовательское соглашение"
	 */
	const CONFIRM_TYPE_AGREEMENT = 1;
	
	/**
	 * @var integer Код типа подтверждения "Протокол безопасности"
	 */
	const CONFIRM_TYPE_SECURE = 2;
	
	/**
	 * @var array Массив осуществлённых проверок прав доступа пользователя. По умолчанию равно array()
	 */
	protected $_access = array();
	
	/**
	 * @var boolean|CAuthManager Активный экземпляр Менеджера авторизации. В зависимости от ситуации может быть компонентом authDb,
	 * authPhp или false
	 */
	protected $_authManager;
	
	/**
	 * @var boolean Признак доступа текущего пользователя в приватную зону
	 */
	protected $_private;
	
	/**
	 * @var array Ассоциативный массив с описанием ролей пользователей в формате:
	 * 	array(
	 * 		{Идентификатор пользователя}=>array(
	 * 			array(
	 * 				'id'=>{Цифровой идентификатор роли},
	 * 				'name'=>{Символьный идентификатор роли},
	 * 				'description'=>{Описание роли}
	 * 			),
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	protected $_roles;
	
	/**
	 * @var array Ассоциативный массив с информацией о ролях, зарегистрированных в приложении, в формате:
	 * 	array(
	 * 		{Символьный идентификатор роли}=>array(
	 * 			'id'=>{Цифровой идентификатор роли},
	 *			'name'=>{Символьный идентификатор роли},
	 *			'description'=>{Описание роли}
	 * 		)
	 * )
	 */
	protected $_rolesList;
	
	/**
	 * @var array Ассоциативный массив идентификаторов ролей в формате
	 * 	{Цифровой идентификатор роли}=>{Символьный идентификатор роли}
	 */
	protected $_rolesId;
	
	/**
	 * @var boolean Признак авторизации пользователя
	 */
	protected $_authorized;
	
	/**
	 * @var UserModel Экземпляр класса UserModel для текущего пользователя
	 */
	protected $_model;
	
	/**
	 * @var array Список всех зарегистрированных операций
	 */
	protected $_authOperations;
	
	/**
	 * @var boolean|string Символьный идентификатор уровня пропуска подтверждений текущего пользователя или false
	 */
	protected $_confirmSkip;
	
	/**
	 * @var boolean Признак необходимости отображения быстрых сообщений отдельно от контента
	 */
	protected $_flashOnly;
	
	/**
	 * @var array Ассоциативный массив с информацией о подключенных лицевых счетах пользователя в формате
	 * 	array(
	 * 		'bss1_payer'=>array(// Символьный идентификатор роли, связанной с Биллингом
	 * 			1001=>array(// Идентификатор лицевого счёта
	 * 				'id'=>1001,// Идентификатор лицевого счёта
	 * 				'id_conjugation'=>501,// Идентификатор объекта-сопряжения
	 * 				'type'=>1,// Код типа лицевого счёта
	 * 				'kind'=>1,// Код вида лицевого счёта
	 * 				'balance'=>50.34,// Текущий баланс
	 * 				'id_house'=>3423,// Идентификатор дома
	 * 				'address'=>'Пермский край, г.Пермь, ул.Пушкина, д.41',// Адрес
	 * 				'municipality'=>'ГО город Пермь',// Муниципальное образование
	 * 				'locality'=>'г Пермь',// Населённый пункт
	 * 				'street'=>'ул. Зелёная',// Улица
	 * 				'house'=>4// Номер дома
	 * 				'premise'=>'4',// Помещение
	 *	 			'area'=>32.5,// Площадь
	 * 				'share'=>0.5,// Доля
	 * 				'services'=>array(// Список подключенных услуг
	 * 					array(
	 * 						'id'=>3,// Идентификатор услуги в рамках профиля пользователя
	 * 						'name'=>'Холодное водоснабжение',// Название
	 * 						'costs'=>array(// История изменения тарифов по услуге
	 * 							array(// Информация о изменении тарифа
	 * 								'date'=>'2016-01-01T00:00:00',// Дата установки тарифа
	 * 								'cost'=>18.14// Стоимость
	 * 							),
	 * 							...
	 * 						)
	 * 					),
	 * 					...
	 * 				),
	 * 				'recipient'=>'Министерство строительства и ремонта Пермского края',// Получатель
	 * 				'inn'=>'2342432423',// ИНН
	 * 				'kpp'=>'2342084958943',// КПП
	 * 				'bik'=>'2309089450843',// БИК
	 * 				'bank_account'=>'94983809875498754309843'// Расчётный счёт
	 * 			),
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	protected $_accounts;
	
	/**
	 * @var array Ассоциативный массив с информацией о доступных счётчиках пользователя в формате
	 * 	array(
	 * 		'bss1_payer'=>array(// Символьный идентификатор роли, связанной с Биллингом
	 * 			1001=>array(// Идентификатор лицевого счёта
	 * 				401=>array(// Идентификатор счётчика
	 * 					'id'=>401,// Идентификатор счётчика
	 * 					'counter'=>'ГВС',// Название счётчика
	 * 					'reading_previous'=>434,// Предыдущее показание счётчика
	 * 					'reading_current'=>480// Текущее показание счётчика
	 * 				),
	 * 				...
	 * 			),
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	protected $_counters;
	
	/**
	 * @var array Ассоциативный массив с информацией о доступных субсидиях пользователя в формате
	 * 	array(
	 * 		'bss1_payer'=>array(// Символьный идентификатор роли, связанной с Биллингом
	 * 			1001=>array(// Идентификатор лицевого счёта
	 * 				55=>array(// Идентификатор субсидии
	 * 					'id'=>55,// Идентификатор субсидии
	 * 					'base'=>'Приказ Президента РФ',// Основание для получения субсидии
	 * 					'type'=>'Выплата гражданину',// Тип выплаты
	 * 					'state'=>'Удовлетворено'// Статус предоставления
	 * 				),
	 * 				...
	 * 			),
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
// 	protected $_subsidies;
	
	/**
	 * @var array Ассоциативный массив с информацией о доступных компенсациях пользователя в формате
	 * 	array(
	 * 		'bss1_payer'=>array(// Символьный идентификатор роли, связанной с Биллингом
	 * 			1001=>array(// Идентификатор лицевого счёта
	 * 				55=>array(// Идентификатор компенсации
	 * 					'id'=>55,// Идентификатор компенсации
	 * 					'base'=>'Приказ Президента РФ',// Основание для получения компенсации
	 * 					'type'=>'Выплата гражданину',// Тип выплаты
	 * 					'state'=>'Удовлетворено'// Статус предоставления
	 * 				),
	 * 				...
	 * 			),
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
// 	protected $_compensations;
	
	/**
	 * @var array Ассоциативный массив соответствий ролей компонентам приложения в формате
	 * 	array(
	 * 		'bss1_payer'=>'bss1',
	 * 		...
	 * 	)
	 */
	protected $_rolesComponent;
	
	/**
	 * Переопределение стандартного метода init, инициализирующего работу компонента
	 * @see CWebUser::init() 
	 */
	public function init(){
		Html::setCookieParams();
		parent::init ();
	}
	
	/**
	 * Метод-геттер для определения _authManager
	 * @return boolean|CAuthManager Компонент активного Менеджера авторизации или false
	 */
	public function getAuthManager(){
		if ($this->_authManager === null){
			$params = array('def'=>$def = !Yii::app()->controller->dbActive);
			
			try{
				$this->_authManager = $def ? Yii::app()->authPhp : Yii::app()->authDb;
			}catch (CException $e){
				$params['msg'] = $e->getMessage();
				$this->_authManager = false;
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('auth_init', $params, $ok ? 'ok' : 'auth_init');
		}
		
		return $this->_authManager;
	}
	
	/**
	 * Метод-геттер для определения _private
	 * @return integer Признак доступа текущего пользоавателя в приватную зону
	 */
	public function getPrivate(){
		if ($this->_private === null)
			$this->_private = $this->checkAccess('private');
		
		return $this->_private;
	}
	
	/**
	 * Метод-геттер для определения _roles. Результат форматируется с помощью функции Arrays::select
	 * @param boolean|integer $id Идентификатор пользователя, роли которого необходимо узнать, или false, если нужно узнать роли
	 * текущего пользователя. По умолчанию равно false. Для определения всех зарегистрированных ролей можно указать "0"
	 * @param mixed $param1 Первый параметр функции select или false. По умолчанию равно "name"
	 * @param mixed $param2 Второй параметр функции select или false. По умолчанию равно false
	 * @param mixed $except Список значений исключяемых записей или false. По умолчанию равно false
	 * @param mixed $only Список значений необходимых записей или false. По умолчанию равно false
	 * @param boolean|string $name_conditional Название условного параметра или false. По умолчанию равно false
	 * @param boolean $strict Признак регистрозависимого поиска. По умолчанию равно true
	 * @param boolean $unique Признак уникальных записей в матрице. По умолчанию равно true
	 * @return mixed Отформатированный список ролей запрашиваемого пользователя или информация по конкретным ролям
	 * @see Arrays::select()
	 */
	public function getRoles($id = false, $param1 = 'name', $param2 = false, $except = false, $only = false,
		$name_conditional = false, $strict = true, $unique = true){
			$roles = $this->getRolesList();
			
			if ($id !== 0){
				if (!$id)
					$id = $this->isGuest ? 0 : $this->id;
				
				if (isset($this->_roles[$id]))
					$roles = $this->_roles[$id];
				else
					$roles = $this->_roles[$id] = Arrays::filterKey($roles, $id ?
						array_keys($this->getAuthManager()->getAuthAssignments($id)) : ($this->getAuthorized() ? 'developer' : 'guest'));
			}
			
			return !$roles || (!$param1 && !$except && !$only) ? array_values($roles) :
				Arrays::select($roles, $param1, $param2, $except, $only, $name_conditional, $strict, $unique);
		}
	
	/**
	 * Получение информации о ролях с заданными цифровыми идентификаторами. Результат форматируется с помощью
	 * функции Arrays::select
	 * @param array $ids Список цифровых идентификаторов ролей
	 * @param mixed $param1 Значение параметра $param1 функции select. По умолчанию равно "name"
	 * @param mixed $param2 Значение параметра $param2 функции select. По умолчанию равно false
	 * @return array|boolean Отформатированный список ролей или false в случае ошибки
	 */
	public function getRolesById($ids, $param1 = 'name', $param2 = false){
		if (!is_array($ids))
			return false;
		
		$roles = array();
		$roles_list = $this->getRolesList();
		$roles_id = $this->getRolesId();
		
		foreach ($ids as $id)
			if (($role = Arrays::pop($roles_id, $id)) && ($features = Arrays::pop($roles_list, $role)))
				$roles[] = $features;
		
		return $param1 ? Arrays::select($roles, $param1, $param2) : $roles;
	}
	
	/**
	 * Метод-геттер для определения _rolesList
	 * @return array Ассоциативный массив с информацией о ролях, зарегистрированных в приложении
	 */
	public function getRolesList(){
		if ($this->_rolesList === null){
			$auth_manager = $this->getAuthManager();
			$this->_rolesList = $this->_rolesId = array();
			
			if ($auth_manager instanceof PhpAuthManager){
				$count = 1;
				
				foreach ($auth_manager->roles as $name=>$features){
					$this->_rolesList[$name] = array(
						'id'=>$count,
						'name'=>$name,
						'description'=>($auth_item = $auth_manager->getAuthItem($name)) ? $auth_item->description : $name
					);
					
					$this->_rolesId[$count++] = $name;
				}
			}else{
				$this->_rolesList = Arrays::select($auth_manager->items,
					'id, name, description', 'name', false, CAuthItem::TYPE_ROLE, 'type');
				
				foreach ($this->_rolesList as $name=>$features)
					$this->_rolesId[$features['id']] = $name;
			}
			
			foreach (static::ROLES_RADIO as $role)
				if (isset($this->_rolesList[$role]))
					$this->_rolesList[$role]['radio'] = true;
		}
		
		return $this->_rolesList;
	}
	
	/**
	 * Метод-геттер для определения _rolesId
	 * @return array Ассоциативный массив идентификаторов ролей
	 */
	public function getRolesId(){
		if ($this->_rolesId === null)
			$this->getRolesList();
		
		return $this->_rolesId;
	}
	
	/**
	 * Метод-геттер для определения _model
	 * @param boolean|string $scenario Сценарий, который требуется установить модели. По умолчанию равно false. Если равно
	 * false, то будет установлен сценарий по умолчанию static::MODEL_SCENARIO_DEF
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return boolean|UserModel Экземпляр класса UserModel для текущего пользователя или false, если
	 * пользователь не авторизован
	 */
	public function getModel($scenario = false, $fix = true){
		if ($this->_model === null)
			if ($this->id){
				$this->_model = UserModel::model()->with('conjugation')->findByPk($this->id);
				
				if (!$this->_model)
					$this->_model = false;
				
				if ($fix)
					Yii::app()->log->fix('model_search', array('model'=>'user', 'id'=>$this->id), $this->_model ? 'ok' : 'model_search');
			}else
				$this->_model = false;
		
		if ($model = $this->_model ? clone $this->_model : false)
			$model->scenario = $scenario === false ? static::MODEL_SCENARIO_DEF : $scenario;
		
		return $model;
	}
	
	/**
	 * Метод-геттер для определения _authOperations
	 * @return array Список всех зарегистрированных операций
	 */
	public function getAuthOperations(){
		if ($this->_authOperations === null)
			$this->_authOperations = array_keys($this->getAuthManager()->getOperations());
		
		return $this->_authOperations;
	}
	
	/**
	 * Переопределение стандартного метода checkAccess, проверяющего доступ текущего пользователя к выполнению
	 * определённой операции (роль, задача, действие)
	 * @param string $operation Символьный идентификатор операции, доступ к которой нужно проверить у текущего
	 * пользователя
	 * @param array $params Ассоциативный массив параметров для вычисления бизнес-правил проверяемой операции.
	 * По умолчанию равно array()
	 * @param boolean $allowCaching Признак сохранения результатов функции в массив _access. По умолчанию равно true
	 * @return boolean Доступность данной операции текущему пользователлю
	 * @see CWebUser::checkAccess()
	 */
	public function checkAccess($operation, $params = array(), $allowCaching = true){
		if ($allowCaching && $params === array() && isset($this->_access[$operation]))
			return $this->_access[$operation];
		
		if (!$manager = $this->getAuthManager())
			return false;
		
		if ($operation === 'developer' && $manager instanceof PhpAuthManager)
			$access = ($remote_addr = Html::getUserAddr()) && $remote_addr === Yii::app()->param->get('developer');
		else
			$access = $manager->checkAccess($operation, ($id_user = Arrays::pop($params, 'id_user', true)) ?
				$id_user : $this->getId(), $params);
		
		if ($allowCaching && $params === array() && empty($id_user))
			$this->_access[$operation] = $access;
		
		return $access;
	}
	
	/**
	 * Проверка прав доступа для текущего пользователя к набору операций
	 * @param array|string $operations Список символьных идентификаторов операций, доступ к которым нужно проверить
	 * у текущего пользователя. Допускается задавать строкой через запятую
	 * @param boolean $and Признак проверки доступа к каждой операции. По умолчанию равно false. Если равно false, доступ
	 * проверяется до первой успешной проверки
	 * @param array $params Ассоциативный массив параметров для вычисления бизнес-правил проверяемой операций.
	 * По умолчанию равно array()
	 * @return boolean Успешность проверки
	 * @see CWebUser::checkAccess()
	 */
	public function checkAccessSet($operations, $and = false, $params = array()){
		$operations = is_string($operations) ? Strings::devisionWords($operations) : (array)$operations;
		$result = false;
		
		foreach ($operations as $operation){
			$access = $this->checkAccess($operation, $params);
			$result = $result || $access;
			
			if (!$access && $and)
				return false;
			elseif ($access && !$and)
				break;
		}
		
		return $result;
	}
	
	/**
	 * Метод-геттер для определения _authorized
	 * @return boolean Признак авторизации пользователя
	 */
	public function getAuthorized(){
		if ($this->_authorized === null)
			$this->_authorized = !$this->isGuest ||
				($this->getAuthManager() instanceof PhpAuthManager && $this->checkAccess('developer'));
		
		return $this->_authorized;
	}
	
	/**
	 * Подготовка к развёртыванию
	 * @param string $section Название группы таблиц
	 * @param string $type Символьный идентификатор типа предстоящего развёртывания
	 * @param array $params Ассоциативный массив с параметрами развёртывания приложения
	 * @return boolean Успешность выполнения
	 */
	public function beforeDeploy($section, $type, $params){
		try{
			if ($type === 'old'){
				if ($params['auth']['type'] === 'standart')
					if ($removed_users = Yii::app()->getComponent($params['auth']['component'])->removedUsers)
						if (!UserModel::model()->deleteAll(new DbCriteria(array('in'=>array('id', $removed_users)))))
							throw new CHttpException(500);
				
				if (!Yii::app()->table->dropTable('user_code') ||
					($params['user']['type'] !== 'old' && !Yii::app()->table->dropTable('user_conjugation')))
						throw new CHttpException(500);
			}elseif (!Yii::app()->table->dropGroupsTables($section))
				throw new CHttpException(500);
		}catch (CException $e){
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('deploy_group_before', array(
			'section'=>$section,
			'type'=>$type,
			'params'=>$params
		), $ok ? 'ok' : 'deploy_group_before');
		
		return $ok;
	}
	
	/**
	 * Развёртывание
	 * @param string $section Название группы таблиц
	 * @param string $type Символьный идентификатор типа развёртывания
	 * @param array $params Ассоциативный массив с параметрами развёртывания приложения
	 * @return boolean Успешность выполнения
	 */
	public function deploy($section, $type, $params){
		Swamper::timer($tick);
		
		try{
			if ($type !== 'old'){
				if (!Yii::app()->table->createGroupsTables($section))
					throw new CHttpException(500);
				
				$state_working = Yii::app()->conformity->get('user_state', 'code', 'working');
				$date = Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM);
				$base_roles = array_merge(array_diff(Yii::app()->getComponent($params['auth']['component'])->baseRoles,
					array('bss1_payer', 'bss2_payer', 'esia')), array('payer'));
				$developer = static::DEVELOPER_FIO;
				$delivery = Yii::app()->conformity->get('user_delivery_type', 'code', 'email');
				$address = 'г Пермь, ул Малкова, д 12';
				
				if (($key = array_search('developer', $base_roles)) !== false){
					$rows[] = array('developer', $this->preparationPassword('developer'), $state_working,
						$developer['name'], $developer['surname'], $developer['patronymic'], $address, '',
							static::DEVELOPER_EMAIL, $date, $delivery, true, true);
					unset($base_roles[$key]);
				}
				
				if ($type === 'generation' && $params['generation'] && $base_roles && ($data = $this->preparationGenerationData())){
					if ($base = in_array(Yii::app()->conformity->get('user_generation_type', 'code', 'base'), $params['generation']))
						foreach ($base_roles as $role)
							$rows[] = array($role, $this->preparationPassword($role), $state_working,
								$data['name'][mt_rand(0, $data['qu_name'])], $data['surname'][mt_rand(0, $data['qu_surname'])],
								$data['patronymic'][mt_rand(0, $data['qu_patronymic'])], $address, '',
								$role.'@softmtest.ru', $date, $delivery, true, true);
					
					if (in_array(Yii::app()->conformity->get('user_generation_type', 'code', 'rand'), $params['generation'])){
						$logins = array();
						
						for ($count = 0; $count < $params['quantity']; $count++){
							$login = $data['login'][mt_rand(0, $data['qu_login'])];
							
							while (in_array($login, $logins))
								$login .= mt_rand(0, 9);
							
							$logins[] = $login;
							
							$rows[] = array($login, $this->preparationPassword($login), $state_working,
								$data['name'][mt_rand(0, $data['qu_name'])], $data['surname'][mt_rand(0, $data['qu_surname'])],
								$data['patronymic'][mt_rand(0, $data['qu_patronymic'])], $address, '', $login.'@softmtest.ru', $date,
								$delivery, true, true);
						}
					}
				}
				
				if (!empty($rows)){
					array_unshift($rows, array('login', 'password', 'state', 'name', 'surname', 'patronymic', 'address', 'addressid',
						'email', 'registration', 'delivery', 'agreement', 'secure'));
					
					if (!Yii::app()->db->createCommand()->insert('user', $rows))
						throw new CHttpException(500);
					
					if (!empty($base) && ($role_payer = reset(Yii::app()->user->getRoles(0, 'id', false, false, 'bss2_payer'))) &&
						!Yii::app()->db->createCommand()->insert('user_conjugation', array(array('id_owner', 'id_external', 'id_role'),
						array(count($base_roles) + 1, 2424711, $role_payer))))
							throw new CHttpException(500);
				}
			}else if (!Yii::app()->table->createTable('user_code') ||
				($params['user']['type'] !== 'old' && !Yii::app()->table->createTable('user_conjugation')))
					throw new CHttpException(500);
		}catch (CException $e){
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('deploy_group', array(
			'section'=>$section,
			'type'=>$type,
			'params'=>$params,
			'timer'=>Swamper::timer($tick)
		), $ok ? 'ok' : 'deploy_group');
		
		return $ok;
	}
	
	/**
	 * Метод-геттер для определения _confirmSkip
	 * @return boolean|string Символьный идентификатор уровня пропуска подтверждений текущего пользователя или false
	 */
	public function getConfirmSkip(){
		if ($this->_confirmSkip === null){
			$this->_confirmSkip = false;
			
			if ($this->getAuthorized() && ($types = Yii::app()->conformity->get('model_action_confirm_type', 'name', false, 'none')))
				foreach (array_reverse($types) as $type)
					if ($this->checkAccess($type.BaseModel::POSTFIX_CONFIRM_SKIP)){
						$this->_confirmSkip = $type;
						break;
					}
		}
		
		return $this->_confirmSkip;
	}
	
	/**
	 * Переопределение стандартного метода setFlash, заносящего данные flash-сообщений в сессию пользователя
	 * @param string $key Символьный идентификатор типа flash-сообщения
	 * @param mixed $value Данные flash-сообщения
	 * @param mixed $defaultValue Значение по умолчанию. По умолчанию равно null
	 * @see CWebUser::setFlash()
	 */
	public function setFlash($key, $value, $defaultValue = null){
		$value = array($value);
		
		if (is_array($value_old = $this->getState(self::FLASH_KEY_PREFIX.$key)))
			$value = array_merge($value_old, $value);
		
		parent::setFlash($key, $value, $defaultValue);
	}
	
	/**
	 * Переопределение стандартного метода changeIdentity, обновляющего данные сессии
	 * @param mixed $id Цифровой идентификатор пользователя
	 * @param string $name Символьный идентификатор пользователя
	 * @param array $states Данные сессии
	 */
	/* protected function changeIdentity($id, $name, $states){
		Yii::app()->getSession()->regenerateID(false);
		$this->setId($id);
		$this->setName($name);
		$this->loadIdentityStates($states);
	} */
	
	/**
	 * Метод-геттер для определения _flashOnly
	 * @return boolean Признак необходимости отображения быстрых сообщений отдельно от контента
	 */
	public function getFlashOnly(){
		if ($this->_flashOnly === null){
			$this->_flashOnly = false;
			
			if ($flashes = $this->getFlashes(false))
				foreach ($flashes as $type=>$params)
					foreach ($params as $params_message)
						if (!empty($params_message['only_flash'])){
							$this->_flashOnly = true;
							break;
						}
		}
		
		return $this->_flashOnly;
	}
	
	/**
	 * Назначение роли пользователю
	 * @param string $role Символьный идентификатор назначаемой роли
	 * @param boolean|integer $id_user Идентификатор пользователя или false. По умолчанию равно false. Если равно false, то
	 * роль назначается текущему пользователю
	 * @param integer $id_external Идентификатор пользователя во внешней системе. По умолчанию равно false. Применяется при
	 * назначении роли, связанной с внешней системой
	 * @return boolean Успешность выполнения
	 * @throws CHttpException
	 */
	public function assign($role, $id_user = false, $id_external = false){
		$params_fix = array('role'=>$role, 'id_user'=>$id_user, 'id_external'=>$id_external);
		
		try{
			if (!$id_user)
				if ($id_user = $this->id)
					$params_fix['id_user_set'] = $id_user;
				else
					throw new CHttpException(500, '"id_user" expected');
			
			$roles_external = $this->getRoles(0, 'id', 'name', false, call_user_func_array('array_merge', static::ROLES_EXTERNAL));
			
			if (($external = isset($roles_external[$role])) && !$id_external)
				throw new CHttpException(500, '"id_external" expected for external role');
			
			if (!$user = UserModel::model()->findByPk($id_user))
				throw new CHttpException(500, 'user not found');
			
			if ($external && ($conjugations = $user->conjugations) && ($role_conjugations = Arrays::pop($conjugations, $role)) &&
				isset($role_conjugations[$id_external]))
					throw new CHttpException(500, 'this connection already exists');
			
			if (!in_array($role, $this->getRoles($id_user)) && !$this->getAuthManager()->assign($role, $id_user))
				throw new CHttpException(500, 'error assign role');
			
			if ($external){
				$model = new UserConjugationModel;
				$model->setAttributes(array(
					'id_owner'=>$id_user,
					'id_external'=>$id_external,
					'id_role'=>$roles_external[$role]
				), false);
				
				if (!$model->save(false))
					throw new CHttpException(500, 'error conjugation create');
			}
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('user_assign', $params_fix, $ok ? 'ok' : 'user_assign', $this->isGuest ? 0 : $this->id);
		
		return $ok;
	}
	
	/**
	 * Лишение пользователя роли
	 * @param string $role Символьный идентификатор снимаемой роли
	 * @param boolean|integer $id_user Идентификатор пользователя или false. По умолчанию равно false. Если равно false, то
	 * роли лишается текущий пользователь
	 * @param integer $id_external Идентификатор пользователя во внешней системе. По умолчанию равно false. Применяется при
	 * лишении роли, связанной с внешней системой
	 * @return boolean|integer Успешность выполнения или в случае успеха при лишении роли, связанной с внешней системой, -
	 * количество оставшихся однородных сопряжений у данного пользователя
	 * @throws CHttpException
	 */
	public function revoke($role, $id_user = false, $id_external = false){
		$params_fix = array('role'=>$role, 'id_user'=>$id_user, 'id_external'=>$id_external);
		
		try{
			if (!$id_user)
				if ($id_user = $this->id)
					$params_fix['id_user_set'] = $id_user;
				else
					throw new CHttpException(500, '"id_user" expected');
			
			$roles_external = $this->getRoles(0, 'id', 'name', false, call_user_func_array('array_merge', static::ROLES_EXTERNAL));
			
			if (($external = isset($roles_external[$role])) && !$id_external)
				throw new CHttpException(500, '"id_external" expected for external role');
			
			if (!$user = UserModel::model()->findByPk($id_user))
				throw new CHttpException(500, 'user not found');
			
			$conjugations = $user->conjugations;
			
			if (!in_array($role, $this->getRoles($id_user)))
				throw new CHttpException(500, 'this role not set for this user');
			
			if ((!$external || count($conjugations[$role]) == 1) && !$this->getAuthManager()->revoke($role, $id_user))
				throw new CHttpException(500, 'error revoke role');
			
			if ($external){
				if (empty($conjugations[$role][$id_external]))
					throw new CHttpException(500, 'this external id not set for this user');
				
				if (!$conjugations[$role][$id_external]->delete())
					throw new CHttpException(500, 'error conjugation delete');
			}
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('user_revoke', $params_fix, $ok ? 'ok' : 'user_revoke', $this->isGuest ? 0 : $this->id);
		
		return $ok && $external ? count($conjugations[$role]) - 1 : $ok;
	}
	
	/**
	 * Проверка необходимости разавторизации пользователя
	 * @param boolean $logout Признак необходимости разавторизации в случае успешной проверки. По умолчанию равно true
	 * @param boolean $redirect Признак необходимости перенаправления пользователя при разавторизации. По умолчанию равно true
	 * @param boolean $flash Признак необходимости отображения флэш-сообщения. По умолчанию равно true
	 * @return boolean|string Причина необходимости разавторизации или false
	 */
	public function checkLogout($logout = true, $redirect = true, $flash = true){
		$controller = Yii::app()->controller;
		$reason = false;
		
		if ($controller->route != 'user/logout'){
			if (!$guest = $this->isGuest)
				if (!$controller->deployed || (!$controller->working && !$this->getPrivate()))
					$reason = 'application_state';
				elseif (!$model = $this->getModel(false, false))
					$reason = 'user_find';
				elseif (!$model->working)
					$reason = 'user_state';
			
			if (!$reason && is_array($state = $this->getState('esia'))){
				Yii::app()->log->fix('esia_logout', $state, 'ok', $state['user'] ? $state['user']['id'] : false);
				Yii::app()->user->setState('esia', null);
				
				if (!$guest)
					$reason = 'esia';
				else
					Yii::app()->controller->redirect(Yii::app()->controller->createUrl(Yii::app()->defaultController));
			}
		}
		
		if ($reason && $logout)
				$this->logout(true, $reason, $redirect, $flash);
		
		return $reason;
	}
	
	/**
	 * Переопределение стандартного метода login, осуществляющего авторизацию пользователя
	 * @param LoginModel|UserModel $identity Объект с информацией о пользователе
	 * @param integer $duration Время в секундах, в течение которого будет храниться авторизация пользователя. По умолчанию равно 0
	 * @return boolean Успешность выполнения
	 * @see CWebUser::login()
	 */
	public function login($identity, $duration = 0){
		$params_fix = array('class'=>$identity instanceof LoginModel ? 'login' :
			($identity instanceof UserModel ? 'model' : get_class($identity)), 'duration'=>$duration);
		
		try{
			if (!in_array($params_fix['class'], array('login', 'model')))
				throw new CHttpException(500, 'error class object');
			
			if ($params_fix['class'] == 'model'){
				if (!$identity->id)
					throw new CHttpException(500, 'model empty');
				
				$user = $identity;
				$identity = new UserIdentity($identity->id);
			}else{
				$params_fix['login'] = $identity->login;
				$user = $identity->user;
				$identity = $identity->identity;
			}
			
			if (!empty($user))
				$params_fix['user'] = $user->attributes;
			
			if ($identity->errorCode){
				$params_fix['error_code'] = $identity->errorCode;
				throw new CHttpException(500, 'error identity');
			}
			
			if (!parent::login($identity, $duration))
				throw new CHttpException(500, 'error login');
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('user_login', $params_fix, $ok ? 'ok' : 'user_login', !empty($user) ? $user->id : null);
		
		return $ok;
	}
	
	/**
	 * Проверка возможности авторизации пользователя
	 * @param UserModel $model Модель данных пользователя
	 * @param boolean $login Признак проведения авторизации в случае наличия возможности. По умолчанию равно true
	 * @return boolen Признак наличия возможности авторизации или успешность авторизации, если установлен параметр $login
	 */
	public function checkLogin($model, $login = true){
		if (!$model->agreement || (Html::SECURE_HOST && !Html::secureCheck() && $model->secure)){
			$this->setState('confirm', array('id'=>$model->id, 'type'=>$model->agreement ?
				static::CONFIRM_TYPE_SECURE : static::CONFIRM_TYPE_AGREEMENT));
			
			return false;
		}
		
		return $login ? $this->login($model) : true;
	}
	
	/**
	 * Переопределение стандартного метода logout, осуществляющего разавторизацию текущего пользователя
	 * @param boolean $destroySession Признак необходимости удаления данных сессии пользователя. По умолчанию равно true
	 * @param boolean|string $reason Причина необходимости разавторизации или false. По умолчанию равно false. Если равно false,
	 * будет считаться, что пользователь сам запустил процедуру разавторизации
	 * @param boolean $redirect Признак необходимости перенаправления пользователя при разавторизации. По умолчанию равно true
	 * @param boolean $flash Признак необходимости отображения флэш-сообщения. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 * @see CWebUser::logout()
	 */
	public function logout($destroySession = true, $reason = false, $redirect = true, $flash = true){
		$model_user = $this->getModel(false, false);
		
		if (is_int(Yii::app()->user->getState('esia'))){
			$url = Yii::app()->controller->createAbsoluteUrl('esia/logout');
			
			if (Html::isSecure())
				$url = Html::changeSecureUrl($url, false);
			
			Yii::app()->controller->redirect($url);
		}
		
		$params_fix = array('user'=>$model_user ? $model_user->attributes : false, 'reason'=>$reason === false ? 'manual' : $reason);
		
		try{
			parent::logout($destroySession);
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('user_logout', $params_fix, $ok ? 'ok' : 'user_logout', $model_user ? $model_user->id : 0);
		
		if ($ok){
			Yii::app()->cache->deleteExt('sql', Cache::PREFIX_RECORD.sprintf(Cache::PATTERN_PARAM, 'user').
				sprintf(Cache::PATTERN_PARAM, 'user_conjugation').sprintf(Cache::PATTERN_PARAM, $params_fix['user']['id']));
			
			if ($flash && ($label = Yii::app()->label->get('info_logout_'.$params_fix['reason'])))
				$this->setFlash('error', $label);
			
			if ($redirect)
				Yii::app()->controller->redirect(Yii::app()->controller->createUrl(Yii::app()->defaultController));
		}
		
		return $ok;
	}
	
	/**
	 * Метод-геттер для определения _accounts
	 * @return array Ассоциативный массив с информацией о подключенных лицевых счетах пользователя
	 */
	public function getAccounts(){
		if ($this->_accounts === null){
			$this->_accounts = array();
			$params_fix = array('accounts'=>array());
			
			try{
				$conjugations = $this->getModel()->conjugations;
				$roles_external = static::ROLES_EXTERNAL;
				$id_service = 1;
				
				foreach (array('bss1', 'bss2') as $component)
					if ($roles_bss = Arrays::pop($roles_external, $component))
						foreach ($roles_bss as $role_bss)
							if ($role_conjugations = Arrays::pop($conjugations, $role_bss))
								foreach ($role_conjugations as $id_external=>$role_conjugation){
									$params_fix['accounts'][$role_bss][] = $id_external;
									$result = Yii::app()->$component->query('account_info', array('id'=>$id_external));
									
									if (!$result)
										throw new CHttpException(500, 'empty result');
									
									$params_fix['requests'][$role_bss][$id_external] = $result;
									$this->_accounts[$role_bss][$id_external] = array_merge(array('id'=>$id_external,
										'id_conjugation'=>$role_conjugation->id), Arrays::filterKey($result, 'error', false));
									
									if (!empty($this->_accounts[$role_bss][$id_external]['services']))
										foreach ($this->_accounts[$role_bss][$id_external]['services'] as $key=>$service_info){
											$service_info['id'] = $id_service++;
											
											if (!isset($service_info['costs'])){
												$service_info['costs'] = array(array('date'=>'2017-01-01', 'cost'=>Arrays::pop($service_info, 'cost')));
												unset($service_info['cost']);
											}
											
											$this->_accounts[$role_bss][$id_external]['services'][$key] = $service_info;
										}
								}
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('account_list', $params_fix, $ok ? 'ok' :
				(!empty($conjugations) && !$params_fix['accounts'] ? 'application' :
				($params_fix['accounts'] && empty($result) ? 'agent_request' : 'account_list')));
		}
		
		return $this->_accounts;
	}
	
	/**
	 * Метод-геттер для определения _counters
	 * @return array Ассоциативный массив с информацией о доступных счётчиках пользователя
	 */
	public function getCounters(){
		if ($this->_counters === null){
			$this->_counters = array();
			$params_fix = array('accounts'=>array());
			
			try{
				$conjugations = $this->getModel()->conjugations;
				$roles_external = static::ROLES_EXTERNAL;
				
				foreach (array('bss1') as $component)
					if ($roles_bss = Arrays::pop($roles_external, $component))
						foreach ($roles_bss as $role_bss)
							if ($role_conjugations = Arrays::pop($conjugations, $role_bss))
								foreach ($role_conjugations as $id_external=>$role_conjugation){
									$params_fix['accounts'][$role_bss][] = $id_external;
									$result = Yii::app()->$component->query('counter_list', array('id'=>$id_external));
									
									if (!$result)
										throw new CHttpException(500, 'empty result');
									
									$params_fix['requests'][$role_bss][$id_external] = $result;
									
									if ($counters = Arrays::pop($result, 'counters'))
										foreach ($counters as $counter)
											$this->_counters[$role_bss][$id_external][$counter['id']] = $counter;
								}
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('counter_list', $params_fix, $ok ? 'ok' :
				(!empty($conjugations) && !$params_fix['accounts'] ? 'application' :
				($params_fix['accounts'] && empty($result) ? 'agent_request' : 'counter_list')));
		}
		
		return $this->_counters;
	}
	
	/**
	 * Метод-геттер для определения _subsidies
	 * @return array Ассоциативный массив с информацией о доступных субсидиях пользователя
	 */
	/* public function getSubsidies(){
		if ($this->_subsidies === null){
			$this->_subsidies = array();
			$params_fix = array('accounts'=>array());
			
			try{
				$conjugations = $this->getModel()->conjugations;
				$roles_external = static::ROLES_EXTERNAL;
				
				foreach (array_keys($roles_external) as $component)
					if ($roles_bss = Arrays::pop($roles_external, $component))
						foreach ($roles_bss as $role_bss)
							if ($role_conjugations = Arrays::pop($conjugations, $role_bss))
								foreach ($role_conjugations as $id_external=>$role_conjugation){
									$params_fix['accounts'][$role_bss][] = $id_external;
									$result = Yii::app()->$component->query('subsidy_list', array('id'=>$id_external));
									
									if (!$result)
										throw new CHttpException(500, 'empty result');
									
									$params_fix['requests'][$role_bss][$id_external] = $result;
									
									if ($subsidies = Arrays::pop($result, 'subsidies'))
										foreach ($subsidies as $subsidy)
											$this->_subsidies[$role_bss][$id_external][$subsidy['id']] = $subsidy;
								}
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('subsidy_list', $params_fix, $ok ? 'ok' :
				(!empty($conjugations) && !$params_fix['accounts'] ? 'application' :
				($params_fix['accounts'] && empty($result) ? 'agent_request' : 'subsidy_list')));
		}
		
		return $this->_subsidies;
	} */
	
	/**
	 * Метод-геттер для определения _compensations
	 * @return array Ассоциативный массив с информацией о доступных компенсациях пользователя
	 */
	/* public function getCompensations(){
		if ($this->_compensations === null){
			$this->_compensations = array();
			$params_fix = array('accounts'=>array());
			
			try{
				$conjugations = $this->getModel()->conjugations;
				$roles_external = static::ROLES_EXTERNAL;
				
				foreach (array_keys($roles_external) as $component)
					if ($roles_bss = Arrays::pop($roles_external, $component))
						foreach ($roles_bss as $role_bss)
							if ($role_conjugations = Arrays::pop($conjugations, $role_bss))
								foreach ($role_conjugations as $id_external=>$role_conjugation){
									$params_fix['accounts'][$role_bss][] = $id_external;
									$result = Yii::app()->$component->query('compensation_list', array('id'=>$id_external));
									
									if (!$result)
										throw new CHttpException(500, 'empty result');
									
									$params_fix['requests'][$role_bss][$id_external] = $result;
									
									if ($compensations = Arrays::pop($result, 'compensations'))
										foreach ($compensations as $compensation)
											$this->_compensations[$role_bss][$id_external][$compensation['id']] = $compensation;
								}
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('compensation_list', $params_fix, $ok ? 'ok' :
				(!empty($conjugations) && !$params_fix['accounts'] ? 'application' :
				($params_fix['accounts'] && empty($result) ? 'agent_request' : 'compensation_list')));
		}
		
		return $this->_compensations;
	} */
	
	/**
	 * Метод-геттер для определения _rolesComponent
	 * @return array Ассоциативный массив соответствий ролей компонентам приложения
	 */
	public function getRolesComponent(){
		if ($this->_rolesComponent === null){
			$this->_rolesComponent = array();
			
			foreach (static::ROLES_EXTERNAL as $component=>$roles_external)
				foreach ($roles_external as $role_external)
					$this->_rolesComponent[$role_external] = $component;
		}
		
		return $this->_rolesComponent;
	}
	
	/**
	 * Получение ссылки на домашнюю страницу пользователя
	 * @param integer $id Идентификатор пользователя или false. По умолчанию равно false. Если равно false, вернётся результат для
	 * текущего пользователя
	 * @return boolean|string Ссылка или false в случае ошибки
	 */
	public function getHomeUrl($id = false){
		if (!$id)
			$id = $this->id;
		
		$url = Yii::app()->defaultController;
		
		if ($return_url = Arrays::pop($cookies = Yii::app()->request->cookies->cookies, 'return_url'))
			$return_url = json_decode($return_url, true);
		
		if ($id)
			if ($return_url && is_array($return_url) && count($return_url) == 2){
				if (Yii::app()->user->checkAccess(reset($return_url), array('id_user'=>$id)))
					$url = Yii::app()->controller->createUrl(reset($return_url), end($return_url));
				
				Yii::app()->request->cookies->remove('return_url');
			}else{
				if ($this->checkAccessSet('developer, administrator', false, array('id_user'=>$id)))
					$url_new = 'user/index';
				elseif ($this->checkAccessSet('specialist, clerk', false, array('id_user'=>$id)))
					$url_new = 'appeal/index';
				elseif ($this->checkAccess('redactor', array('id_user'=>$id)))
					$url_new = 'news/index';
				elseif ($this->checkAccessSet('bss1_payer, bss2_payer', false, array('id_user'=>$id)))
					$url_new = 'account/index';
				
				if (isset($url_new) && Yii::app()->user->checkAccess($url_new, array('id_user'=>$id)))
					$url = $url_new;
			}
		
		$url = Yii::app()->controller->createAbsoluteUrl($url);
		
		if ($id && Html::secureCheck())
			$url = Html::changeSecureUrl($url);
		
		return $url;
	}
	
	/**
	 * Получение СНИЛС текущего пользователя
	 * @return string СНИЛС
	 */
	/* public function getSnils(){
		if (!$this->id || (!$model = $this->getModel()))
			return false;
		
		if ($model->snils)
			return $model->snils;
		
		if ($model->snils === '' || !Yii::app()->bss2->on)
			return false;
		
		if (!$this->getAuthManager()->checkAccess('citizen', $model->id))
			return false;
		
		$result = Yii::app()->bss2->query('user_identification', array('id'=>$model->id, 'subject_law'=>1));
		
		if (!$result || !isset($result['snils']))
			return false;
		
		$model->snils = $result['snils'] ? $result['snils'] : '';
		
		$model->save(false);
		
		return $result['snils'] ? $result['snils'] : false;
	} */
	
	/**
	 * Переопределение стандартного метода afterLogout, осуществляющего некоторые действия после разавторизации пользователя
	 * @see CWebUser::afterLogout()
	 */
	protected function afterLogout(){
		Yii::app()->getSession()->open();
	}
	
	/**
	 * Подготовка пароля для пользователя
	 * @param string $login Логин пользователя
	 * @return string Зашифрованный хэш пароля
	 */
	protected function preparationPassword($login){
		$password = $login === 'developer' ? static::DEVELOPER_PASSWORD : static::PREFIX_PASSWORD.mb_strtoupper($login);
		
		return Hashing::encode($password, Hashing::key($login));
	}
	
	/**
	 * Подготовка данных для генерации аккаунтов пользователей
	 * @return array|boolean Ассоциативный массив данных или false
	 */
	protected function preparationGenerationData(){
		$path = Yii::getPathOfAlias(static::DIR_CFG).'/'.static::FN_GENERATION;
		
		if (!is_file($path))
			return false;
		
		$data = require $path;
		
		if (empty($data['login']) || empty($data['name']) || empty($data['surname']) || empty($data['patronymic']))
			return false;
		
		$data['qu_login'] = count($data['login']) - 1;
		$data['qu_name'] = count($data['name']) - 1;
		$data['qu_surname'] = count($data['surname']) - 1;
		$data['qu_patronymic'] = count($data['patronymic']) - 1;
		
		return $data;
	}
	
}
