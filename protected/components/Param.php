<?php
/**
 * Компонент для работы с параметрами приложения
 * @author v.zubov
 */
class Param extends Component{
	
	/**
	 * @var string Псевдоним пути к файлу параметров
	 */
	const DIR_CFG = 'application.config';
	
	/**
	 * @var string Псевдоним пути к файлу шаблона параметров
	 */
	const DIR_PATTERN = 'application.views.system';
	
	/**
	 * @var string Имя файла параметров
	 */
	const FN_CFG = 'param.php';
	
	/**
	 * @var string Прификс заблокированного параметра. Применяется для параметра "data['cache']['cache_type']"
	 */
	const PREFIX_LOCKED_PARAM = '_';
	
	/**
	 * @var array Ассоциативный массив параметров приложения
	 */
	private $_data;
	
	/**
	 * @var array Ассоциативный массив стартовой конфигурации компонентов приложения
	 */
	private $_componentsConfig;
	
	/**
	 * Конструктор компонента
	 */
	public function init(){
		$this->_componentsConfig = Yii::app()->getComponents(false);
		$this->_data = array(
			'db'=>DbConnection::PARAMS_DEF,
			'developer'=>Html::IP_LOCALHOST,
			'cache'=>Cache::PARAMS_DEF
		);
		$path = Yii::getPathOfAlias(self::DIR_CFG).'/'.self::FN_CFG;
		
		if (is_file($path) && is_array($data = require $path))
			$this->_data = array_merge($this->_data, $data);
		else
			try{
				$this->saveCfg();
			}catch (CException $e){
				throw new CHttpException(500);
			}
		
		Cache::init($this->_data['cache']);
	}
	
	/**
	 * Выборка и получение данных с помощью функции Arrays::getSelective()
	 * @param [mixed] Ключ для фильтрации очередного уровня массива справочных данных или false
	 * @return array|boolean Выбранные данные или false
	 * @see Arrays::getSelective()
	 */
	public function get(){
		return Arrays::getSelective($this->_data, func_get_args());
	}
	
	/**
	 * Установка блока параметров
	 * @param string $section Название блока
	 * @param mixed $cfg Параметры блока
	 * @return boolean Успешность выполнения
	 */
	public function set($section, $cfg){
		$params_fix = array('section'=>$section, 'cfg'=>$cfg);
		
		try{
			if (!isset($this->_data[$section]) || gettype($this->_data[$section]) != gettype($cfg) ||
				(is_array($cfg) && array_keys($this->_data[$section]) != array_keys($cfg)))
					throw new CHttpException(500, 'invalid cfg');
			
			$this->_data[$section] = $cfg;
			
			if (!$params_fix['path'] = $this->saveCfg())
				throw new CHttpException(500, 'save error');
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('cfg_param_set', $params_fix, $ok ? 'ok' : (Arrays::pop($params_fix, 'msg') == 'write error' ?
			'file_create' : 'cfg_param_set'));
		
		return $ok;
	}
	
	/**
	 * Метод-геттер для определения _componentsConfig
	 * @return array Ассоциативный массив стартовой конфигурации компонентов приложения
	 */
	public function getComponentsConfig(){
		return $this->_componentsConfig;
	}
	
	/**
	 * Создание или изменение параметров приложения в файле
	 * @return string Путь к файлу параметров приложения
	 * @throws CHttpException
	 */
	protected function saveCfg(){
		extract($cfg = $this->_data);
		$path = Yii::getPathOfAlias(self::DIR_CFG).'/'.self::FN_CFG;
		$path_pattern = Yii::getPathOfAlias(self::DIR_PATTERN).'/'.self::FN_CFG;
		
		if (!is_file($path_pattern))
			throw new CHttpException(500, 'pattern not found');
		
		ob_start();
			require($path_pattern);
		$content = ob_get_clean();
		
		if (!$content)
			throw new CHttpException(500, 'empty data');
		
		if (!file_put_contents($path, $content))
			throw new CHttpException(500, 'write error');
		
		return $path;
	}
	
}
