<?php
/**
 * Компонент организации работы меню графического интерфейса приложения
 * @author v.zubov
 */
class Scheme extends ComponentDeploy{
	
	/**
	 * @var string Псевдоним пути к файлу конфигурации
	 */
	const DIR_CFG = 'application.config.default';
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'scheme.php';
	
	/**
	 * @var string Псевдоним пути к каталогу представлений
	 */
	const DIR_VIEW = 'application.views.scheme';
	
	/**
	 * @var string Шаблон имён файлов представлений
	 */
	const FN_VIEW = 'scheme_%s.php';
	
	/**
	 * @var integer Максимальное число уровней вложенности меню
	 */
	const MAX_NUMBER_NESTED = 5;
	
	/**
	 * @var string Шаблон маршрута приложения
	 */
	const PATTERN_ROUTE = '/^(\/?[A-Za-z*\d-]+\/?)+$/u';
	
	/**
	 * @var string Шаблон поиска идентификаторов файлов в HTML-коде статичных страниц
	 */
	const PATTERN_FILE = '/\$file(\d+)\$/u';
	
	/**
	 * @var array Ассоциативный массив кодов псевдонимов, используемых в PHP-выражениях параметров
	 * схемы ("route", "enabled", 'visible") 
	 */
	const ALIAS_CODES = array(
		'{tw}'=>'Yii::app()->controller->technicalWork',
		'{d}'=>'Yii::app()->controller->deployed',
		'{w}'=>'Yii::app()->controller->working',
		'{pa}'=>'Yii::app()->controller->privateArea',
		'{st}'=>'Yii::app()->controller->state',
		'{g}'=>'Yii::app()->user->isGuest',
		'{a}'=>'Yii::app()->request->isAjaxRequest',
		'{c}'=>'Yii::app()->controller',
		'{s}'=>'Yii::app()->setting',
		'{u}'=>'Yii::app()->user',
		'{r}'=>'$this->_route === $item["_route"]'
	);
	
	/**
	 * @var string Шаблон, используемый для проверки наличия в PHP-выражении параметров схемы условий, проверяющих статус
	 * приложения
	 */
	const PATTERN_STATE_CHECK = '/(ler->technicalWork|ler->deployed|ler->working|ler->privateArea|ler->state)/u';
	
	/**
	 * @var string Шаблон, используемый для поиска в PHP-выражениях схемы обозначений фиксированных параметров
	 */
	const PATTERN_FIX_PARAM = '/!fp{(\w+)}/u';
	
	/**
	 * @var array Список маршрутов с обязательной компрессией
	 */
	const	COMPRESSION_ROUTES = array('user/index');
	
	/**
	 * @var integer Код типа стэка меню: "Отключен", "Верхний" или "Нижний"
	 */
	public $stackType;
	
	/**
	 * @var integer Число уровней вложенности бокового меню
	 */
	public $numberNested;
	
	/**
	 * @var boolean Признак отображения заголовка раздела
	 */
	public $titleShow;
	
	/**
	 * @var boolean Признак компрессии меню. Если установлен, то основной контент страницы будет максимально прижат
	 * к боковому меню
	 */
	public $compression;
	
	/**
	 * @var array Ассоциативный массив связей элементов схемы в формате
	 * 	{Идентификатор элемента}=>{Идентификатор родительского элемента}
	 */
	protected $_schemeItemChild = array();
	
	/**
	 * @var array Ассоциативный массив с информацией об элементах схемы
	 */
	protected $_schemeItem = array();
	
	/**
	 * @var array Массив HTML-кода статичных страниц
	 */
	protected $_schemeItemStatic = array();
	
	/**
	 * @var array Массив путей элементов схемы. Представляет из себя ассоциативный массив в формате
	 * 	{Путь раздела}=>{Идентификатор элемента схемы}
	 */
	protected $_routes = array();
	
	/**
	 * @var string Маршрут текущего раздела
	 */
	protected $_route;
	
	/**
	 * @var array Массив узлов связей элементов меню в формате
	 * 	array(
	 * 		{Идентификатор родительского элемента}=>array(
	 * 			{Идентификатор элемента},
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	protected $_idChilds;
	
	/**
	 * @var array Навигационный путь
	 */
	protected $_navigator = array();
	
	/**
	 * @var integer Количество доступных уровней меню
	 */
	protected $_levels;
	
	/**
	 * @var boolean Признак доступности выбранного раздела меню
	 */
	protected $_enabled;
	
	/**
	 * @var array Ассоциативный массив параметров активного элемента меню
	 */
	protected $_activeItem;
	
	/**
	 * @var integer Идентификатор активного элемента меню
	 */
	protected $_activeId;
	
	/**
	 * @var array Массив с информацией о разделении уровней меню на блоки
	 */
	protected $_blocks;
	
	/**
	 * @var boolean Признак смещения основного контента страницы
	 */
	protected $_contentOffset;
	
	/**
	 * @var array Массив с HTML-кодом блоков меню
	 */
	protected $_renders;
	
	/**
	 * @var array Список идентификаторов элементов ветки схемы, предназначенной для развёртывания приложения
	 */
	protected $_deployItems;
	
	/**
	 * @var array Список кодов псевдонимов, используемых в PHP-выражениях
	 */
	protected $_aliasCodes;
	
	/**
	 * @var array Список расшифровок псевдонимов, используемых в PHP-выражениях
	 */
	protected $_aliasTexts;
	
	/**
	 * Конструктор компонента
	 */
	public function init(){
		if (Yii::app()->controller->schemeMode == Controller::SCHEME_MODE_DISABLED)
			return;
		
		$params = array('def'=>$def = Yii::app()->controller->componentDef || !Yii::app()->controller->dbActive);
		
		try{
			if ($def)
				$this->preparationDataDef(false, null, Yii::app()->conformity->get('scheme_item_state', 'code', 'name'));
			else
				$this->preparationData(Yii::app()->conformity->get('scheme_item_state', 'name', 'code'));
		}catch (CException $e){
			$params['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e) && $this->_schemeItemChild && $this->_schemeItem && $this->_routes;
		
		if (Yii::app()->controller->schemeMode == Controller::SCHEME_MODE_NORMAL)
			Yii::app()->log->fix('scheme_init', $params, $ok ? 'ok' : 'scheme_init');
		
		$this->_idChilds = array();
		
		foreach ($this->_schemeItemChild as $id_item=>$id_parent)
			$this->_idChilds[(int)$id_parent][] = $id_item;
		
		Yii::app()->setting->get('gui', $this, true);
		
		$this->navigation();
		
		if (in_array($this->_route, static::COMPRESSION_ROUTES))
			$this->compression = true;
		
		if (!Yii::app()->request->isAjaxRequest){
			$this->setBlocks();
			
			if (in_array(Yii::app()->controller->schemeMode, array(Controller::SCHEME_MODE_SAFE, Controller::SCHEME_MODE_NORMAL)))
				$this->preparationRender();
		}
	}
	
	/**
	 * Метод-геттер для определения _route
	 * @return string Маршрут текущего раздела
	 */
	public function getRoute(){
		return $this->_route;
	}
	
	/**
	 * Метод-геттер для определения _contentOffset
	 * @return boolean|null Признак смещения контента или null
	 */
	public function getContentOffset(){
		return $this->_contentOffset;
	}
	
	/**
	 * Метод-геттер для определения _activeItem
	 * @return array|null Ассоциативный массив параметров активного элемента меню или null
	 */
	public function getActiveItem(){
		return $this->_activeItem;
	}
	
	/**
	 * Выдача HTML-кода заданного блока меню
	 * @param string $type Название блока меню
	 * @param boolean $return Признак возвращения результата рендеринга в переменную. По умолчанию равно false
	 * @return boolean|string HTML-код или false
	 */
	public function render($type, $return = false){
		if (empty($this->_renders[$type]))
			return false;
		elseif ($return)
			return $this->_renders[$type];
		else
			echo $this->_renders[$type];
	}
	
	/**
	 * Развёртывание
	 * @param string $section Название группы таблиц
	 * @param string $type Символьный идентификатор типа развёртывания
	 * @param array $params Ассоциативный массив с параметрами развёртывания приложения
	 * @throws CHttpException
	 */
	public function deployCompute($section, $type, $params){
		parent::deployCompute($section, $type, $params);
		
		if ($type === 'standart'){
			if ($this->_schemeItemStatic){
				$rows = array(array('id', 'html'));
				
				foreach ($this->_schemeItemStatic as $id=>$html)
					$rows[] = array($id, $html);
				
				if (!Yii::app()->db->createCommand()->insert('scheme_static', $rows))
					throw new CHttpException(500);
			}
			
			if ($this->_schemeItem){
				$rows = array(array('id', 'id_static', 'route', 'title', 'title_active', 'state', 'enabled', 'visible'));
				
				foreach ($this->_schemeItem as $features)
					$rows[] = array($features['id'], $features['id_static'],
						str_replace($this->aliasTexts, $this->aliasCodes, $features['route']),
						$features['title'], $features['title_active'], $features['state_code'],
						str_replace($this->aliasTexts, $this->aliasCodes, $features['enabled']),
						str_replace($this->aliasTexts, $this->aliasCodes, $features['visible']));
				
				if (!Yii::app()->db->createCommand()->insert('scheme_item', $rows))
					throw new CHttpException(500);
			}
			
			if ($this->_schemeItemChild){
				$rows = array(array('id_item', 'id_parent'));
				
				foreach ($this->_schemeItemChild as $id_item=>$id_parent)
					$rows[] = array($id_item, $id_parent);
				
				if (!Yii::app()->db->createCommand()->insert('scheme_item_child', $rows))
					throw new CHttpException(500);
			}
			
		}
	}
	
	/**
	 * Выборка и получение данных из массива _schemeItem
	 * @param boolean|integer|string $id Идентификатор, маршрут элемента или false. По умолчанию равно false. Если
	 * равно false, то вернётся массив из всех элементов
	 * @param boolean|string $feature Название необходимой характеристики элемента или false. По умолчанию равно false
	 * @return mixed Выбранные данные или false
	 */
	public function getSchemeItem($id = false, $feature = false){
		if (!$items = $this->_schemeItem)
			return false;
		
		if (!$id)
			return $items;
		
		if (is_numeric($id))
			$item = isset($items[$id]) ? $items[$id] : false;
		else
			$item = isset($this->_routes[$id], $items[$this->_routes[$id]]) ? $items[$this->_routes[$id]] : false;
		
		if ($item && $feature)
			return isset($item[$feature]) ? $item[$feature] : false;
		
		return $item;
	}
	
	/**
	 * Метод-геттер для получения _navigator
	 */
	public function getNavigator(){
		return $this->_navigator;
	}
	
	/**
	 * Метод-геттер для определения _aliasCodes
	 * @return array Список кодов псевдонимов
	 */
	public function getAliasCodes(){
		if ($this->_aliasCodes === null)
			$this->_aliasCodes = array_keys(self::ALIAS_CODES);
		
		return $this->_aliasCodes;
	}
	
	/**
	 * Метод-геттер для определения _aliasTexts
	 * @return array Список расшифровок псевдонимов
	 */
	public function getAliasTexts(){
		if ($this->_aliasTexts === null)
			$this->_aliasTexts = array_values(self::ALIAS_CODES);
		
		return $this->_aliasTexts;
	}
	
	/**
	 * Поуровневая рекурсивная обработка данных структуры схемы из конфигурационного файла
	 * @param array|boolean $scheme Многоуровневый массив данных очередного уровня меню или false. По умолчанию
	 * равно false. Если равно false, данные будут прочитаны из конфигурационного файла.
	 * @param integer|null $id_parent Идентификатор родительского элемента или null. По умолчанию равно null
	 * @param array $states Ассоциативный массив статусов элементов схемы в формате
	 * 	{Символьный идентификатор статуса}=>{Цифровой идентификатор статуса}
	 */
	protected function preparationDataDef($scheme = false, $id_parent = null, $states = array()){
		if ($scheme === false){
			$path = Yii::getPathOfAlias(self::DIR_CFG).'/'.self::FN_CFG;
			
			if (is_file($path))
				$scheme = require $path;
		}
		
		if (!$scheme)
			return;
		
		foreach ($scheme as $section)
			if (isset($states[$state = isset($section['state']) ? $section['state'] : 'working'])){
				$id = count($this->_schemeItem) + 1;
				$id_static = count($this->_schemeItemStatic) + 1;
				
				$item = array(
					'id'=>$id,
					'id_static'=>$static = isset($section['static']) ? $id_static : null,
					'route'=>isset($section['route']) ? $section['route'] : null,
					'title'=>$section['title'],
					'title_active'=>isset($section['title_active']) ? $section['title_active'] : null,
					'state'=>$state,
					'enabled'=>isset($section['enabled']) ? $section['enabled'] : null,
					'visible'=>isset($section['visible']) ? $section['visible'] : null,
					'state_code'=>$states[$state]
				);
				
				if ($route = $this->preparationRoute($item))
					$this->_routes[$route] = $id;
				
				$this->_schemeItem[$id] = $item;
				$this->_schemeItemChild[$id] = $id_parent;
				
				if ($static)
					$this->_schemeItemStatic[$id_static] = $section['static'];
				
				if (!empty($section['childs']))
					$this->preparationDataDef($section['childs'], $id, $states);
			}
	}
	
	/**
	 * Подготовка конфигурации схемы из БД
	 * @param array $states Ассоциативный массив статусов элементов схемы в формате
	 * 	{Цифровой идентификатор статуса}=>{Символьный идентификатор статуса}
	 */
	protected function preparationData($states = array()){
		if (!$this->_schemeItem = SchemeItemModel::model()->with('item', 'static')->findAllSelective('id', false, false,
			array('order'=>'t.id')))
				return;
		
		foreach ($this->_schemeItem as $id=>&$item)
			if (isset($states[$item->state])){
				$this->_schemeItemChild[$id] = $item->item ? $item->item->id_parent : null;
				
				if ($item->static)
					$this->_schemeItemStatic[$item->static->id] = $item->static->html;
				
				$item = $item->attributes;
				$item['state_code'] = $item['state'];
				$item['state'] = $states[$item['state']];
				
				if ($route = $this->preparationRoute($item))
					$this->_routes[$route] = $id;
			}else
				unset($this->_schemeItem[$id]);
		unset($item);
	}
	
	/**
	 * Метод-геттер для определения _deployItems
	 * @return array Список идентификаторов элементов ветки схемы, предназначенной для развёртывания приложения
	 */
	protected function getDeployItems(){
		if ($this->_deployItems === null){
			$this->_deployItems = array();
			
			if (($id = Arrays::pop($this->_routes, 'deploy/db')) && ($id_parent = Arrays::pop($this->_schemeItemChild, $id))){
				if (isset($this->_idChilds[$id_parent]))
					$this->_deployItems = $this->_idChilds[$id_parent];
				
				$this->_deployItems[] = $id_parent;
				
				while ($id_parent = Arrays::pop($this->_schemeItemChild, $id_parent))
					$this->_deployItems[] = $id_parent;
			}
		}
		
		return $this->_deployItems;
	}
	
	/**
	 * Построение навигационного пути, расчёт доступности элементов схемы
	 */
	protected function navigation(){
		$params_action = Yii::app()->controller->actionParams;
		$route = array_slice(explode('/', Yii::app()->controller->route), 0, 2);
		
		$this->_route = '/'.$route[1].(isset($params_action['static']) ? '/'.$params_action['static'] : '');
		$route_alt = '*'.$this->_route;
		$this->_route = $route[0].$this->_route;
		
		if (!isset($this->_routes[$this->_route]) && isset($this->_routes[$route_alt]))
			$this->_route = $route_alt;
		
		if ($found = isset($this->_routes[$this->_route]))
			$this->navigationFull();
		else
			$this->navigationRoot();
		
		$this->calculationItems();
		
		if (Yii::app()->controller->schemeMode == Controller::SCHEME_MODE_NORMAL){
			$params_fix = array('route'=>$this->_route, 'params'=>$params_action);
			
			Yii::app()->log->fix('scheme_navigation', array_merge($params_fix, array('routes'=>$this->_routes)),
				$found ? 'ok' : 'scheme_navigation');
			
			if (Yii::app()->user->isGuest && !in_array($this->_route,
				array(Yii::app()->defaultController, '*/login', 'main/confirm', 'main/secure', 'esia/login')))
					if ($this->_enabled)
						Yii::app()->request->cookies->remove('return_url');
					else
						Yii::app()->request->cookies->add('return_url', new CHttpCookie('return_url', json_encode($params_fix)));
			
			Yii::app()->log->fix('scheme_check_access', array_merge($params_fix, array('active_item'=>$this->_activeItem)),
				$this->_enabled ? 'ok' : 'scheme_access');
		}
	}
	
	/**
	 * Построение полного навигационного пути
	 */
	protected function navigationFull(){
		$id_item_active = $this->_activeId = $this->_routes[$this->_route];
		$this->_activeItem = &$this->_schemeItem[$id_item_active];
		
		while ($id_item_active){
			$id_parent = (int)$this->_schemeItemChild[$id_item_active];
			$level = array();
			
			foreach ($this->_idChilds[$id_parent] as $key=>$id_item)
				$level[$id_item === $id_item_active ? 'active' : $key] = &$this->_schemeItem[$id_item];
			
			array_unshift($this->_navigator, $level);
			$id_item_active = $id_parent;
		}
	}
	
	/**
	 * Построение неполного (только первый уровень) навигационного пути
	 */
	protected function navigationRoot(){
		$level = array();
		
		if (!empty($this->_idChilds[0]))
			foreach ($this->_idChilds[0] as $id_item)
				$level[] = &$this->_schemeItem[$id_item];
		
		$this->_navigator[] = $level;
	}
	
	/**
	 * Расчёт количества доступных уровней меню, видимости, доступности и ссылок каждого элемента этих уровней
	 */
	protected function calculationItems(){
		$levels = 0;
		$enabled = false;
		
		foreach ($this->_navigator as $key_level=>&$level){
			$levels++;
			
			foreach ($level as $key_item=>&$item){
				$this->parsingFixParams($item);
				$item['_link'] = $this->createUrl($item);
				$enabled_item = $this->itemEnabled($item);
				
				if ($key_item === 'active')
					$enabled = $enabled_item;
			}
			unset($item);
			
			if (!$enabled)
				break;
		}
		unset($level);
		
		$this->_levels = $levels;
		$this->_enabled = $enabled;
	}
	
	/**
	 * Поиск и замена в PHP-выражениях элементов схемы обозначений фиксированных параметров. При наличии таких параметров
	 * дополняются условия visible и enabled выражениями, проверяющими наличие одноимённых параметра в GET-массиве. А также
	 * найденные значения записываются в элемент _fix_params массива параметров элемента схемы, который затем будет участвовать
	 * в формировании ссылки для элемента
	 * @param array $item Ассоциативный массив параметров элемента схемы
	 */
	protected function parsingFixParams(&$item){
		foreach (array('visible', 'enabled') as $name)
			if ($item[$name]){
				preg_match_all(self::PATTERN_FIX_PARAM, $item[$name], $matches, PREG_SET_ORDER);
				
				if ($matches)
					foreach ($matches as $match){
						$item[$name] = str_replace($match[0], 'isset($_GET["'.$match[1].'"])', $item[$name]);
						$item['_fix_params'][] = $match[1];
					}
			}
	}
	
	/**
	 * Расчёт видимости и доступности элемента схемы
	 * @param array $item Ассоциативный массив параметров элемента схемы
	 * @return boolean Признак доступности элемента схемы
	 */
	protected function itemEnabled(&$item){
		if (!isset($item['_enabled'])){
			$item['_visible'] = $item['state'] != 'hidden';
			
			if ($item['_visible']){
				$expression = $item['visible'] ? str_replace($this->aliasCodes, $this->aliasTexts, $item['visible']) : '';
				
				if (!in_array($item['id'], $this->deployItems) && (!$expression || !preg_match(self::PATTERN_STATE_CHECK, $expression)))
					$expression = 'Yii::app()->controller->deployed'.($expression ? ' && '.$expression : '');
				
				$item['_visible'] = $expression ? (bool)eval("return $expression;") : null;
				
				if (!$item['visible'] && $item['_visible'])
					$item['_visible'] = null;
			}
			
			$item['_enabled'] = ($item['_visible'] || is_null($item['_visible'])) && $item['state'] == 'working';
			
			if ($item['_enabled']){
				$routing = isset($this->_routes[$item['_route']]);
				$expression = $item['enabled'] ? str_replace($this->aliasCodes, $this->aliasTexts, $item['enabled']) : false;
				
				if ($routing || $expression){
					if ($routing){
						$item['_enabled'] = $item['_route'] == 'user/logout' && !Yii::app()->user->model ? true :
							Yii::app()->user->checkAccess($item['_route']);
						
						if (is_null($item['_visible']))
							$item['_visible'] = $item['_enabled'];
					}
					
					if ($item['_enabled'] && $expression)
						$item['_enabled'] = (bool)eval("return $expression;");
				}else
					$item['_enabled'] = !empty($item['_link']) ? $item['_visible'] || is_null($item['_visible']) : null;
			}
			
			if (is_bool($item['_enabled'])){
				if (is_null($item['_visible']))
					$item['_visible'] = true;
			}else
				$this->itemEnabledChilds($item);
		}
		
		return $item['_enabled'];
	}
	
	/**
	 * Расчёт видимости и доступности элемента схемы, исходя из доступности дочерних элементов
	 * @param array $item Ассоциативный массив параметров элемента схемы
	 */
	protected function itemEnabledChilds(&$item){
		if (isset($this->_idChilds[$item['id']]))
			foreach ($this->_idChilds[$item['id']] as $id_child)
				if ($this->itemEnabled($this->_schemeItem[$id_child])){
					$childs_enabled = true;
					break;
				}
		
		$item['_enabled'] = isset($childs_enabled);
		
		if (!$item['_visible'])
			$item['_visible'] = $item['_enabled'];
	}
	
	/**
	 * Подготовка ссылки для элемента меню. Если для заданного элемента ссылку сгенерировать не получается,
	 * будет произведён поиск подходящего дочернего элемента
	 * @param array $item Ассоциативный массив параметров элемента меню
	 * @return boolean|string Ссылка или false
	 */
	protected function createUrl($item){
		while ($item)
			if (!empty($item['_link']))
				return $item['_link'];
			elseif ($item['_route']){
				$route = array_slice(explode('/', $item['_route']), 0, 2);
				$params = array();
				
				if ($route[0] === '*')
					$route[0] = Yii::app()->controller->id;
				
				if ($item['id_static'])
					$params['static'] = $item['id_static'];
				
				if (!empty($item['_fix_params'])){
					$action_params = Yii::app()->controller->actionParams;
					
					foreach ($item['_fix_params'] as $name)
						if (isset($action_params[$name]))
							$params[$name] = $action_params[$name];
				}
				
				return Yii::app()->createUrl(implode('/', $route), $params);
			}else{
				$id_item = $item['id'];
				$item = false;
				
				if (isset($this->_idChilds[$id_item]))
					foreach ($this->_idChilds[$id_item] as $id_child){
						$item_child = &$this->_schemeItem[$id_child];
						
						if ($this->itemEnabled($item_child)){
							$item = $item_child;
							break;
						}
					}
			}
		
		return false;
	}
	
	/**
	 * Обработка ссылки из строки маршрута элемента схемы. Результат записывается в элемент с ключом _route. Если результат
	 * является внешней ссылкой, то он будет записан в элемент с ключом _link
	 * @param array $item Элемент схемы. Принимается по ссылке
	 * @return boolean|string Маршрут элемента или false
	 */
	protected function preparationRoute(&$item){
		if ($route = $item['route']){
			if (mb_strpos($route, '!') === 0)
				$route = eval('return '.str_replace($this->aliasCodes, $this->aliasTexts, mb_substr($route, 1)).';');
			
			if (preg_match(self::PATTERN_ROUTE, $route)){
				$route = array_slice(Strings::devisionWords($route, false, '/'), 0, 3);
				
				if (count($route) > 1 && $item['id_static'])
					$route[2] = $item['id_static'];
				
				$route = implode('/', $route);
			}else{
				$item['_link'] = Html::formingUrl($route);
				$route = false;
			}
		}
		
		return $item['_route'] = $route;
	}
	
	/**
	 * Разделение навигационного пути на блоки меню: "Главное", "Цепочка", "Боковое", "Стэк". Формирование
     * массива _blocks
	 */
	protected function setBlocks(){
		if ($levels = $this->_levels){
			$stack_upper = $this->stackType === 'upper';
			$stack_lower = $this->stackType === 'lower';
			
			$main = array(0);
			$levels--;
			
			if ($levels){
				$qu_levels = min($this->numberNested, $levels);
				if (!$stack_upper)
					$lateral = range(1, $qu_levels);
				else
					$lateral = range($levels - $qu_levels + 1, $levels);
				$levels -= $qu_levels;
			}else
				$lateral = false;
			
			$chain = $stack_upper && $levels ? range(1, $levels) : false;
			
			$stack = $stack_lower && $levels ? range($this->_levels - $levels, $this->_levels - 1) : false;
			
			$this->_blocks = array(
				'main'=>$main,
				'chain'=>$chain,
				'lateral'=>$lateral,
				'stack'=>$stack
			);
			
			$this->_contentOffset = !empty($lateral);
		}
	}
	
	/**
	 * Подготовка HTML-кода меню. Формирование массива _renders
	 */
	protected function preparationRender(){
		foreach (array('main', 'chain', 'lateral', 'stack', 'title') as $type)
			$this->_renders[$type] = $this->preparationRenderBlock($type);
	}
	
	/**
	 * Подготовка HTML-кода для блока меню заданного типа
	 * @param string $type Название блока меню
	 * @return string HTML-код
	 */
	protected function preparationRenderBlock($type){
		if ($type !== 'title'){
			if (empty($this->_blocks[$type]))
				return false;
		}else{
			$active_item = $this->_activeItem;
			if (!$active_item || !$this->titleShow)
				return false;
		}
		
		$path = Yii::getPathOfAlias(self::DIR_VIEW).'/'.sprintf(self::FN_VIEW, $type);
		
		if (is_file($path)){
			if ($type !== 'title'){
				$block = $this->_blocks[$type];
				$levels = array_slice($this->_navigator, $block[0], count($block));
				$last = $block[count($block) - 1] === $this->_levels - 1;
			}
			
			ob_start();
			require $path;
			return ob_get_clean();
		}else
			return false;
	}
	
}
