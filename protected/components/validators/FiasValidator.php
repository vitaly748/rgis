<?php
/**
 * Класс валидатора для проверки адреса с помощью ФИАС-агента
 * @author v.zubov
 * @see CValidator
 */
class FiasValidator extends CValidator{
	
	/**
	 * @var boolean Признак допустимости пустого значения. По умолчанию равно true
	 */
	public $allowEmpty = true;
	
	/**
	 * Переопределение стандартного метода validateAttribute, осуществляющего проверку значения атрибута
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @see CValidator::validateAttribute()
	 */
	protected function validateAttribute($object, $attribute){
		if ($this->allowEmpty && $this->isEmpty($object->address))
			return;
		
		if (Yii::app()->fias->on && !$object->addressid)
			$this->addError($object, $attribute, $this->message);
	}
	
	/**
	 * Переопределение стандартного метода clientValidateAttribute, генерирующего JavaScript-код проверки значения атрибута на
	 * стороне клиента 
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @return string JavaScript-код проверки значения атрибута
	 * @see CValidator::clientValidateAttribute()
	 */
	public function clientValidateAttribute($object, $attribute){
		if (!Yii::app()->fias->on)
			return;
		
		$message = CJSON::encode($this->message);
		
		$js = <<<JS
if (fiasParams && !fiasParams.validationSkip && fiasParams.inputId && !fiasParams.inputId.val())
	messages.push($message);
JS;
		
		if ($this->allowEmpty)
			$js = "if ($.trim(value) != ''){".PHP_EOL.$js.PHP_EOL.'}'.PHP_EOL;
		
		return Html::clientValidateAttribute($object, $attribute, $js);
	}
	
}
