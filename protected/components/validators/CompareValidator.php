<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CCompareValidator
 * @author v.zubov
 * @see CCompareValidator
 */
class CompareValidator extends CCompareValidator{
	
	/**
	 * Переопределение стандартного метода clientValidateAttribute, генерирующего JavaScript-код проверки значения атрибута на
	 * стороне клиента 
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @return string JavaScript-код проверки значения атрибута
	 * @see CCompareValidator::clientValidateAttribute()
	 */
	public function clientValidateAttribute($object, $attribute){
		return Html::clientValidateAttribute($object, $attribute, parent::clientValidateAttribute($object, $attribute));
	}
	
}
