<?php
/**
 * Класс валидатора для проверки существования хоста
 * @author v.zubov
 * @see CValidator
 */
class HostValidator extends CValidator{
	
	/**
	 * @var boolean Признак допустимости пустого значения. По умолчанию равно true
	 */
	public $allowEmpty = true;
	
	/**
	 * Переопределение стандартного метода validateAttribute, осуществляющего проверку значения атрибута
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @see CValidator::validateAttribute()
	 */
	protected function validateAttribute($object, $attribute){
		if ($this->allowEmpty && $this->isEmpty($object->$attribute))
			return;
		
		if (!Html::hostExists($object->$attribute))
			$this->addError($object, $attribute, $this->message);
	}
	
}
