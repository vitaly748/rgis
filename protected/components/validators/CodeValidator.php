<?php
/**
 * Класс валидатора для проверки правильности номера мобильного телефона
 * @author v.zubov
 * @see CValidator
 */
class CodeValidator extends CValidator{
	
	/**
	 * @var boolean Признак допустимости пустого значения. По умолчанию равно true
	 */
	public $allowEmpty = true;
	
	/**
	 * @var string Регулярное выражение мобильного телефона
	 */
	public $phonePattern = '/^[78]9\d{9}$/';
	
	/**
	 * @var string Строка декоративных символов строки с номером телефона
	 */
	public $decoration = '';
	
	/**
	 * @var string Текст сообщения об ошибке при одновременной блокировке кода
	 */
	public $messageDisabled;
	
	/**
	 * @var string Текст сообщения об ошибке при одновременном обновлении кода
	 */
	public $messageRefresh;
	
	/**
	 * Переопределение стандартного метода validateAttribute, осуществляющего проверку значения атрибута
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @see CValidator::validateAttribute()
	 */
	protected function validateAttribute($object, $attribute){
		$value = $object->$attribute;
		
		if ($this->allowEmpty && $this->isEmpty($value))
			return;
		
		if ($this->decoration)
			$value = preg_replace("/[{$this->decoration}]/u", '', $value);
		
		if (!preg_match($this->phonePattern.'u', $value))
			$this->addError($object, $attribute, Yii::app()->errorAttribute->get(false, false, 'phone'));
	}
	
	/**
	 * Переопределение стандартного метода clientValidateAttribute, генерирующего JavaScript-код проверки значения атрибута на
	 * стороне клиента 
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @return string JavaScript-код проверки значения атрибута
	 * @see CValidator::clientValidateAttribute()
	 */
	public function clientValidateAttribute($object, $attribute){
		$message = CJSON::encode(Yii::app()->errorAttribute->get(false, false, 'phone'));
		
		$js = "if (!{$this->phonePattern}.test(value))".PHP_EOL."messages.push($message);";
		
		if ($this->decoration)
			$js = "value = value.replace(/[{$this->decoration}]/g, '');".PHP_EOL.$js;
		
		if ($this->allowEmpty)
			$js = "if (jQuery.trim(value) != ''){".PHP_EOL.$js.PHP_EOL.'}'.PHP_EOL;
		
		return Html::clientValidateAttribute($object, $attribute, $js);
	}
	
}
