<?php
/**
 * Класс валидатора для проверки уникальности значения
 * @author v.zubov
 * @see CUniqueValidator
 */
class UniqueValidator extends CUniqueValidator{
	
	/**
	 * Переопределение стандартного метода validateAttribute, осуществляющего проверку значения атрибута
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @see CUniqueValidator::validateAttribute()
	 */
	protected function validateAttribute($object, $attribute){
		if ($value = $object->$attribute){
			$object->removeDecoration($attribute);
			
			parent::validateAttribute($object, $attribute);
			
			$object->$attribute = $value;
		}
	}
	
}
