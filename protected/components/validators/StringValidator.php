<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CStringValidator
 * @author v.zubov
 * @see CStringValidator
 */
class StringValidator extends CStringValidator{
	
	/**
	 * Переопределение стандартного метода clientValidateAttribute, генерирующего JavaScript-код проверки значения атрибута на
	 * стороне клиента 
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @return string JavaScript-код проверки значения атрибута
	 * @see CStringValidator::clientValidateAttribute()
	 */
	public function clientValidateAttribute($object, $attribute){
		return Html::clientValidateAttribute($object, $attribute, parent::clientValidateAttribute($object, $attribute));
	}
	
}
