<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CEmailValidator
 * @author v.zubov
 * @see CEmailValidator
 */
class EmailValidator extends CEmailValidator{
	
	/**
	 * Переопределение стандартного метода clientValidateAttribute, генерирующего JavaScript-код проверки значения атрибута на
	 * стороне клиента 
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @return string JavaScript-код проверки значения атрибута
	 * @see CEmailValidator::clientValidateAttribute()
	 */
	public function clientValidateAttribute($object, $attribute){
		return Html::clientValidateAttribute($object, $attribute, parent::clientValidateAttribute($object, $attribute));
	}
	
}
