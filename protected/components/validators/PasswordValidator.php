<?php
/**
 * Класс валидатора для проверки пароля текущего пользователя
 * @author v.zubov
 * @see CValidator
 */
class PasswordValidator extends CValidator{
	
	/**
	 * @var boolean Признак допустимости пустого значения. По умолчанию равно true
	 */
	public $allowEmpty = true;
	
	/**
	 * Переопределение стандартного метода validateAttribute, осуществляющего проверку значения атрибута
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @see CValidator::validateAttribute()
	 */
	protected function validateAttribute($object, $attribute){
		$value = $object->$attribute;
		
		if ($this->allowEmpty && $this->isEmpty($value))
			return;
		
		if (!Hashing::check($value, $object->passwordHash, Hashing::key($object->login)))
			$this->addError($object, $attribute, $this->message);
	}
	
}
