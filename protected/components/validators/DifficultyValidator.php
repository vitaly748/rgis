<?php
/**
 * Класс валидатора для проверки достаточной сложности значения атрибута
 * @author v.zubov
 * @see CValidator
 */
class DifficultyValidator extends CValidator{
	
	/**
	 * @var boolean Признак допустимости пустого значения. По умолчанию равно true
	 */
	public $allowEmpty = true;
	
	/**
	 * Переопределение стандартного метода validateAttribute, осуществляющего проверку значения атрибута
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @see CValidator::validateAttribute()
	 */
	protected function validateAttribute($object, $attribute){
		$value = $object->$attribute;
		
		if ($this->allowEmpty && $this->isEmpty($value))
			return;
		
		if (($pattern = $object->getAttributePattern($attribute)) && isset($pattern['groups'], $pattern['groups_min'])){
			$sets = array();
			$qu = 0;
			
			foreach ($pattern['groups'] as $group)
				if ($group['state'] == 'significant' && isset($pattern['sets'][$group['id']]) && !isset($sets[$group['id']]))
					$sets[$group['id']] = $pattern['sets'][$group['id']];
			
			foreach ($sets as $set)
				if (preg_match("/[$set]/u", $value))
					$qu++;
			
			if ($qu < $pattern['groups_min'])
				$this->addError($object, $attribute, $this->message);
		}
	}
	
	/**
	 * Переопределение стандартного метода clientValidateAttribute, генерирующего JavaScript-код проверки значения атрибута на
	 * стороне клиента 
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @return string JavaScript-код проверки значения атрибута
	 * @see CValidator::clientValidateAttribute()
	 */
	public function clientValidateAttribute($object, $attribute){
		$message = CJSON::encode($this->message);
		
		$js = <<<JS
var inputId = $(this)[0]['inputID'];

if (typeof inputPattern != 'undefined' && inputId in inputPattern && 'groups_significant_find' in inputPattern[inputId] &&
	inputPattern[inputId].groups_significant_find < inputPattern[inputId].groups_min)
		messages.push($message);
JS;
		
		if ($this->allowEmpty)
			$js = "if ($.trim(value) != ''){".PHP_EOL.$js.PHP_EOL.'}'.PHP_EOL;
		
		return Html::clientValidateAttribute($object, $attribute, $js);
	}
	
}
