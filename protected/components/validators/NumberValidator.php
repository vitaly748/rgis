<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CNumberValidator
 * @author v.zubov
 * @see CNumberValidator
 */
class NumberValidator extends CNumberValidator{
	
	/**
	 * @var string Регулярное выражение числа с плавающей точкой
	 */
	public $numberPattern = '/^\s*[-+]?\d*[.,]?\d+\s*$/';
	
	/**
	 * @var string Строка декоративных символов строки с числом
	 */
	public $decoration = '';
	
	/**
	 * Переопределение стандартного метода validateAttribute, осуществляющего проверку значения атрибута
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @see CNumberValidator::validateAttribute()
	 */
	protected function validateAttribute($object, $attribute){
		if ($value = $object->$attribute){
			$object->removeDecoration($attribute);
			
			parent::validateAttribute($object, $attribute);
			
			$object->$attribute = $value;
		}
	}
	
	/**
	 * Переопределение стандартного метода clientValidateAttribute, генерирующего JavaScript-код проверки значения атрибута на
	 * стороне клиента 
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @return string JavaScript-код проверки значения атрибута
	 * @see CNumberValidator::clientValidateAttribute()
	 */
	public function clientValidateAttribute($object, $attribute){
		if (($js = parent::clientValidateAttribute($object, $attribute)) && $this->decoration)
			$js = "value = value.replace(/[{$this->decoration}]/g, '').replace(/,/g, '.');".PHP_EOL.$js;
		
		return Html::clientValidateAttribute($object, $attribute, $js);
	}
	
}
