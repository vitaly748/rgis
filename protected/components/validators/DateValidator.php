<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CDateValidator
 * @author v.zubov
 * @see CDateValidator
 */
class DateValidator extends CDateValidator{
	
	/**
	 * @var string Формат даты
	 * @see CDateTimeParser
	 */
	public $format = array('dd.MM.yyyy', 'dd.MM.yyyy hh:mm');
	
	/**
	 * Переопределение стандартного метода clientValidateAttribute, генерирующего JavaScript-код проверки значения атрибута на
	 * стороне клиента 
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @return string JavaScript-код проверки значения атрибута
	 * @see CDateValidator::clientValidateAttribute()
	 */
	public function clientValidateAttribute($object, $attribute){
		return Html::clientValidateAttribute($object, $attribute, parent::clientValidateAttribute($object, $attribute));
	}
	
}
