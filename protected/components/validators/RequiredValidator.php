<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CRequiredValidator
 * @author v.zubov
 * @see CRequiredValidator
 */
class RequiredValidator extends CRequiredValidator{
	
	/**
	 * Переопределение стандартного метода clientValidateAttribute, генерирующего JavaScript-код проверки значения атрибута на
	 * стороне клиента 
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @return string JavaScript-код проверки значения атрибута
	 * @see CRequiredValidator::clientValidateAttribute()
	 */
	public function clientValidateAttribute($object, $attribute){
		return Html::clientValidateAttribute($object, $attribute, parent::clientValidateAttribute($object, $attribute));
	}
	
}
