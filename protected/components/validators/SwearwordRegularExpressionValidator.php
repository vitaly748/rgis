<?php
/**
 * Класс, переопределяющий и дополняющий класс RegularExpressionValidator
 * @author v.zubov
 * @see RegularExpressionValidator
 */
class SwearwordRegularExpressionValidator extends RegularExpressionValidator{
	
	/**
	 * @var string Шаблон регулярного выражения
	 */
	public $pattern = '/(на|не|ни)( |)ху|ху(и|й|е)|бля|сук(а|и)|твар(ь|и)|мраз(ь|и)|(у|ъ|ь)(е|ё)б|трах|шлюх|пизд|жоп|урод|пид(а|о)р/ui';
	
	/**
	 * @var string Шаблон предварительной очистки обрабатываемой строки
	 */
	public $patternPrecompute = '(абля|обля|ребля|убля|рсук|сукн|сукр|утвар|етрах|страх|трахе|ипидар)';
	
	/**
	 * @var boolean Признак обратной проверки
	 */
	public $not = true;
	
	/**
	 * Переопределение стандартного метода validateAttribute, осуществляющего проверку значения атрибута
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @see CRegularExpressionValidator::validateAttribute()
	 */
	protected function validateAttribute($object, $attribute){
		if ($value = $object->$attribute){
			do
				$object->$attribute = preg_replace("/{$this->patternPrecompute}/u", '', $object->$attribute, -1, $count);
			while ($count);
			
			parent::validateAttribute($object, $attribute);
			
			$object->$attribute = $value;
		}
	}
	
	/**
	 * Переопределение стандартного метода clientValidateAttribute, генерирующего JavaScript-код проверки значения атрибута на
	 * стороне клиента
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @return string JavaScript-код проверки значения атрибута
	 * @see CRegularExpressionValidator::clientValidateAttribute()
	 */
	public function clientValidateAttribute($object, $attribute){
		$js = <<<JS
var valueOld = value;
do{
	var valueCurrent = value;
	
	value = value.replace(/{$this->patternPrecompute}/g, '');
}while (value != valueCurrent);	
JS;
		
		$js = Html::clientValidateAttribute($object, $attribute, $js).PHP_EOL.parent::clientValidateAttribute($object, $attribute).
			PHP_EOL.'value = valueOld;';
		
		return $js;
	}
	
}
