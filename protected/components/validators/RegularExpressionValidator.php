<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CRegularExpressionValidator
 * @author v.zubov
 * @see CRegularExpressionValidator
 */
class RegularExpressionValidator extends CRegularExpressionValidator{
	
	/**
	 * Переопределение стандартного метода clientValidateAttribute, генерирующего JavaScript-код проверки значения атрибута на
	 * стороне клиента 
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @return string JavaScript-код проверки значения атрибута
	 * @see CStringValidator::clientValidateAttribute()
	 */
	public function clientValidateAttribute($object, $attribute){
		return Html::clientValidateAttribute($object, $attribute, parent::clientValidateAttribute($object, $attribute));
	}
	
}
