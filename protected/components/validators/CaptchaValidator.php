<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CCaptchaValidator
 * @author v.zubov
 * @see CCaptchaValidator
 */
class CaptchaValidator extends CCaptchaValidator{
	
	/**
	 * @var boolean Признак регистрозависимой проверки. По умолчанию равно true
	 */
	public $caseSensitive = true;
	
	/**
	 * @var string Текст сообщения об ошибке при одновременном обновлении кода 
	 */
	public $messageRefresh;
	
	/**
	 * Переопределение стандартного метода clientValidateAttribute, генерирующего JavaScript-код проверки значения атрибута на
	 * стороне клиента 
	 * @param CModel $object Экземпляр модели данных
	 * @param string $attribute Название атрибута
	 * @return string JavaScript-код проверки значения атрибута
	 * @see CCaptchaValidator::clientValidateAttribute()
	 */
	public function clientValidateAttribute($object, $attribute){
		$message = CJSON::encode($this->message);
		$message_refresh = CJSON::encode($this->messageRefresh);
		
		$js = <<<JS
if (typeof wCaptcha != 'undefined'){
	var captchaId = wCaptchaGetId($(this)[0]['inputID']),
			captchaCheck = wCaptchaCheck(captchaId, value, $this->caseSensitive);
	
	if (captchaCheck){
		messages.push(captchaCheck == 1 ? $message : $message_refresh);
		
		if (captchaCheck == 2)
			wCaptchaRefresh(id);
	}
}
JS;
		
		if ($this->allowEmpty)
			$js = "if ($.trim(value) != ''){".PHP_EOL.$js.PHP_EOL.'}'.PHP_EOL;
		
		return Html::clientValidateAttribute($object, $attribute, $js);
	}
	
}
