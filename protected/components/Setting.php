<?php
/**
 * Компонент для работы с настройками приложения
 * @author v.zubov
 */
class Setting extends Component{
	
	/**
	 * @var string Псевдоним пути к файлу конфигурации
	 */
	const DIR_CFG = 'application.config.default';
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'setting.php';
	
	/**
	 * @var string Префикс символьных идентификаторов статусов развёртывания приложения
	 */
	const PREFIX_DEPLOY = 'init_';
	
	/**
	 * @var array Ассоциативный массив настроек приложения
	 */
	protected $_data;
	
	/**
	 * Конструктор компонента. Инициализация массива настроек приложения
	 */
	public function init(){
		$params = array('def'=>$def = !Yii::app()->db->active || !Yii::app()->table->checkTable('setting', false, false));
		
		try{
			if (!$data_def = $this->preparationDataDef())
				throw new CHttpException(500, 'empty default data');
			
			if (!$def){
				$data = $this->preparationData();
				
				if (!$data || array_keys($data) != array_keys($data_def))
					$params['def'] = $def = true;
			}
			
			if ($def)
				$data = $data_def;
		}catch (CException $e){
			$params['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e) && $data;
		$this->_data = $data;
		
		Yii::app()->log->fix('application_init', $params, $ok ? 'ok' : 'application');
	}
	
	/**
	 * Выборка и получение настроек приложения
	 * @param [boolean|string $section Название блока настроек или false. По умолчанию равно false. Допускается этим параметром
	 * задавать название настройки. В этом случае настройка будет искаться в блоке "general"
	 * @param [boolean|object|string] Название блока настройки, название настройки, объект или признак установки значений
	 * объекта. По умолчанию все перечисленные параметры равны false. Если не указан ни один параметр, будет возвращён полный
	 * список настроек. Если задан блок настроек, но не задано название настройки, то будут выданы все настройки данного блока.
	 * Если задано название настройки, но не задан блок настройки, то настройка будет искаться в блоке "general". Если настройка
	 * не найдена, будет возвращён false. Если задан объект и не задан блок настроек, то настройки будут искаться в блоке, название
	 * которого соответствует названию класса объекта. При этом если установлен признак установки значений, то соответствующие
	 * свойства объекта будут заполнены полученными настройками
	 * @return mixed Выбранные настройки, false - если настройка не найдена, true - если настройки присвоены свойствам объекта
	 * Возможны следующие комбинации параметров:
	 * section	name		object	set			Результат
	 * ----------------------------------
	 * false		false		false		false		data
	 * section	false		false		false		section
	 * name			false		false		false		general.name
	 * section	name		false		false		section.name
	 * false		false		object	false		section for class object
	 * section	false		object	false		section
	 * false		false		object 	true		set class properties from section for class object
	 * section	false		object 	true		set class properties from section
	 */
	public function get(){
		if (!$data = $this->_data)
			return false;
		
		$section = $name = $object = $set = false;
		$sections = array_keys($data);
		$args = func_get_args();
		
		foreach ($args as $arg)
			if (is_string($arg))
				if (in_array($arg, $sections))
					$section = $arg;
				else{
					$name = $arg;
					
					if (!$section)
						$section = 'general';
				}
			elseif (is_object($arg)){
				$object = $arg;
				$object_name = mb_strtolower(get_class($arg));
				
				if (!$section)
					$section = $object_name;
			}elseif (is_bool($arg))
				$set = $arg;
		
		if (!$section)
			return $data;
		
		if (!$data = Arrays::pop($data, $section))
			return false;
		
		if (!$name)
			if (!isset($object) || !$set)
				return $data;
			else{
				$object_name .= '_';
				$object_name_len = mb_strlen($object_name);
					
				foreach ($data as $key=>$value)
					if (property_exists($object, $object_property = Strings::nameModify(mb_strpos($key, $object_name) === 0 ?
						mb_substr($key, $object_name_len) : $key, Strings::FORMAT_NAME_MODIFY_UPPER, false)))
							$object->$object_property = $value;
				
				return true;
			}
		
		return isset($data[$name]) ? $data[$name] : false;
	}
	
	/**
	 * Перенос данных из конфигурационного массива в БД
	 * @return boolean Успешность выполнения
	 */
	public function deploy(){
		$params = array('data_def'=>$this->_data = $this->_data);
		
		try{
			if ((!$connection = Yii::app()->db) || !$connection->active)
				throw new CHttpException(500, 'connection db failed');
			
			if (!$this->_data)
				throw new CHttpException(500, 'empty data_def');
			
			$transaction = $connection->beginTransaction();
			
			if (($check = Yii::app()->table->checkTable('setting')) !== false){
				if ($check){
					$data_existing = $params['data_existing'] = $this->preparationData();
					
					if ($data_existing)
						foreach ($this->_data as $section=>$data_section)
							foreach ($data_section as $name=>$value)
								if ($section.$name !== 'general.application_state' && isset($data_existing[$section][$name]))
									$this->_data[$section][$name] = $data_existing[$section][$name];
				}
				
				if (!$connection->createCommand()->dropTable('setting', false))
					throw new CHttpException(500, 'error drop table');
			}
			
			if (!Yii::app()->table->createTable('setting'))
				throw new CHttpException(500, 'error create table');
			
			$conformity = Yii::app()->conformity->get(false, 'code', 'name');
			$rows = array(array('section', 'name', 'value'));
			
			foreach ($this->_data as $section=>$data_section)
				foreach ($data_section as $name=>$value)
					$rows[] = array($section, $name, (string)(($code = Arrays::pop($conformity, "$name.$value")) ? $code : $value));
			
			if (!$connection->createCommand()->insert('setting', $rows))
				throw new CHttpException(500, 'error insert');
			
			$transaction->commit();
		}catch (CException $e){
			if (!empty($transaction))
				$transaction->rollback();
			
			$params['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('deploy_settings', $params, $ok ? 'ok' : 'deploy_settings');
		
		return $ok;
	}
	
	/**
	 * Запись конфигурационного блока в БД
	 * @param string $section Название блока
	 * @param array $cfg Ассоциативный массив конфигурации блока
	 * @return boolean Успешность выполнения
	 * @throws CHttpException
	 */
	public function deploySection($section, $cfg){
		$params = array('section'=>$section, 'cfg'=>$cfg);
		
		try{
			if (!$cfg || (!$cfg_def = Arrays::pop($this->_data, $section)) || array_keys($cfg_def) != array_keys($cfg))
				throw new CHttpException(500, 'cfg failed');
			
			if ((!$connection = Yii::app()->db) || !$connection->active)
				throw new CHttpException(500, 'connection db failed');
			
			if (!$connection->createCommand()->updateValues('setting', 'name', 'value', $cfg, "{{setting}}.section = '$section'"))
				throw new CHttpException(500, 'error update');
		}catch (CException $e){
			$params['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('setting_section_update', $params, $ok ? 'ok' : 'setting_section_update');
		
		if ($ok){
			$conformity = Yii::app()->conformity->get(false, 'name', 'code');
			
			foreach ($cfg as $name=>$value)
				if ($value_name = Arrays::pop($conformity, "$name.$value"))
					$cfg[$name] = $value_name;
			
			$this->_data[$section] = $cfg;
			
			if ($section == 'general' && $cfg_def['application_state'] !== $cfg['application_state'] &&
				!$this->setState($cfg['application_state'], false))
					throw new CHttpException(500);
		}
		
		return $ok;
	}
	
	/**
	 * Установка статуса приложения
	 * @param string $state Символьный идентификатор статуса приложения
	 * @param boolean $redirect Признак необходимости перенаправления на очередную страницу настройки приложения
	 * в случае, если приложение проходит этап развёртывания. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 * @throws CHttpException
	 */
	public function setState($state, $redirect = true){
		$params_fix = array('state'=>$state, 'redirect'=>$redirect,
			'state_old'=>Arrays::pop($this->_data, 'general.application_state'));
		
		try{
			if ($params_fix['state_old'] !== $state){
				if (!$state_code = Yii::app()->conformity->get('application_state', 'code', $state))
					throw new CHttpException(500, 'state failed');
				
				if ((!$connection = Yii::app()->db) || !$connection->active)
					throw new CHttpException(500, 'connection db failed');
				
				if ($connection->createCommand()->update('setting', array('value'=>$state_code),
					"section = 'general' AND name = 'application_state'"))
						$this->_data['general']['application_state'] = $state;
					else
						throw new CHttpException(500, 'error update');
			}
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('application_state', $params_fix, $ok ? 'ok' : 'application_state');
		
		if ($ok){
			if (!Yii::app()->controller->setState($state))
				throw new CHttpException(500);
			
			if ($redirect)
				Yii::app()->controller->redirect(Yii::app()->controller->createUrl(mb_strpos($state, static::PREFIX_DEPLOY) === 0 ?
					'deploy/'.mb_substr($state, mb_strlen(static::PREFIX_DEPLOY)) : Yii::app()->defaultController));
		}
		
		return $ok;
	}
	
	/**
	 * Подготовка данных из конфигурационного файла
	 * @return array|boolean Массив данных или false
	 */
	protected function preparationDataDef(){
		$path = Yii::getPathOfAlias(static::DIR_CFG).'/'.static::FN_CFG;
		
		if (is_file($path))
			$data = require $path;
		
		return !empty($data) ? $data : false;
	}
	
	/**
	 * Подготовка данных из БД
	 * @return array|boolean Массив данных или false
	 */
	protected function preparationData(){
		$data = $this->model()->findAll(array('order'=>'id'));
		$result = array();
		
		if ($data){
			$conformity = Yii::app()->conformity->get(false, 'name', 'code');
			
			foreach ($data as $setting)
				$result[$setting->section][$setting->name] = ($name = Arrays::pop($conformity, "{$setting->name}.{$setting->value}")) ?
					$name : $setting->value;
		}
		
		return $result;
	}
	
}
