<?php
/**
 * Класс компонента доступа к моделям данных
 * @author v.zubov
 */
class AccessAttribute extends ComponentDirectory{
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'access_attribute.php';
	
	/**
	 * @var Ключ, обозначающий оператора-владельца модели, в массиве данных компонента
	 */
	const KEY_OWNER = 'owner';
	
	/**
	 * @var Ключ, обозначающий оператора-адресата модели, в массиве данных компонента
	 */
	const KEY_ADDRESSEE = 'addressee';
	
	/**
	 * @var Ключ, обозначающий оператора-свидетеля модели, в массиве данных компонента
	 */
	const KEY_WITNESS = 'witness';
	
	/**
	 * @var Ключ, обозначающий модель без владельца, в массиве данных компонента
	 */
	const KEY_NONE = 'none';
	
	/**
	 * @var array Ассоциативный массив кодов дополнительных ролей для использования в массиве данных компонента
	 */
	const ROLES_ADDITIONAL = array(
		self::KEY_OWNER=>91,
		self::KEY_ADDRESSEE=>92,
		self::KEY_WITNESS=>93,
		self::KEY_NONE=>94
	);
	
	/**
	 * Получение статического экземпляра модели данных
	 * @return boolean|BaseModel Модель данных или false
	 */
	public function model(){
		return AuthAccessAttributeModel::model();
	}
	
	/**
	 * Генерация данных данного компонента в БД
	 * @param string $table Название таблицы
	 * @return boolean Успешность выполнения
	 */
	public function deploy($table){
		if (!$data = $this->data)
			return false;
		
		$actions = Yii::app()->conformity->get('model_action', 'code', 'name');
		$rows = array(array('id_attribute', 'id_attribute_conditional', 'model', 'operator', 'owner', 'value',
			'value_conditional', 'action', 'drop'));
		
		foreach ($data as $model=>$operators)
			foreach ($operators as $operator=>$owners)
				foreach ($owners as $owner=>$attributes)
					foreach ($attributes as $attribute=>$values)
						foreach ($values as $value=>$attributes_conditional)
							foreach ($attributes_conditional as $attribute_conditional=>$values_conditional)
								foreach ($values_conditional as $value_conditional=>$features)
									foreach ($features['actions'] as $action=>$access)
										$rows[] = array(
											$features['id_attribute'],
											$features['id_attribute_conditional'],
											!$attribute,
											$features['id_operator'],
											$features['id_owner'],
											$features['value_code'],
											$features['value_code_conditional'],
											$actions[$action],
											!$access
										);
		
		return Yii::app()->db->createCommand()->insert($table, $rows);
	}
	
	/**
	 * Обработка исходных данных справочника
	 * @param array $data Набор исходных данных
	 * @return array Массив данных справочника
	*/
	protected function preparation($data){
		$result = array();
		$actions_valid = Yii::app()->conformity->get('model_action', 'name', 'code');
		
		if ($data && $actions_valid)
			if (is_object(reset($data))){
				$attr_params = Yii::app()->attribute->getAttributeConformityes(true);
				$roles = Yii::app()->user->getRoles(0, 'name', 'id') + array_flip(static::ROLES_ADDITIONAL);
				
				foreach ($data as $access)
					if ($action = Arrays::pop($actions_valid, $access->action)){
						if (!$attr_param = Arrays::pop($attr_params, $access->id_attribute))
							continue;
						
						if ($access->id_attribute_conditional &&
							(!$attr_param_conditional = Arrays::pop($attr_params, $access->id_attribute_conditional)))
								continue;
						
						if ($access->value && ($access->model || !$attr_param['values'] || !isset($attr_param['values'][$access->value])))
							continue;
						
						if ($access->value_conditional && (empty($attr_param_conditional) || ($attr_param_conditional['values'] &&
							!isset($attr_param_conditional['values'][$access->value_conditional]))))
								continue;
						
						if ($access->operator === null)
							$key_operator = 0;
						elseif ($access->operator < 0)
							$key_operator = -$access->operator;
						else{
							$key_operator = array();
							$operator = $access->operator;
							
							while ($operator){
								$number = $operator < 100 ? $operator : $operator % 100;
								$operator -= $number;
								
								if ($operator)
									$operator = Numbers::intdiv($operator, 100);
								
								if (isset($roles[$number]))
									array_unshift($key_operator, $roles[$number]);
							}
							
							if (!$key_operator)
								continue;
							
							$key_operator = implode('&', $key_operator);
						}
						
						if ($access->owner === null)
							$key_owner = 0;
						elseif ($access->owner < 0)
							$key_owner = -$access->owner;
						elseif (isset($roles[$access->owner]))
							$key_owner = $roles[$access->owner];
						else
							continue;
						
						$key = implode('.', array(
							$attr_param['model'],
							$key_operator,
							$key_owner,
							$access->model ? 0 : $attr_param['attribute'],
							$access->value ? $attr_param['values'][$access->value] : 0,
							$access->id_attribute_conditional ? $attr_param_conditional['attribute'] : 0,
							$access->value_conditional ? ($attr_param_conditional['values'] ?
								$attr_param_conditional['values'][$access->value_conditional] : $access->value_conditional) : 0
						));
						
						if (!Arrays::pop($result, $key))
							Arrays::push($result, $key, array(
								'id_operator'=>$access->operator,
								'id_owner'=>$access->owner,
								'id_attribute'=>$access->id_attribute,
								'value_code'=>$access->value,
								'id_attribute_conditional'=>$access->id_attribute_conditional,
								'value_code_conditional'=>$access->value_conditional,
								'actions'=>array($action=>!$access->drop)
							));
						else
							Arrays::push($result, $key.'.actions.'.$action, !$access->drop);
					}
			}else{
				$models = Yii::app()->attribute->get();
				$confirm_attributes = Yii::app()->attribute->confirmAttributeNames;
				$levels = $params_struct = array('models', 'operators', 'owners', 'attributes', 'values',
					'attributes_conditional', 'values_conditional');
				$params_struct[] = 'actions';
				$qu_levels = count($levels);
				$level = current($levels);
				$data = array($level=>Arrays::extensionParams($data));
				$params = array($level=>array('data'=>array_fill_keys(array('id_operator', 'id_owner', 'id_attribute', 'value_code',
					'id_attribute_conditional', 'value_code_conditional'), null), 'key'=>array()));
				$roles = array_merge(Yii::app()->user->getRoles(0, 'id', 'name'), static::ROLES_ADDITIONAL);
				
				do{
					while (list($key, $value) = each($data[$level]))
						if ($value){
							$data_level = $params[$level]['data'];
							
							switch ($level){
								case 'models':
									if (!$key || (!$params_model = Arrays::pop($models, $key)))
										continue 2;
									
									$data_level['id_attribute'] = reset(reset($params_model));
									
									break;
								case 'operators':
									if ($key){
										if (is_int($key))
											$data_level['id_operator'] = -$key;
										elseif (is_string($key))
											foreach (Strings::devisionWords($key, false, '&') as $word)
												if (isset($roles[$word]))
													$data_level['id_operator'] = ($data_level['id_operator'] ? $data_level['id_operator'] * 100 : 0) +
														$roles[$word];
										
										if (empty($data_level['id_operator']))
											continue 2;
									}
									
									break;
								case 'owners':
									if ($key)
										if (is_int($key))
											$data_level['id_owner'] = -$key;
										elseif (isset($roles[$key]))
											$data_level['id_owner'] = $roles[$key];
										else
											continue 2;
									
									break;
								case 'attributes':
									$params_attribute = $key && $params_model && (!in_array($key, $confirm_attributes) ||
										$params[$level]['key'][0] == 'code') ? Arrays::pop($params_model, $key) : false;
									
									if ($key && !$params_attribute)
										continue 2;
									
									if ($params_attribute){
										$data_level['id_attribute'] = $params_attribute['id'];
										
										if ($params_attribute['values'])
											$params_attribute['values'] = Arrays::select($params_attribute['values'], 'name', 'code');
									}
									
									break;
								case 'values':
									$value_code = $key && !empty($params_attribute['values']) ?
										array_search($key, $params_attribute['values']) : false;
									
									if ($key && !$value_code)
										continue 2;
									
									if ($value_code)
										$data_level['value_code'] = $value_code;
									
									break;
								case 'attributes_conditional':
									$params_attribute_conditional = $key && $params_model ? Arrays::pop($params_model, $key) : false;
									
									if ($key && !$params_attribute_conditional)
										continue 2;
									
									if ($params_attribute_conditional){
										$data_level['id_attribute_conditional'] = $params_attribute_conditional['id'];
										
										if ($params_attribute_conditional['values'])
											$params_attribute_conditional['values'] =
												Arrays::select($params_attribute_conditional['values'], 'name', 'code');
									}
									
									break;
								case 'values_conditional':
									$value_code_conditional = !$key ? false : (empty($params_attribute_conditional['values']) ? (int)$key :
										array_search($key, $params_attribute_conditional['values']));
									
									if ($key && !$value_code_conditional)
										continue 2;
									
									if ($value_code_conditional)
										$data_level['value_code_conditional'] = $value_code_conditional;
									
									break;
							}
							
							$key_level = array_merge($params[$level]['key'], array($key));
							$is_struct = is_array($value) && !array_diff(array_keys($value), $params_struct);
							
							if ($actions = $this->getActions($is_struct ? Arrays::pop($value, 'actions', true) : $value, $actions_valid))
								Arrays::push($result, implode('.', array_pad($key_level, $qu_levels, 0)),
									array_merge($data_level, array('actions'=>$actions)));
							
							if ($is_struct && $level !== 'values_conditional'){
								if (!$level = next($levels))
									break 2;
								
								if (!isset($value[$level]))
									$params_next = array($value);
								else{
									$extension_names = false;
									
									switch ($level){
										case 'attributes':
											if ($params_model)
												$extension_names = array_keys($params_model);
											
											break;
										case 'values':
											if (!empty($params_attribute['values']))
												$extension_names = $params_attribute['values'];
											
											break;
										case 'values_conditional':
											if (!empty($params_attribute_conditional['values']))
												$extension_names = $params_attribute_conditional['values'];
											
											break;
									}
									
									$params_next = Arrays::extensionParams($value[$level], $extension_names, 'actions');
								}
								
								$data[$level] = $params_next;
								$params[$level] = array('data'=>$data_level, 'key'=>$key_level);
							}
						}
				}while ($level = prev($levels));
			}
		
		return $result;
	}
	
	/**
	 * Расчёт действий на уровне для функции preparation 
	 * @param mixed $actions Строка или массив параметров, описывающих набор допустимых действий на уровне
	 * @param mixed $actions_valid Список доступных действий
	 * @return array|boolean Ассоциативный массив параметров доступных действий на уровне
	 */
	private function getActions($actions, $actions_valid){
		if (in_array($general_type = Arrays::getGeneralType($actions), array('boolean', 'string'))){
			$general_type_keys = Arrays::getGeneralType(array_keys($actions));
			
			if ($general_type === 'boolean' && $general_type_keys === 'string')
				return Arrays::filterKey($actions, $actions_valid);
			elseif (!($general_type === 'string' && $general_type_keys === 'integer'))
				return false;
		}elseif (!is_string($actions) || (!$actions = Strings::devisionWords($actions)))
			return false;
		
		foreach ($actions as $action)
			if ($action)
				$actions_res[($drop = $action[0] === '-') ? mb_substr($action, 1) : $action] = !$drop;
		
		return isset($actions_res) ? Arrays::filterKey($actions_res, $actions_valid) : false;
	}
	
}
