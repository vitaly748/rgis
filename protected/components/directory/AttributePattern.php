<?php
/**
 * Класс компонента "Справочник шаблонов атрибутов"
 * @author v.zubov
 */
class AttributePattern extends ComponentDirectory{
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'attribute_pattern.php';
	
	/**
	 * Генерация данных таблицы данного справочника в БД
	 * @param string $table Название таблицы
	 * @return boolean Успешность выполнения
	 */
	public function deploy($table){
		if (!$data = $this->data)
			return false;
		
		$rows_pattern = array(array('id', 'id_attribute', 'length_min', 'length_max', 'groups_min',
			'groups_separation', 'separation_reverse', 'tooltip_inclusion'));
		$rows_group = array(array('id', 'id_pattern', 'id_set', 'state', 'pos', 'length_min', 'length_max'));
		
		foreach ($data as $model=>$attributes){
			if ($model != 'code')
				$attributes = Arrays::filterKey($attributes, Yii::app()->attribute->confirmAttributeNames, false);
			
			foreach ($attributes as $attribute=>$features){
				$rows_pattern[] = array($features['id'], $features['id_attribute'],
					$features['length_min'], $features['length_max'], $features['groups_min'],
					$features['groups_separation'], $features['separation_reverse'], $features['tooltip_inclusion']);
				
				foreach ($features['groups'] as $params_group)
					$rows_group[] = array($params_group['id_group'], $features['id'], $params_group['id_set'],
						$params_group['state_code'], $params_group['pos'], $params_group['length_min'], $params_group['length_max']);
			}
		}
		
		return Yii::app()->db->createCommand()->insert($table, $rows_pattern) &&
			Yii::app()->db->createCommand()->insert('directory_attribute_pattern_group', $rows_group);
	}
	
	/**
	 * Переопределение стандартного метода preparationData, подготавливающего данные из БД
	 * @param array|boolean|string $with Название или список названий символьных идентификаторов табличных связей,
	 * участвующих в запросе на получение данных или false. По умолчанию равно "group"
	 * @see ComponentDirectory::preparationData()
	 */
	protected function preparationData($with = 'group'){
		return parent::preparationData($with);
	}
	
	/**
	 * Обработка исходных данных справочника
	 * @param array $data Набор исходных данных
	 * @return array Массив данных справочника
	 */
	protected function preparation($data){
		$result = array();
		
		if ($data && ($models = Yii::app()->attribute->get()))
			if (is_object(reset($data))){
				$states = Yii::app()->conformity->get('attribute_pattern_group_state', 'name', 'code');
				$attr_params = Yii::app()->attribute->getAttributeConformityes();
				
				foreach ($data as $pattern)
					if ($attr_param = Arrays::pop($attr_params, $pattern->id_attribute)){
						list($model, $attribute) = array_values($attr_param);
						$groups = array();
						
						foreach ($pattern->group as $group)
							$groups[] = array(
								'id_group'=>$group->id,
								'id_set'=>$group->id_set,
								'state'=>$states[$group->state],
								'state_code'=>$group->state,
								'pos'=>$group->pos,
								'length_min'=>$group->length_min,
								'length_max'=>$group->length_max
							);
						
						$result[$model][$attribute] = array(
							'id'=>$pattern->id,
							'id_attribute'=>$id_attribute_old = $pattern->id_attribute,
							'length_min'=>$pattern->length_min,
							'length_max'=>$pattern->length_max,
							'groups_min'=>$pattern->groups_min,
							'groups_separation'=>$pattern->groups_separation,
							'separation_reverse'=>$pattern->separation_reverse,
							'tooltip_inclusion'=>$pattern->tooltip_inclusion,
							'groups'=>$groups
						);
					}
			}else{
				$id = $id_group = 1;
				$states = Yii::app()->conformity->get('attribute_pattern_group_state', 'code', 'name');
				$sets = array_flip(Yii::app()->attributePatternGroupSet->get(false, 'name'));
				$params_attributes = array('length_min', 'length_max', 'groups_min', 'groups_separation', 'separation_reverse',
					'tooltip_inclusion');
				
				foreach ($data as $model=>$attributes)
					if ($attributes = Arrays::extensionParams($attributes)){
						$meta_attributes = Yii::app()->table->getCfgTable($model);
						
						foreach ($attributes as $attribute=>$params_pattern)
							if ($id_attribute = Arrays::pop($models, "$model.$attribute.id")){
								
								if (is_string($params_pattern))
									$params_pattern = Strings::devisionWords($params_pattern);
								
								if (!$params_pattern)
									$params_pattern = array('groups'=>array());
								elseif (!isset($params_pattern['groups']))
									if (array_intersect($params_attributes, array_keys($params_pattern)))
										$params_pattern['groups'] = array();
									else
										$params_pattern = array('groups'=>$params_pattern);
								
								if (is_string($params_pattern['groups']))
									$params_pattern['groups'] = Strings::devisionWords($params_pattern['groups']);
								
								$groups = array();
								
								if ($params_pattern['groups'])
									foreach ($params_pattern['groups'] as $names=>$params_group){
										if (!is_string($names))
											if (is_string($params_group)){
												$names = $params_group;
												$params_group = array();
											}elseif (!is_string($names = Arrays::pop($params_group, 'name')))
												continue;
										
										foreach (Strings::devisionWords($names) as $name){
											$pattern_name = Arrays::pop($params_group, 'name');
											
											$groups[] = array(
												'id_group'=>$id_group++,
												'id_set'=>$sets[$pattern_name ? $pattern_name : $name],
												'state'=>$state = isset($params_group['state']) ? $params_group['state'] : 'significant',
												'state_code'=>$states[$state],
												'pos'=>isset($params_group['pos']) ? $params_group['pos'] : null,
												'length_min'=>isset($params_group['length_min']) ? $params_group['length_min'] : null,
												'length_max'=>isset($params_group['length_max']) ? $params_group['length_max'] : null
											);
										}
									}
								
								$length_min = !empty($params_pattern['length_min']) ? $params_pattern['length_min'] : null;
								$length_max = !empty($params_pattern['length_max']) ? $params_pattern['length_max'] : null;
								
								if ($meta_len = Arrays::pop($meta_attributes, "$attribute.len"))
									$length_max = $length_max ? min($length_max, $meta_len) : $meta_len;
								
								$result[$model][$attribute] = array(
									'id'=>$id++,
									'id_attribute'=>$id_attribute,
									'length_min'=>$length_min,
									'length_max'=>$length_max,
									'groups_min'=>isset($params_pattern['groups_min']) ? $params_pattern['groups_min'] : null,
									'groups_separation'=>isset($params_pattern['groups_separation']) ? $params_pattern['groups_separation'] : null,
									'separation_reverse'=>isset($params_pattern['separation_reverse']) ?
										$params_pattern['separation_reverse'] : null,
									'tooltip_inclusion'=>isset($params_pattern['tooltip_inclusion']) ? $params_pattern['tooltip_inclusion'] : true,
									'groups'=>$groups
								);
							}
					}
			}
		
		if (($confirm_attribute_names = Yii::app()->attribute->confirmAttributeNames) &&
			($code_attributes = Arrays::pop($result, 'code')) &&
			($confirm_attributes = Arrays::filterEmpty(Arrays::pop($code_attributes, $confirm_attribute_names, false, true))))
				foreach (array_diff(array_keys($result), array('code')) as $model)
					$result[$model] = array_merge($result[$model], $confirm_attributes);
		
		return $result;
	}
	
}
