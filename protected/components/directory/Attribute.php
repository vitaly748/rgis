<?php
/**
 * Класс компонента "Справочник атрибутов"
 * @author v.zubov
 */
class Attribute extends ComponentDirectory{
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'attribute.php';
	
	/**
	 * @var string Разделитель подсказок атрибута
	 */
	const SEPARATOR_TOOLTIP = Table::SEPARATOR_WORDS.Table::SEPARATOR_WORDS;
	
	/**
	 * @var array Список названий атрибутов моделей, предназначенных для ввода кодов подтверждения
	 */
	private $_confirmAttributeNames;
	
	/**
	 * Метод-геттер для определения _confirmAttributeNames
	 * @return array Список названий атрибутов моделей, предназначенных для ввода кодов подтверждения
	 */
	public function getConfirmAttributeNames(){
		if ($this->_confirmAttributeNames === null){
			$this->_confirmAttributeNames = ($confirm_types = Yii::app()->conformity->get('model_action_confirm_type', 'name',
				false, 'none')) ? Strings::mergeWords($confirm_types, false, false, BaseModel::POSTFIX_CONFIRM_ATTRIBUTES, false) : array();
		}
		
		return $this->_confirmAttributeNames;
	}
	
	/**
	 * Получение списка соответствий атрибутов и их идентификаторов 
	 * @param boolean $values Признак необходимости включения в результирующий массив данных о наборе фиксированных
	 * значений атрибутов. По умолчанию равно false
	 * @return array|boolean Ассоциативный массив соответствий или false. Формат массива:
	 * 	array(
	 * 		{Цифровой идентификатор атрибута}=>array(
	 * 			'model'=>{Символьный идентификатор модели атрибута},// Обязательный параметр
	 * 			'attribute'=>{Символьный идентификатор атрибута},// Обязательный параметр
	 * 			'values'=>array(// Необязательный параметр
	 * 				{Цифровой идентификатор значения}=>{Символьный идентификатор значения атрибута},
	 * 				...
	 * 			)
	 * 		),
	 * 		...
	 * 	)
	 */
	public function getAttributeConformityes($values = false){
		if (!$data = $this->data)
			return false;
		
		$confirm_attributes = $this->confirmAttributeNames;
		
		foreach ($data as $model=>$attributes)
			foreach ($attributes as $attribute=>$features)
				if (!in_array($attribute, $confirm_attributes) || $model == 'code'){
					$result[$features['id']] = array('model'=>$model, 'attribute'=>$attribute);
					
					if ($values)
						$result[$features['id']]['values'] = $features['values'] ? Arrays::select($features['values'], 'name', 'code') : false;
				}
		
		return $result;
	}
	
	/**
	 * Генерация данных таблицы данного справочника в БД
	 * @param string $table Название таблицы
	 * @return boolean Успешность выполнения
	 */
	public function deploy($table){
		if (!$data = $this->data)
			return false;
		
		$rows = array(array('id', 'id_set', 'model', 'attribute', 'label', 'tooltip', 'state', 'service'));
		
		foreach ($data as $model=>$attributes){
			if ($model != 'code')
				$attributes = Arrays::filterKey($attributes, $this->confirmAttributeNames, false);
			
			foreach ($attributes as $attribute=>$features)
				$rows[] = array($features['id'], $features['id_set'], $model, $attribute,
					$features['label'], $features['tooltip'], $features['state_code'], $features['service']);
		}
		
		return Yii::app()->db->createCommand()->insert($table, $rows);
	}
	
	/**
	 * Обработка исходных данных справочника
	 * @param array $data Набор исходных данных
	 * @return array Массив данных справочника
	 */
	protected function preparation($data){
		$result = array();
		
		if ($data)
			if (is_object(reset($data))){
				$states = Yii::app()->conformity->get('attribute_state', 'name', 'code');
				
				foreach ($data as $attribute)
					$result[$attribute->model][$attribute->attribute] = array(
						'id'=>$attribute->id,
						'id_set'=>$attribute->id_set,
						'label'=>$attribute->label,
						'tooltip'=>$attribute->tooltip,
						'state'=>$states[$attribute->state],
						'state_code'=>$attribute->state,
						'service'=>$attribute->service,
						'values'=>$attribute->id_set ? Yii::app()->conformity->get($attribute->id_set) : null
					);
			}else{
				$states = Yii::app()->conformity->get('attribute_state', 'code', 'name');
				$id = 1;
				
				foreach ($data as $model=>$attributes)
					foreach ($attributes as $attribute=>$features){
						if (!empty($features['set']) && ($set = Yii::app()->conformity->get($features['set']))){
							$id_set = reset(reset($set));
							$values = $set;
						}else
							$id_set = $values = null;
						
						$result[$model][$attribute] = array(
							'id'=>$id++,
							'id_set'=>$id_set,
							'label'=>isset($features['label']) ? $features['label'] : (is_array($features) ? $attribute : $features),
							'tooltip'=>isset($features['tooltip']) ? $features['tooltip'] : null,
							'state'=>$state = isset($features['state']) ? $features['state'] : 'required',
							'state_code'=>$states[$state],
							'service'=>isset($features['service']) ? $features['service'] : true,
							'values'=>$values
						);
					}
			}
		
		if (isset($result['user']['roles']) && ($roles = Yii::app()->user->GetRoles(0, false)))
			foreach ($roles as $features)
				$result['user']['roles']['values'][] = array(
					'id'=>$features['id'],
					'code'=>$features['id'],
					'name'=>$features['name'],
					'description'=>$features['description'],
					'radio'=>Arrays::pop($features, 'radio')
				);
		
		if (($confirm_attribute_names = $this->confirmAttributeNames) && ($code_attributes = Arrays::pop($result, 'code')) &&
			($confirm_attributes = Arrays::filterEmpty(Arrays::pop($code_attributes, $confirm_attribute_names, false, true))))
				foreach (array_diff(array_keys($result), array('code')) as $model)
					$result[$model] = array_merge($result[$model], $confirm_attributes);
		
		return $result;
	}
	
}
