<?php
/**
 * Класс компонента "Справочник текстов уведомлений"
 * @author v.zubov
 */
class NotificationMessage extends ComponentDirectory{
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'notification_message.php';
	
	/**
	 * Генерация данных таблицы данного справочника в БД
	 * @param string $table Название таблицы
	 * @return boolean Успешность выполнения
	 */
	public function deploy($table){
		if (!$data = $this->data)
			return false;
		
		$rows = array(array('id', 'id_event', 'id_error', 'message', 'state'));
		
		foreach ($data as $event=>$errors)
			foreach ($errors as $error=>$features)
				$rows[] = array($features['id'], $features['id_event'], $features['id_error'], $features['message'],
					$features['state_code']);
		
		return Yii::app()->db->createCommand()->insert($table, $rows);
	}
	
	/**
	 * Обработка исходных данных справочника
	 * @param array $data Набор исходных данных
	 * @return array Массив данных справочника
	 */
	protected function preparation($data){
		$result = array();
		
		if ($data){
			$events = Yii::app()->event->get();
			$errors_events = Yii::app()->errorEvent->get();
			
			if (is_object(reset($data))){
				$states = Yii::app()->conformity->get('notification_message_state', 'name', 'code');
				$events = Arrays::facing($events, 'id');
				$errors_events = Arrays::facing($errors_events, 'id');
				
				foreach ($data as $notification)
					if (isset($events[$notification->id_event]) && isset($errors_events[$notification->id_error])){
						$event = $events[$notification->id_event]['name'];
						$error = $errors_events[$notification->id_error]['name'];
						
						$result[$event][$error] = $notification->attributes;
						$result[$event][$error]['state_code'] = $result[$event][$error]['state'];
						$result[$event][$error]['state'] = $states[$result[$event][$error]['state']];
					}
			}else{
				$states = Yii::app()->conformity->get('notification_message_state', 'code', 'name');
				$id = 1;
				
				foreach ($data as $event=>$errors)
					foreach ($errors as $error=>$features){
						if (is_int($error))
							$error = $features;
						
						if (isset($events[$event]) && isset($errors_events[$error])){
							$message = !is_array($features) ? $features :
								(isset($features['message']) ? $features['message'] : $errors_events[$error]['description']);
							$state = isset($features['state']) ? $features['state'] : 'enabled';
							
							$result[$event][$error] = array(
								'id'=>$id++,
								'id_event'=>$events[$event]['id'],
								'id_error'=>$errors_events[$error]['id'],
								'message'=>$message,
								'state'=>$state,
								'state_code'=>$states[$state]
							);
						}
					}
			}
		}
		
		return $result;
	}
	
}
