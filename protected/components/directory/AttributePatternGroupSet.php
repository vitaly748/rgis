<?php
/**
 * Класс компонента "Справочник описаний групп шаблонов атрибутов"
 * @author v.zubov
 */
class AttributePatternGroupSet extends ComponentDirectory{
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'attribute_pattern_group_set.php';
	
	/**
	 * Генерация данных таблицы данного справочника в БД
	 * @param string $table Название таблицы
	 * @return boolean Успешность выполнения
	 */
	public function deploy($table){
		if (!$data = $this->data)
			return false;
		
		$rows = array(array('id', 'description', 'set'));
		
		foreach ($data as $id=>$features)
			$rows[] = array($id, $features['description'], $features['set']);
		
		return Yii::app()->db->createCommand()->insert($table, $rows);
	}
	
	/**
	 * Обработка исходных данных справочника
	 * @param array $data Набор исходных данных
	 * @return array Массив данных справочника
	 */
	protected function preparation($data){
		$result = array();
		
		if ($data)
			if (is_object(reset($data)))
				foreach ($data as $features)
					$result[$features->id] = array(
						'description'=>$features->description,
						'set'=>$set = $features->set,
						'regexp'=>Strings::preparationRegexp($set)
					);
			else{
				$id = 1;
				
				foreach ($data as $name=>$features)
					$result[$id++] = array(
						'name'=>$name,
						'description'=>$features['description'],
						'set'=>$set = $features['set'],
						'regexp'=>Strings::preparationRegexp($set)
					);
			}
		
		return $result;
	}
	
}
