<?php
/**
 * Класс компонента "Справочник событий"
 * @author v.zubov
 */
class Event extends ComponentDirectory{
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'event.php';
	
	/**
	 * Генерация данных таблицы данного справочника в БД
	 * @param string $table Название таблицы
	 * @return boolean Успешность выполнения
	 */
	public function deploy($table){
		if (!$data = $this->data)
			return false;
		
		$rows = array(array('id', 'name', 'description', 'fix_log', 'fix_flash'));
		
		foreach ($data as $features)
			$rows[] = array($features['id'], $features['name'], $features['description'], $features['fix_log_code'],
				$features['fix_flash_code']);
		
		return Yii::app()->db->createCommand()->insert($table, $rows);
	}
	
	/**
	 * Обработка исходных данных справочника
	 * @param array $data Набор исходных данных
	 * @return array Массив данных справочника
	 */
	protected function preparation($data){
		$result = array();
		
		if ($data)
			if (is_object(reset($data))){
				$states = Yii::app()->conformity->get('event_fix', 'name', 'code');
				
				foreach ($data as $event){
					$result[$event->name] = $event->attributes;
					
					$result[$event->name]['fix_log_code'] = $result[$event->name]['fix_log'];
					$result[$event->name]['fix_log'] = $states[$result[$event->name]['fix_log']];
					
					$result[$event->name]['fix_flash_code'] = $result[$event->name]['fix_flash'];
					$result[$event->name]['fix_flash'] = $states[$result[$event->name]['fix_flash']];
				}
			}else{
				$states = Yii::app()->conformity->get('event_fix', 'code', 'name');
				$id = 1;
				
				foreach ($data as $name=>$features)
					$result[$name] = array(
						'id'=>$id++,
						'name'=>$name,
						'description'=>isset($features['description']) ? $features['description'] : $features,
						'fix_log'=>$fix_log = isset($features['fix_log']) ? $features['fix_log'] : 'by_error',
						'fix_flash'=>$fix_flash = isset($features['fix_flash']) ? $features['fix_flash'] : 'by_error',
						'fix_log_code'=>$states[$fix_log],
						'fix_flash_code'=>$states[$fix_flash]
					);
			}
		
		return $result;
	}
	
}
