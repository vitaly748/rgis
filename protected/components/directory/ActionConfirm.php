<?php
/**
 * Класс компонента "Справочник типов подтверждений действий над моделями данных"
 * @author v.zubov
 */
class ActionConfirm extends ComponentDirectory{
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'action_confirm.php';
	
	/**
	 * Генерация данных таблицы данного справочника в БД
	 * @param string $table Название таблицы
	 * @return boolean Успешность выполнения
	 */
	public function deploy($table){
		if (!$data = $this->data)
			return false;
		
		$rows = array(array('id_attribute', 'action', 'type'));
		
		foreach ($data as $confirms)
			foreach ($confirms as $features)
				$rows[] = array($features['id_attribute'], $features['action_code'], $features['type_code']);
		
		return Yii::app()->db->createCommand()->insert($table, $rows);
	}
	
	/**
	 * Обработка исходных данных справочника
	 * @param array $data Набор исходных данных
	 * @return array Массив данных справочника
	 */
	protected function preparation($data){
		$result = array();
		
		if ($data && ($models = Yii::app()->attribute->get()))
			if (is_object(reset($data))){
				$actions = Yii::app()->conformity->get('model_action', 'name', 'code');
				$types = Yii::app()->conformity->get('model_action_confirm_type', 'name', 'code');
				$attr_params = Yii::app()->attribute->getAttributeConformityes();
				
				foreach ($data as $confirm)
					if (($model = Arrays::pop($attr_params, $confirm->id_attribute.'.model')) &&
						isset($actions[$confirm->action], $types[$confirm->type]))
							$result[$model][$actions[$confirm->action]] = array(
								'id_attribute'=>$confirm->id_attribute,
								'type'=>$types[$confirm->type],
								'action_code'=>$confirm->action,
								'type_code'=>$confirm->type
							);
			}else{
				$actions = Yii::app()->conformity->get('model_action', 'code', 'name');
				$types = Yii::app()->conformity->get('model_action_confirm_type', 'code', 'name');
				
				foreach ($data as $model=>$confirms)
					if ($params_model = Arrays::pop($models, $model)){
						$id_attribute = reset(reset($params_model));
						
						foreach (Arrays::extensionParams($confirms) as $action=>$type)
							if (isset($actions[$action], $types[$type]))
								$result[$model][$action] = array(
									'id_attribute'=>$id_attribute,
									'type'=>$type,
									'action_code'=>$actions[$action],
									'type_code'=>$types[$type]
								);
					}
			}
		
		return $result;
	}
	
}
