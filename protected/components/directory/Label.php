<?php
/**
 * Класс компонента "Справочник сообщений"
 * @author v.zubov
 */
class Label extends ComponentDirectory{
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'label.php';
	
	/**
	 * Генерация данных таблицы данного справочника в БД
	 * @param string $table Название таблицы
	 * @return boolean Успешность выполнения
	 */
	public function deploy($table){
		if (!$data = $this->data)
			return false;
		
		$rows = array(array('name', 'description'));
		
		foreach ($data as $name=>$description)
			$rows[] = array($name, $description);
		
		return Yii::app()->db->createCommand()->insert($table, $rows);
	}
	
	/**
	 * Подготовка данных из БД
	 * @param array|boolean|string $with Название или список названий символьных идентификаторов табличных связей,
	 * участвующих в запросе на получение данных или false. По умолчанию равно false
	 * @return array|null Массив данных справочника или null
	 */
	protected function preparationData($with = false){
		$rows = DirectoryLabelModel::model()->findAllSelective('name', 'description');
		
		return $rows ? $rows : null;
	}
	
}
