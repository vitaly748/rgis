<?php
/**
 * Класс компонента "Справочник ошибок событий"
 * @author v.zubov
 */
class ErrorEvent extends ComponentDirectory{
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'error_event.php';
	
	/**
	 * Генерация данных таблицы данного справочника в БД
	 * @param string $table Название таблицы
	 * @return boolean Успешность выполнения
	 */
	public function deploy($table){
		if (!$data = $this->data)
			return false;
		
		$rows = array(array('id', 'name', 'description', 'exception'));
		
		foreach ($data as $features)
			$rows[] = array($features['id'], $features['name'], $features['description'], $features['exception']);
		
		return Yii::app()->db->createCommand()->insert($table, $rows);
	}
	
	/**
	 * Подготовка данных из БД
	 * @param array|boolean|string $with Название или список названий символьных идентификаторов табличных связей,
	 * участвующих в запросе на получение данных или false. По умолчанию равно false
	 * @return array|null Массив данных справочника или null
	 */
	protected function preparationData($with = false){
		$rows = DirectoryErrorEventModel::model()->findAllSelective('name', false, true);
		
		return $rows ? $rows : null;
	}
	
	/**
	 * Обработка исходных данных справочника
	 * @param array $data Набор исходных данных
	 * @return array Массив данных справочника
	 */
	protected function preparation($data){
		$result = array();
		
		if ($data){
			$id = 1;
			
			foreach ($data as $name=>$features)
				$result[$name] = array(
					'id'=>$id++,
					'name'=>$name,
					'description'=>isset($features['description']) ? $features['description'] : $features,
					'exception'=>isset($features['exception']) ? $features['exception'] : 0
				);
		}
		
		return $result;
	}
	
}
