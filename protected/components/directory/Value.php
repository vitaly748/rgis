<?php
/**
 * Класс компонента "Справочник атрибутов"
 * @author v.zubov
 */
class Value extends ComponentDirectory{
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'value.php';
	
	/**
	 * @var array Список элементов данных в неструктурированном виде в формате:
	 * 	array(
	 * 		'appeal_themeid'=>array(// Название группы значений
	 * 			'1.1'=>array(// Код значения
	 * 				'id'=>5,
	 * 				'id_parent'=>1,
	 * 				'name'=>'def',
	 * 				'code'=>'1.1',
	 * 				'description'=>'test,
	 * 				'param1'=>'5',
	 * 				'param2'=>'6',
	 * 				'childs'=>array()
	 * 			),
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	protected $_dataId;
	
	/**
	 * @var array Список маршрутов элементов данных в формате:
	 * 	array(
	 * 		// {Код элемента}=>{Список кодов элементов маршрута до данного элемента в структуре данных компонента}
	 * 		'1.1'=>array('1', '1.1'),
	 * 		...
	 * 	)
	 */
// 	protected $_dataRoute;
	
	/**
	 * Генерация данных таблицы данного справочника в БД
	 * @param string $table Название таблицы
	 * @return boolean Успешность выполнения
	 */
	public function deploy($table){
		if (!$data = $this->data)
			return false;
		
		$rows = array(array('id', 'id_parent', 'name', 'code', 'description', 'param1', 'param2'));
		
		foreach ($data as $name=>$values1)
			foreach ($values1 as $value1){
				$rows[] = array($value1['id'], $value1['id_parent'], $value1['name'], $value1['code'], $value1['description'],
					$value1['param1'], $value1['param2']);
				
				if ($value1['childs'])
					foreach ($value1['childs'] as $value2){
						$rows[] = array($value2['id'], $value2['id_parent'], $value2['name'], $value2['code'], $value2['description'],
							$value2['param1'], $value2['param2']);
						
						if ($value2['childs'])
							foreach ($value2['childs'] as $value3)
								$rows[] = array($value3['id'], $value3['id_parent'], $value3['name'], $value3['code'], $value3['description'],
									$value3['param1'], $value3['param2']);
					}
			}
		
		return Yii::app()->db->createCommand()->insert($table, $rows);
	}
	
	/**
	 * Метод-геттер для определения _dataId
	 * @return array Элементы данных в неструктурированном виде
	 */
	public function getDataId(){
		if ($this->_dataId === null){
// 			$this->_dataId = $this->_dataRoute = array();
			$this->_dataId = array();
			
			if ($this->data)
				foreach ($this->data as $name=>$values1)
					foreach ($values1 as $value1){
						$this->_dataId[$name][$value1['code']] = $value1;
// 						$this->_dataRoute[$value1['code']] = array($value1['code']);
						
						if ($value1['childs'])
							foreach ($value1['childs'] as $value2){
								$this->_dataId[$name][$value2['code']] = $value2;
// 								$this->_dataRoute[$value2['code']] = array($value1['code'], $value2['code']);
								
								if ($value2['childs'])
									foreach ($value2['childs'] as $value3){
										$this->_dataId[$name][$value3['code']] = $value3;
// 										$this->_dataRoute[$value3['code']] = array($value1['code'], $value2['code'], $value3['code']);
									}
							}
					}
		}
		
		return $this->_dataId;
	}
	

	/**
	 * Метод-геттер для определения _dataRoute
	 * @return array Список маршрутов элементов данных
	 */
	/* public function getDataRoute(){
		if ($this->_dataRoute === null)
			$this->getDataId();
		
		return $this->_dataRoute;
	} */
	
	/**
	 * Обработка исходных данных справочника
	 * @param array $data Набор исходных данных
	 * @return array Массив данных справочника
	 */
	protected function preparation($data){
		$result = array();
		
		if ($data)
			if (is_object(reset($data))){
				$parents = array();
				
				foreach ($data as $row){
					$features = array(
						'id'=>$row->id,
						'id_parent'=>$row->id_parent,
						'name'=>$row->name,
						'code'=>$row->code,
						'description'=>$row->description,
						'param1'=>$row->param1,
						'param2'=>$row->param2,
						'childs'=>array()
					);
					
					if (!$row->id_parent){
						$result[$row->name][$row->code] = $features;
						$parents[$row->id] = &$result[$row->name][$row->code];
					}elseif (isset($parents[$row->id_parent])){
						$parents[$row->id_parent]['childs'][$row->code] = $features;
						$parents[$row->id] = &$parents[$row->id_parent]['childs'][$row->code];
					}
				}
			}else{
				$id = 1;
				$stack = array();
				
				foreach ($data as $name=>$values1)
					if ($values1)
						foreach ($values1 as $key1=>$value1){
							$id_value1 = $id++;
							$code1 = !empty($value1['code']) ? $value1['code'] : $key1;
							$childs1 = array();
							
							if ($values2 = Arrays::pop($value1, 'childs'))
								foreach ($values2 as $key2=>$value2){
									$id_value2 = $id++;
									$code2 = !empty($value2['code']) ? $value2['code'] : $key2;
									$childs2 = array();
									
									if ($values3 = Arrays::pop($value2, 'childs'))
										foreach ($values3 as $key3=>$value3){
											$id_value3 = $id++;
											$code3 = !empty($value3['code']) ? $value3['code'] : $key3;
											
											$childs2[$code3] = array(
												'id'=>$id_value3,
												'id_parent'=>$id_value2,
												'name'=>$name,
												'code'=>(string)$code3,
												'description'=>is_string($value3) ? $value3 : Arrays::pop($value3, 'description'),
												'param1'=>!empty($value3['param1']) ? $value3['param1'] : null,
												'param2'=>!empty($value3['param2']) ? $value3['param2'] : null,
												'childs'=>array()
											);
										}
									
									$childs1[$code2] = array(
										'id'=>$id_value2,
										'id_parent'=>$id_value1,
										'name'=>$name,
										'code'=>(string)$code2,
										'description'=>is_string($value2) ? $value2 : Arrays::pop($value2, 'description'),
										'param1'=>!empty($value2['param1']) ? $value2['param1'] : null,
										'param2'=>!empty($value2['param2']) ? $value2['param2'] : null,
										'childs'=>$childs2
									);
								}
							
							$result[$name][$code1] = array(
								'id'=>$id_value1,
								'id_parent'=>null,
								'name'=>$name,
								'code'=>(string)$code1,
								'description'=>is_string($value1) ? $value1 : Arrays::pop($value1, 'description'),
								'param1'=>!empty($value1['param1']) ? $value1['param1'] : null,
								'param2'=>!empty($value1['param2']) ? $value1['param2'] : null,
								'childs'=>$childs1
							);
						}
			}
		
		return $result;
	}
	
}
