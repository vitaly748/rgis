<?php
/**
 * Класс компонента "Справочник соответствий"
 * @author v.zubov
 */
class Conformity extends ComponentDirectory{
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'conformity.php';
	
	/**
	 * @var array Ассоциативный массив идентификаторов секций в формате:
	 * 	array(
	 * 		{Идентификатор секции}=>{Название секции},
	 * 		...
	 * 	)
	 */
	private $_sectionIds;
	
	/**
	 * Выборка и получение данных справочника на основе функции Arrays::select
	 * @param boolean|integer|string $section Название или идентификатор раздела справочника или false. По умолчанию равно false.
	 * Если равно false, то выборка произойдёт по всем разделам с вынесением названия справочника на первый уровень ассоциативного
	 * массива
	 * @param mixed $param1 Первый параметр или false. По умолчанию равно false
	 * @param mixed $param2 Второй параметр или false. По умолчанию равно false
	 * @param mixed $except Список исключений или false. По умолчанию равно false
	 * @param mixed $only Список необходимых или false. По умолчанию равно false
	 * @param boolean|string $name_conditional Название условного параметра или false. По умолчанию равно false
	 * @param boolean $strict Признак регистрозависимого поиска. По умолчанию равно true
	 * @param boolean $unique Признак уникальных записей в матрице. По умолчанию равно true
	 * @return mixed Выбранные данные справочника или false
	 * @see Arrays::select()
	 */
	public function get($section = false, $param1 = false, $param2 = false, $except = false, $only = false,
		$name_conditional = false, $strict = true, $unique = true){
			if (!$data = $this->getData())
				return false;
			
			$select = $param1 || $except || $only;
			
			if ($section){
				if (is_int($section))
					$section = Arrays::pop($this->_sectionIds, $section);
				
				if (!$data_section = Arrays::pop($data, $section))
					return false;
				
				$result = $select ?
					Arrays::select($data_section, $param1, $param2, $except, $only, $name_conditional, $strict, $unique) : $data_section;
			}elseif ($select)
				foreach ($data as $section=>$data_section)
					$result[$section] =
						Arrays::select($data_section, $param1, $param2, $except, $only, $name_conditional, $strict, $unique);
			else
				$result = $data;
			
			return $result;
		}
	
	/**
	 * Генерация данных таблицы данного справочника в БД
	 * @param string $table Название таблицы
	 * @return boolean Успешность выполнения
	 */
	public function deploy($table){
		if (!$data = $this->data)
			return false;
		
		$rows = array(array('id', 'section', 'code', 'name', 'description'));
		
		foreach ($data as $section=>$conformities)
			foreach ($conformities as $conformity)
				$rows[] = array($conformity['id'], $section, $conformity['code'], $conformity['name'], $conformity['description']);
		
		return Yii::app()->db->createCommand()->insert($table, $rows);
	}
	
	/**
	 * Обработка исходных данных справочника
	 * @param array $data Набор исходных данных
	 * @return array Массив данных справочника
	 */
	protected function preparation($data){
		$result = array();
		
		if ($data)
			if (is_object(reset($data))){
				$section = false;
				
				foreach ($data as $conformity){
					if ($conformity->section !== $section)
						$this->_sectionIds[$conformity->id] = $section = $conformity->section;
					
					$result[$conformity->section][] = array(
						'id'=>$conformity->id,
						'code'=>$conformity->code,
						'name'=>$conformity->name,
						'description'=>$conformity->description
					);
				}
			}else{
				$id = 1;
				
				foreach ($data as $section=>$conformities){
					$this->_sectionIds[$id] = $section;
					
					foreach ($conformities as $code=>$conformity)
						$result[$section][] = array(
							'id'=>$id++,
							'code'=>$code,
							'name'=>$conformity['name'],
							'description'=>$conformity['description']
						);
				}
			}
		
		return $result;
	}
	
}
