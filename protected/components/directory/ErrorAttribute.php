<?php
/**
 * Класс компонента "Справочник ошибок атрибутов"
 * @author v.zubov
 */
class ErrorAttribute extends ComponentDirectory{
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'error_attribute.php';
	
	/**
	 * Выборка и получение данных справочника
	 * @param boolean|CModel|string $model Название или объект модели данных или false. По умолчанию равно false
	 * @param boolean|string $attribute Название атрибута модели или false. По умолчанию равно false
	 * @param boolean|string $validator Название валидатора атрибута или false. По умолчанию равно false
	 * @param boolean|string $feature Название характеристики валидатора атрибута или false. По умолчанию равно 'message'
	 * @return array|boolean|string Выбранные данные справочника или false
	 */
	public function get($model = false, $attribute = false, $validator = false, $feature = 'message'){
		if (!$data = $this->getData())
			return false;
		
		if (!$validator)
			return $data;
		
		if ($model instanceof CModel)
			$model = $model->modelName;
		
		$keys = array("$model.$attribute.$validator", "$model.0.$validator", "0.$attribute.$validator", "0.0.$validator");
		
		foreach ($keys as $key)
			if ($features = Arrays::pop($data, $key))
				break;
		
		if (!$features)
			return false;
		
		if (!$feature)
			return $features;
		
		if (!$result = Arrays::pop($features, $feature))
			return false;
		
		if ($feature == 'message' && mb_strpos($result, '{attribute') !== false)
			$result = str_replace('{attribute}', BaseModel::getAttributeLabelStatic($model, $attribute), $result);
		
		return $result;
	}
	
	/**
	 * Генерация данных таблицы данного справочника в БД
	 * @param string $table Название таблицы
	 * @return boolean Успешность выполнения
	 */
	public function deploy($table){
		if (!$data = $this->data)
			return false;
		
		$rows = array(array('id_attribute', 'model', 'attribute', 'validator', 'message'));
		
		foreach ($data as $model=>$attributes)
			foreach ($attributes as $attribute=>$validators)
				foreach ($validators as $validator=>$features)
					$rows[] = array($features['id_attribute'], $model ? true : null, $attribute ? true : null,
						$validator, $features['message']);
		
		return Yii::app()->db->createCommand()->insert($table, $rows);
	}
	
	/**
	 * Обработка исходных данных справочника
	 * @param array $data Набор исходных данных
	 * @return array Массив данных справочника
	 */
	protected function preparation($data){
		$result = array();
		
		if ($data && ($models = Yii::app()->attribute->get()))
			if (is_object(reset($data))){
				$attr_params = Yii::app()->attribute->getAttributeConformityes();
				
				foreach ($data as $error){
					$model = $attribute = 0;
					
					if ($error->id_attribute){
						if ($attr_param = Arrays::pop($attr_params, $error->id_attribute)){
							if ($error->model)
								$model = $attr_param['model'];
							
							if ($error->attribute)
								$attribute = $attr_param['attribute'];
						}else
							continue;
					}
					
					$result[$model][$attribute][$error->validator] = array(
						'id_attribute'=>$error->id_attribute,
						'message'=>$error->message
					);
				}
			}else
				foreach ($data as $model=>$attributes){
					$params_model = $model ? Arrays::pop($models, $model) : false;
					
					if ($model && !$params_model)
						continue;
					
					foreach ($attributes as $attribute=>$validators){
						$params_attribute = false;
						
						if ($attribute){
							if ($params_model)
								$params_attribute = Arrays::pop($params_model, $attribute);
							else
								foreach ($models as $model_attributes)
									if ($params_attribute = Arrays::pop($model_attributes, $attribute))
										break;
							
							if (!$params_attribute)
								continue;
						}elseif ($params_model)
							$params_attribute = reset($params_model);
						
						foreach ($validators as $validator=>$message)
							$result[$model][$attribute][$validator] = array(
								'id_attribute'=>$params_attribute ? Arrays::pop($params_attribute, 'id') : null,
								'message'=>$message
							);
					}
				}
		
		return $result;
	}
	
}
