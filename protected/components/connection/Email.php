<?php
/**
 * Компонент для работы с Почтовым сервером
 * @author v.zubov
 */
class Email extends ComponentConnection{
	
	/**
	 * @var string Псевдоним пути к каталогу почтового агента
	 */
	const DIR_AGENT = 'application.extensions.mailer';
	
	/**
	 * @var string Название класса почтового агента
	 */
	const NAME_AGENT = 'EMailer';
	
	/**
	 * @var boolean Значение параметра isHtml почтового агента по умолчанию
	 */
	const IS_HTML_DEF = true;
	
	/**
	 * @var boolean Значение параметра smtpDebug почтового агента по умолчанию
	 */
	const SMTP_DEBUG_DEF = false;
	
	/**
	 * @var boolean Значение параметра smtpAuth почтового агента по умолчанию
	 */
	const SMTP_AUTH_DEF = true;
	
	/**
	 * @var integer Код ошибки отправки сообщения "Без ошибок"
	 */
	const	ERROR_NONE = 0;
	
	/**
	 * @var integer Код ошибки отправки сообщения "Не удалось инициализировать почтовый агент"
	 */
	const	ERROR_INIT = 1;
	
	/**
	 * @var integer Код ошибки отправки сообщения "Неверный адрес сервера"
	 */
	const	ERROR_HOST = 2;
	
	/**
	 * @var integer Код ошибки отправки сообщения "Неверная авторизация"
	 */
	const	ERROR_AUTH = 3;
	
	/**
	 * @var integer Код ошибки отправки сообщения "Ошибка отправки"
	 */
	const	ERROR_SEND = 4;
	
	/**
	 * @var integer Код ошибки отправки сообщения "Не указан адресат"
	 */
	const	ERROR_ADDRESS = 5;
	
	/**
	 * @var integer Код ошибки отправки сообщения "Некорректный адресат"
	 */
	const	ERROR_ADDRESS_CORRECT = 6;
	
	/**
	 * @var object Экземпляр активного экземпляра почтового агента
	 */
	private $_client;
	
	/**
	 * Метод-геттер для определения _client
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return null|object Экземпляр активного экземпляра почтового агента или null в случае ошибки
	 */
	public function getClient($fix = true){
		if ($this->_client === null){
			if ($fix)
				Yii::app()->log->fix('email_handling', array('on'=>$this->on), $this->on ? 'ok' : 'agent_handling');
			
			if ($this->on){
				$path = Yii::getPathOfAlias(self::DIR_AGENT).'/'.self::NAME_AGENT.'.'.Swamper::EXT_PHP;
				
				if (is_file($path)){
					require $path;
					
					$fn = self::NAME_AGENT;
					$client = $this->_client = new $fn;
					$cfg = $this->_cfg;
					
					if ($client && $cfg){
						$client->IsSMTP();
						
						if (in_array($cfg['email_protocol'], array('ssl', 'tls')))
							$client->SMTPSecure = $cfg['email_protocol'];
						
						$client->IsHTML(self::IS_HTML_DEF);
						$client->SMTPDebug = self::SMTP_DEBUG_DEF;
						$client->SMTPAuth = self::SMTP_AUTH_DEF;
						$client->CharSet = Yii::app()->charset;
						$client->FromName = Yii::app()->name;
						
						$client->Host = $cfg['host'];
						$client->Port = $cfg['port'];
						$client->Username = $client->From = $cfg['login'];
						$client->Password = $cfg['password'];
					}
				}
				
				if ($fix)
					Yii::app()->log->fix('email_init', array('path'=>$path), !empty($client) ? 'ok' : 'email_init');
			}
		}
		
		return $this->_client;
	}
	
	/**
	 * Отправка e-mail сообщения
	 * @param array|string $addresses Адрес или спосок адресов назначения
	 * @param string $view Название файла представления для генерации HTML-кода сообщения
	 * @param array $params Массив параметров представления. По умолчанию равно array()
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return integer Код ошибки отправки сообщения
	 */
	public function send($addresses, $view, $params = array(), $fix = true){
		$params_fix = array(
			'addresses'=>$addresses,
			'view'=>$view,
			'params'=>$params
		);
		$result = self::ERROR_NONE;
		
		try{
			if (!$addresses){
				$result = self::ERROR_ADDRESS;
				throw new CHttpException(500, 'Empty address');
			}
			
			if (!$client = $this->getClient($fix)){
				$result = self::ERROR_INIT;
				throw new CHttpException(500);
			}
			
			$client->Subject = Yii::app()->label->get('etitle_'.$view);
			$client->getView($view, $params, 'main');
			
			if ($attachments = Arrays::pop($params, 'attachment'))
				foreach ((array)$attachments as $attachment)
					if (is_file($attachment))
						$client->addAttachment($attachment);
			
			ob_start();
				if (is_array($addresses))
					foreach ($addresses as $address)
						$client->AddAddress($address);
				else
					$client->AddAddress($addresses);
				
				$client->Send();
			$msg = ob_get_clean();
			
			if ($msg)
				throw new CHttpException(500, $msg);
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
			
			if (mb_strpos($params_fix['msg'], 'Invalid address') === 0)
				$result = self::ERROR_ADDRESS_CORRECT;
			else{
				$msg_paths = explode(' ', $params_fix['msg']);
				
				if ($result == self::ERROR_NONE)
					$result = self::ERROR_SEND;
				
				if (count($msg_paths) > 4){
					$msg = mb_substr($msg_paths[4], 0, 4);
					
					if ($msg === 'conn')
						$result = self::ERROR_HOST;
					elseif ($msg === 'auth')
						$result = self::ERROR_AUTH;
				}
			}
		}
		
		$params_fix['result'] = $result;
		
		if ($fix)
			Yii::app()->log->fix('email_send', $params_fix, $result === self::ERROR_NONE ? 'ok' : 'email_send');
		
		return $result;
	}
	
}
