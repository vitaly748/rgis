<?php
/**
 * Компонент для работы с SMS-шлюзом с подключением через веб-сервис
 * @author v.zubov
 */
class Phone extends ComponentConnection{
	
	/**
	 * @var string Шаблон веб-адреса веб-сервиса подключения к SMS-шлюзу
	 */
	const PATTERN_URL = '%s/sms/json.php';
	
	/**
	 * @var string Веб-адрес веб-сервиса
	 */
	private $_url;
	
	/**
	 * Установка данных конфигурации
	 * @param array $cfg Ассоциативный массив параметров
	 * @see ComponentConnection::set()
	 */
	public function set($cfg){
		parent::set($cfg);
		$this->_url = null;
	}
	
	/**
	 * Метод-геттер для определения _url
	 * @return string Веб-адрес веб-сервиса
	 */
	public function getUrl(){
		if ($this->_url === null){
			$this->_url = false;
			
			if ($host = $this->_cfg['host']){
				if ($this->_cfg['port'])
					$host .= ':'.$this->_cfg['port'];
				
				$this->_url = sprintf(self::PATTERN_URL, $host);
			}
		}
		
		return $this->_url;
	}
	
	/**
	 * Выполнение запроса к SMS-шлюзу посредством веб-сервиса
	 * @param string $function Название вызываемой функции
	 * @param array|boolean $params Массив параметров функции или false. По умолчанию равно false
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return array|boolean Ассоциативный массив с данными из ответа SMS-шлюза или false в случае ошибки
	 */
	public function query($function, $params = false, $fix = true){
		if ($fix)
			Yii::app()->log->fix('phone_handling', array('on'=>$this->on), $this->on ? 'ok' : 'agent_handling');
		
		$params_fix = array('function'=>$function, 'params'=>$params);
		
		try{
			if ($url = $this->formingUrl($function, $params))
				if ($result = @file_get_contents($url))
					$result = json_decode($result, true);//stripslashes
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !empty($result) && isset($result[0]['code']);
		
		if ($fix)
			Yii::app()->log->fix('phone_request', $params_fix, $ok ? 'ok' : 'agent_request');
		
		return $ok ? $result : false;
	}
	
	/**
	 * Формирование веб-адреса запроса к SMS-шлюзу
	 * @param string $function Название вызываемой функции
	 * @param array|boolean Список параметров или false. По умолчанию равно false
	 * @return boolean|string Веб-адрес запроса или false в случае ошибки
	 */
	public function formingUrl($function, $params = false){
		$params_url = array('method'=>$function);
		
		if ($params)
			$params_url['argument'] = json_encode($params);
		
		return ($url = $this->url) ? Html::formingUrl($url, $params_url): false;
	}
	
}
