<?php
/**
 * Компонент для работы с ФИАС-агентом
 * @author v.zubov
 */
class Fias extends ComponentConnection{
	
	/**
	 * @var string HTTP-адрес ФИАС-сервиса
	 */
	const URL = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs';
	
	/**
	 * @var string API-ключ
	 */
	const TOKEN = '7ee20fc5f7083873893429222cd654797c8a49f9';
	
	/**
	 * @var string Значение параметра 'type' запроса к сервису
	 */
	const TYPE = 'ADDRESS';
	
	/**
	 * @var integer Значение параметра 'count' запроса к сервису
	 */
	const COUNT = 5;
	
	/**
	 * @var boolean Признак наличия скриптов ФИАС-агента на странице
	 */
	protected $_active;
	
	/**
	 * Метод-геттер для определения _active
	 * @return boolean Признак наличия скриптов ФИАС-агента на странице
	 */
	public function getActive(){
		return $this->_active;
	}
	
	/**
	 * Активация запуска скриптов ФИАС-агента на странице
	 * @return boolean Признак успешной активации
	 */
	public function activate(){
		if ($this->_active = $this->getOn())
			Html::registerScriptParams('fiasParams', false, false, array(
				'url'=>static::URL,
				'token'=>static::TOKEN,
				'type'=>static::TYPE,
				'cout'=>static::COUNT,
				'post'=>(boolean)$_POST
			));
		
		return $this->_active;
	}
	
}
