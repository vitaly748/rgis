<?php
/**
 * Компонент для работы со старой версией Биллингового центра капремонта с подключением через веб-сервис
 * @author v.zubov
 */
class BssOld extends Component{
	
	/**
	 * @var string Шаблон веб-адреса веб-сервиса подключения к Биллинговому центру
	 */
	const PATTERN_URL = '192.168.60.26/lkhs/hs/lk';
	
	/**
	 * Конструктор компонента
	 */
	public function init(){
	}
	
	/**
	 * Выполнение запроса к Биллинговому центру посредством веб-сервиса
	 * @param string $function Название вызываемой функции
	 * @param array|boolean $params Массив параметров функции или false. По умолчанию равно false
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return array|boolean Ассоциативный массив с данными из ответа Биллингового цента или false в случае ошибки
	 * @throws CHttpException
	 */
	public function query($function, $params = false){
		if ($url = $this->formingUrl($function, $params))
			if ($result = @file_get_contents($url))
				$result = json_decode($result, true);
		
		$ok = !empty($result) && isset($result['error']['code']);
		
		return $ok ? $result : false;
	}
	
	/**
	 * Формирование веб-адреса запроса к Биллинговому центру
	 * @param string $function Название вызываемой функции
	 * @param array|boolean Список параметров или false. По умолчанию равно false
	 * @return boolean|string Веб-адрес запроса или false в случае ошибки
	 */
	public function formingUrl($function, $params = false){
		$params_url = array('function'=>$function);
		
		if ($params)
			$params_url = array_merge($params_url, $params);
		
		return ($url = static::PATTERN_URL) ? Html::formingUrl($url, $params_url): false;
	}
	
}
