<?php
/**
 * Компонент для работы с Единой системой идентификации и авторизации
 * @author v.zubov
 */
class Esia extends ComponentConnection{

	/**
	 * @var boolean Признак работы компонента в режиме эмуляции
	 */
	const EMULATION = false;

	/**
	 * @var string Псевдоним пути к файлу ответа в режиме эмуляции
	 */
	const DIR_ANSWER = 'application.config.test';
	
	/**
	 * @var string пмя файла ответа в режиме эмуляции
	 */
	const FN_ANSWER = 'esia_answer_eb1.php';
	
	/**
	 * @var string Название расширения, предоставляющего сервис для работы по стандарту Simple Saml 2
	 */
	const EXTENSION_NAME = 'simplesamlphp';
	
	/**
	 * @var string Идентификатор блока настроек расширения
	 */
	const CONNECTION_ID = 'esia';
	
	/**
	 * @var string Псевдоним пути к папке, содержащей файл автозагрузчика
	 */
	const DIR_AUTOLOAD = 'lib';
	
	/**
	 * @var string Псевдоним пути к папке, содержащей инициативные файлы провайдера услуг расширения
	 */
	const DIR_SP = 'modules.saml.www.sp';
	
	/**
	 * @var string Имя файла автозагрузчика
	 */
	const FN_AUTOLOAD = '_autoload.php';
	
	/**
	 * @var string Имя файла расширения, осуществляющего авторизацию в ЕСИА
	 */
	const FN_LOGIN = 'saml2-acs.php';
	
	/**
	 * @var string Имя файла расширения, осуществляющего выход из ЕСИА
	 */
	const FN_LOGOUT = 'saml2-logout.php';
	
	/**
	 * @var string Название поля, содержащего идентификатор пользователя, в ответе ЕСИА
	 */
	const FIELD_ID = 'urn:mace:dir:attribute:userId';
	
	/**
	 * @var string Название поля, содержащего глобальную роль пользователя, в ответе ЕСИА
	 */
	const FIELD_ROLE = 'urn:esia:globalRole';
	
	/**
	 * @var string Название поля, содержащего email пользователя, в ответе ЕСИА
	 */
	const FIELD_EMAIL = 'urn:esia:personEMail';
	
	/**
	 * @var string Название поля, содержащего фамилию пользователя, в ответе ЕСИА
	 */
	const FIELD_SURNAME = 'urn:mace:dir:attribute:lastName';
	
	/**
	 * @var string Название поля, содержащего имя пользователя, в ответе ЕСИА
	 */
	const FIELD_NAME = 'urn:mace:dir:attribute:firstName';
	
	/**
	 * @var string Название поля, содержащего отчество пользователя, в ответе ЕСИА
	 */
	const FIELD_PATRONYMIC = 'urn:mace:dir:attribute:middleName';
	
	/**
	 * @var string Название поля, содержащего краткое название организации пользователя, в ответе ЕСИА
	 */
	const FIELD_TITLE = 'urn:esia:orgShortName';
	
	/**
	 * @var string Название поля, содержащего номер мобильного телефона пользователя, в ответе ЕСИА
	 */
	const FIELD_PHONE = 'urn:esia:personMobilePhone';
	
	/**
	 * @var string Название поля, содержащего СНИЛС пользователя, в ответе ЕСИА
	 */
	const FIELD_SNILS = 'urn:esia:personSNILS';
	
	/**
	 * @var string Ассоциативный массив соответствий глобальных ролей ЕСИА и ролей пользователей данного приложения в формате:
	 * 	array(
	 * 		{Символьный идентификатор глобальной роли пользователя ЕСИА}=>{Символьный идентификатор роли пользователя приложения},
	 * 		...
	 * 	)
	 */
	const ROLE_CONFORMITY = array('P'=>'citizen', 'E'=>'organization');
	
	/**
	 * Инициализация расширения
	 * @throws CHttpException
	 */
	public function initExtension(){
		Yii::app()->log->fix('esia_handling', array('class'=>get_class($this), 'on'=>$this->on),
			$this->on ? 'ok' : 'agent_handling');
		
		if (!$this->on)
			throw new CHttpException(500, 'error handling');
		
		require_once(Yii::getPathOfAlias('ext.'.static::EXTENSION_NAME.'.'.static::DIR_AUTOLOAD).
			DIRECTORY_SEPARATOR.static::FN_AUTOLOAD);
		
		YiiBase::registerAutoloader('SimpleSAML_autoload', true);
	}
	
	/**
	 * Действие по авторизации через сервис ЕСИА
	 */
	public function login(){
		$params_fix = array();
		
		try{
			$this->initExtension();
			
			if (static::EMULATION){
				$path = Yii::getPathOfAlias(static::DIR_ANSWER).DIRECTORY_SEPARATOR.static::FN_ANSWER;
				
				if (!is_file($path))
					throw new CHttpException(500, 'file answer not found');
				
				$result = require_once $path;
			}else{
				if ($_POST){
					$params_fix['post'] = $_POST;
					$params_fix['path_handler'] = Yii::getPathOfAlias('ext.'.static::EXTENSION_NAME.'.'.static::DIR_SP).
						DIRECTORY_SEPARATOR.static::FN_LOGIN;
					$_SERVER['PATH_INFO'] = '/'.static::CONNECTION_ID;
					
					require_once($params_fix['path_handler']);
					
					throw new CHttpException(500, 'error auth start');
				}
				
				$auth = new SimpleSAML_Auth_Simple(static::CONNECTION_ID);
				$auth->requireAuth();
				
				if (!$auth->isAuthenticated())
					throw new CHttpException(500, 'error require auth');
				
				$result = $auth->getAttributes();
			}
			
			$params_fix['result'] = $result;
			$params_fix['esia_id'] = $id = Arrays::pop($result, static::FIELD_ID.'.0');
			$params_fix['esia_role'] = $role_esia = Arrays::pop($result, static::FIELD_ROLE.'.0');
			
			if (!$id)
				throw new CHttpException(500, 'field id not found');
			
			if (!$role_esia)
				throw new CHttpException(500, 'field role not found');
			
			$params_fix['role'] = $role = Arrays::pop($rc = static::ROLE_CONFORMITY, $role_esia);
			
			if (!$role)
				throw new CHttpException(500, 'role incorrect');
			
/* 			$model = UserModel::model()->with('conjugation')->find('id_external = :id_external AND id_role = :id_role',
				array(':id_external'=>$id, ':id_role'=>Yii::app()->user->getRoles(0, 'id', 'esia'))); */
			$model = false;
			
			if ($model)
				$params_fix['model_state'] = 'found';
			else{
				if (!$email = Arrays::pop($result, static::FIELD_EMAIL.'.0'))
					throw new CHttpException(404, 'field email not found');
				
				$params_fix['esia_email'] = $email = mb_strtolower($email);
				
				$model = UserModel::model()->find('email = :email', array(':email'=>$email));
				
				if ($model)
					$params_fix['model_state'] = 'conjugation';
				else{
					$params_fix['model_state'] = 'creation';
					$model = new UserModel;
				}
			}
			
			if ($params_fix['model_state'] !== 'creation' &&
				!in_array(Yii::app()->conformity->get('user_state', 'name', $model->state), array('confirm_data', 'working')))
					throw new CHttpException(403, 'user locked');
			
			if ($params_fix['model_state'] != 'found'){
				$transaction = Yii::app()->db->beginTransaction();
				
				if ($params_fix['model_state'] == 'creation')
					$attributes = array(
						'state'=>Yii::app()->conformity->get('user_state', 'code',
							$params_fix['model_state'] == 'creation' ? 'confirm_data' : 'working'),
						'email'=>$email,
						'registration'=>Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM),
						'delivery'=>Yii::app()->conformity->get('user_delivery_type', 'code', 'email'),
						'agreement'=>false,
						'secure'=>true
					);
				
				foreach (array('surname', 'name', 'patronymic', 'title', 'phone', 'snils') as $attribute){
					$const = 'static::FIELD_'.mb_strtoupper($attribute);
					
					if (defined($const) && ($value = Arrays::pop($result, constant($const).'.0')))
						$attributes[$attribute] = $value;
				}
				
				if (in_array($id, array('169199', '1022329580')))
					$attributes['snils'] = '03831156134';
				
				$model->setAttributes($attributes, false);
				$model->removeDecoration(false, true, false);
				
				if (!$model->save(false))
					throw new CHttpException(500, 'error model save');
				
				$assign_base_role = $params_fix['model_state'] == 'creation' ||
					!Yii::app()->user->authManager->checkAccess($role, $model->id);
				
				if ($params_fix['model_state'] == 'conjugation' && $assign_base_role &&
					($role_adjacent = reset(array_diff(static::ROLE_CONFORMITY, array($role)))) &&
					Yii::app()->user->authManager->checkAccess($role_adjacent, $model->id) &&
					!Yii::app()->user->revoke($role_adjacent, $model->id))
						throw new CHttpException(500, 'error revoke adjacent role');
				
				if ($assign_base_role && !Yii::app()->user->assign($role, $model->id))
					throw new CHttpException(500, 'error assign base role');
				
				if (($params_fix['model_state'] == 'creation' || !Yii::app()->user->authManager->checkAccess('esia', $model->id)) &&
					!Yii::app()->user->assign('esia', $model->id, $id))
						throw new CHttpException(500, 'error assign role esia');
			}
			
			$params_fix['user'] = $model->getAttributes($model->classAttributeNames);
			$login = $params_fix['login'] = Yii::app()->user->checkLogin($model);
			Yii::app()->user->setState('esia', $model->id);
			
			if (!empty($transaction))
				$transaction->commit();
		}catch (CHttpException $e){
			$params_fix['msg'] = $e->getMessage();
			
			if (!empty($transaction))
				$transaction->rollback();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('esia_login', $params_fix, $ok ? 'ok' : ($e->statusCode == 403 ? 'esia_locked' :
			($e->statusCode == 404 ? 'esia_email' : 'esia_login')), !empty($model) ? $model->id : null);
		
		Yii::app()->controller->redirect(Yii::app()->user->getHomeUrl($ok && !empty($model) && $login ? $model->id : false));
	}
	
	/**
	 * Действие по разавторизации через сервис ЕСИА
	 * @throws CHttpException
	 */
	public function logout(){
		if (!Yii::app()->user->isGuest)
			$user = Yii::app()->user->model;
		else{
			if ((!$id = Yii::app()->user->getState('esia')) || !is_int($id))
				throw new CHttpException(500);
			
			$user = UserModel::model()->findByPk($id);
			
			Yii::app()->log->fix('model_search', array('model'=>'user', 'id'=>$id), $user ? 'ok' : 'model_search');
		}
		
		$params_fix = array('user'=>$user ? Arrays::filterKey($user->attributes, 'password', false) : false);
		
		if (static::EMULATION){
			Yii::app()->user->setState('esia', $params_fix);
			Yii::app()->controller->refresh();
		}
		
		try{
			$this->initExtension();
			
			if ($_GET){
				$params_fix['get'] = $_GET;
				$params_fix['get_type'] = isset($_GET['SAMLResponse']) ? 'response' : (isset($_GET['SAMLRequest']) ?
					'request' : 'unknown');
				
				if ($params_fix['get_type'] == 'unknown')
					throw new CHttpException(500, 'unknown request');
				
				$params_fix['path_handler'] = Yii::getPathOfAlias('ext.'.static::EXTENSION_NAME.'.'.static::DIR_SP).
					DIRECTORY_SEPARATOR.static::FN_LOGOUT;
				$_SERVER['PATH_INFO'] = '/'.static::CONNECTION_ID;
				
				if ($params_fix['get_type'] == 'response'){
					$obj = unserialize($_SESSION['SimpleSAMLphp_SESSION']);
					$_SERVER['QUERY_STRING'] .= '&RelayState='.end(array_keys($obj->getDataOfType('SimpleSAML_Auth_State')));
					
					$params_fix['query_string'] = $_SERVER['QUERY_STRING'];
				}
				
				Yii::app()->user->setState('esia', $params_fix);
				
				require_once($params_fix['path_handler']);
				
				throw new CHttpException(500, 'error auth end');
			}
			
			$auth = new SimpleSAML_Auth_Simple(static::CONNECTION_ID);
			
			if (!$auth->isAuthenticated()){
				Yii::app()->user->setState('esia', $params_fix);
				
				throw new CHttpException(500, 'error authenticated');
			}
			
			$auth->logout('/');
			
			throw new CHttpException(500, 'error start logout');
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('esia_logout', $params_fix, $ok ? 'ok' : 'esia_logout');
		
		Yii::app()->controller->redirect(Yii::app()->user->getHomeUrl());
	}
	
	/**
	 * Действие по отключению текущего пользователя от ЕСИА
	 * @result boolean Успешность выполнения
	 */
	public function done(){
		$params_fix = array();
		
		try{
			if (!$model = Yii::app()->user->model)
				throw new CHttpException(500, 'not authorized');
			
			if ((!$conjugations = $model->conjugations) || !isset($conjugations['esia']))
				throw new CHttpException(500, 'role esia not found');
			
			$params_fix['esia_id'] = $esia_id = reset(array_keys($conjugations['esia']));
			
			if (Yii::app()->user->revoke('esia', $model->id, $esia_id) === false)
				throw new CHttpException(500, 'error revoke');
			
			Yii::app()->user->setState('esia', null);
		}catch (CHttpException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('esia_done', $params_fix, $ok ? 'ok' : 'esia_done');
		
		return $ok;
	}
	
}
