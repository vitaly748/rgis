<?php
/**
 * Компонент для работы с Биллинговым центром ЖКХ с подключением через веб-сервис
 * @author v.zubov
 */
class Bss1 extends ComponentConnection{
	
	/**
	 * @var boolean Признак работы компонента в режиме эмуляции
	 */
	const EMULATION = false;
	
	/**
	 * @var string Псевдоним пути к файлу конфигурации тестов функций обмена данными с Биллинговым центром
	 */
	const DIR_CFG = 'application.config';

	/**
	 * @var string Псевдоним пути к папке тестов
	 */
	const DIR_TEST = 'application.config.test';
	
	/**
	 * @var string Имя файла конфигурации тестов
	 */
	const FN_TEST = 'bss_test.php';
	
	/**
	 * @var string Имя файла параметров ответов на bss-запросы
	 */
	const FN_ANSWER = 'bss_answer.php';
	
	/**
	 * @var string Имя файла с набором данных для генерации
	 */
	const FN_GENERATION = 'generation.php';
	
	/**
	 * @var string Шаблон веб-адреса веб-сервиса подключения к Биллинговому центру
	 */
	const PATTERN_URL = '%s/%s/hs/rgis';
	
	/**
	 * @var integer Код типа потребителя "Плательщик"
	 */
	const CONSUMER_TYPE_PAYER = 1;
	
	/**
	 * @var integer Код типа потребителя "Собственник"
	 */
	const CONSUMER_TYPE_OWNER = 2;
	
	/**
	 * @var integer Код типа субъекта права "Физическое лицо"
	 */
	const LAW_TYPE_NATURAL = 1;
	
	/**
	 * @var integer Код типа субъекта права "Юридическое лицо"
	 */
	const LAW_TYPE_LEGAL = 2;
	
	/**
	 * @var integer Код типа лицевого счёта "ЖКХ"
	 */
	const ACCOUNT_TYPE_BSS1 = 1;
	
	/**
	 * @var integer Код типа лицевого счёта "Капремонт"
	 */
	const ACCOUNT_TYPE_BSS2 = 2;
	
	/**
	 * @var integer Код вида лицевого счёта Капремонта "Котловой"
	 */
	const ACCOUNT_KIND_BSS2_BOILER = 1;
	
	/**
	 * @var integer Код вида лицевого счёта Капремонта "Специальный РО"
	 */
	const ACCOUNT_KIND_BSS2_SPECIAL_OPERATOR = 2;
	
	/**
	 * @var integer Код вида лицевого счёта Капремонта "Специальный ТСЖ"
	 */
	const ACCOUNT_KIND_BSS2_SPECIAL_ORGANIZATION = 3;
	
	/**
	 * @var integer Код вида лицевого счёта Капремонта "Заблокированный"
	 */
	const ACCOUNT_KIND_BSS2_LOCKED = 4;
	
	/**
	 * @var string Префикс генератора в файле параметров ответов
	 */
	const PREFIX_GENERATOR = '!';
	
	/**
	 * @var string Ключ элемента с названием генератора в файле параметров ответов
	 */
	const KEY_GENERATOR = 'generator';
	
	/**
	 * @var string Ключ элемента с определением фиксированных значений в файле параметров ответов
	 */
	const KEY_FIXED = 'fixed';
	
	/**
	 * @var string Разделитель параметров запроса в ключе-идентификаторе фиксированных значений в файле параметров ответов
	 */
	const FIXED_DELIMITER = '&';
	
	/**
	 * @var array Список названий функций, которые не будут подвергаться кэшированию
	 */
	const FUNCTIONS_NO_CACHING = array('test', 'code_drop', 'reading_add');
	
	/**
	 * @var string Идентификатор компонента приложения, работающий с кэшем
	 */
	public $cacheID = 'cache';
	
	/**
	 * @var integer Время хранения данных в кэше
	 */
	public $cachingDuration;
	
	/**
	 * @var string Веб-адрес веб-сервиса
	 */
	protected $_url;
	
	/**
	 * @var CCache Активный компонент приложения, работающий с кэшем
	 */
	protected $_cache;
	
	/**
	 * @var array Параметры ответов
	 */
	protected $_paramsAnswer;
	
	/**
	 * @var array Ассоциативный массив данных для генерации
	 */
	protected $_generationData;
	
	/**
	 * @var array Параметры генераторов
	 */
	protected $_paramsGenerators = array(
		'date'=>array(// Генератор даты
			'format'=>'Y-m-d H:i:s',// Формат даты
			'current'=>false,// Признак текущей даты
			'min'=>'2015-01-01 00:00:00',// Минимально допустимая дата
			'max'=>'current'// Максимально допустимая дата
		),
		'list'=>array(// Генератор списка
			'qu'=>false,// Количество элементов списка
			'min'=>0,// Минимально допустимое количество
			'max'=>5,// Максимально допустимое количество
			'data'=>false,// Список вложенных параметров
			'sort'=>false,// Название атрибута, по которому нужно отсортировать список
			'sort_desc'=>false,// Признак сортировки по убыванию
			'unique'=>false// Список названий вложенных атрибутов, требующих уникальность значения
		),
		'number'=>array(// Генератор числа
			'min'=>0,// Минимально допустимая цифра
			'max'=>5000,// Максимально допустимая цифра
			'float'=>false,// Признак числа с плавающей точкой
			'long'=>false,// Признак длинного числа
		),
		'phrase'=>array(// Генератор фраз
			'set'=>false,// Название группы значений из файла данных для генерации
			'sequence'=>false// Последовательсть значений, объединяемых в строку
		),
		'variant'=>array(// Генератор вариантов
			'variable'=>false,// Название переменной, содержащей ключ требуемого варианта
			'expression'=>false,// Выражение, возвращающее ключ требуемого варианта
			'variants'=>false// Ассоциативный массив вариантов
		)
	);
	
	/**
	 * Установка данных конфигурации
	 * @param array $cfg Ассоциативный массив параметров
	 * @see ComponentConnection::set()
	 */
	public function set($cfg){
		parent::set($cfg);
		$this->_url = null;
	}
	
	/**
	 * Метод-геттер для определения _url
	 * @return string Веб-адрес веб-сервиса
	 */
	public function getUrl(){
		if ($this->_url === null){
			$this->_url = false;
			
			if (($host = $this->_cfg['host']) && ($name = $this->_cfg['name'])){
				if ($this->_cfg['port'])
					$host .= ':'.$this->_cfg['port'];
				
				$this->_url = sprintf(static::PATTERN_URL, $host, $name);
			}
		}
		
		return $this->_url;
	}
	
	/**
	 * Метод-геттер для определения _paramsAnswer
	 * @return array Параметры ответов
	 */
	public function getParamsAnswer(){
		if ($this->_paramsAnswer === null){
			$this->_paramsAnswer = false;
			
			if (is_file($path = Yii::getPathOfAlias(static::DIR_CFG).DIRECTORY_SEPARATOR.static::FN_ANSWER))
				$this->_paramsAnswer = require $path;
		}
		
		return $this->_paramsAnswer;
	}
	
	/**
	 * Метод-геттер для определения _generationData
	 * @return array Ассоциативный массив данных для генерации
	 */
	public function getGenerationData(){
		if ($this->_generationData === null){
			$this->_generationData = false;
			
			if (is_file($path = Yii::getPathOfAlias(static::DIR_CFG).DIRECTORY_SEPARATOR.static::FN_GENERATION))
				$this->_generationData = require $path;
		}
		
		return $this->_generationData;
	}
	
	/**
	 * Выполнение запроса к Биллинговому центру посредством веб-сервиса
	 * @param string $function Название вызываемой функции
	 * @param array|boolean $params Массив параметров функции или false. По умолчанию равно false
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true
	 * @return array|boolean Ассоциативный массив с данными из ответа Биллингового цента или false в случае ошибки
	 * @throws CHttpException
	 */
	public function query($function, $params = false, $fix = true){
		if ($fix)
			Yii::app()->log->fix('bss_handling', array('class'=>get_class($this), 'on'=>$this->on), $this->on ? 'ok' : 'agent_handling');
		
		$params_fix = array('component'=>mb_strtolower(get_class($this)), 'function'=>$function, 'params'=>$params,
			'emulation'=>static::EMULATION);
		
		try{
			if (($params_answer = Arrays::pop($this->getParamsAnswer(), $function)) === null)
				throw new CHttpException(500, 'function not found');
			else
				$fixed_answers = Arrays::pop($params_answer, static::KEY_FIXED, true);
			
			if ($this->cacheID && $this->cachingDuration && ($cache = Yii::app()->getComponent($this->cacheID)) !== null)
				if (!in_array($function, static::FUNCTIONS_NO_CACHING)){
					$cacheKey = constant('Cache::PREFIX_'.mb_strtoupper($params_fix['component'])).$function;
					
					foreach ($params as $param)
						$cacheKey .= sprintf(Cache::PATTERN_PARAM, preg_replace('/[\*\?<>\\\|\/:"]/u', '', $param));
					
					$result = $cache->getExt($params_fix['component'], $cacheKey);
				}elseif (array_key_exists($function, FileCache::PATTERN_FN_CLEANING) &&
					!$cache->cleaning($params_fix['component'], $function, $params))
						throw new CHttpException(500, 'cache_cleaning');
			
			if (!isset($cacheKey) || $result === false){
				if (static::EMULATION){
					if ($fixed_answers && $params && ($answer = Arrays::pop($fixed_answers, implode(static::FIXED_DELIMITER, $params))))
						$result = $answer;
					else
						$result = $this->generateAnswer($params_answer);
				}else{
					if (!$url = $this->formingUrl($function, $params))
						throw new CHttpException(500, 'forming url error');
					
					$params_fix['url'] = $url;
					
					if (!Html::hostExists($url))
						throw new CHttpException(500, 'host not found');
					
					if ($result = @file_get_contents($url))
						$result = json_decode($result, true);
				}
				
				if (isset($cacheKey))
					$cache->setExt($params_fix['component'], $cacheKey, $result,
						$function == 'accrual_house' ? 60 * 60 * 24 * 3 : $this->cachingDuration);
			}else
				$params_fix['caching'] = true;
			
			$params_fix['result'] = $result;
			
			if (!$result)
				throw new CHttpException(500, 'empty result');
			
			if (!isset($result['error']['code']))
				throw new CHttpException(500, 'error code not found');
			
			if (!$result['error']['code'] && ($diff = array_diff(array_keys($params_answer), array_keys($result))))
				throw new CHttpException(500, 'attribute "'.array_shift($diff).'" not found');
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('bss_request', $params_fix, $ok ? 'ok' :
				($params_fix['msg'] == 'cache_cleaning' ? 'cache_cleaning' : 'agent_request'));
		
		return $ok ? $result : false;
	}
	
	/**
	 * Формирование веб-адреса запроса к Биллинговому центру
	 * @param string $function Название вызываемой функции
	 * @param array|boolean Список параметров или false. По умолчанию равно false
	 * @return boolean|string Веб-адрес запроса или false в случае ошибки
	 */
	public function formingUrl($function, $params = false){
		$params_url = array('function'=>$function);
		
		if ($params)
			$params_url = array_merge($params_url, $params);
		
		return ($url = $this->url) ? Html::formingUrl($url, $params_url): false;
	}
	
	/**
	 * Тестирование функций обмена данными с Биллинговым центром
	 */
	public function test(){
		$path = Yii::getPathOfAlias(static::DIR_TEST).DIRECTORY_SEPARATOR.static::FN_TEST;
		
		if (is_file($path))
			if ($tests = require $path)
				foreach ($tests as $test)
					if (!empty($test[0])){
						$params = !empty($test[1]) ? $test[1] : false;
						$result = !empty($test[2]) ? $this->formingUrl($test[0], $params) : $this->query($test[0], $params, false);
						
						Arrays::printPre($test[0]);
						Arrays::printPre($result, false, false, Arrays::PRINT_PRE_BORDER_BOTTOM);
					}
	}
	
	/**
	 * Генерация ответа на bss-запрос
	 * @param array|string $params_answer Ассоциативный массив параметров ответа функции или название вызываемой функции
	 * @param boolean $recursion Признак рекурсивного вызова. По умолчанию равно false
	 * @param array $result Расчитанные параметры ответа на предыдущем уровне рекурсии. По умолчанию равно false
	 * @return array Ассоциативный массив с атрибутами ответа
	 */
	protected function generateAnswer($params_answer, $recursion = false, $result_parent = false){
		if (is_string($params_answer))
			$params_answer = Arrays::pop($this->getParamsAnswer(), $params_answer);
		
		if (!is_array($params_answer))
			return false;
		
		$result = array();
		
		foreach ($params_answer as $name=>$params){
			$generator = $generator_params = false;
			
			if (is_string($params)){
				if (mb_strpos($params, static::PREFIX_GENERATOR) === 0)
					$generator = mb_substr($params, 1);
			}elseif (is_array($params) && isset($params[static::KEY_GENERATOR])){
				$generator = $params[static::KEY_GENERATOR];
				$generator_params = $params;
			}
			
			if (!$generator)
				$result[$name] = is_array($params) ? $this->generateAnswer($params, true, $result) : $params;
			else{
				if ($generator_params_def = Arrays::pop($this->_paramsGenerators, $generator))
					$generator_params = $generator_params ? array_merge($generator_params_def, $generator_params) : $generator_params_def;
				elseif (!$generator_params && ($generation_data = $this->getGenerationData()) && isset($generation_data[$generator]) &&
					($generator_params_def = Arrays::pop($this->_paramsGenerators, 'phrase'))){
						$generator_params = array_merge($generator_params_def, array('set'=>$generator));
						$generator = 'phrase';
					}else
						continue;
				
				switch ($generator){
					case 'date':
						$current = time();
						
						if ($generator_params['current'])
							$result[$name] = date($generator_params['format'], $current);
						else{
							$min = $generator_params['min'] === 'current' ? $current : strtotime($generator_params['min']);
							$max = $generator_params['max'] === 'current' ? $current : strtotime($generator_params['max']);
							$result[$name] = date($generator_params['format'], rand($min, $max));
						}
						
						break;
					case 'list':
						$items = $unique = array();
						$qu = $generator_params['qu'] ? $generator_params['qu'] : rand($generator_params['min'], $generator_params['max']);
						
						for ($count = 0; $count < $qu; $count++){
							while (true){
								$value = isset($generator_params['data']) ?
									$this->generateAnswer($generator_params['data'], true, $result) : false;
								
								if (!$generator_params['unique'])
									break;
								
								foreach ($generator_params['unique'] as $unique_name)
									if (isset($unique[$unique_name], $value[$unique_name]) &&
										in_array($value[$unique_name], $unique[$unique_name]))
											continue 2;
								
								foreach ($generator_params['unique'] as $unique_name)
									if (isset($value[$unique_name]))
										$unique[$unique_name][] = $value[$unique_name];
								
								break;
							}
							
							$items[] = $value;
						}
						
						if ($generator_params['sort'])
							Arrays::sort($items, $generator_params['sort'], $generator_params['sort_desc']);
						
						$result[$name] = $items;
						
						break;
					case 'number':
						if ($generator_params['long']){
							$result[$name] = '';
							$len = rand($generator_params['min'], $generator_params['max']);
							
							for ($count = 0; $count < $len; $count++)
								$result[$name] .= rand($count ? 0 : 1, 9);
						}elseif ($generator_params['float'])
							$result[$name] = $generator_params['min'] +
								rand(0, ($generator_params['max'] - $generator_params['min']) * 100) / 100;
						else
							$result[$name] = rand($generator_params['min'], $generator_params['max']);
						
						break;
					case 'phrase':
						if ($generator_params['set'] && ($generation_data = $this->getGenerationData()) &&
							($data = Arrays::pop($generation_data, $generator_params['set'])))
								$result[$name] = $data[rand(0, count($data) - 1)];
							elseif ($generator_params['sequence'] && is_array($generator_params['sequence']))
								$result[$name] = implode('', $this->generateAnswer($generator_params['sequence'], true, $result));
						
						break;
					case 'variant':
						if ($generator_params['variants']){
							if ($generator_params['variable'])
								$key_variant = Arrays::pop(array_merge($result, $result_parent ? $result_parent : array()),
									$generator_params['variable']);
							elseif ($generator_params['expression'])
								$key_variant = eval($generator_params['expression']);
							else
								$key_variant = Arrays::pop(array_keys($generator_params['variants']),
									rand(0, count($generator_params['variants']) - 1));
							
							if (($variant = Arrays::pop($generator_params['variants'], $key_variant, false, false, false, false)) !== null)
								$result[$name] = ($result_variant = $this->generateAnswer(array($variant), true, $result)) ?
									reset($result_variant) : false;
						}
						
						break;
				}
			}
		}
		
		if (!$recursion)
			$result['error'] = array('code'=>0, 'description'=>'Ok');
		
		return $result;
	}
	
}
