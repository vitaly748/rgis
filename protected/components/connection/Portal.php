<?php
/**
 * Компонент для работы с Основным порталом
 * @author v.zubov
 */
class Portal extends ComponentConnection{
	
	/**
	 * Переопределение родительской функции "get", осуществляющей выборку и получение данных конфигурации, с целью
	 * добавления обработки параметра "link"
	 * @param boolean|string $name Название параметра или false. По умолчанию равно false
	 * @return array|boolean|string Выбранные данные конфигурации или false
	 */
	public function get($name = false){
		if ($name === 'link'){
			if (!$host = parent::get('host'))
				return false;
			
			if ($port = parent::get('port'))
				$port = ':'.$port;
			
			return $host.$port;
		}else
			return parent::get($name);
	}
	
}
