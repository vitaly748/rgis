<?php
/**
 * Компонент для работы с файлами
 * @author v.zubov
 */
class File extends ComponentDeploy{
	
	/**
	 * @var string Псевдоним пути к папке загрузки файлов
	 */
	const DIR_DOWNLOAD = 'application.downloads';
	
	/**
	 * @var string Название папки временных файлов
	 */
	const DIR_TEMP = 'temp';
	
	/**
	 * @var Прификс идентификаторов файлов, сохранённых на сервере
	 */
	const PREFIX_SAVED_FILES = 's';
	
	/**
	 * @var string Путь к папке с файлами для текущего контроллера
	 */
	protected $_dir;
	
	/**
	 * Конструктор компонента
	 */
	public function init(){
	}
	
	/**
	 * Подготовка к развёртыванию
	 * @param string $section Название группы таблиц
	 * @param string $type Символьный идентификатор типа предстоящего развёртывания
	 * @param array $params Ассоциативный массив с параметрами развёртывания приложения
	 * @throws CHttpException
	 */
	public function beforeDeployCompute($section, $type, $params){
		if ($params[$section]['check']){
			$file_types = Yii::app()->conformity->get('file_type', 'name');
			
			if ($type === 'old')
				foreach ($file_types as $key=>$file_type)
					if (!isset($params[$file_type]) || $params[$file_type]['type'] !== 'standart')
						unset($file_types[$key]);
			
			foreach ($file_types as $file_type)
				$this->deleteByType($file_type);
		}
		
		parent::beforeDeployCompute($section, $type, $params);
	}
	
	/**
	 * Загрузка файлов
	 */
	public function download(){
		$result = array('files'=>array());
		$files_state_new = array();
		$params_fix = array('data'=>$_FILES);
		
		try{
			if (!$_FILES)
				throw new CHttpException(500, 'empty data');
			
			$dir_temp = $this->getDir().static::DIR_TEMP;
			
			if (!is_dir($dir_temp) && !mkdir($dir_temp, 0777, true))
				throw new CHttpException(500, 'error create path');
			
			foreach ($_FILES as $file){
				$path = $dir_temp.DIRECTORY_SEPARATOR.($file_id = $this->getUniqueTempFileName($dir_temp));
				$result_move = move_uploaded_file($file['tmp_name'], $path);
				$params_fix['files'][$file_id] = array('tmp_name'=>$file['tmp_name'], 'result'=>$result_move);
				$files_state_new[$file_id] = array('name'=>$file['name'], 'size'=>$file['size']);
				
				if (!$result_move)
					throw new CHttpException(500, 'error move');
				
				$result['files'][$file['name']] = $file_id;
			}
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('file_download', $params_fix, $ok ? 'ok' : 'file_download');
		
		if ($files_state_new){
			if ($files_state = Yii::app()->user->getState('files'))
				$files_state_new = $files_state + $files_state_new;
			
			Yii::app()->user->setState('files', $files_state_new);
		}
		
		echo json_encode($result);
		
		Yii::app()->end();
	}
	
	/**
	 * Отслеживание загрузки файлов
	 * @param string $key Ключ процесса загрузки
	 */
	public function check($key){
		$key = ini_get('session.upload_progress.prefix').$key;
		$cancel = Arrays::pop($_POST, Files::PARAM_DOWNLOAD_CANCEL);
		
		if ($params = Arrays::pop($_SESSION, $key))
			if ($cancel)
				$_SESSION[$key]['cancel_upload'] = true;
			else{
				$files = array();
				
				if ($params = Arrays::pop($params, 'files'))
					foreach ($params as $file)
						$files[$file['name']] = $file['bytes_processed'];
				
				echo json_encode(array('files'=>$files));
			}
		
		Yii::app()->end();
	}
	
	/**
	 * Выгрузка файла пользователю
	 * @param string $id Идентификатор файла
	 * @param string $name Название файла
	 * @return boolean Успешность выполнения
	 */
	public function upload($id, $name){
		return Files::upload($this->getDir().$id, $name);
	}
	
	/**
	 * Сохранение временного файла в целевом каталоге
	 * @param string $temp_id Имя временного файла
	 * @param boolean|string $name Название файла
	 * @param boolean|integer $type Код типа файла или false. По умолчанию равно false. Если равно false, будет использован
	 * тип, соответствующий текущему контроллеру
	 * @param integer|null $source Идентификатор источника файла или null. По умолчанию равно null
	 * @param integer|null $category Код категории файла или null. По умолчанию равно null
	 * @param boolean|string $date Дата создания файла или false. По умолчанию равно false. Если равно false, будет использована
	 * текущая дата 
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true 
	 * @return boolean|integer Идентификатор созданного файла или false в случае ошибки
	 */
	public function save($temp_id, $name, $type = false, $source = null, $category = null, $date = false, $fix = true){
		if (!$type){
			$type_name = Yii::app()->controller->id;
			$type = Yii::app()->conformity->get('file_type', 'code', $type_name);
		}else
			$type_name = Yii::app()->conformity->get('file_type', 'name', $type);
		
		if (!$date)
			$date = Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM);
		
		$params_fix = array('temp_id'=>$temp_id, 'name'=>$name, 'type'=>$type_name, 'source'=>$source, 'category'=>$category,
			'date'=>$date);
		
		try{
			$path_to = $this->getDir($type_name);
			$path_from = mb_strpos($temp_id, DIRECTORY_SEPARATOR) !== false ? $temp_id :
				$path_to.static::DIR_TEMP.DIRECTORY_SEPARATOR.$temp_id;
			
			if (!is_file($path_from))
				throw new CHttpException(404, 'file not found');
			
			if (!is_dir($path_to) && !mkdir($path_to, 0777, true))
				throw new CHttpException(500, 'error create path');
			
			if (!$name){
				$path_info = pathinfo($path_from);
				
				if (!$name = Arrays::pop($path_info, 'basename'))
					throw new CHttpException(500, 'empty name');
				
				$params_fix['name_set'] = $name;
			}
			
			$model = new FileModel;
			$model->setAttributes(array(
				'type'=>$type,
				'source'=>$source,
				'category'=>$category,
				'name'=>$name,
				'date'=>$date
			), false);
			
			if (!$model->save(false))
				throw new CHttpException(500, 'save model error');
			
			$params_fix['model_id'] = $model->id;
			
			if (!$params_fix['result'] = rename($path_from, $path_to.$model->id))
				throw new CHttpException(500, 'error move');
		}catch (CHttpException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('file_save', $params_fix, $ok ? 'ok' : ($e->statusCode == 404 ? 'file_not_found' : 'file_save'));
		
		return $ok ? $model->id : false;
	}
	
	/**
	 * Удаление загруженного файла и при необходимости перемещение его во временный каталог
	 * @param string $id Идентификатор файла
	 * @param boolean|null|string $temp_id Идентификатор файла во временном каталоге. По умолчанию равно null. Если равно null,
	 * файл будет удалён без перемещения во временный каталог. Если равно false, идентификатор будет сгенерирован автоматически.
	 * Если равно true, идентификатором станет пользовательское название файла
	 * @param string $type_name Символьный идентификатор типа файла. По умолчанию равно false. Если равно false, будет использован
	 * тип, соответствующий текущему контроллеру
	 * @param boolean $fix Признак необходимости фиксации данного события в логах. По умолчанию равно true 
	 * @return boolean|integer Идентификатор временного файла или успешность выполнения
	 */
	public function delete($id, $temp_id = null, $type_name = false, $fix = true){
		$params_fix = array('id'=>$id, 'temp_id'=>$temp_id);
		
		try{
			$path_from = ($path_to = $this->getDir($type_name)).$id;
			$model = FileModel::model()->findByPk($id);
			
			if (!$model || !is_file($path_from))
				throw new CHttpException(404, 'file not found');
			
			$params_fix['model'] = $model->attributes;
			
			if (!$model->delete())
				throw new CHttpException(500, 'delete model error');
			
			if ($temp_id === null){
				if (!$params_fix['result'] = @unlink($path_from))
					throw new CHttpException(500, 'delete file error');
			}else{
				$path_to .= static::DIR_TEMP;
				
				if (!$temp_id)
					$params_fix['temp_id_set'] = $temp_id = $this->getUniqueTempFileName($path_to);
				elseif ($temp_id === true)
					$params_fix['temp_id_set'] = $temp_id = $model->name;
				
				$path_to .= DIRECTORY_SEPARATOR.$temp_id;
				
				if (!$params_fix['result'] = rename($path_from, $path_to))
					throw new CHttpException(500, 'error move');
			}
		}catch (CHttpException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		if ($fix)
			Yii::app()->log->fix('file_delete', $params_fix, $ok ? 'ok' : ($e->statusCode == 404 ? 'file_not_found' : 'file_delete'));
		
		return $ok && $temp_id ? $temp_id : $ok;
	}
	
	/**
	 * Удаление всех загруженных файлов по типу источника
	 * @param string $type_name Символьный идентификатор типа файлов
	 * @throws CHttpException
	 */
	public function deleteByType($type_name){
		if (!$type_code = Yii::app()->conformity->get('file_type', 'code', $type_name))
			throw new CHttpException(500, 'undefined type source');
		
		if (!FileModel::model()->deleteAll('type = '.$type_code))
			throw new CHttpException(500, 'delete models error');
		
		Files::clear(Yii::getPathOfAlias(static::DIR_DOWNLOAD).DIRECTORY_SEPARATOR.$type_name);
	}
	
	/**
	 * Обратное перемещение файла в случае ошибки в процессе сохранения общей модели данных
	 * @param string $temp_id Идентификатор файла во временном каталоге 
	 * @param string $file_id Идентификатор файла
	 * @param boolean $save Признак сохранения файла в целевом каталоге
	 * @param string $type_name Символьный идентификатор типа файлов
	 * @return boolean Успешность выполения
	 */
	public function reverse($temp_id, $file_id, $save = true, $type_name = false){
		if (!$temp_id || !$file_id)
			return false;
		
		$path_to = ($path_from = $this->getDir($type_name)).$file_id;
		$path_from .= static::DIR_TEMP.DIRECTORY_SEPARATOR.$temp_id;
		
		if (!$save)
			Swamper::swap($path_from, $path_to);
		
		if (!file_exists($path_from))
			return false;
		
		return rename($path_from, $path_to);
	}
	
	/**
	 * Получение информации о загруженных файлах
	 * @param boolean|integer $id Идентификатор файла или false. По умолчанию равно false
	 * @param boolean|integer $type Код типа файла или false. По умолчанию равно false. Если равно true, будет использован
	 * тип, соответствующий текущему контроллеру
	 * @param array|boolean $conditions Ассоциативный массив параметров запроса. По умолчанию равно false
	 */
	public function get($id = false, $type = false, $conditions = false){
		if ($id)
			return ($model = FileModel::model()->findByPk($id)) ? $model : false;
		else{
			$condition = $params = array();
			
			if ($type === true)
				$type = Yii::app()->conformity->get('file_type', 'code', Yii::app()->controller->id);
			
			if ($type){
				$condition[] = 'type = :type';
				$params[':type'] = $type;
			}
			
			if ($conditions)
				foreach ($conditions as $name=>$value)
					if (in_array($name, array('source', 'category', 'name', 'date'))){
						$condition[] = "$name = :$name";
						$params[":$name"] = $value;
					}
			
			$models = FileModel::model()->findAll(array(
				'condition'=>implode(' AND ', $condition),
				'params'=>$params,
				'order'=>'id'
			));
			
			return $models ? $models : false;
		}
	}
	
	/**
	 * Получение уникального имени временного файла
	 * @param boolean|string $dir Путь к папке временных файлов или false. По умолчанию равно false. Если равно false, то
	 * используется каталог временных файлов по умолчанию
	 * @return string Уникальное название временного файла
	 */
	public function getUniqueTempFileName($dir = false){
		if (!$dir)
			$dir = $this->getDir().static::DIR_TEMP;
		
		return Files::getUniqueFileName($dir);
	}
	
	/**
	 * Проверка наличия файла
	 * @param string $id Идентификатор файла
	 * @return boolean Признак наличия файла
	 */
	public function exists($id){
		$path = $this->getDir().$id;
		
		return @is_file($path);
	}
	
	/**
	 * Получение размера файла
	 * @param string $id Идентификатор файла
	 * @return boolean|integer Размер файла или false в случае ошибки
	 */
	public function filesize($id){
		$path = $this->getDir().$id;
		
		return @filesize($path);
	}
	
	/**
	 * Получение пути к папке с файлами
	 * @param boolean|string $type_name Символьный идентификатор типа файла. По умолчанию равно false. Если равно false, будет
	 * использован тип, соответствующий текущему контроллеру. Соответствует названию подпапки с файлами в общем каталоге
	 * загрузаемых файлов
	 * @return string Путь к папке с файлами
	 */
	public function getDir($type_name = false){
		$dir = $type_name || !$this->_dir ? Yii::getPathOfAlias(static::DIR_DOWNLOAD).DIRECTORY_SEPARATOR.
			($type_name ? $type_name : Yii::app()->controller->id).DIRECTORY_SEPARATOR : $this->_dir;
		
		if (!$type_name && $this->_dir === null)
			$this->_dir = $dir;
		
		return $dir;
	}
	
}
