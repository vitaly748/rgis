<?php
/**
 * Виджет "Список файлов"
 * @author v.zubov
 */
class FileList extends Widget{
	
	/**
	 * @var array HTML-опции объекта
	 */
	public $htmlOptions;
	
	/**
	 * @var CModel Экземпляр модели данных
	 */
	public $model;
	
	/**
	 * @var string Название элемента
	 */
	public $name;
	
	/**
	 * @var string Значение элемента
	 */
	public $value;
	
	/**
	 * Запуск виджета
	 */
	public function run(){
		if ($this->model && $this->name)
			$this->value = $this->model->{$this->name};
		
		if ($this->value)
			$this->render('file_list', array(
				'html_options'=>$this->preparationHtmlOptions(),
				'value'=>$this->value
			));
	}
	
}
