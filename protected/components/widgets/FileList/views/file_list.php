<?php
Yii::app()->asset->add();

echo Html::openTag('div', $html_options);
	echo Html::openTag('ul');
		
		foreach ($value as $id=>$name){
			$info = pathinfo($name);
			$extension = !empty($info['extension']) ? $info['extension'] : 'none';
			
			echo Html::openTag('li');
				echo Html::openTag('a', array('href'=>Yii::app()->createUrl('appeal/file', array('id'=>$id)), 'target'=>'_blank'));
					echo Html::tag('div', array('class'=>'fl-icon file-'.mb_strtolower($extension)), '');
					echo Html::tag('div', array('class'=>'fl-name'), $name);
				echo Html::closeTag('a');
			echo Html::closeTag('li');
		}
		
	echo Html::closeTag('ul');
echo Html::closeTag('div');
