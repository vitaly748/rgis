<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CActiveForm
 * @author v.zubov
 * @see CActiveForm
 */
class ActiveForm extends CActiveForm{
	
	/**
	 * @var Идентификатор формы
	 */
	public $id;
	
	/**
	 * @var Признак необходимости присутствия командного элемента. По умолчанию равно false
	 */
	public $command = false;
	
	/**
	 * @var boolean Признак проверки данных формы на стороне клиента. По умолчанию равно true
	 */
	public $enableClientValidation = true;
	
	/**
	 * @var array Опции клиентской обработки данных
	 */
	/* public $clientOptions = array(
		'validateOnSubmit'=>true,
		'afterValidate'=>'js:wActiveFormAfterValidate'
	); */
	
	/**
	 * @var array Ассоциативный массив параметров кодов подтверждения введённых данных на данной форме
	 */
	public $confirmParams;
	
	/**
	 * Конструктор виджета
	 */
	public function init(){
		Yii::app()->asset->add();
		$this->id = Yii::app()->controller->getFormId($this->id);
		
		parent::init();
		
		if ($this->command)
			echo Html::hiddenField(Html::ID_COMMAND_DEF);
	}
	
	/**
	 * Переопределение и дополнение стандартного запуска
	 */
	public function run(){
		parent::run();
		
		if ($this->confirmParams)
			Yii::app()->controller->widget(Widget::alias('JuiDialog'), array(
				'action'=>'code',
				'actionParams'=>array('params'=>$this->confirmParams['attributes']),
				'autoOpen'=>$this->confirmParams['open']
			));
	}
	
	/**
	 * Переопределение стандартного метода validate, осуществляющего проверку данных модели
	 * @param mixed $models Экземпляр модели данных или список экземпляров моделей данных
	 * @param array $attributes Список атрибутов, проверку которых необходимо произвести. По умолчанию равно null
	 * @param boolean $loadInput Признак необходимости загрузки данных модели из массива $_POST. По умолчанию равно false
	 * @param boolean $wrap Признак необходимости обёртки результатов проверки в формат JSON. По умолчанию равно false
	 * @result array|string Результат проверки
	 */
	public static function validate($models, $attributes = null, $loadInput = false, $wrap = false){
		$result = array();
		
		if (!is_array($models))
			$models = array($models);
		
		foreach ($models as $model){
			$modelName = CHtml::modelName($model);
			
			if ($loadInput && $model->post)
				$model->attributes = $model->post;
			
			$model->validate($attributes);
			
			foreach ($model->getErrors() as $attribute=>$errors)
				$result[CHtml::activeId($model, $attribute)] = $errors;
		}
		
		return !$wrap ? $result : (function_exists('json_encode') ? json_encode($result) : CJSON::encode($result));
	}
	
}
