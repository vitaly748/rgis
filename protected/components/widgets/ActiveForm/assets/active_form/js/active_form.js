function wActiveFormErrors(form, errors){
	var settings = $.fn.yiiactiveform.getSettings(form);
	
	$.each(settings.attributes, function(){
		$.fn.yiiactiveform.updateInput(this, errors, form);
	});
}

function wActiveFormValidateAttribute(name){
	$('form').each(function(){
		var form = $(this),
			settings = $.fn.yiiactiveform.getSettings(form);
		
		if (settings)
			$.each(settings.attributes, function(index, attribute){
				if (attribute.name == name){
					var input = $('#' + attribute.id);
					
					if (input.length){
						var msg = [],
							messages = {};
						
						attribute.clientValidation(input.val(), msg, attribute);
						messages[attribute.id] = msg; 
						$.fn.yiiactiveform.updateInput(attribute, messages, form);
						
						return false;
					}
				}
			});
	});
}

/*function wActiveFormAfterValidate(form, data, hasError){
	if (!hasError)
		return true;
	
	var result = true;
	
	$.each(data, function(id, errors){
		var input = $('#' + id);
		
		if (input.length && input.is(':visible') && !input.attr('disabled')){
			result = false;
			
			return false;
		}
	});
	
	if (result){
		form.yiiactiveform('data').submitting = true;
		form.submit();
	}
}*/

/*$(window).load(function(){
	$('form').on('submit', function(event){
		var form = $(this),
			settings = $.fn.yiiactiveform.getSettings(form);
		
		settings.submitting = true;
		
		$.fn.yiiactiveform.validate(form, function(data){
			var hasError = false;
			console.log(data);
			$.each(settings.attributes, function(){
				hasError = $.fn.yiiactiveform.updateInput(this, data, form) || hasError;
			});
			
			if (!hasError){
				var button = form.find(':submit:first');
				
//				if (button.length)
//					button.click();
//				else
//					form.submit();
			}
		});
		
		settings.submitting = false;
		event.preventDefault();
	});
});
*/