wCaptcha = {};

function wCaptchaGetId(inputId){
	return $('input#' + inputId).parent().children('div.captcha').attr('id');
}

function wCaptchaRefresh(id){
	if (id in wCaptcha){
		var obj = $('div.captcha#' + id),
			img = obj.children('img');
			button = obj.children('a');
		
		if (!button.length)
			button = img;
		
		button.addClass('disabled');
		
		$.ajax({
			url: wCaptcha.url_refresh,
			dataType: 'json',
			cache: false,
			success: function(data){
				img.attr('src', data['url']);
				wCaptcha[id].hash = [data['hash1'], data['hash2']];
				wCaptcha[id].qu_retries = 0;
			},
			complete: function(){
				setTimeout(function(){
					button.removeClass('disabled');
				}, 2000);
			}
		});
	}
}

function wCaptchaCheck(id, value, caseSensitive){
	if (id in wCaptcha && value.length){
		var hash = wCaptcha[id].hash[caseSensitive ? 0 : 1],
			hash_test = 0;
		
		if (!caseSensitive)
			value = value.toLowerCase();
		
		for (var count = value.length - 1; count >= 0; --count)
			hash_test += value.charCodeAt(count);
		
		if (hash == hash_test)
			return 0;
		else if (value != wCaptcha[id].old_value){
			wCaptcha[id].qu_retries++;
			wCaptcha[id].old_value = value;
			
			if (wCaptcha[id].max_retries && wCaptcha[id].qu_retries >= wCaptcha[id].max_retries){
				wCaptchaRefresh(id);
				
				return 2;
			}
		}
	}
	
	return 1;
}

$(window).load(function(){
	$(document).on('click', 'div.captcha a.button.refresh, div.captcha img.clickable', function(event){
		if (!$(this).hasClass('disabled'))
			wCaptchaRefresh($(this).parent().attr('id'));
		
		event.preventDefault();
	});
});
