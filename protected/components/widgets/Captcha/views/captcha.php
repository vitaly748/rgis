<?php
Yii::app()->asset->add();

echo Html::openTag('div', $html_options);
	
	echo $image;
	
	if ($show_refresh_button)
		echo Html::link('', '#', array('class'=>'button refresh contur circle'));
	
echo Html::closeTag('div');

Html::registerScriptParams(get_class($this), $html_options['id'],
	array('hash'=>$hash, 'qu_retries'=>0, 'max_retries'=>$max_retries, 'old_value'=>''), array('url_refresh'=>$url_refresh),
	false, Html::PREFIX_SCRIPT_WIDGET);
