<?php
/**
 * Класс, переопределяющий и дополняющий стандартный виджет CCaptcha
 * @author v.zubov
 * @see CCaptcha
 */
class Captcha extends CCaptcha{
	
	/**
	 * @var boolean Признак автоматического обновления капчи в случае превышения числа попыток. По умолчанию равно true
	 */
	public $autoRefresh = true;
	
	/**
	 * Переопределение стандартного метода установки данных класса с целью добавлеления возможности подачи на вход
	 * параметров без необходимости их фильтрации на предмет существования в данном классе
	 * @param string $name Название параметра
	 * @param mixed $value Значение параметра
	 * @see CComponent::__set()
	 */
	public function __set($name, $value){
		if (property_exists($this, $name) || method_exists($this, 'set'.$name))
			parent::__set($name, $value);
	}
	
	/**
	 * Запуск виджета
	 * @see CCaptcha::run()
	 */
	public function run(){
		$this->imageOptions = Html::forming(array('id'=>'captcha-'.$this->id, 'clickable'=>$this->clickableImage));
		$show_refresh_button = $this->showRefreshButton;
		$this->showRefreshButton = false;
		$action = $this->getController()->createAction($this->captchaAction);
		$verify_code = $action->getVerifyCode(!$_POST);
		$hash = array($action->generateValidationHash($verify_code), $action->generateValidationHash(mb_strtolower($verify_code)));
		
		ob_start();
			parent::run();
		$image = ob_get_clean();
		
		$this->render('captcha', array(
			'html_options'=>array('id'=>$this->id, 'class'=>'captcha'),
			'image'=>$image,
			'show_refresh_button'=>$show_refresh_button,
			'url_refresh'=>$this->getController()->createUrl($this->captchaAction, array(CCaptchaAction::REFRESH_GET_VAR=>true)),
			'hash'=>$hash,
			'max_retries'=>$this->autoRefresh ? (int)Yii::app()->setting->get('code_retries') : false
		));
	}
	
}
