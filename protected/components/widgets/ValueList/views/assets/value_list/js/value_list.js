function wValueListSetNeedId(){
	if (typeof setNeedId !== 'undifined')
		$('div.value-list').each(function(){
			var a = $(this).find('a.vl-checking');
			
			if (a.length){
				var code = a.attr('href');
				
				if (code)
					setNeedId(code);
			}
		});
}

$(window).load(function(){
	$('div.value-list').on('click', 'a', function(event){
		event.preventDefault();
		
		var a = $(this),
			obj = $(this).closest('div.value-list');
		
		if (a.hasClass('vl-last')){
			if (!a.hasClass('vl-checking') && !a.hasClass('vl-disabled')){
				var code = a.attr('href');
				
				obj.find('a.vl-checking').removeClass('vl-checking');
				a.addClass('vl-checking');
				obj.children('input[type=hidden]').val(code);
				
				if (typeof setNeedId !== 'undifined' && code)
					setNeedId(code);
			}
		}else{
			var li = a.parent(),
				childs = li.children('ul').children('li');
			
			if (!li.hasClass('vl-open'))
				childs.slideDown(function(){
					li.addClass('vl-open');
				});
			else
				childs.slideUp(function(){
					li.removeClass('vl-open');
				});
		}
	});
});
