<?php
Yii::app()->asset->add();

echo Html::openTag('div', $html_options);
	
	if ($model && $name)
		echo Html::activeHiddenField($model, $name);
	else
		echo Html::hiddenField($name);
	
	$this->render('value_list_level', array('data'=>$data, 'value'=>$value, 'open'=>$open));
	
echo Html::closeTag('div');
