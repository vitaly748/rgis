<?php
echo Html::openTag('ul');
	foreach ($data as $item){
		echo Html::openTag('li', Html::forming(array('vl-open'=>in_array($item['code'], $open))));
			
			echo Html::openTag('a', Html::forming(array('href'=>$item['code'],
				'vl-checking'=>$value && $item['code'] === $value, 'vl-last'=>!$item['childs'])));
					
					echo Html::tag('p', array('class'=>'vl-code'), $item['code']);
					echo Html::tag('p', array('class'=>'vl-description'), $item['description']);
					
				echo Html::closeTag('a');
			
			if ($item['childs'])
				$this->render('value_list_level', array('data'=>$item['childs'], 'value'=>$value, 'open'=>$open));
			
		echo Html::closeTag('li');
	}
	
echo Html::closeTag('ul');
