<?php
/**
 * Виджет представления значений в виде вложенного списка
 * @author v.zubov
 */
class ValueList extends Widget{
	
	/**
	 * @var array HTML-опции объекта
	 */
	public $htmlOptions;
	
	/**
	 * @var CModel Экземпляр модели данных
	 */
	public $model;
	
	/**
	 * @var string Название элемента
	 */
	public $name;
	
	/**
	 * @var mixed Значение активного пункта списка
	 */
	public $value;
	
	/**
	 * @var array Ассоциативный массив значений списка в формате
	 * 	array(
	 * 		'2'=>array(
	 * 			'code'=>'2',// Код значения. Обязательный параметр
	 * 			'description'=>'Значение 2',// Значение. Обязательный параметр
	 * 			'childs'=>array(// Вложенные элементы. Необязательный параметр. По умолчанию равно array()
	 * 				'2.1'=>'Значение 2.1',
	 * 				...
	 * 			)
	 * 		),
	 * 		...
	 * 	)
	 */
	public $data;
	
	/**
	 * Запуск виджета
	 */
	public function run(){
		if (!$this->data && $this->model && $this->name)
			$this->data = Yii::app()->value->get($this->model->getAttributeValueId($this->name));
		
		if ($this->data){
			$open = null;
			
			if ($this->value === null && $this->model && $this->name)
				$this->value = $this->model->{$this->name};
			
			if ($this->value){
				foreach ($this->data as $value1){
					if ($value1['code'] == $this->value){
						$open = array();
						
						break;
					}
					
					if ($value1['childs'])
						foreach ($value1['childs'] as $value2){
							if ($value2['code'] == $this->value){
								$open = array($value1['code']);
							
								break 2;
							}
							
							if ($value2['childs'])
								foreach ($value2['childs'] as $value3)
									if ($value3['code'] == $this->value){
										$open = array($value1['code'], $value2['code']);
											
										break 3;
									}
						}
				}
				
				if ($open === null)
					$this->value = null;
			}
			
			$this->render('value_list', array(
				'html_options'=>$this->preparationHtmlOptions(),
				'model'=>$this->model,
				'name'=>$this->name,
				'value'=>$this->value,
				'data'=>$this->data,
				'open'=>(array)$open
			));
		}
	}
	
}
