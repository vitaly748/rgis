<?php
/**
 * Виджет представления данных в виде вложенного списка
 * @author v.zubov
 */
class NestedList extends TreeWidget{
	
	/**
	 * @var string Обязательный HTML-класс контейнера уровня "root"
	 */
	const CLASS_ROOT = 'nested-list';
	
	/**
	 * @var string Обязательный HTML-класс контейнера уровня "blocks"
	 */
	const CLASS_BLOCKS = 'nl-block';
	
	/**
	 * @var string Обязательный HTML-класс контейнера уровня "items"
	 */
	const CLASS_ITEMS = 'nl-item';
	
	/**
	 * @var string Название типа элемента блока по умолчанию, равно "radio"
	 */
	const ITEM_TYPE_DEF = 'radio';
	
	/**
	 * @var array Список названий уровней, для элементов которых нужно автоматически устанавливать
	 * HTML-идентификатор в случае его отсутствия
	 */
	const HTML_OPTIONS_ID_FILLING = array('items');
	
	/**
	 * @var string Префикс имени элемента блока. Применяется, когда не удаётся вычислить имя для элемента блока
	 * типов "radio" или "checkbox"
	 */
	const PREFIX_NAME = 'nl-name-';
	
	/**
	 * @var array|string Список альтернативных названий свойств при передаче параметров от виджета к виджету
	 */
	const ALTERNATIVE_PROPERTIES = 'item';
	
	/**
	 * @var array HTML-опции объекта
	 */
	public $htmlOptions;
	
	/**
	 * @var string Название блока соответствий класса Conformity, сопряжимого с элементами блока
	 */
	public $blockConformity;
	
	/**
	 * @var boolean Признак использования символьного идентификатора в качестве значения элемента соответствеия
	 */
	public $blockConformityName;

	/**
	 * @var string Список исключений для блока соответствий. Применяется совместно с параметром blockConformity.
	 * Задаётся в формате:
	 * 	{Список символьных идентификаторов исключаемых соотвыетствий}=>{PHP-выражение, определяющее необходимость
	 * 		исключения}
	 * Примеры:
	 * 	array('generation'=>'return $data["name"] !=== "users";')
	 * 	array('generation, rand'=>'return $data["name"] !=== "users";')
	 */
	public $blockConformityExceptions;
	
	/**
	 * @var boolean Признак необходимости автоматической генерации HTML-идентификатора блока
	 */
	public $blockNeedId;
	
	/**
	 * @var string Название типа элемента блока. По умолчанию равно BLOCK_TYPE_DEF
	 * @see Html::element()
	 */
	public $itemType;
	
	/**
	 * @var object Экземпляр HTML-формы класса ActiveForm
	 */
	public $itemForm;
	
	/**
	 * @var object|string Название модели данных или экземпляр модели данных элемента блока
	 */
	public $itemModel;
	
	/**
	 * @var string Название элемента блока
	 */
	public $itemName;
	
	/**
	 * @var string Значение элемента блока
	 */
	public $itemValue;
	
	/**
	 * @var string Текстовая метка элемента блока
	 */
	public $itemLabel;
	
	/**
	 * @var boolean Признак выбранности элемента блока, применяется для элементов типа "radio" и "checkbox", если
	 * не задана модель данных
	 */
	public $itemChecked;
	
	/**
	 * @var boolean Признак необходимости автоматической генерации HTML-идентификатора элемента блока
	 */
	public $itemNeedId;

	/**
	 * @var string Название виджета
	 */
	public $widgetName;
	
	/**
	 * @var array Параметры, передаваемые в характеристику атрибута типа widget. Объединяются
	 * с TRANSITION_PROPERTIES
	 */
	public $widgetParams;
	
	/**
	 * @var array|integer Параметры уровня "blocks"
	 */
	public $blocks;
	
	/**
	 * @var array|integer Параметры уровня "items"
	 */
	public $items;
	
	/**
	 * @var array Ассоциативный массив описания уровней и соответствующих им параметров
	 */
	protected $levels = array(
		'root'=>array(
			'blockConformity', 'blockConformityName', 'blockConformityExceptions', 'blockNeedId', 'itemType', 'itemForm', 'itemModel',
			'itemName', 'itemValue', 'itemChecked', 'itemLabel', 'itemNeedId', 'widgetName', 'widgetParams',
			TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions')
		),
		'blocks'=>array(
			TreeWidget::PARAM_TYPE_FIX=>array('blockNeedId'),
			TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions', 'blockConformity', 'blockConformityName', 'blockConformityExceptions')
		),
		'items'=>array(
			TreeWidget::PARAM_TYPE_FIX=>array('itemForm', 'itemModel', 'itemName', 'itemNeedId'),
			TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions', 'itemType', 'itemValue', 'itemChecked', 'itemLabel',
			'widgetName', 'widgetParams')
			//Системные параметры: _fullness
		)
	);
	
	/**
	 * @var array Список возможных нестандартных переходов между уровнями
	 */
	protected $transitions = array('items'=>'blocks');
	
	/**
	 * Запуск виджета
	 */
	public function run(){
		if ($data = $this->preparationRelations($this->preparationNames($this->preparation())))
			$this->render('nested_list', array('data'=>$data));
	}
	
	/**
	 * Специальная обработка параметров уровня "blocks"
	 * @param array $params Параметры уровня
	 */
	protected function preparationBlocks(&$params){
		if ($conformity = $params[TreeWidget::PARAM_TYPE_FIX]['blockConformity']){
			$conformity_exceptions = array();
			
			if ($exceptions = $params[TreeWidget::PARAM_TYPE_FIX]['blockConformityExceptions'])
				foreach ($exceptions as $key_exception=>$expression_exception)
					if (eval("return $expression_exception;"))
						if ($keys = Strings::devisionWords($key_exception))
							$conformity_exceptions = array_merge($conformity_exceptions, $keys);
			
			if ($conformities = Yii::app()->conformity->get($conformity, false, false, $conformity_exceptions)){
				$items_conformities = array();
				list($model, $name) = Arrays::pop($params[TreeWidget::PARAM_TYPE_TRANS], 'itemModel, itemName');
				$fix_codes = $model && $name && isset($model->accessValues[$name]) ?
					Arrays::select($model->accessValues[$name], 'access', 'code') : false;
				
				foreach ($conformities as $conformity){
					$fix_code = Arrays::pop($fix_codes, $conformity['code']);
					
					if ($fix_code === null || $fix_code)
						$items_conformities[$conformity['name']] = Arrays::forming(array(
							'itemValue'=>$conformity[$params[TreeWidget::PARAM_TYPE_FIX]['blockConformityName'] ? 'name' : 'code'],
							'itemLabel'=>$conformity['description'],
							'htmlOptions'=>$fix_code != 'update' ? array('disabled'=>'disabled') : false
						));
				}
				
				if (!empty($params[TreeWidget::PARAM_TYPE_TRANS]['items']))
					$items_conformities = array_merge_recursive($items_conformities,
						$params[TreeWidget::PARAM_TYPE_TRANS]['items']);
				
				$params[TreeWidget::PARAM_TYPE_TRANS]['items'] = $items_conformities;
			}
		}
	}
	
	/**
	 * Специальная обработка параметров уровня "items"
	 * @param array $params Параметры уровня
	 */
	protected function preparationItems(&$params){
		if (!$params[TreeWidget::PARAM_TYPE_FIX]['itemType'])
			$params[TreeWidget::PARAM_TYPE_FIX]['itemType'] = self::ITEM_TYPE_DEF;
		
		if ($params[TreeWidget::PARAM_TYPE_FIX]['itemValue'] === null &&
			$params[TreeWidget::PARAM_TYPE_FIX]['itemModel'] && $params[TreeWidget::PARAM_TYPE_FIX]['itemName']){
				$params[TreeWidget::PARAM_TYPE_FIX]['itemValue'] =
					$params[TreeWidget::PARAM_TYPE_FIX]['itemModel']->$params[TreeWidget::PARAM_TYPE_FIX]['itemName'];
				$params[TreeWidget::PARAM_TYPE_FIX]['_fullness'] = 
					Swamper::dataFullness($params[TreeWidget::PARAM_TYPE_FIX]);
			}
		
		if ($list = in_array($params[TreeWidget::PARAM_TYPE_FIX]['itemType'], array('radio', 'checkbox'))){
			if (!array_key_exists('uncheckValue', $params[TreeWidget::PARAM_TYPE_FIX]['htmlOptions']))
				$params[TreeWidget::PARAM_TYPE_FIX]['htmlOptions']['uncheckValue'] = null;
			
			if ($params[TreeWidget::PARAM_TYPE_FIX]['itemValue'] === null)
				$params[TreeWidget::PARAM_TYPE_FIX]['itemValue'] = $params[TreeWidget::PARAM_TYPE_FIX]['id'];
			
			if (empty($params[TreeWidget::PARAM_TYPE_FIX]['htmlOptions']['value'])){
				$params[TreeWidget::PARAM_TYPE_FIX]['htmlOptions']['value'] = 
					$params[TreeWidget::PARAM_TYPE_FIX]['itemValue'];
				$params[TreeWidget::PARAM_TYPE_FIX]['_fullness'] = 
					Swamper::dataFullness($params[TreeWidget::PARAM_TYPE_FIX]);
			}
			
			if (!$params[TreeWidget::PARAM_TYPE_FIX]['itemName']){
				if ($id = $params[TreeWidget::PARAM_TYPE_FIX]['htmlOptions']['id'])
					$params[TreeWidget::PARAM_TYPE_FIX]['itemName'] =
						mb_substr($id, 0, -(mb_strlen($params[TreeWidget::PARAM_TYPE_FIX]['id']) + 1));
				else
					$params[TreeWidget::PARAM_TYPE_FIX]['itemName'] =
						self::PREFIX_NAME.$params[TreeWidget::PARAM_TYPE_FIX]['id'];
				$params[TreeWidget::PARAM_TYPE_FIX]['_fullness'] =
					Swamper::dataFullness($params[TreeWidget::PARAM_TYPE_FIX]);
			}
			
			if ($params[TreeWidget::PARAM_TYPE_FIX]['_fullness'] < Swamper::FULLNESS_MODEL &&
				!array_key_exists('checked', $params[TreeWidget::PARAM_TYPE_FIX]['htmlOptions']) &&
				$params[TreeWidget::PARAM_TYPE_FIX]['itemChecked'])
					$params[TreeWidget::PARAM_TYPE_FIX]['htmlOptions']['checked'] = 'checked';
		}
		
		if (mb_strpos($params[TreeWidget::PARAM_TYPE_FIX]['htmlOptions']['id'], Widget::PREFIX_ID) === 0 &&
			!$params[TreeWidget::PARAM_TYPE_FIX]['itemNeedId'] &&
			!($list && $params[TreeWidget::PARAM_TYPE_FIX]['itemLabel']))
				unset($params[TreeWidget::PARAM_TYPE_FIX]['htmlOptions']['id']);
		
		if ($params[TreeWidget::PARAM_TYPE_FIX]['itemType'] === 'widget' &&
			$params[TreeWidget::PARAM_TYPE_FIX]['widgetName']){
				$params_fullness = Swamper::dataFullnessCorrect($params[TreeWidget::PARAM_TYPE_FIX]);
				$params_append = array();
				
				foreach (self::TRANSITION_PROPERTIES as $property)
					if (isset($params_fullness[$property]))
						$params_append[$property] = $params_fullness[$property];
				
				if (!empty($params[TreeWidget::PARAM_TYPE_FIX]['widgetParams']['id'])){
					$id = $params[TreeWidget::PARAM_TYPE_FIX]['widgetParams']['id'];
					if (mb_substr($id, 0, 1) === '!')
						$params[TreeWidget::PARAM_TYPE_FIX]['widgetParams']['id'] = eval('return '.mb_substr($id, 1).';');
				}
				
				$params[TreeWidget::PARAM_TYPE_FIX]['widgetParams'] =
					is_array($params[TreeWidget::PARAM_TYPE_FIX]['widgetParams']) ?
					array_merge($params_append, $params[TreeWidget::PARAM_TYPE_FIX]['widgetParams']) : $params_append;
			}
	}
	
	/**
	 * Обработка названий элементов блоков
	 * @param array $data Параметры структуры
	 */
	protected function preparationNames(&$data){
		if (!$data || !is_array($data))
			return false;
		
		foreach ($data['blocks'] as &$block)
			if ($block)
				$this->preparationNamesBlock($block);
		unset($block);
		
		return $data;
	}
	
	/**
	 * Обработка названий элементов блока. Если блок содержит более одного элемента типа "checkbox", то к названиям
	 * таких элементов добавляется "[]"
	 * @param array $data Параметры блока
	 */
	protected function preparationNamesBlock(&$data){
		$checkbox_qu = 0;
		
		foreach ($data['items'] as &$item){
			$checkbox_qu += $item['itemType'] === 'checkbox';
			
			if (!empty($item['blocks']))
				$this->preparationNames($item);
		}
		unset($item);
		
		if ($checkbox_qu > 1){
			foreach ($data['items'] as &$item)
				if ($item['itemType'] === 'checkbox')
					$item['itemName'] .= '[]';
			unset($item);
		}
	}
	
	/**
	 * Подготовка ссылок на дочерние элементы блоков
	 * @param array $data Параметры структуры
	 */
	protected function preparationRelations(&$data){
		if (!$data || !is_array($data))
			return false;
		
		foreach ($data['blocks'] as &$block)
			if ($block){
				foreach ($block['items'] as &$item)
					if (in_array($item['itemType'], array('chechbox', 'radio')) && !empty($item['blocks'])){
						$this->preparationRelations($item);
						if ($relations_id = $this->preparationRelationsItem($item))
							Html::mergeClasses($item['htmlOptions'], $relations_id, true);
					}
				unset($item);
			}
		unset($block);
		
		return $data;
	}
	
	/**
	 * Подготовка ссылок на дочерние элементы блока
	 * @param array $data Параметры блока
	 */
	protected function preparationRelationsItem($data){
		$relations_id = array();
		
		foreach ($data['blocks'] as $block)
			foreach ($block['items'] as $item)
				if (in_array($item['itemType'], array('checkbox', 'radio', 'password', 'text', 'textarea')))
					$relations_id[] = Html::PREFIX_INPUT_CHILD.'-'.$item['htmlOptions']['id'];
		
		return $relations_id;
	}
	
}
