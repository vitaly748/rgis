<?php
Yii::app()->asset->add();
	
echo Html::openTag('div', $data['htmlOptions']);
	
	foreach ($data['blocks'] as $block)
		$this->render('nested_list_block', array('block'=>$block));
	
echo Html::closeTag('div');
