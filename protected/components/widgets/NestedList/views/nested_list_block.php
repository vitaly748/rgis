<?php
echo Html::openTag('div', $block['htmlOptions']);
	
	echo Html::openTag('ul');
		foreach ($block['items'] as $key=>$item){
			
			echo Html::openTag('li', array('id'=>'li-'.$key));
				
				echo Html::element($item);
				
				if (isset($item['blocks']))
					foreach ($item['blocks'] as $block)
						$this->render('nested_list_block', array('block'=>$block));
				
			echo Html::closeTag('li');
		}
		
	echo Html::closeTag('ul');
	
echo Html::closeTag('div');
