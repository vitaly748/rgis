<?php
/**
 * Виджет представления данных в табличном виде
 * @author v.zubov
 */
class DataTable extends TreeWidget{
	
	/**
	 * @var string Обязательный HTML-класс контейнера уровня "root"
	 */
	const CLASS_ROOT = 'data-table';
	
	/**
	 * @var string Обязательный HTML-класс контейнера уровня "blocks"
	 */
	const CLASS_BLOCKS = 'dt-block';
	
	/**
	 * @var string HTML-класс заголовка контейнера уровня "blocks"
	 */
	const CLASS_BLOCKS_TITLE = 'dt-block-title';
	
	/**
	 * @var string HTML-класс кнопки раскрытия контейнера уровня "blocks"
	 */
	const CLASS_BLOCKS_TOGGLE = 'dt-block-toggle';
	
	/**
	 * @var string HTML-класс разделительной линии контейнера уровня "blocks"
	 */
	const CLASS_BLOCKS_BORDER = 'dt-block-border';
	
	/**
	 * @var string Обязательный HTML-класс контейнера уровня "attributes"
	 */
	const CLASS_ATTRIBUTES = 'dt-attr';
	
	/**
	 * @var string HTML-класс контейнера уровня "attributes", определяющий идентификатор родительского блока
	 */
	const CLASS_ATTRIBUTES_PID = 'pid-';
	
	/**
	 * @var string HTML-класс контейнера уровня "attributes", определяющий скрытый атрибут
	 */
	const CLASS_ATTRIBUTES_EMPTY = 'dt-attr-hidden';
	
	/**
	 * @var string Обязательный HTML-класс контейнера уровня "cells"
	 */
	const CLASS_CELLS = 'dt-cell';
	
	/**
	 * @var string Обязательный HTML-класс контейнера уровня "features"
	 */
	const CLASS_FEATURES = 'dt-feature';

	/**
	 * @var string HTML-класс, задающий вертикальный отступ
	 */
	const CLASS_INDENT_TOP = 'pt3';
	
	/**
	 * @var string HTML-класс, задающий горизонтальный отступ
	 */
	const CLASS_INDENT_LEFT = 'ml3';
	
	/**
	 * @var integer Код типа кнопки сворачивания блока "Отсутствует"
	 */
	const BLOCK_TOGGLE_TYPE_NONE = 0;
	
	/**
	 * @var integer Код типа кнопки сворачивания блока "Встроенная"
	 */
	const BLOCK_TOGGLE_TYPE_INBUILD = 1;
	
	/**
	 * @var integer Код типа кнопки сворачивания блока "Иконка"
	 */
	const BLOCK_TOGGLE_TYPE_ICON = 2;
	
	/**
	 * @var string Название типа характеристики атрибута по умолчанию
	 */
	const FEATURE_TYPE_DEF = 'html';
	
	/**
	 * @var array Список названий уровней, для элементов которых нужно автоматически устанавливать
	 * HTML-идентификатор в случае его отсутствия
	 */
	const HTML_OPTIONS_ID_FILLING = array('blocks');
	
	/**
	 * @var array|integer Массив параметров уровня "cells" по умолчанию
	 */
	const CELLS_LEVEL_DEF = array(
		array(
			'htmlOptions'=>array('class'=>'cell-name')
		),
		array(
			'htmlOptions'=>array('class'=>'cell-value')
		)
	);
	
	/**
	 * @var array|integer Массив параметров уровня "features" по умолчанию
	 */
	const FEATURES_LEVEL_DEF = array('1.label', '1.tooltip', '2.text', '2.error');
	
	/**
	 * @var array|string Список альтернативных названий свойств при передаче параметров от виджета к виджету
	 */
	const ALTERNATIVE_PROPERTIES = 'feature';
	
	/**
	 * @var array Список названий уровней, на которых возможно прерывание дальнейшего расчёта в случае отсутствия элементов
	 */
	public $breakLevels = array('attributes');
	
	/**
	 * @var array HTML-опции объекта
	 */
	public $htmlOptions;
	
	/**
	 * @var boolean Признак сокрытия блока
	 */
	public $blockHidden;
	
	/**
	 * @var boolean Вертикальный отступ блока. По умолчанию равно true, для первого блока - false
	 */
	public $blockIndentVer;
	
	/**
	 * @var boolean Горизонтальный отступ блока
	 */
	public $blockIndentHor;
	
	/**
	 * @var boolean Признак формирования блока в отдельной от предыдущего блока таблице
	 */
	public $blockSeparate;
	
	/**
	 * @var string Заголовок блока
	 */
	public $blockTitle;
	
	/**
	 * @var boolean Признак отображения горизонтальной границы блока
	 */
	public $blockBorder;
	
	/**
	 * @var integer Код типа кнопки сворачивания блока: "Отсутствует", "Встроенная" или "Иконка". По умолчанию равно
	 * коду типа "Отсутствует"
	 */
	public $blockToggleType;
	
	/**
	 * @var object|string Название модели данных или экземпляр модели данных input-элемента кнопки сворачивания
	 * блока. Используется совместно с blockToggleName
	 */
	public $blockToggleModel;
	
	/**
	 * @var string Имя input-элемента кнопки сворачивания блока
	 */
	public $blockToggleName;
	
	/**
	 * @var boolean Признак использования всех атрибутов модели данных
	 */
	public $attrFull;
	
	/**
	 * @var array Параметр, задающий список ненужных атрибутов
	 */
	public $attrException;
	
	/**
	 * @var boolean Вертикальный отступ атрибута
	 */
	public $attrIndentVer;
	
	/**
	 * @var boolean Горизонтальный отступ атрибута. По умолчанию равно false, при установленном параметре
	 * blockToggle - true
	 */
	public $attrIndentHor;
	
	/**
	 * @var array|integer Автоматическое разделение атрибутов по блокам. Если задано числом, то доступные для просмотра атрибуты
	 * будут разделены по заданному количеству в блоке, при этом будет учитываться параметр attrException. Более точное
	 * разделение задаётся в формате:
	 * 	array(
	 * 		array('attr1', 'attr2'),// Список атрибутов первого блока
	 * 		'block2'=>array('attr1', 'attr2'),// Список атрибутов второго блока с указанием идентификатора блока
	 * 		array('attr1'=>true, 'attr2'=>false),// Описание атрибутов с указанием признака их наличия
	 * 		...
	 * 	)
	 * При использовании данного параметра параметру attrFull будет автоматически присвоено значение false
	 */
	public $attrSeparation;
	
	/**
	 * @var boolean Признак необходимости автоматической генерации HTML-идентификатора атрибута
	 */
	public $attrNeedId;
	
	/**
	 * @var boolean Признак сокрытия атрибута
	 */
	public $attrHidden;
	
	/**
	 * @var boolean Признак необходимости автоматической генерации HTML-идентификатора ячейки
	 */
	public $cellNeedId;
	
	/**
	 * @var string Название типа характеристики атрибута. По умолчанию равно FEATURE_TYPE_DEF
	 * @see Html::element()
	 */
	public $featureType;
	
	/**
	 * @var CActiveForm Экземпляр HTML-формы
	 */
	public $featureForm;
	
	/**
	 * @var object|string Название модели данных или экземпляр модели данных характеристики
	 */
	public $featureModel;
	
	/**
	 * @var string Название характеристики
	 */
	public $featureName;
	
	/**
	 * @var mixed Значение характеристики. Игнорируется, если задана модель данных
	 */
	public $featureValue;
	
	/**
	 * @var boolean Признак необходимости автоматической генерации HTML-идентификатора характеристики атрибута
	 */
	public $featureNeedId;
	
	/**
	 * @var boolean Признак необходимости автоматического включения привилегированных параметров в набор параметров,
	 * переходящих от виджета к виджету. По умолчанию равно false
	 */
	public $featureTransition;
	
	/**
	 * @var string Название виджета
	 */
	public $widgetName;
	
	/**
	 * @var array Параметры, передаваемые в характеристику атрибута типа widget. Объединяются
	 * с TRANSITION_PROPERTIES
	 */
	public $widgetParams;
	
	/**
	 * @var array|integer Параметры уровня "blocks"
	 */
	public $blocks;
	
	/**
	 * @var array|integer Параметры уровня "attributes"
	 */
	public $attributes;
	
	/**
	 * @var array|integer Параметры уровня "cells"
	 */
	public $cells;
	
	/**
	 * @var array|integer Параметры уровня "features"
	 */
	public $features;
	
	/**
	 * @var array Ассоциативный массив описания уровней и соответствующих им параметров
	 */
	protected $levels = array(
		'root'=>array(
			'blockHidden', 'blockIndentVer', 'blockIndentHor', 'blockSeparate', 'blockTitle', 'blockBorder',
			'blockToggleType', 'blockToggleModel', 'blockToggleName', TreeWidget::PARAM_ATTRIBUTES_FULL,
			TreeWidget::PARAM_ATTRIBUTES_EXCEPTION, 'attrIndentVer', 'attrIndentHor', 'attrNeedId', 'cellNeedId',
			'featureType', 'featureForm', 'featureModel', 'featureName', 'featureValue', 'featureNeedId', 'featureTransition',
			'widgetName', 'widgetParams',
			TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions', 'attrSeparation')
		),
		'blocks'=>array(
			TreeWidget::PARAM_TYPE_FIX=>array('featureForm'),
			TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions', 'blockHidden', 'blockIndentVer', 'blockIndentHor',
			'blockSeparate', 'blockTitle', 'blockBorder', 'blockToggleType', 'blockToggleModel', 'blockToggleName')
			//Системные параметры: _cols, _fullness, _headerEnabled 
		),
		'attributes'=>array(
			TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions', 'attrIndentVer', 'attrIndentHor', 'attrNeedId', 'attrHidden')
			//Системные параметры: _cols, _residue
		),
		'cells'=>array(
			TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions', 'cellNeedId')
		),
		'features'=>array(
			TreeWidget::PARAM_TYPE_LIMIT=>array('htmlOptions', 'featureType', 'featureForm', 'featureModel',
			'featureName', 'featureValue', 'featureNeedId', 'widgetName', 'widgetParams')
			//Системные параметры: _fullness
		)
	);
	
	/**
	 * @var array Список определения ведущих параметров на уровне
	 */
	protected $levelIdParams = array(
		'attributes'=>'featureName',
		'features'=>array('id'=>'featureType', 'exceptions'=>'password.*.text')
	);
	
	/**
	 * Запуск виджета
	 */
	public function run(){
		if ($data = $this->preparationTables($this->preparation()))
			$this->render('data_table', array('data'=>$data));
	}
	
	/**
	 * Специальная обработка параметров уровня "root"
	 * @param array $params Параметры уровня
	 */
	protected function preparationRoot(&$params){
		if ($separation = $params[TreeWidget::PARAM_TYPE_FIX]['attrSeparation']){
			$model = $params[TreeWidget::PARAM_TYPE_TRANS]['featureModel'];
			
			if ($model && !$model instanceof CModel)
				$model = false;
			
			if (is_int($separation)){
				if ($model){
					$attributes = $model->allowedAttributeNames;
					
					if ($exception = $params[TreeWidget::PARAM_TYPE_TRANS]['attrException'])
						$attributes = array_diff($attributes, $exception);
					
					$separation = array_chunk($attributes, $separation);
				}
			}
			
			if ($separation && is_array($separation)){
				foreach ($separation as $separation_block){
					$separation_attributes = array();
					
					foreach ($separation_block as $attributes=>$exists){
						if (is_string($exists))
							$attributes = $exists;
						elseif (!is_string($attributes) || !$exists)
							continue;
						
						foreach (Strings::devisionWords($attributes) as $attribute)
							if (!$model || in_array($attribute, $model->allowedAttributeNames))
								$separation_attributes[] = $attribute;
					}
					
					if ($separation_attributes)
						$blocks[] = array('attributes'=>$separation_attributes);
				}
				
				if (!empty($blocks)){
					if ($this->blocks)
						$blocks = Arrays::merge($blocks, $this->blocks, true);
					
					$this->blocks = $blocks;
					$params[TreeWidget::PARAM_TYPE_TRANS][TreeWidget::PARAM_ATTRIBUTES_FULL] = false;
				}
			}
		}
	}
	
	/**
	 * Специальная обработка параметров уровня "blocks"
	 * @param array $params Параметры уровня
	 */
	protected function preparationBlocks(&$params){
		if ($params[TreeWidget::PARAM_TYPE_FIX]['blockToggleType'] === null)
			$params[TreeWidget::PARAM_TYPE_FIX]['blockToggleType'] = self::BLOCK_TOGGLE_TYPE_NONE;
		elseif ($params[TreeWidget::PARAM_TYPE_FIX]['blockToggleType'] === self::BLOCK_TOGGLE_TYPE_INBUILD &&
			!$params[TreeWidget::PARAM_TYPE_FIX]['blockTitle'])
				$params[TreeWidget::PARAM_TYPE_FIX]['blockToggleType'] = self::BLOCK_TOGGLE_TYPE_ICON;
		
		$params[TreeWidget::PARAM_TYPE_FIX]['_headerEnabled'] = $params[TreeWidget::PARAM_TYPE_FIX]['blockTitle'] ||
			$params[TreeWidget::PARAM_TYPE_FIX]['blockToggleType'] ||
			$params[TreeWidget::PARAM_TYPE_FIX]['blockBorder'];
	}
	
	/**
	 * Специальная обработка параметров уровня "attibutes"
	 * @param array $params Параметры уровня
	 */
	protected function preparationAttributes(&$params){
		if ($params[TreeWidget::PARAM_TYPE_FIX]['attrHidden'])
			Html::mergeClasses($params[TreeWidget::PARAM_TYPE_FIX]['htmlOptions'], self::CLASS_ATTRIBUTES_EMPTY, true);
	}
	
	/**
	 * Специальная обработка параметров уровня "features"
	 * @param array $params Параметры уровня
	 */
	protected function preparationFeatures(&$params){
		$params_fix = &$params[TreeWidget::PARAM_TYPE_FIX];
		
		if (!$params_fix['featureType'])
			$params_fix['featureType'] =
				$params_fix['featureName'] === 'password' ? 'password' : self::FEATURE_TYPE_DEF;
		
		if (in_array($params_fix['featureType'], array('text', 'password', 'checkbox')) &&
			$params_fix['_fullness'] >= Swamper::FULLNESS_MODEL &&
			$params_fix['featureModel']->isAttributeRequired($params_fix['featureName']))
				Html::mergeClasses($params_fix['htmlOptions'], 'required', true);
		
		if ($params_fix['featureType'] === 'error')
			Html::mergeClasses($params_fix['htmlOptions'], Html::$errorMessageCss, true);
		
		if ($params_fix['featureValue'] === null && $params_fix['featureModel'] && $params_fix['featureName']){
			$params_fix['featureValue'] = $params_fix['featureModel']->$params_fix['featureName'];
			$params_fix['_fullness'] = Swamper::dataFullness($params_fix);
		}
		
		/* if (in_array($params_fix['featureType'],
			array('label', 'text', 'password', 'textarea', 'checkbox', 'radio', 'widget')) &&
			$params_fix['_fullness'] >= Swamper::FULLNESS_MODEL &&
			!empty($params_fix['featureModel']->accessAttributes[$params_fix['featureName']]) &&
			$params_fix['featureModel']->accessAttributes[$params_fix['featureName']] === 'view'){
				if ($params_fix['featureType'] !== 'widget'){
					if (!isset($params_fix['htmlOptions']['disabled']))
						$params_fix['htmlOptions']['disabled'] = 'disabled';
					if ($params_fix['featureType'] !== 'label')
						Html::mergeClasses($params_fix['htmlOptions'], 'readonly', true);
				}else{
					if (!isset($params_fix['widgetParams']['disabled']))
						$params_fix['widgetParams']['disabled'] = true;
					if (!isset($params_fix['widgetParams']['readonly']))
						$params_fix['widgetParams']['readonly'] = true;
				}
			} */
		
		if ($params_fix['featureType'] === 'widget' &&
			$params_fix['widgetName']){
				$params_fullness = Swamper::dataFullnessCorrect($params_fix);
				$params_append = array();
				
				if (Arrays::pop($params[TreeWidget::PARAM_TYPE_TRANS], 'featureTransition') !== false)
					foreach (self::TRANSITION_PROPERTIES as $property)
						if (isset($params_fullness[$property]))
							$params_append[$property] = $params_fullness[$property];
				
				if (!empty($params_fix['widgetParams']['id'])){
					$id = $params_fix['widgetParams']['id'];
					
					if (mb_substr($id, 0, 1) === '!')
						$params_fix['widgetParams']['id'] = eval('return '.mb_substr($id, 1).';');
				}
				
				$params_fix['widgetParams'] = is_array($params_fix['widgetParams']) ?
					array_merge($params_append, $params_fix['widgetParams']) : $params_append;
			}
	}
	
	/**
	 * Обработка данных на предмет корректности и целостности табличных параметров
	 * @param mixed $data Исходный массив параметров
	 * @return array|boolean Обработанный массив данных или false
	 */
	protected function preparationTables(&$data){
		if (!$data || !is_array($data))
			return false;
		
		$block_keys = $attr_nested_keys = array();
		$block_cols = $attr_nested_cols = $qu_attr = 0;
		$block_indent_hor = $attr_indent_hor = $header_enabled = false;
		
		foreach ($data['blocks'] as $key_block=>&$block){
			if (!$block['blockSeparate'] && $block_keys && $block['blockIndentHor'] !== $block_indent_hor)
				$block['blockSeparate'] = true;
			
			if ($block['blockIndentVer'] === null && $block_keys)
				$block['blockIndentVer'] = true;
			
			if ($block_keys && ($block['blockIndentHor'] !== $block_indent_hor || $block['blockSeparate'])){
				$extension = $attr_nested_keys && !$header_enabled && !$qu_attr;
				
				if ($attr_nested_keys)
					$this->preparationTableAttrsNested($data, $attr_nested_keys, $attr_nested_cols, $extension);
				
				if (!$extension)
					$this->preparationTableBlocks($data, $block_keys, $block_cols);
				
				$qu_attr = 0;
				$header_enabled = false;
			}
			
			$block_indent_hor = $block['blockIndentHor'];
			
			$header_enabled = $header_enabled || $block['_headerEnabled'];
			
			$block_keys[] = $key_block;
			
			$attr_indent_ver_first = $block['blockIndentVer'] && !$block['_headerEnabled'];
			
			foreach ($block['attributes'] as $key_attr=>&$attr){
				if ($attr['attrIndentHor'] !== $attr_indent_hor && $attr_nested_keys)
					$this->preparationTableAttrsNested($data, $attr_nested_keys, $attr_nested_cols);
				
				if ($attr_indent_hor = $attr['attrIndentHor'])
					$attr_nested_keys[$key_block][] = $key_attr;
				
				if ($block['_headerEnabled'])
					Html::mergeClasses($attr['htmlOptions'], self::CLASS_ATTRIBUTES_PID.$block['htmlOptions']['id'], true);
				
				if (!$attr['attrIndentVer'] && $attr_indent_ver_first)
					$attr['attrIndentVer'] = true;
				
				$attr_indent_ver_first = false;
				
				$attr_cols = 0;
				foreach ($attr['cells'] as &$cell){
					if ($attr['attrIndentVer'])
						Html::mergeClasses($cell['htmlOptions'], self::CLASS_INDENT_TOP, true);
					
					$attr_cols += !empty($cell['htmlOptions']['colspan']) ? $cell['htmlOptions']['colspan'] : 1;
				}
				unset($cell);
				
				if ($attr_indent_hor)
					$attr_nested_cols = max($attr_nested_cols, $attr_cols);
				else
					$qu_attr++;
				
				$block_cols = max($block_cols, $attr_indent_hor ? 1 : $attr_cols);
				
				$attr['_cols'] = $attr_cols;
			}
			unset($attr);
		}
		unset($block);
		
		$extension = $attr_nested_keys && !$header_enabled && !$qu_attr;
		
		if ($attr_nested_keys)
			$this->preparationTableAttrsNested($data, $attr_nested_keys, $attr_nested_cols, $extension);
		
		if ($block_keys && !$extension)
			$this->preparationTableBlocks($data, $block_keys, $block_cols);
		
		return $data;
	}
	
	/**
	 * Учёт колонок конкретной таблицы блоков 
	 * @param array $data Исходный массив параметров
	 * @param array $block_keys Список ключей блоков, входящих в таблицу
	 * @param array $cols Максимальное число колонок во всех строках таблицы
	 */
	protected function preparationTableBlocks(&$data, &$block_keys, &$block_cols){
		foreach ($block_keys as $key){
			$block = &$data['blocks'][$key];
			$block['_cols'] = $block_cols;
			
			foreach ($block['attributes'] as &$attr)
				if (!$attr['attrIndentHor'])
					$attr['_residue'] = $block_cols - $attr['_cols'];
			unset($attr, $block);
		}
		
		$block_keys = array();
		$block_cols = 0;
	}
	
	/**
	 * Учёт колонок конкретной таблицы вложенных атрибутов
	 * @param array $data Исходный массив параметров
	 * @param array $attr_nested_keys Двухмерный массив ключей атрибутов, входящих в таблицу
	 * @param array $cols Максимальное число колонок во всех строках таблицы
	 */
	protected function preparationTableAttrsNested(&$data, &$attr_nested_keys, &$attr_nested_cols,
		$extension = false){
			foreach ($attr_nested_keys as $key_block=>$attr_keys){
				$block = &$data['blocks'][$key_block];
				
				if ($extension){
					$block['blockIndentHor'] = true;
					$block['_cols'] = $attr_nested_cols;
				}
				
				foreach ($attr_keys as $key_attr){
					$attr = &$block['attributes'][$key_attr];
					$attr['_residue'] = $attr_nested_cols - $attr['_cols'];
					
					if ($extension)
						$attr['attrIndentHor'] = null;
					
					unset($attr);
				}
				
				unset($block);
			}
			
			$attr_nested_keys = array();
			$attr_nested_cols = 0;
		}
	
	/**
	 * Смена уровня вложенности данных для представления
	 * @param integer $table_level Текущий уровень вложенности
	 * @param integer $new_level Необходимый уровень вложенности
	 * @param array $htmlOptions HTML-опции табличного тега
	 * @param string $td_colspan Значение HTML-параметра colspan 
	 */
	protected function tableLevelChange(&$table_level, $new_level, $htmlOptions = array(), $td_colspan = false){
		$this->render('data_table_level', array(
			'table_level'=>$table_level,
			'new_level'=>$new_level,
			'htmlOptions'=>$htmlOptions,
			'td_colspan'=>$td_colspan
		));
		$table_level = $new_level;
	}
	
}
