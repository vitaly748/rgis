<?php
Yii::app()->asset->add();
	
$table_level = 0;

echo Html::openTag('div', $data['htmlOptions']);
	
	foreach ($data['blocks'] as $block){
		
		if ($block['blockSeparate'])
			$this->tableLevelChange($table_level, 0);
			
			
		if (!$table_level || $block['blockSeparate'])
			$this->tableLevelChange($table_level, 1,
				Html::forming(array(static::CLASS_INDENT_LEFT=>$block['blockIndentHor'])));
		
		if ($block['_headerEnabled']){
			echo Html::openTag('tr', $block['htmlOptions']);
				
				echo Html::openTag('td', Html::forming(array(
					'colspan'=>$block['_cols'],
					static::CLASS_INDENT_TOP=>$block['blockIndentVer']
				)));
					
					echo Html::openTag('div');
						
						if ($input = $block['blockToggleType'] && $block['_fullness'])
							echo Html::element(array_merge($block, array('type'=>'hidden', 'htmlOptions'=>false)));
						$input_id = $input ? ($block['_fullness'] >= Swamper::FULLNESS_MODEL ?
							Html::activeId($block['blockToggleModel'], $block['blockToggleName']) :
							$block['blockToggleName']) : false;
						
						if ($block['blockTitle'])
							if ($block['blockToggleType'] === static::BLOCK_TOGGLE_TYPE_INBUILD)
								echo Html::tag('a', Html::forming(array(static::CLASS_BLOCKS_TITLE, 'dashed', 'pocket-toggle',
									'setter'=>$input,
									'set-id-'.$input_id=>$input,
									'active'=>!$block['blockHidden'],
									'href'=>'#'
								)), $block['blockTitle']);
							else
								echo Html::tag('div', array('class'=>static::CLASS_BLOCKS_TITLE), $block['blockTitle']);
						
						if ($block['blockToggleType'] === static::BLOCK_TOGGLE_TYPE_ICON)
							echo Html::tag('div', Html::forming(array(static::CLASS_BLOCKS_TOGGLE,
								'setter'=>$input,
								'set-id-'.$input_id=>$input,
								'active'=>!$block['blockHidden']
							)), '');
						
						if ($block['blockBorder'])
							echo Html::tag('div', array('class'=>static::CLASS_BLOCKS_BORDER), '');
						
					echo Html::closeTag('div');
					
				echo Html::closeTag('td');
				
			echo Html::closeTag('tr');
		}
		
		foreach ($block['attributes'] as $attr){
			
			$this->tableLevelChange($table_level, 1 + $attr['attrIndentHor'],
				array('class'=>static::CLASS_INDENT_LEFT), $block['_cols']);
			
			echo Html::openTag('tr', $attr['htmlOptions']);
				
				foreach ($attr['cells'] as $cell){
					
					echo Html::openTag('td', $cell['htmlOptions']);
						
						echo Html::openTag('div', Html::forming(array('dsp-none'=>$block['blockHidden'] || $attr['attrHidden'])));
							
							foreach ($cell['features'] as $feature)
								echo Html::element($feature);
							
						echo Html::closeTag('div');
						
					echo Html::closeTag('td');
				}
				
				if ($attr['_residue'])
					echo Html::tag('td', Html::forming(array(
						static::CLASS_INDENT_TOP=>$attr['attrIndentVer'],
						'colspan'=>$attr['_residue']
					)), '');
				
			echo Html::closeTag('tr');
		}
	}
	
	$this->tableLevelChange($table_level, 0);
	
echo Html::closeTag('div');
