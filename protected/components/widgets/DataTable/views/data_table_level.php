<?php
while ($table_level != $new_level){
	$open = $new_level > $table_level;
	$table_level = $table_level + 2 * $open - 1;
	
	if ($open){
		if ($table_level == 2)
			echo Html::openTag('tr', array('class'=>'container')).
				Html::openTag('td', Html::forming(array('colspan'=>$td_colspan)));
		
		echo Html::openTag('table', $htmlOptions);
	}else{
		echo Html::closeTag('table');
		
		if ($table_level == 1)
			echo Html::closeTag('td').Html::closeTag('tr');
	}
}
