function wDataTableAttrToggle(tr, show){
	tr.each(function(){
		var divs = $(this).children('td').children('div'),
			visible = divs.is(':visible');
		
		if (divs.length)
			if (show && !visible){
				$(this).removeClass('dt-attr-hidden');
				divs.removeClass('dsp-none').hide().slideDown('fast', function(){
					$(this).css('overflow', 'visible');
				});
			}else if (!show && visible){
				$(this).addClass('dt-attr-hidden');
				divs.slideUp('fast');
			}
	});
}

$(window).load(function(){
	$(document).on('click', 'div.data-table a.dt-block-title, div.data-table div.dt-block-toggle', function(event){
		$(this).toggleClass('active').change();
		
		var active = $(this).hasClass('active'),
			tr = $(this).closest('tr');
		
		if (tr.length === 1){
			var id = tr.attr('id');
			
			if (id){
				var divs = $('div.data-table tr.pid-' + id + ' td > div');
				
				if (divs.length)
					if (active){
						$('div.data-table tr.pid-' + id + ' td').
							removeClass('no-padding');
						divs.slideDown('normal', function(){
							$(this).css('overflow', 'visible');
						});
					}else
						divs.slideUp('normal', function(){
							$(this).parent().addClass('no-padding');
						});
			}
		}
		
		event.preventDefault();
	});
});
