<?php
/**
 * Класс, переопределяющий и дополняющий стандартный виджет CCaptcha
 * @author v.zubov
 * @see CCaptcha
 */
class Code extends Widget{
	
	/**
	 * @var array HTML-опции объекта
	 */
	public $htmlOptions;
	
	/**
	 * Запуск виджета
	 */
	public function run(){
		$this->render('code', array(
			'html_options'=>$this->preparationHtmlOptions()
		));
	}
	
}
