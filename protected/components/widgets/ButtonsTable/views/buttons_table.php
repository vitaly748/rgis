<?php
Yii::app()->asset->add();

echo Html::openTag('div', $html_options);
	echo Html::openTag('div', array('class'=>'bt-aligner w100 cover'));
		
		foreach ($buttons as $button)
			if ($button['type'] == 'submit')
				echo Html::button($button['description'], $button['html_options']);
			elseif ($button['type'] == 'ajax_submit')
				echo Html::ajaxSubmitButton($button['description'], Arrays::pop($button['html_options'], 'href', true),
					!empty($button['ajax_params']) ? $button['ajax_params'] : array(), $button['html_options']);
			else
				echo Html::tag($button['type'], $button['html_options'], $button['description']);
		
	echo Html::closeTag('div');
echo Html::closeTag('div');

$this->registerScriptParams(array(
	'compression'=>$compression,
	'widthMax'=>$width_max,
	'horizontalPadding'=>$horizontal_padding,
	'verticalAlign'=>$vertical_align
));
