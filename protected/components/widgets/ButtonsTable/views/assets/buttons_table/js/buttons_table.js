wButtonsTable = {};
wButtonsTableReserv = 20;

function wButtonsTableRepaint(){
	if (typeof wButtonsTable.width == 'undefined'){
		var htmlTable = '<table class="bt-table"><tbody></tbody></table>';
		
		for (id in wButtonsTable)
			if (id !== 'width'){
				var bt = $('div#' + id + '.buttons-table');
				
				if (bt.length === 1){
					var aligner = bt.children('div.bt-aligner'),
						buttons = [],
						horizontalPadding = wButtonsTable[id].horizontalPadding;
					
					preparationRender(aligner, function(){
						aligner.children('.button').each(function(){
							buttons.push({
								html: $(this)[0].outerHTML,
								width: $(this).innerWidth() + 2 * horizontalPadding + 1
							});
						});
						
						aligner.removeClass('cover');
						aligner.html(htmlTable);
					});
					
					wButtonsTable[id].buttons = buttons;
					wButtonsTable[id].cols = 0;
				}
			}
	}
	
	for (id in wButtonsTable)
		if (id !== 'width'){
			var bt = $('div#' + id + '.buttons-table');
			
			if (bt.length === 1 && bt.is(':visible')){
				var params = wButtonsTable[id];
				
				if (typeof params.buttons != 'undefined'){
					var aligner = bt.children('div.bt-aligner'),
						table = aligner.children('table.bt-table'),
						widthMax = (params.widthMax ? $('body').width() : bt.width()) - wButtonsTableReserv,
						buttonsLen = params.buttons.length,
						cols = colsMax = width = count = 0,
						rows = 1;
					
					while (count < buttonsLen){
						cols++;
						
						var widthNew = width + params.buttons[count]['width'],
							widthOk = widthNew < widthMax,
							fullRow = colsMax && cols === colsMax,
							last = count === buttonsLen - 1;
						
						if (!widthOk || fullRow){
							if (!colsMax){
								colsMax = cols - 1;
								
								if (!colsMax)
									break;
								
								rows++;
							}else if (fullRow){
								rows++;
								count++;
							}else{
								colsMax = cols - 1;
								count = 0;
								rows = 1;
							}
							
							cols = width = 0;
						}else{
							if (!last)
								width = widthNew;
							else if (!colsMax)
								colsMax = cols;
							
							count++;
						}
					}
					
					if (colsMax > 0 && params.cols !== colsMax){
						var html = '',
							countTr = countTd = 0,
							trOpen = false;
						
						$.each(params.buttons, function(key, button){
							if (!trOpen){
								html += '<tr>';
								countTr++;
								trOpen = true;
							}
							
							html += '<td class="va-' + params.verticalAlign + '">' + button.html + '</td>';
							countTd++;
							
							if (countTd === colsMax){
								html += '</tr>';
								countTd = 0;
								trOpen = false;
							}
						});
						
						if (trOpen){
							if (countTr > 1){
								var colspan = colsMax - countTd;
								
								html += '<td' + (colspan > 1 ? ' colspan="' + colspan + '"' : '') + '></td>';
							}
							
							html += '</tr>';
						}
						
						table.children('tbody').html(html);
						
						if (!params.compression || countTr > 1){
							aligner.addClass('w100');
							table.addClass('w100');
						}else{
							aligner.removeClass('w100')
							table.removeClass('w100')
						}
						
						table.find('.button').each(function(index, button){
							$(button).focus().blur();
						});
						
						params.cols = colsMax;
					}
				}
			}
		}
	
	wButtonsTable.width = $(document).width();
};

/*function wButtonsTableAjaxSuccess(data){
	$(this).closest('form').find('div.errorMessage').hide().parent('div').removeClass('error');
	
	$.each(data.errors, function(id, errors){
		var errorMessage = $('div#' + id + '_em_');
		
		if (errorMessage.length){
			errorMessage.html(errors[0]);
			errorMessage.show().parent('div').addClass('error');
		}
	});
}*/

$(window).load(function(){
	wButtonsTableRepaint();
	
	/*$(document).click(function(){
		$('#ywp6').removeClass('dsp-none').dialog('open');
		wButtonsTableRepaint();
	});*/
	
});

$(window).resize(function(){
	if (typeof wButtonsTable.width != 'undefined' && wButtonsTable.width !== $(document).width())
		wButtonsTableRepaint();
});
