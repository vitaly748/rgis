<?php
/**
 * Виджет блока кнопок
 * @author v.zubov
 */
class ButtonsTable extends Widget{
	
	/**
	 * @var string Название основной папки представлений
	 */
	const DIR_VIEWS = 'views';
	
	/**
	 * @var integer Максимальная глубина просмотра стэка вызовов при поиске файла текущего представления
	 */
	const MAX_DEPTH_VIEW = 4;
	
	/**
	 * @var array Список доступных значений параметра "type" (тип кнопки)
	 */
	const BUTTON_TYPES = array('a', 'div', 'command', 'submit', 'ajax_submit');
	
	/**
	 * @var string Тип кнопки по умолчанию
	 */
	const BUTTON_TYPE_DEF = 'a';
	
	/**
	 * @var array Список групп символьных идентификаторов кнопок в порядке увеличения приоритета
	 */
	const BUTTON_PRIORITY = array(
		array('create', 'back', 'esia', 'login'),
		array('next', 'ok', 'logout', 'update'),
		array('delete', 'deploy', 'cancel')
	);
	
	/**
	 * @var integer Порядок приоритета кнопки по умолчанию
	 */
	const BUTTON_PRIORITY_DEF = 1;
	
	/**
	 * @var array Список символьных идентификаторов кнопок, которые имеют тип "submit" по умолчанию
	 */
	const BUTTON_CONFIRM_NAMES_DEF = array('ok', 'next', 'save');
	
	/**
	 * @var array Список символьных идентификаторов кнопок, которые имеют тип "command" по умолчанию
	 */
	const BUTTON_COMMAND_NAMES_DEF = array('back', 'deploy', 'accept', 'redirect', 'return', 'request', 'answer', 'complete',
		'deny', 'rollback', 'esiadone', 'extend');
	
	/**
	 * @var string Префикс символьного идентификатора подписи кнопок в Справочнике сообщений
	 */
	const BUTTON_DESCRIPTION_PREFIX = 'button_';
	
	/**
	 * @var array Список символьных идентификаторов кнопок, к которым будет применён класс "orange"
	 */
	const BUTTON_ORANGE = array('esia', 'esiadone', 'delete', 'deny', 'deploy', 'disconnection', 'i', 'revoke', 'rollback',
		'extend', 'activate', 'restore');
	
	/**
	 * @var array Список символьных идентификаторов кнопок, к которым будет применён класс "gray"
	 */
	const BUTTON_GRAY = array('cancel', 'no', 'back');
	
	/**
	 * @var array Ассоциативный массив с описанием наборов кнопок по умолчанию при различных операциях модели
	 */
	const ACTION_BUTTONS_DEF = array(
		'list'=>array('create'),
		'create'=>array('ok'),
		'view'=>array('update', 'delete'),
		'update'=>array('ok', 'delete'),
		'restore'=>array('ok')
	);
	
	/**
	 * @var string Горизонтальное выравнивание для кнопок по умолчанию
	 */
	const HORIZONTAL_ALIGN_DEF = 'left';
	
	/**
	 * @var string Горизонтальное выравнивание для кнопок внутри всплывающего окна по умолчанию
	 */
	const HORIZONTAL_ALIGN_JD_DEF = 'right';
	
	/**
	 * @var boolean Признак тесного расположения кнопок в один ряд по умолчанию
	 */
	const WIDTH_MAX_DEF = false;
	
	/**
	 * @var boolean Признак тесного расположения кнопок в один ряд внутри всплывающих окон по умолчанию
	 */
	const WIDTH_MAX_JD_DEF = false;
	
	/**
	 * @var integer Размер горизонтальных полей для ячеек кнопок по умолчанию
	 */
	const HORIZONTAL_PADDING_DEF = 10;
	
	/**
	 * @var integer Размер горизонтальных полей для ячеек кнопок внутри всплывающих окон по умолчанию
	 */
	const HORIZONTAL_PADDING_JD_DEF = 5;
	
	/**
	 * @var array HTML-опции объекта
	 */
	public $htmlOptions;
	
	/**
	 * @var array Ассоциативный массив параметров кнопок в формате:
	 * 	array(
	 * 		{Символьный идентификатор кнопки},// Сокращённая форма записи
	 * 		{Символьный идентификатор кнопки}=>'a',// Сокращённая форма записи с указанием типа кнопки
	 * 		{Символьный идентификатор кнопки}=>{// Расширенная форма записи
	 * 			'type'=>'a',// Тип кнопки. Необязательный параметр
	 * 			// Набор стандартный параметров ajax-запроса. Необязательный параметр. По умолчанию равно array()
	 * 			'ajax_params'=>array(),
	 * 			'html_options'=>array(),// HTML-опции кнопки. Необязательный параметр. По умолчанию равно array()
	 * 			'check_access'=>true,// Признак проверки прав доступа к кнопке. Необязательный параметр. По умолчанию равно true
	 * 		},
	 * 		...
	 * 	)
	 * Массив параметров кнопок включает в себя набор стандартных HTML-опций кнопки, а так же дополнительные
	 * параметры "type" и "ajax_params"
	 */
	public $params;
	
	/**
	 * @var CActiveForm Экземпляр HTML-формы
	 */
	public $form;
	
	/**
	 * @var CModel Модель данных, связанная с данным виджетом
	 */
	public $model;
	
	/**
	 * @var boolean Признак нерастягивания таблицы кнопок на всю ширину контейнера виджета. По умолчанию равно true
	 */
	public $compression = true;
	
	/**
	 * @var boolean Признак тесного расположения кнопок в один ряд
	 */
	public $widthMax;
	
	/**
	 * @var string Горизонтальное выравнивание кнопок. Используемые значения: left, center, right
	 */
	public $horizontalAlign;
	
	/**
	 * @var string Вертикальное выравнивание кнопок. Используемые значения: top, middle, bottom. По умолчанию
	 * равно middle
	 */
	public $verticalAlign = 'middle';
	
	/**
	 * @var intger Значение CSS-свойства "padding" по горизонтали для кнопок
	 */
	public $horizontalPadding;
	
	/**
	 * @var boolean Признак объединения заданных параметров со стандартыми. По умолчанию равно true
	 */
	public $mergeParams = true;
	
	/**
	 * @var string Класс кнопок: стандартный, "orange", "gray" или "contur". По умолчанию равен стандартному
	 */
	public $class = '';
	
	/**
	 * @var array Ассоциативный массив параметров кодов подтверждения введённых данных текущей модели
	 */
	private $_confirmParams;
	
	/**
	 * Запуск виджета
	 */
	public function run(){
		if ($buttons = $this->preparation())
			$this->render('buttons_table', array(
				'html_options'=>$this->preparationHtmlOptions(),
				'buttons'=>$buttons,
				'compression'=>(boolean)$this->compression,
				'width_max'=>$this->widthMax !== null ? $this->widthMax :
					(Yii::app()->controller->juid ? static::WIDTH_MAX_JD_DEF : static::WIDTH_MAX_DEF),
				'vertical_align'=>$this->verticalAlign,
				'horizontal_padding'=>$this->horizontalPadding ? $this->horizontalPadding :
					(Yii::app()->controller->juid ? static::HORIZONTAL_PADDING_JD_DEF : static::HORIZONTAL_PADDING_DEF),
				'confirm_params'=>$this->_confirmParams
			));
	}
	
	/**
	 * Переопределение стандартой функции preparationHtmlOptions, подготовливающей HTML-опций для виджета
	 * @return array Ассоциативный массив HTML-опций
	 */
	protected function preparationHtmlOptions(){
		parent::preparationHtmlOptions();
		
		$this->htmlOptions['class'] .= ' ta-'.($this->horizontalAlign ? $this->horizontalAlign :
			(Yii::app()->controller->juid ? static::HORIZONTAL_ALIGN_JD_DEF : static::HORIZONTAL_ALIGN_DEF));
		
		return $this->htmlOptions;
	}
	
	/**
	 * Определение списка доступных кнопок, определение параметров отображения каждой из них
	 * @return array Ассоциативный массив параметров доступных кнопок в формате:
	 * 	array(
	 * 		{Символьный идентификатор кнопки}=>array(
	 * 			'type'=>'a',// Тип кнопки
	 * 			'html_options'=>{HTML-опции кнопки},
	 * 			'description'=>'ok',// Надпись на кнопке
	 * 			'ajax_params'=>{Параметры Ajax-запроса}
	 * 		),
	 * 		...
	 * 	)
	 */
	protected function preparation(){
		$params = $buttons = array();
		$model = $this->model instanceof CModel ? $this->model : false;
		
		if ($this->params)
			foreach ($this->params as $name=>$value){
				if (is_int($name))
					list($name, $value) = array($value, false);
				
				if (is_string($name))
					if (is_string($value))
						$params[$name] = array('type'=>$value);
					else
						$params[$name] = is_array($value) ? $value : array();
			}
		
		if (!$params || $this->mergeParams){
			$params_def = array();
			
			if ($states = Yii::app()->controller->chainStates){
				$action = Yii::app()->controller->action->id;
				
				$params_def = Arrays::forming(array(
					'back'=>$action !== reset($states),
					'next'=>$action !== end($states),
					'ok'=>$action === end($states)
				), true);
			}elseif ($model && ($button_types = Arrays::pop($abd = static::ACTION_BUTTONS_DEF, $model->action)))
				$params_def = $button_types;
			
			if ($params_def)
				$params_def = array_fill_keys($params_def, array());
			
			$buttons = !$params ? $params_def : array_merge($params_def, $params);
		}else
			$buttons = $params;
		
		if ($buttons){
			foreach ($buttons as $name=>&$params){
				$params['_priority'] = static::BUTTON_PRIORITY_DEF;
				
				foreach (static::BUTTON_PRIORITY as $priority=>$names)
					if (in_array($name, $names)){
						$params['_priority'] = $priority;
						break;
					}
			}
			unset($params);
			
			Arrays::sort($buttons, '_priority');
			
			foreach ($buttons as $name=>&$params){
				if ($model){
					$check_access = !isset($params['check_access']) || $params['check_access'];
					
					if ($check_access && !$model->checkAccess($name, false)){
						unset($buttons[$name]);
						continue;
					}
				}
				
				if (!isset($params['html_options']))
					$params['html_options'] = array();
				
				Html::mergeClasses($params['html_options'], array('button', $name, $this->class), true);
				
				if (empty($params['type']) || !in_array($params['type'], static::BUTTON_TYPES))
					$params['type'] = in_array($name, static::BUTTON_CONFIRM_NAMES_DEF) ? 'submit' :
						(in_array($name, static::BUTTON_COMMAND_NAMES_DEF) ? 'command' : static::BUTTON_TYPE_DEF);
				
				if ($params['type'] === 'command'){
					if (!Strings::hasWord($params['html_options']['class'], Html::PREFIX_BUTTON_STARTER_COMMAND))
						Html::mergeClasses($params['html_options'], Html::PREFIX_BUTTON_STARTER_COMMAND, true);
					
					$params['type'] = static::BUTTON_TYPE_DEF;
					
					if (empty($params['html_options']['href']))
						$params['html_options']['href'] = '#';
				}elseif ($params['type'] === 'submit')
					$params['html_options']['type'] = 'submit';
				/* elseif ($params['type'] == 'ajax_confirm')
					if (!$model || !$this->form || !($confirm_params = $model->codeConfirmParams))
						$params['type'] = 'submit';
					else{
						$params['type'] = 'ajax_submit';
						$this->form->confirmParams = $confirm_params;
						
						if (!isset($params['ajax_params']))
							$params['ajax_params'] = array(
								'dataType'=>'json',
								'context'=>'js:$(this)',
								'success'=>'wButtonsTableAjaxSuccess'
							);
					} */
				
				if (empty($params['html_options']['href'])){
					if ($url = Arrays::pop($params, 'ajax_params.url', true))
						$params['html_options']['href'] = $url;
					elseif (!in_array($params['type'], array('submit', 'ajax_submit')))
						if (in_array($name, array('back', 'cancel')))
							$params['html_options']['href'] = '#';
						elseif ($params['type'] != 'a')
							$params['html_options']['href'] = Yii::app()->controller->route;
						else{
							$controller = Yii::app()->controller->id;
							$action = Yii::app()->controller->action->id;
							
							if ($controller == 'user' && in_array($action, array('pview', 'pupdate', 'pdelete')))
								$params['html_options']['href'] = Yii::app()->createUrl("$controller/p$name");
							else
								$params['html_options']['href'] = Yii::app()->createUrl("$controller/$name",
									!empty($model->id) ? array('id'=>$model->id) : array());
						}
				}
				
				if ($classes = Strings::devisionWords($params['html_options']['class'], false, ' '))
					if (array_intersect($classes, static::BUTTON_ORANGE))
						Html::mergeClasses($params['html_options'], 'orange', true);
					elseif (array_intersect($classes, static::BUTTON_GRAY))
						Html::mergeClasses($params['html_options'], 'gray', true);
				
				if (empty($params['description'])){
					$description = Yii::app()->label->get(static::BUTTON_DESCRIPTION_PREFIX.$name);
					$params['description'] = $description ? $description : $name;
				}
			}
			unset($params);
		}
		
		return $buttons;
	}
	
}
