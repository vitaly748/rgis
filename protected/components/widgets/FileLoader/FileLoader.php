<?php
/**
 * Виджет "Загрузчик файлов"
 * @author v.zubov
 */
class FileLoader extends Widget{
	
	/**
	 * @var integer Количество попыток загрузки файлов
	 */
	const RETRIES = 3;
	
	/**
	 * @var array HTML-опции объекта
	 */
	public $htmlOptions;
	
	/**
	 * @var CModel Экземпляр модели данных
	 */
	public $model;
	
	/**
	 * @var string Название элемента
	 */
	public $name;
	
	/**
	 * @var string Значение элемента
	 */
	public $value;
	
	/**
	 * @var integer Максимальный размер файла в байтах
	 */
	public $maxSize = 50 * 1024 * 1024;
	
	/**
	 * @var integer Максимальное количество прикрепляемых файлов
	 */
	public $maxQu = 10;
	
	/**
	 * @var array Список допустимых форматов прикрепляемых файлов. Если не задано, то используются все допустимые форматы
	 * @see Files::MIME_TYPES
	 */
	public $mimeTypes;
	
	/**
	 * @var boolean Признак возможности выбора нескольких файлов. По умолчанию равно true
	 */
	public $multiple = true;
	
	/**
	 * Запуск виджета
	 */
	public function run(){
		if ($this->model && $this->name)
			$this->value = $this->model->{$this->name};
		
		if ($this->name)
			$this->render('file_loader', array(
				'html_options'=>$this->preparationHtmlOptions(),
				'model'=>$this->model,
				'name'=>$this->name,
				'value'=>$this->value,
				'max_size'=>$this->maxSize,
				'max_qu'=>$this->maxQu,
				'mime_types'=>$this->mimeTypes ? $this->mimeTypes : Arrays::filterKey(Files::MIME_TYPES,
					'bmp, doc, docx, jpeg, pdf, png, txt, xls, xlsx'),
				'retries'=>static::RETRIES,
				'multiple'=>$this->multiple
			));
	}
	
}
