wFileLoader = {};
wFileLoaderPrefixListItem = 'fl-item';
wFileLoaderParamProgress = 'PHP_SESSION_UPLOAD_PROGRESS';
wFileLoaderParamCancel = 'download_cancel';

function wFileLoaderListModify(id, idFile, action, param){
	var body = $('div.file-loader#' + id + ' table.fl-list tbody');
	
	if (body.length)
		if (action === 'add'){
			var tr = $('<tr class="' + wFileLoaderPrefixListItem + '-' + idFile + '">' +
        		'<td class="fl-list-name"><div style="display:none">' + param + '</div></td>' +
        		'<td class="fl-list-progress"><div style="display:none"><div class="fl-progress-bar"><p></p><span></span></div>' +
        		'<a class="fl-button-cancel" href="#"></a></div></td></tr>');
			
			tr.appendTo(body);
			tr.find('td > div').slideDown();
		}else if (action === 'clear')
			body.children('tr').detach();
		else{
			var tr = body.children('tr.' + wFileLoaderPrefixListItem + '-' + idFile);
			
			if (tr.length)
				switch (action){
					case 'error':
						tr.addClass('complete').find('div.fl-progress-bar p').hide();
						tr.find('div.fl-progress-bar span').html(wFileLoader.textError).show();
						
						break;
					case 'success':
						tr.addClass('complete').find('div.fl-progress-bar p').css('width', '100%');
						
						break;
					case 'wait':
						tr.find('div.fl-progress-bar p').css('width', 0);
						
						break;
					case 'progress':
						if (!tr.hasClass('complete'))
							tr.find('div.fl-progress-bar p').css('width', param + '%');
						
						break;
					case 'cancel':
						tr.find('td > div').slideUp(function(){
							tr.detach();
						});
				}
		}
}

function wFileLoaderCheck(id, idFile){
	var params = wFileLoader[id];
	
	if (params && params.progress == idFile && !params.check){
		var file = params.stack[idFile];
		
		params.check = true;
		
		if (file && !file.serverName)
			$.ajax({
				type: 'POST',
				data: wFileLoaderParamProgress + '=' + idFile,
				cache: false,
				dataType: 'json',
				processData: false,
				success: function(respond, textStatus, jqXHR){
					if (respond && respond.files && respond.files[file.name])
						wFileLoaderListModify(id, idFile, 'progress', Math.round(respond.files[file.name] * 100 / file.size));
				},
				complete: function(jsXHR, textStatus){
					params.check = false;
					
					setTimeout(function(){
						wFileLoaderCheck(id, idFile);
					}, 10);
		        }
			});
	}
}

function wFileLoaderProgress(id){
	var params = wFileLoader[id],
		retries = 0,
		search = false,
		buttons = $('div.file-loader#' + id).closest('.jui-dialog, form').find('div.buttons-table .button');
	
	if (params)
		while (!params.progress && retries < wFileLoader.retries && (!retries || search)){
			for (idFile in params.stack){
				var file = params.stack[idFile];
				
				if (file.retries === retries && !file.serverName){
					var data = new FormData();
					
					if (buttons.length)
						buttonToggle(buttons, false);
					
					params.progress = idFile;
					data.append(wFileLoaderParamProgress, idFile);
					data.append(0, file);
					
					$.ajax({
						type: 'POST',
						data: data,
						cache: false,
						dataType: 'json',
						processData: false,
						contentType: false,
						beforeSend: function(){
							setTimeout(function(){
								wFileLoaderCheck(id, idFile);
							}, 10);
						},
						success: function(respond, textStatus, jqXHR){
							if (respond && respond.files && respond.files[file.name] && params.progress == idFile &&
								params.stack[idFile]){
									file.serverName = respond.files[file.name];
									
									var input = $('div.file-loader#' + id + ' input[type=hidden]');
									
									if (input.length){
										var inputValue = input.val();
										
										input.val(inputValue ? inputValue + ',' + file.serverName : file.serverName).change();
									}
								}else
									jqXHR.status = 500;
						},
						error: function(jqXHR, textStatus, errorThrown){
							jqXHR.status = 500;
						},
				        complete: function(jsXHR, textStatus){
				        	if (params.progress && params.stack[idFile]){
					        	file.retries++;
					        	
					        	var action = 'success';
					        	
					        	if (jsXHR.status != 200)
					        		if (file.retries == wFileLoader.retries)
						        		action = 'error';
						        	else
						        		action = 'wait';
					        	
					        	wFileLoaderListModify(id, idFile, action);
					        	params.progress = false;
				        	}
				        	
				        	setTimeout(function(){
				        		wFileLoaderProgress(id);
							}, 10);
				        }
					});
					
					
					break;
				}else if (!retries && !file.serverName && file.retries < wFileLoader.retries)
					search = true;
			}
			
			retries++;
		}
	
	if (!params.progress && buttons.length)
		buttonToggle(buttons, true);
}

$(window).load(function(){
	$('div.file-loader').on('click', '.button', function(event){
		event.preventDefault();
		
		if (!$(this).hasClass('disabled'))
			$(this).parent().children('input[type=file]').click();
	});
	
	$('div.file-loader').on('change', 'input[type=file]', function(){
		var obj = $(this).closest('div.file-loader'),
			id = obj.attr('id'),
			input = obj.children('input[type=hidden]'),
			params = id ? wFileLoader[id] : false;
		
		if (params){
			if (params.stack === undefined){
				params.stack = {};
				params.keys = {};
			}else if (!params.multiple && arrayLength(params.stack)){
				if (params.progress){
					$.ajax({
						type: 'POST',
						data: wFileLoaderParamProgress + '=' + params.progress + '&' + wFileLoaderParamCancel + '=1',
						cache: false,
						processData: false
					});
					
					params.progress = false;
				}
				
				input.val('').change();
				params.stack = {};
				params.keys = {};
				wFileLoaderListModify(id, false, 'clear');
			}
			
			$.each(this.files, function(key, info){
				var ext = info.name.match(/([^.]*)$/);
				
		        if (arrayLength(params.stack) < params.maxQu && info.size > 0 && info.size <= params.maxSize &&
		        	((info.type && params.mimeTypes.indexOf(info.type) !== -1) ||
		        	(!info.type && ext && params.types.indexOf(ext[0]) !== -1))){
			        	var key = info.name + info.size;
			        	
			        	if (!(key in params.keys)){
			        		var idFile;
			        		
			        		do{
			        			idFile = '_' + uniqid();
			        		}while (idFile in params.stack);
			        		
			        		params.keys[key] = 0;
			        		params.stack[idFile] = info;
			        		params.stack[idFile].key = key;
			        		params.stack[idFile].retries = 0;
			        		wFileLoaderListModify(id, idFile, 'add', info.name);
			        	}
			        }
		    });
			
			$(this).val('');
			
			if (arrayLength(params.stack) == params.maxQu){
				var button = obj.children('.button');
				
				if (button.length)
					buttonToggle(button, false);
			}
			
			if (arrayLength(params.stack) && !params.progress)
				wFileLoaderProgress(id);
		}
	});
	
	$('div.file-loader').on('click', 'a.fl-button-cancel', function(event){
		event.preventDefault();
		
		var tr = $(this).closest('tr'),
			obj = tr.closest('div.file-loader'),
			input = obj.children('input[type=hidden]'),
			button = obj.children('.button');
		
		if (obj.length && tr.length){
			var id = obj.attr('id'), 
				idFile = parseId(tr, wFileLoaderPrefixListItem);
			
			if (id && idFile){
				var params = wFileLoader[id];
				
				if (params && params.stack){
					var file = params.stack[idFile];
					
					if (file){
						if (params.progress == idFile){
							params.progress = false;
							
							$.ajax({
								type: 'POST',
								data: wFileLoaderParamProgress + '=' + idFile + '&' + wFileLoaderParamCancel + '=1',
								cache: false,
								processData: false
							});
						}
						
						if (file.serverName && input.length){
							var value = input.val();
							
							if (value){
								value = value.split(',');
								
								var key = value.indexOf(file.serverName);
								
								if (key !== -1){
									value.splice(key, 1);
									input.val(value.join(',')).change();
								}
							}
						}
						
						delete params.stack[idFile];
						delete params.keys[file.key];
						
						wFileLoaderListModify(id, idFile, 'cancel');
						
						if (arrayLength(params.stack) < params.maxQu && button.length)
							buttonToggle(button, true);
						
						setTimeout(function(){
			        		wFileLoaderProgress(id);
						}, 10);
					}
				}
			}
		}
	});
	
	$(document).on('click', 'label', function(event){
		var id = $(this).attr('for');
		
		if (id){
			var input = $('input[type=hidden]#' + id);
			
			if (input.length && input.closest('div.file-loader').length)
				input.parent().children('.button').click();
		}
	});
});
