<?php
Yii::app()->asset->add();

$params = array('maxSize'=>$max_size, 'maxQu'=>$max_qu, 'types'=>array_keys($mime_types),
	'mimeTypes'=>array_values($mime_types), 'multiple'=>$multiple);

echo Html::openTag('div', $html_options);
	
	echo Html::tag('a', array('class'=>'button download'), Yii::app()->label->get('button_download'));
	
	if ($this->model){
		echo Html::activeHiddenField($model, $name);
		$input_id = Html::activeId($model, $name);
	}else{
		echo Html::hiddenField($name);
		$input_id = Html::getIdByName($name);
	}
	
	echo Html::fileField('file-button', '', Html::forming(array('multiple'=>$multiple, 'accept'=>implode(', ', $mime_types))));
	
	echo Html::openTag('table', array('class'=>'fl-list'));
		echo Html::openTag('tbody');
			if ($value && ($files = Yii::app()->user->getState('files'))){
				$values = Strings::devisionWords($value);
				
				if (!$multiple)
					$values = array_slice($values, 0, 1);
				
				foreach ($values as $file_id){
					list($file_name, $file_size) = Arrays::pop($files, "$file_id.name, $file_id.size");
					
					if ($file_name){
						echo Html::openTag('tr', array('class'=>'fl-item-_'.$file_id.' complete'));
							echo Html::openTag('td', array('class'=>'fl-list-name'));
								echo Html::tag('div', array('style'=>'overflow: hidden'), $file_name);
							echo Html::closeTag('td');
							
							echo Html::openTag('td', array('class'=>'fl-list-progress'));
								echo Html::openTag('div', array('style'=>'overflow: hidden'));
									echo Html::openTag('div', array('class'=>'fl-progress-bar'));
										echo Html::tag('p', array('style'=>'width: 100%'));
										echo Html::tag('span', array(), '');
									echo Html::closeTag('div');
									echo Html::link('', '#', array('class'=>'fl-button-cancel'));
								echo Html::closeTag('div');
							echo Html::closeTag('td');
						echo Html::closeTag('tr');
						
						$params['keys'][$file_key = $file_name.$file_size] = 0;
						$params['stack']['_'.$file_id] = array('key'=>$file_key, 'name'=>$file_name,
							'serverName'=>$file_id, 'size'=>$file_size);
					}
				}
			}
			
		echo Html::closeTag('tbody');
	echo Html::closeTag('table');
	
echo Html::closeTag('div');

$this->registerScriptParams($params, array('retries'=>$retries, 'textError'=>Yii::app()->label->get('wfl_error')));
