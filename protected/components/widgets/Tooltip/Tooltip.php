<?php
/**
 * Виджет всплывающей подсказки
 * @author v.zubov
 */
class Tooltip extends Widget{
	
	/**
	 * HTML-опции объекта
	 */
	public $htmlOptions;
	
	/**
	 * Экземпляр модели данных. Если он задан и задан параметр name, то текст подсказки будет браться из
	 * компонента attributes
	 */
	public $model;
	
	/**
	 * Название атрибута модели. Используется совместно с параметром model
	 */
	public $name;
	
	/**
	 * Текст подсказки
	 */
	public $text;
	
	/**
	 * Запуск виджета
	 */
	public function run(){
		if ($this->model instanceof CModel && $this->name)
			if ($tooltip = $this->model->getAttributeTooltipText($this->name))
				$this->text = $tooltip;
		
		if ($this->text)
			$this->render('tooltip', array(
				'htmlOptions'=>$this->preparationHtmlOptions(),
				'text'=>$this->text
			));
	}
	
}
