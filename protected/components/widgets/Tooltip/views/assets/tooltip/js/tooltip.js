function wTooltipPopupEnter(obj, target){
	if (!obj.hasClass('active')){
		if (!target || target.hasClass('tt-button')){
			var button = obj.children('div.tt-button'),
				text = obj.children('div.tt-text'),
				buttonLeft = button.offset().left - 20,
				textLeft = parseInt(text.css('left'));
			
			if (-buttonLeft > textLeft){
				var add = -buttonLeft - textLeft,
					triangleExt = text.children('div.tt-triangle-external'),
					triangleInt = text.children('div.tt-triangle-internal');
				
				text.css('left', (textLeft + add) + 'px');
				triangleExt.css('left', parseInt(triangleExt.css('left')) - add);
				triangleInt.css('left', parseInt(triangleInt.css('left')) - add);
			}
			
			text.fadeIn('fast');
			obj.addClass('active');
		}
	}else if (target && (target.hasClass('tt-button') || target.hasClass('tt-close')))
		wTooltipPopupDrop(obj);
}

function wTooltipPopupDrop(obj){
	obj.removeClass('active').children('div.tt-text').fadeOut('fast');
}

$(window).load(function(){
	popupAdd('tooltip', wTooltipPopupEnter, wTooltipPopupDrop);
});
