<?php
Yii::app()->asset->add();

echo Html::openTag('div', $htmlOptions);
	
	echo Html::tag('div', array('class'=>'tt-button'), '');
	
	echo Html::openTag('div', array('class'=>'tt-text shadow'));
		echo $text;
		echo Html::tag('div', array('class'=>'tt-close'), '');
		echo Html::tag('div', array('class'=>'tt-triangle-external'), '');
		echo Html::tag('div', array('class'=>'tt-triangle-internal'), '');
	echo Html::closeTag('div');
	
echo Html::closeTag('div');
