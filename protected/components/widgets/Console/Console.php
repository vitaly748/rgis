<?php
/**
 * Виджет консоли приложения
 * @author v.zubov
 */
class Console extends Widget{
	
	/**
	 * Запуск виджета
	 */
	public function run(){
		if ($text = Yii::app()->controller->console)
			$this->render('console', array(
				'text'=>$text
			));
	}
	
}
