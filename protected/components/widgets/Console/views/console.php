<?php
Yii::app()->asset->add();

echo Html::openTag('div', array('class'=>'console'));
	
	echo Html::openTag('div', array('class'=>'console-text'));
		echo $text;
	echo Html::closeTag('div');
	
	echo Html::tag('div', array('class'=>'console-close'), '');
echo Html::closeTag('div');
