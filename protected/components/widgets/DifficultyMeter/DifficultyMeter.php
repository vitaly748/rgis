<?php
/**
 * Виджет "Измеритель сложности значения"
 * @author v.zubov
 */
class DifficultyMeter extends Widget{
	
	/**
	 * @var array Список возможных заголовков
	 */
	const TITLES = array('Пустой', 'Простой', 'Слабый', 'Средний', 'Хороший', 'Надёжный', 'Сложный');
	
	/**
	 * @var array HTML-опции объекта
	 */
	public $htmlOptions;
	
	/**
	 * @var CModel Экземпляр модели данных
	 */
	public $model;
	
	/**
	 * @var string Название элемента
	 */
	public $name;
	
	/**
	 * @var integer Ширина виджета. По умолчанию равно 80
	 */
	public $width = 80;
	
	/**
	 * @var integer Количество символов в строке, дающее дополнительное очко к сложности. По умолчанию равно 7
	 */
	public $lengthUp = 7;
	
	/**
	 * Запуск виджета
	 */
	public function run(){
		if ($this->model && $this->name)
			$this->render('difficulty_meter', array(
				'html_options'=>$this->preparationHtmlOptions(),
				'input_id'=>Html::activeId($this->model, $this->name),
				'width'=>$this->width,
				'length_up'=>$this->lengthUp,
				'titles'=>self::TITLES
			));
	}
	
}
