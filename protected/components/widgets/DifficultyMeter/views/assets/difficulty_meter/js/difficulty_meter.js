wDifficultyMeter = {};

function wDifficultyMeterRepaint(id){
	var obj = $('div.difficulty-meter#' + id);
	
	if (id in wDifficultyMeter && obj.length){
		var params = wDifficultyMeter[id],
			lengthUp = params['length_up'],
			titles = params['titles'],
			inputId = params['input_id'],
			input = $('input#' + inputId),
			title = obj.children('p.dm-title'),
			meter = obj.find('div.dm-meter'),
			text = titles[0],
			width = 0;
		
		if (input.length && input.val() && inputId in inputPattern &&
			'groups_significant_find' in inputPattern[inputId] && inputPattern[inputId].groups_significant_find){
				var value = input.val(),
					testQu = inputPattern[inputId].groups_significant_qu + Boolean(lengthUp),
					testCheck = inputPattern[inputId].groups_significant_find + (lengthUp ? value.length >= lengthUp : 0);
				
				width = testCheck * 100 / testQu;
				text = titles[Math.round((titles.length - 1) * width / 100)];
			}
		
		title.html(text);
		meter.css('width', width + '%');
	}
}

$(window).load(function(){
	if (typeof inputPattern != 'undefined')
		$('div.difficulty-meter').each(function(){
			var id = $(this).attr('id');
			
			if (id in wDifficultyMeter){
				var params = wDifficultyMeter[id],
					inputId = params['input_id'],
					input = $('input#' + inputId);
				
				if (input.length && inputId in inputPattern && 'groups' in inputPattern[inputId] &&
					'groups_min' in inputPattern[inputId]){
						wDifficultyMeterRepaint(id);
						
						input.on('change', function(){
							setTimeout(function(){
								wDifficultyMeterRepaint(id);
							}, 100);
						});
					}
			}
		});
});
