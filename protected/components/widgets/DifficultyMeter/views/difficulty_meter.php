<?php
Yii::app()->asset->add();

if ($width)
	$html_options['style'] = 'width: '.$width.'px';

echo Html::openTag('div', $html_options);
	
	echo Html::tag('p', array('class'=>'dm-title'), reset($titles));
	echo Html::tag('div', array('class'=>'dm-bk round6'), Html::tag('div', array('class'=>'dm-meter round6'), ''));
	
echo Html::closeTag('div');

$this->registerScriptParams(array(
	'input_id'=>$input_id,
	'length_up'=>$length_up,
	'titles'=>$titles
));
