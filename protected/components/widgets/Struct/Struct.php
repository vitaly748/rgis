<?php
/**
 * Виджет "Структура данных"
 * @author v.zubov
 */
class Struct extends Widget{
	
	/**
	 * @var array HTML-опции объекта
	 */
	public $htmlOptions;
	
	/**
	 * @var CModel Экземпляр модели данных
	 */
	public $model;
	
	/**
	 * @var string Название элемента
	 */
	public $name;
	
	/**
	 * @var string Значение элемента
	 */
	public $value;
	
	/**
	 * Запуск виджета
	 */
	public function run(){
		if (!$this->value && $this->model && $this->name)
			$this->value = $this->model->{$this->name};
		
		if ($this->value && is_array($this->value))
			$this->render('struct', array(
				'html_options'=>$this->preparationHtmlOptions(),
				'value'=>$this->value
			));
	}
	
}
