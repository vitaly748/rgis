<?php
echo Html::openTag('table', array('class'=>'struct-level'));
	echo Html::openTag('tbody');
		
		foreach ($value as $key=>$item){
			echo Html::openTag('tr');
				echo Html::openTag('td', array('class'=>'sl-name'));
					echo $key;
				echo Html::closeTag('td');
				echo Html::openTag('td', array('class'=>'sl-value'));
					if (!is_array($item))
						echo $item;
					else
						$this->render('struct_level', array('value'=>$item));
				echo Html::closeTag('td');
			echo Html::closeTag('tr');
		}
		
	echo Html::closeTag('tbody');
echo Html::closeTag('table');
