<?php
Yii::app()->asset->add();

ob_start();
	foreach ($flashes as $type=>$params)
		foreach ($params as $params_message){
			$html_options_message = array('class'=>'flash '.$type);
			
			if ($params_message['html_options'])
				$html_options_message =
					Arrays::mergeParams(array('sources'=>array($html_options_message, $params_message['html_options'])));
			
			echo Html::openTag('div', $html_options_message);
				echo Html::tag('div', array('class'=>'flash-text'), $params_message['text']);
				
				if ($params_message['closing'])
					echo Html::tag('div', array('class'=>'flash-close'), '');
			echo Html::closeTag('div');
		}
$html = ob_get_clean();

if ($html)
	echo Html::tag('div', $html_options, $html);

if ($note)
	echo Html::tag('div', array('class'=>'note'), Html::tag('div', array(), $note));
