function wFlashesSet(type, text){
	var flashes = $('div.flashes'),
		find = false;
	
	if (!flashes.length)
		return;
	
	flashes.children('div.flash.' + type).each(function(){
		if (!$(this).hasClass('dsp-none') && $(this).children('div.flash-text').html() == text)
			find = true;
	});
	
	if (find)
		return;
	
	var flash = flashes.children('div.flash.dsp-none:first');
	
	if (!flash.length)
		flash = flashes.children('div.flash:first-child').clone(true).appendTo(flashes);
	
	if (flash.length){
		flash.toggleClass('error', type == 'error').toggleClass('ok', type == 'ok');
		flash.removeClass('dsp-none').children('div.flash-text').html(text);
	}
}

function wFlashesToggle(type, show){
	var flash = $('div.flash.' + type);
	
	if (flash.length){
		flash.toggleClass('dsp-none', !show);
	}
}

$(window).load(function(){
	$('div.flash div.flash-close').on('click', function(){
		$(this).parent().addClass('dsp-none');
	});
});
