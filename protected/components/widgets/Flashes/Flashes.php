<?php
/**
 * Виджет отображения быстрых сообщений
 * @author v.zubov
 */
class Flashes extends Widget{
	
	/**
	 * HTML-опции объекта
	 */
	public $htmlOptions;
	
	/**
	 * Ассоциативный массив сообщений в формате
	 * 	array(
	 * 		{Символьный идентификатор типа сообщения (ok или error)}=>{Текст сообщения},// Упрощённая форма записи
	 * 		{Символьный идентификатор типа сообщения}=>array(// Расширенная форма записи
	 * 			'text'=>'Текст',// Текст сообщения. Обязательный параметр
	 * 			// Признак наличия закрывающей кнопки. Необязательный параметр. По умолчанию равно значению параметра "closing" класса
	 * 			'closing'=>true,
	 * 			// Массив дополнительных HTML-опций флэш-сообщения. Необязательный параметр. По умолчанию равно array()
	 * 			'html_options'=>array(),
	 * 			// Массив HTML-опций основного контейнера виджета. Необязательный параметр. По умолчанию равно false
	 * 			'html_options_parent'=>array(),
	 * 			// Признак необходимости отображения сообщения отдельно от контента. Необязательный параметр. По умолчанию равно false
	 * 			'only_flash'=>true
	 * 		),
	 * 		{Символьный идентификатор типа сообщения}=>array(// Расширенная форма записи c указанием нескольких сообщений одного типа
	 * 			array(
	 * 				'text'=>'Текст',
	 * 				'closing'=>true,
	 * 				'html_options'=>array(),
	 * 			),
	 * 			...
	 * 		),
	 * 		...
	 * 	)
	 */
	public $flashes;
	
	/**
	 * Признак наличия закрывающих кнопок по умолчанию
	 */
	public $closing = true;
	
	
	/**
	 * Запуск виджета
	 */
	public function run(){
		if (!$this->flashes)
			$this->flashes = Yii::app()->user->flashes;
		
		if (!$this->flashes)
			return false;
		
		$flashes = array();
		$note = '';
		
		foreach ($this->flashes as $type=>$params)
			if (is_string($type)){
				if (is_string($params))
					$params = array(array('text'=>$params));
				elseif (is_array($params)){
					if (is_string(reset(array_keys($params))))
						$params = array($params);
					else{
						$params_new = array();
						
						foreach ($params as $params_message)
							if (is_string($params_message))
								$params_new[] = array('text'=>$params_message);
							elseif (is_array($params_message))
								$params_new[] = $params_message;
						
						$params = $params_new;
					}
				}else
					continue;
				
				foreach ($params as $params_message)
					if ($text = Arrays::pop($params_message, 'text'))
						if (!empty($params_message['only_flash'])){
							if (!$note)
								$note = $text;
							else
								$note .= Html::tag('br').Html::tag('br').Html::tag('br').$text;
						}else{
							$flashes[$type][] = array(
								'text'=>$text,
								'closing'=>isset($params_message['closing']) ? $params_message['closing'] : $this->closing,
								'html_options'=>(array)Arrays::pop($params_message, 'html_options')
							);
							
							if (!empty($params_message['html_options_parent']))
								$this->htmlOptions = $params_message['html_options_parent'];
						}
			}
		
		if (!$flashes && !$note)
			return false;
		
		if ($this->flashes)
			$this->render('flashes', array(
				'html_options'=>$this->preparationHtmlOptions(),
				'flashes'=>$flashes,
				'note'=>$note
			));
	}
	
}
