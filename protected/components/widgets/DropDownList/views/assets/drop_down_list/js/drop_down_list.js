wDropDownList = {};
wddlClassLocked = 'ddl-locked';
wddlClassHeader = 'ddl-header';
wddlClassTitle = 'ddl-title';
wddlClassCurtain = 'ddl-curtain';
wddlClassIcon = 'ddl-icon';
wddlClassList = 'ddl-list';
wddlClassItem = 'ddl-item';
wddlClassItemLocked = 'ddl-item-locked';
wddlClassItemHidden = 'ddl-item-hidden';
wddlClassItemRadio = 'ddl-item-radio';
wddlClassNotFound = 'ddl-not-found';
wddlClassEmpty = 'ddl-empty';
wddlClassInputEmpty = 'ddl-input-empty';
wddlClassBorder = 'ddl-border';
wddlClassAll = 'ddl-all';
wddlClassInputAll = 'ddl-input-all';
wddlClassInput = 'ddl-input';
wddlClassInputHidden = 'ddl-input-hidden';

function wDropDownListPopupEnter(obj, target){
	var id = obj.attr('id'),
		active = obj.hasClass('active'),
		readonly = obj.hasClass('readonly');
	
	if (!obj.hasClass('disabled') && !obj.hasClass(wddlClassLocked) &&
		((!target && !active) || (target && target.closest('.' + wddlClassHeader).length &&
		(target.hasClass(wddlClassIcon) || !(!readonly && active))))){
			var toggle = readonly && (!target || !target.hasClass(wddlClassIcon)) && typeof wDropDownList[id] != 'undefined';
			
			if (!active){
				obj.addClass('active').children('ul.' + wddlClassList).fadeIn('fast');
				obj.find('div.' + wddlClassCurtain).addClass('dsp-none');
				wDropDownListFocus(obj);
				
				if (toggle){
					if (wDropDownList[id].timerToggle)
						clearTimeout(wDropDownList[id].timerToggle);
					
					wDropDownList[id].timerToggle = setTimeout(function(){
						wDropDownList[id].timerToggle = false;
					}, 500);
				}
			}else if (obj.hasClass('ddl-filter')){
				wDropDownListTitleRepaint(obj);
				obj.find('li.' + wddlClassNotFound).addClass('dsp-none');
				obj.find('li').not('.' + wddlClassNotFound).removeClass('dsp-none');
				obj.removeClass('ddl-filter');
			}else if (!toggle || !wDropDownList[id].timerToggle)
					wDropDownListPopupDrop(obj);
		}
}

function wDropDownListPopupDrop(obj, skipDropChecked){
	if (obj.hasClass('active')){
		var ul = obj.children('ul'),
			li = ul.children('li');
		
		obj.removeClass('active');
		
		ul.fadeOut('fast', function(){
			if (obj.hasClass('ddl-filter')){
				li.filter('.' + wddlClassNotFound).addClass('dsp-none');
				li.not('.' + wddlClassNotFound).removeClass('dsp-none');
				obj.removeClass('ddl-filter');
			}
		});
		
		li.filter('.hover').removeClass('hover');
		
		if (!obj.hasClass('multiple') && !skipDropChecked){
			var inputChecked = li.children('input:checked'),
				inputActive = li.filter('.active').not('.' + wddlClassEmpty).children('input[type="radio"]');
			
			if (inputActive){
				inputChecked = inputChecked.not('#' + inputActive.attr('id'));
				
				if (!inputActive.attr('checked'))
					inputActive.attr('checked', true);
			}
			
			inputChecked.attr('checked', false);
		}
		
		wDropDownListTitleRepaint(obj);
		obj.find('.' + wddlClassTitle).blur();
		wDropDownListCheckError(obj);
	}
}

function wDropDownListLabelAssistant(obj){
	return obj.children('div.' + wddlClassHeader);
}

function wDropDownListCheckError(obj){
	if (obj.hasClass('required')){
		var em = obj.parent().children('div.errorMessage');
		
		if (em.length){
			var error = !obj.find('ul.' + wddlClassList + ' input:checked').length,
				id = obj.attr('id'),
				msg = typeof wDropDownList[id] != 'undefined' ? wDropDownList[id].textRequired : false;
			
			if (msg)
				em.html(msg);
				
			em.toggleClass('dsp-none', !error);
			obj.toggleClass('error', error);
		}
	}
};

function wDropDownListFocus(obj){
	var inputFocus;
	
	if (!obj.hasClass('readonly'))
		inputFocus = obj.find('input.' + wddlClassTitle);
	else{
		inputFocus = obj.find('input.' + wddlClassInputEmpty + ', input.' + wddlClassInputAll + ', input.' +
			wddlClassInput).filter(':enabled');
		
		if (inputFocus.length && !obj.hasClass('multiple')){
			var inputRadio = inputFocus.filter(':checked');
			
			if (inputRadio.length)
				inputFocus = inputRadio;
		}
	}
	
	if (inputFocus.length)
		inputFocus[0].focus();
}

function wDropDownListLiActivate(li){
	if (!li.hasClass('active')){
		var ul = li.parent(),
			obj = ul.parent(),
			input = li.children('input[type=radio]');
		
		if (!obj.hasClass('disabled') && !obj.hasClass(wddlClassLocked) && !obj.hasClass('multiple') &&
			input.length && !input.attr('disabled')){
				ul.children('li.active').removeClass('active');
				li.addClass('active');
				wDropDownListPopupDrop(obj, true);
			}
	}
}

function wDropDownListTitleRepaint(obj){
	var id = obj.attr('id'),
		title = obj.find('input.' + wddlClassTitle), 
		curtain = obj.find('div.' + wddlClassCurtain), 
		li = obj.find('li'),
		widthMaxTitle = cssPxToInt(title.css('max-width')),
		widthMax = title.parent().width(),
		multiple = obj.hasClass('multiple'),
		params = typeof wDropDownList[id] != 'undefined' ? wDropDownList[id] : false,
		items = li.find('input.' + wddlClassInput),
		itemDisabled = li.filter('.' + wddlClassItemHidden).length,
		itemsText = [],
		titleText,
		reductionType = params ? params.reductionType : false,
		reductionCutsLenMin = params ? params.reductionCutsLenMin : false,
		textAll = params ? params.textAll : false,
		textEmpty = params ? params.textEmpty : false,
		textNotFound = params ? params.textNotFound : false,
		reductionLevel = 1,
		reductionCutsLen = 0,
		lacksLen = width = false;
	
	items.filter(':checked').each(function(){
		var itemText = $(this).parent().children('label').html(),
			itemLen = itemText.length;
		
		itemsText.push(itemText);
		
		if (itemLen > reductionCutsLen)
			reductionCutsLen = itemLen;
	});
	
	if (!itemsText.length){
		if (textEmpty && !itemDisabled)
			itemsText = [textEmpty];
	}else if (multiple && itemsText.length === items.length && textAll)
		itemsText = [textAll];
	
	var reductionEnabled = multiple && reductionType && widthMax && itemsText.length;
	
	do{
		if (reductionLevel < 3){
			titleText = itemsText.length ? itemsText.join(reductionLevel == 1 ? ', ' : ',') : '';
			reductionLevel++;
		}else if (reductionType == 1){
			titleText = '';
			reductionCutsLen--;
			
			itemsText.forEach(function(itemText){
				if (titleText)
					titleText += ',';
				
				titleText += itemText.substr(0, reductionCutsLen);
			});
		}
		
		width = textWidth(titleText);
		
		if (widthMax){
			lacksLen = width > widthMax;
			lacksLenTitle = width > widthMaxTitle;
		}
	}while (reductionEnabled && lacksLen && (reductionType == 1 && reductionCutsLen > reductionCutsLenMin));
	
	if (width)
		title.css('width', width);
	
	if (!obj.hasClass('active'))
		curtain.toggleClass('dsp-none', !lacksLenTitle);
	
	if (params)
		wDropDownList[id]['lacksLen'] = lacksLenTitle;
	
	if (typeof inputEditableCheck !== 'undefined')
		inputEditableCheck();
	
	title.val(titleText).change();
}

$(window).load(function(){
	popupAdd('drop-down-list', wDropDownListPopupEnter, wDropDownListPopupDrop);
	
	$(document).on('change', 'div.drop-down-list', function(event, hype){
		if (hype == 2)
			return;
		
		var target = $(event.target);
		
		if (target.is('input')){
			var obj = target.closest('.drop-down-list');
			
			if (obj.hasClass('disabled') || obj.hasClass(wddlClassLocked) || (!obj.hasClass('active') &&
				!target.hasClass(wddlClassInputEmpty)))
					return;
			
			if (target.hasClass(wddlClassTitle)){
				if (obj.hasClass('readonly') || target.attr('id') !== inputEditable)
					return;
				
				var li = obj.find('li'),
					values = arrayClean(target.val().split(/[\s,]+/));
				
				obj.addClass('ddl-filter');
				li.addClass('dsp-none');
				
				if (values.length){
					var regexp = new RegExp('(' + values.join('|') + ')', 'i'),
						match = false;
					
					li.filter('.' + wddlClassItem).not('.' + wddlClassNotFound + ', .' + wddlClassEmpty +
						', .' + wddlClassAll).each(function(){
							if (regexp.test($(this).children('label').html())){
								$(this).removeClass('dsp-none');
								match = true;
							}
						});
					
					if (!match)
						li.filter('.' + wddlClassNotFound).removeClass('dsp-none');
				}else
					li.not('.' + wddlClassNotFound).removeClass('dsp-none');
				
				wDropDownListCheckError(obj);
			}else if (!target.hasClass(wddlClassInputHidden)){
				var li = target.parent(),
					ul = li.parent();
				
				if (obj.hasClass('multiple')){
					var all = ul.find('li.' + wddlClassAll + ' input');
					
					if (li.hasClass(wddlClassItemRadio)){
						var input = li.children('input.' + wddlClassInput)
						
						if (!input.attr('disabled'))
							ul.find('li.' + wddlClassItemRadio + ' input.' + wddlClassInput).each(function(){
								if ($(this)[0] != input[0])
									$(this).attr('checked', false).trigger('change', [2]);
							});
					}
					
					if (all.length)
						if (li.hasClass(wddlClassAll)){
							var checked = !!target.attr('checked'),
								skipRadio = ul.find('li.' + wddlClassItemRadio + ' input.' + wddlClassInput + ':checked').length;
							
							ul.children('li.' + wddlClassItem).not('.' + wddlClassAll + ', .' + wddlClassItemLocked).
								children('input.' + wddlClassInput).each(function(){
									var radio = $(this).parent().hasClass(wddlClassItemRadio);
									
									if (!radio || !checked || !skipRadio){
										$(this).attr('checked', checked).trigger('change', [2]);
										
										if (radio)
											skipRadio = true;
									}
								});
						}else{
							var items = ul.children('li.' + wddlClassItem).not('.' + wddlClassAll).
									children('input.' + wddlClassInput),
								checked = items.length == items.filter(':checked').length;
							
							if (!!all.attr('checked') !== checked)
								all.attr('checked', checked).trigger('change', [2]);
						}
					
					wDropDownListTitleRepaint(obj);
				}else if (target.hasClass(wddlClassInputEmpty))
					ul.find('input:checked').attr('checked', false);
			}
		}else if (target.hasClass('drop-down-list')){
			var title = target.find('.' + wddlClassTitle);
			
			if (target.hasClass('disabled'))
				target.find('input').each(function(){
					$(this).attr('disabled', true);
					inputCheckLabel($(this));
				});
			else{
				title.attr('disabled', target.hasClass(wddlClassLocked));
				
				target.find('li').each(function(){
					var disabled = $(this).hasClass(wddlClassItemLocked);
					
					$(this).children('input:not(.' + wddlClassInputHidden + ')').each(function(){
						$(this).attr('disabled', disabled);
						inputCheckLabel($(this));
					});
					
					$(this).children('input.' + wddlClassInputHidden).attr('disabled', false);
				});
			}
		}
	});
	
	$(document).on('click', 'div.drop-down-list li.' + wddlClassItem + ':not(.' + wddlClassNotFound + ')', function(event){
		var target = $(event.target);
		
		if (!target.is('input') && !target.is('span')){
			var li = target.closest('li'),
				input = li.children('input[type=checkbox], input[type=radio]'),
				checked = input.attr('checked');
			
			if (input.length && !input.attr('disabled')){
				if (li.closest('.drop-down-list').hasClass('multiple') || !checked)
					input.attr('checked', !checked).focus().change();
				
				wDropDownListLiActivate(li);
			}
		}
	});
	
	$(document).on('hover', 'div.drop-down-list li', function(event){
		var span = $(this).children('span');
		
		if (span.length){
			var input = inputChRSpan(span);
			
			if (input && !input.attr('disabled')){
				var ul = $(this).parent(),
					obj = ul.parent(),
					enter = event.type == 'mouseenter';
				
				if (obj.hasClass('multiple'))
					span.toggleClass('hover', enter);
				
				if (enter && ul.find('input:focus').length)
					input.focus();
			}
		}
	});
	
	$(document).on('focusin', 'div.drop-down-list', function(event){
		var obj = $(this),
			target = $(event.target);
		
		if (target.is('input')){
			var title = target.hasClass(wddlClassTitle);
			
			if (!obj.hasClass('active')){
				if (title)
					wDropDownListPopupEnter($(this));
			}else if (!title){
				obj.find('li').removeClass('hover');
				target.parent().filter('li').addClass('hover');
			}
			
			if (title && !obj.hasClass('readonly'))
				target.select();
		}
	});
	
	$(document).on('keydown', 'div.drop-down-list li', function(event){
		if (event.which == 32)
			wDropDownListLiActivate($(this));
	});
	
	$(document).on('click', 'label', function(event){
		if ($(this).closest('div.drop-down-list').length){
			$(this).parent().click();
			event.preventDefault();
			event.stopPropagation();
		}
		
		var id = $(this).attr('for');
		
		if (id){
			var obj = $('div.drop-down-list#' + id);
			
			if (obj.length){
				wDropDownListPopupEnter(obj);
				event.preventDefault();
				event.stopPropagation();
			}
		}
	});
	
	$('div.drop-down-list').each(function(){
		var obj = $(this),
			id = obj.attr('id'),
			title = obj.find('input.' + wddlClassTitle),
			icon = obj.find('div.' + wddlClassIcon),
			em = obj.parent().children('div.errorMessage'),
			widthMinObj = cssPxToInt(obj.css('min-width')),
			widthMaxObj = cssPxToInt(obj.css('max-width')),
			ul = obj.children('ul'),
			multiple = obj.hasClass('multiple'),
			params = typeof wDropDownList[id] != 'undefined' ? wDropDownList[id] : false,
			widthInitial = params ? params.widthInitial : false,
			textAll = params ? params.textAll : false,
			textEmpty = params ? params.textEmpty : false,
			widthMinTitle = widthMaxTitle = widthMaxLabel = widthAverage = widthMultipleDef = 0;
		
		preparationRender(obj, function(){
			ul.addClass('dsp-b');
			
			var widthDiff = cssPxToInt(title.css('padding-left')) + icon.width() + 2,
				items = ul.children('li.' + wddlClassItem).not('.' + wddlClassNotFound),
				itemsText = [];
			
			if (widthMinObj)
				widthMinTitle = Math.max(widthMinObj - widthDiff, 0);
			
			if (widthMaxObj)
				widthMaxTitle = Math.max(widthMaxObj - widthDiff, 0);
			
			items.each(function(){
				var label = $(this).children('label'),
					text = label.length ? label.html() : $(this).html();
				
				if (text)
					itemsText.push(text);
			});
			
			itemsText.forEach(function(itemText){
				var width = textWidth(itemText);
				
				widthAverage += width;
				
				if (width > widthMaxLabel)
					widthMaxLabel = width;
			});
			
			if (widthMaxLabel)
				widthMaxLabel += 2;
			
			if (multiple && items.length)
				widthAverage /= items.length;
			
			ul.removeClass('dsp-b');
			items.removeClass('nowrap');
		});
		
		if (multiple && widthAverage)
			widthMultipleDef = Math.min(Math.round(2 * widthAverage), widthMaxTitle ? widthMaxTitle : 250);
		
		if (widthInitial)
			widthMinTitle = widthMaxTitle = widthMultipleDef ? widthMultipleDef : widthMaxLabel;
		else if (widthMultipleDef && !widthMinTitle)
			widthMinTitle = widthMultipleDef;
		
		if (widthMinTitle)
			title.css('min-width', widthMinTitle);
		
		if (widthMaxTitle)
			title.css('max-width', widthMaxTitle);
		
		title.removeClass('wauto').width(widthMaxLabel);
		wDropDownListTitleRepaint(obj);
		
		if (em.length && !em.hasClass('dsp-none'))
			obj.addClass('error');
	});
});
