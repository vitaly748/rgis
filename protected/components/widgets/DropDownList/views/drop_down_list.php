<?php
Yii::app()->asset->add();

$id = $html_options['id'];
$params_items = array('type'=>'checkbox', 'model'=>$model, 'name'=>$name);

if (!$multiple){
	$title = !$value ? $text_empty : (isset($values_visible[$value]) ? $values_visible[$value] : '');
	$params_items = array_merge($params_items, array('type'=>'radio', 'htmlOptions'=>array('class'=>'dsp-none')));
}else{
	$title = !$value ? $text_empty : (count($value) == count($values_visible) ? $text_all :
		implode(', ', Arrays::filterKey($values_visible, $value)));
	$params_items['name'] .= '[]';
	$params_items['htmlOptions'] = array('class'=>Html::CLASS_INPUT_LABEL_HOVER_DISABLED);
}

$html_options = Html::mergeClasses($html_options, array('multiple'=>$multiple,
	'required'=>$required, 'disabled'=>$disabled || $locked, DropDownList::CLASS_LOCKED=>$locked, 'readonly'=>$readonly));

if ($width_min)
	$style[] = "min-width: {$width_min}px";

if ($width_max)
	$style[] = "max-width: {$width_max}px";

if (isset($style))
	$html_options['style'] = implode('; ', $style);

echo Html::openTag('div', $html_options);
	
	echo Html::openTag('div', array('class'=>DropDownList::CLASS_HEADER));
		
		echo Html::element(array('type'=>'text', 'name'=>DropDownList::CLASS_TITLE.'-'.$id, 'value'=>$title,
			Html::ELEMENT_PARAMS_IPPM=>$model, Html::ELEMENT_PARAMS_IPPN=>$name, Html::ELEMENT_PARAMS_IPPA=>', ',
			'htmlOptions'=>Html::forming(array(DropDownList::CLASS_TITLE, 'wauto', 'readonly'=>$readonly,
			'disabled'=>$disabled || $locked, 'autocomplete'=>'off'))));
		
		if ($multiple)
			echo Html::tag('div', array('class'=>DropDownList::CLASS_CURTAIN), '');
		
		echo Html::tag('div', array('class'=>DropDownList::CLASS_ICON), '');
		
	echo Html::closeTag('div');
	
	echo Html::openTag('ul', array('class'=>DropDownList::CLASS_LIST.' shadow'));
		
		if (!$readonly)
			echo Html::tag('li', array('class'=>DropDownList::CLASS_ITEM.' '.DropDownList::CLASS_NOT_FOUND.' dsp-none'),
				$text_not_found);
		
		if ($item_empty_enabled)
			echo Html::tag('li', Html::forming(array(DropDownList::CLASS_ITEM, DropDownList::CLASS_EMPTY,
				'active'=>$title === $text_empty)),
				Html::element(Arrays::mergeParams(array('sources'=>array($params_items, array(
					'htmlOptions'=>Html::forming(array(DropDownList::CLASS_INPUT_EMPTY, 'value'=>DropDownList::CLASS_INPUT_EMPTY,
						'id'=>DropDownList::CLASS_INPUT_EMPTY.'-'.$id, 'disabled'=>$disabled)),
					'label'=>$text_empty
				)))))).Html::tag('li', array('class'=>DropDownList::CLASS_BORDER), '');
		
		if ($item_all_enabled)
			echo Html::tag('li', array('class'=>DropDownList::CLASS_ITEM.' '.DropDownList::CLASS_ALL), Html::element(array(
				'type'=>'checkbox',
				'name'=>DropDownList::CLASS_INPUT_ALL.'-'.$id,
				'htmlOptions'=>Html::forming(array('id'=>DropDownList::CLASS_INPUT_ALL.'-'.$id, DropDownList::CLASS_INPUT_ALL,
					'checked'=>$title === $text_all, 'disabled'=>$disabled)),
				'label'=>$text_all
			))).Html::tag('li', array('class'=>DropDownList::CLASS_BORDER), '');
		
		foreach ($data as $item)
			if (!$item)
				echo Html::tag('li', array('class'=>DropDownList::CLASS_BORDER), '');
			else{
				echo Html::openTag('li', Html::forming(array(
					DropDownList::CLASS_ITEM,
					'active'=>!$multiple && $item['code'] == $value,
					DropDownList::CLASS_ITEM_LOCKED=>$item_locked = $item['access'] != 'update',
					DropDownList::CLASS_ITEM_HIDDEN=>$item_hidden = !$item['access'],
					DropDownList::CLASS_ITEM_RADIO=>$item['radio'],
					'nowrap'
				)));
					
					$checked = $value_real && in_array($item['code'], (array)$value_real);
					
					if (!$item_hidden){
						echo Html::element(Arrays::mergeParams(array('sources'=>array($params_items, array(
							'htmlOptions'=>Html::forming(array(DropDownList::CLASS_INPUT, 'value'=>$item['code'],
								'id'=>$id_item = DropDownList::CLASS_INPUT."-$id-{$item['code']}", 'disabled'=>$disabled || $item_locked,
								'checked'=>!$model && $checked)),
							'label'=>$item['description']
						)))));
						
						$id_items[$item['name']] = $id_item;
					}
					
					if ($item_locked && $checked)
						echo Html::element(array_merge($params_items, array('type'=>'hidden', 'htmlOptions'=>array(
							'class'=>DropDownList::CLASS_INPUT_HIDDEN, 'value'=>$item['code'],
							'id'=>DropDownList::CLASS_INPUT_HIDDEN."-$id-{$item['code']}"))));
					
				echo Html::closeTag('li');
			}
		
	echo Html::closeTag('ul');
	
echo Html::closeTag('div');

$this->registerScriptParams(array(
	'reductionType'=>$reduction_type,
	'reductionCutsLenMin'=>$reduction_cuts_len_min,
	'widthInitial'=>$width_initial,
	'textAll'=>$text_all,
	'textEmpty'=>$text_empty,
	'textNotFound'=>$text_not_found,
	'textRequired'=>$text_required,
	'idItems'=>$id_items
));
