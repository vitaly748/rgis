<?php
/**
 * Виджет "Выпадающий список". Может работать в режиме одиночного или множественного выбора
 * @author v.zubov
 */
class DropDownList extends Widget{
	
	/**
	 * @var string HTML-класс, задающий работу виджета в режиме блокировки
	 */
	const CLASS_LOCKED = 'ddl-locked';
	
	/**
	 * @var string HTML-класс блока заголовка
	 */
	const CLASS_HEADER = 'ddl-header';
	
	/**
	 * @var string HTML-класс поля заголовка
	 */
	const CLASS_TITLE = 'ddl-title';
	
	/**
	 * @var string HTML-класс шторки блока заголовка
	 */
	const CLASS_CURTAIN = 'ddl-curtain';
	
	/**
	 * @var string HTML-класс иконки блока заголовка
	 */
	const CLASS_ICON = 'ddl-icon';
	
	/**
	 * @var string HTML-класс блока списка элементов
	 */
	const CLASS_LIST = 'ddl-list';
	
	/**
	 * @var string HTML-класс элемента списка
	 */
	const CLASS_ITEM = 'ddl-item';
	
	/**
	 * @var string HTML-класс элемента списка, определяющий его как заблокированный
	 */
	const CLASS_ITEM_LOCKED = 'ddl-item-locked';
	
	/**
	 * @var string HTML-класс элемента списка, определяющий его как невидимый
	 */
	const CLASS_ITEM_HIDDEN = 'ddl-item-hidden';
	
	/**
	 * @var string HTML-класс элемента списка, определяющий его как работающий по принципу "radiobutton"
	 */
	const CLASS_ITEM_RADIO = 'ddl-item-radio';
	
	/**
	 * @var string HTML-класс элемента списка с тексом "Совпадений не найдено"
	 */
	const CLASS_NOT_FOUND = 'ddl-not-found';
	
	/**
	 * @var string HTML-класс элемента списка с тексом "Не выбрано"
	 */
	const CLASS_EMPTY = 'ddl-empty';
	
	/**
	 * @var string HTML-класс элемента типа "radiobutton" элемента списка с тексом "Не выбрано"
	 */
	const CLASS_INPUT_EMPTY = 'ddl-input-empty';
	
	/**
	 * @var string HTML-класс разделителя списка
	 */
	const CLASS_BORDER = 'ddl-border';
	
	/**
	 * @var string HTML-класс элемента списка с текстом "Все"
	 */
	const CLASS_ALL = 'ddl-all';
	
	/**
	 * @var string HTML-класс элемента типа "checkbox" элемента списка с текстом "Все"
	 */
	const CLASS_INPUT_ALL = 'ddl-input-all';
	
	/**
	 * @var string HTML-класс элемента типа "checkbox" или "radiobutton" элемента списка
	 */
	const CLASS_INPUT = 'ddl-input';
	
	/**
	 * @var string HTML-класс скрытого элемента элемента списка
	 */
	const CLASS_INPUT_HIDDEN = 'ddl-input-hidden';
	
	/**
	 * @var integer Код типа сокращения текста в заголовке виджета "Без сокоращения"
	 */
	const REDUCTION_TYPE_NONE = 0;
	
	/**
	 * @var integer Код типа сокращения текста в заголовке виджета "Урезание слов"
	 */
	const REDUCTION_TYPE_CUTS = 1;
	
	/**
	 * @var integer Минимальная длина слов при сокращении текста в заголовке виджета по типу "Урезание слов"
	 * по умолчанию
	 */
	const REDUCTION_CUTS_LEN_MIN_DEF = 3;
	
	/**
	 * @var array HTML-опции объекта
	 */
	public $htmlOptions;
	
	/**
	 * @var CModel Экземпляр модели данных
	 */
	public $model;
	
	/**
	 * @var string Название элемента
	 */
	public $name;
	
	/**
	 * @var mixed Значение активного пункта списка
	 */
	public $value;
	
	/**
	 * @var array Ассоциативный массив значений списка в формате
	 * 	array(
	 * 		{Значение пункта}=>{Описание пункта},
	 * 		array(// Расширенный формат
	 * 			'code'=>10,// Значение пункта. Необязательный параметр. По умолчанию равно ключу данного массива
	 * 			'name'=>'create,// Символьный идентификатор пункта. Необязательный параметр. По умолчанию равно значению пункта
	 * 			'description'=>'Создать',// Описание пункта. Необязательный параметр. По умолчанию равно ключу данного массива
	 * 			// Доступное действие ("view", "update" или false). Необязательный параметр. По умолчанию равно "update"
	 * 			'access'=>'view',
	 * 			// Признак работы по принципу "radiobutton". Необязательный параметр. По умолчанию равно false. Имеет смысл только
	 * 			// в случае работы виджета в режиме множественного выбора
	 * 			'radio'=>true
	 * 		),
	 * 		...
	 * 	)
	 * Для указания границы групп нужно указать "false" в качестве описания пункта
	 */
	public $data;
	
	/**
	 * @var boolean Признак работы в режиме множественного выбора
	 */
	public $multiple;
	
	/**
	 * @var integer Код типа сокращения текста в заголовке виджета. По умолчанию равно коду типа "Урезание слов"
	 */
	public $reductionType = self::REDUCTION_TYPE_CUTS;
	
	/**
	 * @var integer Минимальная длина слов при сокращении текста в заголовке виджета по типу "Урезание слов".
	 * По умолчанию равно self::REDUCTION_CUTS_LEN_MIN_DEF
	 */
	public $reductionCutsLenMin = self::REDUCTION_CUTS_LEN_MIN_DEF;
	
	/**
	 * @var integer Минимальная ширина виджета
	 */
	public $widthMin;
	
	/**
	 * @var integer Максимальная ширина виджета
	 */
	public $widthMax;
	
	/**
	 * @var boolean Признак использования инициализируемой ширины виджета
	 */
	public $widthInitial;
	
	/**
	 * @var boolean Признак запрета редактирования в строке заголовка
	 */
	public $readonly;
	
	/**
	 * @var boolean Признак обязательности заполнения
	 */
	public $required;
	
	/**
	 * @var boolean Признак недоступности виджета
	 */
	public $disabled;
	
	/**
	 * @var boolean Признак использования пункта "Выбрать всё". Так же этот пункт не будет отображаться в режиме
	 * одиночного выбора или если в списке значений всего один элемент
	 */
	public $itemAllEnabled;
	
	/**
	 * @var boolean Признак использования пункта "Не выбрано". Так же этот пункт не будет отображаться в режиме
	 * множественного выбора
	 */
	public $itemEmptyEnabled;
	
	/**
	 * @var string Текст, обозначающий выбор всех пунктов
	 */
	public $textAll;
	
	/**
	 * @var string Текст, обозначающий, что ни один пункт не выбран
	 */
	public $textEmpty;
	
	/**
	 * @var string Текст, обозначающий отсутствие совпадений при поиске
	 */
	public $textNotFound;
	
	/**
	 * @var boolean Признак применения виджета для атрибута модели
	 */
	protected $_fullnessModel;
	
	/**
	 * Запуск виджета
	 */
	public function run(){
		$model = $this->model;
		$name = $this->name;
		$access_attribute = $this->fullnessModel && isset($model->accessAttributes[$name]) ? $model->accessAttributes[$name] : null;
		
		if (!$this->data && $this->fullnessModel && isset($model->accessValues[$name]))
			$this->data = $model->accessValues[$name];
		
		if ($this->data && ($access_attribute === null || $access_attribute)){
			$value = $value_real = $this->fullnessModel ? $model->$name : $this->value;
			$data = $values = $values_disabled = $values_view = $values_update = $values_radio = $values_visible = array();
			$cur_border = false;
			
			foreach ($this->data as $key=>$params)
				if ($params === false){
					if ($data && $cur_border === false){
						$data[] = false;
						$cur_border = end(array_keys($data));
					}
				}else{
					$access = isset($params['access']) ? $params['access'] : 'update';
					$radio = isset($params['radio']) && $this->multiple ? (boolean)$params['radio'] : false;
					$code = isset($params['code']) ? $params['code'] : $key;
					$select = in_array($code, (array)$value);
					
					if ($access || $select){
						$data[] = array(
							'code'=>$code,
							'name'=>isset($params['name']) ? $params['name'] : $code,
							'description'=>$description = isset($params['description']) ?
								$params['description'] : (is_string($params) ? $params : $code),
							'access'=>$access,
							'radio'=>$radio
						);
						
						$cur_border = false;
						$values[$code] = $code;
						
						if (!$access)
							$values_disabled[] = $code;
						else{
							if ($access == 'view')
								$values_view[] = $code;
							else
								$values_update[$code] = $code;
							
							$values_visible[$code] = $description;
						}
						
						if ($radio)
							$values_radio[$code] = $code;
					}
				}
			
			if ($cur_border)
				unset($data[$cur_border]);
			
			if ($data){
				if ($this->multiple){
					$value = array_intersect((array)$value, $values);
					
					if ($value && $values_radio && ($values_radio_disabled =
						array_merge(array_intersect($values_radio, $values_view), array_intersect($values_radio, $values_disabled))) &&
						array_intersect($value, $values_radio_disabled))
							foreach ($data as $key=>$params)
								if ($params && $params['radio'] && $params['access'] == 'update'){
									$data[$key]['access'] = 'view';
									$data[$key]['radio'] = false;
									unset($values_update[$code = $params['code']], $values_radio[$code]);
									$values_view[] = $code;
								}
				}elseif (is_array($value))
					$value = ($intersect = array_intersect($value, $values_update)) ? reset($intersect) : null;
				elseif (!isset($values[$value]))
					$value = null;
				
				$required = $this->required === null && $this->fullnessModel ? $model->isAttributeRequired($name) : $this->required;
				$item_all_enabled = $this->itemAllEnabled && $this->multiple && count($values_update) > 1 &&
					(!$values_view || !array_diff($values_view, $value));
				$item_empty_enabled = $this->itemEmptyEnabled && !$this->multiple && !$required && $values_update &&
					($value === null || isset($values_update[$value]));
				$locked = !$values_update || (!$this->multiple && $value &&
					(!isset($values_update[$value]) || count($values_update) == 1));
				
				$this->render('drop_down_list', array(
					'html_options'=>$this->preparationHtmlOptions(),
					'model'=>$model,
					'name'=>$name,
					'value'=>$value,
					'value_real'=>$value_real,
					'data'=>$data,
					'values_visible'=>$values_visible,
					'multiple'=>$this->multiple,
					'reduction_type'=>$this->reductionType,
					'reduction_cuts_len_min'=>$this->reductionCutsLenMin,
					'width_min'=>$this->widthMin,
					'width_max'=>$this->widthMax,
					'width_initial'=>(boolean)$this->widthInitial,
					'readonly'=>$this->readonly,
					'required'=>$required,
					'disabled'=>$this->disabled,
					'locked'=>$locked,
					'item_all_enabled'=>$item_all_enabled,
					'item_empty_enabled'=>$item_empty_enabled,
					'text_all'=>$this->textAll ? $this->textAll : Yii::app()->label->get('wddl_all'),
					'text_empty'=>$this->textEmpty ? $this->textEmpty : Yii::app()->label->get('wddl_empty'),
					'text_not_found'=>$this->textNotFound ? $this->textNotFound : Yii::app()->label->get('wddl_not_found'),
					'text_required'=>Yii::app()->errorAttribute->get($model, $name, 'required')
				));
			}
		}
	}
	
	/**
	 * Метод-геттер для определения _fullnessModel
	 * @return boolean Признак применения виджета для атрибута модели
	 */
	protected function getFullnessModel(){
		if ($this->_fullnessModel === null)
			$this->_fullnessModel = Swamper::dataFullness(get_object_vars($this)) == Swamper::FULLNESS_MODEL;
		
		return $this->_fullnessModel;
	}
	
	/**
	 * Переопределение стандартой функции preparationHtmlOptions, подготовливающей HTML-опций для виджета
	 * @return array Ассоциативный массив HTML-опций
	 */
	protected function preparationHtmlOptions(){
		if (empty($this->htmlOptions['id']) && $this->fullnessModel)
			$this->htmlOptions['id'] = Html::activeId($this->model, $this->name);
		
		return parent::preparationHtmlOptions();
	}
	
}
