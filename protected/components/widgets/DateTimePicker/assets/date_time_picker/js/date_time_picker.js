$(window).load(function(){
	$('div.date-time-picker div.dtp-icon').on('click', function(){
		var input = $(this).parent().children('input');
		
		if (input.length && !input.attr('disabled'))
			input.focus();
	});
	
	$('div.date-time-picker div.dtp-icon').on('mouseenter mouseleave', function(event){
		$(this).parent().children('input').toggleClass('hover', event.type == 'mouseenter');
	});
});
