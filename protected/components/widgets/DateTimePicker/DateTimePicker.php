<?php
/**
 * Виджет выбора даты и времени
 * @author v.zubov
 * @see ext.datepicker.EJuiDateTimePicker
 */

Yii::import('ext.datepicker.EJuiDateTimePicker');

class DateTimePicker extends EJuiDateTimePicker{
	
	/**
	 * @var CActiveForm Экземпляр HTML-формы
	 */
	public $form;
	
	/**
	 * @var boolean Признак применения виджета для атрибута модели
	 */
	protected $_fullnessModel;
	
	/**
	 * Конструктор виджета
	 */
	public function init(){
		$this->attribute = $this->name;
		$html_options = Html::forming(array(
			'required'=>$this->fullnessModel && $this->model->isAttributeRequired($this->name),
			'id'=>$id = $this->fullnessModel ? Html::activeId($this->model, $this->name) : Widget::getIdCounter(),
			'dtp-full'=>$this->mode == 'datetime'
		));
		
		$this->htmlOptions = $this->htmlOptions ?
			Arrays::mergeParams(array('sources'=>array($this->htmlOptions, $html_options))) : $html_options;
		
		if ($this->fullnessModel)
			Html::registerInputPatternParams($id, $this->model, $this->name);
		
		parent::init();
	}
	
	/**
	 * Запуск виджета
	 */
	public function run(){
		Yii::app()->asset->add();
		
		echo Html::openTag('div', array('id'=>Widget::getIdCounter(), 'class'=>'date-time-picker'));
			
			parent::run();
			
			echo Html::tag('div', array('class'=>'dtp-icon'), '');
			
		echo Html::closeTag('div');
	}
	
	/**
	 * Метод-геттер для определения _fullnessModel
	 * @return boolean Признак применения виджета для атрибута модели
	 */
	protected function getFullnessModel(){
		if ($this->_fullnessModel === null)
			$this->_fullnessModel = Swamper::dataFullness(get_object_vars($this)) >= Swamper::FULLNESS_MODEL;
		
		return $this->_fullnessModel;
	}
	
}
