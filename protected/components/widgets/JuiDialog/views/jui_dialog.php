<?php
Yii::app()->asset->add();

echo Html::openTag('div', array('class'=>'dsp-none'));
	
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
		'htmlOptions'=>$html_options,
		'cssFile'=>false,
		'options'=>array(
			'title'=>$title,
			'autoOpen'=>$auto_open,
			'modal'=>$modal,
			'resizable'=>$resizable,
			'draggable'=>$draggable,
			'closeText'=>'',
			'closeOnEscape'=>$close_on_escape,
			'show'=>$show,
			'hide'=>$hide
		)
	));
		
		if ($action){
			if ($params)
				$action->runWithParams($params);
			else
				$action->run();
		}elseif ($view)
			require $view;
		elseif ($html)
			echo $html;
		
	$this->endWidget();

echo Html::closeTag('div');

$this->registerScriptParams(array(
	'relations'=>$relations,
	'buttonOk'=>$button_ok,
	'buttonCancel'=>$button_cancel,
	'idFormSubmit'=>$id_form_submit,
	'reset'=>$reset
));
