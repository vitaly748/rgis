wJuiDialog = {};
prefixStarterJD = 'starter-jd';

function wJuiDialogButtonClick(button, event){
	var jd = button.closest('div.ui-dialog-content');
	
	if (jd.length === 1){
		var idJd = jd.attr('id');
		
		if (idJd){
			var params = wJuiDialog[idJd];
			
			if (params){
				if (params.buttonCancel && button.hasClass(params.buttonCancel)){
					jd.dialog('close');
					event.preventDefault();
				}else if (params.buttonOk && button.hasClass(params.buttonOk)){
					var sourceButton = params.sourceButton,
						sourceButtonObj = params.sourceButtonObj;
					
					for (var count in params.relations){
						var rootItem = params.relations[count].rootItem,
							jdItem = params.relations[count].jdItem;
						
						if (rootItem && jdItem){
							if (rootItem[0] !== '.')
								rootItem = $(rootItem);
							else if (sourceButton)
								rootItem = eval(sourceButton + rootItem);
							else
								rootItem = false;
							
							if (jdItem[0] !== '.')
								jdItem = jd.find(jdItem);
							else
								jdItem = eval('$("div.ui-dialog-content#' + idJd + ' .button.' +
									params.buttonOk + '")' + jdItem);
							
							if (rootItem.length && jdItem.length){
								var value = jdItem.is('input') || jdItem.is('textarea') ? jdItem.val() : jdItem.html();
								
								if (rootItem.is('input') || rootItem.is('textarea')){
									var valueOld = rootItem.val();
									
									rootItem.val(value);
									
									if (valueOld != value)
										rootItem.change();
								}else
									rootItem.html(value);
							}
						}
					}
					
					jd.dialog('close');
					event.preventDefault();
					$("div#content > div.container-grid-view > div.curtain-loading").show();
					
					var submit = false;
					
					if (params.idFormSubmit){
						var form = $('form#' + params.idFormSubmit);
						
						if (form.length === 1){
							form.submit();
							submit = true;
						}
					}
					
					if (!submit && sourceButtonObj)
						if (typeof buttonCheckStarters == 'undefined' || !buttonCheckStarters(sourceButtonObj, true)){
							var href = sourceButtonObj.attr('href');
							
							if (href && href != '#')
								location = href;
						}
				}
			}
		}
	}
}

$(window).load(function(){
	$('div.jui-dialog').each(function(){
		var obj = $(this).parent();
		
		obj.addClass($(this).attr('id'));
		
		if (!$(this).hasClass('juid-not-closing'))
			obj.find('a.ui-dialog-titlebar-close').show();
	});
	
	$(document).on('click', '.button, a', function(event){
		if ($(this).hasClass('disabled'))
			return;
		
		var starterJD = parseId($(this), prefixStarterJD);
		
		if (starterJD.length){
			var params = wJuiDialog[starterJD[0]];
			
			if (params){
				var jd = $('div.ui-dialog-content#' + starterJD[0]);
				
				if (jd.length === 1){
					var idSource = $(this).attr('id'),
						classStarter = prefixStarterJD + '-' + starterJD[0] + (idSource ? '#' + idSource : ''),
						inputs = jd.find('input[type=text], textarea'),
						ddl = jd.find('div.drop-down-list').not('.disabled'),
						gridviews = jd.find('.grid-view'),
						focus = false;
					
					params.sourceButton = '$("' + ($(this).hasClass('button') ? '.button.' : 'a.') + classStarter + '")';
					params.sourceButtonObj = $(this);
					
					if (params.reset)
						inputs.val('').change();
					
					inputs.filter('.hasDatepicker').attr('disabled', true);
					inputs.filter('.filter-field').removeClass('filter-field').addClass('filter-field2');
					ddl.addClass('disabled');
					
					jd.dialog('open');
					
					setTimeout(function(){
						inputs.blur();
					}, 100);
					
					setTimeout(function(){
						inputs.each(function(){
							if ($(this).hasClass('hasDatepicker'))
								$(this).attr('disabled', false);
							else if ($(this).hasClass('filter-field2'))
								$(this).removeClass('filter-field2').addClass('filter-field');
							else if (!focus && !$(this).closest('div.drop-down-list').length &&
								!$(this).closest('div.filter').length){
									var input = $(this);
									
									focus = true;
									
									setTimeout(function(){
										input.focus();
									}, 100);
								}
						});
						
						if (typeof wButtonsTable !== 'undifined')
							wButtonsTableRepaint();
						
						if (typeof wValueListSetNeedId !== 'undefined' && jd.find('div.value-list').length)
							wValueListSetNeedId();
						
						gridviews.each(function(){
							if ($(this).find('td').length < 2)
								$.fn.yiiGridView.update($(this).attr('id'));
						});
						
						ddl.removeClass('disabled');
					}, 200);
				}
			}
			
			event.preventDefault();
		}
	});
	
	$(document).on('click', 'div.ui-dialog .button', function(event){
		if (!$(this).hasClass('disabled'))
			wJuiDialogButtonClick($(this), event);
	});
	
	$(document).on('keydown', 'div.ui-dialog input', function(event){
		if (event.which == 13){
			var jd = $(this).closest('div.ui-dialog-content');
			
			if (jd.length){
				var idJd = jd.attr('id');
				
				if (idJd){
					var params = wJuiDialog[idJd];
					
					if (params && typeof params.buttonOk !== 'undefined'){
						var button = $('.button.' + params.buttonOk);
						
						if (button.length)
							button.click();
					}
				}
			}
		}
	});
});
