<?php
/**
 * Виджет всплывающих окон
 * @author v.zubov
 */
class JuiDialog extends Widget{
	
	/**
	 * Псевдоним пути к папке представлений содержимого экземпляров данного виджета
	 */
	const DIR_VIEW = 'application.views.jui_dialogs';
	
	/**
	 * @var array HTML-опции объекта
	 */
	public $htmlOptions;
	
	/**
	 * @var string Название действия, генерирующего содержимое всплывающего окна
	 */
	public $action;
	
	/**
	 * @var Ассоциативный массив параметров
	 */
	public $params;
	
	/**
	 * @var string Название представления содержимого всплывающего окна
	 */
	public $view;
	
	/**
	 * @var string HTML-код содержимого всплывающего окна
	 */
	public $html;
	
	/**
	 * @var string Заголовок всплывающего окна. По умолчанию равно ''
	 */
	public $title = '';
	
	/**
	 * @var array Ассоциативный массив связей корневого HTML-кода с HTML-кодом всплывающего окна. При клике на
	 * элемент всплывающего окна с HTML-классами "button" и заданным параметром buttonOk, произойдёт перенос
	 * значений полей ввода всплывающего окна в связанные корневые поля ввода. Задаётся в формате
	 * {Строка поиска элементра всплывающего окна}=>{Строка поиска корневого элементра}. Поиск элементов
	 * всплывающего окна производится относительно контейнера всплывающего окна. Если первым символом строки поиска
	 * является точка, то поиск будет производиться относительно нажатой кнопки. Причём в случае со строкой поиска
	 * корневого элемента речь идёт о кнопке, вызвавшей открытие всплывающего окна, если таковая имеется
	 */
	public $relations;
	
	/**
	 * @var string HTML-класс утвердительной кнопки в контейнере всплывающего окна. По умолчанию равно 'ok'. По
	 * нажатию на эту кнопку будут произведены перенос введённых данных в соответствии с параметром relations и
	 * закрытие всплывающего окна
	 */
	public $buttonOk = 'ok';
	
	/**
	 * @var string HTML-класс отменяющей кнопки в контейнере всплывающего окна. По умолчанию равно 'cancel'. По
	 * нажатию на эту кнопку всплывающее окно будет закрыто без каких-либо дополнительных действий
	 */
	public $buttonCancel = 'cancel';
	
	/**
	 * @var string HTML-идентификатор формы, связанной с всплывающим окном. По нажатию на утвердительную кнопку
	 * всплывающего окна, после переноса данных и закрытия окна, данная форма будет автоматически направлена на
	 * проверку
	 */
	public $idFormSubmit;
	
	/**
	 * @var boolean Признак модальности диалогового окна. По умолчанию равно true
	 */
	public $modal = true;
	
	/**
	 * @var boolean Признак растягиваемости диалогового окна. По умолчанию равно false
	 */
	public $resizable = false;
	
	/**
	 * @var boolean Признак автооткрытия всплывающего окна. По умолчанию равно false
	 */
	public $autoOpen = false;
	
	/**
	 * @var boolean Признак перемещаемости всплывающего окна. По умолчанию равно true
	 */
	public $draggable = true;
	
	/**
	 * @var boolean Признак очистки полей ввода всплывающего окна перед его открытием. По умолчанию равно true
	 */
	public $reset = true;
	
	/**
	 * @var boolean Признак того, будет ли закрываться окно при нажатии клавиши ESCAPE. По умолчанию равно true
	 */
	public $closeOnEscape = true;
	
	/**
	 * @var boolean Признак того, можно ли закрыть окно при нажатии на крестик. По умолчанию равно true
	 */
	public $closing = true;
	
	/**
	 * @var array Опции эффекта появления окна
	 */
	public $show = array('effect'=>'fade', 'duration'=>300);
	
	/**
	 * @var array Опции эффекта скрытия окна
	 */
	public $hide = array('effect'=>'fade', 'duration'=>300);
	
	/**
	 * Запуск виджета
	 */
	public function run(){
		Yii::app()->controller->juid = true;
		$relations = array();
		
		/* if (!$this->autoOpen)
			Html::mergeClasses($this->htmlOptions, 'dsp-none', true); */
		
		if ($this->action){
			if (!$this->title)
				$this->title = Yii::app()->scheme->getSchemeItem(Yii::app()->controller->id.'/'.$this->action, 'title');
			
			if (!$this->title)
				$this->title = Yii::app()->scheme->getSchemeItem('*/'.$this->action, 'title');
			
			$this->action = Yii::app()->controller->createAction($this->action);
		}elseif ($this->view){
			if (!$this->title)
				$this->title = Yii::app()->label->get('wjd_title_'.$this->view);
			
			$this->view = is_file($path = Yii::getPathOfAlias(self::DIR_VIEW).'/'.$this->view.'.'.Swamper::EXT_PHP) ?
				$path : false;
		}
		
		if (is_array($this->relations))
			foreach ($this->relations as $jd_item=>$root_item)
				$relations[] = array('jdItem'=>$jd_item, 'rootItem'=>$root_item);
		
		if (!$this->closing)
			$this->closeOnEscape = false;
		
		$this->render('jui_dialog', array(
			'html_options'=>$this->preparationHtmlOptions(),
			'action'=>$this->action,
			'params'=>$this->params,
			'view'=>$this->view,
			'html'=>$this->html,
			'title'=>$this->title,
			'relations'=>$relations,
			'button_ok'=>$this->buttonOk,
			'button_cancel'=>$this->buttonCancel,
			'id_form_submit'=>$this->idFormSubmit,
			'auto_open'=>$this->autoOpen,
			'modal'=>$this->modal,
			'resizable'=>$this->resizable,
			'draggable'=>$this->draggable,
			'reset'=>$this->reset,
			'close_on_escape'=>$this->closeOnEscape,
			'show'=>$this->show,
			'hide'=>$this->hide
		));
		
		Yii::app()->controller->juid = null;
	}
	
	/**
	 * Переопределение стандартой функции preparationHtmlOptions, подготовливающей HTML-опций для виджета
	 * @return array Ассоциативный массив HTML-опций
	 */
	protected function preparationHtmlOptions(){
		parent::preparationHtmlOptions();
		
		if (!$this->closing)
			$this->htmlOptions['class'] .= ' juid-not-closing';
		
		return $this->htmlOptions;
	}
	
}
