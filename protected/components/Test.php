<?php
/**
 * Класс для осуществления тестирования PHP-кода приложения
 * @author v.zubov
 */
class Test extends Component{
	
	/**
	 * @var string Псевдоним пути к файлу конфигурации
	 */
	const DIR_CFG = 'application.config.test';
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'test.php';
	
	/**
	 * @var string Псевдоним пути к папке с файлами тестов
	 */
	const DIR_TESTS = 'application.tests';
	
	/**
	 * @var integer Код ошибки при поиске файла теста
	 */
	const ERROR_FILE = -1;
	
	/**
	 * @var integer Код утверждения false
	 */
	const ASSERT_FALSE = 0;
	
	/**
	 * @var integer Код утверждения true
	 */
	const ASSERT_TRUE = 1;
	
	/**
	 * @var integer Код утверждения equals
	 */
	const ASSERT_EQUALS = 2;
	
	/**
	 * @var integer Код утверждения not_equals
	 */
	const ASSERT_NOT_EQUALS = 3;
	
	/**
	 * @var string Прификс строки, содержащей комментарий к очередному утверждению в файлах тестов. Пример такой строки:
	 * //Тест 15 - Расчёт квадратного корня
	 */
	const PREFIX_TEST_TITLE = '//Тест ';
	
	/**
	 * @var object Ссылка на созданный экземпляр данного класса
	 */
	private static $_this;
	
	/**
	 * @var boolean Признак активации режима тестирования. Устанавливается, если задан хотя бы один тест на выполнение
	 */
	private $_active;
	
	/**
	 * @var array Массив с результатами тестирования. Имеет следующий формат:
	 * 	array(
	 * 		'error'=>0,
	 * 		'tests'=>array(
	 * 			'test_name'=>array(
	 * 				'error'=>0,
	 * 				'asserts'=>array(
	 * 					array(
	 * 						'type'=>ASSERT_TYPE_FALSE,
	 * 						'error'=>0,
	 * 						'func'=>'assertFalse,
	 * 						'line'=>0,
	 * 						'title'=>'',
	 * 						'args'=>array(
	 * 							arg1,
	 * 							...
	 * 						)
	 * 					),
	 * 					...
	 * 				)
	 * 			),
	 * 			...
	 * 		)
	 * 	)
	 */
	private $_result;
	
	/**
	 * @var array Ссылка на активный тест
	 */
	private $_test;
	
	/**
	 * @var array Ссылка на активный блок утверждений
	 */
	private $_asserts;
	
	/**
	 * @var array Содержимое обрабатываемого файла теста
	 */
	private $_text;
	
	/**
	 * Конструктор компонента. Осуществляет запуск всех заданных тестов с сохранением результата в
	 * переменную _result
	 */
	public function init(){
		self::$_this = $this;
		
		$path = Yii::getPathOfAlias(self::DIR_CFG).'/'.self::FN_CFG;
		
		if (is_file($path)){
			$names = require $path;
			
			if ($names && is_array($names)){
				$this->_active = true;
				$this->_result = array('error'=>0, 'tests'=>array());
				Yii::app()->controller->componentDef = true;
				
				foreach ($names as $name)
					$this->runTest($name);
			}
		}
	}
	
	/**
	 * Метод-геттер для определения _active
	 * @return Признак активации режима тестирования
	 */
	public function getActive(){
		return $this->_active;
	}
	
	/**
	 * Метод-геттер для определения _result
	 * @return Массив с информацией о результатах тестирования
	 */
	public function getResult(){
		return $this->_result;
	}
	
	/**
	 * Запуск теста
	 * @param string $name Название теста
	 */
	private function runTest($name){
		$path = Yii::getPathOfAlias(self::DIR_TESTS).'/'.$name.'.'.Swamper::EXT_PHP;
		$this->_test = array('error'=>0);
		
		if (is_file($path)){
			$this->_asserts = array();
			$this->_text = file($path, FILE_IGNORE_NEW_LINES);
			require $path;
			$this->_test['asserts'] = $this->_asserts;
		}else
			$this->_test['error'] = self::ERROR_FILE;
		
		$this->_result['tests'][$name] = $this->_test;
		if ($this->_test['error'])
			$this->_result['error']++;
		
		$this->_test = null;
	}
	
	/**
	 * Проверка утверждения "Ложь"
	 * @param mixed $value Значение для проверки
	 * @return boolean Истинность утверждения
	 */
	public static function assertFalse($value){
		$result = !$value;
		self::assertFix(self::ASSERT_FALSE, $result, array($value));
		return $result;
	}
	
	/**
	 * Проверка утверждения "Правда"
	 * @param mixed $value Значение для проверки
	 * @return boolean Истинность утверждения
	 */
	public static function assertTrue($value){
		$result = !!$value;
		self::assertFix(self::ASSERT_TRUE, $result, array($value));
		return $result;
	}
	
	/**
	 * Проверка утверждения "Равенство"
	 * @param mixed $value1 Значение №1 для проверки
	 * @param mixed $value2 Значение №2 для проверки
	 * @param boolean $type Признак проверки типа. По умолчанию равно true
	 * @return boolean Истинность утверждения
	 */
	public static function assertEquals($value1, $value2, $type = true){
		$result = $type ? $value1 === $value2 : $value1 == $value2;
		self::assertFix(self::ASSERT_EQUALS, $result, array($value1, $value2));
		return $result;
	}
	
	/**
	 * Проверка утверждения "Неравенство"
	 * @param mixed $value1 Значение №1 для проверки
	 * @param mixed $value2 Значение №2 для проверки
	 * @param boolean $type Признак проверки типа. По умолчанию равно true
	 * @return boolean Истинность утверждения
	 */
	public static function assertNotEquals($value1, $value2, $type = true){
		$result = $type ? $value1 !== $value2 : $value1 != $value2;
		self::assertFix(self::ASSERT_NOT_EQUALS, $result, array($value1, $value2));
		return $result;
	}
	
	/**
	 * Запись результата проверки утверждения в переменную _results
	 * @param number $type Код типа утверждения
	 * @param boolean $result Истинность утверждения
	 * @param array $args Список значений, участвующих в проверке утверждения
	 */
	private static function assertFix($type, $result, $args){
		if ($_this = self::$_this)
			if ($_this->_test){
				$trace = array_pop(debug_backtrace(1, 2));
				
				$title = '';
				$line = $trace['line'] - 1;
				while ($line && $_this->_text[$line])
					$line--;
				
				if ($line){
					$line++;
					$string = $_this->_text[$line++];
					$prefix = sprintf(self::PREFIX_TEST_TITLE, count($_this->_asserts) + 1);
					if (mb_strpos($string, self::PREFIX_TEST_TITLE) === 0){
						$pos = mb_strpos($string, '- ');
						if ($pos !== false){
							$title = mb_substr($string, $pos + 2);
							$count = $line;
							while (mb_strpos($_this->_text[$count], '//') === 0){
								$title .= ' '.mb_substr($_this->_text[$count], 2);
								$count++;
							}
						}
					}
				}else
					$line = $trace['line'];
				
				$_this->_asserts[] = array(
					'type'=>$type,
					'error'=>$error = !$result,
					'func'=>$trace['function'],
					'line'=>$line,
					'title'=>$title,
					'args'=>$args
				);
				
				if ($error)
					$_this->_test['error']++;
			}
	}
	
}
