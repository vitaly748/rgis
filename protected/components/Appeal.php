<?php
/**
 * Компонент для работы с обращениями
 * @author v.zubov
 */
class Appeal extends ComponentDeploy{
	
	/**
	 * @var string Код пользовательской темы обращения
	 */
	const CODE_USER_THEME = '4';
// 	const CODE_USER_THEME = '9999';
	
	/**
	 * @var array Массив соответствий кодов статусов этапов обработки обращений и кодов статусов обращений в формате
	 * 	array(
	 * 		{Код статуса этапа обработки обращения}=>{Код статуса обращения},
	 * 		...
	 * 	) 
	 */
	const STATE_CONFORMITY = array(
		10=>3,
		20=>8,
		30=>6,
		40=>6,
		50=>6,
		60=>4,
		70=>6,
		80=>5,
		90=>7,
		100=>6
	);
	
	/**
	 * @var array Список кодов статусов этапов обработки обращений, которые соответствуют статусу обращений "Подтверждено"
	 */
	const STATE_STAGE_CONFIRMED = array(30, 40, 50, 70, 100);
	
	/**
	 * @var integer Код статуса обращения "Подтверждено"
	 */
	const STATE_CONFIRMED = 6;
	
	/**
	 * @var integer Время, выделяемое на обработку обращения в секундах
	 */
	const TIME_LIMIT = 30 * 24 * 60 * 60;
	
	/**
	 * @var array Ассоциативный массив с границами порогового времени обработки обращений в секундах в порядке наращения приоритета
	 */
	const TIME_BORDER = array(3 * 24 * 60 * 60, 0);
	
	/**
	 * Конструктор компонента
	 */
	public function init(){
	}
	
}
