<?php
/**
 * Класс для работы с кодами подтверждения
 * @author v.zubov
 */
class UserCode extends Component{
	
	/**
	 * @var integer Длина кодов подтверждения. Допустимый диапазон: от 1 до 9
	 */
	const CODE_LENGTH = 5;
	
	/**
	 * Конструктор компонента
	 */
	public function init(){
	}
	
	/**
	 * Генерирование кода подтверждения с занесением в БД
	 * @param integer $id_user Идентификатор пользователя
	 * @param string $type Символьный идентфикатор типа кода подтверждения
	 * @return boolean|string Код подтверждения или false в случае ошибки
	 */
	public function getCode($id_user, $type){
		$params = array('id_user'=>$id_user, 'type'=>$type);
		
		try{
			$model = new UserCodeModel;
			$model->id_owner = $id_user;
			$model->code = $code = $this->generationCode();
			$model->type = Yii::app()->conformity->get('user_code_type', 'code', $type);
			$model->date = Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM);
			
			if (!$model->save(false))
				throw new CHttpException(500);
		}catch (CException $e){
			$params['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('code_generation', $params, $ok ? 'ok' : 'code_generation');
		
		return $ok ? $code : false;
	}
	
	/**
	 * Генерирование хеша кода подтверждения на основе логина и даты регистрации пользователя для отправки по e-mail
	 * @param UserModel $model Модель данных пользователя
	 * @param string $type Символьный идентфикатор типа кода подтверждения
	 * @return array|boolean Ассоциативный массив с кодом и хешем кода подтверждения или false
	 */
	public function getHash($model, $type){
		if (!$code = $this->getCode($model->id, $type))
			return false;
		
		$keys = str_split($code);
		
		do{
			$hash = Hashing::encode($model->login.$model->registration, $keys, Hashing::WEIGHT_DATE);
		}while (preg_match('/[.\/]/u', $hash));
		
		return array('code'=>$code, 'hash'=>$hash);
	}
	
	/**
	 * Проверка кода подтверждения
	 * @param string $code Код подтверждения
	 * @param string $type Символьный идентфикатор типа кода подтверждения
	 * @return boolean|UserModel Модель данных пользователя, соответствующая данному коду или false
	 */
	public function checkCode($code, $type){
		$params_fix = array('code'=>$code, 'type'=>$type);
		
		try{
			$conditions = 'code = :code AND type = :type';
			$params = array(':code'=>$code, ':type'=>Yii::app()->conformity->get('user_code_type', 'code', $type));
			
			if (($timeout = Yii::app()->setting->get('code_timeout')) > 0){
				$conditions .= ' AND date > :date';
				$params[':date'] = Swamper::date(time() - $timeout, Swamper::FORMAT_DATE_SYSTEM);
			}
			
			$model = UserModel::model()->with('code')->find($conditions, $params);
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !empty($model);
		
		Yii::app()->log->fix('code_check', $params_fix, $ok ? 'ok' : 'code_check');
		
		return $ok ? $model : false;
	}
	
	/**
	 * Проверка хеша кода подтверждения
	 * @param string $hash Хеш кода подтверждения
	 * @param string $type Символьный идентфикатор типа кода подтверждения
	 * @return boolean|UserModel Модель данных пользователя, соответствующая данному хешу или false
	 */
	public function checkHash($hash, $type){
		$params_fix = array('hash'=>$hash, 'type'=>$type);
		
		try{
			$conditions = 'type = :type';
			$params = array(':type'=>Yii::app()->conformity->get('user_code_type', 'code', $type));
			
			if (($timeout = Yii::app()->setting->get('code_timeout')) > 0){
				$conditions .= ' AND date > :date';
				$params[':date'] = Swamper::date(time() - $timeout, Swamper::FORMAT_DATE_SYSTEM);
			}
			
			$rows = UserModel::model()->with('code')->findAll($conditions, $params);
			
			if ($rows)
				foreach ($rows as $row){
					$keys = array_pad(str_split((string)$row->code[0]->code), -self::CODE_LENGTH, '0');
					
					if (Hashing::check($row->login.$row->registration, $hash, $keys, Hashing::WEIGHT_DATE)){
						$model = $row;
						break;
					}
				}
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !empty($model);
		
		Yii::app()->log->fix('code_hash_check', $params_fix, $ok ? 'ok' : 'code_hash_check');
		
		return $ok ? $model : false; 
	}
	
	/**
	 * Удаление кода подтверждения
	 * @param string $code Код подтверждения
	 * @param string $type Символьный идентфикатор типа кода подтверждения
	 * @boolean Успешность выполнения
	 */
	public function dropCode($code, $type){
		$params = array('code'=>$code, 'type'=>$type);
		
		try{
			if ($model = UserCodeModel::model()->find('code = :code AND type = :type',
				array(':code'=>$code, ':type'=>Yii::app()->conformity->get('user_code_type', 'code', $type))))
					$model->delete();
		}catch (CException $e){
			$params['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('code_drop', $params, $ok ? 'ok' : 'code_drop');
		
		return $ok;
	}
	
	/**
	 * Генерация кода подтверждения
	 * @return string Код подтверждения
	 */
	protected function generationCode(){
		$code = '';
		
		for ($count = 0; $count < self::CODE_LENGTH; $count++)
			$code .= rand(0, 9);
		
		return $code;
	}
	
}
