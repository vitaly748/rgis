<?php
/**
 * SAML 2.0 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote 
 */

/*
 * Guest IdP. allows users to sign up and register. Great for testing!
 */
$metadata['https://openidp.feide.no'] = array(
	'name' => array(
		'en' => 'Feide OpenIdP - guest users',
		'no' => 'Feide Gjestebrukere',
	),
	'description'          => 'Here you can login with your account on Feide RnD OpenID. If you do not already have an account on this identity provider, you can create a new one by following the create new account link and follow the instructions.',

	'SingleSignOnService'  => 'https://openidp.feide.no/simplesaml/saml2/idp/SSOService.php',
	'SingleLogoutService'  => 'https://openidp.feide.no/simplesaml/saml2/idp/SingleLogoutService.php',
	'certFingerprint'      => 'c9ed4dfb07caf13fc21e0fec1572047eb8a7a4cb'
);

$metadata['https://esia.gosuslugi.ru/idp/shibboleth'] = array(
	'SingleSignOnService'  => 'https://esia.gosuslugi.ru/idp/profile/SAML2/Redirect/SSO',
	'SingleLogoutService'  => 'https://esia.gosuslugi.ru/idp/profile/SAML2/Redirect/SLO',
	'certificate' => 'esia.public',
// 	'sign.authnrequest'=>true,
// 	'signature.algorithm'=>'http://www.w3.org/2000/09/xmldsig#rsa-sha1'
//	'certFingerprint'      => 'c9ed4dfb07caf13fc21e0fec1572047eb8a7a4cb'
//	'redirect.validate' => TRUE
);
