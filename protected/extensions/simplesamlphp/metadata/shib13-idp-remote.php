<?php
/**
 * SAML 1.1 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote
 */

/*
$metadata['theproviderid-of-the-idp'] = array(
	'SingleSignOnService'  => 'https://idp.example.org/shibboleth-idp/SSO',
	'certFingerprint'      => 'c7279a9f28f11380509e072441e3dc55fb9ab864',
);
*/

/*$metadata['https://esia-portal1.test.gosuslugi.ru'] = array(
	'SingleSignOnService'  => 'https://esia-portal1.test.gosuslugi.ru/idp/profile/SAML2/Redirect/SSO',
	'SingleLogoutService'  => 'https://esia-portal1.test.gosuslugi.ru/idp/profile/SAML2/Redirect/SLO',
	'certificate' => 'esia.public',
 	'sign.authnrequest'=>true,
 	'signature.algorithm'=>'http://www.w3.org/2000/09/xmldsig#rsa-sha1'
);*/