<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс Html
 * @author v.zubov
 * @see Html
 */
class Html extends CHtml{
	
	/**
	 * @var boolean|string Поддомен работы с сертефикатом повышенной безопасности или false
	 */
	const SECURE_HOST = 'lkg';
	
	/**
	 * @var boolean|string Поддомен работы с сертефикатом повышенной безопасности или false
	 */
	const SECURE_HOST_SIMPLE = 'lk';
	
	/**
	 * @var string Префикс HTML-класса элемента класса "button", определяющего его как активатора отправки формы
	 */
	const PREFIX_BUTTON_STARTER_SUBMIT = 'starter-submit';
	
	/**
	 * @var string Префикс HTML-класса элемента класса "button", определяющего его как активатора подачи команды
	 * через форму
	 */
	const PREFIX_BUTTON_STARTER_COMMAND = 'starter-command';
	
	/**
	 * @var string Префикс HTML-класса элемента класса "button", определяющего его как активатора всплывающей панели
	 */
	const PREFIX_BUTTON_STARTER_FP = 'starter-fp';
	
	/**
	 * @var string Префикс HTML-класса элемента класса "button", определяющего его как активатора Jui-диалога
	 */
	const PREFIX_BUTTON_STARTER_JD = 'starter-jd';
	
	/**
	 * @var string Префикс идентификатора элемента класса "flash-panel"
	 */
	const PREFIX_ID_FLASH_PANEL = 'fp-';
	
	/**
	 * @var string Префикс HTML-класса элемента, определяющего зависимый элемент
	 */
	const PREFIX_INPUT_CHILD = 'input-child';
	
	/**
	 * @var string Префикс HTML-идентификатора скрипта, задающего параметры редактируемого элемента
	 */
	const PREFIX_INPUT_PARAMS_SCRIPT_ID = 'input-params';
	
	/**
	 * @var string Префикс стандартного веб-адреса
	 */
	const PREFIX_URL = 'http://';
	
	/**
	 * @var string Префикс названия переменной, содержащей ассоциативный массив параметров виджета в скриптах 
	 */
	const PREFIX_SCRIPT_WIDGET = 'w';
	
	/**
	 * @var string Префикс HTML-класса, задающего некоторую ссылку 
	 */
	const PREFIX_CLASS_HREF = 'href';
	
	/**
	 * @var string HTML-идентификатор командного элемента
	 */
	const ID_COMMAND_DEF = 'form-command';
	
	/**
	 * @var string HTML-идентификатор элемента, содержащий закодированную информацию о фильтруемых идентификаторах
	 */
	const ID_FILTER_ID = 'filter-id';
	
	/**
	 * @var string HTML-идентификатор элемента, содержащий закодированную информацию о параметрах таблицы GridView
	 */
	const ID_GV_PARAMS = 'gv-params';
	
	/**
	 * @var string HTML-класс элемента, определяющего его как активатора клика по какому-то другому элементу
	 */
	const CLASS_STARTER_CLICK = 'starter-click';
	
	/**
	 * @var string HTML-класс элемента, задающий селектор элемента, по которому нужно совершить псевдо-клик
	 */
	const CLASS_TARGET_CLICK = 'target-click';
	
	/**
	 * @var string HTML-класс элемента класса "button", определяющий его работу в режиме "popup"
	 */
	const CLASS_BUTTON_FP_POPUP = 'fp-popup';
	
	/**
	 * @var string HTML-класс элемента, запрещающий передачу зависимости по атрибуту "disabled"
	 */
	const CLASS_INPUT_CHILD_DNI = 'input-child-dni';
	
	/**
	 * @var string HTML-класс зависимого элемента, запрещающий приём зависимости
	 */
	const CLASS_INPUT_CHILD_DISABLED = 'input-child-disabled';
	
	/**
	 * @var string HTML-класс зависимого элемента, запрещающий приём зависимости
	 */
	const CLASS_INPUT_LABEL_HOVER_DISABLED = 'input-label-hover-disabled';
	
	/**
	 * @var string HTML-класс элемента, требующего некоторый идентификатор для работы
	 */
	const CLASS_NEED_ID = 'need-id';
	
	/**
	 * @var boolean Признак необходимости автоматической генерации HTML-элемента типа "span" для HTML-элементов
	 * типов "checkbox" и "radiobutton", равен true 
	 */
	const CHR_SPAN_GENERATION = true;
	
	/**
	 * @var boolean Признак необходимости автоматической корректировки параметра "uncheckValue" в массиве
	 * опций HTML-элементов типа "checkbox" и "radio", равен true 
	 */
	const CHR_UNCHECK_CORRECT = true;
	
	/**
	 * @var string Название ключа, задающего параметры группы свойств HTML-элементов, в функции mergeParams
	 */
	const FORMING_PARAMS_PROPERTIES = 'properties';
	
	/**
	 * @var string Название ключа, задающего параметры группы классов HTML-элементов, в функции mergeParams
	 */
	const FORMING_PARAMS_CLASSES = 'classes';
	
	/**
	 * @var string Название ключа, задающего параметры группы стилей HTML-элементов, в функции mergeParams
	 */
	const FORMING_PARAMS_STYLES = 'styles';
	
	/**
	 * @var array Список названий параметров группы свойств HTML-элементов по умолчанию в функции mergeParams
	 */
	const FORMING_PROPERTIES_DEF = array('id', 'colspan', 'href', 'target', 'checked', 'readonly', 'disabled', 'name',
		'value', 'autocomplete', 'alt', 'title', 'for', 'required', 'multiple', 'accept');
	
	/**
	 * @var string Название типа HTML-элемента по умолчанию для функции element
	 */
	const ELEMENT_TYPE_DEF = 'html';
	
	/**
	 * @var string Название ключа, задающего экземпляр модели данных, используемой при регистрации параметров шаблона
	 * редактируемого элемента, в функции element
	 */
	const ELEMENT_PARAMS_IPPM = 'ippm';
	
	/**
	 * @var string Название ключа, задающего название атрибута модели данных, используемого при регистрации параметров шаблона
	 * редактируемого элемента, в функции element
	 */
	const ELEMENT_PARAMS_IPPN = 'ippn';
	
	/**
	 * @var string Название ключа, задающего дополнительные символы шаблона редактируемого элемента, в функции element
	 */
	const ELEMENT_PARAMS_IPPA = 'ippa';
	
	/**
	 * @var string Название JS-переменной, хранящей игнорируемые идентификаторы элементов класса "button need-id"
	 */
	const VAR_NEEDID_FILTER = 'needIdFilter';
	
	/**
	 * @var string IP-адрес локального подключения
	 */
	const IP_LOCALHOST = '127.0.0.1';
	
	/**
	 * @var boolean Признак работы по сертификату повышенной безопасности
	 */
	protected static $_secure;
	
	/**
	 * @var boolean|null Признак доступности поддомена с сертификатом повышенной безопасности
	 */
	protected static $_secureCheck;
	
	/**
	 * @var array Ассоциативный массив с информацией об атрибутах с зарегистрированными клиентскими обработчиками в формате:
	 * 	array(
	 * 		{Название модели данных атрибута}=>array(
	 * 			{Название атрибута}=>true
	 * 		),
	 * 		...
	 * 	)
	 */
	protected static $_clientValidateAttributes;
	
	/**
	 * @var boolean Признак наличия в клиентской обработке сообщения о вводе недопустимого символа 
	 */
	protected static $_clientInvalidCharCheck;
	
	/**
	 * Проверка существования хоста
	 * @param string $host Адрес хоста
	 * @return boolean Флаг существования хоста
	 */
	public static function hostExists($host){
		$ch = curl_init($host);
		curl_setopt($ch, CURLOPT_CONNECT_ONLY, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		$result = curl_exec($ch);
		curl_close($ch);
		
		return $result;
	}
	
	/**
	 * Определение self::$_secure
	 * @return boolean Признак работы по сертификату повышенной безопасности
	 */
	public static function isSecure(){
		if (self::$_secure === null)
			self::$_secure = self::SECURE_HOST && isset($_SERVER['HTTP_HOST']) ?
				reset(explode('.', $_SERVER['HTTP_HOST'])) === self::SECURE_HOST : false;
		
		return self::$_secure;
	}
	
	/**
	 * Определение self::$_secureCheck
	 * @return boolean|null Признак доступности поддомена с сертификатом повышенной безопасности
	 */
	public static function secureCheck(){
		$cookies = Yii::app()->request->cookies->cookies;
		
		if (self::$_secureCheck === null && ($cookie = Arrays::pop($cookies, 'secure')))
			self::$_secureCheck = (boolean)$cookie->value;
		
		return self::$_secureCheck;
	}
	
	/**
	 * Изменение поддомена безопасности ссылки
	 * @param boolean|string $url Исходная ссылка или false. По умолчанию равно false. Если равно false, будет использована строка
	 * текущего запроса
	 * @param boolean $secure Признак применения поддомена работы с сертификатом повышенной безопасности. По умолчанию равно true
	 * @return string Изменённая ссылка
	 */
	public static function changeSecureUrl($url = false, $secure = true){
		if (!$url)
			$url = Yii::app()->controller->createAbsoluteUrl(Yii::app()->controller->route, Yii::app()->controller->actionParams);
		
		if (!self::SECURE_HOST)
			return $url;
		
		$info = parse_url($url);
		
		return 'https://'.(($host = Arrays::pop($info, 'host')) ? preg_replace('/^[^\.]+/u',
			$secure ? self::SECURE_HOST : self::SECURE_HOST_SIMPLE, $host) : '').Arrays::pop($info, 'path');
	}
	
	/**
	 * Установка параметров куки-данных
	 */
	public static function setCookieParams(){
		if (self::SECURE_HOST && ($words = explode('.', $_SERVER['HTTP_HOST'])) && count($words) > 2) {
		    if (!is_numeric($words[0])) {
		        $words = array_slice($words, 1);
            }

            Yii::app()->getSession()->setCookieParams(array('domain' => implode('.', $words)));
        }
	}
	
	/**
	 * Формирование массива HTML-параметров
	 * @param array $params Ассоциативный массив с информацией о HTML-параметрах. Параметры делятся на
	 * три группы: "Свойства", "Классы" и "Стили". Задаются элементами с ключами self::FORMING_PARAMS_PROPERTIES,
	 * self::FORMING_PARAMS_CLASSES и self::FORMING_PARAMS_STYLES соответственно.
	 * Если принадлежность группе не задана явно, то она будет определена автоматически в соответствии с предопределённым набором
	 * стандартных ключей свойств. Определение наличия того или иного параметра определяется с помощью фонкции Arrays::forming
	 * @return array|boolean Массив с необходимыми объединёнными параметрами или false
	 */
	public static function forming($params){
		if (!is_array($params))
			return false;
		
		list($class, $style) = Arrays::pop($params, 'class, style', true);
		
		$classes = ($group = Arrays::pop($params, self::FORMING_PARAMS_CLASSES, true)) ? Arrays::forming($group, true) : array();
		$styles = ($group = Arrays::pop($params, self::FORMING_PARAMS_STYLES, true)) ? Arrays::forming($group) : array();
		
		if ($properties = Arrays::pop($params, self::FORMING_PARAMS_PROPERTIES, true))
			$params = Arrays::merge($params, $properties);
		
		if ($params = Arrays::forming($params))
			foreach ($params as $name=>$value)
				if (($key_num = !is_string($name)) || !in_array($name, self::FORMING_PROPERTIES_DEF, true)){
					if (!$key_num)
						$classes[] = $name;
					elseif (is_string($value))
						$classes[] = $value;
					
					unset($params[$name]);
				}elseif ($name == 'colspan' && $value < 2)
					unset($params[$name]);
				elseif ($name == 'href' && !$value)
					$params[$name] = '#';
				elseif ($name == 'target' && is_bool($value))
					$params[$name] = '_blank';
				elseif (in_array($name, array('checked', 'readonly', 'disabled', 'multiple')))
					$params[$name] = $name;
		
		if ($class)
			array_unshift($classes, $class);
		
		if ($classes)
			$params['class'] = implode(' ', $classes);
		
		if ($styles)
			foreach ($styles as $name=>$value)
				if (is_string($name))
					$styles[$name] = "$name: $value";
		
		if ($style)
			$styles[] = $style;
		
		if ($styles)
			$params['style'] = implode('; ', $styles);
		
		return $params;
	}
	
	/**
	 * Объединение классов массива HTML-опций элемента
	 * @param array $html_options Массив HTML-опций
	 * @param array|string $classes Дополняющая строка классов или массив из дополняющих классов. Может задаваться
	 * в формате {Класс}=>{Признак наличия данного класса}
	 * @param boolean $set Признак замещения значений в исходном массиве. По умолчанию равно false
	 * @return array Массив HTML-опций элемента с объединёнными классами 
	 */
	public static function mergeClasses(&$html_options, $classes, $set = false){
		$merge_html_options = $html_options;
		$merge_html_options['class'] = Strings::mergeWords(Arrays::pop($merge_html_options, 'class'), $classes);
		
		if ($set)
			$html_options = $merge_html_options;
		
		return $merge_html_options;
	}
	
	/**
	 * Подготовка HTML-кода для элемента, заданного параметрами
	 * @param array $params Массив, содержащий данные HTML-элемента. Список используемых параметров:
	 * <ul>
	 * <li>"type" - тип элемента ("checkbox", "error", "error_static", "html", "hidden", "link", "label",
	 * "password", "radio", "text", "textarea", "tooltip", "widget", "value"). Необязательный параметр. По умолчанию равно
	 * ELEMENT_TYPE_DEF</li>
	 * <li>"_fullness" - уровень заполненности данными. Необязательный параметр. Если не задан, вычисляется
	 * автоматически)</li>
	 * <li>"htmlOptions" - HTML-опции элемента. Необязательный параметр. По умолчанию равно array()</li>
	 * <li>"form" - Экземпляр HTML-формы класса ActiveForm, включающей данный элемент. Необязательный параметр. По умолчанию
	 * равно false</li>
	 * <li>"model" - Экземпляр модели данных. Необязательный параметр. По умолчанию равно false</li>
	 * <li>"name" - Название атрибута модели данных, сопряжимого с данным элементом. Необязательный параметр. По умолчанию
	 * равно false</li>
	 * <li>"value" - Значение элемента. Необязательный параметр. По умолчанию равно false</li>
	 * <li>self::ELEMENT_PARAMS_IPPM - Экземпляр модели данных, используемой при регистрации параметров шаблона редактируемого
	 * элемента. Необязательный параметр. По умолчанию равно параметру "model"</li>
	 * <li>self::ELEMENT_PARAMS_IPPN - Название атрибута модели данных, используемого при регистрации параметров шаблона
	 * редактируемого элемента. Необязательный параметр. По умолчанию равно параметру "name"</li>
	 * <li>self::ELEMENT_PARAMS_IPPA - Дополнительные символы шаблона редактируемого элемента. Необязательный параметр.
	 * По умолчанию равно false</li>
	 * </ul>
	 * @return boolean|string HTML-код элемента или false в случае ошибки
	 */
	public static function element($params){
		if (!$params = Swamper::dataFullnessCorrect($params))
			return false;
		
		$type = isset($params['type']) ? $params['type'] : self::ELEMENT_TYPE_DEF;
		$fullness = isset($params['_fullness']) ? $params['_fullness'] : Swamper::dataFullness($params);
		
		list($form, $model, $name, $value) = Arrays::pop($params, 'form, model, name, value');
		
		$attribute = ($brackets = mb_substr($name, -2) === '[]') ? mb_substr($name, 0, -2) : $name;
		$html_options = isset($params['htmlOptions']) ? $params['htmlOptions'] : array();
		$patterned = in_array($type, array('text', 'password', 'textarea'));
		$editable = $patterned || in_array($type, array('radio', 'checkbox'));
		
		if (!$brackets && $model && in_array($name, $model->multipleAttributeNames))
			$name .= '[]';
		
		if ($patterned || in_array($type, array('radio', 'checkbox', 'hidden'))){
			if ($fullness >= Swamper::FULLNESS_MODEL)
				self::resolveNameID($model, $name, $html_options);
			elseif (!isset($html_options['id']) && $name)
				$html_options['id'] = self::getIdByName($name);
			
			if ($patterned){
				$ipp_model = !empty($params[self::ELEMENT_PARAMS_IPPM]) ? $params[self::ELEMENT_PARAMS_IPPM] : $model;
				$ipp_name = !empty($params[self::ELEMENT_PARAMS_IPPN]) ? $params[self::ELEMENT_PARAMS_IPPN] : $attribute;
				$ipp_addition = !empty($params[self::ELEMENT_PARAMS_IPPA]) ? $params[self::ELEMENT_PARAMS_IPPA] : false;
				
				if ($ipp_model && $ipp_name)
					self::registerInputPatternParams($html_options['id'], $ipp_model, $ipp_name, $ipp_addition);
			}
		}elseif ($type == 'value' && !isset($html_options['id']) && $fullness >= Swamper::FULLNESS_MODEL)
			$html_options['id'] = self::activeId($model, $attribute);
		
		if ($fullness >= Swamper::FULLNESS_MODEL)
			if (!$access = $model->accessAttributes[$attribute])
				return false;
			else{
				if (($editable || $type == 'label') && $access === 'view'){
					if (!($disabled = isset($html_options['disabled'])) &&
						($type != 'label' || in_array($model->action, array('create', 'update'))))
							$html_options['disabled'] = 'disabled';
					
					if ($editable && $disabled && $html_options['disabled'] === 'disabled')
						self::mergeClasses($html_options, self::CLASS_INPUT_CHILD_DISABLED, true);
				}
				
				if ($patterned && ($placeholder = $model->getAttributePlaceholder($attribute)))
					$html_options['placeholder'] = $placeholder;
			}
		
		switch ($type){
			case 'label':
				return $fullness >= Swamper::FULLNESS_MODEL ? self::activeLabelEx($model, $attribute, $html_options) :
					self::label($value, isset($html_options['for']) ? $html_options['for'] : false, $html_options);
			case 'text':
				if ($fullness >= Swamper::FULLNESS_MODEL){
					if ($value !== null)
						$model->$attribute = $value;
					
					return self::activeTextField($model, $name, $html_options);
				}else
					return self::textField($name, $value, $html_options);
			case 'textarea':
				return $fullness >= Swamper::FULLNESS_MODEL ? self::activeTextArea($model, $name, $html_options) :
					self::textArea($name, $value, $html_options);
			case 'password':
				return $fullness >= Swamper::FULLNESS_MODEL ? self::activePasswordField($model, $name, $html_options) :
					self::passwordField($name, $value, $html_options);
			case 'radio':
			case 'checkbox':
				if ($name){
					if ($fullness >= Swamper::FULLNESS_MODEL){
						if (!isset($html_options['checked']) && isset($html_options['value'])){
							$value_model = $model->$attribute;
							$value_input = $html_options['value'];
							$is_array = is_array($value_model);
							
							if ((!$is_array && $value_model == $value_input) || ($is_array && in_array($value_input, $value_model)))
								$html_options['checked'] = 'checked';
						}
						
						$html = $type === 'radio' ? self::activeRadioButton($model, $name, $html_options) :
							self::activeCheckBox($model, $name, $html_options);
					}else
						$html = $type === 'radio' ?
							self::radioButton($name, isset($html_options['checked']) ? $html_options['checked'] : false, $html_options) :
							self::checkBox($name, isset($html_options['checked']) ? $html_options['checked'] : false, $html_options);
					
					if (!empty($params['label']))
						$html .= self::element(array(
							'type'=>'label',
							'value'=>$params['label'],
							'htmlOptions'=>!empty($html_options['id']) ? array('for'=>$html_options['id']) : array()
						));
					
					return $html;
				}
				
				break;
			case 'hidden':
				return $fullness >= Swamper::FULLNESS_MODEL ? self::activeHiddenField($model, $name, $html_options) :
					self::hiddenField($name, $value, $html_options);
			case 'error':
				if ($fullness === Swamper::FULLNESS_FORM)
					return $form->error($model, $name, $html_options);
				elseif ($fullness === Swamper::FULLNESS_MODEL)
					return self::error($model, $name, $html_options);
				
				break;
			case 'error_static':
				if ($fullness >= Swamper::FULLNESS_MODEL)
					return self::errorStatic($model, $attribute, $html_options);
				
				break;
			case 'tooltip':
				return Yii::app()->controller->widget(Widget::alias('Tooltip'), array(
					'form'=>$form,
					'model'=>$model,
					'name'=>$name,
					'value'=>$value
				), true);
				
				break;
			case 'widget':
				if ($wn = $params['widgetName'])
					return Yii::app()->controller->widget(Widget::alias($wn), Arrays::pop($params, 'widgetParams'), true);
				
				break;
			case 'link':
				$url = Arrays::pop($html_options, 'href', true);
				
				if ($fullness >= Swamper::FULLNESS_MODEL){
					if (!$url)
						$url = $model->$name;
					
					$text = $value;
				}else{
					if (!$url)
						$url = $value;
					
					$text = $name;
				}
				
				return self::link((string)$text, $url ? $url : '#', $html_options);
			case 'value':
				self::mergeClasses($html_options, 'value', true);
				
				if ($fullness >= Swamper::FULLNESS_MODEL){
					$value = $model->$attribute;
					
					if ($value && isset($model->accessValues[$attribute])){
						$values = Arrays::select($model->accessValues[$attribute], 'description', 'code', false, 'view, update', 'access');
						
						if (!is_array($value))
							$value = Arrays::pop($values, $value);
						else{
							foreach ($value as $key=>$item)
								if ($new_value = Arrays::pop($values, $item))
									$value[$key] = $new_value;
								else
									unset($value[$key]);
						}
					}
				}
				
				if (is_array($value))
					if (!$value)
						$value = '';
					elseif (count($value) == 1)
						$value = reset($value);
					elseif (Strings::hasWord($class = $html_options['class'], 'value-list')){
						$items = '';
						
						foreach ($value as $item)
							$items .= self::tag('li', array(), $item);
						
						$value = self::tag('ul', array(), $items);
					}else
						$value = implode(', ', $value);
				
				if ($value && in_array($attribute, BaseModel::ATTRIBUTES_DATE))
					$value = Swamper::date($value);
					
				return Html::tag('div', $html_options, $value ? $value : '&mdash;');
			case 'html':
				return $fullness >= Swamper::FULLNESS_MODEL ? $model->$attribute : $value;
			default:
				return Html::tag($type, $html_options, $fullness >= Swamper::FULLNESS_MODEL ? $model->$attribute : $value);
		}
		
		return false;
	}
	
	/**
	 * Генерация HTML-идентификаторов для атрибутов модели
	 * @param CModel $model Модель данных
	 * @param array $attributes Список названий атрибутов
	 * @return array Список HTML-идентификаторов
	 */
	public static function activeIds($model, $attributes){
		$ids = array();
		
		foreach ($attributes as $attribute)
			$ids[$attribute] = self::activeId($model, $attribute);
		
		return $ids;
	}
	
	/**
	 * Переопределение стандартного метода activeCheckBox, генерирующего HTML-код элемента типа "checkbox" для формы
	 * @param CModel $model Модель данных
	 * @param string $attribute Название атрибута
	 * @param array $htmlOptions HTML-опции генерируемого элемента. По умолчанию равно array()
	 * @return string HTML-код сгенерированного элемента
	 * @see CHtml::activeCheckBox()
	 */
	public static function activeCheckBox($model, $attribute, $htmlOptions = array()){
		$span = self::preparationChR($htmlOptions, 'checkbox');
		
		return parent::activeCheckBox($model, $attribute, $htmlOptions).$span;
	}
	
	/**
	 * Переопределение стандартного метода checkBox, генерирующего HTML-код элемента типа "checkbox"
	 * @param string $name Название элемента
	 * @param boolean $checked Признак выделенности элемента. По умолчанию равно false
	 * @param array $htmlOptions HTML-опции генерируемого элемента. По умолчанию равно array()
	 * @return string HTML-код сгенерированного элемента
	 * @see CHtml::сheckBox()
	 */
	public static function checkBox($name, $checked = false, $htmlOptions = array()){
		$span = self::preparationChR($htmlOptions, 'checkbox');
		
		return parent::checkBox($name, $checked, $htmlOptions).$span;
	}
	
	/**
	 * Переопределение стандартного метода activeRadioButton, генерирующего HTML-код элемента типа "radio" для формы
	 * @param CModel $model Модель данных
	 * @param string $attribute Название атрибута
	 * @param array $htmlOptions HTML-опции генерируемого элемента. По умолчанию равно array()
	 * @return string HTML-код сгенерированного элемента
	 * @see CHtml::activeRadioButton()
	 */
	public static function activeRadioButton($model, $attribute, $htmlOptions = array()){
		$span = self::preparationChR($htmlOptions, 'radio');
		
		return parent::activeRadioButton($model, $attribute, $htmlOptions).$span;
	}
	
	/**
	 * Переопределение стандартного метода radioButton, генерирующего HTML-код элемента типа "radio"
	 * @param string $name Название элемента
	 * @param boolean $checked Признак выделенности элемента. По умолчанию равно false
	 * @param array $htmlOptions HTML-опции генерируемого элемента. По умолчанию равно array()
	 * @return string HTML-код сгенерированного элемента
	 * @see CHtml::radioButton()
	 */
	public static function radioButton($name, $checked = false, $htmlOptions = array()){
		$span = self::preparationChR($htmlOptions, 'radio');
		
		return parent::radioButton($name, $checked, $htmlOptions).$span;
	}
	
	/**
	 * Переопределение стандартного метода ajaxSubmitButton, генерирующего HTML-код элемента типа "ajaxSubmitButton"
	 * @param string $label Текст на кнопке
	 * @param mixed $url URL-адрес запроса
	 * @param array $ajaxOptions AJAX-опции
	 * @param array $htmlOptions HTML-опции кнопки
	 * @return string HTML-код кнопки
	 * @see CHtml::ajaxSubmitButton()
	 */
	public static function ajaxSubmitButton($label, $url, $ajaxOptions = array(), $htmlOptions = array()){
		$ajaxOptions['type'] = 'POST';
		$htmlOptions['type'] = 'submit';
		
		return self::ajaxButton($label, $url, $ajaxOptions, $htmlOptions);
	}
	
	/**
	 * Переопределение стандартного метода ajaxButton, генерирующего HTML-код элемента типа "ajaxButton"
	 * @param string $label Текст на кнопке
	 * @param mixed $url URL-адрес запроса
	 * @param array $ajaxOptions AJAX-опции
	 * @param array $htmlOptions HTML-опции кнопки
	 * @return string HTML-код кнопки
	 * @see CHtml::ajaxButton()
	 */
	public static function ajaxButton($label, $url, $ajaxOptions = array(), $htmlOptions = array()){
		$ajaxOptions['url'] = $url;
		$htmlOptions['ajax'] = $ajaxOptions;
		
		return self::button($label, $htmlOptions);
	}
	
	/**
	 * Переопределение стандартного метода button, генерирующего HTML-код элемента типа "button"
	 * @param string $label Текст на кнопке
	 * @param array $htmlOptions HTML-опции кнопки
	 * @return string HTML-код кнопки
	 * @see CHtml::button()
	 */
	public static function button($label = 'button', $htmlOptions = array()){
		if (!isset($htmlOptions['name']) && !array_key_exists('name', $htmlOptions))
			$htmlOptions['name'] = self::ID_PREFIX.self::$count++;
		
		if (!isset($htmlOptions['type']))
			$htmlOptions['type'] = 'button';
		
		if (!isset($htmlOptions['value']) && $htmlOptions['type'] != 'image')
			$htmlOptions['value'] = $label;
		
		self::clientChange('click',$htmlOptions);
		
		return self::tag('button', $htmlOptions, $htmlOptions['value']);
	}
	
	/**
	 * Переопределение стандартного метода tag, генерирующего HTML-код некоторого элемента
	 * @param string $tag Тип элемента
	 * @param array $htmlOptions HTML-опции генерируемого элемента. По умолчанию равно array()
	 * @param mixed $content HTML-код содержимого элемента или false. По умолчанию равно false
	 * @param boolean $closeTag Признак необходимости закрытия тега. По умолчанию равно true
	 * @return string HTML-код сгенерированного элемента
	 * @see CHtml::tag()
	 */
	public static function tag($tag, $htmlOptions = array(), $content = false, $closeTag = true){
		$span = $tag === 'input' && isset($htmlOptions['type']) &&
			in_array($htmlOptions['type'], array('checkbox', 'radio')) ? self::preparationChR($htmlOptions) : '';
		
		return parent::tag($tag, $htmlOptions, $content, $closeTag).$span;
	}
	
	/**
	 * Герерорование HTML-кода статичного элемента, содержащего текст ошибки атрибута модели
	 * @param CModel $model Модель данных
	 * @param string $attribute Название атрибута модели
	 * @param boolean|string $validator Символьный идентификатор валидатора, содержащего текст ошибки.
	 * По умолчанию равно false. Для обязательного атрибута по умолчанию равно "required"
	 * @param boolean $hidden Признак изначальной скрытости элемента. По умолчанию равно true
	 * @return string
	 */
	public static function errorStatic($model, $attribute, $html_options = array()){
		$error = $model->getError($attribute);
		
		$html_options = Arrays::mergeParams(array('sources'=>array($html_options, self::forming(array(
			'id'=>self::activeId($model, $attribute).'_em_', self::$errorMessageCss, 'dsp-none'=>!$error)))));
		
		return self::tag(self::$errorContainerTag, $html_options, $error ? $error : '');
	}
	
	/**
	 * Получение IP-адреса клиента
	 * @return boolean|string IP-адрес или false
	 */
	public static function getUserAddr(){
		if ((!$addr = Arrays::pop($_SERVER, 'REMOTE_ADDR')) || !preg_match('/^\d+\.\d+\.\d+\.\d+(:\d+)?$/u', $addr))
			return false;
		
		return $addr;
	}
	
	/**
	 * Регистрация параметров редактируемого HTML-элемента в скриптах
	 * @param string $id HTML-идентификатор редактируемого элемента (input[type=text, password], textarea)
	 * @param CModel $model Экземпляр модели данных, связанной с редактируемым элементом
	 * @param string $name Название атрибута модели данных
	 * @param string $addition Дополнительные символы шаблона редактируемого элемента
	 * @return boolean Успешность выполнения
	 */
	public static function registerInputPatternParams($id, $model, $attribute, $addition = false){
		if (!$model instanceof CModel || (!$params = $model->getAttributePattern($attribute, $addition)))
			return false;
		
		$sets = Arrays::pop($params, 'sets', true);
		$params = CJavaScript::encode($params);
		$text_script = "inputPattern['$id'] = $params;";
		
		if ($sets)
			foreach ($sets as $id_set=>$set){
				$set = CJavaScript::encode($set);
				$text_script .= PHP_EOL."inputPatternGroupSet[$id_set] = $set;";
			}
		
		if (!self::$_clientInvalidCharCheck){
			if ($message = CJavaScript::encode(Yii::app()->errorAttribute->get(false, false, 'invalid_char')))
				$text_script .= PHP_EOL."inputInvalidCharMessage = $message;";
			
			self::$_clientInvalidCharCheck = true;
		}
		
		Yii::app()->getClientScript()->registerScript(self::PREFIX_INPUT_PARAMS_SCRIPT_ID.'-'.$id, $text_script);
		
		return true;
	}
	
	/**
	 * Формирование стандартного веб-адреса с параметрами
	 * @param string $url Основная часть веб-адреса
	 * @param array|boolean $params Ассоциативный массив GET-параметров или false. По умолчанию равно false
	 */
	public static function formingUrl($url, $params = false){
		if (!$url)
			return false;
		
		if (mb_strpos($url, '://') === false)
			$url = self::PREFIX_URL.$url;
		
		if ($params){
			$amp = 0;
			
			if (($pos = mb_strpos($url, '?')) === false)
				$url .= '?';
			elseif ($pos != mb_strlen($url) - 1)
				$amp = 1;
			
			foreach ($params as $key=>$value)
				$url .= ($amp++ ? '&' : '').$key.'='.urlencode($value);
		}
		
		return $url;
	}
	
	/**
	 * Корректировка скрипта валидации атрибута с тем, чтобы запускать стандартную валидацию только после потери фокуса полем
	 * @param CModel $model Экземпляр модели данных атрибутв
	 * @param string $attribute Название атрибута
	 * @param string $js Исходный скрипт валидатора
	 * @return string Скорректированный скрипт
	 */
	public static function clientValidateAttribute($model, $attribute, $js){
		if ($js && !isset(self::$_clientValidateAttributes[$model->modelName][$attribute])){
			self::$_clientValidateAttributes[$model->modelName][$attribute] = true;
			$js_app = <<<JS
var input = $('#' + $(this)[0]['inputID']),
		inputType = input.attr('type');

if ((inputType == 'text' || inputType == 'password') && input.is(':focus'))
	return;

JS;
			$js = $js_app.$js;
		}
		
		return $js;
	}
	
	/**
	 * Регистрация параметров представления в скриптах
	 * @param string $class Название класса блока параметров
	 * @param integer $id Идентификатор блока параметров
	 * @param array $params Параметры блока
	 * @param array $params_base Базовые параметры блока. По умолчанию равно false
	 * @param boolean|string $section Название секции добавления параметров или false. По умолчанию равно false
	 * @param boolean|string $prefix Префикс Названия ассоциативного массива параметров в скриптах или false. По умолчанию
	 * равно false
	 */
	public static function registerScriptParams($class, $id, $params, $params_base = false,
		$section = false, $prefix = false){
			$name = $prefix.$class;
			$js = '';
			
			if ($section)
				$section = '.'.$section;
			
			if ($params_base){
				$params_base = CJavaScript::encode($params_base);
				$js = "$name$section = $params_base;";
			}
			
			if ($id && $params){
				if ($js)
					$js .= PHP_EOL;
				
				$params = CJavaScript::encode($params);
				$js .= $name.$section."['$id'] = $params;";
			}
			
			if ($js){
				if ($id)
					$name .= "-$id";
				
				Yii::app()->getClientScript()->registerScript($name, $js);
			}
		}
	
	/**
	 * Генерация HTML-кода представления даты и времени с выделением даты
	 * @param string $date Исходная строка с датой
	 * @return string HTML-код
	 */
	public static function date($date){
		$paths = Strings::devisionWords($date, false, ' ');
		
		return count($paths) == 2 ? Html::tag('strong', array(), $paths[0]).' '.$paths[1] : $date;
	}
	
	/**
	 * Генерация дополнительного стилевого HTML-элемента "span" для HTML-элементов типа "checkbox" и "radiobutton"
	 * @param array $htmlOptions HTML-опции элемента 
	 * @param string $class HTML-класс элемента или false. По умолчанию равно false
	 * @return string HTML-код сгенерированного элемента
	 */
	protected static function preparationChR(&$htmlOptions, $class = false){
		if (self::CHR_SPAN_GENERATION){
			if (!$class && isset($htmlOptions['type']))
				$class = $htmlOptions['type'];
			
			if ($class === 'checkbox' && isset($htmlOptions['class']) && Strings::hasWord($htmlOptions['class'], 'toggle', true))
				$class .= ' toggle';
			
			if (isset($htmlOptions['class']) && Strings::hasWord($htmlOptions['class'], self::CLASS_INPUT_LABEL_HOVER_DISABLED, true))
				$class .= ' '.self::CLASS_INPUT_LABEL_HOVER_DISABLED;
			
			$span = parent::tag('span', $class ? array('class'=>$class) : array(), '');
		}
		
		if (self::CHR_UNCHECK_CORRECT && !isset($htmlOptions['uncheckValue']))
			$htmlOptions['uncheckValue'] = null;
		
		return isset($span) ? $span : '';
	}
	
}
