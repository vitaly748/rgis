<?php
/**
 * Класс дополнительных функций различной тематики
 * @author v.zubov
 */
class Swamper{
	
	/**
	 * @var string Расширение PHP-файлов
	 */
	const EXT_PHP = 'php';
	
	/**
	 * @var integer Код уровеня заполненности данными "Низший"
	 */
	const FULLNESS_NONE = 0;
	
	/**
	 * @var integer Код уровеня заполненности данными "Значение"
	 */
	const FULLNESS_VALUE = 1;
	
	/**
	 * @var integer Код уровеня заполненности данными "Имя"
	 */
	const FULLNESS_NAME = 2;
	
	/**
	 * @var integer Код уровеня заполненности данными "Модель"
	 */
	const FULLNESS_MODEL = 3;
	
	/**
	 * @var integer Код уровеня заполненности данными "Форма"
	 */
	const FULLNESS_FORM = 4;
	
	/**
	 * @var array Список стандартных названий параметров для массивов параметров HTML-элементов
	 */
	const FULLNESS_PARAMS  = array('type', 'form', 'model', 'name', 'value', 'label');
	
	/**
	 * @var array Список исключений для расширенных названий параметров, которые не могут быть преобразованы к
	 * стандартным
	 */
	const FULLNESS_PARAMS_EXCEPTIONS = array('widgetName');
	
	/**
	 * @var string Единица измерения "метры"
	 */
	const UNIT_METER = 'м';
	
	/**
	 * @var string Единица измерения "проценты"
	 */
	const UNIT_PERCENT = '%';
	
	/**
	 * @var string Единица измерения "рубли"
	 */
	const UNIT_RUB = 'р';
	
	/**
	 * @var string Единица измерения "байт"
	 */
	const UNIT_BYTE = 'Б';
	
	/**
	 * @var string Единица измерения "килобайт"
	 */
	const UNIT_KBYTE = 'Кб';
	
	/**
	 * @var string Единица измерения "мегабайт"
	 */
	const UNIT_MBYTE = 'Мб';
	
	/**
	 * @var string Единица измерения "гигабайт"
	 */
	const UNIT_GBYTE = 'Гб';
	
	/**
	 * @var string Формат даты для обмена с РГИС
	 */
	const FORMAT_DATE_RGIS = 'd.m.Y H:i:s';
	
	/**
	 * @var string Формат даты пользовательский
	 */
	const FORMAT_DATE_USER = 'd.m.Y H:i';
	
	/**
	 * @var string Формат даты пользовательский без времени
	 */
	const FORMAT_DATE_USER_WITHOUT_TIME = 'd.m.Y';
	
	/**
	 * @var string Формат даты пользовательский без дня
	 */
	const FORMAT_DATE_USER_WITHOUT_DAY = 'm.Y';
	
	/**
	 * @var string Формат даты пользовательский без дня с текстовым представление месяца
	 */
	const FORMAT_DATE_USER_WITHOUT_DAY_FULL = 'F Y';
	
	/**
	 * @var string Формат даты системный
	 */
	const FORMAT_DATE_SYSTEM = 'Y-m-d H:i:s';
	
	/**
	 * @var string Формат даты системный без времени
	 */
	const FORMAT_DATE_SYSTEM_WITHOUT_TIME = 'Y-m-d';
	
	/**
	 * @var string Псевдоним пути к каталогу содержащему файлы для парсинга
	 */
	const DIR_PARSE = 'application.data';
	
	/**
	 * @var array Ассоциативный массив с переводом названий месяцев
	 */
	const TRANSLATE_MONTH = array(
		'January'=>'Январь',
    'February'=>'Февраль',
    'March'=>'Март',
    'April'=>'Апрель',
    'May'=>'Май',
    'June'=>'Июнь',
    'July'=>'Июль',
    'August'=>'Август',
    'September'=>'Сентябрь',
    'October'=>'Октябрь',
    'November'=>'Ноябрь',
    'December'=>'Декабрь'
	);
	
	/**
	 * Определение уровня заполненности данными для HTML-элемента
	 * @param array $params Массив, содержащий данные
	 * @return boolean|integer Код уровня заполненности данными или false
	 */
	public static function dataFullness($params){
		if (!$params = self::dataFullnessCorrect($params))
			return false;
		
		$form = !empty($params['form']) && $params['form'] instanceof CActiveForm;
		$model = !empty($params['model']) && $params['model'] instanceof CModel;
		$name = !empty($params['name']) && is_string($params['name']);
		$value = isset($params['value']);
		
		if ($form && $model && $name)
			return self::FULLNESS_FORM;
		elseif ($model && $name)
			return self::FULLNESS_MODEL;
		elseif ($name)
			return self::FULLNESS_NAME;
		elseif ($value)
			return self::FULLNESS_VALUE;
		else
			return self::FULLNESS_NONE;
	}
	
	/**
	 * Корректировка названий параметров, работающих с моделями данных, или проверка на наличие таких параметров в
	 * массиве параметров HTML-элемента
	 * @param array $params Массив параметров или массив названий параметров
	 * @param boolean $check Признак проведения проверки на наличие параметров, работающих с моделями данных
	 * @return array|boolean Массив с скорректированными названиями параметров или false
	 */
	public static function dataFullnessCorrect($params, $check = false){
		if (!$params || !is_array($params))
			return false;
		
		$fullness_params = array_fill_keys(self::FULLNESS_PARAMS, false);
		$index = is_string(reset(array_keys($params)));
		$params_res = array();
		
		foreach ($params as $name=>$value){
			$fullness_param = false;
			
			foreach ($fullness_params as $fp_name=>$fp_find){
				$name_index = $index ? $name : $value;
				
				if (!$fp_find && mb_stripos($name_index, $fp_name) !== false &&
					!in_array($name_index, self::FULLNESS_PARAMS_EXCEPTIONS)){
						$fullness_params[$fp_name] = true;
						$fullness_param = true;
						$params_res[$index ? $fp_name : count($params_res)] = $value;
						break;
					}
			}
			
			if (!$fullness_param)
				$params_res[$index ? $name : count($params_res)] = $value;
		}
		
		return $check ? $fullness_params['model'] && $fullness_params['name'] : $params_res;
	}
	
	/**
	 * Таймер
	 * @param float $tick Счётчик времени
	 * @param boolean $reset Признак сброса таймера. По умолчанию равно true
	 * @param integer $round Число знаков после запятой в результате. По умолчанию равно 2
	 * @return boolean|float Время в секундах или false
	 */
	public static function timer(&$tick, $reset = true, $round = 2){
		$time = microtime(true);
		
		$result = isset($tick) ? $time - $tick : false;
		if ($result && $round !== false)
			$result = round($result, $round);
		
		if (!isset($tick) || $reset)
			$tick = $time;
		
		return $result;
	}
	
	/**
	 * Добавление к строке единицы измерения
	 * @param string $value Строка со значением
	 * @param boolean|string $unit Строка с единицей измерения. По умолчанию равно false
	 * @param integer $decimals Число знаков после запятой. По умолчанию равно 2
	 * @param boolean $fixed Признак фиксации числа знаков после запятой. По умолчанию равно false
	 * @param boolean $sub_unit Строка с дополнителем единицы измерения. По умолчанию равно false
	 * @param boolean $sup Признак отображения дополнителя сверху. По умолчанию равно true
	 * @return string Результат
	 */
	public static function unit($value, $unit = false, $decimals = 2, $fixed = false, $sub_unit = false, $sup = true){
		if (!mb_strlen((string)$value))
			return '';
		
		$value = number_format($value, $decimals, ',', ' ');
		
		if (!$fixed && mb_strpos($value, ',') !== false){
			while ($value && mb_substr($value, -1) === '0')
				$value = mb_substr($value, 0, -1);
			
			if (mb_substr($value, -1) === ',')
				$value = mb_substr($value, 0, -1);
		}
		
		if ($unit)
			$value .= ' '.$unit;
		
		if ($sub_unit)
			$value .= Html::tag($sup ? 'sup' : 'sub', array(), $sub_unit);
		
		return $value;
	}
	
	/**
	 * Формирование строки в временном формате
	 * @param boolean|integer|string $date Строка даты, штамп времени или false. По умолчанию равно false. Если равно false, то
	 * за основу возьмутся текущие дата и время
	 * @param string $format Формат даты. По умолчанию равно self::FORMAT_DATE_USER
	 * @return string Результат
	 */
	public static function date($date = false, $format = self::FORMAT_DATE_USER){
		if (!$date)
			$date = time();
		elseif (is_string($date)){
			$features = date_parse($date);
			$date = mktime($features['hour'], $features['minute'], $features['second'],
				$features['month'], $features['day'], $features['year']);
		}
		
		$date = date($format, $date);
		
		if (mb_strpos($format, 'F') !== false)
			$date = str_replace(array_keys(static::TRANSLATE_MONTH), array_values(static::TRANSLATE_MONTH), $date);
		
		return $date;
	}
	
	/**
	 * Перевод числа секунд в строку, содержащую количество часов, минут и секунд
	 * @param integer $secs Число секунд
	 * @param boolean $simple Признак формирования результата в простой форме (именительный падеж). По умолчанию равно
	 * false. Если равно false, результат формируется в родительном падеже
	 * @return string Результат
	 */
	public static function timeout($secs, $simple = false){
		if ($hours = floor($secs / 3600)){
			$end = mb_substr((string)$hours, -1);
			$timeout[] = $hours.' '.($end == '1' && $simple ? 'час' :
				(($simple && in_array($end, array('2', '3', '4'))) || (!$simple && $end == '1') ? 'часа' : 'часов'));
			$secs -= $hours * 3600;
		}
		
		if ($mins = floor($secs / 60)){
			$end = mb_substr((string)$mins, -1);
			$timeout[] = $mins.' '.($end == '1' && $simple ? 'минута' :
				(($simple && in_array($end, array('2', '3', '4'))) || (!$simple && $end == '1') ? 'минуты' : 'минут'));
			$secs -= $mins * 60;
		}
		
		if ($secs || empty($timeout)){
			$end = mb_substr((string)$secs, -1);
			$timeout[] = $secs.' '.($end == '1' && $simple ? 'секунда' :
				(($simple && in_array($end, array('2', '3', '4'))) || (!$simple && $end == '1') ? 'секунды' : 'секунд'));
		}
		
		return implode(' ', $timeout);
	}
	
	/**
	 * Обработка сжатого JS-файла
	 * @param string $fn_in Путь к исходному файлу
	 * @param string $fn_out Название файла с результатом. По умолчанию равно 'result.js' 
	 */
	public static function parseJS($fn_in, $fn_out = 'result.js'){
		$dir = Yii::getPathOfAlias(static::DIR_PARSE).DIRECTORY_SEPARATOR;
		
		if (!is_file($dir.$fn_in))
			Arrays::printPre('file not found', true);
		
		$text = file_get_contents($dir.$fn_in);
		$len = mb_strlen($text);
		$result = $prev = '';
		$border = false;
		$tab = 0;
		$borders = array(array('/*', '*/'), array("'"), array('"'));
		
		for ($count = 0; $count < $len; $count++){
			$char = $text[$count];
			
			if ($border === false && $char === '}'){
				$result .= "\r\n";
				$tab--;
				$result .= str_repeat("\t", $tab);
			}
			
			$result .= $char;
			
			if ($border === false){
				foreach ($borders as $key=>$values)
					if ((mb_strlen($values[0]) == 1 ? $char : $prev.$char) == $values[0])
						$border = $key;
			}elseif (($value = $borders[$border][count($borders[$border]) != 1 ? 1 : 0]) ==
				(mb_strlen($value) == 1 ? $char : $prev.$char))
					$border = false;
			
			if ($border === false)
				switch ($char){
					case "\n":
						$result .= str_repeat("\t", $tab);
						
						break;
					case '{':
						$result .= "\r\n";
						$tab++;
						$result .= str_repeat("\t", $tab);
						
						break;
					case '}':
						$result .= "\r\n";
						
						break;
					case ',':
						$result .= ' ';
						break;
				}
			elseif ($char === "\n")
				$result .= str_repeat("\t", $tab);
			
			$prev = $char;
		}
		
		Arrays::printPre(file_put_contents($dir.$fn_out, $result), true);
	}
	
	/**
	 * Обмен значений переменных
	 * @param mixed $var1 Переменная 1
	 * @param mixed $var2 Переменная 2
	 */
	public static function swap(&$var1, &$var2){
		$bufer = $var1;
		$var1 = $var2;
		$var2 = $bufer;
	}
	
	/**
	 * Формирование строки адреса
	 * @param array $data Ассоциативный массив с данными о местоположении
	 * @param boolean $premise Признак необходимости включения в адрес помещения. По умолчанию равно false
	 * @param boolean $postcode Признак необходимости включения в адрес индекса и региона. По умолчанию равно false
	 */
	public static function address($data, $premise = false, $postcode = false){
		$items = array();
		
		if ($postcode && ($address = Arrays::pop($data, 'address')))
			$items[] = $address;
		else{
			if ($locality = Arrays::pop($data, 'locality'))
				$items[] = $locality;
			
			if ($street = Arrays::pop($data, 'street'))
				$items[] = $street;
			
			if ($house = Arrays::pop($data, 'house'))
				$items[] = is_numeric(mb_substr($house, 0, 1)) ? 'д '.$house : $house;
		}
		
		if ($premise && ($premise = Arrays::pop($data, 'premise')))
			$items[] = is_numeric(mb_substr($premise, 0, 1)) ? 'кв '.$premise : $premise;
		
		return implode(', ', $items);
	}
	
	/**
	 * Форматирование строки размера файла
	 * @param float $value Размер файла в байтах
	 * @return string Отформатированная строка
	 */
	public static function fileSize($value){
		if ($value >= 1024 * 1024 * 1024)
			return self::unit($value / 1024 / 1024 / 1024, self::UNIT_GBYTE);
		elseif ($value >= 1024 * 1024)
			return self::unit($value / 1024 / 1024, self::UNIT_MBYTE);
		elseif ($value >= 1024)
			return self::unit($value / 1024, self::UNIT_KBYTE);
		else
			return self::unit($value, self::UNIT_BYTE);
	}
	
}
