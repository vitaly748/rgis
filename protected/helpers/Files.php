<?php
/**
 * Класс дополнительных функций для работы с файлами
 * @author v.zubov
 */
class Files{
	
	/**
	 * @var array Список допустимых MIME-типов
	 */
	const MIME_TYPES = array(
		'bmp'=>'image/bmp',
		'doc'=>'application/msword',
		'docx'=>'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'jpeg'=>'image/jpeg',
		'pdf'=>'application/pdf',
		'png'=>'image/png',
// 		'rar'=>'application/x-rar-compressed, application/octet-stream',
		'txt'=>'text/plain',
		'xls'=>'application/vnd.ms-excel',
		'xlsx'=>'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'zip'=>'application/x-zip-compressed'//application/zip, application/octet-stream, multipart/x-zip
	);
	
	/**
	 * @var string Параметр HTTP-запроса, задающий признак отмены процесса загрузки файлов
	 */
	const PARAM_DOWNLOAD_CANCEL = 'download_cancel';
	
	/**
	 * Получение уникального имени файла в рамках каталога
	 * @param boolean|string $dir Путь к каталогу или false. По умолчанию равно false. Если равно false, то используется каталог
	 * временных файлов по умолчанию
	 * @param boolean|string $name Название файла или false. По умолчанию равно false. Если равно false, то будет использован
	 * генератор случайных чисел. Если задано, то в случае, когда файл с таким именем в данном каталоге уже существует, к имени
	 * файла будет добавлен постфикс формата "(n)", где n - увеличивающееся от 1 положительное число
	 * @return string Уникальное название временного файла
	 */
	public static function getUniqueFileName($dir = false, $name = false){
		if (!$dir)
			$dir = Yii::app()->getRuntimePath();
		
		if ($name)
			$name = pathinfo($name);
		
		$count = 0;
		
		do{
			$file_name = $name ? $name['filename'].($count ? ' ('.$count.')' : '').'.'.$name['extension'] : (string)Numbers::uniqid();
			$path = $dir.DIRECTORY_SEPARATOR.$file_name;
			$count++;
		}while (is_file($path));
		
		return $file_name;
	}
	
	/**
	 * Очистка каталога от файлов
	 * @param string $path Путь к каталогу для очистки
	 * @param boolean $recursive Признак рекурсивной очистки подкаталогов. По умолчанию равно true
	 * @param boolean $remove_dir Признак удаления каталогов. По умолчанию равно false
	 */
	public static function clear($path, $recursive = true, $remove_dir = false){
		if (is_dir($path)){
			if (($handle = @opendir($path)) !== false){
				while (($file = readdir($handle)) !== false){
					if ($file === '.' || $file === '..' || mb_substr($file, 0, 4) == '.git')
						continue;
					
					$full_path = $path.DIRECTORY_SEPARATOR.$file;
					
					if (is_file($full_path))
						@unlink($full_path);
					elseif (is_dir($full_path) && $recursive)
						self::clear($full_path, $recursive, $remove_dir);
				}
				
				closedir($handle);
			}
			
			if ($remove_dir)
				@rmdir($path);
		}
	}
	
	/**
	 * Выгрузка файла пользователю
	 * @param string $path Путь к файлу
	 * @param string $name Название файла. По умолчанию равно false. Если равно false, название будет взято из пути к файлу
	 * @param boolean $embed Признак встраивания файла в содержимое страницы. По умолчанию равно false
	 * @return boolean Успешность выполнения
	 */
	public static function upload($path, $name = false, $embed = false){
		$params_fix = array('path'=>$path, 'name'=>$name);
		
		try{
			if (!is_file($path))
				throw new CHttpException(404, 'file not found');
			
			if ($embed || !$name){
				$path_info = pathinfo($path);
				
				if (!$embed)
					$name = Arrays::pop($path_info, 'basename');
				else
					$extension = Arrays::pop($path_info, 'extension');
			}
			
			header('Content-Type: application/force-download');
			header('Content-Type: application/octet-stream');
			header('Content-Type: application/download');
			header('Content-Transfer-Encoding: binary');
			
			if (!$embed)
				header('Content-Disposition: attachment; filename='.$name);
			elseif ($extension && ($mime_type = Arrays::pop($mt = self::MIME_TYPES, $extension)))
				header('Content-Type: '.$mime_type);
			
			readfile($path);
		}catch (CHttpException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('file_upload', $params_fix, $ok ? 'ok' : ($e->statusCode == 404 ? 'file_not_found' : 'file_upload'));
		
		return $ok;
	}
	
	/**
	 * Запись отладочной информации в файл
	 * @param string $text Отладочная информация
	 */
	public static function log($text){
		$path = Yii::getPathOfAlias('application.runtime').DIRECTORY_SEPARATOR.'log.txt';
		
		if (Html::getUserAddr() == Html::IP_LOCALHOST)
			file_put_contents($path, $text.PHP_EOL, FILE_APPEND);
	}
	
}
