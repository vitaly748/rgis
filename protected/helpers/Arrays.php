<?php
/**
 * Класс дополнительных функций для работы с массивами
 * @author v.zubov
 */
class Arrays{
	
	/**
	 * @var string Название параметра names функции mergeParams
	 */
	const MERGE_PARAMS_NAMES = 'names';
	
	/**
	 * @var string Название параметра inherit функции mergeParams
	 */
	const MERGE_PARAMS_INHERIT = 'inherit';
	
	/**
	 * @var string Название параметра suffix функции mergeParams
	 */
	const MERGE_PARAMS_SUFFIX = 'suffix';
	
	/**
	 * @var string Название параметра sources функции mergeParams
	 */
	const MERGE_PARAMS_SOURCES = 'sources';
	
	/**
	 * @var string Название параметра class функции mergeParams
	 */
	const MERGE_PARAMS_CLASS = 'class';
	
	/**
	 * @var string Название параметра this функции mergeParams
	 */
	const MERGE_PARAMS_THIS = 'this';
	
	/**
	 * @var string Название параметра rules функции mergeParams
	 */
	const MERGE_PARAMS_RULES = 'rules';
	
	/**
	 * @var string Название параметра union_def функции mergeParams
	 */
	const MERGE_PARAMS_UNIONDEF = 'union_def';
	
	/**
	 * @var string Название параметра prefix функции mergeParams
	 */
	const MERGE_PARAMS_PREFIX = 'prefix';
	
	/**
	 * @var string Название параметра model функции mergeParams
	 */
	const MERGE_PARAMS_MODEL = 'model';
	
	/**
	 * @var string Название параметра id функции mergeParams
	 */
	const MERGE_PARAMS_ID = 'id';
	
	/**
	 * @var integer Код режима объединения параметров по схеме "Приоритет у последнего" для
	 * функции mergeParams
	 */
	const MERGE_PARAMS_UNION_LAST = 0;
	
	/**
	 * @var integer Код режима объединения параметров по схеме "Приоритет у первого" для функции mergeParams
	 */
	const MERGE_PARAMS_UNION_FIRST = 1;
	
	/**
	 * @var integer Код режима объединения параметров по схеме "Слияние" для функции mergeParams. Применяется
	 * для строковых параметров, строки объединяются через пробел с удалением дубликатов
	 */
	const MERGE_PARAMS_UNION_SEPARATOR = 2;
	
	/**
	 * @var integer Код режима объединения параметров по схеме "Рекурсия" для функции mergeParams. Применяется
	 * для массивов-параметров.
	 */
	const MERGE_PARAMS_UNION_RECURSION = 3;
	
	/**
	 * @var integer Название константы в пользовательском классе, дополняющей значение MERGE_PARAMS_RULES_DEF для
	 * функции mergeParams
	 */
	const MERGE_PARAMS_RULES_NAME = 'MERGE_PARAMS_RULES';
	
	/**
	 * @var array Набор стандартных правил объединения параметров для функции mergeParams в виде
	 * {Название параметра}=>{Код схемы объединения}. Название параметра может быть составным в случае рекурсивной
	 * обработки параметров. Данный набор можно изменить (будет применена функция array_merge), подав на вход
	 * функции mergeParams параметр rules, или, объявив константу MERGE_PARAMS_RULES в своём классе и подав
	 * имя класса так же на вход функции. Составное название параметра может содержать один элемент '*' (звёздочка),
	 * предполагающий любой символьный элемент.
	 * Примеры составных названий:
	 * 	common.main.run,
	 * 	common.main.*,
	 * 	common.*.run.
	 * Но не *.main.run, что равноценно main.run
	 */
	const MERGE_PARAMS_RULES_DEF = array(
		'htmlOptions'=>self::MERGE_PARAMS_UNION_RECURSION,
		'class'=>self::MERGE_PARAMS_UNION_SEPARATOR
	);
	
	/**
	 * @var integer Название параметра delimiter функции push
	 */
	const PUSH_PARAMS_DELIMITER = 'delimiter';
	
	/**
	 * @var Код integer режима отображения границ "Без границ" для функции printPre
	 */
	const PRINT_PRE_BORDER_NONE = 0;
	
	/**
	 * @var integer Код режима отображения границ "Верхняя граница" для функции printPre
	 */
	const PRINT_PRE_BORDER_TOP = 1;
	
	/**
	 * @var integer Код режима отображения границ "Нижняя граница" для функции printPre
	 */
	const PRINT_PRE_BORDER_BOTTOM = 2;
	
	/**
	 * @var integer Код режима отображения границ "Обе границы" для функции printPre
	 */
	const PRINT_PRE_BORDER_BOTH = 3;
	
	/**
	 * @var integer Код режима работы функции focussing "Удаление неподходящих"
	 */
	const FOCUSSING_MODE_REMOVE = 0;
	
	/**
	 * @var integer Код режима работы функции focussing "Обнуление неподходящих"
	 */
	const FOCUSSING_MODE_FALSE = 1;
	
	/**
	 * @var integer Код режима работы функции focussing "Оставление неподходящих"
	 */
	const FOCUSSING_MODE_LEAVE = 2;
	
	/**
	 * @var string Название параметра "Название элемента" функции forming
	 */
	const FORMING_PARAMS_NAME = 'af_name';
	
	/**
	 * @var string Название параметра "Значение элемента" функции forming
	 */
	const FORMING_PARAMS_VALUE = 'af_value';
	
	/**
	 * @var string Название параметра "Включение элемента" функции forming
	 */
	const FORMING_PARAMS_ENABLED = 'af_enabled';
	
	/**
	 * Форматированный вывод информационного блока посредством функции print_r или var_dump. Если не задан ни один
	 * параметр, функция будет запущена в тестовом режиме: параметр data будет установлен в 'test', параметр die
	 * будет установлен в true
	 * @param boolean|mixed $data Информационный блок или false. По умолчанию равен false
	 * @param boolean $die Флаг завершения работы скрипта в конце вывода. По умолчанию равен false
	 * @param boolean $var_dump Флаг использования функции var_dump вместо print_r. По умолчанию равен false
	 * @param number $border Код режима отображения границ информационного блока: "Без границ", "Верхняя граница",
	 * "Нижняя граница", "Обе границы". По умолчанию используется режим "Без границ"
	 */
	public static function printPre($data = false, $die = false, $var_dump = false, $border = self::PRINT_PRE_BORDER_NONE){
		if (!func_num_args()){
			$data = 'test';
			$die = true;
		}
		
		if ($border === self::PRINT_PRE_BORDER_TOP || $border === self::PRINT_PRE_BORDER_BOTH)
			echo Html::tag('hr');
		
		echo Html::openTag('pre');
			
			if ($var_dump)
				var_dump($data);
			else
				print_r(self::preparationPrintR($data));
			
		echo Html::closeTag('pre');
		
		if ($border === self::PRINT_PRE_BORDER_BOTTOM || $border === self::PRINT_PRE_BORDER_BOTH)
			echo Html::tag('hr');
		
		if ($die)
			die();
	}
	
	/**
	 * Возвращение результата выполнения функции printPre
	 * @param boolean|mixed $data Информационный блок или false. По умолчанию равен false
	 * @param boolean $var_dump Флаг использования функции var_dump вместо print_r. По умолчанию равен false
	 * @param number $border Код режима отображения границ информационного блока
	 * @return string Результат выполнения функции printPre
	 */
	public static function printPreText($data = false, $var_dump = false, $border = self::PRINT_PRE_BORDER_NONE){
		ob_start();
			self::printPre($data, false, $var_dump, $border);
		return ob_get_clean();
	}
	
	/**
	 * Приведение информационного блока для вывода функцией print_r к удобочитаемому виду
	 * @param mixed $data Информационный блок
	 * @return mixed Модифицированный информационный блок
	 */
	public static function preparationPrintR($data){
		if (is_array($data))
			foreach ($data as $key=>$item)
				$data[$key] = self::preparationPrintRItem($item);
		elseif (is_object($data)){
			$data = clone $data;
			
			foreach (get_object_vars($data) as $key=>$item){
				try{
					$data->$key = self::preparationPrintRItem($item);
				}catch (CException $e){
				}
			}
		}
		else
			$data = self::preparationPrintRItem($data);
		
		return $data;
	}
	
	/**
	 * Приведение элемента информационного блока для вывода функцией print_r к удобочитаемому виду
	 * @param mixed $item Элемент информационного блока
	 * @return mixed Модифицированный элемент информационного блока
	 */
	public static function preparationPrintRItem($item){
		if (is_string($item))
			$item = "'$item'";
		elseif (is_bool($item))
			$item = $item ? 'true' : 'false';
		elseif (is_null($item))
			$item = 'null';
		elseif (is_array($item))
			$item = self::preparationPrintR($item);
		elseif (is_object($item))
			$item = self::preparationPrintR($item);
		
		return $item;
	}
	
	/**
	 * Формирование массива параметров
	 * @param array $params Ассоциатывный массив параметров. Имеет следующий вид:
	 * 	array(
	 * 		'names'=>array(),
	 * 		'inherit'=>false,
	 * 		'suffix'=>'',
	 * 		'sources'=>array(),
	 * 		'class'=>object,
	 * 		'this'=>false,
	 * 		'rules'=>array(),
	 * 		'union_def'=>MERGE_PARAMS_UNION_LAST,
	 * 		'prefix'=>'',
	 * 		'model'=>false,
	 * 		'id'=>false
	 * 	)
	 * @param array $params['names'] Список названий параметров. Может содержать так же вложенные массивы названий,
	 * в этом случае они будут заменены своими значениями посредством функции array_splice. Если параметр не
	 * задан или пуст, то будет сформирован автоматически, в соответствии с переданными массивами исходных
	 * параметров
	 * @param boolean $params['inherit] Флаг наследования параметров, применяется совместно с $params['sources'].
	 * При наследовании параметры, не входящие в список $params['names'], так же будут попадать в финальный массив
	 * параметров. По умолчанию равен false
	 * @param string $params['suffix'] Суффикс, используемый при поиске значения по умолчанию для параметра в
	 * классе, заданном параметром class
	 * @param array $params['sources'] Список массивов-источников параметров. Задаются в порядке увеличения
	 * приоритета
	 * @param object|string $params['class'] Экземпляр объекта класса или название класса, содержащего
	 * дополнительные параметры: MERGE_PARAMS_RULES и {NAME}_{SUFFIX}_DEF. Если дано название, то будет
	 * предпринята попытка создаания экземпляра класса
	 * @param boolean $params['this'] Флаг включения в набор массивов исходных параметров так же параметров,
	 * описанных в классе, заданном параметром class. По умолчанию равен false
	 * @param array $params['rules'] Набор дополнительных правил объединения параметров. Объединяется средствами
	 * функции array_merge с константой MERGE_PARAMS_RULES_DEF. Если задан параметр class, то объединение
	 * сначала происходит с константой MERGE_PARAMS_RULES из этого класса, если она задана
	 * @param integer $params['union_def'] Код схемы объединения параметров по умолчанию. По умолчанию равен
	 * MERGE_PARAMS_UNION_LAST
	 * @param string $params['prefix'] Префикс названия набора правил для данного блока параметров
	 * @param boolean $params['model'] Флаг применения функции BaseModel::modelInit к параметрам с именем, содержащим
	 * подстроку "model". Если задан пользовательский класс-потомок TreeWidget, то по умолчанию данный параметр
	 * равен true, если не задан - false
	 * @params boolean $params['id'] Флаг включения параметра id в список названий параметров. Если задан
	 * пользовательский класс-потомок TreeWidget, то по умолчанию данный параметр равен true и примыкает к группе
	 * limit. Если не задан, то по умолчанию параметр равен false
	 * @return array Финальный массив параметров. Если параметр $params[names] содержит группировки (вложенные
	 * массивы названий параметров), то финальный массив будет вложен в ассоциативный массив, содержащий результаты
	 * работы функции, разбитые по группам. Например:
	 * 	array(
	 * 		0=>array(),// Общий массив параметров
	 * 		'sly'=>array(),// Массив параметров группы sly
	 * 		'common'=>array(),// Массив параметров группы common
	 * 		...
	 * 	)
	 * Если параметр class является объектом-потомком класса TreeWidget, то результат примет вид, отвечающий
	 * требованиям данного класса:
	 * 	array(
	 * 		0=>array(),// Общий массив параметров
	 * 		'fix'=>array(),// Массив параметров группы fix
	 * 		'trans'=>array()// Массив параметров группы trans
	 * 	)
	 */
	public static function mergeParams($params){
		$suffix = isset($params[self::MERGE_PARAMS_SUFFIX]) ? $params[self::MERGE_PARAMS_SUFFIX] : '';
		$prefix = isset($params[self::MERGE_PARAMS_PREFIX]) ? $params[self::MERGE_PARAMS_PREFIX] : '';
		$this_params = isset($params[self::MERGE_PARAMS_THIS]) ? $params[self::MERGE_PARAMS_THIS] : false;
		$inherit = isset($params[self::MERGE_PARAMS_INHERIT]) ? $params[self::MERGE_PARAMS_INHERIT] : false;
		$union_def = isset($params[self::MERGE_PARAMS_UNIONDEF]) ? $params[self::MERGE_PARAMS_UNIONDEF] :
			self::MERGE_PARAMS_UNION_LAST;
		$rules_add = isset($params[self::MERGE_PARAMS_RULES]) ? $params[self::MERGE_PARAMS_RULES] : false;
		
		if (isset($params[self::MERGE_PARAMS_CLASS]) && !is_object($params[self::MERGE_PARAMS_CLASS]))
			BaseModel::modelInit($params[self::MERGE_PARAMS_CLASS]);
		
		$class = isset($params[self::MERGE_PARAMS_CLASS]) && is_object($params[self::MERGE_PARAMS_CLASS]) ?
			$params[self::MERGE_PARAMS_CLASS] : false;
		
		if ($class)
			$class_name = get_class($class);
		
		$class_tree = $class && $class instanceof TreeWidget;
		$id = isset($params[self::MERGE_PARAMS_ID]) ? $params[self::MERGE_PARAMS_ID] : $class_tree && !$prefix;
		$model_init = isset($params[self::MERGE_PARAMS_MODEL]) ? $params[self::MERGE_PARAMS_MODEL] : $class_tree;
		$names = isset($params[self::MERGE_PARAMS_NAMES]) && is_array($params[self::MERGE_PARAMS_NAMES]) ?
			$params[self::MERGE_PARAMS_NAMES] : array();
		$names_struct = $class_tree && !$prefix;
		
		if ($id){
			if (!$names_struct)
				$names_id_level = &$names;
			else{
				if (!isset($names[TreeWidget::PARAM_TYPE_LIMIT]))
					$names[TreeWidget::PARAM_TYPE_LIMIT] = array();
				elseif (!is_array($names[TreeWidget::PARAM_TYPE_LIMIT]))
					$names[TreeWidget::PARAM_TYPE_LIMIT] = array($names[TreeWidget::PARAM_TYPE_LIMIT]);
				
				$names_id_level = &$names[TreeWidget::PARAM_TYPE_LIMIT];
			}
			
			if (!in_array(self::MERGE_PARAMS_ID, $names_id_level))
				$names_id_level[] = self::MERGE_PARAMS_ID;
		}
		
		$names_init = !$names || $inherit;
		$sources = $names_types = array();
		
		if (isset($params[self::MERGE_PARAMS_SOURCES]) && is_array($params[self::MERGE_PARAMS_SOURCES])){
			foreach ($params[self::MERGE_PARAMS_SOURCES] as $source)
				if (is_array($source)){
					$sources[] = $source;
					
					if ($names_init)
						$names = array_merge($names, array_keys($source));
				}
		}
		
		$names_extension = self::extensionNestedParams($names);
		
		if ($groups = $names_extension[1]){
			$names = $names_extension[0];
			
			foreach ($groups as $key_group=>$group)
				$groups[$key_group] = array_flip($group);
			
			$names_types = $groups;
			$names_struct = true;
		}
		
		if ($names_init)
			$names = array_unique($names);
		
		if ($this_params && $class){
			$source = array();
			
			foreach ($names as $name)
				if ($name !== self::MERGE_PARAMS_ID && property_exists($class, $name))
					$source[$name] = $class->$name;
			
			array_unshift($sources, $source);
		}
		
		$sources_last = count($sources) - 1;
		$rules = self::MERGE_PARAMS_RULES_DEF;
		
		if ($class){
			$const = $class_name.'::'.self::MERGE_PARAMS_RULES_NAME;
			
			if (defined($const))
				$rules = array_merge($rules, constant($const));
		}
		
		if ($rules_add)
			$rules = array_merge($rules, $rules_add);
		
		$result = array();
		
		foreach ($names as $name){
			$prefix_rule_name = ($prefix ? $prefix.'.' : '').$name;
			$union = self::getUnion($prefix_rule_name, $rules, $union_def);
			$last = $union === self::MERGE_PARAMS_UNION_LAST;
			$stack = array();
			
			for ($count = $last ? $sources_last : 0; in_array($count, range(0, $sources_last)); $count += 1 - 2 * $last)
				if (isset($sources[$count][$name])){
					$value = $sources[$count][$name];
					
					if ($union === self::MERGE_PARAMS_UNION_RECURSION)
						$stack[] = $value;
					elseif ($union === self::MERGE_PARAMS_UNION_SEPARATOR)
						$stack = array_merge($stack, explode(' ', $value));
					else{
						$result[$name] = $value;
						break;
					}
				}
			
			if ($union === self::MERGE_PARAMS_UNION_RECURSION){
				if ($stack){
					if (count($stack) === 1)
						$result[$name] = $stack[0];
					else
						$result[$name] = call_user_func_array('self::'.__FUNCTION__, array(array(
							self::MERGE_PARAMS_SUFFIX=>$suffix,
							self::MERGE_PARAMS_SOURCES=>$stack,
							self::MERGE_PARAMS_CLASS=>$class,
							self::MERGE_PARAMS_RULES=>$rules_add,
							self::MERGE_PARAMS_PREFIX=>$prefix_rule_name
						)));
				}
			}elseif ($union === self::MERGE_PARAMS_UNION_SEPARATOR)
				$result[$name] = implode(' ', array_unique($stack));
			
			if (!isset($result[$name])){
				$result[$name] = null;
				
				if ($class){
					$const = $class_name.'::'.mb_strtoupper(Strings::nameModify($name.ucfirst($suffix).'Def',
						Strings::FORMAT_NAME_MODIFY_LOWER));
					
					if (defined($const))
						$result[$name] = constant($const);
				}
			}
			
			if ($model_init && mb_stripos($name, 'model') !== false)
				BaseModel::modelInit($result[$name]);
			
			if ($names_struct){
				$set = 0;
				
				foreach ($names_types as $key_nt=>$names_type)
					if (isset($names_type[$name])){
						$names_types[$key_nt][$name] = $result[$name];
						$set = 1;
					}
				
				if ($class_tree && !$set)
					$names_types[TreeWidget::PARAM_TYPE_TRANS][$name] = $result[$name];
			}
		}
		
		if ($names_struct){
			if (!$class_tree)
				$result_addition = $names_types;
			else{
				$trans = isset($names_types[TreeWidget::PARAM_TYPE_TRANS]) ?
					$names_types[TreeWidget::PARAM_TYPE_TRANS] : array();
				$limit = isset($names_types[TreeWidget::PARAM_TYPE_LIMIT]) ?
					$names_types[TreeWidget::PARAM_TYPE_LIMIT] : array();
				$fix = isset($names_types[TreeWidget::PARAM_TYPE_FIX]) ?
					$names_types[TreeWidget::PARAM_TYPE_FIX] : array();
				$result_addition = array(
					TreeWidget::PARAM_TYPE_FIX=>array_merge($limit, $fix),
					TreeWidget::PARAM_TYPE_TRANS=>array_merge($trans, $fix)
				);
			}
		}
		
		return !isset($result_addition) ? $result : array_merge(array($result), $result_addition);
	}
	
	/**
	 * Расширение параметров. Элементы исходного массива со строковым ключом в формате "name1, name2[, ...]"
	 * будут заменены на массив, содержащий продублированные для каждого слова ключа значения данного элемента.
	 * Пример:
	 * 	array('name1'=>value1, 'name2, name3'=>value2, 'name4'=>value4) ->
	 * 	array('name1'=>value1, 'name2'=>value2, 'name3'=>value2, 'name4'=>value4)
	 * Для элементов с одинаковыми строковыми ключами приоритет будут у последнего. В случае если данные элементы являются
	 * массивами, будет применена функция array_merge
	 * @param array $params Исходный массив
	 * @param array|boolean $names Список всех возможных названий элементов массива. По умолчанию равно false. Если задан, то
	 * можно задавать параметры всех элементов массива, описав только один элемент с ключом "*". При этом, если нужно исключить
	 * из общего набора некоторые элементы, то в ключе нужно указать названия исключаемых после "*" через запятую.
	 * Например, "*, name5, name6".
	 * @param boolean|string $wrap Признак необходимости обёртывания значений элементов исходного массива, не являющиеся массивом,
	 * в массив. По умолчанию равно false. Если задан строкой, то в случае обёртывания значение будет вставлено в масси с данным
	 * символьным ключом
	 * @param string $delimiter Разделитель названий параметров в составном ключе. По умолчанию равно ","
	 * @return array|boolean Расширенный массив или false
	 */
	public static function extensionParams($params, $names = false, $wrap = false, $delimiter = ','){
		if (!is_array($params))
			return false;
		
		$names = is_array($names) && self::getGeneralType($names) === 'string' ? array_values($names) : false;
		$params_res = $params_double = array();
		
		foreach ($params as $key=>$item){
			if ($wrap && !is_array($item))
				$item = is_string($wrap) ? array($wrap=>$item) : array($item);
			
			$group = false;
			
			if (is_string($key) && ($key === '*' || mb_strpos($key, $delimiter) !== false)){
				$keys = Strings::devisionWords($key, false, $delimiter);
				
				if ($names && (($pos_group = array_search('*', $keys, true)) !== false)){
					unset($keys[$pos_group]);
					
					$group = true;
					$keys = $keys ? array_diff($names, $keys) : $names;
				}
			}else
				$keys = array($key);
			
			if ($keys)
				foreach ($keys as $name)
					if (!isset($params_res[$name]) || !is_array($params_res[$name]) || !is_array($item))
						$params_res[$name] = $item;
					elseif ($group)
						$params_res[$name] = array_merge($item, $params_res[$name]);
					else
						$params_double[] = array($name, $item);
		}
		
		if ($params_double)
			foreach ($params_double as $param_double){
				list($name, $item) = $param_double;
				$params_res[$name] = array_merge($params_res[$name], $item);
			}
		
		return $params_res;
	}
	
	/**
	 * Расширение вложенных параметров массива параметров. Элементы исходного массива со строковым ключом (группы)
	 * будут заменены на массив, содержащий значения данного элемента. Примеры:
	 * 	array(1, 'group1'=>array(5), 2) -> array(1, 5, 2)
	 * 	array(1, 'group1'=>5, 2) -> array(1, 5, 2)
	 *  array(1, 'group1'=>array(5, 6), 2) -> array(1, 5, 6, 2)
	 * При этом копии групп так же будут возвращены списком
	 * @param array $params Исходный массив
	 * @return array|boolean Расширенный массив или false. Формат:
	 * 	array(
	 * 		{Сжатый массив},
	 * 		{Массив групп}
	 * 	)
	 * Например:
	 * 	array(1, 'group1'=>array(5, 6), 2) -> array(array(1, 5, 6, 2), array('group1'=>array(5, 6)))
	 */
	public static function extensionNestedParams($params){
		if (!is_array($params))
			return false;
		
		$groups = array();
		
		foreach ($params as $group=>$items)
			if (is_string($group)){
				if (!is_array($items))
					$items = array($items);
				
				array_splice($params, array_search($group, array_keys($params), true), 1, $items);
				
				$groups[$group] = $items;
			}
		return array($params, $groups);
	}
	
	/**
	 * Определение схемы объединения параметров
	 * @param string $name Составное имя параметра. Например, "common.parma.htmlOptions"
	 * @param array $rules Список правил
	 * @param integer $union_def Код схемы объединения по умолчанию. По умолчанию равно MERGE_PARAMS_UNION_LAST
	 * @return integer Код схемы объединения параметров или false
	 */
	public static function getUnion($name, $rules, $union_def = self::MERGE_PARAMS_UNION_LAST){
		if (is_array($rules))
			if ($rule = Strings::checkKeyPattern($name, array_keys($rules)))
				return $rules[$rule];
		
		return $union_def;
	}
	
	/**
	 * Получение элемента массива с заданным ключом (ключами)
	 * @param array $source Исходный массив
	 * @param array|integer|string $key Числовой или символьный ключ массива или список ключей. Если нужно получить несколько
	 * элементов за раз, то можно указать их ключи в массиве или в строке с разделителем $delimiter_keys. Для многомерных массивов
	 * можно задавать составные ключи с разделителем $delimiter_levels
	 * @param boolean $remove_item Флаг необходимости удаления найденного элемента из массива. По умолчанию равно false
	 * @param boolean $preserve_keys Флаг необходимости сохранения ключей в случае мультипоиска. По умолчанию равно false. Ключи
	 * сохраняются в той же форме, в какой были заданы
	 * @param boolean|string $delimiter_keys Разделитель ключей элементов в строке. По умолчанию равно ","
	 * @param boolean|string $delimiter_levels Разделитель ключей уровней в составном ключе. По умолчанию равно "."
	 * @return mixed Элемент массива с заданным ключом или null, если элемент не найден. Если был задан список ключей, то
	 * элементы будут возвращены в ассоциативном массиве с соответствующими ключами
	 */
	public static function pop(&$source, $key, $remove_item = false, $preserve_keys = false,
		$delimiter_keys = ',', $delimiter_levels = '.'){
			if (is_array($source)){
				$is_array = is_array($key);
				$multi = $is_array || ($delimiter_keys && is_string($key) && mb_strpos($key, $delimiter_keys) != false);
				$keys = $is_array ? $key : ($multi ? Strings::devisionWords($key, true, $delimiter_keys) : array($key));
				
				foreach ($keys as $key){
					$cur = &$source;
					$words = $delimiter_levels && is_string($key) && mb_strpos($key, $delimiter_levels) != false ?
						explode($delimiter_levels, $key) : array($key);
					
					while ($words){
						$word = array_shift($words);
						
						if (array_key_exists($word, $cur))
							if (!$words){
								if ($preserve_keys)
									$result[$key] = $cur[$word];
								else
									$result[] = $cur[$word];
								
								if ($remove_item)
									unset($cur[$word]);
							}else
								$cur = &$cur[$word];
						else{
							if ($preserve_keys)
								$result[$key] = null;
							else
								$result[] = null;
							
							break;
						}
					}
				}
			}
			
			return empty($result) ? null : ($multi ? $result : reset($result));
		}
	
	/**
	 * Вставка элемента (элементов) в массив с заданным ключом
	 * @param array $source Исходный массив
	 * @param [array|integer|string] Числовой или символьный ключ массива или список ключей. Если нужно задать несколько
	 * ключей элемента за раз, то можно указать эти ключи в массиве или в строке с разделителем ",". Для многомерных массивов
	 * можно задавать составные ключи с разделителем "." При этом в местах, где нужно использовать стандартный автоинкрементный
	 * индекс, нужно использовать пустую строку. Например:
	 * 	'module.section.value',
	 * 	'module.section.',
	 * 	'module..value',
	 * 	'module.x, module2.x',
	 * 	array('module.x', 'module2.x').
	 * Для переопределения разделителя составных ключей, нужно последним параметром указать его значение. Если нужно переопределить
	 * оба разделителя, то последний параметр нужно задать в формате array({Разделитель ключей}, {Разделитель составных ключей}).
	 * Допускается задавать параметры вставляемых элементов в виде ассоциативного массива, подав на вход только его, сразу после
	 * параметра $source. В этом случае для переопределения разделителей будет использоваться элемент
	 * с ключом self::PUSH_PARAMS_DELIMITER
	 * @param [mixed] Значение вставляемого элемента
	 * @return boolean Успешность выполнения
	 */
	public static function push(&$source){
		$args = array_slice(func_get_args(), 1);
		$qu = count($args);
		$delimiter_keys = ',';
		$delimiter_levels = '.';
		
		if ($qu == 1 && is_array($args[0])){
			$items = $args[0];
			$delimiter = self::pop($items, self::PUSH_PARAMS_DELIMITER, true);
		}else{
			if ($qu % 2){
				$delimiter = array_pop($args);
				$qu--;
			}
			
			if (!$qu)
				return false;
			
			for ($count = 0; $count < $qu; $count += 2){
				list($key, $value) = array_slice($args, $count, 2);
				$items[$key === false ? '' : $key] = $value;
			}
		}
		
		if (!empty($delimiter))
			if (is_array($delimiter)){
				if (isset($delimiter[0]))
					$delimiter_keys = $delimiter[0];
				
				if (isset($delimiter[1]))
					$delimiter_levels = $delimiter[1];
			}else
				$delimiter_levels = $delimiter;
		
		foreach ($items as $keys=>$value){
			if (!is_array($keys))
				$keys = $delimiter_keys && is_string($keys) && mb_strpos($keys, $delimiter_keys) != false ?
					Strings::devisionWords($keys, true, $delimiter_keys) : array($keys);
			
			foreach ($keys as $words){
				if (!is_array($words))
					$words = $delimiter_levels && is_string($words) && mb_strpos($words, $delimiter_levels) != false ?
						Strings::devisionWords($words, true, $delimiter_levels) : array($words);
				
				$cur = &$source;
				
				while ($words){
					$word = array_shift($words);
					$def = $word === false || $word === '';
					
					if (!$words){
						if ($def)
							$cur[] = $value;
						else
							$cur[$word] = $value;
					}else{
						if ($def){
							$cur[] = array();
							$cur = &$cur[end(array_keys($cur))];
						}else{
							if (!isset($cur[$word]) || !is_array($cur[$word]))
								$cur[$word] = array();
							
							$cur = &$cur[$word];
						}
					}
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Смена статуса
	 * @param array $states Список сивмольных идентификаторов статусов
	 * @param boolean|string $state Символьный идентификатор текущего статуса или false. По умолчанию равно false.
	 * Если равно false, то расчёт будет вестись относительно первого статуса
	 * @param integer $step Шаг изменения статуса. По умолчанию равно 1
	 * @param boolean $case_sensitive Признак регистрозависимого поиска текущего статуса. По умолчанию равно false
	 * @return boolean|string Символьный идентификатор нового статуса или false в случае ошибки
	 */
	public static function changeState($states, $state = false, $step = 1, $case_sensitive = false){
		if (!$states || !is_array($states))
			return false;
		
		$states = array_values($states);
		
		if ($state === false)
			$state = reset($states);
		
		if (!$case_sensitive)
			$state = mb_strtolower($state);
		
		$pos = array_search($state, $states, true);
		
		if ($pos === false)
			return false;
		
		$pos += $step;
		
		return isset($states[$pos]) ? $states[$pos] : false;
	}
	
	/**
	 * Объединение двух или более массивов. Действие аналогично функции array_merge, за тем
	 * исключением, что объединению будут подвергнуты также элементы с числовыми ключами
	 * @param [array] Список массивов
	 * @param [boolean] Признак необходимости рекурсивной обработки, в этом случае будет применена функция array_merge_recursive.
	 * По умолчанию равно false
	 * @return array|boolean Объединённый массив
	 */
	public static function merge(){
		if (!$args = func_get_args())
			return false;
		
		$key_last = count($args) - 1;
		
		if (!is_array($args[$key_last])){
			$recursive = self::pop($args, $key_last, true);
			
			if (!$args)
				return false;
		}
		
		$arrays = array();
		
		foreach ($args as $arg){
			$array = array();
			
			foreach ($arg as $key=>$val)
				$array[is_int($key) ? 'key_'.$key : $key] = $val;
			
			$arrays[] = $array;
		}
		
		$arrays = call_user_func_array(empty($recursive) ? 'array_merge' : 'array_merge_recursive', $arrays);
		
		$array = array();
		
		foreach ($arrays as $key=>$val){
			if (mb_strpos($key, 'key_') === 0){
				$number = mb_substr($key, 4);
				
				if (is_numeric($number)){
					$array[$number] = $val;
					continue;
				}
			}
			
			$array[$key] = $val;
		}
		
		return $array;
	}
	
	/**
	 * Облицовка массива
	 * @param array $source Исходный массив
	 * @param mixed $key_main Ключ главного элемента в ряду
	 * @param boolean $remove_empty Признак необходимости удаления элементов без главного ключа
	 * @return array|boolean Результат. Например, при "key_main" равном "key2" результат будет следующий:
	 * 	array(
	 * 		array(
	 * 			'key1'=>'value1',
	 * 			'key2'=>'value2',
	 * 			'key3'=>'value3'
	 * 		),
	 * 		array(
	 * 			'key2'=>'value22',
	 * 			'key3'=>'value33',
	 * 			'key4'=>'value44'
	 * 		)
	 * 	)
	 * 	=>
	 *	array(
	 * 		'value2'=>array(
	 * 			'key1'=>'value1',
	 * 			'key2'=>'value2',
	 * 			'key3'=>'value3'
	 * 		),
	 * 		'value22'=>array(
	 * 			'key2'=>'value22',
	 * 			'key3'=>'value33',
	 * 			'key4'=>'value44'
	 * 		)
	 * 	)
	 */
	public static function facing($source, $key_main, $remove_empty = false){
		if (!is_array($source))
			return false;
		
		$result = array();
		
		foreach ($source as $key=>$features){
			$value_main = self::pop($features, $key_main);
			if (isset($value_main) || !$remove_empty)
				$result[isset($value_main) ? $value_main : $key] = $features;
		}
		
		return $result;
	}
	
	/**
	 * Фокусировка многоуровневого массива
	 * @param array $source Исходный массив
	 * @param mixed $key_main Ключ главного элемента второго уровня. По умолчанию равно "description". Элементом
	 * второго уровня может быть объект
	 * @param integer $mode Режим работы функции. По умолчанию равно "Оставление неподходящих"
	 * @return array|boolean Результат. Пример:
	 * 	array(
	 * 		'developer'=>array(
	 * 			'code'=>10,
	 * 			'description'=>'Разработчик',
	 * 			'state'=>20
	 * 		),
	 * 		'citizen'=>array(
	 * 			'code'=>20,
	 * 			'description'=>'Гражданин',
	 * 			'state'=>20
	 * 		)
	 * 	)
	 * 	=>
	 * 	array(
	 * 		'developer'=>'Разработчик',
	 * 		'citizen'=>'Гражданин'
	 * 	)
	 */
	public static function focussing($source, $key_main = 'description', $mode = self::FOCUSSING_MODE_LEAVE){
		if (!is_array($source))
			return false;
		
		$result = array();
		
		foreach ($source as $key=>$value){
			$is_array = is_array($value);
			$is_object = is_object($value);
			
			if (($is_array && array_key_exists($key_main, $value)) ||
				($is_object && in_array($key_main, array_keys(get_object_vars($value)))))
					$result[$key] = $is_array ? $value[$key_main] : $value->$key_main;
				elseif ($mode !== self::FOCUSSING_MODE_REMOVE)
					$result[$key] = $mode == self::FOCUSSING_MODE_FALSE ? false : $value;
		}
		
		return $result;
	}
	
	/**
	 * Сортировка многомерных массивов. Сортировка происходит на втором уровне по заданному ключу
	 * @param array $source Исходный массив
	 * @param string $param Название параметра на втором уровне, по которому будет происходить сортировка
	 * @param boolean $desc Признак сортировки по убыванию. По умолчанию равно false
	 * @return boolean Признак перестановки элементов
	 */
	public static function sort(&$source, $param, $desc = false){
		$source_focus = $source_sort = self::focussing($source_data = $source, $param, self::FOCUSSING_MODE_FALSE);
		$source = array();
		
		if (!$desc)
			sort($source_sort);
		else
			rsort($source_sort);
		
		foreach ($source_sort as $value_sort)
			foreach ($source_focus as $key=>$value)
				if ($value === $value_sort){
					$source[$key] = $source_data[$key];
					unset($source_focus[$key]);
					break;
				}
		
		return $source !== $source_data;
	}
	
	/**
	 * Вычисляет схождение массивов. Работает аналогично стандартной функции array_intersect, за тем исключением, что будет
	 * применена дополнительная проверка типа элементов
	 * @param [array] Список массивов
	 * @param [boolean] Признак регистрозависимого поиска. По умолчанию равное true
	 * @return array|boolean Результат или false
	 */
	public static function intersectStrict(){
		if (!$args = func_get_args())
			return false;
		
		$key_last = count($args) - 1;
		$strict = true;
		
		if (!is_array($args[$key_last])){
			$strict = self::pop($args, $key_last, true);
			
			if (!$args)
				return false;
		}
		
		$source = self::pop($args, 0, true);
		
		if (!is_array($source))
			return false;
		
		if (!$args)
			return $source;
		
		$args = call_user_func_array('array_merge', $args);
		$result = array();
		
		foreach ($source as $item)
			if (in_array($item, $args, $strict))
				$result[] = $item;
		
		return $result;
	}
	
	/**
	 * Определение общего типа элементов массива
	 * @param array $source Исходный массив
	 * @return boolean|string Название общего типа (array|boolean|double|integer|NULL|object|string) массива или false
	 */
	public static function getGeneralType($source){
		if (!$source || !is_array($source))
			return false;
		
		foreach ($source as $item){
			$type = gettype($item);
			
			if (!isset($general_type))
				$general_type = $type;
			elseif ($general_type !== $type)
				return false;
		}
		
		return $general_type;
	}
	
	/**
	 * Получение списка типов элементов массива
	 * @param array $source Исходный массив
	 * @return array|boolean Список названий или false
	 */
	/* public static function types($source){
		if (!is_array($source))
			return false;
		
		foreach ($source as $item)
			$types[] = gettype($item);
		
		return isset($types) ? $types : false;
	} */
	
	/**
	 * Проверка массива на признак ассоциативной матрицы. Ассоциативная матрица - двухмерный массив, на втором уровне которого
	 * находятся массивы с одним и тем же набором строковых ключей и соответствующих им типов значений
	 * @param array $source Исходный массив
	 * @return boolean Признак ассоциативного матрицы
	 */
	/* public static function checkAssociativeMatrix($source){
		if (!is_array($source))
			return false;
		
		foreach ($source as $rec){
			if (!is_array($rec) || self::getGeneralType(array_keys($rec)) !== 'string')
				return false;
			
			$params_rec = array(array_keys($rec), self::types($rec));
			
			if (!isset($params)){
				$params = $params_rec;
			}elseif ($params !== $params_rec)
				return false;
		}
		
		return true;
	} */
	
	/**
	 * Выборка и получение данных из ассоциативной матрицы параметров
	 * @param array $source Ассоциативная матрица. Можно обёртывать все параметры функции в ассоциативный массив и подавать
	 * вместо первого параметра. При этом сам параметр $source, можно задавать с нулевым индексом
	 * @param mixed $param1 Первый параметр или false. По умолчанию равно false. Может задавать как название, так и
	 * значение параметра. Для указания полного набора названий параметров, можно использовать символ "*"
	 * @param mixed $param2 Второй параметр или false. По умолчанию равно false. Может задавать как название, так и
	 * значение параметра
	 * @param mixed $except Список значений исключяемых записей или false. По умолчанию равно false
	 * @param mixed $only Список значений необходимых записей или false. По умолчанию равно false
	 * @param boolean|string $name_conditional Название условного параметра или false. По умолчанию равно false
	 * @param boolean $strict Признак регистрозависимого поиска. По умолчанию равно true
	 * @param boolean $unique Признак уникальных записей в матрице. По умолчанию равно true
	 * @return mixed Выбранные данные справочника или false
	 * Возможны следующие комбинации параметров param1 и param2:
	 * 	param1				param2				Результат
	 *  -------------------------------------
	 * 	false					false					Данные всей матрицы с учётом фильтров $except и $only
	 * 	value					false					Данные всей записи, в которой найдено значение $param1, или false. Если параметр $unique равен
	 * 															false, то будут возвращены все записи, в которых будет найдено данное значение, или array()
	 * 	value,..			false					Данные всей записи, в которой найдено хоть одно значение $param1, или false. Если параметр
	 * 															$unique равен false, то будут возвращены все записи, в которых будет найдено хоть одно из
	 * 															данных значений, или array()
	 * 	feature				false					Список всех значений характеристики $param1 с учётом фильтров $except и $only
	 * 	feature,..		false					Список ассоциативных массивов значений характеристик $param1 с учётом фильтров $except и $only
	 * 	feature				value					Значение характеристики $param1 записи, в котором найдётся значение $param2, или false. Если
	 * 															параметр $unique равен false, то будут возвращены характеристики $param1 всех записей, в
	 * 															которых будет найдено данное значение, или array()
	 * 	feature,..		value,..			Ассоциативный массив значений характеристик $param1 записи, в которой найдётся хоть одно
	 * 															значение $param2, или false. Если параметр $unique равен false, то будет возвращён
	 * 															список ассоциативных массивов значений характеристик $param1 из записей, в которых найдётся
	 * 															хоть одно значение $param2
	 * 	feature				feature				Ассоциативный массив в формате {Значение характеристики param2}=>{Значение характеристики
	 * 															param1} с учётом фильтров $except и $only
	 * 	feature,..		feature				Ассоциативный массив ассоциативных массивов значений характеристик $param1 в формате
	 * 															{Значение характеристики param2}=>{Ассоциативный массив значений характеристик $param1} с
	 * 															учётом фильтров $except и $only
	 */
	public static function select($source, $param1 = false, $param2 = false, $except = false, $only = false,
		$name_conditional = false, $strict = true, $unique = true){
			if (func_num_args() == 1 && ($args = self::filterKey($source,
				array('source', 'param1', 'param2', 'except', 'only', 'name_conditional', 'strict', 'unique')))){
					foreach ($args as $key=>$value)
						$$key = $value;
					
					if (!isset($args['source']))
						$source = self::pop($source, 0);
				}
			
			if (!$source || !is_array($source))
				return false;
			
			if (!$param1 && !$except && !$only)
				return $source;
			
			$features = array_keys(reset($source));
			
			if ($param1 === '*')
				$param1 = $features;
			
			foreach (array('param1', 'param2', 'except', 'only') as $name)
				if ($$name !== false)
					if (is_string($$name))
						$$name = Strings::devisionWords($$name);
					elseif (!is_array($$name))
						$$name = array($$name);
			
			if ($feature1 = (boolean)($value = self::intersectStrict($param1, $features)))
				$param1 = $value;
			else
				$param2 = false;
			
			if ($feature2 = $param2 !== false && ($value = self::intersectStrict($param2, $features)))
				$param2 = $value;
			
			if ($name_conditional && !in_array($name_conditional, $features, true))
				$name_conditional = false;
			
			if ($specific = ($param1 && !$feature1 && $param2 !== false) || ($param2 !== false && !$feature2)){
				$only = $param2 !== false ? $param2 : $param1;
				$except = false;
				$specific = $unique;
			}
			
			if ($feature1 && count($param1) == 1)
				$param1 = reset($param1);
			
			if ($feature2)
				$param2 = reset($param2);
			
			foreach ($source as $rec){
				if ($only !== false || $except !== false)
					if ($name_conditional){
						if (!isset($rec[$name_conditional]) || ($except !== false && in_array($rec[$name_conditional], $except, $strict)) ||
							($only !== false && !in_array($rec[$name_conditional], $only, $strict)))
								continue;
					}elseif (($except !== false && self::intersectStrict($rec, $except, $strict)) ||
						($only !== false && !self::intersectStrict($rec, $only, $strict)))
							continue;
				
				$value = !$feature1 ? $rec : self::pop($rec, $param1, false, true);
				
				if ($specific)
					return $value;
				
				if ($feature2 && ($key = self::pop($rec, $param2)))
					$result[$key] = $value;
				else
					$result[] = $value;
			}
			
			return isset($result) ? $result : ($specific ? false : array());
		}
	
	/**
	 * Выборочное получение данных
	 * @param array $data Ассоциативный массив исходной структуры данных
	 * @param array $args Список ключей для фильтрации очередного уровня массива справочных данных или false. Если данный ключ не
	 * будет найден, возвратится false. Фильтрация прекратится, когда ключом будет задан false. Если после ключа, равного false,
	 * будет передан ещё один, последний, ключ, не равный false, то будет предпринята попытка формирования ассоциативного массива,
	 * значениями которого будут элементы вложенных массивов текущего уровня данных с ключами, равными этому последнему ключу, а
	 * ключами - ключи вложенных массивов
	 * @return array|boolean Выбранные данные или false
	 */
	public static function getSelective($data, $args){
		if (!$data)
			return false;
		
		if (!$args)
			return $data;
		
		if ($args[0] instanceof BaseModel)
			$args[0] = $args[0]->modelName;
		
		while (list($key, $arg) = each($args)){
			if ($arg === false)
				if ($data && ($feature = current($args)) !== false && !next($args)){
					foreach ($data as $key=>$value)
						$result[$key] = !is_array($value) ? $value : (isset($value[$feature]) ? $value[$feature] : false);
					
					return $result;
				}else
					break;
			
			$data = Arrays::pop($data, $arg);
			
			if ($data === null)
				return false;
			
			if (!is_array($data))
				break;
		}
		
		return $data;
	}
	
	/**
	 * Динамическое формирование массива
	 * @param array $source Исходный массив с параметрами формируемого массива. Каждый элемент массива несёт в себе информацию
	 * о названии, значении и включении элемента или элементов формируемого массива. Пример:
	 * 	array(//										Стандартная обработка						Альтернативная обработка
	 * 				//								Ключ			Значение	Включение			Ключ			Значение	Включение
	 * 		5,// 									false			5					5							false			0					5
	 * 		2=>'rele',//					false			'rele'		'rele'				false			2					'rele'
	 * 		'rele'=>10,//					'rele'		10				10						false			'rele'		10
	 * 		'a1, a2'=>'s',//			a1 и a2		's'				's'						false			a1 и a2		's'
	 * 		'fe:10'=>false,//			fe				10				false					fe				10				false
	 * 		'gera:10'=>array(//		ge				20				'che'					ge				20				'che'
	 * 			'af_name'=>'ge',
	 * 			'af_value'=>20,
	 * 			'af_enabled'=>'che'
	 * 		),
	 * 		...
	 * 	)
	 * @param boolean $alt Признак применения альтернативной обработки исходного массива параметров. По умолчанию равно false
	 * @param array|boolean $names Список всех возможных названий элементов массива. Подаётся на вход функции extensionParams.
	 * По умолчанию равно false
	 * @param boolean|string $delimiter_names Разделитель названий элементов в составном ключе. По умолчанию равно ","
	 * @param boolean|string $delimiter_values Разделитель названий и значений элементов в составном ключе. По умолчанию равно ":"
	 * @param boolean|string $delimiter_levels Разделитель названий уровней в многоуровневом ключе. По умолчанию равно "."
	 * @return array Сформированный массив или false в случае ошибки
	 */
	public static function forming($source, $alt = false, $names = false, $delimiter_names = ',', $delimiter_values = ':',
		$delimiter_levels = '.'){
			if (!is_array($source))
				return false;
			
			if ($delimiter_names)
				$source = self::extensionParams($source, $names, false, $delimiter_names);
			
			$result = array();
			
			foreach ($source as $key=>$params){
				$name = false;
				$value = $enabled = $params;
				
				if ($alt)
					$value = $key;
				
				if (is_string($key))
					if ($delimiter_values && mb_strpos($key, $delimiter_values) !== false)
						list($name, $value) = explode($delimiter_values, $key);
					elseif (!$alt)
						$name = $key;
				
				if (is_array($params) && $params && array_intersect(array_keys($params),
					array(self::FORMING_PARAMS_NAME, self::FORMING_PARAMS_VALUE, self::FORMING_PARAMS_ENABLED))){
						if (array_key_exists(self::FORMING_PARAMS_NAME, $params))
							$name = $params[self::FORMING_PARAMS_NAME];
						
						if (array_key_exists(self::FORMING_PARAMS_VALUE, $params))
							$value = $enabled = $params[self::FORMING_PARAMS_VALUE];
						
						if (array_key_exists(self::FORMING_PARAMS_ENABLED, $params))
							$enabled = $params[self::FORMING_PARAMS_ENABLED];
					}
				
				if ($enabled)
					self::push($result, !$name ? false : ($delimiter_levels && mb_strpos($name, $delimiter_levels) !== false ?
						explode($delimiter_levels, $name) : $name), $value);
			}
			
			return $result;
		}
	
	/**
	 * Фильтрация массива по ключу
	 * @param array $source Исходный массив
	 * @param mixed $keys Список фильтруемых ключей. Может задаваться списком, в строке через запятую или отдельным ключом
	 * @param boolean $only Признак работы в режиме "Только обязательные". По умолчанию равно true. В режиме "Только обязательные"
	 * в массиве останутся только элементы с ключами из списка $keys. Если равен false, то работа будет происходить в режиме
	 * "Исключения", в котором из массива будут удалены все элементы с ключами из списка $keys
	 * @param boolean $strict Признак строгой проверки ключей. По умолчанию равно true
	 * @result array|boolean Отфильтрованный массив или false
	 */
	public static function filterKey($source, $keys, $only = true, $strict = true){
		if (!is_array($source))
			return false;
		
		if (!$source || $keys === false)
			return $source;
		elseif (is_string($keys))
			$keys = Strings::devisionWords($keys);
		elseif (!is_array($keys))
			$keys = array($keys);
		
		foreach ($source as $key=>$item)
			if ($only == !in_array($key, $keys, $strict))
				unset($source[$key]);
		
		return $source;
	}
	
	/**
	 * Фильтрация массива по типу элементов
	 * @param array $source Исходный массив
	 * @param mixed $types Список фильтруемых типов элементов. Может задаваться списком или в строке через запятую
	 * @param boolean $only Признак работы в режиме "Только обязательные". По умолчанию равно true. В режиме "Только обязательные"
	 * в массиве останутся только элементы с типами из списка $types. Если равен false, то работа будет происходить в режиме
	 * "Исключения", в котором из массива будут удалены все элементы с типом из списка $types
	 * @result array|boolean Отфильтрованный массив или false
	 */
	public static function filterType($source, $types, $only = true){
		if (!is_array($source))
			return false;
		
		if (!$source || !$types)
			return $source;
		elseif (is_string($types))
			$types = Strings::devisionWords($types);
		elseif (!is_array($types))
			$types = array($types);
		
		foreach ($source as $key=>$item)
			if ($only === !in_array(gettype($item), $types))
				unset($source[$key]);
		
		return $source;
	}
	
	/**
	 * Удаление логически-пустых элементов из массива
	 * @param array $source Исходный массив
	 * @return array|boolean Отфильтрованный массив или false
	 */
	public static function filterEmpty($source){
		if (!is_array($source))
			return false;
		
		foreach ($source as $key=>$item)
			if (!$item)
				unset($source[$key]);
		
		return $source;
	}
	
	/**
	 * Приведение типа элементов массива
	 * @param array $source Исходный массив
	 * @param string $type Название требуемого типа
	 * @return array|boolean Изменённый массив или false
	 */
	public static function setType($source, $type){
		if (!is_array($source))
			return false;
		
		foreach ($source as &$value)
			settype($value, $type);
		unset($value);
		
		return $source;
	}
	
}
