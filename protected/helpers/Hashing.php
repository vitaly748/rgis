<?php
/**
 * Класс дополнительных функций для работы с хэшем на основе Blowfish-алгоритма
 * @author v.zubov
 */
class Hashing{
	
	/**
	 * @var Префикс соли
	 */
	const PREFIX_HASH = '$2y$';
	
	/**
	 * @var Весовой параметр хэша для даты. Допустимый диапазон: от 4 до 31
	 */
	const WEIGHT_DATE = 6;
	
	/**
	 * @var Весовой параметр хэша для пароля
	 */
	const WEIGHT_PASSWORD = 13;
	
	/**
	 * Вычисление ключа хэша по строке
	 * @param string $str Исходная строка
	 * @return integer Ключ хэша, число от 0 до 9
	 */
	public static function key($str){
		$key = 0;
		
		for ($count = 0; $count < mb_strlen($str); $count++)
			$key += ord(mb_substr($str, $count, 1));
		
		return $key % 10;
	}
	
	/**
	 * Подготовка соли для функции хэширования
	 * @param integer $weight Весовой параметр хэша. По умолчанию равно self::WEIGHT_PASSWORD
	 * @return string Соль
	 */
	public static function salt($weight = self::WEIGHT_PASSWORD){
		$values = array_merge(array('/', '.'), range('0', '9'), range('A', 'Z'), range('a', 'z'));
		$qu = count($values) - 1;
		$salt = '';
		
		for ($count = 0; $count < 22; $count++)
			$salt .= $values[mt_rand(0, $qu)];
		
		if ($weight < 10)
			$weight = '0'.$weight;
		
		return self::PREFIX_HASH.$weight.'$'.$salt;
	}
	
	/**
	 * Вычисление зашифрованного хэша строки
	 * @param string $str Исходная строка
	 * @param array|boolean|integer $keys Ключ, список ключей хэша или false. По умолчанию равно false
	 * @param integer $weight Весовой параметр хэша. По умолчанию равно self::WEIGHT_PASSWORD
	 * @return string Зашифрованный хэш строки
	 */
	public static function encode($str, $keys = false, $weight = self::WEIGHT_PASSWORD){
		$str = mb_substr(crypt($str, self::salt($weight)), 7);
		
		if ($keys !== false){
			$len = mb_strlen($str);
			
			if (!is_array($keys))
				$keys = (array)$keys;
			
			foreach ($keys as $key){
				$bufer = '';
				
				foreach (self::permutation($len, (int)$key) as $pos)
					$bufer .= mb_substr($str, $pos, 1);
				
				$str = $bufer;
			}
		}
		
		return $str;
	}
	
	/**
	 * Расшифровка хэша строки
	 * @param string $str Зашифрованный хэш строки
	 * @param array|boolean|integer $keys Ключ, список ключей хэша или false. По умолчанию равно false
	 * @param integer $weight Весовой параметр хэша. По умолчанию равно self::WEIGHT_PASSWORD
	 * @return string Расшифрованный хэш строки
	 */
	public static function decode($str, $keys, $weight = self::WEIGHT_PASSWORD){
		if ($keys !== false){
			$len = mb_strlen($str);
			
			if (!is_array($keys))
				$keys = (array)$keys;
			
			foreach (array_reverse($keys) as $key){
				$bufer = '';
				$permutation = self::permutation($len, (int)$key);
				
				for ($count = 0; $count < $len; $count++)
					$bufer .= mb_substr($str, array_search($count, $permutation), 1);
				
				$str = $bufer;
			}
		}
		
		if ($weight < 10)
			$weight = '0'.$weight;
		
		return self::PREFIX_HASH.$weight.'$'.$str;
	}
	
	/**
	 * Проверка соответствия пароля хэшу
	 * @param string $password Проверяемый пароль
	 * @param array|UserModel|string $hash Зашифрованный хэш строки. Может задаваться объектом класса UserModel или
	 * ассоциативным массивом, содержащим параметры "login" и "password"
	 * @param array|boolean|integer $keys Ключ, список ключей хэша или false. По умолчанию равно false
	 * @param integer $weight Весовой параметр хэша. По умолчанию равно self::WEIGHT_PASSWORD
	 * @return boolean Результат проверки
	 */
	public static function check($password, $hash, $keys = false, $weight = self::WEIGHT_PASSWORD){
		if (!is_string($hash))
			list($hash, $keys) = array($hash['password'], self::key($hash['login']));
		
		$hash = self::decode($hash, $keys, $weight);
		
		return crypt($password, $hash) === $hash;
	}
	
	/**
	 * Тестирование функций класса. В тестировании участвуют 10 строк по заданному числу повторов для каждой
	 * @param integer $weight Весовой параметр. По умолчанию равно 4
	 * @param integer $retries Количество повторов для каждой строки. По умолчанию равно 100
	 */
	public static function test($weight = 4, $retries = 100){
		set_time_limit(0);
		$timer_total = Swamper::timer($tick_total);
		$count_test = $count_correct = 0;
		
		foreach (range('d', 'm') as $name){
			$name = 'vitaly_'.$name;
			$key = self::key($name);
			
			for ($count = 0; $count < $retries; $count++){
				$timer = Swamper::timer($tick, true);
				$hash = self::encode($name, $key, $weight);
				$timer_encode = Swamper::timer($tick, true);
				$decode_hash = self::decode($hash, $key, $weight);
				$timer_decode = Swamper::timer($tick);
				$correct = crypt($name, $decode_hash) === $decode_hash;
				Arrays::printPre("$name  -  $hash  -  ".($correct ? 'true' : 'false')."  -  $timer_encode  -  $timer_decode");
				$count_test++;
				$count_correct += $correct;
			}
		}
		
		Arrays::printPre("$count_correct / $count_test  -  ".Swamper::timer($tick_total), true, false, 1);
	}
	
	/**
	 * Подготовка схемы перестановок хэша
	 * @param integer $length Длина строки хэша
	 * @param integer $key Ключ хэша
	 * @return array Список переставленных индексов символов хэша
	 */
	private static function permutation($length, $key){
		$permutation = array_chunk(range(0, $length - 1), max(min($key, 9), 0) + 1);
		$qu = count($permutation);
		$result = array();
		
		for ($count = 0; $count < $qu; $count += 4)
			foreach ($count + 3 == $qu ? array(2, 1, 0) : array(2, 3, 0, 1) as $add)
				if ($count + $add < $qu)
					$result = array_merge($result, $permutation[$count + $add]);
		
		return $result;
	}
	
}
