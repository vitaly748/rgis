<?php
/**
 * Класс дополнительных функций для работы с числами
 * @author v.zubov
 */
class Numbers{
	
	/**
	 * Формирование строки числа с плавающей точкой
	 * @param float $number Число
	 * @param integer $decimals Число знаков после запятой
	 * @param string $unit Строка с единицей измерения. По умолчанию равно Swamper::UNIT_RUB
	 * @return string Результат
	 */
	public static function format($number, $decimals = 2, $unit = Swamper::UNIT_RUB){
		$number = number_format($number, $decimals, ',', ' ');
		
		if ($number == '-0,00')
			$number = '0,00';
		
		if ($unit)
			$number .= $unit;
		
		return $number;
	}
	
	/**
	 * Целочисленное деление
	 * @param integer $a Делимое
	 * @param integer $b Делитель
	 * @return integer Результат
	 */
	function intdiv($a, $b){
		return ($a - $a % $b) / $b;
	}
	
	/**
	 * Генерация случайного числа
	 * @param integer $len Максимальная длина символьного представления числа. По умолчанию равно 8
	 * @return integer Случайное число
	 */
	function uniqid($len = 8){
		do{
			$id = '';
			
			for ($count = 0; $count < $len; $count++)
				$id .= rand(0, 9);
			
			$id = (int)$id;
		}while (!$id);
		
		return $id;
	}
	
}
