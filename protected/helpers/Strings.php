<?php
/**
 * Класс дополнительных функций для работы со строками
 * @author v.zubov
 */
class Strings{
	
	/**
	 * @var integer Код формата модификации имени "Авто"
	 */
	const FORMAT_NAME_MODIFY_AUTO = 0;
	
	/**
	 * @var integer Код формата модификации имени "Верхний"
	 */
	const FORMAT_NAME_MODIFY_UPPER = 1;
	
	/**
	 * @var integer Код формата модификации имени "Нижний"
	 */
	const FORMAT_NAME_MODIFY_LOWER = 2;
	
	/**
	 * @var integer Код формата модификации имени "Средний"
	 */
	const FORMAT_NAME_MODIFY_MIDDLE = 3;
	
	/**
	 * @var integer Код формата модификации имени "Подробный"
	 */
	const FORMAT_NAME_MODIFY_WORDS = 4;
	
	/**
	 * @var string Шаблон именной строки пользователя в формате "Полный". Пример: Surname Name Patronymic
	 */
	const FORMAT_USER_NAME_FULL = 'S N P';
	
	/**
	 * @var string Шаблон именной строки пользователя в формате "Инициалы". Пример: Surname N.P.
	 */
	const FORMAT_USER_NAME_INITIALS = 'S N1.P1.';
	
	/**
	 * @var string Шаблон именной строки пользователя в формате "Инициалы для обработки". Пример: surname n p
	 */
	const FORMAT_USER_NAME_INITIALS2 = 's n1 p1';
	
	/**
	 * @var array Список символов, которые требуют экранирования в регулярных выражениях
	 */
	const ESCAPE_CHARS = array('\\', '|', '/', '-', '[', ']');
	
	/**
	 * Приведение строкового идентификатора к одному из трёх форматов: "FirstSecond", "first_second", "first-second" или
	 * "array('first', 'second')" ("Верхний", "Нижний", "Средний" или "Словесный" соответственно)
	 * @param array|string $name Исходная строка или массив слов
	 * @param number $format Код требуемого формата. По умолчанию равно "Авто". В режиме "Авто" текущий формат
	 * будет определён автоматически и строка будет переведена в противоположный формат (из "Верхнего" - в "Нижний",
	 * из остальных - в "Верхний"). Автоматическое определение основывается на наличии в строке символов в верхнем
	 * регистре: если таковые имеются, принимается, что строка в формате "Верхний"
	 * @param boolean $uc_first Признак верхнего регистра для первого символа строки, применяется для формата
	 * "Верхний". По умолчанию равно true. При использовании формата "Авто", определяется автоматически
	 * @param boolean $model Признак наличия постфикса "model" в строке. По умолчанию равен false. При использовании формата
	 * "Авто", определяется автоматически
	 * @return boolean|string Строка в нужном формате или false
	 */
	public static function nameModify($name, $format = self::FORMAT_NAME_MODIFY_AUTO, $uc_first = true, $model = false){
		if (!is_string($name))
			return false;
		
		$words = array();
		$format_original = self::FORMAT_NAME_MODIFY_LOWER;
		
		if (is_string($name)){
			$word = '';
			$len = mb_strlen($name);
			
			for ($count = 0; $count < $len; $count++){
				$char = $name[$count];
				$upper = mb_strtoupper($char) === $char;
				$separator = in_array($char, array('_', '-'), true);
				
				if ($separator || $upper){
					if ($word){
						$words[] = $word;
						$word = '';
					}
					
					if (!$separator && $upper){
						$word = mb_strtolower($char);
						$format_original = self::FORMAT_NAME_MODIFY_UPPER;
					}
				}else
					$word .= $char;
			}
			
			if ($word)
				$words[] = $word;
		}else
			foreach ($name as $word)
				$words[] = mb_strtolower($word);
		
		if (!$words)
			return false;
		
		if ($format === self::FORMAT_NAME_MODIFY_WORDS)
			return $words;
		elseif ($format === self::FORMAT_NAME_MODIFY_AUTO){
			$format = $format_original === self::FORMAT_NAME_MODIFY_LOWER ? self::FORMAT_NAME_MODIFY_UPPER :
				self::FORMAT_NAME_MODIFY_LOWER;
			
			$uc_first = $model = $format === self::FORMAT_NAME_MODIFY_UPPER;
		}
		
		$model_present = $words[count($words) - 1] === 'model';
		
		if ($model && !$model_present)
			$words[] = 'model';
		elseif (!$model && $model_present)
			unset($words[count($words) - 1]);
		
		if ($format === self::FORMAT_NAME_MODIFY_UPPER){
			foreach ($words as $key=>$word)
				if ($key || $uc_first)
					$words[$key] = ucfirst($word);
			
			$glue = '';
		}else
			$glue = $format === self::FORMAT_NAME_MODIFY_LOWER ? '_' : '-';
		
		return implode($glue, $words);
	}
	
	/**
	 * Замена всех обратных слэшей в строке на простые
	 * @param string $str Исходная строка
	 * @return string Строка без обратных слэшей
	 */
	public static function correctionSlashes($str){
		return str_replace('\\', '/', $str);
	}
	
	/**
	 * Определение имени файла из полного пути к файлу
	 * @param string $path Полный путь файла
	 * @return boolean|string Имя файла или false в случае ошибки
	 */
	public static function getFileNameFromPath($path){
		$info = pathinfo($path);
		return isset($info['filename']) ? $info['filename'] : false;
	}
	
	/**
	 * Дополнение строки словами с соблюдением уникальности слов
	 * @param array|string $source Исходная строка или массив из слов
	 * @param array|boolean|string $append Дополняющая строка или массив из дополняющих слов.
	 * По умолчанию равно false. Может задаваться в виде ассоциативного массива в формате {Слово}=>{Признак наличия данного слова}
	 * @param boolean|string $prefix Префикс слов или false. По умолчанию равно false
	 * @param boolean|string $postfix Постфикс слов или false. По умолчанию равно false
	 * @param boolean|string $delimiter Разделитель слов в строке. По умолчанию равно " ". Если равен false, то объединения
	 * не произойдёт, будет возвращён массив
	 * @return array|string Объединённая строка или массив с уникальным набором слов
	 */
	public static function mergeWords($source, $append = false, $prefix = false, $postfix = false, $delimiter = ' '){
		if (!is_array($source))
			$source = self::devisionWords((string)$source, false, $delimiter);
		
		if ($append){
			if (!is_array($append))
				$append = self::devisionWords((string)$append, false, $delimiter);
			elseif (is_string(reset(array_keys($append))))
				$append = Arrays::forming($append, true);
			
			$source = array_merge($source, $append);
		}
		
		$source = array_diff($source, array(''));
		
		if ($prefix || $postfix)
			foreach ($source as $key=>$word)
				$source[$key] = $prefix.$word.$postfix;
		
		$source = array_unique($source);
		
		return $delimiter ? implode($delimiter, $source) : $source;
	}
	
	/**
	 * Проверка наличия слова в строке
	 * @param string $source Исходная строка
	 * @param string $word Искомое слово
	 * @param boolean $remove_word Признак необходимости удаления искомого слова из строки. По умолчанию равно false
	 * @param boolean $prefix Признак проверки по префиксу. По умолчанию равно false
	 * @param string $delimiter Разделитель слов в строке. По умолчанию равно " "
	 * @return boolean Признак наличия искомого слова в строке
	 */
	public static function hasWord(&$source, $word, $remove_word = false, $prefix = false, $delimiter = ' '){
		$words = self::devisionWords($source, false, $delimiter);
		
		if ($words)
			foreach ($words as $key=>$value)
				if ($word === $value || ($prefix && mb_strpos($value, $word) === 0)){
					if ($remove_word){
						unset($words[$key]);
						$source = implode($delimiter, $words);
					}
					return true;
				}
		
		return false;
	}
	
	/**
	 * Проверяет соответствие строкового составного ключа некоторому строковому шаблону. Шаблооны могут
	 * содержать звёздочки в любой позиции, звёздочка соответствует любому слову
	 * @param string $key Строковой составной ключ. Например, "name1.name2.name3"
	 * @param array|string $pattern Строковой шаблон или массив из них. Например, "name1.*.name3"
	 * @param string $delimiter Разделитель слов в строке. По умолчанию равно "."
	 * @return boolean|string Совпавший шаблон или false
	 */
	public static function checkKeyPattern($key, $patterns, $delimiter = '.'){
		if ($key && $patterns){
			if (!is_array($patterns))
				$patterns = array($patterns);
			
			if ($key_words = explode($delimiter, $key)){
				$qu_key_words = count($key_words);
				foreach ($patterns as $pattern)
					if ($pattern_words = explode($delimiter, $pattern)){
						$qu_pattern_words = count($pattern_words);
						if ($qu_key_words >= $qu_pattern_words){
							$key_words_slice = array_slice($key_words, -$qu_pattern_words);
							$matches = 0;
							
							foreach ($key_words_slice as $index=>$key_word)
								$matches += in_array($pattern_words[$index], array('*', $key_word));
							
							if ($matches === $qu_pattern_words)
								return $pattern;
						}
					}
			}
		}
		
		return false;
	}
	
	/**
	 * Разделение строки на слова
	 * @param array|string $source Исходная строка или список строк
	 * @param boolean $empty Признак допустимости пустых слов. По умолчанию равно false
	 * @param string $delimiter Разделитель слов в строке. По умолчанию равно ","
	 * @return array Список слов, входящих в исходную строку
	 */
	public static function devisionWords($source, $empty = false, $delimiter = ','){
		$words = array();
		
		foreach (is_array($source) ? $source : array($source) as $string)
			foreach (explode($delimiter, (string)$string) as $word){
				$word = trim($word);
				
				if ($empty || mb_strlen($word))
					$words[] = $word;
			}
		
		return $words;
	}
	
	/**
	 * Подготовка именной строки пользователя
	 * @param array|UserModel|string $user Стока, массив или объект, содержащие ФИО пользователя
	 * @param string $format Шаблон необходимой строки. По умолчанию используется "Полный" формат. Шаблон может
	 * содержать следующие специальные комбинации:
	 * 	S  ->  Surname
	 * 	s  ->  surname
	 * 	SS ->  SURNAME
	 *  S3 ->  Sur
	 *  s4 ->  surn
	 *  N  ->  Name
	 *  P  ->  Patronymic
	 * @return string Именная строка пользователя
	 */
	public static function userName($user, $format = self::FORMAT_USER_NAME_FULL){
		$name = $surname = $patronymic = false;
		
		if (is_string($user)){
			$user = preg_split('/[\s.,]+/u', $user, -1, PREG_SPLIT_NO_EMPTY);
			
			if (isset($user[0]))
				$surname = $user[0];
			
			if (isset($user[1]))
				$name = $user[1];
			
			if (isset($user[2]))
				$patronymic = $user[2];
		}elseif (is_array($user)){
			if (isset($user['name']))
				$name = $user['name'];
			
			if (isset($user['surname']))
				$surname = $user['surname'];
			
			if (isset($user['patronymic']))
				$patronymic = $user['patronymic'];
			
			if (!$name && !$surname && !$patronymic && !empty($user['title']))
				return $user['title'];
		}elseif ($user instanceof UserModel)
			if (($auth_manager = Yii::app()->user->authManager) instanceof PhpAuthManager){
				if ($user->title)
					return $user->title;
				elseif ($user->name || $user->surname || $user->patronymic)
					list($name, $surname, $patronymic) = array($user->name, $user->surname, $user->patronymic);
				else
					return false;
			}else{
				$fio = $user->name || $user->surname || $user->patronymic;
				
				if (Yii::app()->user->authManager->checkAccess('organization', $user->id) && $user->title)
					return $user->title;
				elseif (Yii::app()->user->authManager->checkAccess('citizen', $user->id) && $fio)
					list($name, $surname, $patronymic) = array($user->name, $user->surname, $user->patronymic);
				elseif ($user->title)
					return $user->title;
				elseif ($fio)
					list($name, $surname, $patronymic) = array($user->name, $user->surname, $user->patronymic);
				else
					return reset(Yii::app()->user->getRoles($user->id, 'description'));
			}
		
		$data = array('n'=>$name, 's'=>$surname, 'p'=>$patronymic);
		$result = '';
		
		preg_match_all('/(s{1,2}\d*|n{1,2}\d*|p{1,2}\d*|[^snp\d]*)/ui', $format, $matches);
		
		if (!empty($matches[0]))
			foreach ($matches[0] as $match)
				if ($match){
					$first_char = mb_strtolower(mb_substr($match, 0, 1));
					
					if (!in_array($first_char, array_keys($data)))
						$result .= $match;
					elseif ($str = $data[$first_char]){
						preg_match_all('/([snp]{1,2})(\d*)/ui', $match, $matches2);
						
						if ($prefix = !empty($matches2[1][0]) ? $matches2[1][0] : false){
							if (mb_strlen($prefix) === 2 && $prefix === mb_strtoupper($prefix))
								$str = mb_strtoupper($str);
							elseif ($prefix !== mb_strtoupper($prefix))
								$str = mb_strtolower($str);
							else
								$str = self::ucFirst($str);
							
							if ($len = !empty($matches2[2][0]) ? $matches2[2][0] : false)
								$str = mb_substr($str, 0, $len);
							
							$result .= $str;
						}
					}
				}
		
		return trim(preg_replace('/\s\s+/u', ' ', $result));
	}
	
	/**
	 * Аналог стандартной функции ucfirst, за тем исключением, что работает она для текущей кодировки,
	 * все символы, начиная со второго, приводятся к нижнему регистру, и, в случае надобности, применяется
	 * функция mb_substr
	 * @param string $str Исходная строка
	 * @param boolean|integer Позиция начала подстроки или false. По умолчанию равно false
	 * @param boolean|integer Длина подстроки или false. По умолчанию равно false
	 * @return string Преобразованния строка
	 */
	public static function ucFirst($str, $start = false, $length = false){
		$str = mb_strtoupper(mb_substr($str, 0, 1)).mb_strtolower(mb_substr($str, 1));
		
		if ($start || $length)
			$str = mb_substr($str, $start, $length);
		
		return $str;
	}
	
	/**
	 * Корректировка буквенных диапозонов в регулярном выражении. Также экранирование символов "\" и "-"
	 * @param string $set Исходная строка регулярного выражения
	 * @return string Обработанная строка
	 */
	public static function preparationRegexp($regexp){
		$regexp_preparation = '';
		$replace_matches = array();
		
		preg_match_all('/(\d)-(\d)/u', $regexp, $matches_num);
		preg_match_all('/([A-Za-z])-([A-Za-z])/u', $regexp, $matches_eng);
		preg_match_all('/([А-яЁё])-([А-яЁё])/u', $regexp, $matches_rus);
		
		if ($matches_num)
			foreach ($matches_num[0] as $match)
				$replace_matches[$match] = $match;
		
		if ($matches_eng)
			foreach (array_keys($matches_eng[0]) as $key_match){
				$match = $matches_eng[0][$key_match];
				$left = $matches_eng[1][$key_match];
				$right = $matches_eng[2][$key_match];
				
				if ($left < $right && $left <= 'Z' && $right >= 'a'){
					if ($left !== 'Z')
						$left .= '-Z';
					
					if ($right !== 'a')
						$right = 'a-'.$right;
					
					$match = $left.$right;
				}
				
				$replace_matches[$matches_eng[0][$key_match]] = $match;
			}
		
		if ($matches_rus && count($matches_rus[0])){
			$segments_borders = array(array('А', 'Ё', 'Ж', 'ё', 'ж'), array('Е', 'Ё', 'е', 'ё', 'я'));
			$segments_qu = count($segments_borders[0]);
			
			foreach (array_keys($matches_rus[0]) as $key_match){
				$match = $matches_rus[0][$key_match];
				$left = $matches_rus[1][$key_match];
				$right = $matches_rus[2][$key_match];
				
				foreach (array($left, $right) as $key_char=>$char)
					for ($key_segment = 0; $key_segment < $segments_qu; $key_segment++)
						if ($char >= $segments_borders[0][$key_segment] && $char <= $segments_borders[1][$key_segment]){
							$segments[$key_char] = $key_segment;
							break;
						}
				
				if ($segments[0] < $segments[1]){
					if ($segments[0] % 2)
						$left = $segments_borders[0][$segments[0] + 1];
					
					if ($segments[1] % 2)
						$right = $segments_borders[1][$segments[1] - 1];
					
					if ($right !== $left)
						$left .= '-'.$right;
					
					if ($segments[0] < 2)
						$left .= 'Ё';
					
					if ($segments[1] > 2)
						$left .= 'ё';
					
					$match = $left;
				}
				
				$replace_matches[$matches_rus[0][$key_match]] = $match;
			}
		}
		
		if ($replace_matches){
			$regexp = str_replace(array_keys($replace_matches), '', $regexp);
			$regexp_preparation = implode('', $replace_matches);
		}
		
		if ($regexp === '\\s')
			return $regexp;
		
		$escape_chars = array_map(function($char){
			return '\\'.$char;
		}, self::ESCAPE_CHARS);
		
		return str_replace(self::ESCAPE_CHARS, $escape_chars, $regexp).$regexp_preparation;
	}
	
	/**
	 * Формирование корректного HTML-кода для вывода многострочного текста с учётом переноса строк и начальных пробелов
	 * @param string $text Исходный текст
	 * @return string HTML-код
	 */
	public static function multilineText($text){
		if ($lines = explode("\r\n", $text))
			foreach ($lines as $key=>$line){
				preg_match('/^ */u', $line, $matches);
				
				if ($space = Arrays::pop($matches, 0))
					$lines[$key] = str_repeat('&nbsp;', ($len = mb_strlen($space))).mb_substr($line, $len);
			}
		
		return implode(Html::tag('br'), $lines);
	}
	
}
