<?php
/**
 * Класс дополнительных функций для работы с кэшем
 * @author v.zubov
 */
class Cache{
	
	/**
	 * @var array Ассоциативный массив параметров кэша по умолчанию
	 */
	const PARAMS_DEF = array(
		'cache_type'=>Param::PREFIX_LOCKED_PARAM.'file',// Тип кэширования
		'duration1'=>60 * 60 * 24 * 30,// Время хранения данных в кэше в секундах по схеме "Долго"
		'duration2'=>60 * 60 * 24,// Время хранения данных в кэше в секундах по схеме "Нормально"
		'duration3'=>60 * 30,// Время хранения данных в кэше в секундах по схеме "Быстро"
		'duration4'=>60 * 10// Время хранения данных bss-запросов в кэше в секундах
	);
	
	/**
	 * @var string Префикс идентификаторов записей кэша группы "Системные записи"
	 */
	const PREFIX_SYSTEM = '';
	
	/**
	 * @var string Префикс идентификаторов записей кэша группы "Мета-данные таблиц БД"
	 */
	const PREFIX_META = 'meta';
	
	/**
	 * @var string Префикс идентификаторов записей кэша группы "Данные моделей БД"
	 */
	const PREFIX_MODEL = 'model';
	
	/**
	 * @var string Префикс идентификаторов записей кэша группы "Данные записей моделей БД"
	 */
	const PREFIX_RECORD = 'record';
	
	/**
	 * @var string Префикс идентификаторов записей кэша группы "Данные Биллинга ЖКХ"
	 */
	const PREFIX_BSS1 = '';
	
	/**
	 * @var string Префикс идентификаторов записей кэша группы "Данные Биллинга капремонта"
	 */
	const PREFIX_BSS2 = '';
	
	/**
	 * @var string Шаблон параметров в идентификаторах записей кэша
	 */
	const PATTERN_PARAM = '(%s)';
	
	/**
	 * @var string Название класса фиктивного компонента кэша
	 */
	const CLASS_DUMMY = 'DummyCache';
	
	/**
	 * Инициализация кэша приложения
	 * @param array $params Ассоциативный массив параметров кэша
	 * @return boolean Признак активности кэша
	 */
	public static function init($params = false){
		if (!$params)
			$params = Yii::app()->param->get('cache');
		
		$controller = Yii::app()->controller;
		$components_config = $controller ? Yii::app()->param->componentsConfig : Yii::app()->getComponents(false);
		$on = ($config_component_cache = Arrays::pop($components_config, 'cache')) && ($type = Arrays::pop($params, 'cache_type')) &&
			$type != 'disabled' && (!$locked = mb_strpos($type, Param::PREFIX_LOCKED_PARAM) === 0);
		$component_cache_on = ($component_cache = Yii::app()->getComponent('cache', false)) instanceof CComponent &&
			get_class($component_cache) != static::CLASS_DUMMY;
		
		if ($controller){
			$deployed = $controller->deployed;
			
			if ($on && !$deployed){
				$params['cache_type'] = $type = Param::PREFIX_LOCKED_PARAM.$type;
				Yii::app()->param->set('cache', $params);
				$on = false;
			}elseif (!$on && $deployed && !empty($locked)){
				$params['cache_type'] = $type = mb_substr($type, mb_strlen(Param::PREFIX_LOCKED_PARAM));
				Yii::app()->param->set('cache', $params);
				$on = true;
			}
			
			if (!$on && $component_cache_on)
				Yii::app()->cache->flush();
		}
		
		if ($on != $component_cache_on){
			$config_component_cache['class'] = $on ? ucfirst($type).'Cache' : static::CLASS_DUMMY;
			Yii::app()->setComponent('cache', $config_component_cache);
			
			if ($config_component_db = Arrays::pop($components_config, 'db')){
				$config_component_db['schemaCachingDuration'] = $on ? Arrays::pop($params, 'duration1') : 0;
				$config_component_db['queryCachingDuration'] = $on ? Arrays::pop($params, 'duration2') : 0;
				Yii::app()->setComponent('db', $config_component_db);
			}
			
			if ($config_component_bss1 = Arrays::pop($components_config, 'bss1')){
				$config_component_bss1['cachingDuration'] = $on ? Arrays::pop($params, 'duration4') : 0;
				Yii::app()->setComponent('bss1', $config_component_bss1);
			}
			
			if ($config_component_bss2 = Arrays::pop($components_config, 'bss2')){
				$config_component_bss2['cachingDuration'] = $on ? Arrays::pop($params, 'duration4') : 0;
				Yii::app()->setComponent('bss2', $config_component_bss2);
			}
		}
		
		return $on;
	}
	
}
