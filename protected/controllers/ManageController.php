<?php
/**
 * Контроллер по управлению основными блоками приложения
 * @author v.zubov
 */
class ManageController extends Controller{
	
	/**
	 * @var string Псевдоним пути к папке файла конфигурации
	 */
	const ALIAS_CFG = 'application.config';
	
	/**
	 * @var string Имя файла конфигурации
	 */
	const FN_CFG = 'powers.php';
	
	/**
	 * @var array Ассоциативный массив с информацией об уполномоченных организациях Системы
	 */
	const ORGANIZATION = array(
		10=>array(
			'name'=>'Министерство строительства и жилищно-коммунального хозяйства Пермского края',
			'power'=>'Владелец Системы'
		),
		20=>array(
			'name'=>'ООО "Софт М"',
			'power'=>'Разработчик Системы'
		),
		30=>array(
			'name'=>'Некоммерческая организация "Фонд капитального ремонта общего имущества в многоквартирных домах в Пермском крае"',
			'power'=>'Эксплуатация Системы'
		)
	);
	
	/**
	 * Действие редактирования списка уполномоченных организаций
	 */
	public function actionOrganization(){
		if (!$_POST){
			$powers = static::getPowers();
			
			$this->render('organization', array('powers'=>$powers ? $powers : array()));
		}else{
			$params_fix = array();
			
			try{
				$powers = Arrays::pop($_POST, 'powers');
				
				if ($powers)
					$powers = array_intersect($powers, array_keys(static::ORGANIZATION));
				
				if (!$powers)
					$powers = array();
				
				$params_fix['powers'] = $powers;
				
				$path = $params_fix['path'] = Yii::getPathOfAlias(static::ALIAS_CFG).DIRECTORY_SEPARATOR.static::FN_CFG;
				
				if (file_put_contents($path, $powers ? implode(',', $powers) : '') === false)
					throw new CHttpException(500, 'error record to file');
			}catch (CHttpException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('powers_change', $params_fix, $ok ? 'ok' : 'powers_change');
			
			$this->refreshWithoutParams();
		}
	}
	
	/**
	 * Действие редактирования ролей пользователей
	 */
	public function actionRole(){
		$model = new RoleModel;
		
		if (!$model->access)
			throw new CHttpException(500);
		
		$pages = Yii::app()->db->createCommand()->
			select(array('name'))->
			from('{{auth_item}}')->
			where('type = :type', array(':type'=>CAuthItem::TYPE_TASK))->
			order('id')->
			queryColumn();
		
		$rows = Yii::app()->db->createCommand()->
			select(array(
				't.name',
				't.description',
				'STRING_AGG("t"."child", \',\') AS "pages"'
			))->
			from('('.Yii::app()->db->createCommand()->
				select(array(
					'ai.id',
					'ai.name',
					'ai.description',
					'aic.child'
				))->
				from('{{auth_item}} ai')->
				leftJoin('{{auth_item_child}} aic', '"aic"."parent" = "ai"."name"')->
				where('ai.type = '.CAuthItem::TYPE_ROLE)->
				text.
			') "t"')->
			group('t.id, t.name, t.description')->
			order('t.id')->
			queryAll();
		
		if (!$pages || !$rows)
			throw new CHttpException(500);
		
		foreach ($rows as $row){
			$roles[$row['name']] = $row['description'];
			$roles_pages[$row['name']] = array_values(array_intersect(explode(',', $row['pages']), $pages));
		}
		
		$pages_names = Arrays::select(Yii::app()->scheme->schemeItem, 'title', 'route');
		$pages = array_flip($pages);
		$groups = Yii::app()->conformity->get('role_group', 'code', 'name');
		
		foreach ($pages as $name=>$value)
			$pages[$name] = 'Доступ к странице "'.Arrays::pop($pages_names, $name).'"';
		
		if (!$model->post){
			$model->name = reset(array_keys($roles));
			$model->group = array_values($groups);
			$model->pages = Arrays::pop($roles_pages, $model->name);
		}else{
			$model->attributes = $model->post;
			$params_fix = array('model'=>$model->attributes, 'add_page'=>array(), 'remove_page'=>array());
			
			try{
				$transaction = Yii::app()->db->beginTransaction();
				
				if (!isset($roles[$model->name]))
					throw new CHttpException(500, 'invalid role');
				
				if (!$model->group || array_diff($model->group, $groups))
					throw new CHttpException(500, 'invalid group');
				
				$role_pages = isset($roles_pages[$model->name]) ? $roles_pages[$model->name] : array();
				$pages_set = array_keys($pages);
				
				if ($model->group != array_values($groups)){
					$group = array_search(reset($model->group), $groups);
					
					foreach ($pages_set as $key=>$page)
						if ((($appeal = mb_strpos($page, 'appeal') === 0) && $group != 'appeal') || (!$appeal && $group == 'appeal'))
							unset($pages_set[$key]);
				}
				
				foreach ($pages_set as $page)
					if (!in_array($page, $role_pages)){
						if (in_array($page, $model->pages)){
							$model_auth = new AuthItemChildModel;
							$model_auth->setAttributes(array(
								'parent'=>$model->name,
								'child'=>$page
							), false);
							
							if (!$model_auth->save(false))
								throw new CHttpException(500, 'error auth_item_child save');
							
							$params_fix['add_page'][] = $page;
						}
					}elseif (!in_array($page, $model->pages)){
						$model_auth = AuthItemChildModel::model()->findByAttributes(array('parent'=>$model->name, 'child'=>$page));
						
						if (!$model_auth)
							throw new CHttpException(500, 'auth_item_child not found');
						
						if (!$model_auth->delete())
							throw new CHttpException(500, 'error auth_item_child delete');
						
						$params_fix['remove_page'][] = $page;
					}
				
				$transaction->commit();
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
				$transaction->rollback();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('role_change', $params_fix, $ok ? 'ok' : 'role_change');
			
			$this->refreshWithoutParams();
		}
		
		$this->render('role', array('model'=>$model, 'roles'=>$roles, 'pages'=>$pages, 'roles_pages'=>$roles_pages,
			'groups'=>$groups));
	}
	
	/**
	 * Действие проверки целостности Базы данных
	 */
	public function actionDb(){
		if (!$state_tables = Yii::app()->table->checkTables())
			throw new CHttpException(500);
		
		$id = 1;
		
		foreach ($state_tables as $name=>$state)
			$data[] = array(
				'id'=>$id++,
				'name'=>$name,
				'state'=>$state,
				'state_description'=>$state ? 'OK' : 'Ошибка'
			);
		
		$this->render('db', array('data'=>$data));
	}
	
	/**
	 * Получение полномочий организации
	 * @param string $organization Название организации
	 * @return boolean|string Полномочия организации или false
	 */
	public static function getOrganizationPowers($organization){
		if (!$organization)
			return false;
		
		if (!$powers = self::getPowers())
			return false;
		
		foreach ($powers as $power)
			if (mb_strtolower(self::ORGANIZATION[$power]['name']) === mb_strtolower($organization))
				return self::ORGANIZATION[$power]['power'];
		
		return false;
	}
	
	/**
	 * Получение списка идентификаторов доступных полномочий организаций
	 * @return array Список идентификаторов или false
	 */
	protected static function getPowers(){
		$path = Yii::getPathOfAlias(static::ALIAS_CFG).DIRECTORY_SEPARATOR.static::FN_CFG;
		
		if (!is_file($path))
			return false;
		
		return ($content = file_get_contents($path)) ? explode(',', $content) : false;
	}
	
}
