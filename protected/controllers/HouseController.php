<?php
/**
 * Контроллер работы с информацией по домам, связанным с лицевыми счетами пользователя
 * @author v.zubov
 */
class HouseController extends Controller{
	
	/**
	 * Действие просмотра общей информации по домам
	 * @param integer $id Идентификатор дома. По умолчанию равно false
	 */
	public function actionGeneral($id = false){
		$model = new HouseGeneralModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest){
			if (($operator = Yii::app()->user->checkAccessSet('developer, administrator, clerk, specialist')) && $id)
				$row = Yii::app()->db->createCommand()->
					select(array('id', 'address'))->
					from('{{fias_address}}')->
					where("id_bss = '$id'")->
					queryRow();
			
			$this->render('general', array('model'=>$model, 'operator'=>$operator, 'house'=>$operator && $id && $row ? $row : false));
		}else
			$this->renderPartial('generalGrid', array('model'=>$model));
	}
	
	/**
	 * Действие просмотра информации по дому в разрезе помещений
	 * @param boolean|string $id Идентификатор дома или false. По умолчанию равно false. Если равно false, будет выбран
	 * первый дом из доступных
	 */
	public function actionPremise($id = false){
		$model = new HousePremiseModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest){
			$id_houses = array();
			$roles_component = Yii::app()->user->rolesComponent;
			
			if ($accounts = Yii::app()->user->accounts)
				foreach ($accounts as $bss_role=>$accounts_role)
					if ($bss_role == 'bss2_payer'){
						$component = Arrays::pop($roles_component, $bss_role);
						
						foreach ($accounts_role as $account)
							$id_houses[Arrays::pop($account, 'id_house')] = array('description'=>Swamper::address($account),
								'component'=>$component);
					}
			
			if (Yii::app()->user->checkAccessSet('developer, administrator, clerk, specialist') && $id &&
				!isset($id_houses[$id])){
					$house = Yii::app()->db->createCommand()->
						select(array('address'))->
						from('{{fias_address}}')->
						where("id_bss = '$id'")->
						queryScalar();
					
					if ($house)
						$id_houses[$id] = array('description'=>$house, 'component'=>'bss2');
				}
			
			if (!$id_houses)
				$id = false;
			elseif (!isset($id_houses[$id]))
				$id = reset(array_keys($id_houses));
			
			Yii::app()->user->setState(WebUser::PREFIX_SET.'id_houses', $id_houses);
			$this->render('premise', array('model'=>$model, 'id_houses'=>Arrays::focussing($id_houses), 'id'=>$id));
		}else
			$this->renderPartial('premiseGrid', array('model'=>$model));
	}
	
}
