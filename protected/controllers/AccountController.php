<?php
/**
 * Контроллер работы с лицевыми счетами пользователя
 * @author v.zubov
 */
class AccountController extends Controller{
	
	/**
	 * @var string Шаблон строки запроса на оплату задолежнности по котловому лицевому счёту
	 * 
	 * 	land_in:
	 * 		frame - frame
	 *	 	frame_search - frame with links
	 *
	 *	def_pay_type:
	 *		card - банковская карта
	 *		yd - яндекс деньги
	 *		sms - с баланса смс
	 *		wallet - кошелек ЦК
	 *
	 *	hide_choose_pay_type:
	 *		true - hide choose payment type panel
	 *		false
	 *
	 *	hide_extra:
	 *		true - hide right panel
	 *		false
	 */
	const PATTERN_PAYMENT_URL = 'https://ckassa.ru/payment/?land_in=frame&hide_extra=true&def_pay_type=%s&'.
		'hide_choose_pay_type=true#!search_provider/pt_search/%s/pay&amount=%s&Л/СЧЕТ=%s&ФИО=%s';
	
	/**
	 * @var array Ассоциативный массив кодов оплаты в формате {Код вида лицевого счёта}=>{Код оплаты}
	 */
	const PAYMENT_CODE = array(1=>'100-11042-1', 2=>'100-11042-2');
	
	/**
	 * @var array Ассоциативный массив коммиссионных отчислений в формате
	 * 	array(
	 * 		{Символьный идентификатор типа оплаты}=>array(
	 * 			{Цифровой идентификатор типа ЛС}=>array(
	 * 				{Цифровой идентификатор вида ЛС}=>{Процент коммиссионного сбора},
	 * 				...
	 * 			),
	 * 			...
	 *		),
	 * 		...
	 * 	)
	 */
	const PAYMENT_COMMISSION = array(
		'card'=>array(2=>array(1=>1.5, 2=>1.5)),
		'yd'=>array(2=>array(1=>6, 2=>5.8)),
		'sms'=>array(2=>array(1=>1.5, 2=>1.5))
	);
	
	/**
	 * @var string Путь к папке с квитанциями
	 */
	const PATH_RECEIPT = 'C:/inetpub/receipts/';
	
	/**
	 * Действие просмотра баланса лицевых счетов пользователя
	 */
	public function actionIndex(){
		$model = new AccountModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest){
			$filter_id = array();
			
			if ($accounts = Yii::app()->user->accounts){
				$counters = Yii::app()->user->counters;
				
				foreach ($accounts as $bss_role=>$accounts_role)
					foreach ($accounts_role as $id_account=>$account)
						if (!Arrays::pop($counters, "$bss_role.$id_account") && ($id_conjugation = Arrays::pop($account, 'id_conjugation')))
							$filter_id[] = $id_conjugation;
			}
			
			$this->render('index', array('model'=>$model, 'filter_id'=>$filter_id));
		}else
			$this->renderPartial('indexGrid', array('model'=>$model));
	}
	
	/**
	 * Действие просмотра операций по лицевому счёту пользователя
	 * @param boolean|string $id Идентификатор сопряжения или false. По умолчанию равно false. Если равно false, будет выбран
	 * первый идентификатор из доступных
	 */
	public function actionOperation($id = false){
		$model = new AccountOperationModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest){
			$id_accounts = array();
			
			if (!$accounts = Yii::app()->user->accounts)
				throw new CHttpException(500);
			
			$roles_component = Yii::app()->user->rolesComponent;
			
			foreach ($accounts as $bss_role=>$accounts_role){
				$component = Arrays::pop($roles_component, $bss_role);
				
				foreach ($accounts_role as $id_account=>$account)
					$id_accounts[$account['id_conjugation']] = array('account'=>$account['id'], 'description'=>$id_account.' - '.
						Yii::app()->conformity->get('account_type', 'description', $component).' - '.Swamper::address($account, true),
						'component'=>$component);
			}
			
			if (!$id_accounts)
				throw new CHttpException(500);
			
			if (!isset($id_accounts[$id]))
				$id = reset(array_keys($id_accounts));
			
			Yii::app()->user->setState(WebUser::PREFIX_SET.'id_accounts', $id_accounts);
			$this->render('operation', array('model'=>$model, 'id_accounts'=>Arrays::focussing($id_accounts), 'id'=>$id));
		}else
			$this->renderPartial('operationGrid', array('model'=>$model));
	}
	
	/**
	 * Действие выбора способа оплаты по лицевому счёту пользователя
	 * @param boolean|string $id Идентификатор сопряжения или false. По умолчанию равно false. Если равно false, будет выбран
	 * первый идентификатор из доступных
	 */
	public function actionPayment($id = false){
		$model = new AccountPaymentModel;
		
		if (!$model->access ||
			(!$code_types = Arrays::select($model->accessValues['type'], 'name', false, false, 'update', 'access')))
				throw new CHttpException(500);
		
		if (!$model->post){
			if (!$accounts = Yii::app()->user->accounts)
				throw new CHttpException(500);
			
			$account_info = $account_repair = array();
			$roles_component = Yii::app()->user->rolesComponent;
			
			foreach ($accounts as $bss_role=>$accounts_role){
				$component = Arrays::pop($roles_component, $bss_role);
				
				foreach ($accounts_role as $id_account=>$account){
					$account_info[$account['id_conjugation']] = array(
						'account'=>$account['id'],
						'description'=>$id_account.' - '.Yii::app()->conformity->get('account_type', 'description', $component).' - '.
							Swamper::address($account, true),
						'type'=>Arrays::pop($account, 'type'),
						'kind'=>Arrays::pop($account, 'kind'),
						'bank'=>Arrays::pop($account, 'bank'),
						'sum'=>$sum = $account['balance'] < 0 ? -$account['balance'] : 0,
						'recipient'=>Arrays::pop($account, 'recipient'),
						'inn'=>Arrays::pop($account, 'inn'),
						'kpp'=>Arrays::pop($account, 'kpp'),
						'bik'=>Arrays::pop($account, 'bik'),
						'bank_account'=>Arrays::pop($account, 'bank_account'),
						'corr_account'=>Arrays::pop($account, 'corr_account')
					);
					
					/* if ($component == 'bss2')
						$account_repair[] = $account['id_conjugation']; */
				}
			}
			
			if (!$account_info)
				throw new CHttpException(500);
			
// 			Yii::app()->user->setState(WebUser::PREFIX_SET.'account_info', $account_info);
			
			if (!isset($account_info[$id]))
				$id = reset(array_keys($account_info));
			
			$model->id = $id;
			$model->type = reset($code_types);
			$model->sum = $account_info[$id]['sum'];
		}else{
			/* if (!$account_info = Yii::app()->user->getState(WebUser::PREFIX_SET.'account_info'))
				throw new CHttpException(500);
			
			$model->attributes = $params_fix = $model->post;
			
			try{
				if (!isset($account_info[$model->id]))
					$model->id = $params_fix['correct_id'] = reset($account_info);
				
				$id = $model->id;
				
				if (!$model->validate()){
					$params_fix['errors'] = $model->errors;
					
					if (isset($params_fix['errors']['sum']))
						$model->sum = $params_fix['correct_sum'] = $account_info[$id]['sum'];
					
					if (isset($params_fix['errors']['type']))
						$model->type = $params_fix['correct_type'] = reset($code_types);
				}
				
				if (!in_array($accounts[$id]['type'], array_keys(static::PAYMENT_CODE)))
					throw new CHttpException(500, 'error type');
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('account_payment', $params_fix, $ok ? 'ok' : 'account_payment');
			
			if ($ok){
				$url = sprintf(static::PATTERN_PAYMENT_URL, static::PAYMENT_CODE[$accounts[$id]['type']],
					(float)$model->sum * 100, $account_info[$model->id]['account']);
				$this->redirect($url);
			} */
		}
		
		$this->render('payment', array(
			'model'=>$model,
			'account_info'=>$account_info,
			'id'=>$id,
			'print_url'=>$this->createUrl('account/receipt'),
			'bilsys_url'=>static::PATTERN_PAYMENT_URL,
			'payment_code'=>static::PAYMENT_CODE,
			'payment_commission'=>static::PAYMENT_COMMISSION,
			'payer'=>Strings::userName(Yii::app()->user->model, Strings::FORMAT_USER_NAME_FULL)
		));
	}
	
	/**
	 * Действие просмотра квитанции на оплату по данному лицевому счёту
	 * @param string $id Идентификатор сопряжения
	 * @param boolean|integer $file Код типа запроса файла квитанции. По умолчанию равно false. Если равно 0, вернётся файл для
	 * скачивания. Если равно 1, вернётся файл для встраивания в страницу. Если равно 2, файл будет отправлен на почту
	 * @throws CHttpException
	 */
	public function actionReceipt($id, $file = false){
		$params_fix = array('id_conjugation'=>$id, 'file'=>$file !== false, 'embed'=>$file);
		
		try{
			if (!$id)
				throw new CHttpException(500, 'id_conjugation empty');
			
			if (!$accounts = Yii::app()->user->accounts)
				throw new CHttpException(500, 'list accounts empty');
			
			foreach ($accounts as $bss_role=>$accounts_role)
				foreach ($accounts_role as $account)
					if ($account['id_conjugation'] == $id){
						$params_fix['id_account'] = $id_account = $account['id'];
						break 2;
					}
			
			if (empty($id_account))
				throw new CHttpException(403, 'account access denied');
			
			$params_fix['path'] = $path = static::PATH_RECEIPT.mb_substr($id_account, 0, 5).DIRECTORY_SEPARATOR.
				($file_name = $id_account.'.pdf');
			
			if (!is_file($path))
				throw new CHttpException(404, 'file not found');
		}catch (CHttpException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('receipt', $params_fix, $ok ? 'ok' : ($e->statusCode == 403 ?
			($params_fix['embed'] ? 'nm_access' : 'receipt_access') : ($e->statusCode == 404 ?
			($params_fix['embed'] ? 'nm_not_found' : 'receipt_not_found') : ($params_fix['embed'] ? 'nm_application' : 'receipt'))));
		
		if (!$params_fix['file'])
			$this->render('receipt', array('id'=>$ok ? $id : false));
		elseif (!$ok)
			throw new CHttpException(500);
		elseif ($params_fix['embed'] === '2')
			echo !Yii::app()->email->send(Yii::app()->user->model->email, 'receipt', array(
				'account'=>$account['id'],
				'address'=>$account['address'],
				'link'=>$this->createAbsoluteUrl('', array('id'=>$id)),
				'attachment'=>$path
			));
		elseif (!Files::upload($path, false, $params_fix['embed']))
			throw new CHttpException(500);
	}
	
	/**
	 * Действие подключения лицевого счёта
	 */
	public function actionConnection(){
		$model = new AccountConnectionModel();
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->bss1->on)
			$model->setAccessValue('type', 'bss1');
		
		if (!Yii::app()->bss2->on)
			$model->setAccessValue('type', 'bss2');
		
		if ((!$types = $model->accessValues['type']) || (!$codes = Arrays::select($types, 'code', false, false, 'update', 'access')))
			throw new CHttpException(500);
		
		if (!$model->post){
			$model->type = $model->executor = reset($codes);
			
			$model->payer = Strings::userName(Yii::app()->user->model);
		}else{
			$model->attributes = $model->post;
			$model->executor = $model->type;
			$params_fix = array('account'=>$model->account,
				'type'=>Yii::app()->conformity->get('account_type', 'name', (int)$model->type));
			
			try{
				if (!$model->validate())
					throw new CHttpException(500, 'validation');
				
				$transaction = Yii::app()->db->beginTransaction();
				
				if (!Yii::app()->user->assign($params_fix['type'].'_payer', false, $params_fix['account']))
					throw new CHttpException(500, 'error assign role');
				
				$transaction->commit();
			}catch (CException $e){
				if (!empty($transaction))
					$transaction->rollback();
				
				$params_fix['msg'] = $e->getMessage();
				
				if ($params_fix['msg'] == 'validation')
					$params_fix['errors'] = $model->errors;
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('account_connection', $params_fix, $ok ? 'ok' : 'account_connection');
			
			if ($ok)
				$this->redirect($this->createUrl('account/index'));
		}
		
		$this->render('connection', array('model'=>$model));
	}
	
	/**
	 * Действие отключения лицевого счёта
	 * @param string $id Идентификатор сопряжения
	 */
	public function actionDisconnection($id){
		$model = UserConjugationModel::model()->findByPk($id);
		
		Yii::app()->log->fix('model_search', array('model'=>'user_conjugation', 'id'=>$id), $model ? 'ok' : 'model_search');
		
		if (!$model || !$model->access)
			throw new CHttpException(500);
		
		$params_fix = array('id'=>$id);
		
		try{
			$transaction = Yii::app()->db->beginTransaction();
			
			if (!$role = Yii::app()->user->getRoles(0, 'name', $model->id_role, false, false, 'id'))
				throw new CHttpException(500, 'role not found');
			
			if (Yii::app()->user->revoke($role, false, $model->id_external) === false)
				throw new CHttpException(500, 'error revoke role');
			
			$transaction->commit();
		}catch (CException $e){
			$transaction->rollback();
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('account_disconnection', $params_fix, $ok ? 'ok' :
			(empty($role) ? 'application' : 'account_disconnection'));
		
		$this->redirect($this->createUrl('account/index'));
	}
	
}
