<?php
/**
 * Основной контроллер приложения
 * @author v.zubov
 */
class MainController extends Controller{
	
	/**
	 * @var array Ассоцитивный массив идентификаторов задач приложения. Задаётся в формате:
	 * 	array(
	 * 		{Цифровой идентификатор задачи}=>{Название метода данного контроллера (без префикса), отвечающего за выполнение задачи},
	 * 		...
	 * 	)
	 */
	const TASK_ACTIONS = array(
		'9482028490'=>'BackupDb',
		'4038500321'=>'BackupLog',
		'4320103383'=>'BackupAppeal',
		'2382947104'=>'BackupUser',
		'9584733545'=>'BackupExport',
		'4302285554'=>'Cleaning'
	);
	
	/**
	 * @var string Сетевой путь к папке хранения архивных данных
	 */
	const DIR_EXTERNAL_BACKUP = DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR.'192.168.60.28'.DIRECTORY_SEPARATOR.'backups'.
		DIRECTORY_SEPARATOR.'lk';
	
	/**
	 * @var string Имя файла с информацией об архивируемом обращении
	 */
	const FN_APPEAL_INFO = '_info.txt';
	
	/**
	 * @var integer Срок хранения логов, задаётся в днях
	 */
	const TIMELIMIT_LOG = 6 * 30;
	
	/**
	 * @var integer Срок хранения обращений, задаётся в днях
	 */
	const TIMELIMIT_APPEAL = 6 * 30;
	
	/**
	 * @var integer Срок хранения неактивных профилей пользователей, задаётся в днях
	 */
	const TIMELIMIT_USER = 3 * 30;
	
	/**
	 * @var integer Срок хранения файлов выгрузок данных в РГИС, задаётся в днях
	 */
	const TIMELIMIT_EXPORT = 6 * 30;
	
	/**
	 * @var integer Срок хранения временных пользовательских файлов, задаётся в секундах
	 */
	const TIMELIMIT_TEMP = 24 * 60 * 60;
	
	/**
	 * Главное действие
	 */
	public function actionIndex(){
		$model = new NewsModel('list');
		
		$this->render('index', array('model'=>$model->access ? $model : false));
	}
	
	/**
	 * Действие приватного входа
	 */
	public function actionPrivate(){
		$this->actionIndex();
	}
	
	/**
	 * Действие отображения критических ошибок приложения
	 * @throws CHttpException
	 */
	public function actionError(){
		if ($error = Yii::app()->errorHandler->error)
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else{
				if ($message = $error['message']){
					if (mb_strpos($message, 'nm_') === 0)
						Yii::app()->end();
					
					$this->schemeMode = self::SCHEME_MODE_SAFE;
				}else{
					$this->schemeMode = self::SCHEME_MODE_DISABLED;
					$message = Yii::app()->errorEvent->get($error['code'], 'description');
				}
				
				if ($message)
					Yii::app()->user->setFlash('error', $message);
				
				$this->renderText(null);
			}
		else
			throw new CHttpException(500, 'Application error');
	}
	
	/**
	 * Действие выполнения запланированных заданий приложения
	 * @param string $id Идентификатор задачи
	 */
	public function actionTask($id){
		$params_fix = array('id'=>$id, 'task'=>$id);
		
		try{
			if (Html::getUserAddr() !== Html::IP_LOCALHOST)
				throw new CHttpException(403, 'access denied');
			
			if (!array_key_exists($id, static::TASK_ACTIONS))
				throw new CHttpException(404, 'id task not found');
			
			$method_name = $params_fix['method_name'] = 'task'.($params_fix['task'] = static::TASK_ACTIONS[$id]);
			
			if (!method_exists($this, $method_name))
				throw new CHttpException(404, 'method task not found');
			
			if (!$this->$method_name())
				throw new CHttpException(500, 'error executing task');
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('task', $params_fix, $ok ? 'ok' : 'task', isset($params_fix['method_name']) ? 0 : false);
		
		Yii::app()->user->flashes;
		
		if (!$ok && $this->deployed && Yii::app()->email->on &&
			($user = UserModel::model()->findByPk(1)) && ($email = $user->email))
				Yii::app()->email->send($email, 'report', array('task'=>$params_fix['task']));
	}
	
	/**
	 * Действие запроса подтверждений пользователя
	 * @throws CHttpException
	 */
	public function actionConfirm(){
		if (!$confirm = Yii::app()->user->getState('confirm'))
			throw new CHttpException(500);
		
		$type = $confirm['type'];
		$model = new ConfirmModel('update');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (Yii::app()->request->requestType !== 'POST'){
			$model->agreement = true;
			$model->secure = true;
			
			$this->renderPartial('confirm', array('model'=>$model, 'type'=>$type));
		}else{
			$model->attributes = $model->post;
			$params_fix = array('type'=>$type, 'model'=>$model->attributes);
			
			try{
				$cancel = $model->command === 'juid_confirm/cancel';
				$user = UserModel::model()->findByPk($confirm['id']);
				
				Yii::app()->log->fix('model_search', array('model'=>'user', 'id'=>$confirm['id']), $user ? 'ok' : 'model_search');
				
				$params_fix['user'] = Arrays::filterKey($user->attributes, 'password', false);
				$state = Yii::app()->conformity->get('user_state', 'name', $user->state);
				
				if (($type === WebUser::CONFIRM_TYPE_AGREEMENT && $state !== 'confirm_data') ||
					($type === WebUser::CONFIRM_TYPE_SECURE && $state !== 'working'))
						throw new CHttpException(403, 'incorrect user state');
				
				if (!$cancel)
					if ($type === WebUser::CONFIRM_TYPE_AGREEMENT){
						if (!$model->agreement)
							$cancel = true;
						else{
							$user->state = Yii::app()->conformity->get('user_state', 'code', 'working');
							$user->agreement = true;
							
							if (!$user->save(false))
								throw new CHttpException(500, 'error model save');
						}
					}elseif (!$model->secure)
						$cancel = true;
					elseif ($model->remember){
						$user->secure = false;
						
						if (!$user->save(false))
							throw new CHttpException(500, 'error model save');
					}
				
				if ($cancel)
					throw new CHttpException(500, 'cancel');
			}catch (CHttpException $e){
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			if ($ok || $cancel)
				Yii::app()->user->setState('confirm', null);
			
			Yii::app()->log->fix('user_confirm', $params_fix, $ok ? 'ok' : ($e->statusCode == 404 ? 'model_search' : 'user_confirm'));
			
			if (!$ok){
				if ($params_fix['msg'] === 'cancel' && is_int(Yii::app()->user->getState('esia')))
					Yii::app()->controller->redirect(Yii::app()->controller->createUrl('esia/logout'));
				
				$this->redirect($this->createUrl(Yii::app()->defaultController));
			}else{
				$login = $type === WebUser::CONFIRM_TYPE_AGREEMENT ? Yii::app()->user->checkLogin($user) :
					Yii::app()->user->login($user);
				
				$this->redirect(Yii::app()->user->getHomeUrl($login ? $user->id : false));
			}
		}
	}
	
	/**
	 * Действие проверки доступности поддомена с сертификатом повышенной безопасности
	 * @param string $id Признак доступности
	 */
	public function actionSecure($id = false){
		if (isset($_GET['callback']))
			Yii::app()->end();
		
		if ($this->working && Html::SECURE_HOST && Yii::app()->user->isGuest && Html::secureCheck() === null &&
			((!$state = Yii::app()->user->getState('confirm')) || $state['type'] !== WebUser::CONFIRM_TYPE_SECURE))
				Yii::app()->request->cookies->add('secure', new CHttpCookie('secure', $id));
	}
	
	/**
	 * Задача по резервному копированию Базы данных приложения
	 * @return boolean Успешность выполнения
	 */
	protected function taskBackupDb(){
		try{
			$this->createAction('reserve')->run();
		}catch (CException $e){
		}
		
		return !isset($e);
	}
	
	/**
	 * Задача по резервному копированию логов приложения
	 * @return boolean Успешность выполнения
	 */
	protected function taskBackupLog(){
		Swamper::timer($tick);
		$params_fix = array('rows'=>0, 'files'=>$files = array());
		set_time_limit(0);
		
		try{
			$transaction = Yii::app()->db->beginTransaction();
			$timeout = mktime(0, 0, 0, date('n'), date('j') - static::TIMELIMIT_LOG);
			$path = $params_fix['path'] = Yii::getPathOfAlias(Log::DIR_LOG);
			$params_fix['path_archive'] = Yii::getPathOfAlias(ExportController::ALIAS_EXPORT).DIRECTORY_SEPARATOR.
				Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM_WITHOUT_TIME).DIRECTORY_SEPARATOR.'log';
			$params_sql = array('condition'=>'date <= :date',
				'params'=>array(':date'=>Swamper::date($timeout, Swamper::FORMAT_DATE_SYSTEM)));
			
			if ($files_find = glob($path.DIRECTORY_SEPARATOR.'*-*-*.txt')){
				foreach ($files_find as $file_find){
					$file = pathinfo($file_find, PATHINFO_FILENAME);
					
					if (strtotime($file) <= $timeout)
						$files[] = $file;
				}
				
				$params_fix['files'] = $files;
			}
			
			if ($rows = LogModel::model()->findAll(array_merge($params_sql, array('order'=>'id')))){
				$params_fix['rows'] = count($rows);
				$day = $action = false;
				
				foreach ($rows as $row){
					if (($day_current = mb_substr($row->date, 0, 10)) !== $day){
						$action = in_array($day_current, $params_fix['files']) ? 'skip' : 'forming';
						$day = $day_current;
						
						if ($action == 'forming')
							$files[] = $params_fix['files_form'][] = $day_current;
					}
					
					if ($action == 'forming' && !Yii::app()->log->writeToFile($row->id_event, $row->id_error, $row->date,
						json_decode($row->params, true), $row->id_user))
							throw new CHttpException(500, 'error file forming');
				}
				
				if (!LogModel::model()->deleteAll($params_sql))
					throw new CHttpException(500, 'error rows delete');
			}
			
			if ($files){
				if (!is_dir($params_fix['path_archive']) && !mkdir($params_fix['path_archive'], 0777, true))
					throw new CHttpException(500, 'error create path archive');
				
				foreach ($files as $file)
					if (!rename($path.DIRECTORY_SEPARATOR.$file.'.txt', $params_fix['path_archive'].DIRECTORY_SEPARATOR.$file.'.txt'))
						throw new CHttpException(500, 'error move file');
			}
			
			$params_fix['timer'] = Swamper::timer($tick);
			$transaction->commit();
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
			$transaction->rollback();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('backup_log', $params_fix, $ok ? 'ok' : 'backup_log');
		
		return $ok;
	}
	
	/**
	 * Задача по резервному копированию обращений граждан
	 * @return boolean Успешность выполнения
	 */
	protected function taskBackupAppeal(){
		Swamper::timer($tick);
		$params_fix = array('qu'=>0, 'files'=>array());
		set_time_limit(0);
		
		try{
			$transaction = Yii::app()->db->beginTransaction();
			$params_fix['path_archive'] = Yii::getPathOfAlias(ExportController::ALIAS_EXPORT).DIRECTORY_SEPARATOR.
				Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM_WITHOUT_TIME).DIRECTORY_SEPARATOR.'appeal';
			$states = Yii::app()->conformity->get('appeal_stage_state');
			
			$id_appeals = Yii::app()->db->createCommand()->
				select('id_appeal')->
				from('{{appeal_stage}}')->
				where(array('and',
					"date <= '".Swamper::date(mktime(0, 0, 0, date('n'), date('j') - static::TIMELIMIT_APPEAL),
						Swamper::FORMAT_DATE_SYSTEM)."'",
					array('in', 'state', Arrays::select($states, 'code', false, false, 'revoked, completed, denied'))
				))->
				order('id_appeal')->
				queryColumn();
			
			if ($id_appeals){
				$params_fix['qu'] = count($id_appeals);
				
				$rows = Yii::app()->db->createCommand()->
					select(array('a.id id_appeal', 'a.theme', 'a.themeid', 'a.address', 'a.addressid', 'a.responder', 'a.category',
						'a.type', 'a.timeout', 's.id id_stage', 's.id_owner', 's.text', 's.state', 's.date', 'u.surname', 'u.name',
						'u.patronymic', 'u.title', 'f.id id_file', 'f.name fname'))->
					from('{{appeal}} a')->
					leftJoin('{{appeal_stage}} s', '"s"."id_appeal" = "a"."id"')->
					leftJoin('{{user}} u', '"u"."id" = "s"."id_owner"')->
					leftJoin('{{file}} f', '"f"."type" = '.Yii::app()->conformity->get('file_type', 'code', 'appeal').
						' AND "f"."source" = "s"."id"')->
					where('"a"."id" IN ('.implode(', ', $id_appeals).')')->
					order('a.id, s.id, f.id')->
					queryAll();
				
				if (!$rows)
					throw new CHttpException(500, 'error appeal id');
				
				if (!is_dir($params_fix['path_archive']) && !mkdir($params_fix['path_archive'], 0777, true))
					throw new CHttpException(500, 'error create path archive');
				
				$id_appeal = false;
				$responders = Yii::app()->conformity->get('appeal_responder', 'description', 'code');
				$categoryes = Yii::app()->conformity->get('appeal_category', 'description', 'code');
				$types = Yii::app()->conformity->get('appeal_type', 'description', 'code');
				$state_requested = Arrays::select($states, 'code', 'requested');
				$dir_file_temp = Yii::app()->file->getDir('appeal').File::DIR_TEMP.DIRECTORY_SEPARATOR;
				
				while ((list($key, $row) = each($rows)) || $id_appeal){
					if (!$row || ($id_appeal && $id_appeal != $row['id_appeal'])){
						$info = 
							'Обращение № '.$id_appeal.PHP_EOL.PHP_EOL.
							'Тема: '.$appeal['theme'].' ['.$appeal['themeid'].']'.PHP_EOL.
							'Автор: '.$author.' ['.$appeal['id_owner'].']'.PHP_EOL.
							'Адрес: '.$appeal['address'].' ['.$appeal['addressid'].']'.PHP_EOL.
							'Адресат: '.$responder.' ['.$appeal['responder'].']'.PHP_EOL.
							'Категория: '.Arrays::pop($categoryes, $appeal['category']).' ['.$appeal['category'].']'.PHP_EOL.
							'Тип: '.Arrays::pop($types, $appeal['type']).' ['.$appeal['type'].']'.PHP_EOL.
							'Дата создания: '.$appeal['date'].PHP_EOL.
							'Срок рассмотрения: '.$appeal['timeout'].PHP_EOL.
							$info;
						
						if (!file_put_contents($path_info, $info))
							throw new CHttpException(500, 'error create file info appeal');
						
						$params_fix['files'][$id_appeal] = array_merge(array(static::FN_APPEAL_INFO),
							isset($params_fix['files'][$id_appeal]) ? $params_fix['files'][$id_appeal] : array());
						$id_appeal = false;
					}
					
					if ($row){
						if (!$id_appeal){
							$id_appeal = $row['id_appeal'];
							$appeal = $row;
							$path_appeal = $params_fix['path_archive'].DIRECTORY_SEPARATOR.$id_appeal;
							$path_info = $path_appeal.DIRECTORY_SEPARATOR.static::FN_APPEAL_INFO;
							
							if (!is_dir($path_appeal) && !mkdir($path_appeal))
								throw new CHttpException(500, 'error create path appeal');
							
							$id_stage = $author = false;
							$info = '';
							$responder = $row['responder'] > 0 ? Arrays::pop($responders, $row['responder']) : $row['responder'];
						}
						
						if ($id_stage != $row['id_stage']){
							$id_stage = $row['id_stage'];
							$info .= PHP_EOL.
								$row['date'].' ['.$id_stage.']'.PHP_EOL.
								Arrays::select($states, 'description', $row['state']).' ['.$row['state'].']'.PHP_EOL.
								($owner = Strings::userName($row)).' ['.$row['id_owner'].']'.PHP_EOL.
								($row['text'] ? $row['text'].PHP_EOL : '');
							
							if ($author === false)
								$author = $owner;
							
							if ($responder < 0 && $row['id_owner'] == -$responder)
								$responder = $owner;
						}
						
						if ($row['id_file']){
							if (!$temp_id = Yii::app()->file->delete($row['id_file'], false, 'appeal'))
								throw new CHttpException(500, 'error file "'.$row['id_file'].'" delete');
							
							$params_fix['files_delete'][$row['id_file']] = $path_file = $dir_file_temp.$temp_id;
							$info .= ($fname = Files::getUniqueFileName($path_appeal, $row['fname'])).' ['.$row['id_file'].']'.PHP_EOL;
							
							if (!rename($path_file, iconv('utf-8', 'cp1251', $path_file = $path_appeal.DIRECTORY_SEPARATOR.$fname)))
								throw new CHttpException(500, 'error file "'.$row['id_file'].'" move');
							
							$params_fix['files_delete'][$row['id_file']] = $path_file;
							$params_fix['files'][$id_appeal][] = $fname;
						}
					}
				}
				
				if (!AppealModel::model()->deleteAll('id IN ('.implode(', ', $id_appeals).')'))
					throw new CHttpException(500, 'error delete appeals');
			}
			
			$params_fix['timer'] = Swamper::timer($tick);
			$transaction->commit();
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
			$transaction->rollback();
			
			if (!empty($params_fix['files_delete'])){
				$params_fix['files_delete_return'] = true;
				$dir_file = Yii::app()->file->getDir('appeal');
				
				foreach ($params_fix['files_delete'] as $id_file=>$path_file)
					if (!rename(iconv('utf-8', 'cp1251', $path_file), $dir_file.$id_file))
						$params_fix['files_delete_return'] = false;
			}
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('backup_appeal', $params_fix, $ok ? 'ok' : 'backup_appeal');
		
		return $ok;
	}
	
	/**
	 * Задача по резервному копированию профилей пользователей
	 * @return boolean Успешность выполнения
	 */
	protected function taskBackupUser(){
		Swamper::timer($tick);
		$params_fix = array('qu'=>0, 'id'=>array());
		set_time_limit(0);
		
		try{
			$params_fix['path_archive'] = Yii::getPathOfAlias(ExportController::ALIAS_EXPORT).DIRECTORY_SEPARATOR.
				Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM_WITHOUT_TIME).DIRECTORY_SEPARATOR.'user';
			$states = Yii::app()->conformity->get('user_state');
			
			$users = Yii::app()->db->createCommand()->
				select(array('u.id', 'u.login', 'u.state', 'u.surname', 'u.name', 'u.patronymic', 'u.title', 'u.address', 'u.addressid',
					'u.email', 'u.phone', 'u.snils', 'u.registration', 'u.locked', 'u.delivery', 'u.agreement', 'u.secure',
					'STRING_AGG("i"."description", \', \' ORDER BY "i"."id") AS roles'))->
				from('{{user}} u')->
				leftJoin('{{appeal_stage}} s', '"s"."id_owner" = "u"."id"')->
				leftJoin('{{log}} l', '"l"."id_user" = "u"."id"')->
				leftJoin('{{auth_assignment}} a', '"a"."userid" = "u"."id"')->
				leftJoin('{{auth_item}} i', '"i"."name" = "a"."itemname"')->
				where(array('and',
					'"u"."registration" <= \''.Swamper::date(mktime(0, 0, 0, date('n'), date('j') - static::TIMELIMIT_USER),
						Swamper::FORMAT_DATE_SYSTEM)."'",
					array('in', 'u.state', Arrays::select($states, 'code', false, 'working, locked')),
					'"s"."id" IS NULL',
					'"l"."id" IS NULL'
				))->
				group('u.id')->
				order('u.id')->
				queryAll();
			
			if ($users){
				$params_fix['qu'] = count($users);
				$states_delivery = Yii::app()->conformity->get('user_delivery_type', 'description', 'code');
				
				if (!is_dir($params_fix['path_archive']) && !mkdir($params_fix['path_archive'], 0777, true))
					throw new CHttpException(500, 'error create path archive');
				
				foreach ($users as $user){
					$name = Strings::userName($user);
					
					if (!$name)
						$name = 'noname';
					
					$info =
						$name.' ['.$user['id'].']'.PHP_EOL.PHP_EOL.
						'Группа: '.$user['roles'].PHP_EOL.
						'Логин: '.$user['login'].PHP_EOL.
						'Состояние: '.Arrays::select($states, 'description', $user['state']).' ['.$user['state'].']'.PHP_EOL.
						'Фамилия: '.$user['surname'].PHP_EOL.
						'Имя: '.$user['name'].PHP_EOL.
						'Отчество: '.$user['patronymic'].PHP_EOL.
						'Организация: '.$user['title'].PHP_EOL.
						'Адрес: '.$user['address'].($user['addressid'] ? ' ['.$user['addressid'].']' : '').PHP_EOL.
						'E-mail: '.$user['email'].PHP_EOL.
						'Телефон: '.$user['phone'].PHP_EOL.
						'СНИЛС: '.$user['snils'].PHP_EOL.
						'Дата регистрации: '.$user['registration'].PHP_EOL.
						'Дата удаления: '.$user['locked'].PHP_EOL.
						'Способ доставки уведомлений: '.$states_delivery[$user['delivery']].' ['.$user['delivery'].']'.PHP_EOL.
						'Согласие с условиями Пользовательского соглашения: '.($user['agreement'] ? 'Да' : 'Нет').PHP_EOL.
						'Согласие на работу по протоколу с пониженным уровнем безопасности: '.($user['secure'] ? 'Нет' : 'Да').PHP_EOL;
					
					if (!file_put_contents($params_fix['path_archive'].DIRECTORY_SEPARATOR.$user['id'].' - '.
						iconv('utf-8', 'cp1251', str_replace(array('\'', '"', '«', '»'), '', $name)).'.txt', $info))
							throw new CHttpException(500, 'error create file');
					
					$params_fix['id'][] = $user['id'];
				}
				
				if (!UserModel::model()->deleteAll('id IN ('.implode(', ', $params_fix['id']).')'))
					throw new CHttpException(500, 'error delete users');
			}
			
			$params_fix['timer'] = Swamper::timer($tick);
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('backup_user', $params_fix, $ok ? 'ok' : 'backup_user');
		
		return $ok;
	}
	
	/**
	 * Задача по экспорту резервных копий приложения в сетевую папку
	 * @return boolean Успешность выполнения
	 */
	protected function taskBackupExport(){
		Swamper::timer($tick);
		$params_fix = array('files'=>array());
		set_time_limit(0);
		
		try{
			$path = $params_fix['path'] = Yii::getPathOfAlias(ExportController::ALIAS_EXPORT).DIRECTORY_SEPARATOR.
				($date = Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM_WITHOUT_TIME));
			$params_fix['path_external'] = static::DIR_EXTERNAL_BACKUP;
			$path_len = mb_strlen($path) + 1;
			
			if (!extension_loaded('zip'))
				throw new CHttpException(500, 'zip extension not load');
			
			if (!is_dir($path))
				throw new CHttpException(500, 'dir not found');
			
			if (!$files = glob($path.DIRECTORY_SEPARATOR.'*'.DIRECTORY_SEPARATOR.
				'*{'.DIRECTORY_SEPARATOR.'*,}', GLOB_BRACE))
					throw new CHttpException(500, 'dir is empty');
			
			$zip = new ZipArchive();
			
			if (!$zip->open($path_zip = $path.DIRECTORY_SEPARATOR.$date.'.zip', ZIPARCHIVE::CREATE))
				throw new CHttpException(500, 'zip file creation failed');
			
			foreach ($files as $file)
				if (is_file($file)){
					$params_fix['files'][] = iconv('cp1251', 'utf-8', $file);
					
					if (!$zip->addFile($file, iconv('cp1251', 'cp866', ($name = substr($file, $path_len)))))
						throw new CHttpException(500, 'error add file "'.iconv('cp1251', 'utf-8', $name).'" to zip archive');
				}
			
			if (!$zip->close())
				throw new CHttpException(500, 'error zip closing');
			
			if (!rename($path_zip, $params_fix['path_external'].DIRECTORY_SEPARATOR.$date.'.zip'))
// 			if (!rename($path_zip, Yii::getPathOfAlias(Log::DIR_LOG).DIRECTORY_SEPARATOR.$date.'.zip'))
				throw new CHttpException(500, 'error move file');
			
			$params_fix['timer'] = Swamper::timer($tick);
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('backup_export', $params_fix, $ok ? 'ok' : 'backup_export');
		
		if (!empty($path))
			Files::clear($path, true, true);
		
		return $ok;
	}
	
	/**
	 * Задача по чистке данных приложения
	 * @return boolean Успешность выполнения
	 */
	protected function taskCleaning(){
		Swamper::timer($tick);
		$params_fix = array('temp_appeal'=>array(), 'temp_import'=>array(), 'export'=>array(), 'user_code'=>array());
		set_time_limit(0);
		
		try{
			$timeout = ($time = time()) - static::TIMELIMIT_TEMP;
			
			foreach (array('appeal', 'import') as $type){
				$path = Yii::app()->file->getDir($type).File::DIR_TEMP;
				
				if ($files = glob($path.DIRECTORY_SEPARATOR.'*'))
					foreach ($files as $file)
						if (filemtime($file) <= $timeout){
							@unlink($file);
							$params_fix['temp_'.$type][] = $file;
						}
			}
			
			$timeout = Swamper::date($time - static::TIMELIMIT_EXPORT * 24 * 60 * 60, Swamper::FORMAT_DATE_SYSTEM);
			
			if ($files = Yii::app()->file->get(false, Yii::app()->conformity->get('file_type', 'code', 'export')))
				foreach ($files as $file)
					if ($file->date <= $timeout){
 						if (!Yii::app()->file->delete($file->id, null, 'export'))
 							throw new CHttpException(500, 'error file "'.$file->id.'" delete');
						
						$params_fix['export'][] = $file->name;
					}
			
			$params_fix['user_code'] = (int)UserCodeModel::model()->count($params = array(
				'condition'=>'date <= :date',
				'params'=>array(':date'=>Swamper::date(time() - static::TIMELIMIT_TEMP, Swamper::FORMAT_DATE_SYSTEM))
			));
			
			if ($params_fix['user_code'] && !UserCodeModel::model()->deleteAll($params))
				throw new CHttpException(500, 'error delete user_code');
			
			Files::clear(Yii::getPathOfAlias(ExportController::ALIAS_EXPORT), true, true);
			Files::clear(Yii::getPathOfAlias(ImportController::ALIAS_IMPORT), true, true);
			Files::clear(Yii::getPathOfAlias(ReserveController::ALIAS_RESERVE), true, true);
			
			$params_fix['timer'] = Swamper::timer($tick);
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('cleaning', $params_fix, $ok ? 'ok' : 'cleaning');
		
		return $ok;
	}
	
}
