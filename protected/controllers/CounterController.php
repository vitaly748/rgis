<?php
/**
 * Контроллер работы с счётчиками
 * @author v.zubov
 */
class CounterController extends Controller{
	
	/**
	 * Действие просмотра информации по счётчикам
	 * @param boolean|string $id Идентификатор сопряжения или false. По умолчанию равно false. Если равно false, будет выбран
	 * первый идентификатор из доступных
	 */
	public function actionIndex($id = false){
		$model = new CounterModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest){
			$id_addresses = array();
			
			if (($accounts_counters = Yii::app()->user->counters) && ($accounts = Yii::app()->user->accounts))
				foreach ($accounts_counters as $bss_role=>$accounts_role)
					foreach ($accounts_role as $id_account=>$counters)
						if ($account = Arrays::pop($accounts, "$bss_role.$id_account"))
							$id_addresses[$account['id_conjugation']] = array(
								'description'=>$id_account.' - '.Swamper::address($account, true),
								'counters'=>$counters
							);
			
			if (!$id_addresses)
				$id = false;
			elseif (!isset($id_addresses[$id]))
				$id = reset(array_keys($id_addresses));
			
			Yii::app()->user->setState(WebUser::PREFIX_SET.'id_addresses', $id_addresses);
			$this->render('index', array('model'=>$model, 'id_addresses'=>Arrays::focussing($id_addresses), 'id'=>$id));
		}else
			$this->renderPartial('indexGrid', array('model'=>$model));
	}
	
	/**
	 * Действие подачи нового показания счётчика
	 */
	public function actionReading(){
		$model = new CounterModel('update');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!$model->post)
			$this->renderPartial('reading', array('model'=>$model));
		else{
			$model->attributes = $model->post;
			$params_fix = array('id_counter'=>$model->id, 'new'=>$model->new);
			
			try{
				if (!$model->validate())
					throw new CHttpException(500, 'validation');
				
				if (!$accounts_counters = Yii::app()->user->counters)
					throw new CHttpException(500, 'empty counters');
				
				foreach ($accounts_counters as $bss_role=>$accounts_role)
					foreach ($accounts_role as $id_account=>$counters)
						if (array_key_exists($model->id, $counters)){
							$params_fix['id_account'] = $id_account;
							$component = Arrays::pop($rc = Yii::app()->user->rolesComponent, $bss_role);
							break 2;
						}
				
				if (empty($params_fix['id_account']))
					throw new CHttpException(500, 'counter not access');
				
				$result = Yii::app()->$component->query('reading_add', array('id'=>$model->id, 'value'=>$model->new));
				
				if (!$result)
					throw new CHttpException(500, 'empty result');
				
				$params_fix['result'] = $result;
				
				if ($result['error']['code'])
					throw new CHttpException(500, 'counter not found');
			}catch (CException $e){
				$params_fix['msg'] = $e->getMessage();
				
				if ($validation = $params_fix['msg'] == 'validation')
					$params_fix['errors'] = $model->errors;
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('counter_reading_add', $params_fix, $ok ? 'ok' : ($validation ? 'counter_reading_add' :
				(empty($params_fix['id_account']) ? 'model_access_ne' : (empty($result) ? 'agent_request_ne' : 'counter_reading_add'))));
			
			if (!empty($validation))
				Yii::app()->user->setFlash('error', reset(reset($params_fix['errors'])));
			
			$this->redirect($this->createUrl('counter/index'));
		}
	}
	
	/**
	 * Действие просмотра истории ввода показаний счётчиков
	 * @param boolean|string $id Идентификатор счётчика или false. По умолчанию равно false. Если равно false, будет выбран
	 * первый счётчик из доступных
	 */
	public function actionHistory($id = false){
		$model = new CounterHistoryModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest){
			$id_counters = array();
			$roles_component = Yii::app()->user->rolesComponent;
			
			if ($accounts_counters = Yii::app()->user->counters)
				foreach ($accounts_counters as $bss_role=>$accounts_role){
					$component = Arrays::pop($roles_component, $bss_role);
					
					foreach ($accounts_role as $id_account=>$counters)
						foreach ($counters as $id_counter=>$counter)
							$id_counters[$id_counter] = array(
								'description'=>$id_account.' - '.Arrays::pop($counter, 'counter'),
								'component'=>$component
							);
				}
			
			if (!$id_counters)
				$id = false;
			elseif (!isset($id_counters[$id]))
				$id = reset(array_keys($id_counters));
			
			Yii::app()->user->setState(WebUser::PREFIX_SET.'id_counters', $id_counters);
			$this->render('history', array('model'=>$model, 'id_counters'=>Arrays::focussing($id_counters), 'id'=>$id));
		}else
			$this->renderPartial('historyGrid', array('model'=>$model));
	}
	
}
