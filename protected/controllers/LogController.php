<?php
/**
 * Контроллер работы с логами
 * @author v.zubov
 */
class LogController extends Controller{
	
	/**
	 * Действие просмотра списка событий
	 */
	public function actionIndex(){
		$model = new LogModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest)
			$this->render('index', array('model'=>$model));
		else
			$this->renderPartial('indexGrid', array('model'=>$model));
	}
	
	/**
	 * Действие просмотра информации по конкретному событию
	 */
	public function actionView($id){
		$model = LogModel::model()->findByPk($id);
		
		Yii::app()->log->fix('model_search', array('model'=>'log', 'id'=>$id), $model ? 'ok' : 'model_search');
		
		if (!$model || !$model->access)
			throw new CHttpException(500);
		
		if ($model->id_user > 0 && ($model_user = UserModel::model()->findByPk($model->id_user)))
			$model->id_user = Html::link(Strings::userName($model_user), $this->createUrl('user/view', array('id'=>$model->id_user)),
				array('target'=>'_blank'));
		elseif ($model->id_user === 0)
			$model->id_user = Yii::app()->label->get('user_system');
		elseif ($model->id_user === null)
			$model->id_user = Yii::app()->authPhp->roles['guest']->description;
		else
			$model->id_user = Yii::app()->label->get('user_stranger');
		
		$model->id_event = ($event = Arrays::select(Yii::app()->event->get(), 'description', false, false,
			(int)$model->id_event, 'code')) ? reset($event) : null;
		$model->id_error = ($error = Arrays::select(Yii::app()->errorEvent->get(), 'description', false, false,
			(int)$model->id_error, 'code')) ? reset($error) : null;
		
		if ($model->params)
			$model->params = json_decode($model->params, true);
		
		$this->render('view', array(
			'model'=>$model,
			'next'=>Yii::app()->db->createCommand()->
				from('{{log}}')->
				where('id > :id', array(':id'=>$id))->
				order('id')->
				queryScalar(),
			'back'=>Yii::app()->db->createCommand()->
				from('{{log}}')->
				where('id < :id', array(':id'=>$id))->
				order('id DESC')->
				queryScalar()
		));
	}
	
}
