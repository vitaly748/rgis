<?php
/**
 * Контроллер работы с экспортом данных
 * @author v.zubov
 */
class ImportController extends Controller{
	
	/**
	 * @var string Псевдоним пути к папке временных файлов, создаваемых при обработке файлов выгрузки РГИС
	 */
	const ALIAS_IMPORT = 'application.runtime.import';
	
	/**
	 * @var string Название поставщика выгрузки РГИС
	 */
	const PROVIDER = 'Министерство социального развития Пермского края';
	
	/**
	 * @var string Префикс выгружаемых файлов выгрузок РГИС
	 */
	const PREFIX_UPLOAD_FILE = 'Минсоц_';
	
	/**
	 * Действия просмотра списка новостей
	 */
	public function actionIndex(){
		if ($_FILES)
			Yii::app()->file->download();
		
		if ($key = Arrays::pop($_POST, ini_get('session.upload_progress.name')))
			Yii::app()->file->check($key);
		
		$model = new ImportModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest)
			$this->render('index', array('model'=>$model));
		else
			$this->renderPartial('indexGrid', array('model'=>$model));
	}
	
	/**
	 * Действие формирования файла выгрузки
	 * @throws CHttpException
	 */
	public function actionImport(){
		$model = new ImportModel('update');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!$model->post)
			$this->renderPartial('import', array('model'=>$model));
		else{
			$model->attributes = $model->post;
			$params_fix = array('file'=>(int)$model->file);
			
			try{
				$transaction = Yii::app()->db->beginTransaction();
				
				if (!$params_fix['file'])
					throw new CHttpException(500, 'id file is empty');
				
				if (!extension_loaded('zip'))
					throw new CHttpException(500, 'zip extension not load');
				
				$path = $params_fix['path'] = Yii::getPathOfAlias(static::ALIAS_IMPORT).DIRECTORY_SEPARATOR.
					Yii::app()->user->id.'-'.time();
				
				if (!is_dir($path) && !mkdir($path, 0777, true))
					throw new CHttpException(500, 'error create path');
				
				if (!rename(Yii::app()->file->getDir().File::DIR_TEMP.DIRECTORY_SEPARATOR.$params_fix['file'],
					$path_zip = $path.DIRECTORY_SEPARATOR.$params_fix['file'].'.zip'))
						throw new CHttpException(500, 'error move file');
				
				$zip = new ZipArchive();
				
				if (!$zip->open($path_zip))
					throw new CHttpException(500, 'zip file opening failed');
				
				if (!$zip->extractTo($path))
					throw new CHttpException(500, 'error extracting zip archive');
				
				if (!$zip->close())
					throw new CHttpException(500, 'error zip closing');
				
				if (!file_exists($path_info = $path.DIRECTORY_SEPARATOR.'_info.csv'))
					throw new CHttpException(500, 'file "_info.csv" not found');
				
				if ((!$content = file($path_info)) || !isset($content[1]))
					throw new CHttpException(500, 'file "_info.csv" is empty');
				
				$content = explode(';', $content[1]);
				
				if (Arrays::pop($content, 0) !== ExportController::VERSION_FORMAT)
					throw new CHttpException(500, 'version format incorrect');
				
				if (iconv('cp1251', 'utf-8', Arrays::pop($content, 4)) !== static::PROVIDER)
					throw new CHttpException(500, 'provider incorrect');
				
				if ((!$year = Arrays::pop($content, 6)) || (!$month = Arrays::pop($content, 7)) || (!$date = Arrays::pop($content, 8)))
					throw new CHttpException(500, 'date incorrect');
				
				$name = $year.'-'.str_pad($month, 2, '0', STR_PAD_LEFT).'.zip';
				
				if (Yii::app()->file->get(false, true, array('name'=>$name)))
					throw new CHttpException(403, 'file already exists');
				
				if (!file_exists($path_filelist = $path.DIRECTORY_SEPARATOR.'_filelist.csv'))
					throw new CHttpException(500, 'file "_filelist.csv" not found');
				
				if (!$content = file($path_filelist))
					throw new CHttpException(500, 'file "_filelist.csv" is empty');
				
				$files = array();
				
				foreach ($content as $line){
					$line = explode(';', $line);
					
					if (substr($line[0], -3) === 'csv')
						$files[] = $line[0];
				}
				
				if (array_diff(array('ind.csv', 'lgot.csv', 'subcalc.csv', 'subdec.csv', 'subpay.csv'), $files))
					throw new CHttpException(500, 'filelist is not full set');
				
				if (!Yii::app()->file->save($path_zip, $name, false, null, null, $date ?
					Swamper::date(strtotime($date), Swamper::FORMAT_DATE_SYSTEM) : false))
						throw new CHttpException(500, 'error save file');
				
				$transaction->commit();
			}catch (CHttpException $e){
				$params_fix['msg'] = $e->getMessage();
				$transaction->rollback();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('import_package', $params_fix, $ok ? 'ok' :
				($e->statusCode == 403 ? 'file_exists' : 'import_package'));
			
			if (!empty($path))
				Files::clear($path, true, true);
			
			$this->redirect($this->createUrl('import/index'));
		}
	}
	
	/**
	 * Действие выгрузки файлов
	 * @param integer $id Идентификатор файла
	 * @throws CHttpException
	 */
	public function actionFile($id){
		$model = Yii::app()->file->get($id);
		
		Yii::app()->log->fix('model_search', array('model'=>'import_file', 'id'=>$id), $model ? 'ok' : 'model_search');
		
		if (!$model)
			throw new CHttpException(500);
		
		$params_fix = array('id'=>$id, 'model'=>$model->attributes);
		
		try{
			if ($model->type !== Yii::app()->conformity->get('file_type', 'code', 'import'))
				throw new CHttpException(500, 'error file type');
			
			if (!Yii::app()->file->upload($id, static::PREFIX_UPLOAD_FILE.$model->name))
				throw new CHttpException(500, 'error upload file');
		}catch (CHttpException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('import_file_upload', $params_fix, $ok ? 'ok' : 'import_file_upload');
	}
	
	/**
	 * Действие удаления файла выгрузки
	 * @param integer $id Идентификатор файла
	 * @throws CHttpException
	 */
	public function actionDelete($id){
		$model = Yii::app()->file->get($id);
		
		Yii::app()->log->fix('model_search', array('model'=>'import_file', 'id'=>$id), $model ? 'ok' : 'model_search');
		
		if (!$model)
			throw new CHttpException(500);
		
		$params_fix = array('id'=>$id, 'model'=>$model->attributes);
		
		try{
			$transaction = Yii::app()->db->beginTransaction();
			
			if ($model->type !== Yii::app()->conformity->get('file_type', 'code', 'import'))
				throw new CHttpException(500, 'error file type');
			
			if ($model->category && (!Yii::app()->db->createCommand()->truncateTable('payment') ||
				!Yii::app()->db->createCommand()->truncateTable('subsidy')))
					throw new CHttpException(500, 'error truncate');
			
			if (!Yii::app()->file->delete($id))
				throw new CHttpException(500, 'error delete');
			
			$transaction->commit();
		}catch (CException $e){
			$transaction->rollback();
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('import_delete', $params_fix, $ok ? 'ok' : 'import_delete');
		
		$this->redirect($this->createUrl('import/index'));
	}
	
	/**
	 * Действие удаления файла выгрузки
	 * @param integer $id Идентификатор файла
	 * @throws CHttpException
	 */
	public function actionActivate($id){
		$model = Yii::app()->file->get($id);
		
		Yii::app()->log->fix('model_search', array('model'=>'import_file', 'id'=>$id), $model ? 'ok' : 'model_search');
		
		if (!$model)
			throw new CHttpException(500);
		
		$params_fix = array('id'=>$id, 'model'=>$model->attributes);
		set_time_limit(0);
		
		try{
			$transaction = Yii::app()->db->beginTransaction();
			
			if ($model->type !== Yii::app()->conformity->get('file_type', 'code', 'import'))
				throw new CHttpException(500, 'error file type');
			
			if (!$model->category && ($model_activate = Yii::app()->file->get(false, true, array('category'=>1)))){
				$params_fix['id_deactivate'] = $model_activate->id;
				$model_activate->category = null;
				
				if (!$model_activate->save(false))
					throw new CHttpException(500, 'error save model_activate');
			}
			
			$model->category = $model->category ? null : 1;
			
			if (!$model->save(false))
				throw new CHttpException(500, 'error save');
			
			if (!Yii::app()->db->createCommand()->truncateTable('payment') ||
				!Yii::app()->db->createCommand()->truncateTable('subsidy'))
					throw new CHttpException(500, 'error truncate');
			
			if (!extension_loaded('zip'))
				throw new CHttpException(500, 'zip extension not load');
			
			$path = $params_fix['path'] = Yii::getPathOfAlias(static::ALIAS_IMPORT).DIRECTORY_SEPARATOR.
				Yii::app()->user->id.'-'.time();
			
			if (!is_dir($path) && !mkdir($path, 0777, true))
				throw new CHttpException(500, 'error create path');
			
			$zip = new ZipArchive();
			
			if (!$zip->open(Yii::app()->file->getDir().$id))
				throw new CHttpException(500, 'zip file opening failed');
			
			if (!$zip->extractTo($path))
				throw new CHttpException(500, 'error extracting zip archive');
			
			if (!$zip->close())
				throw new CHttpException(500, 'error zip closing');
			
			/* if (!$handle = fopen($path.DIRECTORY_SEPARATOR.'ind.csv', 'r'))
				throw new CHttpException(500, 'error open file "ind.csv"');
			
				while (($buffer = fgets($handle)) !== false){
					$words = explode(';', $buffer);
				
					if (count($words) == 3 && is_numeric(reset($words))){
						$id_payer = (int)$words[0];
						$organization = $words[1] ? 'true' : 'false';
						$id_account = (int)$words[2];
						$content[] =
						"insert into rgis_user_account (id_payer, organization, id_account) values ($id_payer, $organization, $id_account);";
							
						if (count($content) == 100){
							file_put_contents($path2, implode(PHP_EOL, $content).PHP_EOL, FILE_APPEND);
							$content = array();
						}
					}
				}
				fclose($handle); */
			
			$transaction->commit();
		}catch (CException $e){
			$transaction->rollback();
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('import_activate', $params_fix, $ok ? 'ok' : 'import_activate');
		
		if (!empty($path))
			Files::clear($path, true, true);
		
		$this->redirect($this->createUrl('import/index'));
	}
	
}
