<?php
/**
 * Класс действия авторизации пользователя
 * @author v.zubov
 * @see CAction
 */
class LoginAction extends CAction{
	
	/**
	 * Обработчик действия
	 * @throws CHttpException
	 * @see CAction::run()
	 */
	public function run(){
		$model = new LoginModel('update');
		
		if (Yii::app()->user->checkAccess('*/login') && !$model->access)
			throw new CHttpException(500);
		
		if (Yii::app()->request->isAjaxRequest){
			if ($model->post){
				$model->attributes = $model->post;
				$errors = ActiveForm::validate($model);
				
				if ($errors)
					echo CJSON::encode(array('errors'=>$errors));
				else
					echo CJSON::encode(array('location'=>Yii::app()->user->getHomeUrl(Yii::app()->user->checkLogin($model->user) ?
						$model->user->id : false)));
			}
			
			Yii::app()->end();
		}
		
		return $this->controller->renderPartial('/user/login', array('model'=>$model), true);
	}
	
}
