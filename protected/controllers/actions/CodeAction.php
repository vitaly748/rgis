<?php
/**
 * Класс действия для обработки кодов подтверждения введённых данных моделей данных
 * @author v.zubov
 * @see CAction
 */
class CodeAction extends CAction{
	
	/**
	 * Обработчик действия
	 * @param array $params Ассоциативный массив параметров атрибутов
	 * @see CAction::run()
	 */
	public function run(array $params){
		$model = new CodeModel('update');
		
		if (!$model->access || !$params)
			throw new CHttpException(500);
		
		foreach ($model->attributeNames() as $attribute)
			if ($params_attribute = Arrays::pop($params, $attribute)){
				$model->$attribute = $params_attribute['value'];
				
				if ($params_attribute['error'])
					$model->addError(attribute, $params_attribute['error']);
			}
		
		$this->controller->renderPartial('/user/code', array('model'=>$model, 'params'=>$params));
	}
	
	/**
	 * Переопределение и дополнение стандартного метода validate, проверяющего значение атрибута
	 * @param string $input Значение атрибута, введённое пользователем
	 * @param boolean $caseSensitive Признак регистрозависимой проверки
	 * @see CCaptchaAction::validate()
	 */
	public function validate($input, $caseSensitive){
		$this->testLimit = (int)Yii::app()->setting->get('code_retries');
		
		parent::validate($input, $caseSensitive);
	}
	
	/**
	 * Переопределение и дополнение стандартного метода generateVerifyCode, генерирующего код капчи
	 * @see CCaptchaAction::generateVerifyCode()
	 */
	protected function generateVerifyCode(){
		$min_length = max(min($this->minLength, self::MAX_LENGHT), self::MIN_LENGHT);
		$max_length = max(min($this->maxLength, self::MAX_LENGHT), self::MIN_LENGHT);
		$qu_vowels = mb_strlen(self::VOWELS) - 1;
		$qu_consonants = mb_strlen(self::CONSONANTS) - 1;
		$code = '';
		
		if ($qu_vowels){
			if ($min_length == $max_length)
				$length = $min_length;
			else{
				if ($min_length > $max_length)
					$max_length = $min_length;
				
				$length = mt_rand($min_length, $max_length);
			}
			
			while ($length){
				if (!$qu_consonants)
					$char = mb_substr(self::VOWELS, mt_rand(0, $qu_vowels), 1);
				else{
					$odd = $length % 2;
					$luck = mt_rand(0, 10) > 2;
					
					if ($odd && $luck || !$odd && !$luck)
						$char = mb_substr(self::VOWELS, mt_rand(0, $qu_vowels), 1);
					else
						$char = mb_substr(self::CONSONANTS, mt_rand(0, $qu_consonants), 1);
				}
				
				$code .= self::CASE_FULL && mt_rand(0, 1) ? mb_strtoupper($char) : $char;
				$length--;
			}
		}
		
		return $code;
	}
	
}
