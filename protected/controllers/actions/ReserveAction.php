<?php
/**
 * Класс действия создания резервной копии Базы данных
 * @author v.zubov
 * @see CAction
 */
class ReserveAction extends CAction{
	
	/**
	 * Обработчик действия
	 * @throws CHttpException
	 * @see CAction::run()
	 */
	public function run(){
		Swamper::timer($tick);
		$state = $this->controller->state;
		$params_fix = array('tables'=>array());
		$time = time();
		set_time_limit(0);
		
		try{
			$transaction = Yii::app()->db->beginTransaction();
			$type_code = Yii::app()->conformity->get('file_type', 'code', 'reserve');
			
			if (!extension_loaded('zip'))
				throw new CHttpException(500, 'zip extension not load');
			
			if (!Yii::app()->setting->setState('locked', false))
				throw new CHttpException(500, 'error locked application');
			
			if ($files = Yii::app()->file->get(false, $type_code)){
				Arrays::sort($files, 'date');
				
				while (count($files) >= ReserveController::MAX_COPYES){
					$file = array_shift($files);
					$params_fix['remove_files'][] = $file->id;
					
					if (!Yii::app()->file->delete($file->id, null, 'reserve'))
						throw new CHttpException(500, 'error delete');
				}
			}
			
			$path = $params_fix['path_temp'] = Yii::getPathOfAlias(ReserveController::ALIAS_RESERVE).DIRECTORY_SEPARATOR.
				(int)Yii::app()->user->id.'-'.$time;
			
			if (!is_dir($path) && !mkdir($path, 0777, true))
				throw new CHttpException(500, 'error create path');
			
			foreach (Yii::app()->table->get(false, true) as $name=>$attributes)
				if (!in_array($name, ReserveController::IGNORE_TABLES)){
					$model = call_user_func(array(Strings::nameModify($name), 'model'));
					$qu = $params_fix['tables'][$name] = $model->count();
					$data = array();
					$order = reset(array_keys($attributes));
					$count = 0;
					
					while ($count < $qu){
						$rows = $model->findAll(array('order'=>$order, 'offset'=>$count, 'limit'=>ReserveController::MAX_ROWS));
						
						foreach ($rows as $row)
							if ($name != 'file' || $row->type != $type_code)
								$data[] = array_values($row->attributes);
						
						$count += ReserveController::MAX_ROWS;
					}
					
					if ($data){
						if (!file_put_contents($path_file = $path.DIRECTORY_SEPARATOR.($name = $name.'.bin'),
							serialize($data)))
								throw new CHttpException(500, 'error file "'.$name.'"create');
						
						$params_fix['files'][$name] = $path_file;
					}
				}
			
			$zip = new ZipArchive();
			
			if (!$zip->open($path_zip = $path.DIRECTORY_SEPARATOR.
				Swamper::date($time, Swamper::FORMAT_DATE_SYSTEM_WITHOUT_TIME).'.zip', ZIPARCHIVE::CREATE))
					throw new CHttpException(500, 'zip file creation failed');
			
			foreach ($params_fix['files'] as $name=>$path_file)
				if (!$zip->addFile($path_file, $name))
					throw new CHttpException(500, 'error add file "'.$name.'" to zip archive');
			
			if (!$zip->close())
				throw new CHttpException(500, 'error zip closing');
			
			if (!Yii::app()->file->save($path_zip, false, $type_code, null, null, Swamper::date($time, Swamper::FORMAT_DATE_SYSTEM)))
				throw new CHttpException(500, 'error save file');
			
			if ($state != 'locked' && !Yii::app()->setting->setState($state, false))
				throw new CHttpException(500, 'error unlocked application');
			
			$params_fix['timer'] = Swamper::timer($tick);
			$transaction->commit();
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
			$transaction->rollback();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('reserve_create', $params_fix, $ok ? 'ok' : 'reserve_create');
		
		if (!empty($path))
			Files::clear($path, true, true);
		
		if (!$ok)
			throw new CHttpException(500);
	}
	
}
