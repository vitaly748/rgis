<?php
/**
 * Класс, переопределяющий и дополняющий стандартный класс CCaptchaAction виджета CCaptcha
 * @author v.zubov
 * @see CCaptchaAction
 */
class CaptchaAction extends CCaptchaAction{
	
	/**
	 * @var string Псевдоним пути до файла шрифта, используемого при генерации капчи
	 */
	const FONT_PATH = 'application.components.widgets.captcha.views.assets.captcha.fonts';
	
	/**
	 * @var string Название файла шрифта
	 */
	const FONT_NAME = 'pts75f-webfont.ttf';
	
	/**
	 * @var integer Минимальная длина кода капчи
	 */
	const MIN_LENGHT = 3;
	
	/**
	 * @var boolean|string Набор основных (гласных) символов капчи в нижнем регистре. Основные символы будут присутствовать
	 * в каче с большей вероятностью
	 */
	const VOWELS = 'aeiou';
// 	const VOWELS = 'аеиоуыэюя';
// 	const VOWELS = '0123456789';
	
	/**
	 * @var boolean|string Набор второстепенных (согласных) символов капчи в нижнем регистре или false
	 */
	const CONSONANTS = 'bcdfghjklmnpqrstvwxyz';
// 	const CONSONANTS = 'бвгджзйклмнпрстфхцчшщ';
	
	/**
	 * @var Максимальная длина кода капчи
	 */
	const MAX_LENGHT = 20;
	
	/**
	 * @var boolean Признак генерации капчи в обоих регистрах
	 */
	const CASE_FULL = false;
	
	/**
	 * @var integer Ширина генерируемого изображения. По умолчанию равно 100
	 */
	public $width = 100;
	
	/**
	 * @var integer Высота генерируемого изображения. По умолчанию равно 30
	 */
	public $height = 30;
	
	/**
	 * @var integer Внутренний отступ текста в изображении. По умолчанию равно 0
	 */
	public $padding = 2;
	
	/**
	 * @var integer Код цвета фона капчи
	 */
	public $backColor = 0xeeeeff;
	
	/**
	 * @var integer Код цвета текста капчи
	 */
	public $foreColor = 0x194b71;
	
	/**
	 * @var integer Минимальное количество символов в строке капчи. По умолчанию равно 5
	 */
	public $minLength = 5;
	
	/**
	 * @var integer Максимальное количество символов в строке капчи. По умолчанию равно 5
	 */
	public $maxLength = 5;
	
	/**
	 * @var integer Межсимвольное расстояние текста капчи. По умолчанию равно 2
	 */
	public $offset = 2;
	
	/**
	 * Обработчик действия
	 * @see CCaptchaAction::run()
	 */
	public function run(){
		if (is_file($path = Yii::getPathOfAlias(self::FONT_PATH).'/'.self::FONT_NAME))
			$this->fontFile = $path;
		
		parent::run();
	}
	
	/**
	 * Переопределение и дополнение стандартного метода validate, проверяющего значение атрибута
	 * @param string $input Значение атрибута, введённое пользователем
	 * @param boolean $caseSensitive Признак регистрозависимой проверки
	 * @see CCaptchaAction::validate()
	 */
	public function validate($input, $caseSensitive){
		$this->testLimit = (int)Yii::app()->setting->get('code_retries');
		
		return parent::validate($input, $caseSensitive);
	}
	
	/**
	 * Переопределение и дополнение стандартного метода generateVerifyCode, генерирующего код капчи
	 * @see CCaptchaAction::generateVerifyCode()
	 */
	protected function generateVerifyCode(){
		$min_length = max(min($this->minLength, self::MAX_LENGHT), self::MIN_LENGHT);
		$max_length = max(min($this->maxLength, self::MAX_LENGHT), self::MIN_LENGHT);
		$qu_vowels = mb_strlen(self::VOWELS) - 1;
		$qu_consonants = mb_strlen(self::CONSONANTS) - 1;
		$code = '';
		
		if ($qu_vowels){
			if ($min_length == $max_length)
				$length = $min_length;
			else{
				if ($min_length > $max_length)
					$max_length = $min_length;
				
				$length = mt_rand($min_length, $max_length);
			}
			
			while ($length){
				if (!$qu_consonants)
					$char = mb_substr(self::VOWELS, mt_rand(0, $qu_vowels), 1);
				else{
					$odd = $length % 2;
					$luck = mt_rand(0, 10) > 2;
					
					if ($odd && $luck || !$odd && !$luck)
						$char = mb_substr(self::VOWELS, mt_rand(0, $qu_vowels), 1);
					else
						$char = mb_substr(self::CONSONANTS, mt_rand(0, $qu_consonants), 1);
				}
				
				$code .= self::CASE_FULL && mt_rand(0, 1) ? mb_strtoupper($char) : $char;
				$length--;
			}
		}
		
		return $code;
	}
	
}
