<?php
/**
 * Контроллер работы с экспортом данных
 * @author v.zubov
 */
class ExportController extends Controller{
	
	/**
	 * @var string Псевдоним пути к папке временных файлов, создаваемых при генерации файлов выгрузки
	 */
	const ALIAS_EXPORT = 'application.runtime.export';
	
	/**
	 * @var string Версия формата обмена данными с РГИС
	 */
	const VERSION_FORMAT = '4.0.5';
	
	/**
	 * @var array Ассоциативный массив параметров поставщиков выгрузки 
	 */
	const PROVIDER_PARAMS = array(
		'repair'=>array(
			'inn'=>'5902990563',
			'kpp'=>'590201001',
			'ogrn'=>'1145958003366',
			'type'=>'14',
			'key'=>'appeal.permkrai.repair',
			'operator'=>'Администратор',
			'phone'=>'+79000000000',
			'prefix_upload_file'=>'Фонд_капремонта_'
		),
		'tsg_gor77'=>array(
			'inn'=>'5904241954',
			'kpp'=>'590401001',
			'ogrn'=>'1115904000519',
			'type'=>'1',
			'key'=>'appeal.permkrai.gor77',
			'operator'=>'Администратор',
			'phone'=>'+79000000000',
			'prefix_upload_file'=>'ТСЖ_Горького77_'
		)
	);
	
	/**
	 * @var string Период начала отчёта для формирования файлов выгрузок
	 */
	const PERIOD_START = '2017-05';
	
	/**
	 * @var string Префикс выгружаемых файлов выгрузок
	 */
	const PREFIX_UPLOAD_FILE = 'Обращения_';
	
	/**
	 * @var array Ассоциативный массив с информацией о файлах, входящих в выгрузку
	 */
	const FILES = array(
		'_info.csv'=>array(
			'Версия формата',
			'ИНН',
			'КПП',
			'ОГРН (ОГРНИП)',
			'Наименование организации (ФИО ИП)',
			'Ключ банка данных (Наименование банка данных)',
			'Год',
			'Месяц',
			'Дата и время формирования файла',
			'ФИО отправителя',
			'Телефон отправителя',
			'Тип поставщика информации'
		),
		'_filelist.csv'=>array(
			'Наименование файла',
			'Количество строк данных',
			'Контрольная строка'
		),
		'files.csv'=>array(
			'Уникальный код файла в системе отправителя',
			'Наименование файла',
			'Размер файла в байтах',
			'Описание файла',
			'Каталог файла'
		),
		'appeal.csv'=>array(
			'Уникальный код',
			'Дом',
			'Дата создания',
			'Дата регистрации',
			'Категория обращения',
			'Номер обращения',
			'Тема обращения',
			'Тип обращения',
			'Тип заявителя',
			'Организация-ответчик',
			'Содержание',
			'Дата отзыва',
			'Фамилия',
			'Имя',
			'Отчество',
			'Название организации',
			'Почтовый адрес заявителя (ФИАС)',
			'Статус обращений'
		),
		'appealstatushistory.csv'=>array(
			'Обращение',
			'Статус обращений',
			'Дата установки статус'
		),
		'appealanswer.csv'=>array(
			'Уникальный код',
			'Обращение',
			'Организация-ответчик',
			'Содержание',
			'Дата ответа'
		)
	);
	
	/**
	 * Действия просмотра списка новостей
	 */
	public function actionIndex(){
		$model = new ExportModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest){
			$id_responders = Yii::app()->conformity->get('appeal_responder', 'description', 'code');
			
			if (!Yii::app()->user->checkAccess('developer')){
				$organization = Yii::app()->user->model->title;
				
				foreach ($id_responders as $code=>$description)
					if (mb_strtolower($description) !== mb_strtolower($organization))
						unset($id_responders[$code]);
			}
			
			$id_dates = array();
			$date = getdate();
			$date = $date['year'].'-'.str_pad($date['mon'], 2, '0', STR_PAD_LEFT);
			list($year_count, $month_count) = Arrays::pop(date_parse(static::PERIOD_START), 'year, month');
			
			while (($date_count = $year_count.'-'.str_pad($month_count, 2, '0', STR_PAD_LEFT)) < $date){
				$id_dates[$date_count] = Swamper::date($date_count, 'F Y');
				$month_count++;
				
				if ($month_count > 12){
					$month_count = 1;
					$year_count++;
				}
			}
			
			Yii::app()->user->setState(WebUser::PREFIX_SET.'id_responders', $id_responders);
			Yii::app()->user->setState(WebUser::PREFIX_SET.'id_dates', $id_dates);
			$this->render('index', array('model'=>$model, 'id_responders'=>$id_responders, 'id_dates'=>$id_dates));
		}else
			$this->renderPartial('indexGrid', array('model'=>$model));
	}
	
	/**
	 * Действие формирования файла выгрузки
	 * @throws CHttpException
	 */
	public function actionExport(){
		$model = new ExportModel('update');
		
		if (!$model->access || (!$id_responders = Yii::app()->user->getState(WebUser::PREFIX_SET.'id_responders')) ||
			(!$id_dates = Yii::app()->user->getState(WebUser::PREFIX_SET.'id_dates')))
				throw new CHttpException(500);
		
		if (!$model->post)
			$this->renderPartial('export', array('model'=>$model, 'id_responders'=>$id_responders, 'id_dates'=>$id_dates));
		else{
			$model->attributes = $model->post;
			$params_fix = array('provider'=>$model->provider, 'period'=>$model->period, 'records'=>0);
			set_time_limit(0);
			$date = Swamper::date($time = time(), Swamper::FORMAT_DATE_SYSTEM);
			
			try{
				$transaction = Yii::app()->db->beginTransaction();
				$provider_name = Yii::app()->conformity->get('appeal_responder', 'name', (int)$params_fix['provider']);
				
				if (!isset($id_responders[$params_fix['provider']]) || (!$params_provider = Arrays::pop($pp = static::PROVIDER_PARAMS,
					$provider_name)))
						throw new CHttpException(500, 'invalid provider');
				
				if (!isset($id_dates[$params_fix['period']]))
					throw new CHttpException(500, 'invalid period');
				
				if (Yii::app()->file->get(false, true, array('source'=>$params_fix['provider'], 'name'=>$params_fix['period'].'.zip')))
					throw new CHttpException(403, 'file already exists');
				
				if (!extension_loaded('zip'))
					throw new CHttpException(500, 'zip extension not load');
				
				$path = $params_fix['path'] = Yii::getPathOfAlias(static::ALIAS_EXPORT).DIRECTORY_SEPARATOR.
					Yii::app()->user->id.'-'.$time;
				
				if (!is_dir($path) && !mkdir($path, 0777, true))
					throw new CHttpException(500, 'error create path');
				
				foreach (static::FILES as $name=>$titles){
					$title_line = array();
					
					foreach ($titles as $title)
						$title_line[] = iconv('utf-8', 'cp1251', $title);
					
					$data[$name] = array(implode(';', $title_line));
				}
				
				$zip = new ZipArchive();
				$params_date = date_parse($params_fix['period']);
				$date_from = date(Swamper::FORMAT_DATE_SYSTEM, mktime(0, 0, 0, $month = $params_date['month'], 1,
					$year = $params_date['year']));
				$params_date['month']++;
				
				if (!$zip->open($path_zip = $path.DIRECTORY_SEPARATOR.$params_fix['period'].'.zip', ZIPARCHIVE::CREATE))
					throw new CHttpException(500, 'zip file creation failed');
				
				if ($params_date['month'] > 12){
					$params_date['month'] = 1;
					$params_date['year']++;
				}
				
				$date_to = date(Swamper::FORMAT_DATE_SYSTEM, mktime(0, 0, 0, $params_date['month'], 1, $params_date['year']));
				$id_appeals = Yii::app()->db->createCommand()->
					select('id_appeal')->
					from('{{appeal_stage}}')->
					where('date >= :date_from AND date < :date_to', array(':date_from'=>$date_from, ':date_to'=>$date_to))->
					queryColumn();
				
				if ($id_appeals = array_unique($id_appeals)){
					$states = Yii::app()->conformity->get('appeal_stage_state', 'code', 'name');
					$states_complete = Yii::app()->conformity->get('appeal_state', 'code', false, false, 'done, denied');
					$appeals = AppealModel::model()->
						with(array('stage', 'stage.owner'))->
						findAll('"t"."id" IN ('.implode(', ', $id_appeals).') AND "t"."responder" = '.$params_fix['provider']);
					
					foreach ($appeals as $appeal)
						if (($stages = $appeal->stage) && isset($stages[0]) && ($author = $stages[0]->owner)){
							$change = $state = false;
							
							foreach ($stages as $stage){
								$state_new = Appeal::STATE_CONFORMITY[$stage->state];
								
								if ($state != $state_new){
									if (($less = $stage->date < $date_to) && $stage->date >= $date_from){
										$data['appealstatushistory.csv'][] = implode(';', array(
											$appeal->id,
											$state_new,
											Swamper::date($stage->date, Swamper::FORMAT_DATE_RGIS),
										));
										$change = true;
									}
									
									if ($less)
										$state = $state_new;
								}
							}
							
							if ($change){
								$data['appeal.csv'][] = implode(';', array(
									$appeal->id,
									'',
									Swamper::date($stages[0]->date, Swamper::FORMAT_DATE_RGIS),
									isset($stages[1]) && $stages[1]->state == $states['accepted'] && $stages[1]->date <= $date_to ?
										Swamper::date($stages[1]->date, Swamper::FORMAT_DATE_RGIS) : '',
									$appeal->category,
									$appeal->id,
									$provider_name == 'repair' ? Appeal::CODE_USER_THEME : $appeal->themeid,
									$appeal->type,
									($citizen = Yii::app()->user->checkAccess('citizen', array('id_user'=>$author->id))) ? 1 : 2,
									'',
									iconv('utf-8', 'cp1251', static::checkValue(mb_substr(str_replace("\r\n", '', $stages[0]->text), 0, 1200))),
									isset($stages[1]) && $stages[1]->state == $states['revoked'] && $stages[1]->date <= $date_to ?
										Swamper::date($stages[1]->date, Swamper::FORMAT_DATE_RGIS) : '',
									$citizen ? iconv('utf-8', 'cp1251', $author->surname) : '',
									$citizen ? iconv('utf-8', 'cp1251', $author->name) : '',
									$citizen ? iconv('utf-8', 'cp1251', $author->patronymic) : '',
									$citizen ? '' : iconv('utf-8', 'cp1251', static::checkValue($author->title)),
									$author->addressid ? $author->addressid : $appeal->addressid,
									$state
								));
								
								if (in_array($state, $states_complete))
									$data['appealanswer.csv'][] = implode(';', array(
										$appeal->id,
										$appeal->id,
										'',
										iconv('utf-8', 'cp1251', static::checkValue(mb_substr(str_replace("\r\n", '', $stage->text), 0, 1200))),
										Swamper::date($stage->date, Swamper::FORMAT_DATE_RGIS)
									));
							}
						}
				}
				
				$data['_info.csv'][] = implode(';', array(
					static::VERSION_FORMAT,
					$params_provider['inn'],
					$params_provider['kpp'],
					$params_provider['ogrn'],
					iconv('utf-8', 'cp1251', static::checkValue($id_responders[$params_fix['provider']])),
					$params_provider['key'],
					$year,
					$month,
					Swamper::date($time, Swamper::FORMAT_DATE_RGIS),
					iconv('utf-8', 'cp1251', $params_provider['operator']),
					$params_provider['phone'],
					$params_provider['type']
				));
				
				$data['_filelist.csv'][] = implode(';', array('_info.csv', 1, ''));
				$data['_filelist.csv'][] = implode(';', array('_filelist.csv', count($data), ''));
				$data['_filelist.csv'][] = implode(';', array('files.csv', 0, ''));
				
				foreach (array('appeal.csv', 'appealstatushistory.csv', 'appealanswer.csv') as $name){
					$data['_filelist.csv'][] = implode(';', array($name, $records = count($data[$name]) - 1, ''));
					$params_fix['records'] += $records;
				}
				
				foreach ($data as $name=>$lines){
					if (!file_put_contents($path_file = $path.DIRECTORY_SEPARATOR.$name, implode("\r\n", $lines)))
						throw new CHttpException(500, 'file "'.$name.'" not created');
					
					if (!$zip->addFile($path_file, $name))
						throw new CHttpException(500, 'error add file "'.$name.'" to zip archive');
				}
				
				if (!$zip->close())
					throw new CHttpException(500, 'error zip closing');
				
				if (!Yii::app()->file->save($path_zip, false, false, $params_fix['provider'], null, $date))
					throw new CHttpException(500, 'error save file');
				
				$transaction->commit();
			}catch (CHttpException $e){
				$params_fix['msg'] = $e->getMessage();
				$transaction->rollback();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('export_package', $params_fix, $ok ? 'ok' :
				($e->statusCode == 403 ? 'file_exists' : 'export_package'));
			
			if (!empty($path))
				Files::clear($path, true, true);
			
			if ($ok)
				Yii::app()->user->setFlash('ok', 'Внесено записей: '.Swamper::unit($params_fix['records']));
			
			$this->redirect($this->createUrl('export/index'));
		}
	}
	
	/**
	 * Действие выгрузки файлов
	 * @param integer $id Идентификатор файла
	 * @throws CHttpException
	 */
	public function actionFile($id){
		$model = Yii::app()->file->get($id);
		
		Yii::app()->log->fix('model_search', array('model'=>'export_file', 'id'=>$id), $model ? 'ok' : 'model_search');
		
		if (!$model)
			throw new CHttpException(500);
		
		$params_fix = array('id'=>$id, 'model'=>$model->attributes);
		
		try{
			if ($model->type !== Yii::app()->conformity->get('file_type', 'code', 'export'))
				throw new CHttpException(500, 'error file type');
			
			if (!$id_responders = Yii::app()->user->getState(WebUser::PREFIX_SET.'id_responders'))
				throw new CHttpException(500, 'not set id_responders');
			
			if (!isset($id_responders[$model->source]))
				throw new CHttpException(403, 'access denied');
			
			$name = Arrays::pop($pp = static::PROVIDER_PARAMS, Yii::app()->conformity->get('appeal_responder', 'name', $model->source).
				'.prefix_upload_file');
			$name = static::PREFIX_UPLOAD_FILE.($name ? $name : '').$model->name;
			
			if (!Yii::app()->file->upload($id, $name))
				throw new CHttpException(500, 'error upload file');
		}catch (CHttpException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('export_file_upload', $params_fix, $ok ? 'ok' :
			($e->statusCode == 403 ? 'file_access' : 'export_file_upload'));
	}
	
	/**
	 * Действие удаления файла выгрузки
	 * @param integer $id Идентификатор файла
	 * @throws CHttpException
	 */
	public function actionDelete($id){
		$model = Yii::app()->file->get($id);
		
		Yii::app()->log->fix('model_search', array('model'=>'export_file', 'id'=>$id), $model ? 'ok' : 'model_search');
		
		if (!$model)
			throw new CHttpException(500);
		
		$params_fix = array('id'=>$id, 'model'=>$model->attributes);
		
		try{
			$transaction = Yii::app()->db->beginTransaction();
			
			if ($model->type !== Yii::app()->conformity->get('file_type', 'code', 'export'))
				throw new CHttpException(500, 'error file type');
			
			if (!$id_responders = Yii::app()->user->getState(WebUser::PREFIX_SET.'id_responders'))
				throw new CHttpException(500, 'not set id_responders');
			
			if (!Yii::app()->file->delete($id))
				throw new CHttpException(500, 'error delete');
			
			$transaction->commit();
		}catch (CException $e){
			$transaction->rollback();
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('export_delete', $params_fix, $ok ? 'ok' : 'export_delete');
		
		$this->redirect($this->createUrl('export/index'));
	}
	
	/**
	 * Проверка на корректность значения, вносимого в файл выгрузки
	 * @param string $value Исходное значение
	 * @return string Скорректированное занчение
	 */
	protected static function checkValue($value){
		$value = str_replace('"', '""', $value);
		
		if (mb_strpos($value, ';') !== false || mb_strpos($value, '"') !== false)
			$value = '"'.$value.'"';
		
		return $value;
	}
	
}
