<?php
/**
 * Контроллер работы с новостями
 * @author v.zubov
 */
class NewsController extends Controller{
	
	/**
	 * Действия просмотра списка новостей
	 */
	public function actionIndex(){
		$model = new NewsModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest)
			$this->render('index', array('model'=>$model));
		else
			$this->renderPartial('indexGrid', array('model'=>$model));
	}
	
	/**
	 * Действие создания новой новости
	 */
	public function actionCreate(){
		$this->actionUpdate();
	}
	
	/**
	 * Действие редактирования новости
	 * @param integer $id Идентификатор обращения. По умолчанию равно false
	 */
	public function actionUpdate($id = false){
		$action = $this->action->id;
		
		if (!$id)
			$model = new NewsModel;
		else{
			$model = NewsModel::model()->findByPk($id);
			
			Yii::app()->log->fix('model_search', array('model'=>'news', 'id'=>$id), $model ? 'ok' : 'model_search');
		}
		
		if (!$model || !$model->access)
			throw new CHttpException(500);
		
		if (!$model->post){
			if (!$id){
				$model->date = Swamper::date(false, Swamper::FORMAT_DATE_USER_WITHOUT_TIME);
				$model->state = Yii::app()->conformity->get('news_state', 'code', 'publication');
			}else
				$model->date = Swamper::date($model->date, Swamper::FORMAT_DATE_USER_WITHOUT_TIME);
		}else{
			$model->attributes = $model->post;
			
			if ($model->validate()){
				$params_fix = array('model'=>$model->attributes);
				
				try{
					$model->date = Swamper::date($model->date, Swamper::FORMAT_DATE_SYSTEM);
					
					if (!$model->save(false))
						throw new CHttpException(500, 'save error');
				}catch (CException $e){
					$params_fix['msg'] = $e->getMessage();
				}
				
				$ok = !isset($e);
				
				Yii::app()->log->fix('news_'.$action, $params_fix, $ok ? 'ok' : 'news_'.$action);
				
				if ($ok){
					$params_redirect = array(
						'news/view'=>array('id'=>$model->id),
						'news/update'=>array('id'=>$model->id),
					);
					
					if ($action == 'update')
						$params_redirect = array_reverse($params_redirect);
					
					$this->redirectAccessible($params_redirect + array('news/index'));
					$this->refreshWithoutParams(true);
				}
			}
		}
		
		$this->render('update', array('model'=>$model));
	}
	
	/**
	 * Действие просмотра новости
	 * @param integer $id Идентификатор новости
	 * @throws CHttpException
	 */
	public function actionView($id = false){
		if ($model = NewsModel::model()->findByPk($id))
			$model->scenario = 'view';
		
		Yii::app()->log->fix('model_search', array('model'=>'news', 'id'=>$id), $model ? 'ok' : 'model_search');
		
		if (!$model || !$model->access)
			throw new CHttpException(500);
		
		if ((!$redactor = Yii::app()->user->checkAccessSet('developer, administrator, redactor')) &&
			$model->date > Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM))
				throw new CHttpException(403, 'Нет доступа на совершение запрашиваемого действия');
		
		$model->text = Strings::multilineText($model->text);
		$model->date = Swamper::date($model->date, Swamper::FORMAT_DATE_USER_WITHOUT_TIME);
		
		$this->render('view', array('model'=>$model, 'redactor'=>$redactor));
	}
	
	/**
	 * Действие удаления новости
	 * @param integer $id Идентификатор новости
	 */
	public function actionDelete($id){
		if ($model = NewsModel::model()->findByPk($id))
			$model->scenario = 'delete';
		
		Yii::app()->log->fix('model_search', array('model'=>'news', 'id'=>$id), $model ? 'ok' : 'model_search');
		
		$params_fix = array('model'=>$model->attributes);
		
		try{
			$model->state = Yii::app()->conformity->get('news_state', 'code', 'remote');
				
			if (!$model->save(false))
				throw new CHttpException(500, 'error save');
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('news_delete', $params_fix, $ok ? 'ok' : 'news_delete');
		
		$this->redirect(Yii::app()->request->getUrlReferrer());
	}
	
}
