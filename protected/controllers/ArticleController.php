<?php
/**
 * Контроллер работы со статьями
 * @author v.zubov
 */
class ArticleController extends Controller{
	
	/**
	 * Действия просмотра статьи "Часто задаваемые вопросы"
	 */
	public function actionAnswer(){
		$this->render('answer');
	}
	
}
