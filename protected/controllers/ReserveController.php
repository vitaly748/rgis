<?php
/**
 * Контроллер по управлению резервными копиями БД
 * @author v.zubov
 */
class ReserveController extends Controller{
	
	/**
	 * @var integer Максимальное число резервных копий
	 */
	const MAX_COPYES = 7;
	
	/**
	 * @var integer Максимальное число строк, читаемых из таблицы БД за раз
	 */
	const MAX_ROWS = 8000;
	
	/**
	 * @var array Список названий таблиц приложения, игнорируемых при резервном копировании
	 */
	const IGNORE_TABLES = array('subsidy', 'payment', 'fias_address', 'log');
	
	/**
	 * @var string Псевдоним пути к папке временных файлов резервных копий
	 */
	const ALIAS_RESERVE = 'application.runtime.reserve';
	
	/**
	 * Действие просмотра списка резервных копий Базы данных
	 */
	public function actionIndex(){
		$model = new ReserveModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest)
			$this->render('index', array('model'=>$model));
		else
			$this->renderPartial('indexGrid', array('model'=>$model));
	}
	
	/**
	 * Действие создания резервной копии Базы данных
	 */
	public function actionCreate(){
		try{
			$this->createAction('reserve')->run();
		}catch (CException $e){
		}
		
		$this->redirect($this->createUrl('reserve/index'));
	}
	
	/**
	 * Действие восстановления резервной копии Базы данных
	 * @param integer $id Идентификатор файла
	 */
	public function actionRestore($id){
		$model = Yii::app()->file->get($id);
		
		Yii::app()->log->fix('model_search', array('model'=>'reserve_file', 'id'=>$id), $model ? 'ok' : 'model_search');
		
		if (!$model)
			throw new CHttpException(500);
		
		Swamper::timer($tick);
		$params_fix = array('tables'=>array());
		set_time_limit(0);
		
		try{
			$transaction = Yii::app()->db->beginTransaction();
			$type_code = Yii::app()->conformity->get('file_type', 'code', 'reserve');
			
			if ($model->type !== $type_code)
				throw new CHttpException(500, 'error file type');
			
			if (!extension_loaded('zip'))
				throw new CHttpException(500, 'zip extension not load');
			
			$params_fix['path'] = Yii::app()->file->getDir().$id;
			$path = $params_fix['path_temp'] = Yii::getPathOfAlias(static::ALIAS_RESERVE).DIRECTORY_SEPARATOR.
				Yii::app()->user->id.'-'.time();
				
			if (!is_dir($path) && !mkdir($path, 0777, true))
				throw new CHttpException(500, 'error create path');
			
			$zip = new ZipArchive();
			
			if (!$zip->open($params_fix['path']))
				throw new CHttpException(500, 'zip file opening failed');
			
			if (!$zip->extractTo($path))
				throw new CHttpException(500, 'error extracting zip archive');
			
			if (!$zip->close())
				throw new CHttpException(500, 'error zip closing');
			
			if (!Yii::app()->table->dropForeignKeys('log'))
				throw new CHttpException(500, 'error drop foreign keys for log table');
			
			$tables = Yii::app()->table->get(false, true);
			$files = Yii::app()->file->get(false, true);
			
			foreach (array_reverse($tables) as $name=>$attributes)
				if (!in_array($name, static::IGNORE_TABLES))
					Yii::app()->db->createCommand()->setText('TRUNCATE TABLE "{{'.$name.'}}" CASCADE')->execute();
			
			foreach ($tables as $name=>$attributes)
				if (!in_array($name, static::IGNORE_TABLES)){
					$path_file = $path.DIRECTORY_SEPARATOR.$name.'.bin';
					$data = is_file($path_file) ? unserialize(file_get_contents($path_file)) : false;
					
					if ($params_fix['tables'][$name] = $data ? count($data) : 0){
						if ($name == 'file'){
							foreach ($data as $key=>$row)
								if ($row[1] == $type_code)
									unset($data[$key]);
							
							if ($files)
								foreach ($files as $file)
									$data[] = array_values($file->attributes);
						}
						
						array_unshift($data, array_keys($attributes));
						
						if (!Yii::app()->db->createCommand()->insert($name, $data))
							throw new CHttpException(500, 'error insert data on table "'.$name.'"');
					}
				}
			
			if (!Yii::app()->table->addForeignKeys('log'))
				throw new CHttpException(500, 'error add foreign keys for log table');
			
			$params_fix['timer'] = Swamper::timer($tick);
			$transaction->commit();
		}catch (CException $e){
			$params_fix['msg'] = $e->getMessage();
			$transaction->rollback();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('reserve_restore', $params_fix, $ok ? 'ok' : 'reserve_restore');
		
		if (!empty($path))
			Files::clear($path, true, true);
		
		$this->redirect($this->createUrl('reserve/index'));
	}
	
	/**
	 * Действие удаления резервной копии Базы данных
	 * @param integer $id Идентификатор файла
	 */
	public function actionDelete($id){
		$model = Yii::app()->file->get($id);
		
		Yii::app()->log->fix('model_search', array('model'=>'reserve_file', 'id'=>$id), $model ? 'ok' : 'model_search');
		
		if (!$model)
			throw new CHttpException(500);
		
		$params_fix = array('id'=>$id, 'model'=>$model->attributes);
		
		try{
			$transaction = Yii::app()->db->beginTransaction();
			
			if ($model->type !== Yii::app()->conformity->get('file_type', 'code', 'reserve'))
				throw new CHttpException(500, 'error file type');
			
			if (!Yii::app()->file->delete($id))
				throw new CHttpException(500, 'error delete');
			
			$transaction->commit();
		}catch (CException $e){
			$transaction->rollback();
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('reserve_delete', $params_fix, $ok ? 'ok' : 'reserve_delete');
		
		$this->redirect($this->createUrl('reserve/index'));
	}
	
}
