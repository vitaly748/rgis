<?php
/**
 * Контроллер работы с услугами
 * @author v.zubov
 */
class ServiceController extends Controller{
	
	/**
	 * Действие просмотра информации по услугам, привязанным к лицевому счёту
	 * @param boolean|string $id Идентификатор сопряжения или false. По умолчанию равно false. Если равно false, будет выбран
	 * первый идентификатор из доступных
	 */
	public function actionIndex($id = false){
		$model = new ServiceModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest){
			$id_accounts = array();
			
			$roles_component = Yii::app()->user->rolesComponent;
			
			if ($accounts = Yii::app()->user->accounts)
				foreach ($accounts as $bss_role=>$accounts_role){
					$component = Arrays::pop($roles_component, $bss_role);
					
					foreach ($accounts_role as $id_account=>$account)
						$id_accounts[$account['id_conjugation']] = array(
							'description'=>$id_account.' - '.Yii::app()->conformity->get('account_type', 'description', $component).' - '.
								Swamper::address($account, true),
							'services'=>$account['services']
						);
				}
			
			if (!$id_accounts)
				$id = false;
			elseif (!isset($id_accounts[$id]))
				$id = reset(array_keys($id_accounts));
			
			Yii::app()->user->setState(WebUser::PREFIX_SET.'id_accounts', $id_accounts);
			$this->render('index', array('model'=>$model, 'id_accounts'=>Arrays::focussing($id_accounts), 'id'=>$id));
		}else
			$this->renderPartial('indexGrid', array('model'=>$model));
	}
	
	/**
	 * Действие просмотра истории изменения тарифа по услуге
	 * @param boolean|string $id Идентификатор услуги или false. По умолчанию равно false. Если равно false, будет выбран
	 * первая услуга из доступных
	 */
	public function actionHistory($id = false){
		$model = new ServiceHistoryModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest){
			$id_services = array();
			$roles_component = Yii::app()->user->rolesComponent;
			
			if ($accounts = Yii::app()->user->accounts)
				foreach ($accounts as $bss_role=>$accounts_role){
// 					if ($bss_role == 'bss2_payer')
// 						continue;
					$component = Arrays::pop($roles_component, $bss_role);
					
					foreach ($accounts_role as $id_account=>$account)
						if (!empty($account['services']))
							foreach ($account['services'] as $service)
								$id_services[$service['id']] = array(
									'description'=>$id_account.' - '.Yii::app()->conformity->get('account_type', 'description', $component).' - '.
										Swamper::address($account, true).' - '.$service['name'],
									'costs'=>$service['costs']
								);
				}
			
			if (!$id_services)
				$id = false;
			elseif (!isset($id_services[$id]))
				$id = reset(array_keys($id_services));
			
			Yii::app()->user->setState(WebUser::PREFIX_SET.'id_services', $id_services);
			$this->render('history', array('model'=>$model, 'id_services'=>Arrays::focussing($id_services), 'id'=>$id));
		}else
			$this->renderPartial('historyGrid', array('model'=>$model));
	}
	
}
