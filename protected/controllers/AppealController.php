<?php
/**
 * Контроллер работы с обращениями
 * @author v.zubov
 */
class AppealController extends Controller{
	
	/**
	 * Действия просмотра списка обращений
	 */
	public function actionIndex(){
		$model = new AppealModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest)
			$this->render('index', array('model'=>$model));
		else
			$this->renderPartial('indexGrid', array('model'=>$model));
	}
	
	/**
	 * Действие по созданию обращения
	 * @param boolean|string $id Идентификатор сопряжения, из которого будет взят адрес для инициализации атрибута
	 * "Адрес по обращению", или false. По умолчанию равно false. Если равно false, данный атрибут будет заполнен адресом из
	 * профиля пользователя
	 */
	public function actionCreate($id = false){
		$this->actionUpdate(false, $id);
	}
	
	/**
	 * Действие по редактированию обращения
	 * @param boolean|integer $id Идентификатор обращения. По умолчанию равно false
	 * @param boolean|string $id_conjugation Идентификатор сопряжения, из которого будет взят адрес для инициализации атрибута
	 * "Адрес по обращению", или false. По умолчанию равно false. Если равно false, данный атрибут будет заполнен адресом из
	 * профиля пользователя
	 */
	public function actionUpdate($id = false, $id_conjugation = false){
		if ($_FILES)
			Yii::app()->file->download();
		
		if ($key = Arrays::pop($_POST, ini_get('session.upload_progress.name')))
			Yii::app()->file->check($key);
		
		if (Yii::app()->request->isAjaxRequest && isset($_GET['ajax'])){
			$this->renderPartial('/jui_dialogs/userGrid');
			Yii::app()->end();
		}
		
		$action = $this->action->id;
		$author_enabled = Yii::app()->user->checkAccessSet('citizen, organization, clerk');
		
		if (!$id)
			$model = new AppealModel;
		else{
			$model = AppealModel::model()->findByPk($id);
			
			Yii::app()->log->fix('model_search', array('model'=>'appeal', 'id'=>$id), $model && $model->stage ? 'ok' : 'model_search');
		}
		
		if (!$model || !$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->user->checkAccess('bss1_payer'))
			$model->setAccessValue('responder', 'tsg_gor77');
		
		if (!$model->post){
			if (!$id){
				if ($author_enabled)
					$model->authorid = Yii::app()->user->id;
				
				$model->initAttributes();
				
				if ($id_conjugation && ($accounts = Yii::app()->user->accounts)){
					$roles_external = WebUser::ROLES_EXTERNAL;
					
					foreach (array('bss1', 'bss2') as $component)
						if ($roles_bss = Arrays::pop($roles_external, $component))
							foreach ($roles_bss as $role_bss)
								if ($role_accounts = Arrays::pop($accounts, $role_bss))
									foreach ($role_accounts as $account)
										if ($account['id_conjugation'] == $id_conjugation){
											$model->address = Swamper::address($account, true);
											break 3;
										}
				}
				
				if (!$model->address){
					$model_user = Yii::app()->user->model;
					$model->address = $model_user->address;
					$model->addressid = $model_user->addressid;
				}
				
				if ($responder_update = Arrays::select($model->accessValues['responder'], 'code', 'name', false, 'update', 'access'))
					$model->responder = reset($responder_update);
			}
		}else{
			$model->attributes = $model->post;
			
			if (!$id){
				if (!$model->authorid && $author_enabled)
					$model->authorid = Yii::app()->user->id;
				
				$model->initAttributes();
			}
			
			if ($model->validate()){
				if ($theme_info = Arrays::pop($di = Yii::app()->value->dataId,
					$model->getAttributeValueId('themeid').','.$model->themeid, false, false, false, ',')){
						if ($theme_category = Arrays::pop($theme_info, 'param1'))
							$model->category = Yii::app()->conformity->get('appeal_category', 'code', $theme_category);
						if ($theme_type = Arrays::pop($theme_info, 'param2'))
							$model->type = Yii::app()->conformity->get('appeal_type', 'code', $theme_type);
					}
				
				$model->timeout = Swamper::date(time() + Appeal::TIME_LIMIT, Swamper::FORMAT_DATE_SYSTEM);
				$params_fix = array('model'=>$model->getAttributes(
					array_merge(array('id'), $model->allowedAttributeNames, array('category', 'type'))), 'clerk'=>$model->responder < 0,
					'email_send'=>false);
				$model_clone = clone $model;
				
				try{
					$transaction = Yii::app()->db->beginTransaction();
					
					if (!$model->save(false))
						throw new CHttpException(500, 'save appeal error');
					
					if (!$id){
						$params_fix['model']['id'] = $model->id;
						
						$model_stage = new AppealStageModel;
						$model_stage->setAttributes(array(
							'id_appeal'=>$model->id,
							'id_owner'=>$model->authorid,
							'text'=>$params_fix['clerk'] ? '' : $model->text,
							'state'=>Yii::app()->conformity->get('appeal_stage_state', 'code', 'created'),
							'date'=>Swamper::date($time = time(), Swamper::FORMAT_DATE_SYSTEM)
						), false);
						
						if ($params_fix['clerk']){
							if (!$model_stage->save(false))
								throw new CHttpException(500, 'save appeal_stage error');
							
							$model_stage = new AppealStageModel;
							$model_stage->setAttributes(array(
								'id_appeal'=>$model->id,
								'id_owner'=>$model->authorid,
								'text'=>'',
								'state'=>Yii::app()->conformity->get('appeal_stage_state', 'code', 'accepted'),
								'date'=>Swamper::date(++$time, Swamper::FORMAT_DATE_SYSTEM)
							), false);
							
							if (!$model_stage->save(false))
								throw new CHttpException(500, 'save appeal_stage error');
							
							$model_stage = new AppealStageModel;
							$model_stage->setAttributes(array(
								'id_appeal'=>$model->id,
								'id_owner'=>-$model->responder,
								'text'=>$model->text,
								'state'=>Yii::app()->conformity->get('appeal_stage_state', 'code', 'requested'),
								'date'=>Swamper::date(++$time, Swamper::FORMAT_DATE_SYSTEM)
							), false);
						}
					}else{
						$model_stage = $model->stage[0];
						$model_stage->setAttributes(array(
							'id_owner'=>$model->authorid,
							'text'=>$model->text
						), false);
					}
					
					if (!$model_stage->save(false))
						throw new CHttpException(500, 'save appeal_stage error');
					
					if ($model->files && ($files_state = Yii::app()->user->getState('files')))
						foreach ($files_id = Strings::devisionWords($model->files) as $temp_id)
							if (mb_strpos($temp_id, File::PREFIX_SAVED_FILES) === false &&
								($file_name = Arrays::pop($files_state, "$temp_id.name"))){
									if (!$file_id = Yii::app()->file->save($temp_id, $file_name, false, $model_stage->id))
										throw new CHttpException(500, 'save appeal_file error');
									
									$params_fix['files'][$temp_id] = $file_id;
								}
					
					if ($id && ($models_file = $model_stage->file))
						foreach ($models_file as $model_file)
							if (empty($files_id) || !in_array(File::PREFIX_SAVED_FILES.$model_file->id, $files_id)){
								if (!$temp_id = Yii::app()->file->delete($model_file->id))
									throw new CHttpException(500, 'delete appeal_file error');
								
								$params_fix['files_delete'][$temp_id] = $model_file->id;
							}
					
					if ($action == 'create' && Yii::app()->email->on){
						if ($params_fix['clerk']){
							$email_title = 'appeal_create_clerk';
							$recipient = UserModel::model()->findByPk(-$model->responder, '', array(), true, false, false);
						}else{
							$email_title = 'appeal_create';
							$recipient = $model->author;
						}
						
						if ($recipient->delivery == Yii::app()->conformity->get('user_delivery_type', 'code', 'email')){
							$email_send = !Yii::app()->email->send($recipient->email, $email_title, array(
								'title'=>Strings::userName($recipient, Strings::FORMAT_USER_NAME_INITIALS),
								'link'=>$this->createAbsoluteUrl('appeal/view', array('id'=>$model->id)),
								'theme'=>$model->theme
							));
							
							if ($email_send)
								$params_fix['email_send'] = true;
							else
								throw new CHttpException(500, 'error email send');
						}
					}
					
					$transaction->commit();
					Yii::app()->user->setState('files', null);
				}catch (CException $e){
					$params_fix['msg'] = $e->getMessage();
					$transaction->rollback();
					
					$model = $model_clone;
					
					if (!empty($params_fix['files'])){
						$params_fix['files_return'] = true;
						
						foreach ($params_fix['files'] as $temp_id=>$file_id)
							if (!Yii::app()->file->reverse($temp_id, $file_id, false))
								$params_fix['files_return'] = false;
					}
					
					if (!empty($params_fix['files_delete'])){
						$params_fix['files_delete_return'] = true;
						
						foreach ($params_fix['files_delete'] as $temp_id=>$file_id)
							if (!Yii::app()->file->reverse($temp_id, $file_id))
								$params_fix['files_delete_return'] = false;
					}
				}
				
				$ok = !isset($e);
				
				Yii::app()->log->fix('appeal_'.$action, $params_fix, $ok ? 'ok' : 'appeal_'.$action);
				
				if ($ok){
					$params_redirect = array(
						'appeal/view'=>array('id'=>$model->id),
						'appeal/update'=>array('id'=>$model->id),
					);
					
					if ($action == 'update')
						$params_redirect = array_reverse($params_redirect);
					
					$this->redirectAccessible($params_redirect + array('appeal/index'));
					$this->refreshWithoutParams(true);
				}
			}
		}
		
		$this->render('update', array('model'=>$model, 'author_enabled'=>$author_enabled));
	}
	
	/**
	 * Действие по просмотру обращения
	 * @param integer $id Идентификатор обращения
	 * @throws CHttpException
	 */
	public function actionView($id){
		if ($_FILES)
			Yii::app()->file->download();
		
		if ($key = Arrays::pop($_POST, ini_get('session.upload_progress.name')))
			Yii::app()->file->check($key);
		
		if (Yii::app()->request->isAjaxRequest && isset($_GET['ajax'])){
			$this->renderPartial('/jui_dialogs/userGrid');
			Yii::app()->end();
		}
		
		$model = AppealModel::model()->findByPk($id);
		
		Yii::app()->log->fix('model_search', array('model'=>'appeal', 'id'=>$id), $model ? 'ok' : 'model_search');
		
		if (!$model || !$model->access)
			throw new CHttpException(500);
		
		$user_responder = $model->responder < 0 ?
			UserModel::model()->findByPk(-$model->responder, '', array(), true, false, false) : false;
		$importance = 0;
		
		if (in_array(Yii::app()->conformity->get('appeal_state', 'name', $model->state),
			array('registered', 'confirmed', 'extended')))
				for ($count = count(Appeal::TIME_BORDER) - 1; $count >= 0; $count--)
					if ($model->timeout <= Swamper::date(time() + Appeal::TIME_BORDER[$count], Swamper::FORMAT_DATE_SYSTEM)){
						$importance = $count + 1;
						
						break;
					}
		
		$model->timeout = Swamper::date($model->timeout, Swamper::FORMAT_DATE_USER_WITHOUT_TIME);
		
		if ($_POST){
			if ($model->post)
				$model->attributes = $model->post;
			
			if ($model->validate() && $model->command){
				$params_fix = array('model'=>$model->getAttributes($model->allowedAttributeNames), 'command'=>$model->command);
				
				try{
					$command_state = array(
						'accept'=>'accepted',
						'redirect'=>'redirected',
						'return'=>'returned',
						'request'=>'requested',
						'answer'=>'answered',
						'complete'=>'completed',
						'deny'=>'denied',
						'rollback'=>'rollback',
						'extend'=>'extended'
					);
					
					if (!isset($command_state[$model->command]))
						throw new CHttpException(500, 'unknown command');
					
					$transaction = Yii::app()->db->beginTransaction();
					
					if ($model->command !== 'rollback'){
						if (in_array($model->command, array('redirect', 'return', 'request', 'complete', 'deny')) &&
							$importance == count(Appeal::TIME_BORDER))
								throw new CHttpException(500, 'command denied because have timeout');
						
						if ($model->command == 'extend'){
							if ($model->state !== Yii::app()->conformity->get('appeal_state', 'code', 'confirmed'))
								throw new CHttpException(500, 'extend not access');
							
							$timeout = strtotime($model->timeout);
							$time = time();
							
							do{
								$timeout += Appeal::TIME_LIMIT;
							}while ($timeout <= $time);
							
							$model->timeout = Swamper::date($timeout, Swamper::FORMAT_DATE_SYSTEM);
						}
						
						$is_gor77 = $model->responder == Yii::app()->conformity->get('appeal_responder', 'code', 'tsg_gor77');
// 						$is_responder_gor77 = Yii::app()->user->id == 9667;
						$is_responder_gor77 = mb_strtolower(Yii::app()->user->model->title) ==
							mb_strtolower(Yii::app()->conformity->get('appeal_responder', 'description', 'tsg_gor77'));
						
						if ($model->command == 'accept' && !Yii::app()->user->checkAccessSet('developer, administrator') &&
							$is_gor77 != $is_responder_gor77)
								throw new CHttpException(500, 'invalid responder');
						
						if (in_array($model->command, array('accept', 'extend')) && !$model->save(false))
							throw new CHttpException(500, 'save appeal error');
						
						$model_stage = new AppealStageModel;
						$model_stage->setAttributes(array(
							'id_appeal'=>$model->id,
							'text'=>$model->comment,
							'state'=>Yii::app()->conformity->get('appeal_stage_state', 'code', $command_state[$model->command]),
							'date'=>Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM)
						), false);
						
						if (in_array($model->command, array('accept', 'complete', 'deny', 'extend')))
							$model_stage->id_owner = Yii::app()->user->id;
						elseif ($model->command == 'request')
							$model_stage->id_owner = $model->responder > 0 ? $model->authorid : -$model->responder;
						elseif ($model->command == 'redirect'){
							if (!$id_partner = Arrays::pop($_POST, 'partnerid'))
								throw new CHttpException(500, 'param "partnerid" expected');
							
							$model_partner = UserModel::model()->findByPk($id_partner, '', array(), true, false, false);
							
							Yii::app()->log->fix('model_search', array('model'=>'user', 'id'=>$id_partner),
								$model_partner ? 'ok' : 'model_search');
							
							if (!$model_partner)
								throw new CHttpException(500, 'partner not found');
							
							$params_fix['id_partner'] = $id_partner;
							$model_stage->id_owner = $id_partner;
						}else{
							if (!$model->clerk)
								throw new CHttpException(500, 'clerk not found');
							
							$params_fix['id_clerk'] = $model->clerk;
							$model_stage->id_owner = $model->clerk;
						}
						
						if (!$model_stage->save(false))
							throw new CHttpException(500, 'save appeal_stage error');
						
						if ($model->commentfiles && ($files_state = Yii::app()->user->getState('files')))
							foreach ($files_id = Strings::devisionWords($model->commentfiles) as $temp_id)
								if ($file_name = Arrays::pop($files_state, "$temp_id.name")){
									if (!$file_id = Yii::app()->file->save($temp_id, $file_name, false, $model_stage->id))
										throw new CHttpException(500, 'save appeal_file error');
									
									$params_fix['files'][$temp_id] = $file_id;
								}
						
						if (in_array($model->command, array('request', 'complete', 'deny', 'extend'))){
							$params_fix['email_send'] = false;
							
							if (Yii::app()->email->on){
								$recipient = $model->responder > 0 ? $model->author : $user_responder;
								$email_title = $model->command == 'request' ? 'appeal_request' : ($model->command == 'extend' ?
									'appeal_extend' : 'appeal_complete');
								
								if ($recipient && $recipient->delivery == Yii::app()->conformity->get('user_delivery_type', 'code', 'email')){
									$email_send = !Yii::app()->email->send($recipient->email, $email_title, array(
										'title'=>Strings::userName($recipient, Strings::FORMAT_USER_NAME_INITIALS),
										'link'=>$this->createAbsoluteUrl('appeal/view', array('id'=>$model->id)),
										'theme'=>$model->theme
									));
									
									if ($email_send)
										$params_fix['email_send'] = true;
									else
										throw new CHttpException(500, 'error email send');
								}
							}
						}
					}else{
						$stages = $model->stage;
						$qu = count($stages);
						
						if (($model->responder > 0 && $qu < 2) || ($model->responder < 0 && $qu < 4))
							throw new CHttpException(500, 'rollback denied');
						
						$model_stage = end($stages = $model->stage);
						
						if ($models_file = $model_stage->file)
							foreach ($models_file as $model_file){
								if (!$temp_id = Yii::app()->file->delete($model_file->id))
									throw new CHttpException(500, 'delete appeal_file error');
								
								$params_fix['files_delete'][$temp_id] = $model_file->id;
							}
						
						if (!$model_stage->delete())
							throw new CHttpException(500, 'delete appeal_stage error');
					}
					
					$transaction->commit();
					Yii::app()->user->setState('files', null);
				}catch (CException $e){
					$params_fix['msg'] = $e->getMessage();
					
					if (!empty($transaction))
						$transaction->rollback();
					
					if (!empty($params_fix['files'])){
						$params_fix['files_return'] = true;
						
						foreach ($params_fix['files'] as $temp_id=>$file_id)
							if (!Yii::app()->file->reverse($temp_id, $file_id, false))
								$params_fix['files_return'] = false;
					}
					
					if (!empty($params_fix['files_delete'])){
						$params_fix['files_delete_return'] = true;
						
						foreach ($params_fix['files_delete'] as $temp_id=>$file_id)
							if (!Yii::app()->file->reverse($temp_id, $file_id))
								$params_fix['files_delete_return'] = false;
					}
				}
				
				$ok = !isset($e);
				
				Yii::app()->log->fix('appeal_processing', $params_fix, $ok ? 'ok' : 'appeal_processing');
				
				if ($ok)
					$this->refreshWithoutParams(true);
				elseif ($params_fix['msg'] == 'command denied because have timeout')
					Yii::app()->user->setFlash('error', 'Необходимо продлить срок рассмотрения');
				elseif ($params_fix['msg'] == 'invalid responder')
					Yii::app()->user->setFlash('error', 'Вы не уполномочены на обработку данного обращения');
			}
		}
		
		if ($operator = Yii::app()->user->checkAccessSet('developer, administrator, clerk, specialist'))
			$house = !$model->addressid ? false : Yii::app()->db->createCommand()->
				select(array('id_bss', 'id_bars'))->
				from('{{fias_address}}')->
				where("id = '{$model->addressid}'")->
				queryRow();
		
		$this->render('view', array('model'=>$model, 'user_responder'=>$user_responder, 'importance'=>$importance,
			'operator'=>$operator, 'house'=>$operator && $house ? $house : false));
	}
	
	/**
	 * Действие по отзыву обращения
	 * @param integer $id Идентификатор обращения
	 */
	public function actionRevoke($id){
		if ($model = AppealModel::model()->findByPk($id, '', array(), true, true, false, false))
			$model->scenario = 'revoke';
		
		Yii::app()->log->fix('model_search', array('model'=>'appeal', 'id'=>$id), $model ? 'ok' : 'model_search');
		
		if (!$model || !$model->access)
			throw new CHttpException(500);
		
		$params_fix = array('model'=>$model->attributes);
		
		try{
			$transaction = Yii::app()->db->beginTransaction();
			
			$model_stage = new AppealStageModel;
			$model_stage->setAttributes(array(
				'id_appeal'=>$model->id,
				'id_owner'=>Yii::app()->user->id,
				'state'=>Yii::app()->conformity->get('appeal_stage_state', 'code', 'revoked'),
				'date'=>Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM)
			), false);
			
			if (!$model_stage->save(false))
				throw new CHttpException(500, 'error save');
			
			$transaction->commit();
		}catch (CException $e){
			$transaction->rollback();
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('appeal_revoke', $params_fix, $ok ? 'ok' : 'appeal_revoke');
		
		$this->redirect(($url = Yii::app()->request->getUrlReferrer()) && mb_strpos($url, 'index') ? $url : 
			$this->createUrl('appeal/view', array('id'=>$id)));
	}
	
	/**
	 * Действие выгрузки прикреплённых к обращениям файлов
	 * @param integer $id Идентификатор файла
	 */
	public function actionFile($id){
		$model = Yii::app()->file->get($id);
		
		Yii::app()->log->fix('model_search', array('model'=>'appeal_file', 'id'=>$id), $model ? 'ok' : 'model_search');
		
		if (!$model)
			throw new CHttpException(500);
		
		$params_fix = array('id'=>$id, 'model'=>$model->attributes);
		
		try{
			if (!$ok = Yii::app()->user->checkAccessSet('developer, administrator, clerk'))
				if (Yii::app()->user->checkAccessSet('specialist, citizen, organization')){
					$id_appeal = Yii::app()->db->createCommand()->
						select('a.id')->
						from('{{appeal}} a')->
						leftJoin('{{appeal_stage}} s', 's.id_appeal = a.id')->
						leftJoin('{{file}} f', 'f.type = :type AND f.source = s.id',
							array(':type'=>Yii::app()->conformity->get('file_type', 'code', 'appeal')))->
						where('f.id = :id', array(':id'=>$id))->
						queryScalar();
					
					if (!$id_appeal)
						throw new CHttpException(500, 'appeal not found');
					
					$model_appeal = AppealModel::model()->findByPk($id_appeal, '', array(), true, false, false, false);
					
					Yii::app()->log->fix('model_search', array('model'=>'appeal', 'id'=>$id), $model_appeal ? 'ok' : 'model_search');
					
					$ok = $model_appeal->witness;
				}
			
			if (empty($ok))
				throw new CHttpException(403, 'access denied');
			
			if (!Yii::app()->file->upload($id, $model->name))
				throw new CHttpException(500, 'error upload file');
		}catch (CHttpException $e){
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('appeal_file_upload', $params_fix, $ok ? 'ok' : ($e->statusCode == 404 ? 'file_not_found' :
			($e->statusCode == 403 ? 'file_access' : 'appeal_file_upload')));
	}
	
}
