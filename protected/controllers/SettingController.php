<?php
/**
 * Контроллер главной страницы приложения
 * @author v.zubov
 */
class SettingController extends Controller{
	
	/**
	 * Действие работы с общими настройками приложения
	 */
	public function actionGeneral(){
		$model = new SettingGeneralModel;
		
		if (!$model->access)
			throw new CHttpException(500);
		
		$name_transfer = array(
			'state'=>'application_state',
			'name'=>'name',
			'registration'=>'user_registration_type',
			'retries'=>'code_retries',
			'timeout'=>'code_timeout',
			'logdb'=>'log_db',
			'logfile'=>'log_file',
			'cache'=>'cache_type',
			'duration1'=>'duration1',
			'duration2'=>'duration2',
			'duration3'=>'duration3',
			'duration4'=>'duration4'
		);
		
		if ((!$setting_general = Yii::app()->setting->get('general')) || (!$setting_cache = Yii::app()->param->get('cache')))
			throw new CHttpException(500);
		
		$params = array_merge($setting_general, $setting_cache);
		$conformity = Yii::app()->conformity->get(false, 'code', 'name');
		
		foreach ($params as $name=>$value)
			if ($code = Arrays::pop($conformity, "$name.$value"))
				$params[$name] = $code;
		
		foreach ($name_transfer as $name=>$name2)
			$model->$name = Arrays::pop($params, $name2);
		
		if ($model->post){
			if ($model->command === 'deploy'){
				$params_fix = array('params'=>Yii::app()->param->get());
				
				try{
					if (!Yii::app()->setting->setState('init_db', false))
						throw new CHttpException(500, 'error change state');
				}catch (CException $e){
					$params_fix['msg'] = $e->getMessage();
				}
				
				$ok = !isset($e);
				
				Yii::app()->log->fix('deploy_init', $params_fix, $ok ? 'ok' : 'deploy_init');
				
				if ($ok){
					Cache::init($setting_cache);
					
					if ($remote_addr = Html::getUserAddr())
						Yii::app()->param->set('developer', $remote_addr);
					
					Yii::app()->user->checkLogout(true, true, false);
				}
			}else{
				$model->attributes = $model->post;
				
				if ($model->validate()){
					$model->removeDecoration();
					$conformity = Yii::app()->conformity->get(false, 'name', 'code');
					
					foreach ($name_transfer as $name=>$name2)
						$setting_general_new[$name2] = $model->$name;
					
					$params_fix = array('params'=>$setting_general_new);
					$setting_cache_new = Arrays::pop($setting_general_new, array_keys($setting_cache), true, true);
					
					foreach ($setting_cache_new as $name=>$value)
						if ($value_name = Arrays::pop($conformity, "$name.$value"))
							$setting_cache_new[$name] = $value_name;
					
					try{
						$transaction = Yii::app()->db->beginTransaction();
						
						if (!Yii::app()->setting->deploySection('general', $setting_general_new) ||
							(!$set_cache = Yii::app()->param->set('cache', $setting_cache_new)))
								throw new CHttpException(500, 'error update setting');
						
						$init_cache = Cache::init($setting_cache_new);
						
						if ($this->deployed && $init_cache != ($setting_cache_new['cache_type'] !== 'disabled'))
							throw new CHttpException(500, 'error update cache');
						
						$transaction->commit();
					}catch (CException $e){
						if (!empty($transaction))
							$transaction->rollback();
						
						if (!empty($set_cache))
							Yii::app()->param->set('cache', $params_cache);
						
						$params_fix['msg'] = $e->getMessage();
					}
					
					$ok = !isset($e);
					
					Yii::app()->log->fix('setting_general_update', $params_fix, $ok ? 'ok' : 'setting_general_update');
					
					if ($ok){
						if (!$this->deployed && ($remote_addr = Html::getUserAddr()))
							Yii::app()->param->set('developer', $remote_addr);
						
						Yii::app()->user->checkLogout(true, true, false);
						$this->refreshWithoutParams();
					}
				}
			}
		}
		
		$model->addDecoration();
		$this->render('general', array('model'=>$model));
	}
	
}
