<?php
/**
 * Контроллер работы с пользователями
 * @author v.zubov
 */
class UserController extends Controller{
	
	/**
	 * Действия просмотра списка пользователей
	 */
	public function actionIndex(){
		$model = new UserModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest)
			$this->render('index', array('model'=>$model));
		else
			$this->renderPartial('indexGrid', array('model'=>$model));
	}
	
	/**
	 * Действие разлогинивания пользователя
	 */
	public function actionLogout(){
		if (!Yii::app()->user->logout())
			throw new CHttpException(500);
	}
	
	/**
	 * Действие самостоятельной регистриции пользователя
	 * @param boolean|string Код подтверждения
	 */
	public function actionRegistration($code = false){
		/* $fn = Yii::getPathOfAlias('webroot.assets.user_agreement').'.pdf';
		
		if (is_file($fn)){
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");
			header("Content-Disposition: attachment; filename=1.pdf");
			header("Content-Transfer-Encoding: binary");
			
			readfile($fn);
		}else
			Arrays::printPre(); */
		
		if ($code){
			$params_fix = array('code'=>$code, 'email_send'=>false);
			
			try{
				$transaction = Yii::app()->db->beginTransaction();
				
				if (!($model = Yii::app()->code->checkHash($code, 'email')) ||
					$model->state != Yii::app()->conformity->get('user_state', 'code', 'confirm_data') ||
					!Yii::app()->code->dropCode($model->code[0]->code, 'email'))
						throw new CHttpException(500);
				
				$params_fix['user'] = $model->attributes;
				$model->state = Yii::app()->conformity->get('user_state', 'code', 'working');
				$model->agreement = true;
				
				if (!$model->save(false))
					throw new CHttpException(500);
				
				if (Yii::app()->email->on){
					$email_send = !Yii::app()->email->send($model->email, 'registration', array('fio'=>Strings::userName($model,
						Strings::FORMAT_USER_NAME_INITIALS), 'login'=>$model->login, 'link'=>$this->createAbsoluteUrl('user/restore')));
					
					if ($email_send)
						$params_fix['email_send'] = true;
					else
						throw new CHttpException(500, 'error email send');
				}
				
				$transaction->commit();
			}catch (CException $e){
				$transaction->rollback();
				$params_fix['msg'] = $e->getMessage();
			}
			
			$ok = !isset($e);
			
			Yii::app()->log->fix('user_registration_confirm', $params_fix, $ok ? 'ok' : 'user_registration_confirm');
			
			if ($ok){
				if ($description = Yii::app()->label->get('info_user_registration_confirm'))
					Yii::app()->user->setFlash('ok', array('text'=>$description, 'only_flash'=>true));
				
				$this->refreshWithoutParams();
			}
			
			Yii::app()->end();
		}
		
		$this->actionUpdate();
	}
	
	/**
	 * Действие добавления нового пользователя
	 */
	public function actionCreate(){
		$this->actionUpdate();
	}
	
	/**
	 * Действие редактирования данных профиля текущего пользователя
	 */
	public function actionPupdate(){
		$this->actionUpdate();
	}
	
	/**
	 * Действие редактирования данных пользователя.
	 * Таблица возможных действий над моделью:
	 * 	controller	action				id		model_action
	 * 	----------------------------------------
	 * 	user				registration				create
	 * 	user				create							create
	 * 	user				update				+			update
	 * 	user				pupdate							update
	 * @throws CHttpException
	 */
	public function actionUpdate($id = false){
		$action = $this->action->id;
		$registration = $action == 'registration';
		$user_states = Yii::app()->conformity->get('user_state', 'code', 'name');
		
		if ($pupdate = $action == 'pupdate')
			$model = Yii::app()->user->model;
		elseif (!$id)
			$model = new UserModel;
		else{
			$model = UserModel::model()->findByPk($id);
			
			Yii::app()->log->fix('model_search', array('model'=>'user', 'id'=>$id), $model ? 'ok' : 'model_search');
		}
		
		if (!$model)
			throw new CHttpException(500);
		
		$create = $model->action == 'create';
		
		if (!$create && !empty($model->post['password']) && empty($model->post['password2']) &&
			empty($model->post['passwordcode']) && Hashing::check($model->post['password'], $model))
				$model->setPost(Arrays::filterKey($model->post, 'password', false), $model::SET_POST_MODE_EASY);
		
		$model->hidePassword();
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!$model->post){
			if ($create){
				if ($roles_update = Arrays::select($model->accessValues['roles'], 'code', 'name', false, 'update', 'access'))
					$model->roles = array(isset($roles_update['citizen']) ? $roles_update['citizen'] :
						(isset($roles_update['organization']) ? $roles_update['organization'] : reset($roles_update)));
				
				if ($state_update = Arrays::select($model->accessValues['state'], 'code', 'name', false, 'update', 'access'))
					$model->state = array(isset($state_update['working']) ? $state_update['working'] : reset($state_update));
				
				$model->agreement = true;
			}
		}else{
			$model->attributes = $model->post;
			
			if ($model->validate()){
				$model->removeDecoration();
				$password = $model->password;
				
				try{
					$transaction = Yii::app()->db->beginTransaction();
					$confirm_data = Yii::app()->setting->get('user_registration_type') == 'confirm_data';
					
					if ($create){
						if ($registration)
							$model->state = Yii::app()->conformity->get('user_state', 'code', $confirm_data ? 'confirm_data' : 'working');
						
						$model->registration = Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM);
						$model->agreement = !$confirm_data || !$registration;
						$model->secure = true;
					}elseif ((int)$model->state === $user_states['remote'])
						$model->locked = Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM);
					elseif ((int)$model->state == $user_states['locked'])
						$model->locked = Swamper::date($model->locked, Swamper::FORMAT_DATE_SYSTEM);
					else
						$model->locked = null;
					
					$model->delivery = Yii::app()->conformity->get('user_delivery_type', 'code', 'email');
					$model->password = $model->accessAttributes['password'] == 'update' ?
						Hashing::encode($model->password, Hashing::key($model->login)) : $model->passwordHash;
					
					if (!$model->save(false))
						throw new CHttpException(500);
					
					if ($model->accessAttributes['roles'] == 'update'){
						$roles_prev = $model->prevValues['roles'] ?
							Yii::app()->user->getRoles(0, 'name', false, false, $model->prevValues['roles'], 'id', false) : array();
						$roles = $model->roles ? Yii::app()->user->getRoles(0, 'name', false, false, $model->roles, 'id', false) : array();
						
						foreach (array_diff($roles_prev, $roles) as $role)
							Yii::app()->user->revoke($role, $model->id);
						
						foreach (array_diff($roles, $roles_prev) as $role)
							Yii::app()->user->assign($role, $model->id);
					}
					
					if ($registration && $confirm_data && ((!$hash = Yii::app()->code->getHash($model, 'email')) ||
						Yii::app()->email->send($model->email, 'registration_confirm',
						array('link'=>$this->createAbsoluteUrl('', array('code'=>$hash['hash']))))))
							throw new CHttpException(500, 'error email send');
					
					$transaction->commit();
				}catch (CException $e){
					$transaction->rollback();
				}
				
				$ok = !isset($e);
				$model->password = $password;
				$params_fix = array('model'=>$model->getAttributes($model->classAttributeNames));
				
				if (!$ok)
					$params_fix['msg'] = $e->getMessage();
				elseif (!empty($hash))
					$params_fix['code'] = $hash['code'];
				
				Yii::app()->log->fix('user_'.$action, $params_fix, $ok ? 'ok' : 'user_'.$action);
				
				if ($ok){
					if ($create)
						if ($registration){
							if ($confirm_data){
								if ($description = Yii::app()->label->get('info_user_registration'))
									Yii::app()->user->setFlash('ok', array('text'=>$description, 'only_flash'=>true));
							}else{
								if (Yii::app()->email->on)
									Yii::app()->email->send($model->email, 'registration',
										array('fio'=>Strings::userName($model, Strings::FORMAT_USER_NAME_INITIALS), 'login'=>$model->login,
										'link'=>$this->createAbsoluteUrl('user/restore')));
								
								$this->redirect($this->createUrl(Yii::app()->defaultController));
							}
						}else
							$this->redirectAccessible(array(
								'user/view'=>array('id'=>$model->id),
								'user/update'=>array('id'=>$model->id),
								'user/index'
							));
					elseif ($pupdate && $model->state !== Yii::app()->conformity->get('user_state', 'code', 'working'))
						Yii::app()->user->logout();
					
					$this->refreshWithoutParams(true);
				}
			}
		}
		
		if ((int)$model->state === $user_states['remote'])
			$model->delete = $model->locked;
		
		$model->locked = Swamper::date((int)$model->state === $user_states['locked'] && $model->locked ?
			$model->locked : time() + 3600);
		$model->post = false;
		
		$this->render('update', array('action'=>$action, 'model'=>$model, 'user_states'=>$user_states));
	}
	
	/**
	 * Действие просмотра данных профиля текущего пользователя
	 */
	public function actionPview(){
		$this->actionView();
	}
	
	/**
	 * Действие просмотра данных пользователя
	 * Таблица возможных действий над моделью:
	 * 	controller	action				id		model_action
	 * 	----------------------------------------
	 * 	user				view					+			view
	 * 	user				pview								view
	 * @param integer $id Идентификатор обращения
	 * @throws CHttpException
	 */
	public function actionView($id = false){
		if ($this->action->id == 'pview')
			$model = Yii::app()->user->getModel('view');
		else{
			if ($model = UserModel::model()->findByPk($id))
				$model->scenario = 'view';
			
			Yii::app()->log->fix('model_search', array('model'=>'user', 'id'=>$id), $model ? 'ok' : 'model_search');
		}
		
		if (!$model || !$model->access)
			throw new CHttpException(500);
		
		if ((int)$model->state === Yii::app()->conformity->get('user_state', 'code', 'remote'))
			$model->delete = $model->locked;
		
		if ($powers = ManageController::getOrganizationPowers($model->title))
			$model->powers = $powers;
		
		$this->render('view', array('model'=>$model));
	}
	
	/**
	 * Действие удаления профиля текущего пользователя
	 */
	public function actionPdelete(){
		$this->actionDelete();
	}
	
	/**
	 * Действие удаления профиля пользователя
	 * Таблица возможных действий над моделью:
	 * 	controller	action				id		model_action
	 * 	----------------------------------------
	 * 	user				delete				+			delete
	 * 	user				pdelete							delete
	 * @param boolean|integer $id Идентификатор пользователя
	 * @throws CHttpException
	 */
	public function actionDelete($id = false){
		$action = $this->action->id;
		
		if ($action == 'pdelete')
			$model = Yii::app()->user->getModel('delete');
		else{
			if ($model = UserModel::model()->findByPk($id))
				$model->scenario = 'delete';
			
			Yii::app()->log->fix('model_search', array('model'=>'user', 'id'=>$id), $model ? 'ok' : 'model_search');
		}
		
		if (!$model || !$model->access)
			throw new CHttpException(500);
		
		$params_fix = array('action'=>$action, 'model'=>$model->attributes);
		
		try{
			$transaction = Yii::app()->db->beginTransaction();
			
			$model->state = Yii::app()->conformity->get('user_state', 'code', 'remote');
			$model->locked = Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM);
			
			if (!$model->save(false))
				throw new CHttpException(500, 'error save');
			
			$transaction->commit();
		}catch (CException $e){
			$transaction->rollback();
			$params_fix['msg'] = $e->getMessage();
		}
		
		$ok = !isset($e);
		
		Yii::app()->log->fix('user_'.$action, $params_fix, $ok ? 'ok' : 'user_'.$action);
		
		if ($ok && ($action == 'pdelete' || $model->id == Yii::app()->user->id) && !Yii::app()->user->logout())
			throw new CHttpException(500);
		
		$this->redirect(Yii::app()->request->getUrlReferrer());
	}
	
	/**
	 * Действие редактирования настроек профиля пользователя
	 */
	public function actionSetting(){
		$model = new UserSettingModel();
		
		if (!$model_user = Yii::app()->user->model)
			throw new CHttpException(500);
		
		$model->esia = in_array(Yii::app()->user->getRoles(0, 'id', 'esia'), $model_user->roles, true);
		$model->delivery = $model_user->delivery;
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if ($model->post){
			$model->attributes = $model->post;
			
			if ($model->validate()){
				$params_fix = array('model'=>$model->attributes, 'action'=>$model->command ? $model->command : $model->action);
				
				try{
					$transaction = Yii::app()->db->beginTransaction();
					
					if ($model->command == 'esiadone'){
						if (!$model_user->login || !$model_user->password)
							throw new CHttpException(403, 'empty login or password');
						
						if (!Yii::app()->esia->done())
							throw new CHttpException(500, 'error revoke esia role');
						
						$model->esia = false;
						$model->setAction($model->action);
						$params_fix['model']['esia'] = false;
					}else{
						$model_user->delivery = $model->delivery;
						
						if (!$model_user->save(false))
							throw new CHttpException(500, 'error save');
					}
					
					$transaction->commit();
				}catch (CHttpException $e){
					$params_fix['msg'] = $e->getMessage();
					$transaction->rollback();
				}
				
				$ok = !isset($e);
				
				Yii::app()->log->fix('user_setting', $params_fix, $ok ? 'ok' :
					($e->statusCode == 403 ? 'user_setting_empty' : 'user_setting'));
				
				if ($ok)
					$this->refreshWithoutParams(true);
			}
		}
		
		$this->render('setting', array('model'=>$model));
	}
	
	/**
	 * Действие восстановления пароля профиля пользователя
	 * @param boolean|string $code Код подтверждения
	 */
	public function actionRestore($code = false){
		$model = new UserModel('restore');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if ($code){
			$params = array('code'=>$code);
			
			try{
				if ($model_restore = Yii::app()->code->checkHash($code, 'restore')){
					$model->setAttributes($model_restore->attributes, false);
					$model->hidePassword();
					$params['user'] = $model->getAttributes($model->classAttributeNames);
					$model->isNewRecord = false;
				}
			}catch (CException $e){
				$params['msg'] = $e->getMessage();
			}
			
			$ok = !empty($model_restore);
			
			Yii::app()->log->fix('user_restore_check', $params, $ok ? 'ok' : 'user_restore_check');
		}
		
		if ($model->post){
			$model->attributes = $model->post;
			
			if ($model->validate()){
				$params = array('user'=>$model->getAttributes($model->classAttributeNames));
				$password = $model->password;
				
				try{
					$transaction = Yii::app()->db->beginTransaction();
					
					if (!$code){
						if (!Yii::app()->email->on)
							throw new CHttpException(500, 'component "email" disabled');
						
						if (!($hash = Yii::app()->code->getHash($model, 'restore')) || Yii::app()->email->send($model->email,
							'restore', array('link'=>$this->createAbsoluteUrl('', array('code'=>$hash['hash'])))))
								throw new CHttpException(500);
						
						$params['code'] = $hash['code'];
					}else{
						$model->password = Hashing::encode($model->password, Hashing::key($model->login));
						$params['code'] = $model_restore->code[0]->code;
						
						if (!Yii::app()->code->dropCode($model_restore->code[0]->code, 'restore') || !$model->save(false))
							throw new CHttpException(500);
					}
					
					$transaction->commit();
				}catch (CException $e){
					$transaction->rollback();
					$params['msg'] = $e->getMessage();
				}
				
				$ok = !isset($e);
				$model->password = $password;
				
				Yii::app()->log->fix($event = 'user_restore'.($code ? '_confirm' : ''), $params, $ok ? 'ok' : $event);
				
				if ($ok){
					if (!$code && ($description = Yii::app()->label->get('info_user_restore')))
						Yii::app()->user->setFlash('ok', $description);
					
					$this->redirect($this->createUrl(Yii::app()->defaultController));
				}
			}
		}
		
		$this->render('restore', array('model'=>$model, 'code'=>$code));
	}
	
}
