<?php
/**
 * Контроллер работы с информацией по компенсациям, связанным с лицевыми счетами пользователя
 * @author v.zubov
 */
class CompensationController extends Controller{
	
	/**
	 * Действие просмотра общей информации по компенсациям
	 */
	public function actionIndex(){
		$model = new CompensationModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest)
			$this->render('index', array('model'=>$model));
		else
			$this->renderPartial('indexGrid', array('model'=>$model));
	}
	
	/**
	 * Действие просмотра информации по выплатам по компенсации
	 */
	public function actionPayment(){
		$model = new CompensationPaymentModel('list');
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (!Yii::app()->request->isAjaxRequest){
			/* if (!$accounts = Yii::app()->user->accounts)
				throw new CHttpException(500);
			
			$id_accounts = array();
			$roles_component = Yii::app()->user->rolesComponent;
			
			foreach ($accounts as $bss_role=>$accounts_role)
				if ($bss_role == 'bss1_payer'){
					$component = Arrays::pop($roles_component, $bss_role);
					
					foreach ($accounts_role as $id_account=>$account)
						$id_accounts[$account['id']] = array('description'=>$id_account.' - '.Swamper::address($account, true),
							'component'=>$component);
				}
			
			$id = !$id_accounts ? false : reset(array_keys($id_accounts)); */
			
// 			Yii::app()->user->setState(WebUser::PREFIX_SET.'id_accounts', $id_accounts);
			$this->render('payment', array('model'=>$model/* , 'id_accounts'=>Arrays::focussing($id_accounts), 'id'=>$id */));
		}else
			$this->renderPartial('paymentGrid', array('model'=>$model));
	}
	
}
