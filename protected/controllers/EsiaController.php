<?php
/**
 * Контроллер работы с ЕСИА
 * @author v.zubov
 */
class EsiaController extends Controller{
	
	/**
	 * Действие авторизации пользователя через сервис ЕСИА
	 */
	public function actionLogin(){
		Yii::app()->esia->login();
	}
	
	/**
	 * Действие разавторизации пользователя, авторизованного через сервис ЕСИА
	 */
	public function actionLogout(){
		Yii::app()->esia->logout();
	}
	
}
