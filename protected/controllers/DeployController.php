<?php
/**
 * Контроллер развёртывания приложения
 * @author v.zubov
 */
class DeployController extends Controller{
	
	/**
	 * @var string Псевдоним пути к файлу лога миграции
	 */
	const DIR_MIGRATE_LOG = 'application.logs';
	
	/**
	 * @var string Псевдоним пути к файлу лога миграции
	 */
	const FN_MIGRATE_LOG = 'migrate %s.txt';
	
	/**
	 * @var string Символьный идентификатор роли Плательщика в БД старой версии ЛК
	 */
	const MIGRATE_ROLE_PAYER = 'repair_payer';
	
	/**
	 * Действие настройки подключения к БД
	 * @throws CHttpException
	 */
	public function actionDb(){
		$model = new DbModel;
		
		if (!$model->access)
			throw new CHttpException(500);
		
		if (Yii::app()->db->active){
			$model->setAttributes(Yii::app()->db->get(), false);
			$model->addDecoration();
		}
		
		if ($model->post){
			$model->attributes = $model->post;
			
			if ($model->validate()){
				$model->removeDecoration();
				$cfg = Arrays::filterKey($model->attributes, Yii::app()->attribute->confirmAttributeNames, false);
				
				if (Yii::app()->db->applyCfg($cfg)){
					if (Yii::app()->db->setCfg($cfg) && Yii::app()->setting->deploy())
						$this->changeState();
				}else
					$model->addError('host', Yii::app()->errorAttribute->get($model->modelName, 'host', 'connect'));
			}
		}
		
		$this->render('db', array('model'=>$model));
	}
	
	/**
	 * Действие настройки подключения к Биллинговому центру ЖКХ
	 */
	public function actionBss1(){
		$this->actionConnection();
	}
	
	/**
	 * Действие настройки подключения к Биллинговому центру КР
	 */
	public function actionBss2(){
		$this->actionConnection();
	}
	
	/**
	 * Действие настройки подключения к Основному порталу
	 */
	public function actionPortal(){
		$this->actionConnection();
	}
	
	/**
	 * Действие настройки подключения к Почтовому серверу
	 */
	public function actionEmail(){
		$this->actionConnection();
	}
	
	/**
	 * Действие настройки подключения к SMS-шлюзу
	 */
	public function actionPhone(){
		$this->actionConnection();
	}
	
	/**
	 * Действие настройки подключения к ЕСИА
	 */
	public function actionEsia(){
		$this->actionConnection();
	}
	
	/**
	 * Действие настройки подключения к ФИАС-агенту
	 */
	public function actionFias(){
		$this->actionConnection();
	}
	
	/**
	 * Действие по генерации данных БД
	 * @throws CHttpException
	 */
	public function actionData(){
// 		$this->actionMigrate();
		
		$model = new DataModel('deploy_generation');
		$install_types = Yii::app()->conformity->get('table_group_install_type', 'code', 'name');
		$new_data = array();
		
		if (!$model->access || (!$state_tables = Yii::app()->table->checkTables()))
			throw new CHttpException(500);
		
		foreach ($state_tables as $name=>$check){
			if (!$check)
				$new_data[] = $name;
			
			$model->$name = $install_types[in_array($name, array('user', 'message', 'appeal', 'log', 'file')) && $check ?
				'old' : 'standart'];
		}
		
		if ($model->post){
			if ($model->command === 'back')
				$this->changeState(false);
			else{
				if (empty($model->post['generation']) || !in_array(
					Yii::app()->conformity->get('user_generation_type', 'code', 'rand'), $model->post['generation']))
						$model->scenario = 'deploy';
				
				$model->attributes = $model->post;
				
				foreach ($state_tables as $name=>$check){
					$correct_values = Arrays::forming(array(
						$install_types['standart']=>true,
						$install_types['generation']=>$name == 'user' && $model->generation,
						$install_types['old']=>$check
					), true);
					
					if (!in_array($model->$name, $correct_values))
						$model->$name = $install_types['standart'];
				}
				
				if ($model->validate()){
					$model->removeDecoration();
					
					if (Yii::app()->table->deploy($model, $state_tables)){
						Yii::app()->setting->setState('locked', false);
						
						if ($user = UserModel::model()->find('login = :login', array(':login'=>'developer')))
							$login = Yii::app()->user->login($user);
						
						$this->redirect($this->createUrl(!empty($login) ? 'setting/general' : Yii::app()->defaultController));
					}
				}
			}
			
			$model->scenario = 'deploy_generation';
		}
		
		$this->render('data', array('model'=>$model, 'new_data'=>$new_data));
	}
	
	/**
	 * Действие настройки подключений с переключателем
	 * @throws CHttpException
	 */
	public function actionConnection(){
		$component_name = $this->action->id;
		$model_name = preg_replace('/\d/u', '', $component_name);
		$class_name = Strings::nameModify($model_name, Strings::FORMAT_NAME_MODIFY_UPPER, true, true);
		$model = new $class_name('on', $component_name);
		
		if (!$model->access)
			throw new CHttpException(500);
		
		$model->setAttributes(Yii::app()->$component_name->get(), false);
		
		if ($component_name == 'email' && ($protocol = Yii::app()->$component_name->get('email_protocol')))
			$model->protocol = Yii::app()->conformity->get('email_protocol', 'code', $protocol);
		
		$model->addDecoration();
		
		if ($model->post){
			if ($model->command === 'back')
				$this->changeState(false);
			else{
				if (empty($model->post['on']))
					$model->scenario = 'skip';
				
				$model->attributes = $model->post;
				
				if ($model->validate()){
					$model->removeDecoration();
					$attributes = $model->attributes;
					
					if ($component_name == 'email')
						$attributes['email_protocol'] = (int)$attributes['protocol'];
					
					Yii::app()->$component_name->set($attributes);
					
					if (Yii::app()->$component_name->deploy())
						$this->changeState();
				}
			}
			
			$model->scenario = 'on';
		}
		
		$this->render('connection', array('model'=>$model));
	}
	
	/**
	 * Действие миграции данных пользователей
	 */
	public function actionMigrate(){
		$path = Yii::getPathOfAlias(static::DIR_MIGRATE_LOG).DIRECTORY_SEPARATOR.
			sprintf(static::FN_MIGRATE_LOG, Swamper::date(false, Swamper::FORMAT_DATE_SYSTEM_WITHOUT_TIME));
		$command = Yii::app()->dbOld->createCommand()->
			from('{{user}}')->
			where('state = :state', array(':state'=>Yii::app()->conformity->get('user_state', 'code', 'working')));
		
		if (!Yii::app()->request->isAjaxRequest){
			$qu = $command->select('count(*)')->queryScalar();
			Yii::app()->user->setState('migrate', 0);
			file_put_contents($path, '');
			
			$this->render('migrate', array('qu'=>$qu));
		}else{
			try{
				set_time_limit(0);
				$migrate = Yii::app()->user->getState('migrate');
				$limit = rand(20, 100);
// 				$limit = 10;
				
				$users = $command->
// 					leftJoin('{{auth_assignment}} aa', 'aa.userid = t.id')->
					offset((int)$migrate)->
					limit($limit)->
					order('id')->
					queryAll();
				
				if ($users){
					$transaction = Yii::app()->db->beginTransaction();
					$roles = Arrays::focussing(Yii::app()->user->rolesList, 'id');
					$role_payer = $roles['bss2_payer'];
					
					foreach ($users as $key=>$user){
						$assignments = Yii::app()->dbOld->createCommand()->
							select('itemname')->
							from('{{auth_assignment}}')->
							where('userid = :userid', array(':userid'=>$user['id']))->
							queryColumn();
						
						if (!$assignments){
							file_put_contents($path, 'roles not found: '.$user['id'].PHP_EOL, FILE_APPEND);
							continue;
						}
						
						$conjugations = Yii::app()->dbOld->createCommand()->
							from('{{user_conjugation}}')->
							where('id_owner = :id_owner', array(':id_owner'=>$user['id']))->
							order('id')->
							queryAll();
						
						$model_user = new UserModel;
						$model_user->setAttributes($user, false);
						$model_user->id = null;
						$model_user->delivery = Yii::app()->conformity->get('user_delivery_type', 'code', 'email');
						$model_user->secure = true;
						
						if (!$model_user->save(false))
							throw new CHttpException(500, 'error model user save: '.$user['id']);
						
						foreach ($assignments as $assignment){
							$role = $assignment === static::MIGRATE_ROLE_PAYER ? 'bss2_payer' : $assignment;
							
							if (!isset($roles[$role])){
								file_put_contents($path, 'unknown role: '.$user['id'].' '.$role.PHP_EOL, FILE_APPEND);
								continue;
							}
							
							if ($role === 'bss2_payer'){
								if (!$conjugations){
									file_put_contents($path, 'empty conjugations: '.$user['id'].' '.$role.PHP_EOL, FILE_APPEND);
									continue;
								}
								
								$assign = false;
								
								foreach ($conjugations as $conjugation){
									$params = json_decode($conjugation['params'], true);
									
									if (!isset($params['organization'])){
										file_put_contents($path, 'param "organization" expected: '.$user['id'].' '.$conjugation['id'].PHP_EOL,
											FILE_APPEND);
										continue;
									}
									
// 									BSS VARIANT
									/*
									$result = Yii::app()->bssOld->query('user_accounts', array(
										'id'=>$conjugation['id_external'],
										'organization'=>$params['organization'],
										'type'=>2
									));
									
									if (!$result)
										throw new CHttpException(500, 'error bss request: '.$user['id'].' '.$conjugation['id']);
									
									if (!$account_list = Arrays::pop($result, 'account_list'))
										continue; 
									*/
									
// 									TABLE VARIANT
									$account_list = Yii::app()->db->createCommand()->
										from('{{user_account}}')->
										where('id_payer = :id_payer AND organization = :organization',
											array(':id_payer'=>$conjugation['id_external'],
											':organization'=>$params['organization'] ? 'true' : 'false'))->
										queryAll();
									
									if (!$account_list){
										file_put_contents($path, 'empty account list: '.$user['id'].' '.$conjugation['id'].PHP_EOL, FILE_APPEND);
										continue;
									}
									
									foreach ($account_list as $account){
										$model_conjugation = new UserConjugationModel;
										$model_conjugation->id_owner = $model_user->id;
										$model_conjugation->id_external = $account['id_account'];
// 										$model_conjugation->id_external = $account['id'];
										$model_conjugation->id_role = $role_payer;
										
										if (!$model_conjugation->save(false))
											throw new CHttpException(500, 'error model conjugation save: '.$user['id'].' '.
												$conjugation['id'].' '.$account['id_account']);
// 												$conjugation['id'].' '.$account['id']);
										
										$assign = true;
									}
								}
								
								if (!$assign){
									file_put_contents($path, 'invalid conjugations: '.$user['id'].' '.$role.PHP_EOL, FILE_APPEND);
									continue;
								}
							}
							
							$model_assignment = new AuthAssignmentModel;
							$model_assignment->itemname = $role;
							$model_assignment->userid = $model_user->id;
							$model_assignment->data = 'N;';
							
							if (!$model_assignment->save(false))
								throw new CHttpException(500, 'error model assignment save: '.$user['id'].' '.$role);
							
							$payer = $roles['bss2_payer'];
						}
					}
					
					$transaction->commit();
					
					Yii::app()->user->setState('migrate', $migrate = $migrate + count($users));
					echo $migrate;
				}else{
					Yii::app()->user->setState('migrate', null);
					echo 'end';
				}
			}catch (CException $e){
				if (!empty($transaction))
					$transaction->rollback();
				
				echo $e->getMessage();
			}
		}
		
		Yii::app()->end();
	}
	
	/**
	 * Изменение статуса развёртывания приложения
	 * @param string $forward Признак изменения статуса в большую сторону. По умолчанию равно true
	 * @return boolean Успешность выполнения
	 */
	protected function changeState($forward = true){
		if ($this->chainStates)
			return Yii::app()->setting->setState(Setting::PREFIX_DEPLOY.Arrays::changeState($this->chainStates,
				$this->action->id, 2 * $forward - 1));
	}
	
}
