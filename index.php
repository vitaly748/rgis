<?php
/**
 * Входной скрипт
 */
$dir_root = dirname(__FILE__).'/protected/';
$yii = $dir_root.'framework/yii.php';
$config = $dir_root.'config/main.php';

//defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 4);

require_once($yii);

Yii::createWebApplication($config)->run();
